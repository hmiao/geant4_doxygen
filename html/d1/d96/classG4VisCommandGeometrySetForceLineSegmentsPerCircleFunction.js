var classG4VisCommandGeometrySetForceLineSegmentsPerCircleFunction =
[
    [ "~G4VisCommandGeometrySetForceLineSegmentsPerCircleFunction", "d1/d96/classG4VisCommandGeometrySetForceLineSegmentsPerCircleFunction.html#a2544496a809b6b943dfe08149c76198d", null ],
    [ "G4VisCommandGeometrySetForceLineSegmentsPerCircleFunction", "d1/d96/classG4VisCommandGeometrySetForceLineSegmentsPerCircleFunction.html#ae0a0c8f887b1567ddca08d26824f66f6", null ],
    [ "operator()", "d1/d96/classG4VisCommandGeometrySetForceLineSegmentsPerCircleFunction.html#a58999ec39e4ca16f75722411fb0e9199", null ],
    [ "fLineSegmentsPerCircle", "d1/d96/classG4VisCommandGeometrySetForceLineSegmentsPerCircleFunction.html#ae5fddeeb651c9f810a9f39caf9e6fcb9", null ]
];