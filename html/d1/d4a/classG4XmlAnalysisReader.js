var classG4XmlAnalysisReader =
[
    [ "~G4XmlAnalysisReader", "d1/d4a/classG4XmlAnalysisReader.html#a18c2359b90447db79e63f7dce7bc22ae", null ],
    [ "G4XmlAnalysisReader", "d1/d4a/classG4XmlAnalysisReader.html#a1c53406033bdd821fb5a56253fdf91e5", null ],
    [ "CloseFilesImpl", "d1/d4a/classG4XmlAnalysisReader.html#a9efea39c0d50f4d84358ac3de404b676", null ],
    [ "GetNtuple", "d1/d4a/classG4XmlAnalysisReader.html#a49d92473ec55c3c46dea00e0e06a2201", null ],
    [ "GetNtuple", "d1/d4a/classG4XmlAnalysisReader.html#abbb85d623bd3215dfc51c42b90525840", null ],
    [ "GetNtuple", "d1/d4a/classG4XmlAnalysisReader.html#afa9151cb1719cc716638804040e5bb9d", null ],
    [ "Instance", "d1/d4a/classG4XmlAnalysisReader.html#af3990d7e8bb804db1be7469af6335756", null ],
    [ "Reset", "d1/d4a/classG4XmlAnalysisReader.html#ac38f81b56b4d0e404774c514f92b665d", null ],
    [ "G4ThreadLocalSingleton< G4XmlAnalysisReader >", "d1/d4a/classG4XmlAnalysisReader.html#a6616b7f2bd757a343da6cb22d41b09c3", null ],
    [ "fFileManager", "d1/d4a/classG4XmlAnalysisReader.html#a32b67d5e53d50e18ef60d2aa3da2c3ef", null ],
    [ "fgMasterInstance", "d1/d4a/classG4XmlAnalysisReader.html#a3f232e16f2dd8e1cd6fb19c6788352c7", null ],
    [ "fkClass", "d1/d4a/classG4XmlAnalysisReader.html#afd4722a3901b256a6f54bd3b6bc6de91", null ],
    [ "fNtupleManager", "d1/d4a/classG4XmlAnalysisReader.html#a834da91790e5e91f8f67154edcec2dc3", null ]
];