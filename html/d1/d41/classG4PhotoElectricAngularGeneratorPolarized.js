var classG4PhotoElectricAngularGeneratorPolarized =
[
    [ "G4PhotoElectricAngularGeneratorPolarized", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#af3fe5a587bfebf82637f0d7b3454adef", null ],
    [ "~G4PhotoElectricAngularGeneratorPolarized", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#acc7e500000ae5271bb27f5fe3949dcb9", null ],
    [ "G4PhotoElectricAngularGeneratorPolarized", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#a626787a571402084dc20c398ba9132ef", null ],
    [ "CrossSectionMajorantFunction", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#a4844dd3131666f88d6336af91feb79fd", null ],
    [ "DSigmaKshellGavrila1959", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#afc2e4cbc7ac2de82df97e037583c49be", null ],
    [ "DSigmaL1shellGavrila", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#a0b4a3bec90e29636d66b167acd182912", null ],
    [ "operator=", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#ad30088578cc48c98f8117788ad5350ab", null ],
    [ "PerpendicularVector", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#a1bee998785249ddd47acee85464d29c4", null ],
    [ "PhotoElectronComputeFinalDirection", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#a26ba096900102734327a00b55bea50fa", null ],
    [ "PhotoElectronGeneratePhiAndTheta", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#a802ebdcbef01fb531bc9fdf4fa886977", null ],
    [ "PhotoElectronGetMajorantSurfaceAandCParameters", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#a107adc3d8c1af968f88955f12025f298", null ],
    [ "PhotoElectronRotationMatrix", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#a869a99c31017a8504c04ee9c539b06d6", null ],
    [ "PrintGeneratorInformation", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#a3a138ba653d97c32570c7e9958830e94", null ],
    [ "SampleDirection", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#ac02ac504f0ea82387e5f366851bbc375", null ],
    [ "aMajorantSurfaceParameterTable", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#adbcb949ceaa569eaf5e8d36173aa6e05", null ],
    [ "betaArray", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#a56b80e0844b81c3d5be2ed9cd50454f1", null ],
    [ "cMajorantSurfaceParameterTable", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html#a85c425748b3b745e520f583557522bfb", null ]
];