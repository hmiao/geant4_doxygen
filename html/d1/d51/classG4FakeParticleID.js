var classG4FakeParticleID =
[
    [ "G4FakeParticleID", "d1/d51/classG4FakeParticleID.html#a4ffa903707238f08ce693d711f235cd1", null ],
    [ "G4FakeParticleID", "d1/d51/classG4FakeParticleID.html#a09ac04582bfacd5e86f46756b0f700a5", null ],
    [ "G4FakeParticleID", "d1/d51/classG4FakeParticleID.html#a964937969a57b913d1559b1c36e9ea17", null ],
    [ "Create", "d1/d51/classG4FakeParticleID.html#a610f986b607a6aed09938b92073b3172", null ],
    [ "Initialize", "d1/d51/classG4FakeParticleID.html#acdfb17a9fe4ee12c3bb76a5bb4700f8c", null ],
    [ "Last", "d1/d51/classG4FakeParticleID.html#a398d6dab69e49c9986d5ef7c74c532f1", null ],
    [ "operator const int &", "d1/d51/classG4FakeParticleID.html#aae0546b272a3271cd10518e2b4aa87e1", null ],
    [ "operator int &", "d1/d51/classG4FakeParticleID.html#a531c1a03527c8cbc38465e107df5d362", null ],
    [ "operator<", "d1/d51/classG4FakeParticleID.html#a6aab94ac8b8d6ab99d1831f19115cdfe", null ],
    [ "operator=", "d1/d51/classG4FakeParticleID.html#a11f785fffe23497d1d92e8821f9dfde0", null ],
    [ "operator=", "d1/d51/classG4FakeParticleID.html#ae330993595983553c008919c33c21bd0", null ],
    [ "operator==", "d1/d51/classG4FakeParticleID.html#ad9a19faec00ca86bb06da51a6d0a4a7d", null ],
    [ "operator==", "d1/d51/classG4FakeParticleID.html#a616ed95f0a6b17a6b2a8a5f84f594319", null ],
    [ "operator+", "d1/d51/classG4FakeParticleID.html#aeb46a749c75a480ca8343ce28ceba3ab", null ],
    [ "operator-", "d1/d51/classG4FakeParticleID.html#a32fcd6da78fb15256d4c313d9bdb7b4d", null ],
    [ "fLastValue", "d1/d51/classG4FakeParticleID.html#a66fdc4e9d32eeb4a15a416c82b213344", null ],
    [ "fValue", "d1/d51/classG4FakeParticleID.html#aed13f3f06e794fdff0ee37a10e9bd470", null ]
];