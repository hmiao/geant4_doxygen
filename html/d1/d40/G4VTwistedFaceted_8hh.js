var G4VTwistedFaceted_8hh =
[
    [ "G4VTwistedFaceted", "d4/d92/classG4VTwistedFaceted.html", "d4/d92/classG4VTwistedFaceted" ],
    [ "G4VTwistedFaceted::LastState", "d3/d70/classG4VTwistedFaceted_1_1LastState.html", "d3/d70/classG4VTwistedFaceted_1_1LastState" ],
    [ "G4VTwistedFaceted::LastVector", "de/dd3/classG4VTwistedFaceted_1_1LastVector.html", "de/dd3/classG4VTwistedFaceted_1_1LastVector" ],
    [ "G4VTwistedFaceted::LastValue", "dc/d54/classG4VTwistedFaceted_1_1LastValue.html", "dc/d54/classG4VTwistedFaceted_1_1LastValue" ],
    [ "G4VTwistedFaceted::LastValueWithDoubleVector", "db/d44/classG4VTwistedFaceted_1_1LastValueWithDoubleVector.html", "db/d44/classG4VTwistedFaceted_1_1LastValueWithDoubleVector" ]
];