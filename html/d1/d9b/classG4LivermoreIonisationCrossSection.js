var classG4LivermoreIonisationCrossSection =
[
    [ "G4LivermoreIonisationCrossSection", "d1/d9b/classG4LivermoreIonisationCrossSection.html#ab986d90b87a530fa6d27cfc8563fda86", null ],
    [ "~G4LivermoreIonisationCrossSection", "d1/d9b/classG4LivermoreIonisationCrossSection.html#a78a20c4800f7aa362db819c8c9924485", null ],
    [ "G4LivermoreIonisationCrossSection", "d1/d9b/classG4LivermoreIonisationCrossSection.html#a24e174e287864f4ce863df45a0b6efbb", null ],
    [ "CrossSection", "d1/d9b/classG4LivermoreIonisationCrossSection.html#a9c293e8a8f6f99c05beef0767a136b4e", null ],
    [ "GetCrossSection", "d1/d9b/classG4LivermoreIonisationCrossSection.html#a94e76b00d7c088e8629c2f3616f1b578", null ],
    [ "Initialise", "d1/d9b/classG4LivermoreIonisationCrossSection.html#a7300bf8a4c82b6a96f5646651f6f0546", null ],
    [ "operator=", "d1/d9b/classG4LivermoreIonisationCrossSection.html#ac7e124a1e345b76e8f6f7a81170ad7d7", null ],
    [ "Probabilities", "d1/d9b/classG4LivermoreIonisationCrossSection.html#afaef11a92bdbf2d379f8f408dc220348", null ],
    [ "crossSectionHandler", "d1/d9b/classG4LivermoreIonisationCrossSection.html#ad6bb6424d552c007c782b45deca36369", null ],
    [ "fHighEnergyLimit", "d1/d9b/classG4LivermoreIonisationCrossSection.html#abbb0c6976af2569788d1f1fcde9cf994", null ],
    [ "fLowEnergyLimit", "d1/d9b/classG4LivermoreIonisationCrossSection.html#ad1bf7acb27658e7427ceeabb27e9024d", null ],
    [ "transitionManager", "d1/d9b/classG4LivermoreIonisationCrossSection.html#a63121a736d5c56651673d692874bf4b6", null ],
    [ "verboseLevel", "d1/d9b/classG4LivermoreIonisationCrossSection.html#a3139e4b68eb3a5e1ccd3587b16d533aa", null ]
];