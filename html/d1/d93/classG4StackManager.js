var classG4StackManager =
[
    [ "G4StackManager", "d1/d93/classG4StackManager.html#a09cd4d28e1e7acaf24c2f7ed255451b2", null ],
    [ "~G4StackManager", "d1/d93/classG4StackManager.html#af5bf25b28aa8b9f0109bec68fbce0ae0", null ],
    [ "clear", "d1/d93/classG4StackManager.html#a5ee965d4fab8e714c8a758519a31494a", null ],
    [ "ClearPostponeStack", "d1/d93/classG4StackManager.html#ae75f4363a080b509433f94546636a5b1", null ],
    [ "ClearUrgentStack", "d1/d93/classG4StackManager.html#a833b0bf1a8a92b1360e59fba61960b06", null ],
    [ "ClearWaitingStack", "d1/d93/classG4StackManager.html#ab3c0dce3b49531f726802014d8f7a9e5", null ],
    [ "DefaultClassification", "d1/d93/classG4StackManager.html#a4e5b61e682c89caece70c51aeeb30be3", null ],
    [ "GetNPostponedTrack", "d1/d93/classG4StackManager.html#a5c0b6c835312fc0975b14ccdd51287e4", null ],
    [ "GetNTotalTrack", "d1/d93/classG4StackManager.html#a09d5baa0367964e43b1c3afb9178eeff", null ],
    [ "GetNUrgentTrack", "d1/d93/classG4StackManager.html#a290ef6455dae90817a0bd501b6633020", null ],
    [ "GetNWaitingTrack", "d1/d93/classG4StackManager.html#af2bc5a9981461c3d33df30d1307d6cb2", null ],
    [ "operator!=", "d1/d93/classG4StackManager.html#a53f17d5805e0e3f0ae5cff4fd406e09d", null ],
    [ "operator=", "d1/d93/classG4StackManager.html#a855313f19139bf0e32078091edda1f83", null ],
    [ "operator==", "d1/d93/classG4StackManager.html#af21b4ece4234c5a72f9d25506d9a7b53", null ],
    [ "PopNextTrack", "d1/d93/classG4StackManager.html#a7b77549c59f9c8f77d88f309ac65b9e2", null ],
    [ "PrepareNewEvent", "d1/d93/classG4StackManager.html#ac0e35cc29bdc83dee751b23f3b8f43e2", null ],
    [ "PushOneTrack", "d1/d93/classG4StackManager.html#a87ab322c44212368394f226bc4d02ee9", null ],
    [ "ReClassify", "d1/d93/classG4StackManager.html#ad07984e6e7650df2255a42eea64e4a07", null ],
    [ "SetNumberOfAdditionalWaitingStacks", "d1/d93/classG4StackManager.html#a63b0f8270becfba2c24f4d1d2cc7da51", null ],
    [ "SetUserStackingAction", "d1/d93/classG4StackManager.html#ae4395abe780a256f52e480ed6f33f472", null ],
    [ "SetVerboseLevel", "d1/d93/classG4StackManager.html#a677a3ac2800c853ec2acc01c3a65f97d", null ],
    [ "TransferOneStackedTrack", "d1/d93/classG4StackManager.html#a25dd80909e1efe32e4b5891bb6714ee7", null ],
    [ "TransferStackedTracks", "d1/d93/classG4StackManager.html#ae12564f62e1d6e897b64ad99381460b5", null ],
    [ "additionalWaitingStacks", "d1/d93/classG4StackManager.html#af827e77f99462d709fd7442d18f073fe", null ],
    [ "numberOfAdditionalWaitingStacks", "d1/d93/classG4StackManager.html#a2e2e74900d76214384e3bbae47537aa8", null ],
    [ "postponeStack", "d1/d93/classG4StackManager.html#a32bf1f61a1cc10dd8c5a42a6b9db0669", null ],
    [ "theMessenger", "d1/d93/classG4StackManager.html#abb4dd61fa75005e4b8276e76443fd2d3", null ],
    [ "urgentStack", "d1/d93/classG4StackManager.html#a61158bad183f133e48ac6eed3f5fe5fb", null ],
    [ "userStackingAction", "d1/d93/classG4StackManager.html#af931d60552ed6bf65c009284bf25fa52", null ],
    [ "verboseLevel", "d1/d93/classG4StackManager.html#aa215e4fabd452d80b4112f31986954c0", null ],
    [ "waitingStack", "d1/d93/classG4StackManager.html#a22c60590c465e959fafa548d44baba9a", null ]
];