var classG4AdjointCSMatrix =
[
    [ "G4AdjointCSMatrix", "d1/d5a/classG4AdjointCSMatrix.html#ab90c82b4ec2ed63f581cfd4457902a3b", null ],
    [ "~G4AdjointCSMatrix", "d1/d5a/classG4AdjointCSMatrix.html#afdc7d824ad3b52d7ba8f7639bbe8cfb0", null ],
    [ "AddData", "d1/d5a/classG4AdjointCSMatrix.html#ab5dbe2f6c2c5413f7b7653e14b9a6124", null ],
    [ "Clear", "d1/d5a/classG4AdjointCSMatrix.html#ad29800421de2d031f662ce4c4592f3da", null ],
    [ "GetData", "d1/d5a/classG4AdjointCSMatrix.html#a5f4a9f423fccaaaf3f63351725d31df4", null ],
    [ "GetLogCrossSectionvector", "d1/d5a/classG4AdjointCSMatrix.html#a0ac720a7434b51879f19ec221a303498", null ],
    [ "GetLogPrimEnergyVector", "d1/d5a/classG4AdjointCSMatrix.html#a642903194dd535bec4d1deef6fde2ebc", null ],
    [ "IsScatProjToProj", "d1/d5a/classG4AdjointCSMatrix.html#a43885dd9cb25dac802b3268b5293974d", null ],
    [ "Read", "d1/d5a/classG4AdjointCSMatrix.html#ad6bdfbee96db59adfee543596bca55d4", null ],
    [ "Write", "d1/d5a/classG4AdjointCSMatrix.html#a2cae8da5846ab6530960dc29d87ee8a3", null ],
    [ "fLog0Vector", "d1/d5a/classG4AdjointCSMatrix.html#a9767c2be38dfa7d892cdbb2d3972ecf0", null ],
    [ "fLogCrossSectionVector", "d1/d5a/classG4AdjointCSMatrix.html#a1a7bfa9a6b4bbcc1209dab378561b319", null ],
    [ "fLogPrimEnergyVector", "d1/d5a/classG4AdjointCSMatrix.html#ac4e6be47a4d9ee11a6b4f899ca0b80b5", null ],
    [ "fLogProbMatrix", "d1/d5a/classG4AdjointCSMatrix.html#a47f535e72d6e196b3199b19c51d906b7", null ],
    [ "fLogProbMatrixIndex", "d1/d5a/classG4AdjointCSMatrix.html#a183c214a6a0cd0d3d64f7a49584091f1", null ],
    [ "fLogSecondEnergyMatrix", "d1/d5a/classG4AdjointCSMatrix.html#a25ddad9aa933b8fdac581d130f5a625d", null ],
    [ "fNbPrimEnergy", "d1/d5a/classG4AdjointCSMatrix.html#a1772f4b70409b105a79af203b407b1b4", null ],
    [ "fScatProjToProj", "d1/d5a/classG4AdjointCSMatrix.html#a820082f8b81f75669f330f51e2130476", null ]
];