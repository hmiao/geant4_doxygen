var classG4ecpssrFormFactorLixsModel =
[
    [ "G4ecpssrFormFactorLixsModel", "d1/d15/classG4ecpssrFormFactorLixsModel.html#aebd220ff4904d58f51bc91bbfb2ee830", null ],
    [ "~G4ecpssrFormFactorLixsModel", "d1/d15/classG4ecpssrFormFactorLixsModel.html#afd0d868aa7d10c1483eee38b982a2189", null ],
    [ "G4ecpssrFormFactorLixsModel", "d1/d15/classG4ecpssrFormFactorLixsModel.html#a89dcee01698859ccbf602eb399ab9561", null ],
    [ "CalculateL1CrossSection", "d1/d15/classG4ecpssrFormFactorLixsModel.html#a16c34aab3b580c9a5c2ae30f385f9986", null ],
    [ "CalculateL2CrossSection", "d1/d15/classG4ecpssrFormFactorLixsModel.html#a31b15657d84d09c01e02f581f748848d", null ],
    [ "CalculateL3CrossSection", "d1/d15/classG4ecpssrFormFactorLixsModel.html#ac34b8f760b5539e1502915dd925c50c5", null ],
    [ "operator=", "d1/d15/classG4ecpssrFormFactorLixsModel.html#adc98bab9fab16a10bdbc7da643da9e19", null ],
    [ "alphaL1DataSetMap", "d1/d15/classG4ecpssrFormFactorLixsModel.html#a99242ac8c2801e62bf6e88d5a69a60c2", null ],
    [ "alphaL2DataSetMap", "d1/d15/classG4ecpssrFormFactorLixsModel.html#adf59bd8aaf594226fd1b023ba3b7dfc9", null ],
    [ "alphaL3DataSetMap", "d1/d15/classG4ecpssrFormFactorLixsModel.html#a6f1c159eefa4094939983bacf7a96ba2", null ],
    [ "interpolation", "d1/d15/classG4ecpssrFormFactorLixsModel.html#accd61698b01bc56bf9346e922bde41db", null ],
    [ "protonL1DataSetMap", "d1/d15/classG4ecpssrFormFactorLixsModel.html#a4289daba06f314eef15932f362e9292f", null ],
    [ "protonL2DataSetMap", "d1/d15/classG4ecpssrFormFactorLixsModel.html#a8b228d13928c7d5251ee369b7873e0af", null ],
    [ "protonL3DataSetMap", "d1/d15/classG4ecpssrFormFactorLixsModel.html#af868dc7cec2e4b3ee42ab439a4a33e55", null ]
];