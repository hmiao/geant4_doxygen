var classG4ecpssrFormFactorKxsModel =
[
    [ "G4ecpssrFormFactorKxsModel", "d1/df9/classG4ecpssrFormFactorKxsModel.html#ac260be94901bf5abdfc18d8adaa5fe4c", null ],
    [ "~G4ecpssrFormFactorKxsModel", "d1/df9/classG4ecpssrFormFactorKxsModel.html#a32de65d0008b07689c8ff74dbb936bb4", null ],
    [ "G4ecpssrFormFactorKxsModel", "d1/df9/classG4ecpssrFormFactorKxsModel.html#ae4c55dd677d593e1a4d1e0f1151a7a7b", null ],
    [ "CalculateCrossSection", "d1/df9/classG4ecpssrFormFactorKxsModel.html#a61c3b2a7d38895ea5ae46034249996d8", null ],
    [ "operator=", "d1/df9/classG4ecpssrFormFactorKxsModel.html#aa4163b9bc99d9b1d8aca9c1ba3360ff7", null ],
    [ "alphaDataSetMap", "d1/df9/classG4ecpssrFormFactorKxsModel.html#a4a3ad459be446e80fdb0056b6fce334a", null ],
    [ "interpolation", "d1/df9/classG4ecpssrFormFactorKxsModel.html#afe4b76f2a8b4a7f1ab7b8a40d6b88b76", null ],
    [ "protonDataSetMap", "d1/df9/classG4ecpssrFormFactorKxsModel.html#a1504fe7465b88b0975efc2b7977e9d69", null ]
];