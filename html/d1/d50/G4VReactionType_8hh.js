var G4VReactionType_8hh =
[
    [ "G4VReactionType", "d0/dd1/classG4VReactionType.html", "d0/dd1/classG4VReactionType" ],
    [ "ReactionType", "d1/d50/G4VReactionType_8hh.html#a360e20f142dbd097b0d0d0620111b30b", [
      [ "totallyDiffusionControlled", "d1/d50/G4VReactionType_8hh.html#a360e20f142dbd097b0d0d0620111b30bac875630d5b1e146ca657cf16b9c54517", null ],
      [ "partiallyDiffusionControlled", "d1/d50/G4VReactionType_8hh.html#a360e20f142dbd097b0d0d0620111b30ba555abcbf21ae218064e09708f7f459cb", null ]
    ] ]
];