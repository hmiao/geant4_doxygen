var classG4XmlRFileManager =
[
    [ "G4XmlRFileManager", "d1/d9c/classG4XmlRFileManager.html#a1c08a6a5fd5315cae8237a1057d95d7d", null ],
    [ "G4XmlRFileManager", "d1/d9c/classG4XmlRFileManager.html#a2b75e9e59b668b96f2869918ee4f70b3", null ],
    [ "~G4XmlRFileManager", "d1/d9c/classG4XmlRFileManager.html#a1d34563722d9b657410f89e4a06a1c4f", null ],
    [ "CloseFiles", "d1/d9c/classG4XmlRFileManager.html#a0d93138bc590dec4d53902eb12493572", null ],
    [ "GetFileType", "d1/d9c/classG4XmlRFileManager.html#aece567ef1f90c62bc7bf497ff21a4d9c", null ],
    [ "GetHandler", "d1/d9c/classG4XmlRFileManager.html#a7bfe6a8c0ca9aff820d70846b2afe8fd", null ],
    [ "GetRFile", "d1/d9c/classG4XmlRFileManager.html#a142e3b7c5b82e549503a8ae9465dd2ef", null ],
    [ "OpenRFile", "d1/d9c/classG4XmlRFileManager.html#a3830113b6373a75c5e3dbacf4234131c", null ],
    [ "fkClass", "d1/d9c/classG4XmlRFileManager.html#a3e56ac5b706203f33360a97d8d5fdcc2", null ],
    [ "fReadFactory", "d1/d9c/classG4XmlRFileManager.html#a46ddd6c9627a5bbd26995e18a7f92aaa", null ],
    [ "fRFiles", "d1/d9c/classG4XmlRFileManager.html#ae22b6692a68f90acf092f34d71788599", null ]
];