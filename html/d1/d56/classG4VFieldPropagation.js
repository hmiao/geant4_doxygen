var classG4VFieldPropagation =
[
    [ "G4VFieldPropagation", "d1/d56/classG4VFieldPropagation.html#a30b685a95f5fe0603a2a36561f9a0ad8", null ],
    [ "~G4VFieldPropagation", "d1/d56/classG4VFieldPropagation.html#aebca493f17d3014040cb6133a58ac1b3", null ],
    [ "G4VFieldPropagation", "d1/d56/classG4VFieldPropagation.html#a27afc11d35cdc08aa334bfd777d6d9b5", null ],
    [ "GetMomentumTransfer", "d1/d56/classG4VFieldPropagation.html#aad1e49e8e1380a332805309438bed772", null ],
    [ "Init", "d1/d56/classG4VFieldPropagation.html#a18c8adbdbb0d0fb06169c48b2b198e38", null ],
    [ "operator!=", "d1/d56/classG4VFieldPropagation.html#a010641c99471aa510427fb12b791597e", null ],
    [ "operator=", "d1/d56/classG4VFieldPropagation.html#ae6a09fcc7f280f702568f30caae70dfb", null ],
    [ "operator==", "d1/d56/classG4VFieldPropagation.html#ae6f8352ca0a7ca8383b799651dcf50df", null ],
    [ "Transport", "d1/d56/classG4VFieldPropagation.html#aac3a00f079f1cff449f20fd872d9db01", null ]
];