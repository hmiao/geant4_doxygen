var classG4PenelopeIonisationCrossSection =
[
    [ "G4PenelopeIonisationCrossSection", "d1/d17/classG4PenelopeIonisationCrossSection.html#ae85ed36c04b1a6eeea1b848df1e4fab2", null ],
    [ "~G4PenelopeIonisationCrossSection", "d1/d17/classG4PenelopeIonisationCrossSection.html#a273aa8fee83b3d3eb1dfa762e30d9733", null ],
    [ "G4PenelopeIonisationCrossSection", "d1/d17/classG4PenelopeIonisationCrossSection.html#ad5953bcc6414d4ee263829ac30457f9d", null ],
    [ "CrossSection", "d1/d17/classG4PenelopeIonisationCrossSection.html#a8406c5973921a8ef4f069247c4a80aab", null ],
    [ "FindShellIDIndex", "d1/d17/classG4PenelopeIonisationCrossSection.html#a73d868c5d937563af962ccdd44befd9c", null ],
    [ "GetCrossSection", "d1/d17/classG4PenelopeIonisationCrossSection.html#a74c772f8a7b942610a365e7919a893a2", null ],
    [ "GetVerbosityLevel", "d1/d17/classG4PenelopeIonisationCrossSection.html#abc966dbecfb7f20bd51963f3c915b737", null ],
    [ "operator=", "d1/d17/classG4PenelopeIonisationCrossSection.html#a692b9ab0fb830f46c353b7318728387c", null ],
    [ "Probabilities", "d1/d17/classG4PenelopeIonisationCrossSection.html#aff0306508f458f7f7711cb17e2987d9b", null ],
    [ "SetVerbosityLevel", "d1/d17/classG4PenelopeIonisationCrossSection.html#a076264c69f3cbd567e480e7eaf19c26d", null ],
    [ "fCrossSectionHandler", "d1/d17/classG4PenelopeIonisationCrossSection.html#a9e2a097aa16fa58252d92ffca120383d", null ],
    [ "fHighEnergyLimit", "d1/d17/classG4PenelopeIonisationCrossSection.html#a4008c275b5db55e083c2b145e7e6457e", null ],
    [ "fLowEnergyLimit", "d1/d17/classG4PenelopeIonisationCrossSection.html#a98ea0e5136ce596badf7dd231aab89aa", null ],
    [ "fNMaxLevels", "d1/d17/classG4PenelopeIonisationCrossSection.html#a63e05d19caefa668d12ea50b4a87b661", null ],
    [ "fOscManager", "d1/d17/classG4PenelopeIonisationCrossSection.html#a31e7a90a28467c1fe4c75e49b0c18056", null ],
    [ "fShellIDTable", "d1/d17/classG4PenelopeIonisationCrossSection.html#a0a2656c49abdd5d63eb4f0ab8bc0c570", null ],
    [ "fTransitionManager", "d1/d17/classG4PenelopeIonisationCrossSection.html#a8346d7d0e9022249748995f96f88c24e", null ],
    [ "fVerboseLevel", "d1/d17/classG4PenelopeIonisationCrossSection.html#af8e2445263ba0195e6e32c6e8c8d3268", null ]
];