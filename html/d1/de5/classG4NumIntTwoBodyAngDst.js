var classG4NumIntTwoBodyAngDst =
[
    [ "G4NumIntTwoBodyAngDst", "d1/de5/classG4NumIntTwoBodyAngDst.html#a13f574953e5200be165e5b2c44306a66", null ],
    [ "~G4NumIntTwoBodyAngDst", "d1/de5/classG4NumIntTwoBodyAngDst.html#a6b66442051a23747561578dbef46694e", null ],
    [ "GetCosTheta", "d1/de5/classG4NumIntTwoBodyAngDst.html#a7275221e4528d23436ac04db4f964a47", null ],
    [ "Interpolate", "d1/de5/classG4NumIntTwoBodyAngDst.html#a063558c440e96f58f9a83d690e912d5d", null ],
    [ "angDist", "d1/de5/classG4NumIntTwoBodyAngDst.html#a6de4cf464c40fc1ec177703b218621e2", null ],
    [ "angDists", "d1/de5/classG4NumIntTwoBodyAngDst.html#afb5f0f7bf36641a6fc3e8d5b575a910d", null ],
    [ "cosBins", "d1/de5/classG4NumIntTwoBodyAngDst.html#a2132c5d39f0c1dea8afae660373b747c", null ],
    [ "labKE", "d1/de5/classG4NumIntTwoBodyAngDst.html#a9a2c52f299a38336c839449af74d7c82", null ],
    [ "tcoeff", "d1/de5/classG4NumIntTwoBodyAngDst.html#afacd6991026d6c23c96d60bf4e5c47f5", null ]
];