var G4ScintillationTrackInformation_8hh =
[
    [ "G4ScintillationTrackInformation", "d8/de8/classG4ScintillationTrackInformation.html", "d8/de8/classG4ScintillationTrackInformation" ],
    [ "G4ScintillationType", "d1/d9e/G4ScintillationTrackInformation_8hh.html#aa72eadfe1573a50d09fb9e20a243c0a1", [
      [ "Fast", "d1/d9e/G4ScintillationTrackInformation_8hh.html#aa72eadfe1573a50d09fb9e20a243c0a1a44e0add47cd0ec96505d444d425b70af", null ],
      [ "Medium", "d1/d9e/G4ScintillationTrackInformation_8hh.html#aa72eadfe1573a50d09fb9e20a243c0a1a8cfb9311a439a51b14159ed0970f398b", null ],
      [ "Slow", "d1/d9e/G4ScintillationTrackInformation_8hh.html#aa72eadfe1573a50d09fb9e20a243c0a1a15e47d3c9733bd519c1bd00349ce10e3", null ]
    ] ],
    [ "aScintillationTIAllocator", "d1/d9e/G4ScintillationTrackInformation_8hh.html#a7f15d488a7b964841e6d8cd9d89aab6a", null ]
];