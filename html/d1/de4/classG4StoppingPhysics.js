var classG4StoppingPhysics =
[
    [ "G4StoppingPhysics", "d1/de4/classG4StoppingPhysics.html#aa507b46a0de8d6141eec32e4df5e4b39", null ],
    [ "G4StoppingPhysics", "d1/de4/classG4StoppingPhysics.html#a56214a42b076eedc0c96a4e3308c708f", null ],
    [ "~G4StoppingPhysics", "d1/de4/classG4StoppingPhysics.html#a7e6fbc2ec485b4d7caf5dedfd67fb44a", null ],
    [ "ConstructParticle", "d1/de4/classG4StoppingPhysics.html#a02e753b1612fae96181394c257262629", null ],
    [ "ConstructProcess", "d1/de4/classG4StoppingPhysics.html#af42a1eaa770069d2d6ef85f0ae8d94a4", null ],
    [ "SetMuonMinusCapture", "d1/de4/classG4StoppingPhysics.html#a160856deed78a259e4ae42bbc00ea818", null ],
    [ "useMuonMinusCapture", "d1/de4/classG4StoppingPhysics.html#afb06ce259db545d14451d0f29064c1bc", null ],
    [ "verbose", "d1/de4/classG4StoppingPhysics.html#a349ba196ad436a35e672573768926aef", null ]
];