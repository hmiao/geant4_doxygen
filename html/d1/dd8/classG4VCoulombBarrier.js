var classG4VCoulombBarrier =
[
    [ "G4VCoulombBarrier", "d1/dd8/classG4VCoulombBarrier.html#aab99d2d74cfed1998d077c4771a8910a", null ],
    [ "~G4VCoulombBarrier", "d1/dd8/classG4VCoulombBarrier.html#aa05ab51fd4c45eade4117a2b879e4e20", null ],
    [ "G4VCoulombBarrier", "d1/dd8/classG4VCoulombBarrier.html#a70e42186285eaab09ec2bec3b319b49a", null ],
    [ "BarrierPenetrationFactor", "d1/dd8/classG4VCoulombBarrier.html#a5e7ec44df864bbc579330f46fb398ebd", null ],
    [ "GetA", "d1/dd8/classG4VCoulombBarrier.html#a18422576168d2f31870a5fac555ad934", null ],
    [ "GetCoulombBarrier", "d1/dd8/classG4VCoulombBarrier.html#a5d4e63dc08180d5c02070cfce9fcb060", null ],
    [ "GetR0", "d1/dd8/classG4VCoulombBarrier.html#a2e631f9722c938b7f28bd5550fda70d9", null ],
    [ "GetRho", "d1/dd8/classG4VCoulombBarrier.html#a101c479c0392c1dc872359d34b4bf524", null ],
    [ "GetZ", "d1/dd8/classG4VCoulombBarrier.html#af1bb400d21c2f72d54b9a85a3c8b0536", null ],
    [ "operator!=", "d1/dd8/classG4VCoulombBarrier.html#a027b96f72a178e65b4f313e09ddc2ed7", null ],
    [ "operator=", "d1/dd8/classG4VCoulombBarrier.html#aa62a0b3bc427e1a3d1ff0efad73e18b9", null ],
    [ "operator==", "d1/dd8/classG4VCoulombBarrier.html#ab76f707b206bb0a8aa4a3cb8c0e075c6", null ],
    [ "SetParameters", "d1/dd8/classG4VCoulombBarrier.html#aff9e75c13a78b1f624b7cca25e7b7722", null ],
    [ "theA", "d1/dd8/classG4VCoulombBarrier.html#ae715e24e6cf9c00c5d0bb5e9b6839351", null ],
    [ "theR0", "d1/dd8/classG4VCoulombBarrier.html#ae8e7d27f693e1b322da3a7d75cfa111d", null ],
    [ "theRho", "d1/dd8/classG4VCoulombBarrier.html#a07263b93bd04659040ce9404789e11c2", null ],
    [ "theZ", "d1/dd8/classG4VCoulombBarrier.html#aeeeeb94b9263a8bd3d51d8efd4590452", null ]
];