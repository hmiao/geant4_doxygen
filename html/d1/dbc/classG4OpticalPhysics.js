var classG4OpticalPhysics =
[
    [ "G4OpticalPhysics", "d1/dbc/classG4OpticalPhysics.html#af1388e54bae5a09fa1e9de2480b813e8", null ],
    [ "~G4OpticalPhysics", "d1/dbc/classG4OpticalPhysics.html#a336c191c8dcd1715bee7400369502576", null ],
    [ "G4OpticalPhysics", "d1/dbc/classG4OpticalPhysics.html#a7725ab86248736db76ef29b93d1d9582", null ],
    [ "ConstructParticle", "d1/dbc/classG4OpticalPhysics.html#ad55b27ddd9cfeeb4907c81a32d93fcb2", null ],
    [ "ConstructProcess", "d1/dbc/classG4OpticalPhysics.html#a05e98910b962dc02b18887d59a8add29", null ],
    [ "operator=", "d1/dbc/classG4OpticalPhysics.html#a3c8abd719d6fb3428b690425bb3be3bf", null ],
    [ "PrintStatistics", "d1/dbc/classG4OpticalPhysics.html#af0fe212ffa96646fcf0caf97e30691f5", null ],
    [ "PrintWarning", "d1/dbc/classG4OpticalPhysics.html#ab1603573a8f046a5dce05a77b065fed2", null ]
];