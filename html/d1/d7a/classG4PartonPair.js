var classG4PartonPair =
[
    [ "G4PartonPair", "d1/d7a/classG4PartonPair.html#adfeae038b35a0d311ec880753528c0d9", null ],
    [ "~G4PartonPair", "d1/d7a/classG4PartonPair.html#a49f9a389e1c9bfe001bbc1a129ab311c", null ],
    [ "G4PartonPair", "d1/d7a/classG4PartonPair.html#adcb3a0e2a95591f7c4f1894aaa3ed665", null ],
    [ "GetCollisionType", "d1/d7a/classG4PartonPair.html#ab9f0855b329199f34228dc190930d024", null ],
    [ "GetDirection", "d1/d7a/classG4PartonPair.html#aeb63ad4a70dcc51c47ffb6380c0716e7", null ],
    [ "GetParton1", "d1/d7a/classG4PartonPair.html#a9424e4aed9f0ef20698050ca7048bbc9", null ],
    [ "GetParton2", "d1/d7a/classG4PartonPair.html#a13d649cffddddb2e2d84c08ee36a03b1", null ],
    [ "operator!=", "d1/d7a/classG4PartonPair.html#ab2770bc88b1aab3345dbca30204c7c7c", null ],
    [ "operator==", "d1/d7a/classG4PartonPair.html#a46d3854f12299d1bf106b805a9c6fdcf", null ],
    [ "SetCollisionType", "d1/d7a/classG4PartonPair.html#a3f76e6a25bc830a19d7eb4f0aab5ceb6", null ],
    [ "SetPartons", "d1/d7a/classG4PartonPair.html#affaf6b10b834964bbc25072e61b9adb6", null ],
    [ "CollisionType", "d1/d7a/classG4PartonPair.html#ada4e5d1b0d26047e92c4787756060d5f", null ],
    [ "Direction", "d1/d7a/classG4PartonPair.html#a5b866f4196638b57b302edfdbc0951e9", null ],
    [ "Parton1", "d1/d7a/classG4PartonPair.html#ad52a36ed63eab3caa0e7866de3380715", null ],
    [ "Parton2", "d1/d7a/classG4PartonPair.html#a499f82893998cd6eab576c0fbeaaabbf", null ]
];