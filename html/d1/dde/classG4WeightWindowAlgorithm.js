var classG4WeightWindowAlgorithm =
[
    [ "G4WeightWindowAlgorithm", "d1/dde/classG4WeightWindowAlgorithm.html#af5029e81e01a0e01955630a80269e847", null ],
    [ "~G4WeightWindowAlgorithm", "d1/dde/classG4WeightWindowAlgorithm.html#a356be4b5e675bf5f67d4e531d9f22558", null ],
    [ "Calculate", "d1/dde/classG4WeightWindowAlgorithm.html#abdb336e6e319a0b9f213dc55c8d2a8b5", null ],
    [ "fMaxNumberOfSplits", "d1/dde/classG4WeightWindowAlgorithm.html#aa50563eaf476631c40602bf6bbdc021c", null ],
    [ "fSurvivalFactor", "d1/dde/classG4WeightWindowAlgorithm.html#affecd3b37e0d30ddeaf6842fd707a011", null ],
    [ "fUpperLimitFactor", "d1/dde/classG4WeightWindowAlgorithm.html#ab6b4ffd001fb3b74d79fd06441109318", null ]
];