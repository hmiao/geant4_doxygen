var structG4OpenInventorQtExaminerViewer_1_1viewPtData =
[
    [ "aspectRatio", "d1/d4c/structG4OpenInventorQtExaminerViewer_1_1viewPtData.html#a02b640471930411663272b7b1be73284", null ],
    [ "camType", "d1/d4c/structG4OpenInventorQtExaminerViewer_1_1viewPtData.html#a5e6e9d7b0425b2615eabf88873547ae1", null ],
    [ "farDistance", "d1/d4c/structG4OpenInventorQtExaminerViewer_1_1viewPtData.html#ae584f52ecc0fe942c3ed7015e39e5574", null ],
    [ "focalDistance", "d1/d4c/structG4OpenInventorQtExaminerViewer_1_1viewPtData.html#a503b82cf90303681f7624e6d8a02e940", null ],
    [ "height", "d1/d4c/structG4OpenInventorQtExaminerViewer_1_1viewPtData.html#a3df559f8d3a4987fbe42e0c7e9422984", null ],
    [ "nearDistance", "d1/d4c/structG4OpenInventorQtExaminerViewer_1_1viewPtData.html#af220fe5054782cf5f3700b97013e2eb4", null ],
    [ "orientation", "d1/d4c/structG4OpenInventorQtExaminerViewer_1_1viewPtData.html#a41b45c4f28533bd1e9840d7817dbeec0", null ],
    [ "position", "d1/d4c/structG4OpenInventorQtExaminerViewer_1_1viewPtData.html#a616d5b83d372cd2744fbac2f563df7be", null ],
    [ "viewportMapping", "d1/d4c/structG4OpenInventorQtExaminerViewer_1_1viewPtData.html#aa3dbcc079d296789505263a953d13419", null ],
    [ "viewPtName", "d1/d4c/structG4OpenInventorQtExaminerViewer_1_1viewPtData.html#a0d6bb3ab50b7bc100a4a2f4e80c784d0", null ]
];