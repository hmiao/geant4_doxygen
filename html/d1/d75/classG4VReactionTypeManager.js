var classG4VReactionTypeManager =
[
    [ "G4VReactionTypeManager", "d1/d75/classG4VReactionTypeManager.html#ae0384e20efada3ea639fe926544c8dd1", null ],
    [ "~G4VReactionTypeManager", "d1/d75/classG4VReactionTypeManager.html#acaa1add924f351584f33e8bdf898d7df", null ],
    [ "GetReactionTypeTable", "d1/d75/classG4VReactionTypeManager.html#aee95c91c41a94cac4080e2dc3739b23e", null ],
    [ "SetReactionTypeTable", "d1/d75/classG4VReactionTypeManager.html#a6ba40fda6239cb2bfc66e41709099359", null ],
    [ "SetTypeTableByID", "d1/d75/classG4VReactionTypeManager.html#ab2a36ab029753d14406c4b8baac5eccd", null ]
];