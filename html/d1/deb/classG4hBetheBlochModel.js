var classG4hBetheBlochModel =
[
    [ "G4hBetheBlochModel", "d1/deb/classG4hBetheBlochModel.html#a913d4394941e1bfaa976d5dc0ddbca2d", null ],
    [ "~G4hBetheBlochModel", "d1/deb/classG4hBetheBlochModel.html#aac3c47d7527f86b897fe1815effe97f4", null ],
    [ "BetheBlochFormula", "d1/deb/classG4hBetheBlochModel.html#a247ba4e841047ced9e20cba43530addd", null ],
    [ "HighEnergyLimit", "d1/deb/classG4hBetheBlochModel.html#ae9dd171974f395caf74b2e3176a29834", null ],
    [ "HighEnergyLimit", "d1/deb/classG4hBetheBlochModel.html#a8dd4100539bce47f5e7ebdb1130f2898", null ],
    [ "IsInCharge", "d1/deb/classG4hBetheBlochModel.html#a458ba73d8d5ff4f5fa9b071b1068d19b", null ],
    [ "IsInCharge", "d1/deb/classG4hBetheBlochModel.html#a0d13de4a747c8368a8be8c3a1f57d0be", null ],
    [ "LowEnergyLimit", "d1/deb/classG4hBetheBlochModel.html#a501023671bb959f959884962e6ab7011", null ],
    [ "LowEnergyLimit", "d1/deb/classG4hBetheBlochModel.html#a7b2b6ebdde8ef93238f1e07e86ff537a", null ],
    [ "TheValue", "d1/deb/classG4hBetheBlochModel.html#a7f40a1842453a220a4bc5ec734a2932f", null ],
    [ "TheValue", "d1/deb/classG4hBetheBlochModel.html#a8645d348e42e924afc5b511b16d2f5f8", null ],
    [ "bg2lim", "d1/deb/classG4hBetheBlochModel.html#ade9471af548d1cd676d68192953e5539", null ],
    [ "highEnergyLimit", "d1/deb/classG4hBetheBlochModel.html#a81cd421d49da1d4727bf04dfc1dded46", null ],
    [ "lowEnergyLimit", "d1/deb/classG4hBetheBlochModel.html#a07edae98d2aa7ca3728390cec03f69db", null ],
    [ "taulim", "d1/deb/classG4hBetheBlochModel.html#a9746ee5418a39ba4747c1614897ff97d", null ],
    [ "twoln10", "d1/deb/classG4hBetheBlochModel.html#ada6afe7bf957dc9ee95ba6e5c154c9d2", null ]
];