var classG4CrossSectionInelastic =
[
    [ "G4CrossSectionInelastic", "d1/da8/classG4CrossSectionInelastic.html#a6908dcee352c44505859782ed7517dc8", null ],
    [ "~G4CrossSectionInelastic", "d1/da8/classG4CrossSectionInelastic.html#a152b7e7d2d030b51a9e70a1b239ad6bb", null ],
    [ "G4CrossSectionInelastic", "d1/da8/classG4CrossSectionInelastic.html#ab29310e3573ee84f5aeba7548b5d93e1", null ],
    [ "BuildPhysicsTable", "d1/da8/classG4CrossSectionInelastic.html#a2876d7b621e8d3d212f83fdefd5adf2d", null ],
    [ "CrossSectionDescription", "d1/da8/classG4CrossSectionInelastic.html#a9a006c7d954248eecf16da62582123c2", null ],
    [ "DumpPhysicsTable", "d1/da8/classG4CrossSectionInelastic.html#ae9979e9f474e89c32e815f96070c938c", null ],
    [ "GetElementCrossSection", "d1/da8/classG4CrossSectionInelastic.html#a8eb4d629cfad65bf3c3e413146f25f72", null ],
    [ "IsElementApplicable", "d1/da8/classG4CrossSectionInelastic.html#ab93cc1ff3e50e4380b283869a2787326", null ],
    [ "operator=", "d1/da8/classG4CrossSectionInelastic.html#a1cc4fd226b02bb85a47f50f66b74227f", null ],
    [ "component", "d1/da8/classG4CrossSectionInelastic.html#a7ea81bdcc23491129f9676fef14da1f1", null ],
    [ "nist", "d1/da8/classG4CrossSectionInelastic.html#ad1c35b112263385d50b3269607916d71", null ],
    [ "Zmax", "d1/da8/classG4CrossSectionInelastic.html#af35bfb68b65a9d351acd4d4bea205a5b", null ],
    [ "Zmin", "d1/da8/classG4CrossSectionInelastic.html#a1d9ec0165b2d84f871d750f6fab4eba9", null ]
];