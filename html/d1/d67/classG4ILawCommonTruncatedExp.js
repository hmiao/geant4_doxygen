var classG4ILawCommonTruncatedExp =
[
    [ "G4ILawCommonTruncatedExp", "d1/d67/classG4ILawCommonTruncatedExp.html#a05c4b3fdcb0d38ebb54464ae60c9461e", null ],
    [ "~G4ILawCommonTruncatedExp", "d1/d67/classG4ILawCommonTruncatedExp.html#a03381568f319e40adf3fe23ff42b2ddc", null ],
    [ "ComputeEffectiveCrossSectionAt", "d1/d67/classG4ILawCommonTruncatedExp.html#a24f7e720e3617b70d0a6ca2f07a13c82", null ],
    [ "ComputeNonInteractionProbabilityAt", "d1/d67/classG4ILawCommonTruncatedExp.html#a64a513bfc73882aa9ba0255f6ac1f50b", null ],
    [ "GetInteractionDistance", "d1/d67/classG4ILawCommonTruncatedExp.html#a5306d1ac4f87051a25a234c11419c2f8", null ],
    [ "GetMaximumDistance", "d1/d67/classG4ILawCommonTruncatedExp.html#a669f0d48b7863e9acb80cd1509b42e73", null ],
    [ "IsEffectiveCrossSectionInfinite", "d1/d67/classG4ILawCommonTruncatedExp.html#a962a39781870b12e799e160fe2990158", null ],
    [ "IsSingular", "d1/d67/classG4ILawCommonTruncatedExp.html#aae0eade1db5c9742e99c0ee513057be0", null ],
    [ "SampleInteractionLength", "d1/d67/classG4ILawCommonTruncatedExp.html#aae8d5611f8b9ecf97239c0c04cd63971", null ],
    [ "SetForceCrossSection", "d1/d67/classG4ILawCommonTruncatedExp.html#a00ec11dc197e02598734b0345edfd8e4", null ],
    [ "SetMaximumDistance", "d1/d67/classG4ILawCommonTruncatedExp.html#a969553c4d9c5162e5cffb6d3e45fb27a", null ],
    [ "SetSelectedProcessXSfraction", "d1/d67/classG4ILawCommonTruncatedExp.html#a8067d1067544daf2c8278b2a57b93572", null ],
    [ "SetSelectedProcessXSfraction", "d1/d67/classG4ILawCommonTruncatedExp.html#ac9ba488adfb04af1e4be2ef6c3bf3118", null ],
    [ "UpdateInteractionLengthForStep", "d1/d67/classG4ILawCommonTruncatedExp.html#ae46a1256fd231f719c6e717f0e5cdee0", null ],
    [ "fExpInteractionLaw", "d1/d67/classG4ILawCommonTruncatedExp.html#a439863e56dadf34c1bc8b908a4bedb53", null ],
    [ "fInteractionDistance", "d1/d67/classG4ILawCommonTruncatedExp.html#ab1937503c7bfa656b9d2d76e11bbcee1", null ],
    [ "fSelectedProcessXSfraction", "d1/d67/classG4ILawCommonTruncatedExp.html#a784a1a209fecc663c39fd55c2f4aa3ed", null ]
];