var classG4PolarizedBremsstrahlungModel =
[
    [ "G4PolarizedBremsstrahlungModel", "d1/db4/classG4PolarizedBremsstrahlungModel.html#abc13c76060c33ea56f999dfb14b85fed", null ],
    [ "~G4PolarizedBremsstrahlungModel", "d1/db4/classG4PolarizedBremsstrahlungModel.html#ac5dbda5588897e28cbb62d7a2f5b9ff9", null ],
    [ "G4PolarizedBremsstrahlungModel", "d1/db4/classG4PolarizedBremsstrahlungModel.html#a3fb029119f2b2d2f246611ef30df92d4", null ],
    [ "Initialise", "d1/db4/classG4PolarizedBremsstrahlungModel.html#a45de797b39ce4b70c8046c4720ad4caf", null ],
    [ "operator=", "d1/db4/classG4PolarizedBremsstrahlungModel.html#a7a145180777f7805752448cfccc0dc0b", null ],
    [ "SampleSecondaries", "d1/db4/classG4PolarizedBremsstrahlungModel.html#ab200a6adfd5c9ca16c37df237f901262", null ],
    [ "SelectedAtom", "d1/db4/classG4PolarizedBremsstrahlungModel.html#a0ae537a2c03043c8daf9783898b4ded3", null ],
    [ "fCrossSectionCalculator", "d1/db4/classG4PolarizedBremsstrahlungModel.html#a4b60e9d67536066ba7b1f8c828802097", null ]
];