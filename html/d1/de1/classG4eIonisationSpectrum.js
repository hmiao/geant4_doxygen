var classG4eIonisationSpectrum =
[
    [ "G4eIonisationSpectrum", "d1/de1/classG4eIonisationSpectrum.html#a5b80bf4c6842606755457f10c3caa46d", null ],
    [ "~G4eIonisationSpectrum", "d1/de1/classG4eIonisationSpectrum.html#af65763ebc5085176c2b5427cfd945b03", null ],
    [ "G4eIonisationSpectrum", "d1/de1/classG4eIonisationSpectrum.html#a68d71378aa1eb18381c935ae83c92acd", null ],
    [ "AverageEnergy", "d1/de1/classG4eIonisationSpectrum.html#a0ae234eb564dc3e2fde86982155469ba", null ],
    [ "AverageValue", "d1/de1/classG4eIonisationSpectrum.html#aa183961250cba91100262f3304b51286", null ],
    [ "Excitation", "d1/de1/classG4eIonisationSpectrum.html#afebf6aef225aade420f66cad3af389ad", null ],
    [ "Function", "d1/de1/classG4eIonisationSpectrum.html#a9c8b81a4f3faa8e35b9e0b8799af96f7", null ],
    [ "IntSpectrum", "d1/de1/classG4eIonisationSpectrum.html#a106411329ae62cb813964eb892249277", null ],
    [ "MaxEnergyOfSecondaries", "d1/de1/classG4eIonisationSpectrum.html#a90b38f073e0f603100243c70ce4d0d75", null ],
    [ "operator=", "d1/de1/classG4eIonisationSpectrum.html#a4ecde0f3d4164171fe87018ec0fa168e", null ],
    [ "PrintData", "d1/de1/classG4eIonisationSpectrum.html#ab1289dcd35e47129b5bfe3a55acdafbb", null ],
    [ "Probability", "d1/de1/classG4eIonisationSpectrum.html#af576bcddd02a2c884b129516fafbbead", null ],
    [ "SampleEnergy", "d1/de1/classG4eIonisationSpectrum.html#a79b419af4e10f37a14dda4079c4e316a", null ],
    [ "factor", "d1/de1/classG4eIonisationSpectrum.html#a35648fab55decbf7b39add89ecdb165c", null ],
    [ "iMax", "d1/de1/classG4eIonisationSpectrum.html#a128b47c5b3b1e9c78d96b1c80fbf177b", null ],
    [ "lowestE", "d1/de1/classG4eIonisationSpectrum.html#aa72c45ae1088fe091fff37054919fe40", null ],
    [ "theParam", "d1/de1/classG4eIonisationSpectrum.html#a1ceec283f8d52d22181a83b0fdc65b23", null ],
    [ "verbose", "d1/de1/classG4eIonisationSpectrum.html#ac25e8637816c6f841473c9b0ba3f0747", null ]
];