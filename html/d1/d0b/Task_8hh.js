var Task_8hh =
[
    [ "PTL::TaskFuture< RetT >", "d0/d8c/classPTL_1_1TaskFuture.html", "d0/d8c/classPTL_1_1TaskFuture" ],
    [ "PTL::PackagedTask< RetT, Args >", "d6/d83/classPTL_1_1PackagedTask.html", "d6/d83/classPTL_1_1PackagedTask" ],
    [ "PTL::Task< RetT, Args >", "d0/df1/classPTL_1_1Task.html", "d0/df1/classPTL_1_1Task" ],
    [ "PTL::Task< RetT, void >", "df/d6d/classPTL_1_1Task_3_01RetT_00_01void_01_4.html", "df/d6d/classPTL_1_1Task_3_01RetT_00_01void_01_4" ],
    [ "PTL::Task< void, void >", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4" ]
];