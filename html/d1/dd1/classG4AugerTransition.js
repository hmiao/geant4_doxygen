var classG4AugerTransition =
[
    [ "G4AugerTransition", "d1/dd1/classG4AugerTransition.html#a3943f19cb829ae21097cc8e0c7969ad1", null ],
    [ "~G4AugerTransition", "d1/dd1/classG4AugerTransition.html#a90c867666bf4c66c0bcba03e92ed7c75", null ],
    [ "AugerOriginatingShellId", "d1/dd1/classG4AugerTransition.html#a00d18cb6dfcbe8f849e9f7ee947ffaa4", null ],
    [ "AugerOriginatingShellIds", "d1/dd1/classG4AugerTransition.html#a1d6cb3b837a7760ffd71c82d55020b2e", null ],
    [ "AugerTransitionEnergies", "d1/dd1/classG4AugerTransition.html#a23ffcbb048b3809b13e2ecb6700d6a9a", null ],
    [ "AugerTransitionEnergy", "d1/dd1/classG4AugerTransition.html#a9b6695fe1a7bee9989bb301206ac39a7", null ],
    [ "AugerTransitionProbabilities", "d1/dd1/classG4AugerTransition.html#a7a90d81adb264ea5323e8bf54aa8baab", null ],
    [ "AugerTransitionProbability", "d1/dd1/classG4AugerTransition.html#ac3fcd0cec254f79cc80306138072ae28", null ],
    [ "FinalShellId", "d1/dd1/classG4AugerTransition.html#a14907e9bfb02b7431453786f552fab43", null ],
    [ "TransitionOriginatingShellId", "d1/dd1/classG4AugerTransition.html#ad9196c98c2a4997abb65343309f8a03c", null ],
    [ "TransitionOriginatingShellIds", "d1/dd1/classG4AugerTransition.html#abbb6fb9ca0be2c4db1bcb6f02d9e8b16", null ],
    [ "augerOriginatingShellIdsMap", "d1/dd1/classG4AugerTransition.html#ac95f20d56e3c8c2fb7e2e22810397b39", null ],
    [ "augerTransitionEnergiesMap", "d1/dd1/classG4AugerTransition.html#a9b41c5363c420ed2eb8ea14ef32951d4", null ],
    [ "augerTransitionProbabilitiesMap", "d1/dd1/classG4AugerTransition.html#a2bf4e6d5aa7633766c6bd7c53da1e09a", null ],
    [ "finalShellId", "d1/dd1/classG4AugerTransition.html#abe9a18743cd62ba8ff3d9ac5b77c31b8", null ],
    [ "transitionOriginatingShellIds", "d1/dd1/classG4AugerTransition.html#aef809660fbe36044a1b334655ffea700", null ]
];