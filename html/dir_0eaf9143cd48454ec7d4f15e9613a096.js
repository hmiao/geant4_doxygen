var dir_0eaf9143cd48454ec7d4f15e9613a096 =
[
    [ "G4FastSimulationPhysics.hh", "dc/db6/G4FastSimulationPhysics_8hh.html", "dc/db6/G4FastSimulationPhysics_8hh" ],
    [ "G4GenericBiasingPhysics.hh", "d2/dda/G4GenericBiasingPhysics_8hh.html", "d2/dda/G4GenericBiasingPhysics_8hh" ],
    [ "G4ImportanceBiasing.hh", "d1/dd9/G4ImportanceBiasing_8hh.html", "d1/dd9/G4ImportanceBiasing_8hh" ],
    [ "G4MaxTimeCuts.hh", "d0/da3/G4MaxTimeCuts_8hh.html", "d0/da3/G4MaxTimeCuts_8hh" ],
    [ "G4MinEkineCuts.hh", "dd/d97/G4MinEkineCuts_8hh.html", "dd/d97/G4MinEkineCuts_8hh" ],
    [ "G4NeutronTrackingCut.hh", "dc/d2f/G4NeutronTrackingCut_8hh.html", "dc/d2f/G4NeutronTrackingCut_8hh" ],
    [ "G4ParallelWorldPhysics.hh", "d1/d36/G4ParallelWorldPhysics_8hh.html", "d1/d36/G4ParallelWorldPhysics_8hh" ],
    [ "G4SpecialCuts.hh", "d8/d1c/G4SpecialCuts_8hh.html", "d8/d1c/G4SpecialCuts_8hh" ],
    [ "G4StepLimiterPhysics.hh", "d0/d79/G4StepLimiterPhysics_8hh.html", "d0/d79/G4StepLimiterPhysics_8hh" ],
    [ "G4WeightWindowBiasing.hh", "de/d2e/G4WeightWindowBiasing_8hh.html", "de/d2e/G4WeightWindowBiasing_8hh" ]
];