var G4OpticalMaterialProperties_8hh =
[
    [ "ConvertToEnergy", "d8/dda/G4OpticalMaterialProperties_8hh.html#a9e88d2992618ce57dec6f2d7fac5bb9a", null ],
    [ "GetProperty", "d8/dda/G4OpticalMaterialProperties_8hh.html#a7bec84d187ddad831cf8fa5f8f37287d", null ],
    [ "GetRefractiveIndex", "d8/dda/G4OpticalMaterialProperties_8hh.html#a218e4ab75cf5a99cbdac9f1e6065043b", null ]
];