var classSoBox =
[
    [ "SoBox", "d8/db2/classSoBox.html#a19f939394c17e8022d52de5ba3a45ffc", null ],
    [ "~SoBox", "d8/db2/classSoBox.html#a6739f23f9e82fb9868de738be9717453", null ],
    [ "clearAlternateRep", "d8/db2/classSoBox.html#a831380395cb0ffcc5ee82d1a08e7d6bf", null ],
    [ "computeBBox", "d8/db2/classSoBox.html#ad5de4b4f360588dc41675fa4214d3918", null ],
    [ "generateAlternateRep", "d8/db2/classSoBox.html#a10c1a6923491976576623e4b9a2eaf22", null ],
    [ "generateChildren", "d8/db2/classSoBox.html#a72a579a26708915905c114a2e1dd9413", null ],
    [ "generatePrimitives", "d8/db2/classSoBox.html#aa7dbacaa0f772975e78771a9cff14739", null ],
    [ "getChildren", "d8/db2/classSoBox.html#aa49a4fd15eaff704ffbefa784880b5d7", null ],
    [ "initClass", "d8/db2/classSoBox.html#a9be3ad17bf07c9ff147726d18a741c68", null ],
    [ "SO_NODE_HEADER", "d8/db2/classSoBox.html#a7840abb67130ccc0a20e447178a9a033", null ],
    [ "updateChildren", "d8/db2/classSoBox.html#aef66a4475814438b6959ce2824bce140", null ],
    [ "alternateRep", "d8/db2/classSoBox.html#a6974db8c237efa22d8292cc24bf5fde4", null ],
    [ "children", "d8/db2/classSoBox.html#a2150798959535b7a1f104f53ab3f89a8", null ],
    [ "fDx", "d8/db2/classSoBox.html#a3ff16306363ca590dd8c37d02aba0bb8", null ],
    [ "fDy", "d8/db2/classSoBox.html#a70a89c9c02efa53c31cb06fe38aab76d", null ],
    [ "fDz", "d8/db2/classSoBox.html#a9e6bcb6e8243add31910d9321c8de432", null ]
];