var classG4ee2KChargedModel =
[
    [ "G4ee2KChargedModel", "d8/d9a/classG4ee2KChargedModel.html#a6ec1805b1dc7876658dee276f98d9955", null ],
    [ "~G4ee2KChargedModel", "d8/d9a/classG4ee2KChargedModel.html#ae2c9abc7a034372c978d1abedf655bb1", null ],
    [ "G4ee2KChargedModel", "d8/d9a/classG4ee2KChargedModel.html#a8106e0ba52c2f5fb321a1b49f97f8961", null ],
    [ "ComputeCrossSection", "d8/d9a/classG4ee2KChargedModel.html#a9d1c859e10eeadb5a58578540a10f9d0", null ],
    [ "operator=", "d8/d9a/classG4ee2KChargedModel.html#a068085e3580e2f233587b53cb569de6a", null ],
    [ "PeakEnergy", "d8/d9a/classG4ee2KChargedModel.html#ad7f736df593c60599ebf8610bc93e244", null ],
    [ "SampleSecondaries", "d8/d9a/classG4ee2KChargedModel.html#a7af2d247eedcc05ad1773921c024d655", null ],
    [ "massK", "d8/d9a/classG4ee2KChargedModel.html#a8cc0065f7e81c33f1703fb1fb44cde98", null ],
    [ "massPhi", "d8/d9a/classG4ee2KChargedModel.html#ad4769dc55b5bd8bb9f2dfb5389ab2393", null ]
];