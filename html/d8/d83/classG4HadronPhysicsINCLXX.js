var classG4HadronPhysicsINCLXX =
[
    [ "G4HadronPhysicsINCLXX", "d8/d83/classG4HadronPhysicsINCLXX.html#a61bab2760fe7c58606a04da2c5355879", null ],
    [ "G4HadronPhysicsINCLXX", "d8/d83/classG4HadronPhysicsINCLXX.html#a6a71e52ceff93619454bf2715bd70e45", null ],
    [ "~G4HadronPhysicsINCLXX", "d8/d83/classG4HadronPhysicsINCLXX.html#a818beeefc784171c7c0ecfbb8661fb05", null ],
    [ "G4HadronPhysicsINCLXX", "d8/d83/classG4HadronPhysicsINCLXX.html#a91940a82af1640b9fc936f5ffe58d84d", null ],
    [ "ConstructProcess", "d8/d83/classG4HadronPhysicsINCLXX.html#aed67e9047a4f8ff4212ee87bd63bab06", null ],
    [ "Kaon", "d8/d83/classG4HadronPhysicsINCLXX.html#afefabaf20963e9559f0f6437c72909ed", null ],
    [ "Neutron", "d8/d83/classG4HadronPhysicsINCLXX.html#a19e85650a1529a61034075f5245fb985", null ],
    [ "operator=", "d8/d83/classG4HadronPhysicsINCLXX.html#a1e83878b0251cd2ae4e095268711ab1f", null ],
    [ "Others", "d8/d83/classG4HadronPhysicsINCLXX.html#ae16f8cde15c8225b2dff6e951cb48df0", null ],
    [ "Pion", "d8/d83/classG4HadronPhysicsINCLXX.html#a334f39d9c6e70733f5f87a9db55f7cb4", null ],
    [ "Proton", "d8/d83/classG4HadronPhysicsINCLXX.html#a40e0f5814d3f338edc371823a573ea3f", null ],
    [ "SetQuasiElastic", "d8/d83/classG4HadronPhysicsINCLXX.html#a81670966d0f3cedb4971d8a0d046197f", null ],
    [ "withFTFP", "d8/d83/classG4HadronPhysicsINCLXX.html#a5293cd39657b1d08bdfbc323caf4ed40", null ],
    [ "withNeutronHP", "d8/d83/classG4HadronPhysicsINCLXX.html#a67ec619236a0e15bffbddca2a2a1d1ca", null ]
];