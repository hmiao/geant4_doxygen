var classG4tgbMaterial =
[
    [ "G4tgbMaterial", "d8/dc1/classG4tgbMaterial.html#a8e34d82170b58ef2c9475486fbfdb574", null ],
    [ "~G4tgbMaterial", "d8/dc1/classG4tgbMaterial.html#aeec00b4811ba9bad788aaaef3ea9e933", null ],
    [ "G4tgbMaterial", "d8/dc1/classG4tgbMaterial.html#af8463912548e3f958aeebaebc7abd2a5", null ],
    [ "BuildG4Material", "d8/dc1/classG4tgbMaterial.html#aff937e6c29f36daeb73bdd091d9e4038", null ],
    [ "GetA", "d8/dc1/classG4tgbMaterial.html#a302ec6c9d9df21f563f5d7112c23f20a", null ],
    [ "GetDensity", "d8/dc1/classG4tgbMaterial.html#abadf2a610580b1fd3f7ee4f650936785", null ],
    [ "GetName", "d8/dc1/classG4tgbMaterial.html#a33a9eabc0e2d4e675426f9213a430110", null ],
    [ "GetNumberOfMaterials", "d8/dc1/classG4tgbMaterial.html#a3ad1fa5df36aee77098a3c97a49ebf9a", null ],
    [ "GetTgrMate", "d8/dc1/classG4tgbMaterial.html#aec74996a639a7ec23a7d6f0e36c22724", null ],
    [ "GetType", "d8/dc1/classG4tgbMaterial.html#ac928b2f983bfe938a46259b059ce1a4a", null ],
    [ "GetZ", "d8/dc1/classG4tgbMaterial.html#a16e2c1a0ecf25b7e39edc7822bdef755", null ],
    [ "operator<<", "d8/dc1/classG4tgbMaterial.html#a7694ebf25e13cdb9ffed42c57f75eff4", null ],
    [ "theG4Mate", "d8/dc1/classG4tgbMaterial.html#a6ead6373ccdce194426a32431e64f291", null ],
    [ "theTgrMate", "d8/dc1/classG4tgbMaterial.html#a5512e4cc2f2a8c7af2372bdee3d7f7ba", null ]
];