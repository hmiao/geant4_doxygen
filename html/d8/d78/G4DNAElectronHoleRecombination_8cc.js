var G4DNAElectronHoleRecombination_8cc =
[
    [ "A", "d8/d78/G4DNAElectronHoleRecombination_8cc.html#a15ad9c54954e42adb2d25a5221754324", null ],
    [ "B", "d8/d78/G4DNAElectronHoleRecombination_8cc.html#a609055c5a74cf4c9c4815d4580ebdf2c", null ],
    [ "C", "d8/d78/G4DNAElectronHoleRecombination_8cc.html#a291225c9deee5fdfdb56e096d5c6944d", null ],
    [ "D", "d8/d78/G4DNAElectronHoleRecombination_8cc.html#a6378c9d7fd974c7e13f9f79ea527825d", null ],
    [ "epsilon", "d8/d78/G4DNAElectronHoleRecombination_8cc.html#a03ee032ffb62b17dd8e9eab3f45c604e", null ],
    [ "S", "d8/d78/G4DNAElectronHoleRecombination_8cc.html#a5645854e99ede06d772cf0c0e600508a", null ],
    [ "Y", "d8/d78/G4DNAElectronHoleRecombination_8cc.html#ae97a5115ce4d1f5bb36ebf8e40cac764", null ],
    [ "onsager_constant", "d8/d78/G4DNAElectronHoleRecombination_8cc.html#af1f6be2870cf101f32fecb577ae20daf", null ]
];