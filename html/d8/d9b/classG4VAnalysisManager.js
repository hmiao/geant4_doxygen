var classG4VAnalysisManager =
[
    [ "G4VAnalysisManager", "d8/d9b/classG4VAnalysisManager.html#a8c45f9b03039815d7097384ebd88c74b", null ],
    [ "G4VAnalysisManager", "d8/d9b/classG4VAnalysisManager.html#a1529d38ff14b9d96dabb5b47e634aa59", null ],
    [ "~G4VAnalysisManager", "d8/d9b/classG4VAnalysisManager.html#a1bae209bf991bbf183a9bdca5446a0c3", null ],
    [ "AddNtupleRow", "d8/d9b/classG4VAnalysisManager.html#a37f6d468137190916538e7b4e1b2b8e9", null ],
    [ "AddNtupleRow", "d8/d9b/classG4VAnalysisManager.html#abadf7dce897b27de99a9c2bd2474273e", null ],
    [ "Clear", "d8/d9b/classG4VAnalysisManager.html#ab1c637806b2926c7f39a5d79ea987cf7", null ],
    [ "ClearImpl", "d8/d9b/classG4VAnalysisManager.html#a7599c160f6d01f5089a35befb32a41e8", null ],
    [ "CloseFile", "d8/d9b/classG4VAnalysisManager.html#a70993e4dd4893c24e78f823f076cf70e", null ],
    [ "CloseFileImpl", "d8/d9b/classG4VAnalysisManager.html#aed34b7f25ff2c7166e5c0c354c07c943", null ],
    [ "CreateH1", "d8/d9b/classG4VAnalysisManager.html#a330782020e9aff731b1b0d44860ac3aa", null ],
    [ "CreateH1", "d8/d9b/classG4VAnalysisManager.html#a2e9965df4de74bca253a3126ce609bc7", null ],
    [ "CreateH2", "d8/d9b/classG4VAnalysisManager.html#ade82d2ffce27fbc8a9a3e7f046f81c1e", null ],
    [ "CreateH2", "d8/d9b/classG4VAnalysisManager.html#ad81adde3a12ef1c8fe6d20fc45ab07db", null ],
    [ "CreateH3", "d8/d9b/classG4VAnalysisManager.html#afe35bfbf519c2bb4e11cd9738500526b", null ],
    [ "CreateH3", "d8/d9b/classG4VAnalysisManager.html#a8f2458a9d69a7e5449d55b24f474a620", null ],
    [ "CreateNtuple", "d8/d9b/classG4VAnalysisManager.html#abfe80514201e8613af8fd7d6dd6a9013", null ],
    [ "CreateNtupleDColumn", "d8/d9b/classG4VAnalysisManager.html#a3a3f226033f099ebd2c633dd5230d37d", null ],
    [ "CreateNtupleDColumn", "d8/d9b/classG4VAnalysisManager.html#a3c724ebbf741f65699971e75cb73ce51", null ],
    [ "CreateNtupleDColumn", "d8/d9b/classG4VAnalysisManager.html#a8f880b67deace2ef63b093cd4f28b6fa", null ],
    [ "CreateNtupleDColumn", "d8/d9b/classG4VAnalysisManager.html#ac2ea0a8919906d73276f7939cccb9658", null ],
    [ "CreateNtupleFColumn", "d8/d9b/classG4VAnalysisManager.html#a43b501afc55948492ecc5e283c30e112", null ],
    [ "CreateNtupleFColumn", "d8/d9b/classG4VAnalysisManager.html#a882a2d8a28e753e1f37df5a58a21a11c", null ],
    [ "CreateNtupleFColumn", "d8/d9b/classG4VAnalysisManager.html#a3dc6673486e6322f1bb178e77cf0cb0a", null ],
    [ "CreateNtupleFColumn", "d8/d9b/classG4VAnalysisManager.html#a9713b96422460bfb2189679a88525c4d", null ],
    [ "CreateNtupleIColumn", "d8/d9b/classG4VAnalysisManager.html#a36b9a9647127e3025ab2acaced46254f", null ],
    [ "CreateNtupleIColumn", "d8/d9b/classG4VAnalysisManager.html#a383dabc675de659909480e63509db9c9", null ],
    [ "CreateNtupleIColumn", "d8/d9b/classG4VAnalysisManager.html#a91e8faab1d3e66cec193550e5793026f", null ],
    [ "CreateNtupleIColumn", "d8/d9b/classG4VAnalysisManager.html#a476351069048d550041ab448e81f0aa4", null ],
    [ "CreateNtupleSColumn", "d8/d9b/classG4VAnalysisManager.html#a8a1d7ad86dcb9fec6cb511d6294d5aeb", null ],
    [ "CreateNtupleSColumn", "d8/d9b/classG4VAnalysisManager.html#a6c5065fee63fb2bd98efff347050660c", null ],
    [ "CreateNtupleSColumn", "d8/d9b/classG4VAnalysisManager.html#a02c91e72822ba8283ad8ed281bb018af", null ],
    [ "CreateNtupleSColumn", "d8/d9b/classG4VAnalysisManager.html#a5510849de89c453e26755bc8fdb2e125", null ],
    [ "CreateP1", "d8/d9b/classG4VAnalysisManager.html#a68616fddf4edb0dcaaeaa39e8a49c3f5", null ],
    [ "CreateP1", "d8/d9b/classG4VAnalysisManager.html#a0191f258aadb40e170248ff35ff2906b", null ],
    [ "CreateP2", "d8/d9b/classG4VAnalysisManager.html#a33876c2a3fb32b61da41109b64f932c1", null ],
    [ "CreateP2", "d8/d9b/classG4VAnalysisManager.html#a0ea43530f49c8b654d1c484a277b89ab", null ],
    [ "FillH1", "d8/d9b/classG4VAnalysisManager.html#a978368567c1dca5d45598cca6e580bff", null ],
    [ "FillH2", "d8/d9b/classG4VAnalysisManager.html#a3e6f846c8c89c4f77b782c8a37e33ea1", null ],
    [ "FillH3", "d8/d9b/classG4VAnalysisManager.html#a44fd2a7fa7b840a9eaa5deb2e0a80ea9", null ],
    [ "FillNtupleDColumn", "d8/d9b/classG4VAnalysisManager.html#a4fea5eb9e0e252794fd2d872e79b1012", null ],
    [ "FillNtupleDColumn", "d8/d9b/classG4VAnalysisManager.html#a74e4e04106317b832aec62e45213bbc7", null ],
    [ "FillNtupleFColumn", "d8/d9b/classG4VAnalysisManager.html#a8fd09fd744abd7026625109d20523d14", null ],
    [ "FillNtupleFColumn", "d8/d9b/classG4VAnalysisManager.html#af141b5ae6f122f2ba5710b761c4cb8b9", null ],
    [ "FillNtupleIColumn", "d8/d9b/classG4VAnalysisManager.html#a5d12fef2d180855e9115cc70bf95b2a2", null ],
    [ "FillNtupleIColumn", "d8/d9b/classG4VAnalysisManager.html#a067a32a4abf97f4bd1a439731b369c42", null ],
    [ "FillNtupleSColumn", "d8/d9b/classG4VAnalysisManager.html#a16a6661f84b3560af2ec7a40ce3097f9", null ],
    [ "FillNtupleSColumn", "d8/d9b/classG4VAnalysisManager.html#a5ca0b1005bea371e4d38e3a339637bfd", null ],
    [ "FillP1", "d8/d9b/classG4VAnalysisManager.html#a32e4a7b242247f7617c12bb6fbfa006b", null ],
    [ "FillP2", "d8/d9b/classG4VAnalysisManager.html#aab92f6871b8193ab9ce0b456b7a72684", null ],
    [ "FinishNtuple", "d8/d9b/classG4VAnalysisManager.html#ab59b94509f09e99b12e510c0e3f1ff9c", null ],
    [ "FinishNtuple", "d8/d9b/classG4VAnalysisManager.html#a904a9c7738938cb211c98c307c5d1c56", null ],
    [ "GetActivation", "d8/d9b/classG4VAnalysisManager.html#a0c7e782207ce42f2d774af68cfbee961", null ],
    [ "GetCompressionLevel", "d8/d9b/classG4VAnalysisManager.html#ad9bcf8ba587b1749db67eb2a9d65d260", null ],
    [ "GetFileManager", "d8/d9b/classG4VAnalysisManager.html#a1ba160d5cb2fdc1454eb2a5a42d8d4da", null ],
    [ "GetFileName", "d8/d9b/classG4VAnalysisManager.html#a6bcff45f0d949bc23eddd7c1f2758b54", null ],
    [ "GetFileType", "d8/d9b/classG4VAnalysisManager.html#acd37698427e3c447cb32cc97059d186d", null ],
    [ "GetFirstH1Id", "d8/d9b/classG4VAnalysisManager.html#a4990cd9f9ed465c3f97bb5f59847f65e", null ],
    [ "GetFirstH2Id", "d8/d9b/classG4VAnalysisManager.html#a12bc94fb15fa5201785947482a366557", null ],
    [ "GetFirstH3Id", "d8/d9b/classG4VAnalysisManager.html#aea2bff738a777f2fb66847ebd43174a5", null ],
    [ "GetFirstNtupleColumnId", "d8/d9b/classG4VAnalysisManager.html#ab4f7873edebff486320a26072dd1151a", null ],
    [ "GetFirstNtupleId", "d8/d9b/classG4VAnalysisManager.html#a0b4ec0acbcec7a705022a1b437285822", null ],
    [ "GetFirstP1Id", "d8/d9b/classG4VAnalysisManager.html#a2b6776ddcb0245ea5fba0af9f3b8895d", null ],
    [ "GetFirstP2Id", "d8/d9b/classG4VAnalysisManager.html#a0581a0d285a2c906c3b448ebf9f598db", null ],
    [ "GetH1Activation", "d8/d9b/classG4VAnalysisManager.html#a7410a0c5e90c1c34603ecbf94bec227a", null ],
    [ "GetH1Ascii", "d8/d9b/classG4VAnalysisManager.html#afdfaf6d36047191cd7089a37572bf61b", null ],
    [ "GetH1FileName", "d8/d9b/classG4VAnalysisManager.html#a2cd5c389dc97af69a782f5f9561faa27", null ],
    [ "GetH1Id", "d8/d9b/classG4VAnalysisManager.html#a5841ae3f8b81c63480bb2135ba08160c", null ],
    [ "GetH1Name", "d8/d9b/classG4VAnalysisManager.html#a296bc444780ae9572d0b06590e399205", null ],
    [ "GetH1Nbins", "d8/d9b/classG4VAnalysisManager.html#a78624387b5286b235bdc518a9fb41afd", null ],
    [ "GetH1Plotting", "d8/d9b/classG4VAnalysisManager.html#a4cef7af6b01a6d288b67b6b5156df830", null ],
    [ "GetH1Title", "d8/d9b/classG4VAnalysisManager.html#a73157e02e16cfb01ff53d7f9c16cdc55", null ],
    [ "GetH1Unit", "d8/d9b/classG4VAnalysisManager.html#aae04be6a2c56262ca608bfef3d1662b3", null ],
    [ "GetH1Width", "d8/d9b/classG4VAnalysisManager.html#a937fa33e81839fccbc52588fbe78a6f6", null ],
    [ "GetH1XAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a8df2047761f0a4402a4af4b9dc1b2e32", null ],
    [ "GetH1XAxisTitle", "d8/d9b/classG4VAnalysisManager.html#ac885239f564709156f706a0bd34b6c28", null ],
    [ "GetH1Xmax", "d8/d9b/classG4VAnalysisManager.html#ae376bce98d20c3d8e9cbd6540e69101a", null ],
    [ "GetH1Xmin", "d8/d9b/classG4VAnalysisManager.html#ab99c040dfcbdb225b03903e7915ec0ee", null ],
    [ "GetH1YAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a19943caf05bf57a3ddcbb07c164bde1b", null ],
    [ "GetH1YAxisTitle", "d8/d9b/classG4VAnalysisManager.html#a2750517bab7a35eae2e1ab7bf1eb8598", null ],
    [ "GetH2Activation", "d8/d9b/classG4VAnalysisManager.html#a78c120a939d40449755324cd38ef1cc0", null ],
    [ "GetH2Ascii", "d8/d9b/classG4VAnalysisManager.html#a98bd6b868e7ca9c06e288d0405bb6ed5", null ],
    [ "GetH2Id", "d8/d9b/classG4VAnalysisManager.html#a2ba5794b32752bec4ddc92cb9030bd6e", null ],
    [ "GetH2Name", "d8/d9b/classG4VAnalysisManager.html#a350c07e34db3d406f6fe8d0da9abb0d4", null ],
    [ "GetH2Nxbins", "d8/d9b/classG4VAnalysisManager.html#aeeb5f6fbc32c88abd53d15602c9ccd84", null ],
    [ "GetH2Nybins", "d8/d9b/classG4VAnalysisManager.html#a2eb5a7f449d7fb786e4af7308d0481a8", null ],
    [ "GetH2Plotting", "d8/d9b/classG4VAnalysisManager.html#a6e93a7532d2ca625861cbcd3a4eca025", null ],
    [ "GetH2Title", "d8/d9b/classG4VAnalysisManager.html#ac89a7a29c5aae76e8ca90f91ff775ece", null ],
    [ "GetH2XAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a9b7e067688e6aacf8945499729a6ea32", null ],
    [ "GetH2XAxisTitle", "d8/d9b/classG4VAnalysisManager.html#a4c08964a49343fc89a8515100e1305db", null ],
    [ "GetH2Xmax", "d8/d9b/classG4VAnalysisManager.html#ac660ca791d762404940acb82c5bb82c7", null ],
    [ "GetH2Xmin", "d8/d9b/classG4VAnalysisManager.html#a470df8d20f7120c367a869969fdc8a02", null ],
    [ "GetH2XUnit", "d8/d9b/classG4VAnalysisManager.html#a2f249d1a0908a2d0808e2198d72dc0c7", null ],
    [ "GetH2XWidth", "d8/d9b/classG4VAnalysisManager.html#aee69d2702aa236b486385a2f211f0194", null ],
    [ "GetH2YAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a39daaa47a8b7aa55b6267c09ebea3347", null ],
    [ "GetH2YAxisTitle", "d8/d9b/classG4VAnalysisManager.html#ab764471331ad0d4a29e5c3c83cf59df2", null ],
    [ "GetH2Ymax", "d8/d9b/classG4VAnalysisManager.html#ae68f7b126c7eb5703a0799ede0137542", null ],
    [ "GetH2Ymin", "d8/d9b/classG4VAnalysisManager.html#aaf269be9cf86083a2a24d50b5db90baf", null ],
    [ "GetH2YUnit", "d8/d9b/classG4VAnalysisManager.html#a58cbc8831cb0d43f7f32863a48931f8c", null ],
    [ "GetH2YWidth", "d8/d9b/classG4VAnalysisManager.html#a54e4b2e262463727566179357e1ef893", null ],
    [ "GetH2ZAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a2e70a31452c179a23facff03a83a8cd7", null ],
    [ "GetH2ZAxisTitle", "d8/d9b/classG4VAnalysisManager.html#acce3b0ced22a0ed0fb9803a297eba4fb", null ],
    [ "GetH3Activation", "d8/d9b/classG4VAnalysisManager.html#ac0785814815ff971e416fac47b34831a", null ],
    [ "GetH3Ascii", "d8/d9b/classG4VAnalysisManager.html#a7fa27b7a515f480228ea3ea35f3019e1", null ],
    [ "GetH3Id", "d8/d9b/classG4VAnalysisManager.html#ab03976bd74dd3ac5abcc4b27e3d467a1", null ],
    [ "GetH3Name", "d8/d9b/classG4VAnalysisManager.html#aa527f3f431e885d8a39b59a712bb1c18", null ],
    [ "GetH3Nxbins", "d8/d9b/classG4VAnalysisManager.html#a06dfb17a96a86dd396e7e0b5e024b4d2", null ],
    [ "GetH3Nybins", "d8/d9b/classG4VAnalysisManager.html#afb7241204b3a62dbb9621233b011ccb1", null ],
    [ "GetH3Nzbins", "d8/d9b/classG4VAnalysisManager.html#a4eda15482930960fa09694a7b3ff5203", null ],
    [ "GetH3Plotting", "d8/d9b/classG4VAnalysisManager.html#ae115a62e81b332a14a5044877b6dfec1", null ],
    [ "GetH3Title", "d8/d9b/classG4VAnalysisManager.html#aa49952a2f2871c2992164938e6ac1f98", null ],
    [ "GetH3XAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a76bd8b5ce83176a16ce40f950950565e", null ],
    [ "GetH3XAxisTitle", "d8/d9b/classG4VAnalysisManager.html#abd83dd886f50668aeb80164f78702e6d", null ],
    [ "GetH3Xmax", "d8/d9b/classG4VAnalysisManager.html#aa3db18edfd3fbc318443179a5362ff1f", null ],
    [ "GetH3Xmin", "d8/d9b/classG4VAnalysisManager.html#a00f7465ed868539066b4b430987e98c2", null ],
    [ "GetH3XUnit", "d8/d9b/classG4VAnalysisManager.html#abd9bf1346baafc5105b4427200084fbd", null ],
    [ "GetH3XWidth", "d8/d9b/classG4VAnalysisManager.html#ad7e6891614fea498cc1674b15e979f4e", null ],
    [ "GetH3YAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a6eff96aac1aad10707d971cafcfcb6e4", null ],
    [ "GetH3YAxisTitle", "d8/d9b/classG4VAnalysisManager.html#a0d2dcece5e2b6d20ebc58beff3ee3603", null ],
    [ "GetH3Ymax", "d8/d9b/classG4VAnalysisManager.html#ac5cc72372803b701d436743354a2326f", null ],
    [ "GetH3Ymin", "d8/d9b/classG4VAnalysisManager.html#aaf900bc75900bbd183598266f8511f99", null ],
    [ "GetH3YUnit", "d8/d9b/classG4VAnalysisManager.html#aabb88643b9ac899945a4823058948bc5", null ],
    [ "GetH3YWidth", "d8/d9b/classG4VAnalysisManager.html#a1c76d92d5ba2ef5608a5d12fd2ed78ec", null ],
    [ "GetH3ZAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a3bbf8ddcab43ab4bc863cfdd0a5688a4", null ],
    [ "GetH3ZAxisTitle", "d8/d9b/classG4VAnalysisManager.html#a9ebd76c986e8f856f29b85d6e57888b4", null ],
    [ "GetH3Zmax", "d8/d9b/classG4VAnalysisManager.html#a8f0de4cc3ac2c1e461a9fbda906f3c68", null ],
    [ "GetH3Zmin", "d8/d9b/classG4VAnalysisManager.html#a4bc439845d8fced75b81e9ab2d2d32b8", null ],
    [ "GetH3ZUnit", "d8/d9b/classG4VAnalysisManager.html#a8f1a02541d21616d421f75679148a8c7", null ],
    [ "GetH3ZWidth", "d8/d9b/classG4VAnalysisManager.html#aeddba2ed39cadf58e72c140cf1bb5966", null ],
    [ "GetHistoDirectoryName", "d8/d9b/classG4VAnalysisManager.html#a376262dcfe0647d627066b04c8d73c09", null ],
    [ "GetNofH1s", "d8/d9b/classG4VAnalysisManager.html#a04b5080ae62690a60c4dc9be148c1d76", null ],
    [ "GetNofH2s", "d8/d9b/classG4VAnalysisManager.html#afd638ed8b33b3d2043928e596b7b16f0", null ],
    [ "GetNofH3s", "d8/d9b/classG4VAnalysisManager.html#a0597e3376fc23348f6cbef3284586f1e", null ],
    [ "GetNofNtuples", "d8/d9b/classG4VAnalysisManager.html#a2886ac9ab1e54da1d81e507da5bf675d", null ],
    [ "GetNofP1s", "d8/d9b/classG4VAnalysisManager.html#a138741c5a34fb4e187edd6a1eb76d4a4", null ],
    [ "GetNofP2s", "d8/d9b/classG4VAnalysisManager.html#aeddccebc3c479eb58ab1263af81a0328", null ],
    [ "GetNtupleActivation", "d8/d9b/classG4VAnalysisManager.html#a2547802e32190f6cd1feab8c05bce7e9", null ],
    [ "GetNtupleDirectoryName", "d8/d9b/classG4VAnalysisManager.html#adc9695329c9a6d030976c80ccc4ba1eb", null ],
    [ "GetNtupleFileName", "d8/d9b/classG4VAnalysisManager.html#a50ba8ce810efbdca8c93875303f7dd90", null ],
    [ "GetP1Activation", "d8/d9b/classG4VAnalysisManager.html#a4e14c65dda722439a666f25dffa4eae1", null ],
    [ "GetP1Ascii", "d8/d9b/classG4VAnalysisManager.html#a4d937acd3092fdd031a7d1d4cebab0aa", null ],
    [ "GetP1Id", "d8/d9b/classG4VAnalysisManager.html#a4b6402c30e402ca4003fce5ab17c19da", null ],
    [ "GetP1Name", "d8/d9b/classG4VAnalysisManager.html#a9d705ec5a0a1bad59c170422d612f0a9", null ],
    [ "GetP1Nbins", "d8/d9b/classG4VAnalysisManager.html#a0236c533b4d84f79d90c7e6a29a03a0b", null ],
    [ "GetP1Plotting", "d8/d9b/classG4VAnalysisManager.html#a839c7ef61df3c37dc53e99cc5d207a2f", null ],
    [ "GetP1Title", "d8/d9b/classG4VAnalysisManager.html#ac0edc8d0d6e4f40e1cd6c0f5d9c5d09e", null ],
    [ "GetP1XAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a0b57bdab1007752717c17796082f5a8d", null ],
    [ "GetP1XAxisTitle", "d8/d9b/classG4VAnalysisManager.html#a10501986b3766555e8446bb0aa1bd295", null ],
    [ "GetP1Xmax", "d8/d9b/classG4VAnalysisManager.html#a069e1e1b9b9486df68ddf3163b4b51c9", null ],
    [ "GetP1Xmin", "d8/d9b/classG4VAnalysisManager.html#a2fec8857db38de596739ec4e402287c9", null ],
    [ "GetP1XUnit", "d8/d9b/classG4VAnalysisManager.html#a2ee1ac49f79f3f36db9f612857c4f739", null ],
    [ "GetP1XWidth", "d8/d9b/classG4VAnalysisManager.html#a1e5f94a454b5499ea2ca717a15b92d08", null ],
    [ "GetP1YAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a514228ae7a1267dc72b15e7ba991f847", null ],
    [ "GetP1YAxisTitle", "d8/d9b/classG4VAnalysisManager.html#a7e22cc7dc88c5d3d9a8009a2b9ab7e5b", null ],
    [ "GetP1Ymax", "d8/d9b/classG4VAnalysisManager.html#a0bf3e898a13aecc9e12fb6c081edfd35", null ],
    [ "GetP1Ymin", "d8/d9b/classG4VAnalysisManager.html#a7599c2a01a86b7c5fa07c5d0147c800b", null ],
    [ "GetP1YUnit", "d8/d9b/classG4VAnalysisManager.html#af37273c88f119a5902883e7ed6b249ec", null ],
    [ "GetP2Activation", "d8/d9b/classG4VAnalysisManager.html#acbe024b7373662fbb21bc06d8ab10ba6", null ],
    [ "GetP2Ascii", "d8/d9b/classG4VAnalysisManager.html#aaeec219bef041946cbf74bcefff07f87", null ],
    [ "GetP2Id", "d8/d9b/classG4VAnalysisManager.html#a29e7d7a09c63b5c1fb89ae4a54662deb", null ],
    [ "GetP2Name", "d8/d9b/classG4VAnalysisManager.html#a42ef025dd022531e06ddf24383a5a748", null ],
    [ "GetP2Nxbins", "d8/d9b/classG4VAnalysisManager.html#ac77d5a183d0c6e0dcdf92d71877aefd4", null ],
    [ "GetP2Nybins", "d8/d9b/classG4VAnalysisManager.html#ad5aab6f35bcc013b7e5fc6e559baa134", null ],
    [ "GetP2Plotting", "d8/d9b/classG4VAnalysisManager.html#a1b3621ec118243db3cd435db01338c18", null ],
    [ "GetP2Title", "d8/d9b/classG4VAnalysisManager.html#adbd8b72e5b19e476907d8570545b4cfa", null ],
    [ "GetP2XAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#aa76d1717d1df6659a4f5dda94eac878f", null ],
    [ "GetP2XAxisTitle", "d8/d9b/classG4VAnalysisManager.html#ac1bbcf9bcd61f75eea1795cbbd68915e", null ],
    [ "GetP2Xmax", "d8/d9b/classG4VAnalysisManager.html#aed1f45f75509fcb7c61a4f90fb59c0db", null ],
    [ "GetP2Xmin", "d8/d9b/classG4VAnalysisManager.html#a54b407e51463661d949072d6a19195a7", null ],
    [ "GetP2XUnit", "d8/d9b/classG4VAnalysisManager.html#ab5c1b4dafc1a52ba87a93cdda850ab22", null ],
    [ "GetP2XWidth", "d8/d9b/classG4VAnalysisManager.html#a50308f027baba589c6b30f08ea7fd19f", null ],
    [ "GetP2YAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a94568b181eb1b590e3e9b983a146e496", null ],
    [ "GetP2YAxisTitle", "d8/d9b/classG4VAnalysisManager.html#a94c69fcf2b3e92ffe64a3676d8937491", null ],
    [ "GetP2Ymax", "d8/d9b/classG4VAnalysisManager.html#a3055c7589f37f9c11cdb249203861500", null ],
    [ "GetP2Ymin", "d8/d9b/classG4VAnalysisManager.html#aae26390b7c3bb3859a2acbdd4d4f3a9c", null ],
    [ "GetP2YUnit", "d8/d9b/classG4VAnalysisManager.html#a5231c4f080bf9a22c4d5cf26af63e3ef", null ],
    [ "GetP2YWidth", "d8/d9b/classG4VAnalysisManager.html#a6e77b363075e7602e2f6e8e4b253281b", null ],
    [ "GetP2ZAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a00fe1ef39b47c2594ae8d8a3fd535316", null ],
    [ "GetP2ZAxisTitle", "d8/d9b/classG4VAnalysisManager.html#a223e03aff68e9b2862dfe3eeeaf640d0", null ],
    [ "GetP2Zmax", "d8/d9b/classG4VAnalysisManager.html#aaae6eea3e8b413ceeedfa4efcda06bd2", null ],
    [ "GetP2Zmin", "d8/d9b/classG4VAnalysisManager.html#a60c7d6af06fb6e2376f433f089b9151d", null ],
    [ "GetP2ZUnit", "d8/d9b/classG4VAnalysisManager.html#a8cc9b6b19fc3a52dbaec9b5583b7c398", null ],
    [ "GetType", "d8/d9b/classG4VAnalysisManager.html#a6c36008d52c37bce3a715bb326003aa8", null ],
    [ "GetVerboseLevel", "d8/d9b/classG4VAnalysisManager.html#a03584544993b1a8959353e1456b25dad", null ],
    [ "IsActive", "d8/d9b/classG4VAnalysisManager.html#a2c3951c391132da6031a2e50dc98b9b2", null ],
    [ "IsAscii", "d8/d9b/classG4VAnalysisManager.html#ada03c7d88c708e711eef4c2e43886f37", null ],
    [ "IsOpenFile", "d8/d9b/classG4VAnalysisManager.html#adfabe8c215068a8aee69d07d1f79f9c8", null ],
    [ "IsOpenFileImpl", "d8/d9b/classG4VAnalysisManager.html#a0dff987f4b9433fc4224f2387b4179e5", null ],
    [ "IsPlotting", "d8/d9b/classG4VAnalysisManager.html#a8c560f4a3d2c3bc8a14b88b5c5b176f2", null ],
    [ "Merge", "d8/d9b/classG4VAnalysisManager.html#af0b4b55a31d2a48689dec1488b1d9fb9", null ],
    [ "MergeImpl", "d8/d9b/classG4VAnalysisManager.html#a0496cadc09caa044c1956c35dc87f0b1", null ],
    [ "Message", "d8/d9b/classG4VAnalysisManager.html#a88992988e746ddfb63c26cd3ef80f6dc", null ],
    [ "OpenFile", "d8/d9b/classG4VAnalysisManager.html#a70bc2844c95579a2bc0a0bcebf004e7d", null ],
    [ "OpenFileImpl", "d8/d9b/classG4VAnalysisManager.html#a5d1f6d3c406f0d736a89a804e79c5b8a", null ],
    [ "Plot", "d8/d9b/classG4VAnalysisManager.html#a96ef5f17183355fe3818a345b5bb8bd2", null ],
    [ "PlotImpl", "d8/d9b/classG4VAnalysisManager.html#a8b7e392c6f575bd03600146dbdc4072f", null ],
    [ "Reset", "d8/d9b/classG4VAnalysisManager.html#a310ff91c644c3c230205d430da0da82d", null ],
    [ "ResetImpl", "d8/d9b/classG4VAnalysisManager.html#acd388e302d389b8d54ceae8ddb374b23", null ],
    [ "ScaleH1", "d8/d9b/classG4VAnalysisManager.html#adb9f41f92c8c94f568e58ef195e24240", null ],
    [ "ScaleH2", "d8/d9b/classG4VAnalysisManager.html#aedd3b8e40b85a3c52312dddc90aedfdd", null ],
    [ "ScaleH3", "d8/d9b/classG4VAnalysisManager.html#a861cc89c450f406091cb5ef3a560d162", null ],
    [ "ScaleP1", "d8/d9b/classG4VAnalysisManager.html#a7cbf0845b5515b9f1f6dd9a9410805a8", null ],
    [ "ScaleP2", "d8/d9b/classG4VAnalysisManager.html#aebb2fa8aea382737e69e8021d5bd77cb", null ],
    [ "SetActivation", "d8/d9b/classG4VAnalysisManager.html#a67ae9c6a893bd4cb47db3a37703eea6b", null ],
    [ "SetBasketEntries", "d8/d9b/classG4VAnalysisManager.html#ab46f2e625944100a85f4f7bfbae7ef14", null ],
    [ "SetBasketSize", "d8/d9b/classG4VAnalysisManager.html#aa6b8739d6a7695a5bc3bffd1e3a89b07", null ],
    [ "SetCompressionLevel", "d8/d9b/classG4VAnalysisManager.html#ad7697e11ff1d477319511b181b732026", null ],
    [ "SetFileManager", "d8/d9b/classG4VAnalysisManager.html#aade3f9c88b5254cdd14d8320ad4779bb", null ],
    [ "SetFileName", "d8/d9b/classG4VAnalysisManager.html#ad603e24f4f1b8433d0773d4f46371559", null ],
    [ "SetFirstH1Id", "d8/d9b/classG4VAnalysisManager.html#ada6c0f68cbc8ede0cd1fadb8801cdfac", null ],
    [ "SetFirstH2Id", "d8/d9b/classG4VAnalysisManager.html#ac26b261f0476e6fcc14d1e7e20ff53c6", null ],
    [ "SetFirstH3Id", "d8/d9b/classG4VAnalysisManager.html#a90569f6d445b4404f27b4d8e004612ff", null ],
    [ "SetFirstHistoId", "d8/d9b/classG4VAnalysisManager.html#ae1166ca7eb7490b02f5aaccaca058cda", null ],
    [ "SetFirstNtupleColumnId", "d8/d9b/classG4VAnalysisManager.html#a6862a7908a7e7aebd0c5ff2975f4b87c", null ],
    [ "SetFirstNtupleId", "d8/d9b/classG4VAnalysisManager.html#adf4eff9a934c1ca748d1a65aa9d7e09f", null ],
    [ "SetFirstP1Id", "d8/d9b/classG4VAnalysisManager.html#a54baaf6cdd929f1b08fb456b64d4e132", null ],
    [ "SetFirstP2Id", "d8/d9b/classG4VAnalysisManager.html#ac69d4289f0b456c10eba66482a95a784", null ],
    [ "SetFirstProfileId", "d8/d9b/classG4VAnalysisManager.html#ae44bea833aa066f699245a21dbe743b8", null ],
    [ "SetH1", "d8/d9b/classG4VAnalysisManager.html#a559a20e27ab243a40dc86bc6b8bbcf4b", null ],
    [ "SetH1", "d8/d9b/classG4VAnalysisManager.html#abd0ab1c7d9e523d50bd274341984b66c", null ],
    [ "SetH1Activation", "d8/d9b/classG4VAnalysisManager.html#a1591812d99661a730deab57a47bace72", null ],
    [ "SetH1Activation", "d8/d9b/classG4VAnalysisManager.html#acd1e8170683e12381526b5f28159ea70", null ],
    [ "SetH1Ascii", "d8/d9b/classG4VAnalysisManager.html#a0b0efc7e04a0aa1f22d621c943639311", null ],
    [ "SetH1FileName", "d8/d9b/classG4VAnalysisManager.html#a9a366675c0a688895676ae804a6cdda2", null ],
    [ "SetH1Manager", "d8/d9b/classG4VAnalysisManager.html#a2360aa994eb36e21a2189760928090de", null ],
    [ "SetH1Plotting", "d8/d9b/classG4VAnalysisManager.html#a20638a3f20d16dd947c91ee65a4e071e", null ],
    [ "SetH1Title", "d8/d9b/classG4VAnalysisManager.html#a9fdbef07ce216593f7546cc4e7fa5474", null ],
    [ "SetH1XAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#aadec4f30e08fc7e25f9b3b2f4e777c1b", null ],
    [ "SetH1XAxisTitle", "d8/d9b/classG4VAnalysisManager.html#aaa84ba0678711a41dbd19926882a1af6", null ],
    [ "SetH1YAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a419add3d7f9d17d7771c77b1020133f5", null ],
    [ "SetH1YAxisTitle", "d8/d9b/classG4VAnalysisManager.html#af1c782700458ae0af073126363b2e8f1", null ],
    [ "SetH2", "d8/d9b/classG4VAnalysisManager.html#a8afee47a1dc9f37f0ae2e2543d741d12", null ],
    [ "SetH2", "d8/d9b/classG4VAnalysisManager.html#acb401ac5fe1e9b8cf305d270a2a9c5bf", null ],
    [ "SetH2Activation", "d8/d9b/classG4VAnalysisManager.html#abe45bea79e65ebb91ce016406dc6e590", null ],
    [ "SetH2Activation", "d8/d9b/classG4VAnalysisManager.html#a41caaebd79d6f3d711e4c08254655cd6", null ],
    [ "SetH2Ascii", "d8/d9b/classG4VAnalysisManager.html#a864acf8f4e97d222d6a80894af128dce", null ],
    [ "SetH2FileName", "d8/d9b/classG4VAnalysisManager.html#a14b64429cf8ba421415bf178dce99159", null ],
    [ "SetH2Manager", "d8/d9b/classG4VAnalysisManager.html#a2e9c5aa1905fab3535e49463cf48fc27", null ],
    [ "SetH2Plotting", "d8/d9b/classG4VAnalysisManager.html#af466c937124f1df35331ca72039a04d3", null ],
    [ "SetH2Title", "d8/d9b/classG4VAnalysisManager.html#a1128250106e9d00598dacc2cb5080cfa", null ],
    [ "SetH2XAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a01b8fcf9e287b525302423cfd73f3964", null ],
    [ "SetH2XAxisTitle", "d8/d9b/classG4VAnalysisManager.html#a9ea2df127eacb8c16e92410235cecff8", null ],
    [ "SetH2YAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a520b938d17ed7f9dba14d6aedbf0346c", null ],
    [ "SetH2YAxisTitle", "d8/d9b/classG4VAnalysisManager.html#ac09c0217dc2ab41e6dd90438cf3b5e65", null ],
    [ "SetH2ZAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#ad3a4d54a05861f42ea0e8c3b6438f768", null ],
    [ "SetH2ZAxisTitle", "d8/d9b/classG4VAnalysisManager.html#addeaca37508da96c7301c1e6e243bbc5", null ],
    [ "SetH3", "d8/d9b/classG4VAnalysisManager.html#a8f583c5c74e1e6f49e83a84568beef47", null ],
    [ "SetH3", "d8/d9b/classG4VAnalysisManager.html#a49036e47e485d458832509129b8f62ea", null ],
    [ "SetH3Activation", "d8/d9b/classG4VAnalysisManager.html#a2a0e37d132c5699b7794935dcb435b91", null ],
    [ "SetH3Activation", "d8/d9b/classG4VAnalysisManager.html#ac2434edcac96b537f3dc12a7abc78a28", null ],
    [ "SetH3Ascii", "d8/d9b/classG4VAnalysisManager.html#a23576088111ac7ecff7927a7543a4131", null ],
    [ "SetH3FileName", "d8/d9b/classG4VAnalysisManager.html#a72ccfbb4a6005a7ef3c77a4dde9a0d82", null ],
    [ "SetH3Manager", "d8/d9b/classG4VAnalysisManager.html#ac60464b392598a55c59c2157a6805259", null ],
    [ "SetH3Plotting", "d8/d9b/classG4VAnalysisManager.html#a3c8963674400ed52cfbaa3b96d17cea5", null ],
    [ "SetH3Title", "d8/d9b/classG4VAnalysisManager.html#a4745edb175ff9561a342f51779f27b58", null ],
    [ "SetH3XAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a95c44d72d2fc657f7551a83e80716098", null ],
    [ "SetH3XAxisTitle", "d8/d9b/classG4VAnalysisManager.html#ab750589f5ad12699729d79fe0e5127a7", null ],
    [ "SetH3YAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#abd2f5da7d42f5b7e1b70e6f5b5265260", null ],
    [ "SetH3YAxisTitle", "d8/d9b/classG4VAnalysisManager.html#a5cdf4ded77361122938ccecb45f55863", null ],
    [ "SetH3ZAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#ac2e96ea83368619474bf912f7c2233e6", null ],
    [ "SetH3ZAxisTitle", "d8/d9b/classG4VAnalysisManager.html#acbf86ebd59253eb3561db0f3d1743105", null ],
    [ "SetHistoDirectoryName", "d8/d9b/classG4VAnalysisManager.html#a9180107a0f4d0422588d2e1b41319c19", null ],
    [ "SetNtupleActivation", "d8/d9b/classG4VAnalysisManager.html#ad85f84bf1d3c221b1f8b7f4f5bbdf982", null ],
    [ "SetNtupleActivation", "d8/d9b/classG4VAnalysisManager.html#adfb3401dc1aeb1cbe7ea117b8a94af63", null ],
    [ "SetNtupleDirectoryName", "d8/d9b/classG4VAnalysisManager.html#ae556ded0a66399a14180a0ed7d6c1044", null ],
    [ "SetNtupleFileName", "d8/d9b/classG4VAnalysisManager.html#a42aa3475517375b8e374c92321852822", null ],
    [ "SetNtupleFileName", "d8/d9b/classG4VAnalysisManager.html#abcb4df6ce204eef3df2ee2bd88655476", null ],
    [ "SetNtupleManager", "d8/d9b/classG4VAnalysisManager.html#ac2198795f518f9c9b3f3c3d35eac96a2", null ],
    [ "SetNtupleMerging", "d8/d9b/classG4VAnalysisManager.html#ab01d7801b47678ff03b8540f5692fc26", null ],
    [ "SetNtupleRowWise", "d8/d9b/classG4VAnalysisManager.html#aa016d82a7a8ed25016f176bebe3daa2d", null ],
    [ "SetP1", "d8/d9b/classG4VAnalysisManager.html#a93b87642aa5da12c22e7660e3df7d222", null ],
    [ "SetP1", "d8/d9b/classG4VAnalysisManager.html#a5401401dbbfbd1ad4011ea957d85d8a2", null ],
    [ "SetP1Activation", "d8/d9b/classG4VAnalysisManager.html#ab27a80e824c4596211d396c23215336b", null ],
    [ "SetP1Activation", "d8/d9b/classG4VAnalysisManager.html#acedb7e04bc45de8fc29c578d4b5de5a4", null ],
    [ "SetP1Ascii", "d8/d9b/classG4VAnalysisManager.html#a8d2997e30f6937d66476a21b978a57d7", null ],
    [ "SetP1FileName", "d8/d9b/classG4VAnalysisManager.html#aadee15ebf42777317ecd56c09f694955", null ],
    [ "SetP1Manager", "d8/d9b/classG4VAnalysisManager.html#ac555f4cb420386bd194c4764c8cdc4e2", null ],
    [ "SetP1Plotting", "d8/d9b/classG4VAnalysisManager.html#aa33dbbdb8ee28892a9f9a43a1fa31100", null ],
    [ "SetP1Title", "d8/d9b/classG4VAnalysisManager.html#ab40c8242ff253aa85897eaa417c2ca50", null ],
    [ "SetP1XAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#aa12bfe316810202d53dc01fc8a76d1cc", null ],
    [ "SetP1XAxisTitle", "d8/d9b/classG4VAnalysisManager.html#a3154ab1591cbadd3e1c3735de2df76d0", null ],
    [ "SetP1YAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a790175a62b70cb5edeeddeceb9bbe924", null ],
    [ "SetP1YAxisTitle", "d8/d9b/classG4VAnalysisManager.html#a9d2913b5b921035480cf69f1a1a86cc0", null ],
    [ "SetP2", "d8/d9b/classG4VAnalysisManager.html#a3e0389763d6b9d4624abde5a00461f48", null ],
    [ "SetP2", "d8/d9b/classG4VAnalysisManager.html#a5a2b6ef3deb60fee0bc458f7f3c6ec3c", null ],
    [ "SetP2Activation", "d8/d9b/classG4VAnalysisManager.html#a99c97f37d58e44bf2ad4fd98015717ae", null ],
    [ "SetP2Activation", "d8/d9b/classG4VAnalysisManager.html#a6194ff2b25623f7c70db652d4866dc32", null ],
    [ "SetP2Ascii", "d8/d9b/classG4VAnalysisManager.html#af1aa9a90d85e3755be5e70d45bd0646a", null ],
    [ "SetP2FileName", "d8/d9b/classG4VAnalysisManager.html#a40b791a81c56bdcc4bb638fef929fac6", null ],
    [ "SetP2Manager", "d8/d9b/classG4VAnalysisManager.html#ab0ef2340763868b20970ddb64be8e587", null ],
    [ "SetP2Plotting", "d8/d9b/classG4VAnalysisManager.html#a4b1ba15fd4c2578d4bb088e122e7c1cd", null ],
    [ "SetP2Title", "d8/d9b/classG4VAnalysisManager.html#a80a5a8b50c6d6cfdb0c4825e119c893e", null ],
    [ "SetP2XAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#af8904f8c73522fdca552f302c643374a", null ],
    [ "SetP2XAxisTitle", "d8/d9b/classG4VAnalysisManager.html#af9a43ccfc59e7567f91fc91581041330", null ],
    [ "SetP2YAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#ab02fee7cabe0155141e52a0fd2501255", null ],
    [ "SetP2YAxisTitle", "d8/d9b/classG4VAnalysisManager.html#a00e14acd46a31e807a1302fda0c0f15c", null ],
    [ "SetP2ZAxisIsLog", "d8/d9b/classG4VAnalysisManager.html#a543aac83849e371e91a8103bb46916df", null ],
    [ "SetP2ZAxisTitle", "d8/d9b/classG4VAnalysisManager.html#aa473c0b7df82085e1879e059a4ca7e41", null ],
    [ "SetPlotManager", "d8/d9b/classG4VAnalysisManager.html#a7f7fa4092470683508354c54f878309b", null ],
    [ "SetVerboseLevel", "d8/d9b/classG4VAnalysisManager.html#aba42497f736d972ba217ec025a727ad6", null ],
    [ "Write", "d8/d9b/classG4VAnalysisManager.html#afdf9a40a81bc5911d0115012f62ea0c3", null ],
    [ "WriteAscii", "d8/d9b/classG4VAnalysisManager.html#aaff45dc82daf7a0dd6a04b6def51775e", null ],
    [ "WriteImpl", "d8/d9b/classG4VAnalysisManager.html#a3752bfe0dc0622207b0031b0f00fe257", null ],
    [ "fH1HnManager", "d8/d9b/classG4VAnalysisManager.html#aa6d3073f6cf7cb9bfb04871ecba65f69", null ],
    [ "fH2HnManager", "d8/d9b/classG4VAnalysisManager.html#ad5e2f4ff53dd9bd78154b951a5e96c0b", null ],
    [ "fH3HnManager", "d8/d9b/classG4VAnalysisManager.html#a98c8ccfa16430631e5a7dfa7489e793d", null ],
    [ "fkClass", "d8/d9b/classG4VAnalysisManager.html#a1c26bb3e15e0dd4835b4e4c709646cb6", null ],
    [ "fMessenger", "d8/d9b/classG4VAnalysisManager.html#a42dc2cd898f34ec00a5173fd8a522957", null ],
    [ "fNtupleBookingManager", "d8/d9b/classG4VAnalysisManager.html#a7aff3a0bfb5ee504d692eed6fdc1a44f", null ],
    [ "fP1HnManager", "d8/d9b/classG4VAnalysisManager.html#a1d127c1a3408210e1a795dfd8754d899", null ],
    [ "fP2HnManager", "d8/d9b/classG4VAnalysisManager.html#a5a8e827882cd5d770c71a745d7008a9f", null ],
    [ "fPlotManager", "d8/d9b/classG4VAnalysisManager.html#a05b530cd35cff6646e379565218dad51", null ],
    [ "fState", "d8/d9b/classG4VAnalysisManager.html#a9951cd6855644af7a3c6a333f254df5d", null ],
    [ "fVFileManager", "d8/d9b/classG4VAnalysisManager.html#ad1605efc832a0bcc6b513db1f2e0b51b", null ],
    [ "fVH1Manager", "d8/d9b/classG4VAnalysisManager.html#a7867ac2ed6d0deef55182d5c7dc15cc5", null ],
    [ "fVH2Manager", "d8/d9b/classG4VAnalysisManager.html#aa88ec49aecf25931bef6ee3bb24642f6", null ],
    [ "fVH3Manager", "d8/d9b/classG4VAnalysisManager.html#ad332db0607bcd009dc52ee49a6d3d710", null ],
    [ "fVNtupleManager", "d8/d9b/classG4VAnalysisManager.html#ac1661e1569f53b194d5dbdd594ead69b", null ],
    [ "fVP1Manager", "d8/d9b/classG4VAnalysisManager.html#a2820e39bd8ee0df76ad5de21831d1950", null ],
    [ "fVP2Manager", "d8/d9b/classG4VAnalysisManager.html#acf6731e46987999ce950e4f2df8f0333", null ]
];