var classG4HadElementSelector =
[
    [ "G4HadElementSelector", "d8/da8/classG4HadElementSelector.html#a26c5d96b5bac70c53112a54d13f8f6e5", null ],
    [ "~G4HadElementSelector", "d8/da8/classG4HadElementSelector.html#a0ee4d5868dc045c78f0a245a787c8263", null ],
    [ "G4HadElementSelector", "d8/da8/classG4HadElementSelector.html#adfe6695de683d42745d716783c98696a", null ],
    [ "Dump", "d8/da8/classG4HadElementSelector.html#a474b2faa23ff82324097566ee39e44f6", null ],
    [ "operator=", "d8/da8/classG4HadElementSelector.html#a1436ed412e5a343b3be19d7d1e0e0d50", null ],
    [ "SelectRandomAtom", "d8/da8/classG4HadElementSelector.html#aa598011fddac8ef6f9b46de35871fdfe", null ],
    [ "nElmMinusOne", "d8/da8/classG4HadElementSelector.html#ad44726379579e22377efc2994bc557a4", null ],
    [ "theElementVector", "d8/da8/classG4HadElementSelector.html#ac3e302ec4f6dfa1dc811b49da0506266", null ],
    [ "xSections", "d8/da8/classG4HadElementSelector.html#a1b1988448c474451ab1909ea291021f6", null ]
];