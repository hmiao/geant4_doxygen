var classG4ICRU73NoDeltaModel =
[
    [ "G4ICRU73NoDeltaModel", "d8/d55/classG4ICRU73NoDeltaModel.html#a870b8cf8146627a0bb270b129401f100", null ],
    [ "~G4ICRU73NoDeltaModel", "d8/d55/classG4ICRU73NoDeltaModel.html#a0a062a1c6efd93f694b10090b384a6f0", null ],
    [ "G4ICRU73NoDeltaModel", "d8/d55/classG4ICRU73NoDeltaModel.html#af48ff6cd1e09903bc14585739ad2e679", null ],
    [ "ComputeDEDXPerVolume", "d8/d55/classG4ICRU73NoDeltaModel.html#a70bfc09961c31010c96f09d41999e4d0", null ],
    [ "CrossSectionPerVolume", "d8/d55/classG4ICRU73NoDeltaModel.html#a948c7a8144bedfb8a9b050e171051fa8", null ],
    [ "operator=", "d8/d55/classG4ICRU73NoDeltaModel.html#a5a7257cba8eac2f9a2530fd3ad0cb4cc", null ]
];