var classG4HadronPhysicsQGS__BIC =
[
    [ "G4HadronPhysicsQGS_BIC", "d8/dbb/classG4HadronPhysicsQGS__BIC.html#a8696205ea8e31314090f59c1a2db1209", null ],
    [ "G4HadronPhysicsQGS_BIC", "d8/dbb/classG4HadronPhysicsQGS__BIC.html#a99863667c5654f56d55eb08b009db450", null ],
    [ "~G4HadronPhysicsQGS_BIC", "d8/dbb/classG4HadronPhysicsQGS__BIC.html#a73818bdba0194d337cee9c731ddb6562", null ],
    [ "G4HadronPhysicsQGS_BIC", "d8/dbb/classG4HadronPhysicsQGS__BIC.html#a8c5af3715221ddfd3e5f1d96372e4cea", null ],
    [ "Neutron", "d8/dbb/classG4HadronPhysicsQGS__BIC.html#acc77c082741e0594e60c79964eb6cc06", null ],
    [ "operator=", "d8/dbb/classG4HadronPhysicsQGS__BIC.html#af31a6896b61439e54f76cec5566accff", null ],
    [ "Pion", "d8/dbb/classG4HadronPhysicsQGS__BIC.html#ab18b8758744a04402a10faf64be5d8d7", null ],
    [ "Proton", "d8/dbb/classG4HadronPhysicsQGS__BIC.html#aef108f008490e5df2121494863523cee", null ],
    [ "maxBIC_pion", "d8/dbb/classG4HadronPhysicsQGS__BIC.html#a181087daa24632e92cf5af0e8da27f2a", null ],
    [ "minBERT_pion", "d8/dbb/classG4HadronPhysicsQGS__BIC.html#af77d721aff97d0e3491cef93cb7fc61e", null ]
];