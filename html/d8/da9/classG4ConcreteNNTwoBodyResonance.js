var classG4ConcreteNNTwoBodyResonance =
[
    [ "G4ConcreteNNTwoBodyResonance", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#aac3dc04e76e7e97eecbab44a2565d6e4", null ],
    [ "G4ConcreteNNTwoBodyResonance", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#a86932a53a550e0154448283498beb1f3", null ],
    [ "~G4ConcreteNNTwoBodyResonance", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#aa4e67374b9e161eb96079ee7fce221a3", null ],
    [ "G4ConcreteNNTwoBodyResonance", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#afb7ceb85973cb7345d56fe645e8b38ad", null ],
    [ "establish_G4MT_TLS_G4ConcreteNNTwoBodyResonance", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#af8879ba10eb07adb3613be0cd1e9f634", null ],
    [ "GetCrossSectionSource", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#a73f8f3d3b1112b50e2439b7971d73d2e", null ],
    [ "GetListOfColliders", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#ab2cfd4520b3a02e0374bb3fca7bd6e72", null ],
    [ "GetName", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#a306e40a8badf9d26e479fec97ebe94dc", null ],
    [ "GetOutgoingParticles", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#a40bba3b3f4a2f167690f02479ecf3a21", null ],
    [ "IsInCharge", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#a56ba37d758d4925e3f62555ecc8591f3", null ],
    [ "operator!=", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#a5c49fbe6326bd577b18f15e05ed0e63b", null ],
    [ "operator=", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#aa59a9c20c56888f83814cdb31786f0d5", null ],
    [ "operator==", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#a54546d1cf761c3058bbc5bd05b4257be", null ],
    [ "crossSectionSource", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#a02b6279be7122cf4d4b29cd93d4c54e6", null ],
    [ "theOutGoing", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#a498fa1ce6ba91b6ca943093e61aef857", null ],
    [ "thePrimary1", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#a28a3e742062e670c4965ce4f32a607e5", null ],
    [ "thePrimary2", "d8/da9/classG4ConcreteNNTwoBodyResonance.html#ab656f12a4721052cbc05c5a1e16144b9", null ]
];