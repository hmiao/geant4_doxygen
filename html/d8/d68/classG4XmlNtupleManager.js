var classG4XmlNtupleManager =
[
    [ "G4XmlNtupleManager", "d8/d68/classG4XmlNtupleManager.html#a23fa715e48e330a0a43cf140cb1e774c", null ],
    [ "G4XmlNtupleManager", "d8/d68/classG4XmlNtupleManager.html#a9968f7162dcc6b4581d4375cc073aab3", null ],
    [ "~G4XmlNtupleManager", "d8/d68/classG4XmlNtupleManager.html#ad42c4a18a2cfb408ab02896b22f7af7a", null ],
    [ "CreateTNtupleFromBooking", "d8/d68/classG4XmlNtupleManager.html#aa195d483d1d71db3ecd5c36c9457b9f7", null ],
    [ "FinishTNtuple", "d8/d68/classG4XmlNtupleManager.html#afada9e5e9e7f5a2051d1637e4f585501", null ],
    [ "GetNtupleDescriptionVector", "d8/d68/classG4XmlNtupleManager.html#ad8d2a76963a593ec90ac38d5c4b016b0", null ],
    [ "SetFileManager", "d8/d68/classG4XmlNtupleManager.html#a2966777ef14ab7abfcff375c9b3137cd", null ],
    [ "G4XmlAnalysisManager", "d8/d68/classG4XmlNtupleManager.html#ab4b502a9d570be0f97534f34768ef06b", null ],
    [ "G4XmlNtupleFileManager", "d8/d68/classG4XmlNtupleManager.html#a1483a7678170bd035bb6fa87627910f4", null ],
    [ "fFileManager", "d8/d68/classG4XmlNtupleManager.html#a6196db1e0bc1b924ae040cbd8c071c0b", null ],
    [ "fkClass", "d8/d68/classG4XmlNtupleManager.html#a5a9f52af93a20ba971b6c461b2181b97", null ]
];