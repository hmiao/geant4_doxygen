var classG4HadronInelasticProcess =
[
    [ "G4HadronInelasticProcess", "d8/d13/classG4HadronInelasticProcess.html#a2b6daf2ba87c8a678e64eeb1ffa2a7ec", null ],
    [ "~G4HadronInelasticProcess", "d8/d13/classG4HadronInelasticProcess.html#a7091c8f22638e27d5176767fe7ec8b8b", null ],
    [ "G4HadronInelasticProcess", "d8/d13/classG4HadronInelasticProcess.html#a4e9c030abd6859802fb81f9ea950b47d", null ],
    [ "GetParticleDefinition", "d8/d13/classG4HadronInelasticProcess.html#a43de425bbb6ad0322feff65bd235c901", null ],
    [ "IsApplicable", "d8/d13/classG4HadronInelasticProcess.html#a4d1e55644ad95cb76f5bdaff95d2f14b", null ],
    [ "operator=", "d8/d13/classG4HadronInelasticProcess.html#a70c571b47a2aa683f7d2902ace304db8", null ],
    [ "fParticleDef", "d8/d13/classG4HadronInelasticProcess.html#a067123fc1496fe1bb6581869413eed60", null ]
];