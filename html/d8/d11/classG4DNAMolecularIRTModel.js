var classG4DNAMolecularIRTModel =
[
    [ "G4DNAMolecularIRTModel", "d8/d11/classG4DNAMolecularIRTModel.html#a34efe07d155a0b7f1a629a1eba4c3b6c", null ],
    [ "G4DNAMolecularIRTModel", "d8/d11/classG4DNAMolecularIRTModel.html#a8dfc451847ebdf54f191b66a57daaf72", null ],
    [ "G4DNAMolecularIRTModel", "d8/d11/classG4DNAMolecularIRTModel.html#a9113bff2360246da01425d0fe365a59e", null ],
    [ "~G4DNAMolecularIRTModel", "d8/d11/classG4DNAMolecularIRTModel.html#a33495e2754cf6b82f63f38b7e046c1af", null ],
    [ "GetReactionModel", "d8/d11/classG4DNAMolecularIRTModel.html#acd7cd7f088724d4953cb145c583870de", null ],
    [ "Initialize", "d8/d11/classG4DNAMolecularIRTModel.html#aa27f1d9f7fdaf603c779bd6b970ec8cd", null ],
    [ "operator=", "d8/d11/classG4DNAMolecularIRTModel.html#a13894ac53545957adf9af97e24af2fb6", null ],
    [ "PrintInfo", "d8/d11/classG4DNAMolecularIRTModel.html#acb7b99c78489fee699ac054ee6a1dbef", null ],
    [ "SetReactionModel", "d8/d11/classG4DNAMolecularIRTModel.html#a4effdc0eaef852f8e385689b955f8c4c", null ],
    [ "fMolecularReactionTable", "d8/d11/classG4DNAMolecularIRTModel.html#a7807ac6f48bea20ad6b49a2792f3cbf9", null ],
    [ "fpReactionModel", "d8/d11/classG4DNAMolecularIRTModel.html#a08ca99b68f3ca402da419ee8fc0b4c7c", null ]
];