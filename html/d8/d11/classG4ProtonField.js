var classG4ProtonField =
[
    [ "G4ProtonField", "d8/d11/classG4ProtonField.html#aa91cfcadda48bf45ddd5e69e1d389f82", null ],
    [ "~G4ProtonField", "d8/d11/classG4ProtonField.html#ac652d077ee6bee07d091c5704005e702", null ],
    [ "G4ProtonField", "d8/d11/classG4ProtonField.html#aea455058a2647175a414a364a5d6a2df", null ],
    [ "GetBarrier", "d8/d11/classG4ProtonField.html#ab3c6c0aca53d0f0d9fbccd51ad05c7e9", null ],
    [ "GetDensity", "d8/d11/classG4ProtonField.html#a6c54fff26e5aa435de8555ab65480f59", null ],
    [ "GetFermiMomentum", "d8/d11/classG4ProtonField.html#a6a9c36a3570ccdb28ba338409640ee78", null ],
    [ "GetField", "d8/d11/classG4ProtonField.html#a816170adc257ec3543a4be0badf25a4c", null ],
    [ "operator!=", "d8/d11/classG4ProtonField.html#aac4b20abbd59c15b2fae0011d86adf5a", null ],
    [ "operator=", "d8/d11/classG4ProtonField.html#afc653671d944b9ed66a1a57a82b6a5cc", null ],
    [ "operator==", "d8/d11/classG4ProtonField.html#a56f2aaddc4e25dd316e6c7e00a362548", null ],
    [ "theA", "d8/d11/classG4ProtonField.html#a5a0a72d307265b05c244a75f4ebe66b0", null ],
    [ "theBarrier", "d8/d11/classG4ProtonField.html#a9e59ec914585c40fd75f5ede592f84c5", null ],
    [ "theDensity", "d8/d11/classG4ProtonField.html#a6f86f5e97d248cc0b5ff4720dda338e9", null ],
    [ "theFermi", "d8/d11/classG4ProtonField.html#ae36de1a3221de04a994c0e5ea5ca09da", null ],
    [ "theFermiMomBuffer", "d8/d11/classG4ProtonField.html#ad0754adae69946e36f7281272c8c934d", null ],
    [ "theRadius", "d8/d11/classG4ProtonField.html#ae8618111423ed25142f20c33a10b7c0c", null ],
    [ "theZ", "d8/d11/classG4ProtonField.html#a7c2dfc010ac02c3c35445d05cb526b9b", null ]
];