var classG4NucleusLimits =
[
    [ "G4NucleusLimits", "d8/d35/classG4NucleusLimits.html#aecfc079c700c1215d0192c79378e280a", null ],
    [ "G4NucleusLimits", "d8/d35/classG4NucleusLimits.html#adc52ed10f3336b793ab5dbd66dd908ed", null ],
    [ "~G4NucleusLimits", "d8/d35/classG4NucleusLimits.html#a23addb993ce255412b5ba599c80de45b", null ],
    [ "GetAMax", "d8/d35/classG4NucleusLimits.html#af0a2702b6dcfbc60de21404f0d55bb99", null ],
    [ "GetAMin", "d8/d35/classG4NucleusLimits.html#aef93099c342b359261eec7d2f49b520e", null ],
    [ "GetZMax", "d8/d35/classG4NucleusLimits.html#a5f17c88859d085def9a35f5bfad37f25", null ],
    [ "GetZMin", "d8/d35/classG4NucleusLimits.html#a7b3e0cd457e8f4b4879ef32a0a8f7463", null ],
    [ "operator<<", "d8/d35/classG4NucleusLimits.html#a0a562364d0f95f466b2c1e032e63335f", null ],
    [ "aMax", "d8/d35/classG4NucleusLimits.html#ac9394bf4030d3ae5788db6a6f715a2a8", null ],
    [ "aMin", "d8/d35/classG4NucleusLimits.html#a4545c49fb3b1610f7bd63180d41dbcad", null ],
    [ "zMax", "d8/d35/classG4NucleusLimits.html#a4384fad19189397ba0f7929cfc24cb3d", null ],
    [ "zMin", "d8/d35/classG4NucleusLimits.html#a8c4551c5c6aa3d0b5fa3679b98c12d6b", null ]
];