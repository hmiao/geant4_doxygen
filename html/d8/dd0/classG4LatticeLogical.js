var classG4LatticeLogical =
[
    [ "G4LatticeLogical", "d8/dd0/classG4LatticeLogical.html#a7f4f7a855d07066c8312539e2d99db02", null ],
    [ "~G4LatticeLogical", "d8/dd0/classG4LatticeLogical.html#ae7c016bc7d004b98554cc3d3ac4470bf", null ],
    [ "Dump", "d8/dd0/classG4LatticeLogical.html#add6908a7e26b1cb770899445d2ab70aa", null ],
    [ "Dump_NMap", "d8/dd0/classG4LatticeLogical.html#ac2f7433bbe07f35c7c9d0ebb24ac25cb", null ],
    [ "DumpMap", "d8/dd0/classG4LatticeLogical.html#a4883aa2b5ea1b6c65586bed0a8c2a8e6", null ],
    [ "GetAnhDecConstant", "d8/dd0/classG4LatticeLogical.html#a9492d1c699a47d8a588e4be738204910", null ],
    [ "GetBeta", "d8/dd0/classG4LatticeLogical.html#a3bc65ef9059e8969cfecd6f1b78ae27b", null ],
    [ "GetFTDOS", "d8/dd0/classG4LatticeLogical.html#ad6cabad0f32ac7e359161dfffae38999", null ],
    [ "GetGamma", "d8/dd0/classG4LatticeLogical.html#a36e8fdecc5fb75d5e5fe2f92b115aa2e", null ],
    [ "GetLambda", "d8/dd0/classG4LatticeLogical.html#a304abed8617051aa74d4ea13dc1db45c", null ],
    [ "GetLDOS", "d8/dd0/classG4LatticeLogical.html#a05f2e9489051f49f0859503fc3e4776f", null ],
    [ "GetMu", "d8/dd0/classG4LatticeLogical.html#a1eed29f794f6dd9861b020ac55cadc0e", null ],
    [ "GetScatteringConstant", "d8/dd0/classG4LatticeLogical.html#a432c4dfc77a89c57238a7b40bb1fb779", null ],
    [ "GetSTDOS", "d8/dd0/classG4LatticeLogical.html#a5619825d686f28ea45a23bf2e38682b3", null ],
    [ "Load_NMap", "d8/dd0/classG4LatticeLogical.html#ade2d18d93b7c85eab29e0874302bfac6", null ],
    [ "LoadMap", "d8/dd0/classG4LatticeLogical.html#a5716fb0f295a7c4c486312525bcd83bf", null ],
    [ "MapKtoV", "d8/dd0/classG4LatticeLogical.html#a1501d1d743f3cfd356394171cee89e39", null ],
    [ "MapKtoVDir", "d8/dd0/classG4LatticeLogical.html#ac8e75443e17852a94519bc0b81e99efb", null ],
    [ "SetAnhDecConstant", "d8/dd0/classG4LatticeLogical.html#a141408bf26763c2d7f19f768502c2b0b", null ],
    [ "SetDynamicalConstants", "d8/dd0/classG4LatticeLogical.html#aa3bf21d6610a9f48ed4b4a45b84a8152", null ],
    [ "SetFTDOS", "d8/dd0/classG4LatticeLogical.html#a2ebabe42c4f18a6b7f780bf133245b10", null ],
    [ "SetLDOS", "d8/dd0/classG4LatticeLogical.html#a0586db66929ddab98b5b92306c383dbf", null ],
    [ "SetScatteringConstant", "d8/dd0/classG4LatticeLogical.html#aaa3b3a39f5a7123daa61cc3a0d4fbf31", null ],
    [ "SetSTDOS", "d8/dd0/classG4LatticeLogical.html#aa14eb25abb96d0b4282e8f6197cf4c84", null ],
    [ "SetVerboseLevel", "d8/dd0/classG4LatticeLogical.html#ab298bff9f75920b79b1532130694adae", null ],
    [ "fA", "d8/dd0/classG4LatticeLogical.html#a0bbbe2cd0d7e202a973d9276a0121900", null ],
    [ "fB", "d8/dd0/classG4LatticeLogical.html#af2abf9d3e9e6a585975661168646e7ec", null ],
    [ "fBeta", "d8/dd0/classG4LatticeLogical.html#ac8ec702921f79442946498dc9d746d51", null ],
    [ "fDresPhi", "d8/dd0/classG4LatticeLogical.html#abccf2bfda46fbfcf83598b4b4d13dd38", null ],
    [ "fDresTheta", "d8/dd0/classG4LatticeLogical.html#a43c0bd2ce5365387d364b4308d7462dc", null ],
    [ "fFTDOS", "d8/dd0/classG4LatticeLogical.html#a65bd17ca0714bf68e1cd3f9ae4ed2e15", null ],
    [ "fGamma", "d8/dd0/classG4LatticeLogical.html#aad0191547239ef92cde2eb71c4f191db", null ],
    [ "fLambda", "d8/dd0/classG4LatticeLogical.html#ac889ef2cd68753bbe1b76a6b0279600f", null ],
    [ "fLDOS", "d8/dd0/classG4LatticeLogical.html#acfca541855d2c866731de30ddcc2b05d", null ],
    [ "fMap", "d8/dd0/classG4LatticeLogical.html#afb2fa6c8bf7000537545f37e0ead79c9", null ],
    [ "fMu", "d8/dd0/classG4LatticeLogical.html#a0a7f40419dba345a22efd4f56bc88086", null ],
    [ "fN_map", "d8/dd0/classG4LatticeLogical.html#a14126bc19a455be3a3394358c5e24890", null ],
    [ "fSTDOS", "d8/dd0/classG4LatticeLogical.html#a13d57489b259b53088362ec159237445", null ],
    [ "fVresPhi", "d8/dd0/classG4LatticeLogical.html#ace1a2bc3eed795ae359864d763b432b4", null ],
    [ "fVresTheta", "d8/dd0/classG4LatticeLogical.html#a4128d4e68ed6989eb0d31da967b4b67e", null ],
    [ "verboseLevel", "d8/dd0/classG4LatticeLogical.html#a71af7cbc1f892f0d910ba1350ec6c9f3", null ]
];