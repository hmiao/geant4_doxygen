var MCGIDI__misc_8h =
[
    [ "MCGIDI_misc_pointerToAttributeIfAllOk2", "d8/da2/MCGIDI__misc_8h.html#ab08dcfa26b0b5045c21c06e960d3987b", null ],
    [ "MCGIDI_misc_pointerToAttributeIfAllOk3", "d8/da2/MCGIDI__misc_8h.html#a2695fad14ff7f45634ed344931af0bb8", null ],
    [ "MCGIDI_misc_pointerToTOMAttributeIfAllOk2", "d8/da2/MCGIDI__misc_8h.html#a86a80f542c297189ea6921adc436f359", null ],
    [ "MCGIDI_misc_pointerToTOMAttributeIfAllOk3", "d8/da2/MCGIDI__misc_8h.html#a4685af8acb09ab47b884a610d85c7300", null ],
    [ "MCGIDI_misc_copyXMLAttributesToTOM", "d8/da2/MCGIDI__misc_8h.html#ad79b01505adabb72d1e524e206b2d33d", null ],
    [ "MCGIDI_misc_dataFromElement2ptwXYPointsInUnitsOf", "d8/da2/MCGIDI__misc_8h.html#a368336c2c701f0d0e1437990de15e4e2", null ],
    [ "MCGIDI_misc_dataFromXYs2ptwXYPointsInUnitsOf", "d8/da2/MCGIDI__misc_8h.html#a05484d4046a53cd9c85146bf86c3f5bc", null ],
    [ "MCGIDI_misc_getAbsPath", "d8/da2/MCGIDI__misc_8h.html#a56c20b6e8a2183952bf0b285cfb07022", null ],
    [ "MCGIDI_misc_getProductFrame", "d8/da2/MCGIDI__misc_8h.html#ae8ec89af492a35832976e6f58a10e6c3", null ],
    [ "MCGIDI_misc_getUnitConversionFactor", "d8/da2/MCGIDI__misc_8h.html#a646247305be21bb8144966857111292b", null ],
    [ "MCGIDI_misc_pointerToAttributeIfAllOk", "d8/da2/MCGIDI__misc_8h.html#ad0095c1a667fa7995f0d6ad074970f67", null ],
    [ "MCGIDI_misc_pointerToTOMAttributeIfAllOk", "d8/da2/MCGIDI__misc_8h.html#a738dbbdbd7baf38d639bae524ab78264", null ],
    [ "MCGIDI_misc_setMessageError_Element", "d8/da2/MCGIDI__misc_8h.html#a557956a42a5e51d2125c85966b967c4d", null ]
];