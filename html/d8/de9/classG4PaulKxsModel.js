var classG4PaulKxsModel =
[
    [ "G4PaulKxsModel", "d8/de9/classG4PaulKxsModel.html#a7a0c647f6758770bc2e0322c2d094197", null ],
    [ "~G4PaulKxsModel", "d8/de9/classG4PaulKxsModel.html#abda2a8771c5f916749d71fce1435ad7f", null ],
    [ "G4PaulKxsModel", "d8/de9/classG4PaulKxsModel.html#a9e40dea9233212336ae1440cc00a9e65", null ],
    [ "CalculateKCrossSection", "d8/de9/classG4PaulKxsModel.html#a0ade67b2c2e465dfc983a07584313188", null ],
    [ "operator=", "d8/de9/classG4PaulKxsModel.html#a0563fd046986db327f1ccf1ad0e298b2", null ],
    [ "alphaDataSetMap", "d8/de9/classG4PaulKxsModel.html#a4aa9720006a8429992d81b0ca2972ddc", null ],
    [ "interpolation", "d8/de9/classG4PaulKxsModel.html#a8cb06d776224508414e883d2e780dbfd", null ],
    [ "protonDataSetMap", "d8/de9/classG4PaulKxsModel.html#a7c07456a998161e121e4f38a07a67e81", null ]
];