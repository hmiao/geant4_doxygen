var structG4DNAScavengerProcess_1_1G4DNAScavengerProcessState =
[
    [ "G4DNAScavengerProcessState", "d8/dc4/structG4DNAScavengerProcess_1_1G4DNAScavengerProcessState.html#a52459a5011929e404516f3a8715c9bd4", null ],
    [ "~G4DNAScavengerProcessState", "d8/dc4/structG4DNAScavengerProcess_1_1G4DNAScavengerProcessState.html#ae71f00e1709f15cbfd548ba76406d4f1", null ],
    [ "fIsInGoodMaterial", "d8/dc4/structG4DNAScavengerProcess_1_1G4DNAScavengerProcessState.html#ae0013a2e743c6e8b1459f79267c3527b", null ],
    [ "fPreviousTimeAtPreStepPoint", "d8/dc4/structG4DNAScavengerProcess_1_1G4DNAScavengerProcessState.html#a36c9d7108d61d20ef36c5a0d13a974a1", null ]
];