var gzwrite_8c =
[
    [ "gz_comp", "d8/db6/gzwrite_8c.html#a4df5a8f6e323ea9874c8de14f8f1fe49", null ],
    [ "gz_init", "d8/db6/gzwrite_8c.html#ac40fbfaf148025a6fe940ca6c4313e9c", null ],
    [ "gz_write", "d8/db6/gzwrite_8c.html#ab45bd5a82ba95602ff8621f63990d5df", null ],
    [ "gz_zero", "d8/db6/gzwrite_8c.html#ac51b44560d2898b88fa94510b838e82a", null ],
    [ "gzclose_w", "d8/db6/gzwrite_8c.html#a6f657e5b498bef603821cdf41c8375d4", null ],
    [ "gzflush", "d8/db6/gzwrite_8c.html#a6b70d5783d03b12adfd369bd9b7394a2", null ],
    [ "gzfwrite", "d8/db6/gzwrite_8c.html#abbc1f2c9217d3f887a2038216a38893f", null ],
    [ "gzprintf", "d8/db6/gzwrite_8c.html#a23cb6f01c1de1f861e22091d0ca092ef", null ],
    [ "gzputc", "d8/db6/gzwrite_8c.html#a37f7621e057bd71abdbe2f2480b69935", null ],
    [ "gzputs", "d8/db6/gzwrite_8c.html#a2760b9a721dd2950d101046d9b57c461", null ],
    [ "gzsetparams", "d8/db6/gzwrite_8c.html#a20e49f282c69489d3733593736212876", null ],
    [ "gzwrite", "d8/db6/gzwrite_8c.html#a93323ef1288f105db8a89e80f2764d3b", null ],
    [ "OF", "d8/db6/gzwrite_8c.html#adf8c7b20b9049ed662bfe3408c8a4524", null ],
    [ "OF", "d8/db6/gzwrite_8c.html#aa6bd6b7ef8bf5b68ddbcc2dbe8d98757", null ],
    [ "OF", "d8/db6/gzwrite_8c.html#a47fdb41486390d6772db4546c4285f54", null ],
    [ "OF", "d8/db6/gzwrite_8c.html#aad9b669f8ceed4b1d5f732cde398b7be", null ]
];