var classG4AdjointProcessEquivalentToDirectProcess =
[
    [ "G4AdjointProcessEquivalentToDirectProcess", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a71b308f2d424ba37ed937841cf1e2163", null ],
    [ "~G4AdjointProcessEquivalentToDirectProcess", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a26fdcd3da0930164cb772a07646eda9f", null ],
    [ "G4AdjointProcessEquivalentToDirectProcess", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#aae4d5bd5f07afc1a5a41ef502f266106", null ],
    [ "AlongStepDoIt", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a92c0e48dc19f78773b934fd2884d029d", null ],
    [ "AlongStepGetPhysicalInteractionLength", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#ad8a4f028dd63c8aa8e1ff957319c26bf", null ],
    [ "AtRestDoIt", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a33909935799734b451f4766d6f5463ff", null ],
    [ "AtRestGetPhysicalInteractionLength", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a55f16b1f2d5cd7b1be231ffe3b700cc7", null ],
    [ "BuildPhysicsTable", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a9f153380b0743550be081de6abf0ac98", null ],
    [ "EndTracking", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a3e7a618273985515a7c5c186aafb1161", null ],
    [ "IsApplicable", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#ac4453defa109e60aaddb1eb9c84d05f5", null ],
    [ "operator=", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a387474823148e7604a305f08574b5b7f", null ],
    [ "PostStepDoIt", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a388577e77e51b65e3aa72e5731f71536", null ],
    [ "PostStepGetPhysicalInteractionLength", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a9d4206a83013af0380047e658961ddaa", null ],
    [ "PreparePhysicsTable", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a3c5fc181b044f228c170afe6d20dfcbe", null ],
    [ "ResetNumberOfInteractionLengthLeft", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#af6304946067d4ee87eeeb15ce95a607d", null ],
    [ "RetrievePhysicsTable", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a7b33d9d1c28963dfc95104abad67c92e", null ],
    [ "StartTracking", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a18577bd5b78471319fb392361908d9fa", null ],
    [ "StorePhysicsTable", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a4c5c57469035afba23b7b11dec909e7b", null ],
    [ "fDirectProcess", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a00fcc4a5ab4c74dfa95eb3ed6dd958cf", null ],
    [ "fFwdParticleDef", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html#a097bb04bebceaf87bfacdc534f7f85c4", null ]
];