var classG4tgrSolid =
[
    [ "G4tgrSolid", "d8/d3d/classG4tgrSolid.html#ac86d72bac139ffcdfd5b740f8728490d", null ],
    [ "G4tgrSolid", "d8/d3d/classG4tgrSolid.html#ae3618e513c95eb8d646712a6ea2d8ef6", null ],
    [ "~G4tgrSolid", "d8/d3d/classG4tgrSolid.html#a16e091bd73821b9d0d7a73f3639618eb", null ],
    [ "FillSolidParams", "d8/d3d/classG4tgrSolid.html#a34466a0d47ad265c7adda6ec553e73e2", null ],
    [ "GetName", "d8/d3d/classG4tgrSolid.html#a61156e810f6aa1e71a98a78d2bc702ae", null ],
    [ "GetRelativePlace", "d8/d3d/classG4tgrSolid.html#a8fdd8758d27fcb7a5490b1720a234e40", null ],
    [ "GetRelativeRotMatName", "d8/d3d/classG4tgrSolid.html#a859f0e6d1378df01312536e02341878f", null ],
    [ "GetSolidParams", "d8/d3d/classG4tgrSolid.html#ab82d4b292651a44d6bc9211634702afc", null ],
    [ "GetType", "d8/d3d/classG4tgrSolid.html#a1912894974af5bd59abb2b8edcbaed72", null ],
    [ "operator<<", "d8/d3d/classG4tgrSolid.html#a1673c3fbed6b2ded5071f3f98abd6080", null ],
    [ "theName", "d8/d3d/classG4tgrSolid.html#ab3e1029f002c0dd796fcba301ddb921a", null ],
    [ "theSolidParams", "d8/d3d/classG4tgrSolid.html#af2e413bf2c1482acdad52a1815d588fc", null ],
    [ "theType", "d8/d3d/classG4tgrSolid.html#a2334b0e2bb1b770cd308bc18619dd4ae", null ]
];