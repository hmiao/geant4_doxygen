var classG4AlphaPHPBuilder =
[
    [ "G4AlphaPHPBuilder", "d8/d23/classG4AlphaPHPBuilder.html#a92950d6b94b5d7dad4837509369b1997", null ],
    [ "~G4AlphaPHPBuilder", "d8/d23/classG4AlphaPHPBuilder.html#ab74b7b9fac3a5cf21c26afca1800509c", null ],
    [ "Build", "d8/d23/classG4AlphaPHPBuilder.html#a202f2b548125202e47769f048daf9b57", null ],
    [ "Build", "d8/d23/classG4AlphaPHPBuilder.html#acfc76ac7537536f00c766eea256419a1", null ],
    [ "Build", "d8/d23/classG4AlphaPHPBuilder.html#a2702b5ee019459a6cb1c0afe57c0f8a2", null ],
    [ "Build", "d8/d23/classG4AlphaPHPBuilder.html#aaa7dd55ee04b0c8f0e5e5e8dff304123", null ],
    [ "SetMaxEnergy", "d8/d23/classG4AlphaPHPBuilder.html#acec5742922cbf5ba8a7c28771fa59713", null ],
    [ "SetMinEnergy", "d8/d23/classG4AlphaPHPBuilder.html#ad3507e97da34647846deb64d3545601e", null ],
    [ "theMax", "d8/d23/classG4AlphaPHPBuilder.html#ac352415913e66695a014a96da07631fc", null ],
    [ "theMin", "d8/d23/classG4AlphaPHPBuilder.html#a4b45c2abeec56fe88c8ba2d26d5d7e25", null ],
    [ "theParticlePHPModel", "d8/d23/classG4AlphaPHPBuilder.html#abd731924168b06a40fc8ab31e7627ee9", null ]
];