var classG4FastHit =
[
    [ "G4FastHit", "d8/d90/classG4FastHit.html#a2ba5e2fef800cab0f88f5974394d62b6", null ],
    [ "G4FastHit", "d8/d90/classG4FastHit.html#a4289e34add9e3878548991dc8fc81edc", null ],
    [ "G4FastHit", "d8/d90/classG4FastHit.html#ab1b84fdd1b7f143adaecd25e61702f39", null ],
    [ "~G4FastHit", "d8/d90/classG4FastHit.html#aff801aaa4d48cac2c6a3a73bd6c8b322", null ],
    [ "GetEnergy", "d8/d90/classG4FastHit.html#af229e5f3b26ced2b85ddad1a0ece5b1d", null ],
    [ "GetPosition", "d8/d90/classG4FastHit.html#a05024bb13560f49c3f0b5afed7a77041", null ],
    [ "SetEnergy", "d8/d90/classG4FastHit.html#a0e43eda1abbd36baa231777959efa5e9", null ],
    [ "SetPosition", "d8/d90/classG4FastHit.html#ad8d02732db37f61b87fed19a4306e887", null ],
    [ "fEnergy", "d8/d90/classG4FastHit.html#a1467d2303425fdfc82938458de38aa8f", null ],
    [ "fPosition", "d8/d90/classG4FastHit.html#aa15a5e752fe08e217d604e0fa94c1d07", null ]
];