var classG4VCurvedTrajectoryFilter =
[
    [ "G4VCurvedTrajectoryFilter", "d8/de2/classG4VCurvedTrajectoryFilter.html#ae4200c2b80d6ac08f7b30b7590090389", null ],
    [ "~G4VCurvedTrajectoryFilter", "d8/de2/classG4VCurvedTrajectoryFilter.html#a27459bb9b02a8fd3a5678a46d36b2f34", null ],
    [ "CreateNewTrajectorySegment", "d8/de2/classG4VCurvedTrajectoryFilter.html#a687cdeadcbc96ca93dc202b2e5fe265b", null ],
    [ "GimmeThePointsAndForgetThem", "d8/de2/classG4VCurvedTrajectoryFilter.html#a325f4ee1c9e8bd2764bbcf49b01505c3", null ],
    [ "TakeIntermediatePoint", "d8/de2/classG4VCurvedTrajectoryFilter.html#acc01fd230a9ab4800c30ba7d543bd665", null ],
    [ "fpFilteredPoints", "d8/de2/classG4VCurvedTrajectoryFilter.html#a2986b3d8b4064f275babd2f229e2732d", null ]
];