var classG4PolarizedComptonModel =
[
    [ "G4PolarizedComptonModel", "d8/ddd/classG4PolarizedComptonModel.html#a655dd7e96e3be320cb5cc0dabf7724a3", null ],
    [ "~G4PolarizedComptonModel", "d8/ddd/classG4PolarizedComptonModel.html#a6d6b6bd07e5b32c4a4bc438af99b0f1a", null ],
    [ "G4PolarizedComptonModel", "d8/ddd/classG4PolarizedComptonModel.html#af5c15b40a536282cc92dd9b228ad5b68", null ],
    [ "ComputeAsymmetryPerAtom", "d8/ddd/classG4PolarizedComptonModel.html#a26eeb1723d64af49eab2f0eaee347dff", null ],
    [ "ComputeCrossSectionPerAtom", "d8/ddd/classG4PolarizedComptonModel.html#a8513ae47c0516989791cc5c1f714790b", null ],
    [ "GetBeamPolarization", "d8/ddd/classG4PolarizedComptonModel.html#afdebd966f77471d47f0242461a41d630", null ],
    [ "GetFinalElectronPolarization", "d8/ddd/classG4PolarizedComptonModel.html#aa6a2278eb7d236392dbeb893265b13d2", null ],
    [ "GetFinalGammaPolarization", "d8/ddd/classG4PolarizedComptonModel.html#a56bcf10d77791b46a2f5c9cb9680a980", null ],
    [ "GetTargetPolarization", "d8/ddd/classG4PolarizedComptonModel.html#a06d55cf5f5574f6303352cce2c6c53c5", null ],
    [ "operator=", "d8/ddd/classG4PolarizedComptonModel.html#a710fdb4b2387e3edbb8a64b286e99916", null ],
    [ "PrintWarning", "d8/ddd/classG4PolarizedComptonModel.html#a08f617338dfb348c7cd5401870245454", null ],
    [ "SampleSecondaries", "d8/ddd/classG4PolarizedComptonModel.html#a0d92a5a8a34939ecc398ac2f37ed19a6", null ],
    [ "SetBeamPolarization", "d8/ddd/classG4PolarizedComptonModel.html#a1105f2c68e01b44dad74010dab514af1", null ],
    [ "SetTargetPolarization", "d8/ddd/classG4PolarizedComptonModel.html#a045332dc76e4842717fe921ba71f8082", null ],
    [ "fBeamPolarization", "d8/ddd/classG4PolarizedComptonModel.html#a3c61744ce8de8d4c040cba7f65c80e6e", null ],
    [ "fCrossSectionCalculator", "d8/ddd/classG4PolarizedComptonModel.html#ae4cc6fbc59f6e337e4d50924edae5cf3", null ],
    [ "fFinalGammaPolarization", "d8/ddd/classG4PolarizedComptonModel.html#ad9ce90b0cc2ef8bcc9f6f2416dec4eee", null ],
    [ "finalElectronPolarization", "d8/ddd/classG4PolarizedComptonModel.html#a3f5e4d96ef7cdf10334bea8acbe63e4c", null ],
    [ "fLoopLim", "d8/ddd/classG4PolarizedComptonModel.html#ac6cee7960776cd2b4d5a41f3d772fa01", null ],
    [ "fTargetPolarization", "d8/ddd/classG4PolarizedComptonModel.html#aaf35125fc7da198b2a407b71029b5bcb", null ],
    [ "fVerboseLevel", "d8/ddd/classG4PolarizedComptonModel.html#af12f19aaf727f0ae8df3a77fc05a5ce7", null ]
];