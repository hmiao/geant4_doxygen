var classG4ILawForceFreeFlight =
[
    [ "G4ILawForceFreeFlight", "d8/d7a/classG4ILawForceFreeFlight.html#a6cfee5637d77194be4e37b232a3b57fe", null ],
    [ "~G4ILawForceFreeFlight", "d8/d7a/classG4ILawForceFreeFlight.html#a435fa701fdb913d3484273d963e1fc3a", null ],
    [ "ComputeEffectiveCrossSectionAt", "d8/d7a/classG4ILawForceFreeFlight.html#a754d348d478d125b29e701e830e2248e", null ],
    [ "ComputeNonInteractionProbabilityAt", "d8/d7a/classG4ILawForceFreeFlight.html#a53487115a893b03783209fc5e9a3d28d", null ],
    [ "IsSingular", "d8/d7a/classG4ILawForceFreeFlight.html#a6c745e0f647ee1b8df7ee25932c8b397", null ],
    [ "SampleInteractionLength", "d8/d7a/classG4ILawForceFreeFlight.html#ae249dadb924b5cf9a15c4181ffb26d43", null ],
    [ "UpdateInteractionLengthForStep", "d8/d7a/classG4ILawForceFreeFlight.html#a478f9999e7668058ce0287eab0def196", null ]
];