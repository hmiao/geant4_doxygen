var classG4MonopoleEq =
[
    [ "G4MonopoleEq", "d8/d77/classG4MonopoleEq.html#a0a9a6a226ccf79df3c315c08029778ce", null ],
    [ "~G4MonopoleEq", "d8/d77/classG4MonopoleEq.html#a1ec586aedad1bebea7cd488886cce1fd", null ],
    [ "EvaluateRhsGivenB", "d8/d77/classG4MonopoleEq.html#ab333e6245e41f2d3695a5d59a6778067", null ],
    [ "SetChargeMomentumMass", "d8/d77/classG4MonopoleEq.html#a4e6f51e66b1ce97ef8c13f1823a7ebc4", null ],
    [ "fElectroMagCof", "d8/d77/classG4MonopoleEq.html#add77b89b5eb5a8cda45141e4d1912c98", null ],
    [ "fMassCof", "d8/d77/classG4MonopoleEq.html#ad13e13b30bee7ab424ec25e62402563a", null ]
];