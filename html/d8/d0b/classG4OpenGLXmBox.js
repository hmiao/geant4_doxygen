var classG4OpenGLXmBox =
[
    [ "G4OpenGLXmBox", "d8/d0b/classG4OpenGLXmBox.html#ac07d286e699d81e8bcf1931aacddcec6", null ],
    [ "~G4OpenGLXmBox", "d8/d0b/classG4OpenGLXmBox.html#a32acf2b4cac27157fbcad1d1857b685e", null ],
    [ "G4OpenGLXmBox", "d8/d0b/classG4OpenGLXmBox.html#aa191a8aabd083ab340cdab98a071ddd8", null ],
    [ "AddChild", "d8/d0b/classG4OpenGLXmBox.html#add973e35608cb881eb0182a68e28eda6", null ],
    [ "AddYourselfTo", "d8/d0b/classG4OpenGLXmBox.html#a4b1e15e3d0e129337595c0b8f5fa61bc", null ],
    [ "GetName", "d8/d0b/classG4OpenGLXmBox.html#ab91e3772f5a50d11ab551e45a861e85b", null ],
    [ "GetPointerToParent", "d8/d0b/classG4OpenGLXmBox.html#a1f0e6f16fd91c82fed18dcc21287d0fd", null ],
    [ "GetPointerToWidget", "d8/d0b/classG4OpenGLXmBox.html#a8501dd8dfad0a8fe90350317c6de6aa9", null ],
    [ "operator=", "d8/d0b/classG4OpenGLXmBox.html#a7f4eac2cfa3a6465eb55fcf646e73bc1", null ],
    [ "SetName", "d8/d0b/classG4OpenGLXmBox.html#a180f79c468022bb99f1d0921b341f54c", null ],
    [ "box_row_col", "d8/d0b/classG4OpenGLXmBox.html#ab774a35d7656a7f8182ff17de6c1cb45", null ],
    [ "name", "d8/d0b/classG4OpenGLXmBox.html#a30ea4e0652332bf6231dff2815c472e2", null ],
    [ "parent", "d8/d0b/classG4OpenGLXmBox.html#a4c5586d4129b0eaf2508dce9ae38f645", null ],
    [ "radio", "d8/d0b/classG4OpenGLXmBox.html#aa0713cb6cf95c1913f266febaa5f9f21", null ]
];