var classG4LogicalBorderSurface =
[
    [ "G4LogicalBorderSurface", "d8/ddc/classG4LogicalBorderSurface.html#aa7614697445b302f5841b2096a2f3aa3", null ],
    [ "~G4LogicalBorderSurface", "d8/ddc/classG4LogicalBorderSurface.html#a5f83220f8773fb30fe6724c898dde679", null ],
    [ "G4LogicalBorderSurface", "d8/ddc/classG4LogicalBorderSurface.html#a2917b9a70dc994d65879b5a840141888", null ],
    [ "CleanSurfaceTable", "d8/ddc/classG4LogicalBorderSurface.html#ab21c37142e440babc86f9e03cf9ebc0a", null ],
    [ "DumpInfo", "d8/ddc/classG4LogicalBorderSurface.html#ad1545760909e164d880b42284e4196d1", null ],
    [ "GetIndex", "d8/ddc/classG4LogicalBorderSurface.html#abcb7ae5a153f39e520676a5ecb8eec74", null ],
    [ "GetNumberOfBorderSurfaces", "d8/ddc/classG4LogicalBorderSurface.html#a8063b277a68a9f1fdf95668ec4cdd156", null ],
    [ "GetSurface", "d8/ddc/classG4LogicalBorderSurface.html#a8154b4168ebfdaf3e09302fa1615f6ec", null ],
    [ "GetSurfaceTable", "d8/ddc/classG4LogicalBorderSurface.html#a83dd2d9ad8ab5ba133aa2a757a71a96d", null ],
    [ "GetVolume1", "d8/ddc/classG4LogicalBorderSurface.html#ac04ee12142a07acf52174a6edcfef94b", null ],
    [ "GetVolume2", "d8/ddc/classG4LogicalBorderSurface.html#a97353e0b31c2cd734aed6fccd429877a", null ],
    [ "operator!=", "d8/ddc/classG4LogicalBorderSurface.html#a60918507e64d5dc9e7ee6668aa3c7467", null ],
    [ "operator=", "d8/ddc/classG4LogicalBorderSurface.html#aa75f1917fa909cef492e1ff5d2da35cf", null ],
    [ "operator==", "d8/ddc/classG4LogicalBorderSurface.html#adca9b1923d39411bca530e57058a102e", null ],
    [ "SetPhysicalVolumes", "d8/ddc/classG4LogicalBorderSurface.html#a8c5850814906aa8a8b522462e34cceb3", null ],
    [ "SetVolume1", "d8/ddc/classG4LogicalBorderSurface.html#acb48f59c0e7e51eb7acd1f04e7bb393f", null ],
    [ "SetVolume2", "d8/ddc/classG4LogicalBorderSurface.html#ad40e494448ab90a316eda894690b1561", null ],
    [ "Index", "d8/ddc/classG4LogicalBorderSurface.html#a5c86d865945b3d9dc147721e4c3f5acc", null ],
    [ "theBorderSurfaceTable", "d8/ddc/classG4LogicalBorderSurface.html#a020962a03a6da898eac80a8bbade5016", null ],
    [ "Volume1", "d8/ddc/classG4LogicalBorderSurface.html#abac0744a77c4b814c1df779d60ff19cd", null ],
    [ "Volume2", "d8/ddc/classG4LogicalBorderSurface.html#a23b8be3d4b67c5c5b844b1233a0603e4", null ]
];