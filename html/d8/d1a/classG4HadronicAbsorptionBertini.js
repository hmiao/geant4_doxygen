var classG4HadronicAbsorptionBertini =
[
    [ "G4HadronicAbsorptionBertini", "d8/d1a/classG4HadronicAbsorptionBertini.html#a8411a5bd036f9b3e2de06f9dfcbe41ad", null ],
    [ "~G4HadronicAbsorptionBertini", "d8/d1a/classG4HadronicAbsorptionBertini.html#a677571f8760885cf9455ddfe2407e857", null ],
    [ "G4HadronicAbsorptionBertini", "d8/d1a/classG4HadronicAbsorptionBertini.html#a6b1484c682e0761558eb2ce8418f8185", null ],
    [ "IsApplicable", "d8/d1a/classG4HadronicAbsorptionBertini.html#a34d17ed923bbbd9bbd7e88082f2762f4", null ],
    [ "operator=", "d8/d1a/classG4HadronicAbsorptionBertini.html#a30819990c5e214ae4587081084340dcc", null ],
    [ "ProcessDescription", "d8/d1a/classG4HadronicAbsorptionBertini.html#a61643656bb11de6196bbff9eb0d52e11", null ],
    [ "pdefApplicable", "d8/d1a/classG4HadronicAbsorptionBertini.html#a59b6b85b1ef7c794c3e515667bcc7cfc", null ],
    [ "theCascade", "d8/d1a/classG4HadronicAbsorptionBertini.html#a4c4ff628a73e05f48e98c2f6783ea483", null ]
];