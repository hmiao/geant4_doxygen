var classG4PenelopeIonisationXSHandler =
[
    [ "G4PenelopeIonisationXSHandler", "d8/d69/classG4PenelopeIonisationXSHandler.html#a10df5c57123aa1edda11ede3f19a79ef", null ],
    [ "~G4PenelopeIonisationXSHandler", "d8/d69/classG4PenelopeIonisationXSHandler.html#a1689d10c8603a394cff0182ae4210284", null ],
    [ "G4PenelopeIonisationXSHandler", "d8/d69/classG4PenelopeIonisationXSHandler.html#abd2c3345c1decb531a4ba64e9a8a007a", null ],
    [ "BuildDeltaTable", "d8/d69/classG4PenelopeIonisationXSHandler.html#ac6a47313fd4eb3a9b2c4349bf5ce953f", null ],
    [ "BuildXSTable", "d8/d69/classG4PenelopeIonisationXSHandler.html#a4faf9b67f14aa576e1a1a6c57db8b8fc", null ],
    [ "ComputeShellCrossSectionsElectron", "d8/d69/classG4PenelopeIonisationXSHandler.html#af56b4f603d767cd043a996a3412ea1a4", null ],
    [ "ComputeShellCrossSectionsPositron", "d8/d69/classG4PenelopeIonisationXSHandler.html#a7c0582f103481f2d654cec8e58737371", null ],
    [ "GetCrossSectionTableForCouple", "d8/d69/classG4PenelopeIonisationXSHandler.html#a8f5906682bea3a4d1f1bbbd294c6df8f", null ],
    [ "GetDensityCorrection", "d8/d69/classG4PenelopeIonisationXSHandler.html#a080e183f7478cbe7a1c4929fdd6ab04c", null ],
    [ "operator=", "d8/d69/classG4PenelopeIonisationXSHandler.html#aeaa78fb4394c67ae5dc6d95832b2c522", null ],
    [ "SetVerboseLevel", "d8/d69/classG4PenelopeIonisationXSHandler.html#acc3504091d2e23a39ce8bab6359d1688", null ],
    [ "fDeltaTable", "d8/d69/classG4PenelopeIonisationXSHandler.html#ae3b62bbff36d857b8bab846e9e0026be", null ],
    [ "fEnergyGrid", "d8/d69/classG4PenelopeIonisationXSHandler.html#a70b8c7f5e550ab3f23085006b2b05e51", null ],
    [ "fNBins", "d8/d69/classG4PenelopeIonisationXSHandler.html#ae812495d6712264d95522d0dc5491e75", null ],
    [ "fOscManager", "d8/d69/classG4PenelopeIonisationXSHandler.html#a96a23c2e12a10e4b35d413e2fdbdad7e", null ],
    [ "fVerboseLevel", "d8/d69/classG4PenelopeIonisationXSHandler.html#a758e6513afb5e5b3f2a9abd3949e59e9", null ],
    [ "fXSTableElectron", "d8/d69/classG4PenelopeIonisationXSHandler.html#aaba263fc9e3ef1dd286dbd8e7b0776b4", null ],
    [ "fXSTablePositron", "d8/d69/classG4PenelopeIonisationXSHandler.html#a65de84a2e7b84b52a8b564ac11c9a4ba", null ]
];