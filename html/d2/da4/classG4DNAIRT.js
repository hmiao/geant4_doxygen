var classG4DNAIRT =
[
    [ "G4DNAIRT", "d2/da4/classG4DNAIRT.html#a9876dfe03e29ccfa819eb9b9bed4bc8f", null ],
    [ "G4DNAIRT", "d2/da4/classG4DNAIRT.html#ab77cc68fe224b4862b45438a2b745bef", null ],
    [ "~G4DNAIRT", "d2/da4/classG4DNAIRT.html#ab962dc8212b71d970c052f118e9efae7", null ],
    [ "G4DNAIRT", "d2/da4/classG4DNAIRT.html#a25e4178570405aee628706b865ec2880", null ],
    [ "FindBin", "d2/da4/classG4DNAIRT.html#a663cd77fbc60f5ac14bba4a4529822f4", null ],
    [ "FindReaction", "d2/da4/classG4DNAIRT.html#a9f1fc980a062ec9b9408631e5198c92c", null ],
    [ "GetIndependentReactionTime", "d2/da4/classG4DNAIRT.html#a6947f2c7751401cc8f8835f81c787ebe", null ],
    [ "Initialize", "d2/da4/classG4DNAIRT.html#a564d4825c326628b96c9d0831378e1be", null ],
    [ "IRTSampling", "d2/da4/classG4DNAIRT.html#a061ecb55afb955111494362b7e141768", null ],
    [ "MakeReaction", "d2/da4/classG4DNAIRT.html#aaad4bbd42e04d7fde75394e16556c83f", null ],
    [ "operator=", "d2/da4/classG4DNAIRT.html#a19b73f91069107b083b6220dfd87b333", null ],
    [ "SamplePDC", "d2/da4/classG4DNAIRT.html#a468a00d72ecb8e30f82b2725abab7ce3", null ],
    [ "Sampling", "d2/da4/classG4DNAIRT.html#af7f5e1c0fba92f12d87797a76a61b78f", null ],
    [ "SetReactionModel", "d2/da4/classG4DNAIRT.html#a2cb7d0426845632efa30376bddfd2f22", null ],
    [ "SpaceBinning", "d2/da4/classG4DNAIRT.html#aa1b2cc6c85df8d60cfed5e4c753f758b", null ],
    [ "TestReactibility", "d2/da4/classG4DNAIRT.html#a42afa8e0b06e64e05aac4a2deef183e5", null ],
    [ "erfc", "d2/da4/classG4DNAIRT.html#accbfb24abdbef4a125c1a1c7e1f22c8a", null ],
    [ "fMolReactionTable", "d2/da4/classG4DNAIRT.html#a54e9f38b6d36b0d4ca7078af5efb4405", null ],
    [ "fNx", "d2/da4/classG4DNAIRT.html#a340fe1686464f22cda096ffad0926bc1", null ],
    [ "fNy", "d2/da4/classG4DNAIRT.html#abb50cee162d889dc2cc2979049ce196b", null ],
    [ "fNz", "d2/da4/classG4DNAIRT.html#a63f0f6ba5f153c194dedbd13352012fe", null ],
    [ "fpReactionModel", "d2/da4/classG4DNAIRT.html#aea11d5e50b6124bcde91291396694d3c", null ],
    [ "fRCutOff", "d2/da4/classG4DNAIRT.html#acaedfdea6ed97ac4e6398b8d35c603df", null ],
    [ "fReactionSet", "d2/da4/classG4DNAIRT.html#a2484289e8de450650299fd92c3f5ef80", null ],
    [ "fTrackHolder", "d2/da4/classG4DNAIRT.html#a0f9cbab82ea069b8b6ffd1c956260154", null ],
    [ "fXMax", "d2/da4/classG4DNAIRT.html#a6f6d5183aa86c3cabf66c27f79ce866e", null ],
    [ "fXMin", "d2/da4/classG4DNAIRT.html#a287150359dc19647b15afe407284602c", null ],
    [ "fYMax", "d2/da4/classG4DNAIRT.html#acf859d1acfe8c79b8c0baf0b27ae1288", null ],
    [ "fYMin", "d2/da4/classG4DNAIRT.html#a0fad3bb6bbfc68476ef7e642bab5fd7f", null ],
    [ "fZMax", "d2/da4/classG4DNAIRT.html#af30397355d9f8a4328f1358b2cc1127e", null ],
    [ "fZMin", "d2/da4/classG4DNAIRT.html#a012a9f45ddb716bdf39683008d8741a3", null ],
    [ "spaceBinned", "d2/da4/classG4DNAIRT.html#a633f481fcd4e51592906f19939039090", null ],
    [ "timeMax", "d2/da4/classG4DNAIRT.html#a465427e9e09aff340af922dcd650bc75", null ],
    [ "timeMin", "d2/da4/classG4DNAIRT.html#a93ccd891bc425150b07a285cfc0dfc6c", null ],
    [ "xendIndex", "d2/da4/classG4DNAIRT.html#a7580d43c6ef09b5cef1e8a26290580d7", null ],
    [ "xiniIndex", "d2/da4/classG4DNAIRT.html#aa3d2e493017862bd8698b092b4227b02", null ],
    [ "yendIndex", "d2/da4/classG4DNAIRT.html#a31ef65233c88b68a6de6a27ddb8c78bf", null ],
    [ "yiniIndex", "d2/da4/classG4DNAIRT.html#ae05f5e0467e102834c63fd371e13b8a2", null ],
    [ "zendIndex", "d2/da4/classG4DNAIRT.html#a7c3cea83554779ae3d28dcf96467a53c", null ],
    [ "ziniIndex", "d2/da4/classG4DNAIRT.html#af50931cc4537a84933b90be04c23d7c9", null ]
];