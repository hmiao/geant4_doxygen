var classG4TouchableHistory =
[
    [ "G4TouchableHistory", "d2/dc3/classG4TouchableHistory.html#a02e68c68063034ee8f9919158def0a59", null ],
    [ "G4TouchableHistory", "d2/dc3/classG4TouchableHistory.html#aece613056e7ce0d63083b20cf9903beb", null ],
    [ "~G4TouchableHistory", "d2/dc3/classG4TouchableHistory.html#af2183d6259108bf9f7f52a86d060db8f", null ],
    [ "CalculateHistoryIndex", "d2/dc3/classG4TouchableHistory.html#ad3c229fe66d00bc87275dfe3a547a229", null ],
    [ "GetHistory", "d2/dc3/classG4TouchableHistory.html#a86a59d18b03e3789c13be4317fb74f81", null ],
    [ "GetHistoryDepth", "d2/dc3/classG4TouchableHistory.html#a6a62a1bd43da7d707ea9472589d78c17", null ],
    [ "GetReplicaNumber", "d2/dc3/classG4TouchableHistory.html#a9a949bb454fede4d6401df4c76a817cd", null ],
    [ "GetRotation", "d2/dc3/classG4TouchableHistory.html#a3dd731d54255c22851c4913046d8286f", null ],
    [ "GetSolid", "d2/dc3/classG4TouchableHistory.html#a216a22b025f928b08dfe2271c0097cf8", null ],
    [ "GetTranslation", "d2/dc3/classG4TouchableHistory.html#a967bba7ad3ad82ea4469cede5d9c9bff", null ],
    [ "GetVolume", "d2/dc3/classG4TouchableHistory.html#a58b6885ad2b226f1f390a0802b4326db", null ],
    [ "MoveUpHistory", "d2/dc3/classG4TouchableHistory.html#ad24725b319b0b037eb5dfbaff3cae405", null ],
    [ "operator delete", "d2/dc3/classG4TouchableHistory.html#a608d2032666e511eeab1913926c77d67", null ],
    [ "operator new", "d2/dc3/classG4TouchableHistory.html#a8a82dd18e05493f6b5e1c940e315e091", null ],
    [ "UpdateYourself", "d2/dc3/classG4TouchableHistory.html#a2907f9e1583f7e1a0b777125508a798b", null ],
    [ "fhistory", "d2/dc3/classG4TouchableHistory.html#ab6b3ce7d500d6848fd1f99ef0c593444", null ],
    [ "frot", "d2/dc3/classG4TouchableHistory.html#a304b1fbe57294aa30e82d805ca6625e4", null ],
    [ "ftlate", "d2/dc3/classG4TouchableHistory.html#a285923d9b7a37cd6ea81ab447a1b59d3", null ]
];