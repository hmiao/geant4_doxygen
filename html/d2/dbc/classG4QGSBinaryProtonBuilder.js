var classG4QGSBinaryProtonBuilder =
[
    [ "G4QGSBinaryProtonBuilder", "d2/dbc/classG4QGSBinaryProtonBuilder.html#a65a312f33a1ef50cd2aa03dcb2086cbd", null ],
    [ "~G4QGSBinaryProtonBuilder", "d2/dbc/classG4QGSBinaryProtonBuilder.html#ad9eec502889baf7d0b85705d052b7b81", null ],
    [ "Build", "d2/dbc/classG4QGSBinaryProtonBuilder.html#a7e1333014acbb13a7a3bd0102dca66e2", null ],
    [ "Build", "d2/dbc/classG4QGSBinaryProtonBuilder.html#a7a534415d8d1a3953326b85a9936955e", null ],
    [ "Build", "d2/dbc/classG4QGSBinaryProtonBuilder.html#af74410776188cd9bdeb82e09dafb11d6", null ],
    [ "Build", "d2/dbc/classG4QGSBinaryProtonBuilder.html#a7d7a3fe1fc3f798f594a92694411bb49", null ],
    [ "SetMinEnergy", "d2/dbc/classG4QGSBinaryProtonBuilder.html#aea02b48128acc2cba9874d7fc12f8e9e", null ],
    [ "theMin", "d2/dbc/classG4QGSBinaryProtonBuilder.html#a38fd1f32beaeecfbb7d781c125dd6701", null ],
    [ "theModel", "d2/dbc/classG4QGSBinaryProtonBuilder.html#ac795446b2d4e6e6a8ee45d5ccd6b8eea", null ]
];