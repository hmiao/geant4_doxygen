var classG4ANuElNucleusCcModel =
[
    [ "G4ANuElNucleusCcModel", "d2/d07/classG4ANuElNucleusCcModel.html#afbe64ee8b7794fd03a717d663d0f050f", null ],
    [ "~G4ANuElNucleusCcModel", "d2/d07/classG4ANuElNucleusCcModel.html#a59df90321b029cfc254e62102437340a", null ],
    [ "ApplyYourself", "d2/d07/classG4ANuElNucleusCcModel.html#a56f561ad3e27fb745cbc05924e6c35dc", null ],
    [ "GetMinNuElEnergy", "d2/d07/classG4ANuElNucleusCcModel.html#a3bda36e9bcabbb08f60bca9b27d184aa", null ],
    [ "InitialiseModel", "d2/d07/classG4ANuElNucleusCcModel.html#a6df434b7caa2ed5dfef705d46a0f0b63", null ],
    [ "IsApplicable", "d2/d07/classG4ANuElNucleusCcModel.html#a91dc5c13aa4a93011609fd3ec96f45d7", null ],
    [ "ModelDescription", "d2/d07/classG4ANuElNucleusCcModel.html#a04696efa0555a4faf44a4889b1276f07", null ],
    [ "SampleLVkr", "d2/d07/classG4ANuElNucleusCcModel.html#abdc0d77446d5d3301504ddbb6f9a41ad", null ],
    [ "ThresholdEnergy", "d2/d07/classG4ANuElNucleusCcModel.html#a0aceb8fd96c5cc929d668a09981dc124", null ],
    [ "fData", "d2/d07/classG4ANuElNucleusCcModel.html#adbbe4ad84feac32b2036d5ac4239261c", null ],
    [ "fMaster", "d2/d07/classG4ANuElNucleusCcModel.html#a080b866b4b26e1b3c48d88cf2c7f0378", null ],
    [ "fMel", "d2/d07/classG4ANuElNucleusCcModel.html#ac0073b16899ca17b4f54e42ad8ac07ab", null ],
    [ "thePositron", "d2/d07/classG4ANuElNucleusCcModel.html#aa1678028dcb357d01e56f45856091599", null ]
];