var classG4eIonisationCrossSectionHandler =
[
    [ "G4eIonisationCrossSectionHandler", "d2/d30/classG4eIonisationCrossSectionHandler.html#a670bff85d503b79a97b8b47b253a70fe", null ],
    [ "~G4eIonisationCrossSectionHandler", "d2/d30/classG4eIonisationCrossSectionHandler.html#afbfb7149a7a919485dc927f4a8cfc77d", null ],
    [ "G4eIonisationCrossSectionHandler", "d2/d30/classG4eIonisationCrossSectionHandler.html#a384d6cd6d10c09157bca13c73937d49b", null ],
    [ "BuildCrossSectionsForMaterials", "d2/d30/classG4eIonisationCrossSectionHandler.html#af495962a1543c762f69979607fb648f6", null ],
    [ "GetCrossSectionAboveThresholdForElement", "d2/d30/classG4eIonisationCrossSectionHandler.html#a14d52380980c538babed05fa4fa0a45c", null ],
    [ "operator=", "d2/d30/classG4eIonisationCrossSectionHandler.html#af279e6e6b1724eff57cdf32fa6f56fe5", null ],
    [ "interp", "d2/d30/classG4eIonisationCrossSectionHandler.html#acfb1efc5dceecc931bcdf6316de1c3ca", null ],
    [ "theParam", "d2/d30/classG4eIonisationCrossSectionHandler.html#a06540aba635e9e41142324d8d57889d5", null ],
    [ "verbose", "d2/d30/classG4eIonisationCrossSectionHandler.html#adf9f02d0878852cbdcf0199729019662", null ]
];