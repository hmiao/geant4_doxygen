var classG4HETCProton =
[
    [ "G4HETCProton", "d2/dca/classG4HETCProton.html#a6b23cb8d242a56562092ffda9c462108", null ],
    [ "~G4HETCProton", "d2/dca/classG4HETCProton.html#a2519350910d0888061a372a81d326179", null ],
    [ "G4HETCProton", "d2/dca/classG4HETCProton.html#aeeb6b6f185d4fe2e37f5e64d3bf0888a", null ],
    [ "GetAlpha", "d2/dca/classG4HETCProton.html#aa71eb835964dafcdc413a0ccdb120a26", null ],
    [ "GetBeta", "d2/dca/classG4HETCProton.html#a1ef3d9a7a02fcc87d9b7af92df9293fd", null ],
    [ "GetSpinFactor", "d2/dca/classG4HETCProton.html#a696938a943c103a8ba5305e86cbe37a7", null ],
    [ "K", "d2/dca/classG4HETCProton.html#a06dd6f6b61041c1fff6fadb1c0db33c7", null ],
    [ "operator!=", "d2/dca/classG4HETCProton.html#a2282164afae12c0fb27edb04259299a0", null ],
    [ "operator=", "d2/dca/classG4HETCProton.html#a23538abf2c7202d9d3bcbd335bea5639", null ],
    [ "operator==", "d2/dca/classG4HETCProton.html#a1eaf16a922b6928883a786f47eadd7a5", null ],
    [ "theProtonCoulombBarrier", "d2/dca/classG4HETCProton.html#afe9d7a9e35dd78d8457d5fa5ee12515f", null ]
];