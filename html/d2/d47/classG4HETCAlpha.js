var classG4HETCAlpha =
[
    [ "G4HETCAlpha", "d2/d47/classG4HETCAlpha.html#af42dd1df11e50ecdf2342eff217538cc", null ],
    [ "~G4HETCAlpha", "d2/d47/classG4HETCAlpha.html#a855831ed4ef9f40530d5f69cd8d1cc97", null ],
    [ "G4HETCAlpha", "d2/d47/classG4HETCAlpha.html#a303312fdf260ad4df611db8d5f0572eb", null ],
    [ "GetAlpha", "d2/d47/classG4HETCAlpha.html#aa3c7e949a50a93c6af954f28f00a6256", null ],
    [ "GetBeta", "d2/d47/classG4HETCAlpha.html#a1d3b4b22be200d5624971a87700e09ee", null ],
    [ "GetSpinFactor", "d2/d47/classG4HETCAlpha.html#a005c16bd5880900826a8d60c7dbc26ef", null ],
    [ "K", "d2/d47/classG4HETCAlpha.html#a65550ff5f7ee91e6d7b16173ed09f29d", null ],
    [ "operator!=", "d2/d47/classG4HETCAlpha.html#a7bacfcf5c949c7d8734930f77da5bab9", null ],
    [ "operator=", "d2/d47/classG4HETCAlpha.html#a343da1fde9cf5a65bb36162eb0482e34", null ],
    [ "operator==", "d2/d47/classG4HETCAlpha.html#a22fd4fa895a0c5e9e5e0b854b72b03ad", null ],
    [ "theAlphaCoulombBarrier", "d2/d47/classG4HETCAlpha.html#a3b0daaea1f4ebe93758b64a4e298ff0f", null ]
];