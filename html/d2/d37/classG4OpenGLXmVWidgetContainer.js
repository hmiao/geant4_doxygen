var classG4OpenGLXmVWidgetContainer =
[
    [ "G4OpenGLXmVWidgetContainer", "d2/d37/classG4OpenGLXmVWidgetContainer.html#aa697b76553acecb4be3b0f559cd4ecd8", null ],
    [ "~G4OpenGLXmVWidgetContainer", "d2/d37/classG4OpenGLXmVWidgetContainer.html#ad1803d90731c9793a04b1c2ff27b1f62", null ],
    [ "AddChild", "d2/d37/classG4OpenGLXmVWidgetContainer.html#a41a714e7f049bc0b06b8465790a5d5ef", null ],
    [ "AddYourselfTo", "d2/d37/classG4OpenGLXmVWidgetContainer.html#ac89fda82e3394bd3dfea5fd4ccdad19c", null ],
    [ "GetPointerToParent", "d2/d37/classG4OpenGLXmVWidgetContainer.html#a92af980f8de8e2cb850ac48bcf1146dd", null ],
    [ "GetPointerToWidget", "d2/d37/classG4OpenGLXmVWidgetContainer.html#a5ef6ada3ee950f84fedce7c11f70f64d", null ]
];