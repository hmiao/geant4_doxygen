var classG4VIntraNuclearTransportModel =
[
    [ "G4VIntraNuclearTransportModel", "d2/d50/classG4VIntraNuclearTransportModel.html#a2153f0158a892f7669f141f4af3a204e", null ],
    [ "~G4VIntraNuclearTransportModel", "d2/d50/classG4VIntraNuclearTransportModel.html#aac8784ed5454e495cf8bce5f2bb7950b", null ],
    [ "G4VIntraNuclearTransportModel", "d2/d50/classG4VIntraNuclearTransportModel.html#a78eebb4d92a30ceb0fdd95a11e327651", null ],
    [ "Get3DNucleus", "d2/d50/classG4VIntraNuclearTransportModel.html#aa54af2ff63ceed78e7b3dc115b9f6b40", null ],
    [ "GetDeExcitation", "d2/d50/classG4VIntraNuclearTransportModel.html#a8f69e0d63bf4d11ca49ba14030690180", null ],
    [ "GetModelName", "d2/d50/classG4VIntraNuclearTransportModel.html#a7809e9d05a30077d0a025a6a2bbedd39", null ],
    [ "GetPrimaryProjectile", "d2/d50/classG4VIntraNuclearTransportModel.html#a9bb857ff51bd2c197ffb7772675bc7fc", null ],
    [ "ModelDescription", "d2/d50/classG4VIntraNuclearTransportModel.html#a1dd376ec3189a35031bb471e8217bced", null ],
    [ "operator!=", "d2/d50/classG4VIntraNuclearTransportModel.html#aa7a8bd6fd09801d8bab7f79a19a0dd82", null ],
    [ "operator=", "d2/d50/classG4VIntraNuclearTransportModel.html#afa81a843cdf13f3a70ed729d32aa23d3", null ],
    [ "operator==", "d2/d50/classG4VIntraNuclearTransportModel.html#aef6b4536d0451ee776cafecd5f5f8421", null ],
    [ "Propagate", "d2/d50/classG4VIntraNuclearTransportModel.html#ac4e4321f2fb4f7fce467cc5e6c85d683", null ],
    [ "PropagateModelDescription", "d2/d50/classG4VIntraNuclearTransportModel.html#ab64bc8274016bb5ec50fc09490d0901f", null ],
    [ "PropagateNuclNucl", "d2/d50/classG4VIntraNuclearTransportModel.html#a069292968a4b415e26cc623bae627560", null ],
    [ "Set3DNucleus", "d2/d50/classG4VIntraNuclearTransportModel.html#aff16c05402b69cf3119d082c97aab9cc", null ],
    [ "SetDeExcitation", "d2/d50/classG4VIntraNuclearTransportModel.html#aacd1bd25b7f64aab4ed1cae46687341d", null ],
    [ "SetPrimaryProjectile", "d2/d50/classG4VIntraNuclearTransportModel.html#a323f202382c7f51cbdfc75d31f822748", null ],
    [ "the3DNucleus", "d2/d50/classG4VIntraNuclearTransportModel.html#a6f4441034489444059b985e5cbf2dd18", null ],
    [ "theDeExcitation", "d2/d50/classG4VIntraNuclearTransportModel.html#a0eb7ffe1546a03103ed5543620303191", null ],
    [ "thePrimaryProjectile", "d2/d50/classG4VIntraNuclearTransportModel.html#af58cfd0f62e7f13de5a16e03e6c3f684", null ],
    [ "theTransportModelName", "d2/d50/classG4VIntraNuclearTransportModel.html#a1be69ce3878da67a2c3026ce307d02f9", null ]
];