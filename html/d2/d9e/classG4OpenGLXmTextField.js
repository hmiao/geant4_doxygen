var classG4OpenGLXmTextField =
[
    [ "G4OpenGLXmTextField", "d2/d9e/classG4OpenGLXmTextField.html#ad82f0195edb220ca8f09417edb7976a7", null ],
    [ "G4OpenGLXmTextField", "d2/d9e/classG4OpenGLXmTextField.html#a6a3d6a5cf58347ab9df741030d2b56c9", null ],
    [ "~G4OpenGLXmTextField", "d2/d9e/classG4OpenGLXmTextField.html#abe456c9d0a32d57074fcaa4c2c47f481", null ],
    [ "G4OpenGLXmTextField", "d2/d9e/classG4OpenGLXmTextField.html#a98557558f88c6c92459a6a79ae683910", null ],
    [ "AddYourselfTo", "d2/d9e/classG4OpenGLXmTextField.html#a03203d3b26d1497d7d0902dc46b5fed7", null ],
    [ "GetName", "d2/d9e/classG4OpenGLXmTextField.html#a8586604bce0de207cf01985866cc4d6e", null ],
    [ "GetPointerToParent", "d2/d9e/classG4OpenGLXmTextField.html#a754f1a7e31767ad990f3510c8053c2e8", null ],
    [ "GetPointerToWidget", "d2/d9e/classG4OpenGLXmTextField.html#a172156d59a672803b54b821d7ca61d63", null ],
    [ "GetValue", "d2/d9e/classG4OpenGLXmTextField.html#afd1d499e0d86064832e55e5431a14b94", null ],
    [ "operator=", "d2/d9e/classG4OpenGLXmTextField.html#acc37c80ff134084df35578ceb49090a7", null ],
    [ "SetName", "d2/d9e/classG4OpenGLXmTextField.html#a872e1ab998d64432f8f64ee142e9996d", null ],
    [ "SetValue", "d2/d9e/classG4OpenGLXmTextField.html#ae6d47c888550ce633aeab38ba1e086a7", null ],
    [ "SetValue", "d2/d9e/classG4OpenGLXmTextField.html#a8983a5744654a2e1b447665ab6320a27", null ],
    [ "initial", "d2/d9e/classG4OpenGLXmTextField.html#a42264d9f4366cbb1e2a72e836886442f", null ],
    [ "name", "d2/d9e/classG4OpenGLXmTextField.html#a77d995d1b3a41c5d2c7dba6622b99506", null ],
    [ "parent", "d2/d9e/classG4OpenGLXmTextField.html#a8e3983f4c234b4c613a9b017e6096acb", null ],
    [ "text", "d2/d9e/classG4OpenGLXmTextField.html#a2fd692c60d30e5c74f744675f8a74561", null ],
    [ "text_field", "d2/d9e/classG4OpenGLXmTextField.html#a52938bc2f998696a6b7a301956a90451", null ],
    [ "text_label", "d2/d9e/classG4OpenGLXmTextField.html#a7d89da7781b9a3ec3aa844620ee95c23", null ],
    [ "value", "d2/d9e/classG4OpenGLXmTextField.html#a71f0bd62addcc2016edeaeb7b401b36f", null ]
];