var ptwXY__methods_8cc =
[
    [ "ptwXY_clip", "d2/d5a/ptwXY__methods_8cc.html#aeaa4d1d45576aa21e7ee084073224ad8", null ],
    [ "ptwXY_clip2", "d2/d5a/ptwXY__methods_8cc.html#ab1bbc88acb08895889c7794356d5a47f", null ],
    [ "ptwXY_scaleOffsetXAndY", "d2/d5a/ptwXY__methods_8cc.html#a33d91be713a8305857acf9450f9f72cb", null ],
    [ "ptwXY_thicken", "d2/d5a/ptwXY__methods_8cc.html#adbe66a65e8e76b2d7c9d40cd054118ad", null ],
    [ "ptwXY_thicken_linear_dx", "d2/d5a/ptwXY__methods_8cc.html#a0a76f577859a08c87e8f639bca0df783", null ],
    [ "ptwXY_thin", "d2/d5a/ptwXY__methods_8cc.html#a1c3b48fd72676252a95f7b2437f09027", null ],
    [ "ptwXY_thin2", "d2/d5a/ptwXY__methods_8cc.html#aa8170a27b249550715d26230a7d1ebe2", null ],
    [ "ptwXY_trim", "d2/d5a/ptwXY__methods_8cc.html#a55b95ee9945d804d203b96bc09ef45eb", null ],
    [ "ptwXY_union", "d2/d5a/ptwXY__methods_8cc.html#a2211debafce0bef441659a8a443c7bab", null ]
];