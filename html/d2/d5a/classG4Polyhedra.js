var classG4Polyhedra =
[
    [ "surface_element", "dc/d69/structG4Polyhedra_1_1surface__element.html", "dc/d69/structG4Polyhedra_1_1surface__element" ],
    [ "G4Polyhedra", "d2/d5a/classG4Polyhedra.html#a2851a2d070f72765e085ce034aa5715b", null ],
    [ "G4Polyhedra", "d2/d5a/classG4Polyhedra.html#a16b21c73fc4baba68a7e2b1784cf275d", null ],
    [ "~G4Polyhedra", "d2/d5a/classG4Polyhedra.html#a91559b42b438e22f7d068a85d2668891", null ],
    [ "G4Polyhedra", "d2/d5a/classG4Polyhedra.html#a5b90d1aed427eef46aab10e24a3cd828", null ],
    [ "G4Polyhedra", "d2/d5a/classG4Polyhedra.html#a5699ff2706e1fc53bdce5f9e1a0f049d", null ],
    [ "BoundingLimits", "d2/d5a/classG4Polyhedra.html#af8397c03b36bb264b825d9d34f45216a", null ],
    [ "CalculateExtent", "d2/d5a/classG4Polyhedra.html#a259fecd5acb69af024cb66cdbb7f8c67", null ],
    [ "Clone", "d2/d5a/classG4Polyhedra.html#a4485825ba5099ee9b5e46070f9312c3e", null ],
    [ "ComputeDimensions", "d2/d5a/classG4Polyhedra.html#a7562857a8a55c7858c8a6ad1bd507d0c", null ],
    [ "CopyStuff", "d2/d5a/classG4Polyhedra.html#a415bd914b49f6cc784ca8f8d0970c1cc", null ],
    [ "Create", "d2/d5a/classG4Polyhedra.html#a4b2e9adde5bcaf70be2fa3f1badc6eb0", null ],
    [ "CreatePolyhedron", "d2/d5a/classG4Polyhedra.html#af159bfc03405275816a1f8f74858d051", null ],
    [ "DeleteStuff", "d2/d5a/classG4Polyhedra.html#a9a426bdf3df1eecf4bceac4a787d060a", null ],
    [ "DistanceToIn", "d2/d5a/classG4Polyhedra.html#a3ff27dbb807fd5c4fce2cefb737adffc", null ],
    [ "DistanceToIn", "d2/d5a/classG4Polyhedra.html#ad5c2bbc5eadf817da072fb079833652c", null ],
    [ "GetCorner", "d2/d5a/classG4Polyhedra.html#aaa8d0e0184a8c9f26de2988cb49f763c", null ],
    [ "GetCosEndPhi", "d2/d5a/classG4Polyhedra.html#ab19b04f3764aa172146518048062a827", null ],
    [ "GetCosStartPhi", "d2/d5a/classG4Polyhedra.html#ac1aef71379920cf054eacf4c15e60f70", null ],
    [ "GetCubicVolume", "d2/d5a/classG4Polyhedra.html#a1f1cf88561dda0b52c0c4be46a94253c", null ],
    [ "GetEndPhi", "d2/d5a/classG4Polyhedra.html#a70dbf25e19495ddd82e001618dca4239", null ],
    [ "GetEntityType", "d2/d5a/classG4Polyhedra.html#a54f10893580829738936357da96b2f75", null ],
    [ "GetNumRZCorner", "d2/d5a/classG4Polyhedra.html#a286d7f476f0a6cb43ae7d044ca210cc8", null ],
    [ "GetNumSide", "d2/d5a/classG4Polyhedra.html#a88849e978600af916b5a531a34a6a1ac", null ],
    [ "GetOriginalParameters", "d2/d5a/classG4Polyhedra.html#aa8ee14cd1d208a43b086adca1f743b01", null ],
    [ "GetPointOnSurface", "d2/d5a/classG4Polyhedra.html#a0408d72ada09a99ea890f695f0462ad7", null ],
    [ "GetSinEndPhi", "d2/d5a/classG4Polyhedra.html#aca27578d28b6e759b2c542cf97458dd4", null ],
    [ "GetSinStartPhi", "d2/d5a/classG4Polyhedra.html#a43d054487a075bd34bb8e0c381809c5e", null ],
    [ "GetStartPhi", "d2/d5a/classG4Polyhedra.html#ab58bb6d74c6e2af7aff4bec24fce515d", null ],
    [ "GetSurfaceArea", "d2/d5a/classG4Polyhedra.html#a6bd9676fc55b1b6f02c2899df543ffa3", null ],
    [ "Inside", "d2/d5a/classG4Polyhedra.html#add17a5882a0778594d9d72c94ff20760", null ],
    [ "IsGeneric", "d2/d5a/classG4Polyhedra.html#ac2090bb345db88fa80665848a45a1af9", null ],
    [ "IsOpen", "d2/d5a/classG4Polyhedra.html#a4ca9a771a2a0bfa1738413814e898393", null ],
    [ "operator=", "d2/d5a/classG4Polyhedra.html#a0c4c3d87155112e3d6ed59d0adb4bfd6", null ],
    [ "Reset", "d2/d5a/classG4Polyhedra.html#ab0a6228f944de1f0a4c63f1118b32d22", null ],
    [ "SetOriginalParameters", "d2/d5a/classG4Polyhedra.html#a751ec82861cb446a5e1d3a3a6e6755f4", null ],
    [ "SetOriginalParameters", "d2/d5a/classG4Polyhedra.html#ab3b425762c7cc49cadda63fdd146cb9a", null ],
    [ "SetSurfaceElements", "d2/d5a/classG4Polyhedra.html#ad471ca9e1286e851c00a1e285eb63728", null ],
    [ "StreamInfo", "d2/d5a/classG4Polyhedra.html#a1302ab93385ac09684cf445367a61a5f", null ],
    [ "corners", "d2/d5a/classG4Polyhedra.html#a21db2fbdda1aa6efb0ec67b87761f9e3", null ],
    [ "enclosingCylinder", "d2/d5a/classG4Polyhedra.html#abb97104e8ed8e314d26c774f16f0a663", null ],
    [ "endPhi", "d2/d5a/classG4Polyhedra.html#a61ef1889d5dd4b05ae484dae3fcf2460", null ],
    [ "fElements", "d2/d5a/classG4Polyhedra.html#a723c04a7829914aa7f5408187cd7e977", null ],
    [ "genericPgon", "d2/d5a/classG4Polyhedra.html#a3c642a83b0dfe9d65d5083afb9d46852", null ],
    [ "numCorner", "d2/d5a/classG4Polyhedra.html#a8baa7b549407b7ae2eb9a5cf97a7e018", null ],
    [ "numSide", "d2/d5a/classG4Polyhedra.html#a5535a4c893482586d8e0ea69dca5eb90", null ],
    [ "original_parameters", "d2/d5a/classG4Polyhedra.html#a1addda291b65df0bbc1445abdcad50b6", null ],
    [ "phiIsOpen", "d2/d5a/classG4Polyhedra.html#a79e2626d7aed30e012a1d8b69db5d232", null ],
    [ "startPhi", "d2/d5a/classG4Polyhedra.html#a817fefcec36aea9dcf6dccb8e100d56d", null ]
];