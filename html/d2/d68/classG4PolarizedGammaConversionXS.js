var classG4PolarizedGammaConversionXS =
[
    [ "G4PolarizedGammaConversionXS", "d2/d68/classG4PolarizedGammaConversionXS.html#a4b1882f74a71e24e58193017ac1f5a96", null ],
    [ "~G4PolarizedGammaConversionXS", "d2/d68/classG4PolarizedGammaConversionXS.html#a7edb022e92f6c312e47c32b0150f4e3c", null ],
    [ "G4PolarizedGammaConversionXS", "d2/d68/classG4PolarizedGammaConversionXS.html#ab3a8a248eee249bad0fd60098382176a", null ],
    [ "GetPol2", "d2/d68/classG4PolarizedGammaConversionXS.html#a9efbc543c9c712eec8c243d551cbeed5", null ],
    [ "GetPol3", "d2/d68/classG4PolarizedGammaConversionXS.html#a32a59c767c1d322953dd1e0ae6eaa3cf", null ],
    [ "Initialize", "d2/d68/classG4PolarizedGammaConversionXS.html#aa2feeacdb825ac26820c6d41e3833d43", null ],
    [ "operator=", "d2/d68/classG4PolarizedGammaConversionXS.html#a1224ed97c011505a9da1734b038ae389", null ],
    [ "XSection", "d2/d68/classG4PolarizedGammaConversionXS.html#a5635d0fa2a0864c4c3ad0372b2af72c5", null ],
    [ "fFinalElectronPolarization", "d2/d68/classG4PolarizedGammaConversionXS.html#a5fd2cf9cbddd1311ce71c4d5946b4960", null ],
    [ "fFinalPositronPolarization", "d2/d68/classG4PolarizedGammaConversionXS.html#ad3bc56cfbc7defc975e6386ededfd2f6", null ],
    [ "SCRN", "d2/d68/classG4PolarizedGammaConversionXS.html#a5ff7f80172fdf6c5a3c034af6f5fb0ee", null ]
];