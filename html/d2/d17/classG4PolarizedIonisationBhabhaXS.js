var classG4PolarizedIonisationBhabhaXS =
[
    [ "G4PolarizedIonisationBhabhaXS", "d2/d17/classG4PolarizedIonisationBhabhaXS.html#ab07e58026c54da89fd4f5cbff85f6dca", null ],
    [ "~G4PolarizedIonisationBhabhaXS", "d2/d17/classG4PolarizedIonisationBhabhaXS.html#afb61d562f052ea9de6b6812a5cfcf40d", null ],
    [ "G4PolarizedIonisationBhabhaXS", "d2/d17/classG4PolarizedIonisationBhabhaXS.html#ab70cf1ee922710f12987ae6d2f145b67", null ],
    [ "GetPol2", "d2/d17/classG4PolarizedIonisationBhabhaXS.html#a06c8b117875fb428b11521c755cdc3e5", null ],
    [ "GetPol3", "d2/d17/classG4PolarizedIonisationBhabhaXS.html#a1d08a79f37e1a8f3ccb9619330139769", null ],
    [ "Initialize", "d2/d17/classG4PolarizedIonisationBhabhaXS.html#a9500998dbef855a05f4073d7b49a0be2", null ],
    [ "operator=", "d2/d17/classG4PolarizedIonisationBhabhaXS.html#ada01f5c25faed64b81681e22611bc951", null ],
    [ "TotalXSection", "d2/d17/classG4PolarizedIonisationBhabhaXS.html#a1616fb0902bd6a471e53aa83b6ad6ea3", null ],
    [ "XSection", "d2/d17/classG4PolarizedIonisationBhabhaXS.html#af17ff7d7d9d579dbfc7bf79d2b7b4d0c", null ],
    [ "fPhi0", "d2/d17/classG4PolarizedIonisationBhabhaXS.html#aa3b2c4f0b279eddbcd4aee2e19943435", null ],
    [ "fPhi2", "d2/d17/classG4PolarizedIonisationBhabhaXS.html#ace79bcc305b5dab14ef446603909282f", null ],
    [ "fPhi3", "d2/d17/classG4PolarizedIonisationBhabhaXS.html#a69f14b0a11984428772b8741a65e51d1", null ]
];