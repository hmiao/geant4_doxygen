var classG4AntiProtonField =
[
    [ "G4AntiProtonField", "d2/db2/classG4AntiProtonField.html#a230a626712a1b0cb9caeda7ec802df15", null ],
    [ "~G4AntiProtonField", "d2/db2/classG4AntiProtonField.html#ab67e68663b97fa2a8abe363f540c70c0", null ],
    [ "G4AntiProtonField", "d2/db2/classG4AntiProtonField.html#a655081b82731cb7de2e8e58258f2fdb3", null ],
    [ "GetBarrier", "d2/db2/classG4AntiProtonField.html#ad90f8178ca4307b0d75cbf655adf4bae", null ],
    [ "GetCoeff", "d2/db2/classG4AntiProtonField.html#aab908353e58e48279e852943eafcef47", null ],
    [ "GetField", "d2/db2/classG4AntiProtonField.html#aff802093535498678cdb5fc08ccbf3b0", null ],
    [ "operator!=", "d2/db2/classG4AntiProtonField.html#a4f2eb697df73014c1bd9b207fb2a3c4e", null ],
    [ "operator=", "d2/db2/classG4AntiProtonField.html#ad09a9508c8b3abb7a55915033ec1334b", null ],
    [ "operator==", "d2/db2/classG4AntiProtonField.html#a456a13fa00ffd52de9cb214b6cf9f959", null ],
    [ "theCoeff", "d2/db2/classG4AntiProtonField.html#addbd64ad092edb1a882eaaa559b3d877", null ]
];