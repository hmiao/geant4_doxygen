var classG4hZiegler1985p =
[
    [ "G4hZiegler1985p", "d2/d3c/classG4hZiegler1985p.html#adb687b6106fd90fc3174c1a401cf8fa0", null ],
    [ "~G4hZiegler1985p", "d2/d3c/classG4hZiegler1985p.html#a0fedc3457f2494dd2282c512bf5772fe", null ],
    [ "ElectronicStoppingPower", "d2/d3c/classG4hZiegler1985p.html#acb83a75de1e5570b2e71ba81a2b8e39c", null ],
    [ "HasMaterial", "d2/d3c/classG4hZiegler1985p.html#af1e8eb086c0714911fc3577cca4b3175", null ],
    [ "StoppingPower", "d2/d3c/classG4hZiegler1985p.html#ae6466651aea27bb13a3789135716c2cd", null ],
    [ "a", "d2/d3c/classG4hZiegler1985p.html#af7b4745cfccc833dcad5d59bfae95337", null ],
    [ "protonMassAMU", "d2/d3c/classG4hZiegler1985p.html#a055ff9156cf1b54e9f22ed0bfee22ee2", null ]
];