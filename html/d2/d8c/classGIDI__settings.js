var classGIDI__settings =
[
    [ "GIDI_settings", "d2/d8c/classGIDI__settings.html#a8f807af85b122f9a64f72bdf06330050", null ],
    [ "~GIDI_settings", "d2/d8c/classGIDI__settings.html#ad0652ef92cd0476ad4fd3050da777e22", null ],
    [ "addParticle", "d2/d8c/classGIDI__settings.html#aa5861a5868db8eee3091292a3470f5f2", null ],
    [ "eraseParticle", "d2/d8c/classGIDI__settings.html#a7260895e46f21d8c3bcd8a42edd0dc4d", null ],
    [ "getParticle", "d2/d8c/classGIDI__settings.html#a1691110aa8c6bafa7b49b766180d8676", null ],
    [ "releaseMemory", "d2/d8c/classGIDI__settings.html#a51dc826566410fe86d943dea421ad978", null ],
    [ "mParticles", "d2/d8c/classGIDI__settings.html#adb01a088f3fc3c9c2829b5c59ad5f022", null ]
];