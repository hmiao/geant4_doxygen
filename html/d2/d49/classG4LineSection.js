var classG4LineSection =
[
    [ "G4LineSection", "d2/d49/classG4LineSection.html#a9e5017280361d9211b6c048c9c2bb4c2", null ],
    [ "Dist", "d2/d49/classG4LineSection.html#aaf6eb7f364788d22b140ee4247dd0a0c", null ],
    [ "Distline", "d2/d49/classG4LineSection.html#ad95380de7b26e5ffbe2aa6f71ab11b93", null ],
    [ "GetABdistanceSq", "d2/d49/classG4LineSection.html#a117665a49ac2ba83e25f404991840a7a", null ],
    [ "EndpointA", "d2/d49/classG4LineSection.html#afeb2303c4fdfc0c0282780daa922a711", null ],
    [ "fABdistanceSq", "d2/d49/classG4LineSection.html#ae774bcd8711c37e5c854a2c44962972c", null ],
    [ "VecAtoB", "d2/d49/classG4LineSection.html#a10c30ab2ef52b4f5b047fa875702f1e5", null ]
];