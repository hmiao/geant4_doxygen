var classG4ParticleHPInelasticBaseFS =
[
    [ "G4ParticleHPInelasticBaseFS", "d2/da9/classG4ParticleHPInelasticBaseFS.html#aeac1f3585a0ed112f9c572261cc013fd", null ],
    [ "~G4ParticleHPInelasticBaseFS", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a698b77caf29b04114d71da3880a0d608", null ],
    [ "ApplyYourself", "d2/da9/classG4ParticleHPInelasticBaseFS.html#aab07ee33698483a4de0fc1db3c1dcd5c", null ],
    [ "BaseApply", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a03b9f96a46bb076f23634497a50769fc", null ],
    [ "GetXsec", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a1d171b8edf45ea36a62c544ade57003f", null ],
    [ "GetXsec", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a90b6767122e8a3bccf8eb86640a3e5f2", null ],
    [ "Init", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a330b67b8a54d5991f4a9f0c8aa47eada", null ],
    [ "InitGammas", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a21113eaeab1a68132da7e171f4d6c25d", null ],
    [ "New", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a5392a6212126cc45cb84465c93d6e4db", null ],
    [ "gammaPath", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a8dfe37ff3718e3d790c785f0e5b5fe1d", null ],
    [ "Qvalue", "d2/da9/classG4ParticleHPInelasticBaseFS.html#acc9f6c5ea73296edf2a744a7b2ae17ab", null ],
    [ "theAngularDistribution", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a6ad289b76de57b6d17e4678b101e086b", null ],
    [ "theEnergyAngData", "d2/da9/classG4ParticleHPInelasticBaseFS.html#aa126da0a80a33108066e8cba5e5aeb6f", null ],
    [ "theEnergyDistribution", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a3478615518c1b9065a35088077f8c977", null ],
    [ "theFinalStatePhotons", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a44bb65a8a040fc551207f76534bbffe0", null ],
    [ "theGammas", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a76a5c3f5d784325678e5bfad24f61921", null ],
    [ "theNuclearMassDifference", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a48b10a6f930234b0ab52c228a21e64c4", null ],
    [ "theXsection", "d2/da9/classG4ParticleHPInelasticBaseFS.html#a6b0f4dcac1e029a76f22bd1d16e270a7", null ]
];