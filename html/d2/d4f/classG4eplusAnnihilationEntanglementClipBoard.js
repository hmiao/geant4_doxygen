var classG4eplusAnnihilationEntanglementClipBoard =
[
    [ "G4eplusAnnihilationEntanglementClipBoard", "d2/d4f/classG4eplusAnnihilationEntanglementClipBoard.html#a12a52510fcf2f8d318fc7db83b0b60f0", null ],
    [ "~G4eplusAnnihilationEntanglementClipBoard", "d2/d4f/classG4eplusAnnihilationEntanglementClipBoard.html#ac173f91da3047bdf8287c53d209d7656", null ],
    [ "GetComptonCosTheta1", "d2/d4f/classG4eplusAnnihilationEntanglementClipBoard.html#a2c4af410e34951a986a88f29c14e8694", null ],
    [ "GetComptonPhi1", "d2/d4f/classG4eplusAnnihilationEntanglementClipBoard.html#a25e5475a71db95aa76cdc9fc246ee874", null ],
    [ "SetComptonCosTheta1", "d2/d4f/classG4eplusAnnihilationEntanglementClipBoard.html#af4114fad877a76303b837d9843d2d68f", null ],
    [ "SetComptonPhi1", "d2/d4f/classG4eplusAnnihilationEntanglementClipBoard.html#a4a5cca6a3383d32b2ca106d62d5c3c62", null ],
    [ "fComptonCosTheta1", "d2/d4f/classG4eplusAnnihilationEntanglementClipBoard.html#a990c1c1b9893ccfee3e4e614a26fee90", null ],
    [ "fComptonPhi1", "d2/d4f/classG4eplusAnnihilationEntanglementClipBoard.html#adc1d9e5470840b610a0105c4ee484e1b", null ]
];