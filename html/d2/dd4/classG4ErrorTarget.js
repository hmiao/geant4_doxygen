var classG4ErrorTarget =
[
    [ "G4ErrorTarget", "d2/dd4/classG4ErrorTarget.html#ad847df5e9d7f67e0ac2c2b40a1aa2d26", null ],
    [ "~G4ErrorTarget", "d2/dd4/classG4ErrorTarget.html#affc79b60d15b59061f521a00e728dfa3", null ],
    [ "Dump", "d2/dd4/classG4ErrorTarget.html#a1a947954352eed2231e5c6736f4ea6e9", null ],
    [ "GetDistanceFromPoint", "d2/dd4/classG4ErrorTarget.html#a439296ac5904e51e37f941784aa5f8fe", null ],
    [ "GetDistanceFromPoint", "d2/dd4/classG4ErrorTarget.html#ac29b8795472c46203890bc5ce836f96a", null ],
    [ "GetType", "d2/dd4/classG4ErrorTarget.html#aa6494e88684996872f4f47a439f930a4", null ],
    [ "TargetReached", "d2/dd4/classG4ErrorTarget.html#a7b76a069402d105f3f955b9cdb7702f4", null ],
    [ "theType", "d2/dd4/classG4ErrorTarget.html#a959eec5850cc60f50704cf88309580b5", null ]
];