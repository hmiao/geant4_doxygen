var classG4BinaryProtonBuilder =
[
    [ "G4BinaryProtonBuilder", "d2/d98/classG4BinaryProtonBuilder.html#a720f76282370f402bde93837b4a8078f", null ],
    [ "~G4BinaryProtonBuilder", "d2/d98/classG4BinaryProtonBuilder.html#af9ecf6be06bf67319aafa4b1ef24ba29", null ],
    [ "Build", "d2/d98/classG4BinaryProtonBuilder.html#a9874fb50c1fe95ec048cb41341c16493", null ],
    [ "Build", "d2/d98/classG4BinaryProtonBuilder.html#a7a534415d8d1a3953326b85a9936955e", null ],
    [ "Build", "d2/d98/classG4BinaryProtonBuilder.html#ac2a255a9ebb0563ba348ea132f25de92", null ],
    [ "Build", "d2/d98/classG4BinaryProtonBuilder.html#a7d7a3fe1fc3f798f594a92694411bb49", null ],
    [ "SetMaxEnergy", "d2/d98/classG4BinaryProtonBuilder.html#a5d025c58d0059f385a62fb61baea01d6", null ],
    [ "SetMinEnergy", "d2/d98/classG4BinaryProtonBuilder.html#abeb5659f8ef01b72a511c822f013f9a6", null ],
    [ "theMax", "d2/d98/classG4BinaryProtonBuilder.html#a258abe137d5bc0fdee508380d6597481", null ],
    [ "theMin", "d2/d98/classG4BinaryProtonBuilder.html#a0b2ab3fe240751715421f297ab1f52f1", null ],
    [ "theModel", "d2/d98/classG4BinaryProtonBuilder.html#ab69d939a324d732b945cee9e3ae776c2", null ]
];