var classG4PhysicalVolumeModel_1_1G4PhysicalVolumeModelTouchable =
[
    [ "G4PhysicalVolumeModelTouchable", "d2/d98/classG4PhysicalVolumeModel_1_1G4PhysicalVolumeModelTouchable.html#adf66c3260e74e4693001ed635164c2ac", null ],
    [ "GetHistoryDepth", "d2/d98/classG4PhysicalVolumeModel_1_1G4PhysicalVolumeModelTouchable.html#a896ef46fbc21d6cb5892b7d515e07ce5", null ],
    [ "GetReplicaNumber", "d2/d98/classG4PhysicalVolumeModel_1_1G4PhysicalVolumeModelTouchable.html#aa86409ec001d9838b81e80d9c9abd564", null ],
    [ "GetRotation", "d2/d98/classG4PhysicalVolumeModel_1_1G4PhysicalVolumeModelTouchable.html#ab45d99163c70b966b11b1ae0ce40d3a8", null ],
    [ "GetSolid", "d2/d98/classG4PhysicalVolumeModel_1_1G4PhysicalVolumeModelTouchable.html#a03f189db3e05b6d92066ba7ec3946289", null ],
    [ "GetTranslation", "d2/d98/classG4PhysicalVolumeModel_1_1G4PhysicalVolumeModelTouchable.html#ae5b6f72980702604f9270bfd6bb4406c", null ],
    [ "GetVolume", "d2/d98/classG4PhysicalVolumeModel_1_1G4PhysicalVolumeModelTouchable.html#a1642c99af1bd61068382cb79e3ee469b", null ],
    [ "fFullPVPath", "d2/d98/classG4PhysicalVolumeModel_1_1G4PhysicalVolumeModelTouchable.html#a39cfd675efe5da936c84587e65e3dc08", null ]
];