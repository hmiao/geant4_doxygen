var classG4VScatteringCollision =
[
    [ "G4VScatteringCollision", "d2/d66/classG4VScatteringCollision.html#a5f3bf2683695d1cebd9a58adb1a26053", null ],
    [ "~G4VScatteringCollision", "d2/d66/classG4VScatteringCollision.html#a9421743a8d6095fbd66658e527fafa3c", null ],
    [ "G4VScatteringCollision", "d2/d66/classG4VScatteringCollision.html#aad17058e60a64012e1cb5324c78110ce", null ],
    [ "BrWigInt0", "d2/d66/classG4VScatteringCollision.html#a38fd1e554ba1fee4397f5a4a5bcac923", null ],
    [ "BrWigInt1", "d2/d66/classG4VScatteringCollision.html#a00a6a9ec65276c1fa7ebb38ecfa08337", null ],
    [ "BrWigInv", "d2/d66/classG4VScatteringCollision.html#ab3f38b590b570db0bff0440ba49f2bc0", null ],
    [ "establish_G4MT_TLS_G4VScatteringCollision", "d2/d66/classG4VScatteringCollision.html#a3d45d419bd38e95fbfa7f13021e1a7d8", null ],
    [ "FinalState", "d2/d66/classG4VScatteringCollision.html#aba4e60504b56ca3ab54f2b45bbfddcf6", null ],
    [ "GetAngularDistribution", "d2/d66/classG4VScatteringCollision.html#a61f78d9f0675dfb8f7d7adef858bbf98", null ],
    [ "GetOutgoingParticles", "d2/d66/classG4VScatteringCollision.html#af4192657ff7e1c37b8a6f74a74e7f5dd", null ],
    [ "operator!=", "d2/d66/classG4VScatteringCollision.html#a354627786064acd1fde75f35ae6cc1c2", null ],
    [ "operator=", "d2/d66/classG4VScatteringCollision.html#a7a9e0b1650a118e0a7162529624d0d83", null ],
    [ "operator==", "d2/d66/classG4VScatteringCollision.html#a75899bd277732cfc21cd266e6e62e086", null ],
    [ "SampleResonanceMass", "d2/d66/classG4VScatteringCollision.html#a873366eab9baf8e5e7d967a0890fb230", null ],
    [ "theAngularDistribution", "d2/d66/classG4VScatteringCollision.html#a31fabc770ccc8a7b5a10b65a5ae383dc", null ]
];