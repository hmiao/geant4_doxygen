var classG4DNAReactionTypeManager =
[
    [ "G4DNAReactionTypeManager", "d2/df1/classG4DNAReactionTypeManager.html#ac0ea5df1dc32cd550148d461b1f837e6", null ],
    [ "~G4DNAReactionTypeManager", "d2/df1/classG4DNAReactionTypeManager.html#af9aedcdd01d654a8b7d631be14eee337", null ],
    [ "Clear", "d2/df1/classG4DNAReactionTypeManager.html#a4afd93af0945f329ccbdf64f11bc7046", null ],
    [ "GetReactionTypeByID", "d2/df1/classG4DNAReactionTypeManager.html#a9d8235b88d43e1dd86de9174243b9144", null ],
    [ "GetReactionTypeTable", "d2/df1/classG4DNAReactionTypeManager.html#a3ab26d45c33a19e788d03b2e3875c920", null ],
    [ "SetReactionTypeTable", "d2/df1/classG4DNAReactionTypeManager.html#a167844640657bb55e68464cdb5273d45", null ],
    [ "SetTypeTableByID", "d2/df1/classG4DNAReactionTypeManager.html#a1f7fa63e5a69c7f02a9a8702c38dca1d", null ],
    [ "fReactionTypeByID", "d2/df1/classG4DNAReactionTypeManager.html#a7706f46d94ef1b4900f4e8a18400dc4e", null ],
    [ "fReactionTypeTable", "d2/df1/classG4DNAReactionTypeManager.html#a7af2d9c050b7c6da10f7c09cf3697b7f", null ]
];