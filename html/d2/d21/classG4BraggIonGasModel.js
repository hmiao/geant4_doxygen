var classG4BraggIonGasModel =
[
    [ "G4BraggIonGasModel", "d2/d21/classG4BraggIonGasModel.html#a13988129138389f49e37000baa80c73c", null ],
    [ "~G4BraggIonGasModel", "d2/d21/classG4BraggIonGasModel.html#a6d9c3ff8c5a738835c0ca13526f5d1b5", null ],
    [ "G4BraggIonGasModel", "d2/d21/classG4BraggIonGasModel.html#ac41cf7911f57f86ae27434cfff568946", null ],
    [ "ChargeSquareRatio", "d2/d21/classG4BraggIonGasModel.html#a746e3382e4edd7def043a114568a37af", null ],
    [ "GetParticleCharge", "d2/d21/classG4BraggIonGasModel.html#a90fffa93817b45c0c306145ac4d760a8", null ],
    [ "operator=", "d2/d21/classG4BraggIonGasModel.html#a6fb6eb7771f20e19b8bdf9fab9f89aa5", null ],
    [ "currentCharge", "d2/d21/classG4BraggIonGasModel.html#a538aa1d96967475f94124c1ec391ad9d", null ]
];