var classG4HadronPhysicsQGSP__BIC__AllHP =
[
    [ "G4HadronPhysicsQGSP_BIC_AllHP", "d2/df9/classG4HadronPhysicsQGSP__BIC__AllHP.html#a3b0734cabe157c7beb589b0158525403", null ],
    [ "G4HadronPhysicsQGSP_BIC_AllHP", "d2/df9/classG4HadronPhysicsQGSP__BIC__AllHP.html#a14ba4e2b7353fb486306703b02c7875c", null ],
    [ "~G4HadronPhysicsQGSP_BIC_AllHP", "d2/df9/classG4HadronPhysicsQGSP__BIC__AllHP.html#a080eabd2edd9bb7c8fa9ebf4c0fbd4be", null ],
    [ "G4HadronPhysicsQGSP_BIC_AllHP", "d2/df9/classG4HadronPhysicsQGSP__BIC__AllHP.html#ab333efad9b3ce074fa4122ed7ab8f178", null ],
    [ "operator=", "d2/df9/classG4HadronPhysicsQGSP__BIC__AllHP.html#aee14f49732351d7150c932e549cd61db", null ],
    [ "Proton", "d2/df9/classG4HadronPhysicsQGSP__BIC__AllHP.html#a28bd8f279ece80cb3ffa2657d3272515", null ],
    [ "maxHP_proton", "d2/df9/classG4HadronPhysicsQGSP__BIC__AllHP.html#a5c19fdcaedcbb6accbe152ca93525fc9", null ]
];