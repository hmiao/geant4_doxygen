var classG4ParamExpTwoBodyAngDst =
[
    [ "G4ParamExpTwoBodyAngDst", "d2/df9/classG4ParamExpTwoBodyAngDst.html#ae2b8474f944af5160df8033a66b59978", null ],
    [ "~G4ParamExpTwoBodyAngDst", "d2/df9/classG4ParamExpTwoBodyAngDst.html#a46fd8621ab57c73a96e70726e167060c", null ],
    [ "GetCosTheta", "d2/df9/classG4ParamExpTwoBodyAngDst.html#a22fc4d763cbbd99e4c836a78b98423b6", null ],
    [ "angleCut", "d2/df9/classG4ParamExpTwoBodyAngDst.html#a66c2aa2aec4f5b626cddb9a186297b6c", null ],
    [ "cosScale", "d2/df9/classG4ParamExpTwoBodyAngDst.html#acb184e86823a83c21758ccc3747dd776", null ],
    [ "interpolator", "d2/df9/classG4ParamExpTwoBodyAngDst.html#ad60f437a4c58b926ec10c046b5b2e42d", null ],
    [ "labKE", "d2/df9/classG4ParamExpTwoBodyAngDst.html#a28d9cadef9be5ab7307cc2b00f641e53", null ],
    [ "largeScale", "d2/df9/classG4ParamExpTwoBodyAngDst.html#a209aee4f5c802feabd46f01b4728e0b7", null ],
    [ "smallScale", "d2/df9/classG4ParamExpTwoBodyAngDst.html#a1ea4a71df3805d6068a4d1f8d6075cac", null ]
];