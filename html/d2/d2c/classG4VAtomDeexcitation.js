var classG4VAtomDeexcitation =
[
    [ "G4VAtomDeexcitation", "d2/d2c/classG4VAtomDeexcitation.html#a1c1387ba49e1b890e45d45139ca440dd", null ],
    [ "~G4VAtomDeexcitation", "d2/d2c/classG4VAtomDeexcitation.html#ad4c9f03dae3eeedb19a81b591dbcbc3e", null ],
    [ "G4VAtomDeexcitation", "d2/d2c/classG4VAtomDeexcitation.html#ab6699e0d07343c289b2791bfd3496b1f", null ],
    [ "AlongStepDeexcitation", "d2/d2c/classG4VAtomDeexcitation.html#ad757ef5d2d05f83f5e0c9a3b34b21b9d", null ],
    [ "CheckAugerActiveRegion", "d2/d2c/classG4VAtomDeexcitation.html#a4a889401ad53ce74b77838e69f15c5b9", null ],
    [ "CheckDeexcitationActiveRegion", "d2/d2c/classG4VAtomDeexcitation.html#a016085f6ccdc85ee37811f3645068634", null ],
    [ "ComputeShellIonisationCrossSectionPerAtom", "d2/d2c/classG4VAtomDeexcitation.html#a3fbb31ec43e49bfb2fa75ce2935aabbc", null ],
    [ "GenerateParticles", "d2/d2c/classG4VAtomDeexcitation.html#a6972c3cee0c28ef19153834ef11aa096", null ],
    [ "GenerateParticles", "d2/d2c/classG4VAtomDeexcitation.html#a6d050fc4614801e2fc1ba9630dc02220", null ],
    [ "GetAtomicShell", "d2/d2c/classG4VAtomDeexcitation.html#a385c54a1e02aa1769763371e7493c8e6", null ],
    [ "GetListOfActiveAtoms", "d2/d2c/classG4VAtomDeexcitation.html#a01f0af7158ae91279e80914a9aeb09e1", null ],
    [ "GetName", "d2/d2c/classG4VAtomDeexcitation.html#a4871e0b363633114f259d3f01ba8b18b", null ],
    [ "GetShellIonisationCrossSectionPerAtom", "d2/d2c/classG4VAtomDeexcitation.html#adfa6f78464364725da6ae6ec30706e5b", null ],
    [ "GetVerboseLevel", "d2/d2c/classG4VAtomDeexcitation.html#aa365d739dd7979b3129bbe8466291803", null ],
    [ "InitialiseAtomicDeexcitation", "d2/d2c/classG4VAtomDeexcitation.html#aa50e4f27f75d5607c8cb31d46601b121", null ],
    [ "InitialiseForExtraAtom", "d2/d2c/classG4VAtomDeexcitation.html#a5820918ea42a3a43df6f513dc18b4c51", null ],
    [ "InitialiseForNewRun", "d2/d2c/classG4VAtomDeexcitation.html#af1a76eda54f2362920236d3455c0fc43", null ],
    [ "IsAugerActive", "d2/d2c/classG4VAtomDeexcitation.html#a252597e344d415261e7c2b086e273ddb", null ],
    [ "IsAugerCascadeActive", "d2/d2c/classG4VAtomDeexcitation.html#ad8fa1d722305e2bccebcffdc61496000", null ],
    [ "IsFluoActive", "d2/d2c/classG4VAtomDeexcitation.html#aa070132bc8dcf4aa0000e96c1374125c", null ],
    [ "IsPIXEActive", "d2/d2c/classG4VAtomDeexcitation.html#aef3b1a7374beaeeac81b4a86b8ce934e", null ],
    [ "operator=", "d2/d2c/classG4VAtomDeexcitation.html#a8e37474129391c5a4c5d28eeedbf2946", null ],
    [ "SetAuger", "d2/d2c/classG4VAtomDeexcitation.html#a1b26c71f14d82060a8b1b908c094c02e", null ],
    [ "SetAugerCascade", "d2/d2c/classG4VAtomDeexcitation.html#ab8a6ec0efa02225595143d3882e8b899", null ],
    [ "SetDeexcitationActiveRegion", "d2/d2c/classG4VAtomDeexcitation.html#aed0720c488a0bbe2ef05d72031bcd832", null ],
    [ "SetFluo", "d2/d2c/classG4VAtomDeexcitation.html#a29e86068831919ad5b6384c2c83f5df5", null ],
    [ "SetPIXE", "d2/d2c/classG4VAtomDeexcitation.html#a09f401fe5bf9818db688f39e3b9a8e8a", null ],
    [ "SetVerboseLevel", "d2/d2c/classG4VAtomDeexcitation.html#a999294740c2b058cc1609651f9c7928e", null ],
    [ "activeAugerMedia", "d2/d2c/classG4VAtomDeexcitation.html#a2a35cfe0732ad70cbdddfc03c3190fcb", null ],
    [ "activeDeexcitationMedia", "d2/d2c/classG4VAtomDeexcitation.html#afae902124276e42bedb8711a2b33d671", null ],
    [ "activePIXEMedia", "d2/d2c/classG4VAtomDeexcitation.html#a6a07412465f565ae54232c57ecb2d39a", null ],
    [ "activeRegions", "d2/d2c/classG4VAtomDeexcitation.html#a14f366869eb7e49b19c595742703c119", null ],
    [ "activeZ", "d2/d2c/classG4VAtomDeexcitation.html#a771f05b639c712e3999683db2247616a", null ],
    [ "AugerRegions", "d2/d2c/classG4VAtomDeexcitation.html#a2c45b494b6a565fe7c4907b30accd80d", null ],
    [ "deRegions", "d2/d2c/classG4VAtomDeexcitation.html#a3e33616205dd646b856b0670bb5267e8", null ],
    [ "flagAuger", "d2/d2c/classG4VAtomDeexcitation.html#a2aa9e47ad5955ee1ef92c0760ac90aa2", null ],
    [ "flagPIXE", "d2/d2c/classG4VAtomDeexcitation.html#a733d16d02e81b4acfc7766e2919d3941", null ],
    [ "gamma", "d2/d2c/classG4VAtomDeexcitation.html#aedc06ee47608226e7c000146f822e9bd", null ],
    [ "ignoreCuts", "d2/d2c/classG4VAtomDeexcitation.html#ac36923e44e5228fc0bae34d8affbc2c2", null ],
    [ "isActive", "d2/d2c/classG4VAtomDeexcitation.html#af643252b2fb19acccd36b61cdc6b33be", null ],
    [ "isActiveLocked", "d2/d2c/classG4VAtomDeexcitation.html#a00ea5a93215d0cdcb3e701fb9489285d", null ],
    [ "isAugerLocked", "d2/d2c/classG4VAtomDeexcitation.html#a8ded8e8b423f68bf5267e4dd2dd9c4cf", null ],
    [ "isPIXELocked", "d2/d2c/classG4VAtomDeexcitation.html#a04b13da81f80c9f101d5cade72731695", null ],
    [ "name", "d2/d2c/classG4VAtomDeexcitation.html#a18718f92a465b07d734df932bba9267e", null ],
    [ "nCouples", "d2/d2c/classG4VAtomDeexcitation.html#aa6d791feefd0aa2ab154b92e274571df", null ],
    [ "PIXERegions", "d2/d2c/classG4VAtomDeexcitation.html#a4c49d8546580f3f38fce7c73bd9b6af2", null ],
    [ "theCoupleTable", "d2/d2c/classG4VAtomDeexcitation.html#afe7a66c0f85acb55736ea23b8db1e986", null ],
    [ "vdyn", "d2/d2c/classG4VAtomDeexcitation.html#aa936e9de94f27f73afd1697bb3f0faaf", null ],
    [ "verbose", "d2/d2c/classG4VAtomDeexcitation.html#ac5093a9abb6dd58104deb2e600d55e03", null ]
];