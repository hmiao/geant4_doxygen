var classG4QGSPLundStrFragmProtonBuilder =
[
    [ "G4QGSPLundStrFragmProtonBuilder", "d2/d73/classG4QGSPLundStrFragmProtonBuilder.html#a96bc38040989db16fb7c4957cc2416f7", null ],
    [ "~G4QGSPLundStrFragmProtonBuilder", "d2/d73/classG4QGSPLundStrFragmProtonBuilder.html#a2a5e401741160a56107e7ac2b384eeac", null ],
    [ "Build", "d2/d73/classG4QGSPLundStrFragmProtonBuilder.html#a5a66f49999f6a41e8abf7d87882cea90", null ],
    [ "Build", "d2/d73/classG4QGSPLundStrFragmProtonBuilder.html#a7a534415d8d1a3953326b85a9936955e", null ],
    [ "Build", "d2/d73/classG4QGSPLundStrFragmProtonBuilder.html#a41a68bd3705b33f7b4ef8f3b1a53b95f", null ],
    [ "Build", "d2/d73/classG4QGSPLundStrFragmProtonBuilder.html#a7d7a3fe1fc3f798f594a92694411bb49", null ],
    [ "SetMinEnergy", "d2/d73/classG4QGSPLundStrFragmProtonBuilder.html#a2352f32c9fb8e5bef8fe75a327cdcc63", null ],
    [ "theMin", "d2/d73/classG4QGSPLundStrFragmProtonBuilder.html#aef5b6770b4da97b84c1b5b146a5135a4", null ],
    [ "theModel", "d2/d73/classG4QGSPLundStrFragmProtonBuilder.html#a6708328e28802b7d67e48ed91dc5c45e", null ]
];