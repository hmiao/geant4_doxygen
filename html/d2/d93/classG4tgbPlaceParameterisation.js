var classG4tgbPlaceParameterisation =
[
    [ "G4tgbPlaceParameterisation", "d2/d93/classG4tgbPlaceParameterisation.html#a13314632702615850419795f3a760dfc", null ],
    [ "~G4tgbPlaceParameterisation", "d2/d93/classG4tgbPlaceParameterisation.html#a4c784a7eb5098bc3800f0441ebde8dd6", null ],
    [ "CheckNExtraData", "d2/d93/classG4tgbPlaceParameterisation.html#a5d15bf80746f7f5b40bccb9892e57d37", null ],
    [ "ComputeTransformation", "d2/d93/classG4tgbPlaceParameterisation.html#aac7357d7d436f95e361ceb17bfc68f8b", null ],
    [ "GetAxis", "d2/d93/classG4tgbPlaceParameterisation.html#a4cbdce3777af07430e21b0acc8ab56a7", null ],
    [ "GetNCopies", "d2/d93/classG4tgbPlaceParameterisation.html#a0b850c1192cb56e3cde4c38147b5e5a9", null ],
    [ "theAxis", "d2/d93/classG4tgbPlaceParameterisation.html#a355d1ae15c0cb768f757c9fb0bd15c92", null ],
    [ "theNCopies", "d2/d93/classG4tgbPlaceParameterisation.html#a1794d7a1741b2fb714be52935104f8dc", null ],
    [ "theRotationMatrix", "d2/d93/classG4tgbPlaceParameterisation.html#a947b64edfce0e787eb3c194b60c4490f", null ],
    [ "theTranslation", "d2/d93/classG4tgbPlaceParameterisation.html#aece36ae7c21402d34571a92d137539ac", null ]
];