var classG4Win32 =
[
    [ "~G4Win32", "d2/d0c/classG4Win32.html#ac3d4477dfb15fda54a0a85ab8904c4bf", null ],
    [ "G4Win32", "d2/d0c/classG4Win32.html#a7c8411f613c48d8f1536286c26b1c7ba", null ],
    [ "DispatchWin32Event", "d2/d0c/classG4Win32.html#a45f6124b076d6b88ceb49087728e7828", null ],
    [ "FlushAndWaitExecution", "d2/d0c/classG4Win32.html#af112f3b1ece48aa1c59e7570d8bfed2d", null ],
    [ "GetEvent", "d2/d0c/classG4Win32.html#a2ffafbde76c88204a61f726f8af0dd86", null ],
    [ "getInstance", "d2/d0c/classG4Win32.html#a5d6ae8892964ff42943c3b19adda8d9d", null ],
    [ "Inited", "d2/d0c/classG4Win32.html#aabdba84737911876ab351cc3a8ce8bf3", null ],
    [ "instance", "d2/d0c/classG4Win32.html#a93bb7028512ba535a8e0299853380f57", null ]
];