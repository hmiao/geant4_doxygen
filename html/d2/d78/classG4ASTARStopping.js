var classG4ASTARStopping =
[
    [ "G4ASTARStopping", "d2/d78/classG4ASTARStopping.html#a26313e50cc4fe519b15bb09aa369f74c", null ],
    [ "~G4ASTARStopping", "d2/d78/classG4ASTARStopping.html#a34da06483639c4dde7586c68b84ef2cb", null ],
    [ "G4ASTARStopping", "d2/d78/classG4ASTARStopping.html#af6eb887f581bcfd5eb3239f080dd6515", null ],
    [ "AddData", "d2/d78/classG4ASTARStopping.html#a987bb0da4adee0084c0efb4072ff2b9e", null ],
    [ "FindData", "d2/d78/classG4ASTARStopping.html#a5023ad5e7964c040c644a5f5e009fa25", null ],
    [ "GetElectronicDEDX", "d2/d78/classG4ASTARStopping.html#a818c52ca39b85c353e8aff9e49243ed7", null ],
    [ "GetElectronicDEDX", "d2/d78/classG4ASTARStopping.html#ad4cba002354bf83880609072d40aa38e", null ],
    [ "GetIndex", "d2/d78/classG4ASTARStopping.html#a320483c3ed6dc09dbe8c79c5951b0b21", null ],
    [ "GetIndex", "d2/d78/classG4ASTARStopping.html#a4243f2e3f00d34bad9b470d1db2b9304", null ],
    [ "Initialise", "d2/d78/classG4ASTARStopping.html#a59119d673ab69ad4e249cfe5daabfccb", null ],
    [ "operator=", "d2/d78/classG4ASTARStopping.html#a263d76838ee06dc3e54e4fcb00eb6fa8", null ],
    [ "PrintWarning", "d2/d78/classG4ASTARStopping.html#a3570933cb9febc264741ec51e22b81c3", null ],
    [ "emin", "d2/d78/classG4ASTARStopping.html#a94951f87e875227e093567fc1955eda6", null ],
    [ "materials", "d2/d78/classG4ASTARStopping.html#a796abb0548cb507e33b5f76af0b7a12d", null ],
    [ "nvectors", "d2/d78/classG4ASTARStopping.html#afa1f5f03c964db6d3a90b9383df85776", null ],
    [ "sdata", "d2/d78/classG4ASTARStopping.html#a8d422ec37fa1dba1e4abb79343da37c0", null ]
];