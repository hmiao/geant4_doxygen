var classG4VhShellCrossSection =
[
    [ "G4VhShellCrossSection", "d2/d80/classG4VhShellCrossSection.html#a38ff21f14f77a9b7b4f7af6465964329", null ],
    [ "~G4VhShellCrossSection", "d2/d80/classG4VhShellCrossSection.html#a54fcc349d0ca675831dac6c1444d6bc3", null ],
    [ "G4VhShellCrossSection", "d2/d80/classG4VhShellCrossSection.html#a18b6a3bdf641a7bec52c21a48e262c52", null ],
    [ "CrossSection", "d2/d80/classG4VhShellCrossSection.html#a87f8b01f695dca1377a8286a291ccf0d", null ],
    [ "GetCrossSection", "d2/d80/classG4VhShellCrossSection.html#a9e709fab2ab719eb40f028ba82b94dde", null ],
    [ "GetName", "d2/d80/classG4VhShellCrossSection.html#ac643153215aba1806920f289413e44a8", null ],
    [ "operator=", "d2/d80/classG4VhShellCrossSection.html#ae21a6ad361c0ef03692e721b98bd107a", null ],
    [ "Probabilities", "d2/d80/classG4VhShellCrossSection.html#a797391814bd6414b665b20d786bc8b50", null ],
    [ "SelectRandomShell", "d2/d80/classG4VhShellCrossSection.html#ac762c9f5c9832c14ae854981d01097d3", null ],
    [ "SetTotalCS", "d2/d80/classG4VhShellCrossSection.html#a05f7a281e8c3d5a9561212a48fa4cebf", null ],
    [ "name", "d2/d80/classG4VhShellCrossSection.html#a16616a89428d396394aa3cd2fc361c53", null ]
];