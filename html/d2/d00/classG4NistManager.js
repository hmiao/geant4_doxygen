var classG4NistManager =
[
    [ "~G4NistManager", "d2/d00/classG4NistManager.html#ae04dd752ef51cee766e39508c60d4654", null ],
    [ "G4NistManager", "d2/d00/classG4NistManager.html#abff0041482cd8c3988c07195203b6e21", null ],
    [ "BuildMaterialWithNewDensity", "d2/d00/classG4NistManager.html#a90d0fe1c99b47baa2b1c2432d9bfcaa2", null ],
    [ "ConstructNewGasMaterial", "d2/d00/classG4NistManager.html#a01377d26643ce993283928c04e141eb1", null ],
    [ "ConstructNewIdealGasMaterial", "d2/d00/classG4NistManager.html#a4186e0b4d70bf6eb342e1975b0c4bdfe", null ],
    [ "ConstructNewMaterial", "d2/d00/classG4NistManager.html#a77e32907351805279f53e8ddb38e96bb", null ],
    [ "ConstructNewMaterial", "d2/d00/classG4NistManager.html#ab2de129709586652e4b018f61603a2e9", null ],
    [ "FindElement", "d2/d00/classG4NistManager.html#a152cafcfe0e9d90fdc7f790486970f02", null ],
    [ "FindMaterial", "d2/d00/classG4NistManager.html#abf296b03ffdb89769a19d24adf6744c8", null ],
    [ "FindOrBuildElement", "d2/d00/classG4NistManager.html#a24c52e3b10ca6b6f34008329f2b9daaa", null ],
    [ "FindOrBuildElement", "d2/d00/classG4NistManager.html#af3df7aafa89652b9fe31735c0eab7b12", null ],
    [ "FindOrBuildMaterial", "d2/d00/classG4NistManager.html#abb3a53647400f8477dfe1cae931c3e72", null ],
    [ "FindOrBuildSimpleMaterial", "d2/d00/classG4NistManager.html#a8061c4d90c4ecd5804286d222d17f5a6", null ],
    [ "FindSimpleMaterial", "d2/d00/classG4NistManager.html#a3528e4332c4cbdec85d31fa1143ddf8d", null ],
    [ "GetA27", "d2/d00/classG4NistManager.html#a1334d0f1b9613ce22db945df5617027c", null ],
    [ "GetAtomicMass", "d2/d00/classG4NistManager.html#a7d83df77dc47817d674145c18ace961c", null ],
    [ "GetAtomicMassAmu", "d2/d00/classG4NistManager.html#afea8ff62b2c01274aca07ef8069ecc7a", null ],
    [ "GetAtomicMassAmu", "d2/d00/classG4NistManager.html#a61f809c2eaad73ffb0bb8cd74fca2b52", null ],
    [ "GetElement", "d2/d00/classG4NistManager.html#a5a545591456ed9506c0acf5e38d8ce68", null ],
    [ "GetICRU90StoppingData", "d2/d00/classG4NistManager.html#a0c2f9f5a2f9cbe2261e28961acbc3792", null ],
    [ "GetIsotopeAbundance", "d2/d00/classG4NistManager.html#ae13295f2d669c0f8c5f3dd8492ec0d0b", null ],
    [ "GetIsotopeMass", "d2/d00/classG4NistManager.html#a59798c2d25ceb6cb5965d040250f04f1", null ],
    [ "GetLOGAMU", "d2/d00/classG4NistManager.html#af409f74592a071e6f02982c3ad601c18", null ],
    [ "GetLOGZ", "d2/d00/classG4NistManager.html#ab68c49bdbb640fcfb7f22137d09c01b4", null ],
    [ "GetMaterial", "d2/d00/classG4NistManager.html#af3eb5215fb6241d1e5dad1e442598ac4", null ],
    [ "GetMeanIonisationEnergy", "d2/d00/classG4NistManager.html#a9b428508929e4679829263a96c3b4f68", null ],
    [ "GetNistElementNames", "d2/d00/classG4NistManager.html#aa18048bff4c1b071eaa9cb941d26b31f", null ],
    [ "GetNistFirstIsotopeN", "d2/d00/classG4NistManager.html#ace924580f6add99f68ebc68a730db8e8", null ],
    [ "GetNistMaterialNames", "d2/d00/classG4NistManager.html#a6f801a50703ed649e8f9c995a68cee45", null ],
    [ "GetNominalDensity", "d2/d00/classG4NistManager.html#affdec6b987bac0034ec8dd848fff6492", null ],
    [ "GetNumberOfElements", "d2/d00/classG4NistManager.html#ad075d2c931c60324b29a36f894b6dade", null ],
    [ "GetNumberOfMaterials", "d2/d00/classG4NistManager.html#adfdf00dca05972b3656cd5b7e7ba3496", null ],
    [ "GetNumberOfNistIsotopes", "d2/d00/classG4NistManager.html#a04eda267922bf3162cf3828c160185b5", null ],
    [ "GetTotalElectronBindingEnergy", "d2/d00/classG4NistManager.html#a1ab87ec5369c094aba9fb258c6e6fe9f", null ],
    [ "GetVerbose", "d2/d00/classG4NistManager.html#a3de80644ddfc46ea07baec47d18517e0", null ],
    [ "GetZ", "d2/d00/classG4NistManager.html#ac6ef4754cc498e0b341b3e8e6e359766", null ],
    [ "GetZ13", "d2/d00/classG4NistManager.html#a324bed500e2f28d4b8287bef0cdb71c5", null ],
    [ "GetZ13", "d2/d00/classG4NistManager.html#a8378a452d31b134042ad3206bd92ed84", null ],
    [ "Instance", "d2/d00/classG4NistManager.html#ad0dbad7785358d8c46139b4d808c65db", null ],
    [ "ListMaterials", "d2/d00/classG4NistManager.html#a8184e8fb333ac3ed8928727fa4fc835e", null ],
    [ "PrintElement", "d2/d00/classG4NistManager.html#aefcdec540aedf61b172bf87fc0627ac5", null ],
    [ "PrintElement", "d2/d00/classG4NistManager.html#a3855b205e27a9f2b402bab0abe26b05a", null ],
    [ "PrintG4Element", "d2/d00/classG4NistManager.html#aa9ce6ff4ef9fefce3e7762385c3412ee", null ],
    [ "PrintG4Material", "d2/d00/classG4NistManager.html#aa3982663dc30b16f5b8d2528ba4ad80b", null ],
    [ "SetDensityEffectCalculatorFlag", "d2/d00/classG4NistManager.html#a394feeab81cf81fed0b51c2a2ba02c1e", null ],
    [ "SetDensityEffectCalculatorFlag", "d2/d00/classG4NistManager.html#ac36469ffe756833f8824cf9b7d8e1dc3", null ],
    [ "SetVerbose", "d2/d00/classG4NistManager.html#ade14f43f7de5abba3022b4f91d599414", null ],
    [ "elements", "d2/d00/classG4NistManager.html#a214c29caac7e9f99b5bbaaddfc3dffcd", null ],
    [ "elmBuilder", "d2/d00/classG4NistManager.html#a83d7f37bc072a1d5c7a6f6fe5621ae4b", null ],
    [ "fICRU90", "d2/d00/classG4NistManager.html#a240b150769fe694f5079ca9a3a7ff4d0", null ],
    [ "g4pow", "d2/d00/classG4NistManager.html#ab7ea2df3b5605444cc22d425b8e3c999", null ],
    [ "instance", "d2/d00/classG4NistManager.html#a5da65ef9e8eff5cc73d8eaaf118f44e7", null ],
    [ "LOGAZ", "d2/d00/classG4NistManager.html#a5f98132a55433780e66f5bea11fb074c", null ],
    [ "matBuilder", "d2/d00/classG4NistManager.html#a748f3b60422cc245bbaf0ded705249f1", null ],
    [ "materials", "d2/d00/classG4NistManager.html#ab8658647d2ca4f65401db350a4685211", null ],
    [ "messenger", "d2/d00/classG4NistManager.html#a1871adaddabe0fa245a0703d85f89bf8", null ],
    [ "nElements", "d2/d00/classG4NistManager.html#abd39b3a99c4083f9bd03e7c60db8307a", null ],
    [ "nMaterials", "d2/d00/classG4NistManager.html#acd1aac03ec58de55ba9503c564ffa854", null ],
    [ "POWERA27", "d2/d00/classG4NistManager.html#a38cc2f5b0b9c65cfc538546c36daa7a5", null ],
    [ "verbose", "d2/d00/classG4NistManager.html#a2a7d334c176c8eb580d7ed94042450b4", null ]
];