var classG4VEmAngularDistribution =
[
    [ "G4VEmAngularDistribution", "da/d90/classG4VEmAngularDistribution.html#a11dba3e5df99d2b7ab7ece36033e1efc", null ],
    [ "~G4VEmAngularDistribution", "da/d90/classG4VEmAngularDistribution.html#adf38ced9e7309e4addda34a7121c932d", null ],
    [ "G4VEmAngularDistribution", "da/d90/classG4VEmAngularDistribution.html#a12209598aef25b2a90fc3eec07604aa7", null ],
    [ "GetName", "da/d90/classG4VEmAngularDistribution.html#a0d0b2a07d85a644ba11a83efa7b8d1ee", null ],
    [ "operator=", "da/d90/classG4VEmAngularDistribution.html#a45758295869fce9fbb8b5370079520c5", null ],
    [ "PrintGeneratorInformation", "da/d90/classG4VEmAngularDistribution.html#a62155fd438249787e2b31f0b9aa5afed", null ],
    [ "SampleDirection", "da/d90/classG4VEmAngularDistribution.html#afafd92640a2fa85aef2c9135acd3ad82", null ],
    [ "SampleDirectionForShell", "da/d90/classG4VEmAngularDistribution.html#a0ff759c23f0fd9fc4250b606006e2565", null ],
    [ "SamplePairDirections", "da/d90/classG4VEmAngularDistribution.html#a978bb2049fb86b020fd57c40adfa11e3", null ],
    [ "fLocalDirection", "da/d90/classG4VEmAngularDistribution.html#a37c5699f8d2dff8acfa250cc796431c3", null ],
    [ "fName", "da/d90/classG4VEmAngularDistribution.html#abf1e8ff6f0add4dd28bd3c667c13ac88", null ],
    [ "fPolarisation", "da/d90/classG4VEmAngularDistribution.html#a7abb79d841f3b9492c82927cd36f6d96", null ]
];