var classG4FTFBinaryProtonBuilder =
[
    [ "G4FTFBinaryProtonBuilder", "da/d90/classG4FTFBinaryProtonBuilder.html#a56990d16cb14071d08d72203be1a3baa", null ],
    [ "~G4FTFBinaryProtonBuilder", "da/d90/classG4FTFBinaryProtonBuilder.html#a5b887af34cd658442e612c16d2a01a4a", null ],
    [ "Build", "da/d90/classG4FTFBinaryProtonBuilder.html#a3567edfbf4a23823e0f7dd697a2fca8a", null ],
    [ "Build", "da/d90/classG4FTFBinaryProtonBuilder.html#a7a534415d8d1a3953326b85a9936955e", null ],
    [ "Build", "da/d90/classG4FTFBinaryProtonBuilder.html#a35709bbc7ec2dc17862acf5c73cda4f4", null ],
    [ "Build", "da/d90/classG4FTFBinaryProtonBuilder.html#a7d7a3fe1fc3f798f594a92694411bb49", null ],
    [ "SetMaxEnergy", "da/d90/classG4FTFBinaryProtonBuilder.html#ab91fd2d55d173566ad52835fc8e49e88", null ],
    [ "SetMinEnergy", "da/d90/classG4FTFBinaryProtonBuilder.html#a5afe9c1a6585fa4a03806d03d305590d", null ],
    [ "theMax", "da/d90/classG4FTFBinaryProtonBuilder.html#acd6b5e42751e580e8aa38bf294dcf7e7", null ],
    [ "theMin", "da/d90/classG4FTFBinaryProtonBuilder.html#ab9f3b704d75d4e9b3075457e6a5e35d8", null ],
    [ "theModel", "da/d90/classG4FTFBinaryProtonBuilder.html#a5a0d454b513d50240bf3be837c834270", null ]
];