var classG4eIonisation =
[
    [ "G4eIonisation", "da/d09/classG4eIonisation.html#ab44b152a3ff9ea371931c8f7878bf9e7", null ],
    [ "~G4eIonisation", "da/d09/classG4eIonisation.html#a3e820332ddf41f02efeb0ff13905861e", null ],
    [ "G4eIonisation", "da/d09/classG4eIonisation.html#a859838aad59c54e94bef9dd481b784c6", null ],
    [ "InitialiseEnergyLossProcess", "da/d09/classG4eIonisation.html#aa2bb78671607e6f0c73f37ee1187d5b6", null ],
    [ "IsApplicable", "da/d09/classG4eIonisation.html#a3f8b15c11ae8848255078bf1bdac399b", null ],
    [ "MinPrimaryEnergy", "da/d09/classG4eIonisation.html#ae64b8b8ee7994d68408f63711eb11e8e", null ],
    [ "operator=", "da/d09/classG4eIonisation.html#a330827d628678cbc397239516a7b2c9e", null ],
    [ "ProcessDescription", "da/d09/classG4eIonisation.html#ad57513cb26c595c1c06039b2e99d6950", null ],
    [ "isElectron", "da/d09/classG4eIonisation.html#ad38261049590db0172d3d4aa6180c430", null ],
    [ "isInitialised", "da/d09/classG4eIonisation.html#a50654cb08b12c713db57976507759040", null ],
    [ "theElectron", "da/d09/classG4eIonisation.html#a5baf07d0c81b6f80f21e0993fa3d9fd2", null ]
];