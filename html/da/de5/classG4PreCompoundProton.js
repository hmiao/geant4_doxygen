var classG4PreCompoundProton =
[
    [ "G4PreCompoundProton", "da/de5/classG4PreCompoundProton.html#ad4062602944f9756b9d07c9a16644a34", null ],
    [ "~G4PreCompoundProton", "da/de5/classG4PreCompoundProton.html#af9d147b421a6483887bd671b42522cf5", null ],
    [ "G4PreCompoundProton", "da/de5/classG4PreCompoundProton.html#a644d46e1d5f2c77ea2c2cf6dfe9cad36", null ],
    [ "GetAlpha", "da/de5/classG4PreCompoundProton.html#a1397bbdcc3a78ded453a94b9037292bf", null ],
    [ "GetBeta", "da/de5/classG4PreCompoundProton.html#ac2fe678e329cff94a674e4135bdb27be", null ],
    [ "GetRj", "da/de5/classG4PreCompoundProton.html#ab98fd81eebe782a172ecade4977d4705", null ],
    [ "operator!=", "da/de5/classG4PreCompoundProton.html#aa7c5eb5e86f26c578a64493ed217b615", null ],
    [ "operator=", "da/de5/classG4PreCompoundProton.html#a9b59f0b1ad6d783023408a284320cdc0", null ],
    [ "operator==", "da/de5/classG4PreCompoundProton.html#aba5900bdfde0fbea7b910b03c0977c64", null ],
    [ "theProtonCoulombBarrier", "da/de5/classG4PreCompoundProton.html#a7b0820f31aacc9ab245c358b50848c28", null ]
];