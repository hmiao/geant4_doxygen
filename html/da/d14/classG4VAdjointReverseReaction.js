var classG4VAdjointReverseReaction =
[
    [ "G4VAdjointReverseReaction", "da/d14/classG4VAdjointReverseReaction.html#a1b6519799e22760e912ead39879b38c7", null ],
    [ "~G4VAdjointReverseReaction", "da/d14/classG4VAdjointReverseReaction.html#a7816817571dbeb10f176431beebb36ff", null ],
    [ "G4VAdjointReverseReaction", "da/d14/classG4VAdjointReverseReaction.html#aa1733e04edf8721e6bce68ea683d133a", null ],
    [ "BuildPhysicsTable", "da/d14/classG4VAdjointReverseReaction.html#ac924a9a73fcb33a34ed4a7826513cc2e", null ],
    [ "GetMeanFreePath", "da/d14/classG4VAdjointReverseReaction.html#aa04b5703b57f772a465c60afaaf09881", null ],
    [ "operator=", "da/d14/classG4VAdjointReverseReaction.html#a68ed660f8f05648b5c0dc53bf30098a6", null ],
    [ "PostStepDoIt", "da/d14/classG4VAdjointReverseReaction.html#a860b8c8de06f393fd4c0ffd5d698a652", null ],
    [ "fAdjointModel", "da/d14/classG4VAdjointReverseReaction.html#a6fc23c4276003aa6519a04ea0b3f6a97", null ],
    [ "fCSManager", "da/d14/classG4VAdjointReverseReaction.html#a5f0d4f6f2d8576be80008c14858d9a9f", null ],
    [ "fIsFwdCSUsed", "da/d14/classG4VAdjointReverseReaction.html#a46ec3c7956e335c54346a2b43500a0b2", null ],
    [ "fIsScatProjToProj", "da/d14/classG4VAdjointReverseReaction.html#a61341c8047aca8dbf4e0cdccc3abfa8f", null ],
    [ "fParticleChange", "da/d14/classG4VAdjointReverseReaction.html#a0ba6708bdcc7ac818a1bd120ea375054", null ],
    [ "fTrackId", "da/d14/classG4VAdjointReverseReaction.html#ad1aba940d56c9935ae51f227c7d47fe1", null ]
];