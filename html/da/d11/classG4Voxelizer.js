var classG4Voxelizer =
[
    [ "G4VoxelComparator", "d7/de8/classG4Voxelizer_1_1G4VoxelComparator.html", "d7/de8/classG4Voxelizer_1_1G4VoxelComparator" ],
    [ "G4Voxelizer", "da/d11/classG4Voxelizer.html#afe7b1a5e83defa4d701d3330d3057adf", null ],
    [ "~G4Voxelizer", "da/d11/classG4Voxelizer.html#ab25128af52dc00bf21145d2fbfa1f070", null ],
    [ "AllocatedMemory", "da/d11/classG4Voxelizer.html#abefde3ebd1f95a95fe9968fe1f55f122", null ],
    [ "BinarySearch", "da/d11/classG4Voxelizer.html#ad39f6181335950f7868dda665bdf7b08", null ],
    [ "BuildBitmasks", "da/d11/classG4Voxelizer.html#ab1070eebc8105e321a5fb70ffb6f4938", null ],
    [ "BuildBoundaries", "da/d11/classG4Voxelizer.html#aa1162671f72a9419570fdb8d9baa4dc4", null ],
    [ "BuildBoundingBox", "da/d11/classG4Voxelizer.html#a8b0459b9f06dca959ec385330e87e915", null ],
    [ "BuildBoundingBox", "da/d11/classG4Voxelizer.html#aae07191930619abb362addfaff466954", null ],
    [ "BuildEmpty", "da/d11/classG4Voxelizer.html#ac06d22ea7dbd02b86ac4a607a56b65e9", null ],
    [ "BuildReduceVoxels", "da/d11/classG4Voxelizer.html#a4c045b9f94e5c35ea5f9edb0aa30c88a", null ],
    [ "BuildReduceVoxels2", "da/d11/classG4Voxelizer.html#a8953ea4b3f653cd8cbfaa100c27f0bed", null ],
    [ "BuildVoxelLimits", "da/d11/classG4Voxelizer.html#ade6c2bbaa052dcfa4869dd815f52f0c5", null ],
    [ "BuildVoxelLimits", "da/d11/classG4Voxelizer.html#a7da3eb62f8b53eddf4c19ba37df831b2", null ],
    [ "Contains", "da/d11/classG4Voxelizer.html#af009100b219cd42787e1b499936b9a9b", null ],
    [ "CountVoxels", "da/d11/classG4Voxelizer.html#a07f4e7430e8b662582f04de44cdfaa60", null ],
    [ "CreateMiniVoxels", "da/d11/classG4Voxelizer.html#af82851e071c5493f1d32623341560279", null ],
    [ "CreateSortedBoundary", "da/d11/classG4Voxelizer.html#ae2099cf869bede22c48926c6bae92751", null ],
    [ "DisplayBoundaries", "da/d11/classG4Voxelizer.html#ad2683ff7088085bc499a02db5be718db", null ],
    [ "DisplayBoundaries", "da/d11/classG4Voxelizer.html#a8387c7b4b569d15a53fab424f817367b", null ],
    [ "DisplayListNodes", "da/d11/classG4Voxelizer.html#a6db5de94874e2f61dbc50890e514b973", null ],
    [ "DisplayVoxelLimits", "da/d11/classG4Voxelizer.html#a721d19fce22659c2103c796e04d6c2ad", null ],
    [ "DistanceToBoundingBox", "da/d11/classG4Voxelizer.html#a0f20605d32ff362fdb0f504bac8ff7b4", null ],
    [ "DistanceToFirst", "da/d11/classG4Voxelizer.html#a8d81078f3d1fa62ee7361716c387ac44", null ],
    [ "DistanceToNext", "da/d11/classG4Voxelizer.html#ae1d33aad3f09284ca98ec864115c50ae", null ],
    [ "Empty", "da/d11/classG4Voxelizer.html#a00addbd1b32e973309f98fa2d9066597", null ],
    [ "FindComponentsFastest", "da/d11/classG4Voxelizer.html#a27e7cdee978a02ab26c4cca2d96efb9b", null ],
    [ "GetBitsPerSlice", "da/d11/classG4Voxelizer.html#a8c7ea95556874575d6be59d7dc95bea5", null ],
    [ "GetBoundary", "da/d11/classG4Voxelizer.html#a35eaa78ee7118d70314476b43b462d28", null ],
    [ "GetBoxes", "da/d11/classG4Voxelizer.html#a389d3df4c85991004fa22d74b0b216ba", null ],
    [ "GetCandidates", "da/d11/classG4Voxelizer.html#af70ed9f67d92a0ca51e6626e87bed8b9", null ],
    [ "GetCandidatesAsString", "da/d11/classG4Voxelizer.html#addb87986709acdc6ac697a6acc6081e1", null ],
    [ "GetCandidatesVoxel", "da/d11/classG4Voxelizer.html#abae27de9ba4db15254c10288750af237", null ],
    [ "GetCandidatesVoxelArray", "da/d11/classG4Voxelizer.html#a9c1e8cc73e79f8290d6d493feabb3841", null ],
    [ "GetCandidatesVoxelArray", "da/d11/classG4Voxelizer.html#a71ca56bf161efa91dc02f3926af9bfd4", null ],
    [ "GetCandidatesVoxelArray", "da/d11/classG4Voxelizer.html#ab7bab5fcb08ff4cafca67274971f596d", null ],
    [ "GetCountOfVoxels", "da/d11/classG4Voxelizer.html#a1bf47266819710455d6372d709781538", null ],
    [ "GetDefaultVoxelsCount", "da/d11/classG4Voxelizer.html#acd7d61eba70fbedf58030e24308e5a5e", null ],
    [ "GetGlobalPoint", "da/d11/classG4Voxelizer.html#ae8bafdde67f5a99eadecfdd96a0d7c90", null ],
    [ "GetMaxVoxels", "da/d11/classG4Voxelizer.html#a79ec4762b928ff2f5ca8a65a8e5371a1", null ],
    [ "GetPointIndex", "da/d11/classG4Voxelizer.html#ac49aeffd1c33c593477c3a69fee55abd", null ],
    [ "GetPointVoxel", "da/d11/classG4Voxelizer.html#a14269ad01c8daccb68506e413061dacc", null ],
    [ "GetTotalCandidates", "da/d11/classG4Voxelizer.html#af7774f176e3f2f647649a34b25da5135", null ],
    [ "GetVoxel", "da/d11/classG4Voxelizer.html#a715af9075456343ac7e493390aebaa3f", null ],
    [ "GetVoxelBox", "da/d11/classG4Voxelizer.html#ab65cc2495304aef8744b06be2a8e8606", null ],
    [ "GetVoxelBoxCandidates", "da/d11/classG4Voxelizer.html#ae9045f4cbb51c40ab911c1c5a9820270", null ],
    [ "GetVoxelBoxesSize", "da/d11/classG4Voxelizer.html#aa6ceb1230ac9d6744d3b223fb3dff54c", null ],
    [ "GetVoxelsIndex", "da/d11/classG4Voxelizer.html#ac5d529f87a2cdf63204cbc79de5fea52", null ],
    [ "GetVoxelsIndex", "da/d11/classG4Voxelizer.html#aeff8aa0bd40407b32aee53839a68cef3", null ],
    [ "IsEmpty", "da/d11/classG4Voxelizer.html#a56f850fe346aec975bd6af427136894a", null ],
    [ "MinDistanceToBox", "da/d11/classG4Voxelizer.html#aab57fd25af6604e98a39876ca7b0e235", null ],
    [ "SetDefaultVoxelsCount", "da/d11/classG4Voxelizer.html#a7b14e097c69761a63d4cbcb05c4a2153", null ],
    [ "SetMaxVoxels", "da/d11/classG4Voxelizer.html#a3f7a0e439fc324d2b40d6f0400698f28", null ],
    [ "SetMaxVoxels", "da/d11/classG4Voxelizer.html#aabcc9d87e8649d1cc093dc506bf816c2", null ],
    [ "SetReductionRatio", "da/d11/classG4Voxelizer.html#a7287d07d34aa36555b289a901292d6af", null ],
    [ "TransformLimits", "da/d11/classG4Voxelizer.html#a9166a1382d91f939bb13257b0094be57", null ],
    [ "UpdateCurrentVoxel", "da/d11/classG4Voxelizer.html#a614446f354e25d6c07553fd51653426a", null ],
    [ "Voxelize", "da/d11/classG4Voxelizer.html#afda8e51dec76c3b7576f7e733451fdbe", null ],
    [ "Voxelize", "da/d11/classG4Voxelizer.html#ae60d488b2d2f3f72ceba475b2f21ea28", null ],
    [ "fBitmasks", "da/d11/classG4Voxelizer.html#a2f125ca5d833babdc084b1657cf9399b", null ],
    [ "fBoundaries", "da/d11/classG4Voxelizer.html#a324bff97550a892fa29b59c42ee4760f", null ],
    [ "fBoundingBox", "da/d11/classG4Voxelizer.html#a93b4ff9ce7326579f73ef5930fd8858d", null ],
    [ "fBoundingBoxCenter", "da/d11/classG4Voxelizer.html#a1cc330058b7f993faacabf0be686b861", null ],
    [ "fBoundingBoxSize", "da/d11/classG4Voxelizer.html#a02bc3df0bce8e25bbfc364becc4a805e", null ],
    [ "fBoxes", "da/d11/classG4Voxelizer.html#a2a402171b057a335886bc6edbf9580b9", null ],
    [ "fCandidates", "da/d11/classG4Voxelizer.html#a9999098087a6ddc24fbb9fef49922672", null ],
    [ "fCandidatesCounts", "da/d11/classG4Voxelizer.html#aa9fb6fee3540b3bf39b3368473a0bbc9", null ],
    [ "fCountOfVoxels", "da/d11/classG4Voxelizer.html#acaa3dd670fa582a3f1a1a32d1237192d", null ],
    [ "fDefaultVoxelsCount", "da/d11/classG4Voxelizer.html#a2a9de63adf7a24a8abfa48d01ad241e1", null ],
    [ "fEmpty", "da/d11/classG4Voxelizer.html#a74bf81433500e4e089a7bf87fd786cb3", null ],
    [ "fMaxVoxels", "da/d11/classG4Voxelizer.html#a6c5b8b4f0696c5dc2a1987e4e53af731", null ],
    [ "fNoCandidates", "da/d11/classG4Voxelizer.html#adfd537cafcf725c62afd583cfe2611e0", null ],
    [ "fNPerSlice", "da/d11/classG4Voxelizer.html#a50a063584993d2b55ca3eaa78c87f8b4", null ],
    [ "fReductionRatio", "da/d11/classG4Voxelizer.html#a7710c014539a5c64f531a4b68bce1d0f", null ],
    [ "fTolerance", "da/d11/classG4Voxelizer.html#a0e43b1892f532371d599f27971ae1508", null ],
    [ "fTotalCandidates", "da/d11/classG4Voxelizer.html#a212b9c6576f5f78c23fba6166d4a0557", null ],
    [ "fVoxelBoxes", "da/d11/classG4Voxelizer.html#a46e5a0f9692edf688086b9a42220a506", null ],
    [ "fVoxelBoxesCandidates", "da/d11/classG4Voxelizer.html#a72c89b23b33dafaeec22b30ee2f97edd", null ]
];