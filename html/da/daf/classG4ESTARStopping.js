var classG4ESTARStopping =
[
    [ "G4ESTARStopping", "da/daf/classG4ESTARStopping.html#aa3668c6aec73d4dd27aff649ada5dd6e", null ],
    [ "~G4ESTARStopping", "da/daf/classG4ESTARStopping.html#a0b7e70dc85252751c1ff124c299849a5", null ],
    [ "G4ESTARStopping", "da/daf/classG4ESTARStopping.html#a3df3366e76615425b55c386e12507dab", null ],
    [ "AddData", "da/daf/classG4ESTARStopping.html#accba3716d7173d13b8167f81c40749a5", null ],
    [ "GetElectronicDEDX", "da/daf/classG4ESTARStopping.html#ad0df845dfb5d629c91f752929ab9ffaf", null ],
    [ "GetElectronicDEDX", "da/daf/classG4ESTARStopping.html#a53cef020b50a2303b4fbaa1a7ecb2087", null ],
    [ "GetIndex", "da/daf/classG4ESTARStopping.html#aaaf7cccec5cdc40faa62337e3f64c25d", null ],
    [ "Initialise", "da/daf/classG4ESTARStopping.html#a18b3296e91d8d0e1a286ac5836c1d0a9", null ],
    [ "operator=", "da/daf/classG4ESTARStopping.html#a2d4ffb58d80bcf974cb7cca30fe293b3", null ],
    [ "currentMaterial", "da/daf/classG4ESTARStopping.html#a8460cbf34c546e04fcf0d59d67d1c2bc", null ],
    [ "dirPath", "da/daf/classG4ESTARStopping.html#a7f5e00b8416de32dc1c8e262dd67d03f", null ],
    [ "matIndex", "da/daf/classG4ESTARStopping.html#aca977b1cb45aaa4833fc32ad4a8f603d", null ],
    [ "name", "da/daf/classG4ESTARStopping.html#a59dae52a674e113be7449feef9f8d4bf", null ],
    [ "sdata", "da/daf/classG4ESTARStopping.html#a2f9a583e02c0e11da0bbbe5919ed169e", null ],
    [ "type", "da/daf/classG4ESTARStopping.html#ac6784fbc92ae2b050b4811e489a4fa0f", null ]
];