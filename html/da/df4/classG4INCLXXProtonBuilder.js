var classG4INCLXXProtonBuilder =
[
    [ "G4INCLXXProtonBuilder", "da/df4/classG4INCLXXProtonBuilder.html#a0bd50104707b004bbe0653c80996e6b4", null ],
    [ "~G4INCLXXProtonBuilder", "da/df4/classG4INCLXXProtonBuilder.html#acaefc39366213913f7a3871b01dafd4a", null ],
    [ "Build", "da/df4/classG4INCLXXProtonBuilder.html#ad2b264e79226a0611cfcb8bd84093ab7", null ],
    [ "Build", "da/df4/classG4INCLXXProtonBuilder.html#a7a534415d8d1a3953326b85a9936955e", null ],
    [ "Build", "da/df4/classG4INCLXXProtonBuilder.html#a54307ded55e4343807c6fb57cc7e9a39", null ],
    [ "Build", "da/df4/classG4INCLXXProtonBuilder.html#a7d7a3fe1fc3f798f594a92694411bb49", null ],
    [ "SetMaxEnergy", "da/df4/classG4INCLXXProtonBuilder.html#aac1877db41b450cda79f819b998e230f", null ],
    [ "SetMinEnergy", "da/df4/classG4INCLXXProtonBuilder.html#a7fc1dd5f04140fce481509b1b7df970c", null ],
    [ "theMax", "da/df4/classG4INCLXXProtonBuilder.html#af3045d2a3846365b25ecd0b0db9a7571", null ],
    [ "theMin", "da/df4/classG4INCLXXProtonBuilder.html#a87b979a086e2f38cab0822ce27d67313", null ],
    [ "theModel", "da/df4/classG4INCLXXProtonBuilder.html#ad68431213c46a68ac8bd5b303bebc947", null ],
    [ "thePreCompoundMax", "da/df4/classG4INCLXXProtonBuilder.html#a98e40f86f99b30999fd639d0862f2857", null ],
    [ "thePreCompoundMin", "da/df4/classG4INCLXXProtonBuilder.html#a0f7d4b1434ea95a91d25be53c4ebd3f9", null ],
    [ "thePreCompoundModel", "da/df4/classG4INCLXXProtonBuilder.html#ac080c5692fb22b1dc0e9b04c268d84ee", null ]
];