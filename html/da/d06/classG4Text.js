var classG4Text =
[
    [ "Layout", "da/d06/classG4Text.html#abbffb5edaef6b2d2e65beab9c98cdcdf", [
      [ "left", "da/d06/classG4Text.html#abbffb5edaef6b2d2e65beab9c98cdcdfae61406fedccbb0a5e054b3e9354b3dac", null ],
      [ "centre", "da/d06/classG4Text.html#abbffb5edaef6b2d2e65beab9c98cdcdfa4a0a34393693b94a44c26a9cbe16ed28", null ],
      [ "right", "da/d06/classG4Text.html#abbffb5edaef6b2d2e65beab9c98cdcdfacfbe921659894af130b2cd40f2695df3", null ]
    ] ],
    [ "G4Text", "da/d06/classG4Text.html#a87a0910b19b4b087bb905fcb9eb9d733", null ],
    [ "G4Text", "da/d06/classG4Text.html#a928eeee2f14dbe6f16f725455b30dfc5", null ],
    [ "G4Text", "da/d06/classG4Text.html#a2899280b4243be2fa1bc5981eaf7953d", null ],
    [ "G4Text", "da/d06/classG4Text.html#ac3f1970ca0aed1432efa6b69f75560ec", null ],
    [ "G4Text", "da/d06/classG4Text.html#ad5bcebbbaf1e089704fc662668790a6d", null ],
    [ "~G4Text", "da/d06/classG4Text.html#ab3d1ebe7c62cc5ce560a6257c624ab61", null ],
    [ "GetLayout", "da/d06/classG4Text.html#a4895e197bd10e6ea5c1844d26be461ab", null ],
    [ "GetText", "da/d06/classG4Text.html#aa4b2605e32d7da2556f24f652ce36950", null ],
    [ "GetXOffset", "da/d06/classG4Text.html#a6db1d53e3537e5b91ff8c750bd94cb13", null ],
    [ "GetYOffset", "da/d06/classG4Text.html#a57f2d42064f23fd3c1eb213ee145f228", null ],
    [ "operator=", "da/d06/classG4Text.html#a085e109dc6062bc4c6d0586ee116912a", null ],
    [ "operator=", "da/d06/classG4Text.html#a080ce6a5ceae5fe217ca047e02ad2011", null ],
    [ "SetLayout", "da/d06/classG4Text.html#a4ace8f5a6ddd554e694fd236be01a4ac", null ],
    [ "SetOffset", "da/d06/classG4Text.html#a8dfa66448f067b8331a739ee56df04dc", null ],
    [ "SetText", "da/d06/classG4Text.html#a6d305f81cf5af9564d5d306d57ace93f", null ],
    [ "fLayout", "da/d06/classG4Text.html#ab70c7274e13897a364428e532b5ea05c", null ],
    [ "fText", "da/d06/classG4Text.html#a7bad6a86863ddb9882020d730aa1b68e", null ],
    [ "fXOffset", "da/d06/classG4Text.html#abf76785e4eaacc8b90ebb19945cea782", null ],
    [ "fYOffset", "da/d06/classG4Text.html#a5e5e44b9bcf260e5ac3e58671d5eefbe", null ]
];