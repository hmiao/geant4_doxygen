var classG4NuMuNucleusNcModel =
[
    [ "G4NuMuNucleusNcModel", "da/da7/classG4NuMuNucleusNcModel.html#a055dcdf376f3b160256eaa87ae2e67ac", null ],
    [ "~G4NuMuNucleusNcModel", "da/da7/classG4NuMuNucleusNcModel.html#aff770eaec075db55e9e81641a100b4c8", null ],
    [ "ApplyYourself", "da/da7/classG4NuMuNucleusNcModel.html#a790e8687db2d4dc62c21d21b3e4988d4", null ],
    [ "GetMinNuMuEnergy", "da/da7/classG4NuMuNucleusNcModel.html#a1e4a7648f395c6e93344b11434c52a0f", null ],
    [ "InitialiseModel", "da/da7/classG4NuMuNucleusNcModel.html#add2715403979a953d9698cb06f8feddb", null ],
    [ "IsApplicable", "da/da7/classG4NuMuNucleusNcModel.html#a147b1fa00fc4737976b3298818f8392e", null ],
    [ "ModelDescription", "da/da7/classG4NuMuNucleusNcModel.html#a8eecba13707ce73e6c97785616a3101b", null ],
    [ "SampleLVkr", "da/da7/classG4NuMuNucleusNcModel.html#a9b85bacede7193f7d9a2b727497056bb", null ],
    [ "ThresholdEnergy", "da/da7/classG4NuMuNucleusNcModel.html#aff438c92157233a5ab3b318e1ef3dfe0", null ],
    [ "fData", "da/da7/classG4NuMuNucleusNcModel.html#a3703d81de3962217bba4b87ff8ad62e2", null ],
    [ "fMaster", "da/da7/classG4NuMuNucleusNcModel.html#ad4b9bdadaea6f77c23cad38844aef24c", null ],
    [ "fMnumu", "da/da7/classG4NuMuNucleusNcModel.html#a1c6fb3eb3bed74039692cf586b666c81", null ],
    [ "theANuMu", "da/da7/classG4NuMuNucleusNcModel.html#a23a500bd3d4b1b489bde70ad4109ccb5", null ],
    [ "theNuMu", "da/da7/classG4NuMuNucleusNcModel.html#a359f178f94786835d83900e4a5066ba8", null ]
];