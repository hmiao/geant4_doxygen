var classG4DNAPTBExcitationModel =
[
    [ "MapMeanEnergy", "da/dd5/classG4DNAPTBExcitationModel.html#ae6cd9b98f2aaf7c7996e88728c6cf7d4", null ],
    [ "G4DNAPTBExcitationModel", "da/dd5/classG4DNAPTBExcitationModel.html#a77df7683ee6fc9eaa80eefec9c8025b8", null ],
    [ "~G4DNAPTBExcitationModel", "da/dd5/classG4DNAPTBExcitationModel.html#a10ad489ab73bc1674d20a56e850ccec1", null ],
    [ "G4DNAPTBExcitationModel", "da/dd5/classG4DNAPTBExcitationModel.html#ae2c1283fa8b38bbe4c672a33c9e6faef", null ],
    [ "CrossSectionPerVolume", "da/dd5/classG4DNAPTBExcitationModel.html#a8a82b18ca4ac5ee613dc47f405d466e2", null ],
    [ "Initialise", "da/dd5/classG4DNAPTBExcitationModel.html#a9e1c7356e93fb0c1773e8980d3a524c8", null ],
    [ "operator=", "da/dd5/classG4DNAPTBExcitationModel.html#a39200f981db83197f1adc10b0615ecb9", null ],
    [ "SampleSecondaries", "da/dd5/classG4DNAPTBExcitationModel.html#aa865fb2f26cce62f6891453efa1b5a42", null ],
    [ "tableMeanEnergyPTB", "da/dd5/classG4DNAPTBExcitationModel.html#a04fa5ece118772d4887cb00ccb57c912", null ],
    [ "verboseLevel", "da/dd5/classG4DNAPTBExcitationModel.html#af72031fbe033796d12c861f3fb34bb21", null ],
    [ "waterStructure", "da/dd5/classG4DNAPTBExcitationModel.html#a3a2836508d9162fc4b1a25824eb5645e", null ]
];