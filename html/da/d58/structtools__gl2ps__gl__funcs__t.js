var structtools__gl2ps__gl__funcs__t =
[
    [ "m_glBegin", "da/d58/structtools__gl2ps__gl__funcs__t.html#a7e708d1ceeafab3e04779a2dbda04bfb", null ],
    [ "m_glEnd", "da/d58/structtools__gl2ps__gl__funcs__t.html#aafb73724583197da76a6333c196914ea", null ],
    [ "m_glFeedbackBuffer", "da/d58/structtools__gl2ps__gl__funcs__t.html#a57dd1e563e5604971e417a3daa9209c2", null ],
    [ "m_glGetBooleanv", "da/d58/structtools__gl2ps__gl__funcs__t.html#a06e7f9a0ca067792aeebb1e99888bc5c", null ],
    [ "m_glGetFloatv", "da/d58/structtools__gl2ps__gl__funcs__t.html#a2d7a60ddc59b0f7f5a43d88562dd0def", null ],
    [ "m_glGetIntegerv", "da/d58/structtools__gl2ps__gl__funcs__t.html#abde24475fb280d4f6b54c4d6bd33d09c", null ],
    [ "m_glIsEnabled", "da/d58/structtools__gl2ps__gl__funcs__t.html#a9cc7887aecad5c0fcfbc92b8343582de", null ],
    [ "m_glPassThrough", "da/d58/structtools__gl2ps__gl__funcs__t.html#a722ae7921e500016e5c761cc5744d460", null ],
    [ "m_glRenderMode", "da/d58/structtools__gl2ps__gl__funcs__t.html#a64a0d80c2836d8d7b8968952eed6278f", null ],
    [ "m_glVertex3f", "da/d58/structtools__gl2ps__gl__funcs__t.html#a96d0f5cb0ee91571b665352a8a010126", null ]
];