var classG4FTFPPiKBuilder =
[
    [ "G4FTFPPiKBuilder", "da/d5c/classG4FTFPPiKBuilder.html#a3c0480a58eff8c3c1465416a6d539e6d", null ],
    [ "~G4FTFPPiKBuilder", "da/d5c/classG4FTFPPiKBuilder.html#a50781ab78c3893c59606ed839e704b04", null ],
    [ "Build", "da/d5c/classG4FTFPPiKBuilder.html#a367391adc2f8418bd33dbcf45b856971", null ],
    [ "Build", "da/d5c/classG4FTFPPiKBuilder.html#af3c8c4e78798c97ae1cde4cdeb196b5f", null ],
    [ "Build", "da/d5c/classG4FTFPPiKBuilder.html#a5f9175ea1265185be5fad082602699c2", null ],
    [ "Build", "da/d5c/classG4FTFPPiKBuilder.html#acd114187fc9387804d5f775d5e70b62f", null ],
    [ "SetMaxEnergy", "da/d5c/classG4FTFPPiKBuilder.html#a7b9290734a08e22f4ff9107f0793df94", null ],
    [ "SetMinEnergy", "da/d5c/classG4FTFPPiKBuilder.html#ae43a94886f2b9dd90ae6c5e1128c3cde", null ],
    [ "theMax", "da/d5c/classG4FTFPPiKBuilder.html#a8e795fe3dae3797283a9c980f1f10d79", null ],
    [ "theMin", "da/d5c/classG4FTFPPiKBuilder.html#a00200fc2a71922b9beac80e402077c96", null ],
    [ "theModel", "da/d5c/classG4FTFPPiKBuilder.html#a05b31a60312b0a21c312248353019794", null ]
];