var classG4EmDNAPhysics =
[
    [ "G4EmDNAPhysics", "da/df0/classG4EmDNAPhysics.html#a415b5e8d178f1887685e72f649472384", null ],
    [ "~G4EmDNAPhysics", "da/df0/classG4EmDNAPhysics.html#a9f83f73f8af6fbf7df3b8df62f58d981", null ],
    [ "ConstructGammaPositronProcesses", "da/df0/classG4EmDNAPhysics.html#a8d3e6d1a646ae2e85c728c383a884a98", null ],
    [ "ConstructParticle", "da/df0/classG4EmDNAPhysics.html#a587d5634235d93ac522e01b5bb470c33", null ],
    [ "ConstructProcess", "da/df0/classG4EmDNAPhysics.html#a9c2cfe7c355a56961a26fbf5085835f2", null ]
];