var classG4PolarizedBremsstrahlungXS =
[
    [ "G4PolarizedBremsstrahlungXS", "da/d0f/classG4PolarizedBremsstrahlungXS.html#a6f3771179234b784714bb5b607d2e349", null ],
    [ "~G4PolarizedBremsstrahlungXS", "da/d0f/classG4PolarizedBremsstrahlungXS.html#aa9982b6d3c67af3b95da8160a43f2b71", null ],
    [ "G4PolarizedBremsstrahlungXS", "da/d0f/classG4PolarizedBremsstrahlungXS.html#a86150204229aba0338f569503e41e138", null ],
    [ "GetPol2", "da/d0f/classG4PolarizedBremsstrahlungXS.html#aee61411f41bd557e1c3049039ecabf50", null ],
    [ "GetPol3", "da/d0f/classG4PolarizedBremsstrahlungXS.html#af019e4a512030be98f8140a116b765c9", null ],
    [ "Initialize", "da/d0f/classG4PolarizedBremsstrahlungXS.html#a89c0d897cb75aa25531c2a713ca55d57", null ],
    [ "operator=", "da/d0f/classG4PolarizedBremsstrahlungXS.html#a1dfbc9d03edd77b45cf4463682347598", null ],
    [ "XSection", "da/d0f/classG4PolarizedBremsstrahlungXS.html#a7457c2b0987689c3fa0fe7793dee9aaf", null ],
    [ "fFinalGammaPolarization", "da/d0f/classG4PolarizedBremsstrahlungXS.html#ad6dcaf4971d970804794cf856b169307", null ],
    [ "fFinalLeptonPolarization", "da/d0f/classG4PolarizedBremsstrahlungXS.html#a1c29621d3d6ac080f74a8992ebf3f03c", null ],
    [ "SCRN", "da/d0f/classG4PolarizedBremsstrahlungXS.html#a9607d2aa501d434ce9d259770bb2cd96", null ]
];