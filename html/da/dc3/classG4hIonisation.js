var classG4hIonisation =
[
    [ "G4hIonisation", "da/dc3/classG4hIonisation.html#ae61ca6de5b039f515936ae0d84046f0a", null ],
    [ "~G4hIonisation", "da/dc3/classG4hIonisation.html#ac903cc148487f2dcce29e3db2b23e775", null ],
    [ "G4hIonisation", "da/dc3/classG4hIonisation.html#a5839ec897b025fa3d16d8604ed5f757e", null ],
    [ "InitialiseEnergyLossProcess", "da/dc3/classG4hIonisation.html#ae7d3d3e863c8d96c096e5a37e90d1bcb", null ],
    [ "IsApplicable", "da/dc3/classG4hIonisation.html#a2ff98678b6da032086bc3aef3f229297", null ],
    [ "MinPrimaryEnergy", "da/dc3/classG4hIonisation.html#aae62bc1439f67bb0429f4e342e0606d3", null ],
    [ "operator=", "da/dc3/classG4hIonisation.html#aee1ed6410861d4d4907c21e4ea1fb9a1", null ],
    [ "ProcessDescription", "da/dc3/classG4hIonisation.html#a5e7b361934c89f608f2d3a9bf15a4cca", null ],
    [ "eth", "da/dc3/classG4hIonisation.html#a5df8f48f87a3b09bcece97fc4b8c94db", null ],
    [ "isInitialised", "da/dc3/classG4hIonisation.html#a11eedd506ca98583c0b74995c1d1862f", null ],
    [ "mass", "da/dc3/classG4hIonisation.html#ad4df07cbec40e744ec365e8e0f5efa6d", null ],
    [ "ratio", "da/dc3/classG4hIonisation.html#aff8a5bd9db447ef2cfd2dc90683405e6", null ]
];