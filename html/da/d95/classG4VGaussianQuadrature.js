var classG4VGaussianQuadrature =
[
    [ "G4VGaussianQuadrature", "da/d95/classG4VGaussianQuadrature.html#a3f7a6fb418a7766b6ce0099f7fde7c38", null ],
    [ "~G4VGaussianQuadrature", "da/d95/classG4VGaussianQuadrature.html#a8b52b7e66f3ba24722337a2c75843fa0", null ],
    [ "G4VGaussianQuadrature", "da/d95/classG4VGaussianQuadrature.html#afc50f3160b80c7317c2582f702480e3a", null ],
    [ "GammaLogarithm", "da/d95/classG4VGaussianQuadrature.html#ac2bdae164210adabd77e9a6bfaa4282d", null ],
    [ "GetAbscissa", "da/d95/classG4VGaussianQuadrature.html#aca5a7b12063b89705dcae693d880705a", null ],
    [ "GetNumber", "da/d95/classG4VGaussianQuadrature.html#a0e4627de06eb6cdd83b3c3865df989a4", null ],
    [ "GetWeight", "da/d95/classG4VGaussianQuadrature.html#a212afa4d4eb97687fd8ee70a0527673a", null ],
    [ "operator=", "da/d95/classG4VGaussianQuadrature.html#a8e158f97ce43c5f5c063fa99e6862d6b", null ],
    [ "fAbscissa", "da/d95/classG4VGaussianQuadrature.html#ae528ccb419d344745d10ba963c72e70a", null ],
    [ "fFunction", "da/d95/classG4VGaussianQuadrature.html#a59e8095e1073821192d90401823f9893", null ],
    [ "fNumber", "da/d95/classG4VGaussianQuadrature.html#aac09eff48aa501af39b97cba4fbec3ef", null ],
    [ "fWeight", "da/d95/classG4VGaussianQuadrature.html#a7b8e70d6d9021c0af19f3c1572754704", null ]
];