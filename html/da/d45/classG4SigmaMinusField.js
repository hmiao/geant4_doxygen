var classG4SigmaMinusField =
[
    [ "G4SigmaMinusField", "da/d45/classG4SigmaMinusField.html#abb3a3fecbd87f48c0c1826a8497eba4f", null ],
    [ "~G4SigmaMinusField", "da/d45/classG4SigmaMinusField.html#ab5dca4cc55ed94b3e12f63e9d2dbcce1", null ],
    [ "G4SigmaMinusField", "da/d45/classG4SigmaMinusField.html#a26867bd2e0b17e95f0351d88beabc6a1", null ],
    [ "GetBarrier", "da/d45/classG4SigmaMinusField.html#a75a14417ae6d063e11dcf6a8c5d568a3", null ],
    [ "GetCoeff", "da/d45/classG4SigmaMinusField.html#adc970e8086bcc5d8304fa28360c85b03", null ],
    [ "GetField", "da/d45/classG4SigmaMinusField.html#aeadccbe32386ac41cf3cb44649b34d70", null ],
    [ "operator!=", "da/d45/classG4SigmaMinusField.html#a8ba93faa23cb8f9bc5386c0f6a197e4e", null ],
    [ "operator=", "da/d45/classG4SigmaMinusField.html#a285e4606b7961d50a4936fadd52604ec", null ],
    [ "operator==", "da/d45/classG4SigmaMinusField.html#a0922a9e7e8c5a68e35d8a99268dfb911", null ],
    [ "theCoeff", "da/d45/classG4SigmaMinusField.html#a55259234a9e2c29162ee795ce3060da5", null ]
];