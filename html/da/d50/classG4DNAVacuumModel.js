var classG4DNAVacuumModel =
[
    [ "G4DNAVacuumModel", "da/d50/classG4DNAVacuumModel.html#abc64e0217fb9b1b9a282aea0a0ad4a1d", null ],
    [ "~G4DNAVacuumModel", "da/d50/classG4DNAVacuumModel.html#a93638da9c9327b602df9a8c5c5ad06fd", null ],
    [ "G4DNAVacuumModel", "da/d50/classG4DNAVacuumModel.html#acf69ecd951741dbc2323506e9fed1cb1", null ],
    [ "CrossSectionPerVolume", "da/d50/classG4DNAVacuumModel.html#afa12a484b894d2aa42d53d0270e01011", null ],
    [ "Initialise", "da/d50/classG4DNAVacuumModel.html#ae58eff48fc79671b266202f74e27f542", null ],
    [ "operator=", "da/d50/classG4DNAVacuumModel.html#a5581c70eea88afbb52c023d387450b5a", null ],
    [ "SampleSecondaries", "da/d50/classG4DNAVacuumModel.html#a1204665cda65728df8b4aa7fe5e1ecde", null ],
    [ "verboseLevel", "da/d50/classG4DNAVacuumModel.html#a4a7eb6ae9edbb17467644e4354845e74", null ]
];