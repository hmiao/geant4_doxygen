var classG4DNARuddAngle =
[
    [ "G4DNARuddAngle", "da/d50/classG4DNARuddAngle.html#a7ebeca0d2b9934a4b4deb63abbf7018b", null ],
    [ "~G4DNARuddAngle", "da/d50/classG4DNARuddAngle.html#a06d0398500dac492178d70ba6e2c5a41", null ],
    [ "G4DNARuddAngle", "da/d50/classG4DNARuddAngle.html#abd97c4e274b3cfbf9788d84dc335186c", null ],
    [ "operator=", "da/d50/classG4DNARuddAngle.html#ad72cff8192e46b71e9556d73364c9643", null ],
    [ "PrintGeneratorInformation", "da/d50/classG4DNARuddAngle.html#a6f1e19664d5cd648c5a678f193b2e7ee", null ],
    [ "SampleDirection", "da/d50/classG4DNARuddAngle.html#af00f1bd2ae549c54f90a2b72a60d1a86", null ],
    [ "SampleDirectionForShell", "da/d50/classG4DNARuddAngle.html#a69d7a6805a7e60a0ec8354b2648f2662", null ],
    [ "fElectron", "da/d50/classG4DNARuddAngle.html#a8dbb1500f79ffa7842653cdb2cf792b9", null ]
];