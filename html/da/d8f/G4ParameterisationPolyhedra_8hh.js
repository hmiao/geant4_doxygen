var G4ParameterisationPolyhedra_8hh =
[
    [ "G4VParameterisationPolyhedra", "df/d57/classG4VParameterisationPolyhedra.html", "df/d57/classG4VParameterisationPolyhedra" ],
    [ "G4ParameterisationPolyhedraRho", "df/d4d/classG4ParameterisationPolyhedraRho.html", "df/d4d/classG4ParameterisationPolyhedraRho" ],
    [ "G4ParameterisationPolyhedraPhi", "d7/dda/classG4ParameterisationPolyhedraPhi.html", "d7/dda/classG4ParameterisationPolyhedraPhi" ],
    [ "G4ParameterisationPolyhedraZ", "de/d28/classG4ParameterisationPolyhedraZ.html", "de/d28/classG4ParameterisationPolyhedraZ" ]
];