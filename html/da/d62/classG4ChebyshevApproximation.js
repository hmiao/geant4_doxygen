var classG4ChebyshevApproximation =
[
    [ "G4ChebyshevApproximation", "da/d62/classG4ChebyshevApproximation.html#a7ef825327946ccaf61f1b90e14e9a63b", null ],
    [ "G4ChebyshevApproximation", "da/d62/classG4ChebyshevApproximation.html#a426092865d5223363ef55118797a44a8", null ],
    [ "G4ChebyshevApproximation", "da/d62/classG4ChebyshevApproximation.html#a31fec6cf815820242cdb3d8ec8de862f", null ],
    [ "~G4ChebyshevApproximation", "da/d62/classG4ChebyshevApproximation.html#a1b0fe6ad054becc87d898a8e9935514d", null ],
    [ "G4ChebyshevApproximation", "da/d62/classG4ChebyshevApproximation.html#ae14b80966e7396528ed4799f78184ba7", null ],
    [ "ChebyshevEvaluation", "da/d62/classG4ChebyshevApproximation.html#ad8d0972bb57ec567ff0c7988f9b0707d", null ],
    [ "DerivativeChebyshevCof", "da/d62/classG4ChebyshevApproximation.html#a84cb41feccadeef631415f19791c5914", null ],
    [ "GetChebyshevCof", "da/d62/classG4ChebyshevApproximation.html#a265a7d01e093b020e54e2af7a343d86d", null ],
    [ "IntegralChebyshevCof", "da/d62/classG4ChebyshevApproximation.html#a95662d1a3bd220717aa4a5a1b8ed9855", null ],
    [ "operator=", "da/d62/classG4ChebyshevApproximation.html#aaffcda8fce24b60db159f322eaab5219", null ],
    [ "fChebyshevCof", "da/d62/classG4ChebyshevApproximation.html#a656df22d63916f2003d63633e6ff988b", null ],
    [ "fDiff", "da/d62/classG4ChebyshevApproximation.html#a37133b4aca90e735ca21ef4d7b41f1e1", null ],
    [ "fFunction", "da/d62/classG4ChebyshevApproximation.html#a87dc24ea933e7dac0f7545d0763a6e08", null ],
    [ "fMean", "da/d62/classG4ChebyshevApproximation.html#aed53ff3b7c73ef575db70aefb5bd9641", null ],
    [ "fNumber", "da/d62/classG4ChebyshevApproximation.html#a3608ab24ed7075a4e8c5e49e4cdeb59d", null ]
];