var classG4CoulombBarrier =
[
    [ "G4CoulombBarrier", "da/d3b/classG4CoulombBarrier.html#aa871a0484ca6097eaef7aa7244baf878", null ],
    [ "~G4CoulombBarrier", "da/d3b/classG4CoulombBarrier.html#a95d5501dff8e4ce967f558bfbb4e948f", null ],
    [ "G4CoulombBarrier", "da/d3b/classG4CoulombBarrier.html#a690ad4591443c2fe3f80ee601345cf21", null ],
    [ "BarrierPenetrationFactor", "da/d3b/classG4CoulombBarrier.html#a4aeb74601e787c6e5bf65e399ef195f9", null ],
    [ "GetCoulombBarrier", "da/d3b/classG4CoulombBarrier.html#adc81b5563b90f8c645dee417958b17a0", null ],
    [ "operator!=", "da/d3b/classG4CoulombBarrier.html#a1d4e4c1e7664ef4a4f828681f2f0fb97", null ],
    [ "operator=", "da/d3b/classG4CoulombBarrier.html#ab0419725d0d786d66529edbc4c7d52be", null ],
    [ "operator==", "da/d3b/classG4CoulombBarrier.html#a7571a87f8283af7777bd9318ac6bc780", null ],
    [ "factor", "da/d3b/classG4CoulombBarrier.html#a7dd9d9d666e07bf00261994b24c82cbd", null ],
    [ "g4calc", "da/d3b/classG4CoulombBarrier.html#ab72cdad10b4e00d8753be911f6bbd6e1", null ]
];