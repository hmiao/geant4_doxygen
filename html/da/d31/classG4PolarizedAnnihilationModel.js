var classG4PolarizedAnnihilationModel =
[
    [ "G4PolarizedAnnihilationModel", "da/d31/classG4PolarizedAnnihilationModel.html#a7c9d3e66b12d9f328ac8f51730f98639", null ],
    [ "~G4PolarizedAnnihilationModel", "da/d31/classG4PolarizedAnnihilationModel.html#ad0488035bc0ca418c7c43a95b1dd2575", null ],
    [ "G4PolarizedAnnihilationModel", "da/d31/classG4PolarizedAnnihilationModel.html#afd0a4072eeab66aa3d5d547ba00f592d", null ],
    [ "ComputeAsymmetriesPerElectron", "da/d31/classG4PolarizedAnnihilationModel.html#a3f7b0d3f336979f5ac8261392083066b", null ],
    [ "ComputeCrossSectionPerElectron", "da/d31/classG4PolarizedAnnihilationModel.html#a297cf6453e3ec02af7305c528068e245", null ],
    [ "GetBeamPolarization", "da/d31/classG4PolarizedAnnihilationModel.html#a793da64a65fda9928d9c6bfdec783750", null ],
    [ "GetFinalGamma1Polarization", "da/d31/classG4PolarizedAnnihilationModel.html#af5904192539818bc048f6fbed14c2841", null ],
    [ "GetFinalGamma2Polarization", "da/d31/classG4PolarizedAnnihilationModel.html#a92991261cbae2d649ae129dec2690bac", null ],
    [ "GetTargetPolarization", "da/d31/classG4PolarizedAnnihilationModel.html#a6de6cfa2a02ca734264280627ccb5bf0", null ],
    [ "Initialise", "da/d31/classG4PolarizedAnnihilationModel.html#a019a444c28a1102e348c3fdad2050576", null ],
    [ "operator=", "da/d31/classG4PolarizedAnnihilationModel.html#a37aeb95bf87297b9b352352b2102b884", null ],
    [ "SampleSecondaries", "da/d31/classG4PolarizedAnnihilationModel.html#a4a52526df8a95e5febe4b799b5063e68", null ],
    [ "SetBeamPolarization", "da/d31/classG4PolarizedAnnihilationModel.html#aee9af3f8426aa1d3d4d056b011609843", null ],
    [ "SetTargetPolarization", "da/d31/classG4PolarizedAnnihilationModel.html#a930fb896ff3746bf1a69e527ee0b76c8", null ],
    [ "fBeamPolarization", "da/d31/classG4PolarizedAnnihilationModel.html#a75d549753efb6bc2a5081100f7941e62", null ],
    [ "fCrossSectionCalculator", "da/d31/classG4PolarizedAnnihilationModel.html#ac6abaf8a0054d37206990cd25c9909b4", null ],
    [ "fFinalGamma1Polarization", "da/d31/classG4PolarizedAnnihilationModel.html#a166a91c8ac84d2132bc0114bb7f3aafd", null ],
    [ "fFinalGamma2Polarization", "da/d31/classG4PolarizedAnnihilationModel.html#a86d1bd9e19d05ab1247749a917005ae9", null ],
    [ "fParticleChange", "da/d31/classG4PolarizedAnnihilationModel.html#a00df30ec0ff1810b553f8fd2b2fac32f", null ],
    [ "fTargetPolarization", "da/d31/classG4PolarizedAnnihilationModel.html#a308fd560ee18579f0cfd3e0817bf36a3", null ],
    [ "fVerboseLevel", "da/d31/classG4PolarizedAnnihilationModel.html#a661753f23a6cb4888d6982eab56481cc", null ]
];