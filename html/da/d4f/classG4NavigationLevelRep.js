var classG4NavigationLevelRep =
[
    [ "G4NavigationLevelRep", "da/d4f/classG4NavigationLevelRep.html#a8128d04977e5b88916b44d047ab28640", null ],
    [ "G4NavigationLevelRep", "da/d4f/classG4NavigationLevelRep.html#a22fd1139b1ac3c3c0494263c148e8264", null ],
    [ "G4NavigationLevelRep", "da/d4f/classG4NavigationLevelRep.html#ae891d4c59426ad2893d8e5b66eb57f9f", null ],
    [ "G4NavigationLevelRep", "da/d4f/classG4NavigationLevelRep.html#a19a5a43818883a38b69bd7c473445a82", null ],
    [ "~G4NavigationLevelRep", "da/d4f/classG4NavigationLevelRep.html#a1c5568c5b739817b974b56efb45df807", null ],
    [ "AddAReference", "da/d4f/classG4NavigationLevelRep.html#a0cc120363878965d4999084199eb4022", null ],
    [ "GetPhysicalVolume", "da/d4f/classG4NavigationLevelRep.html#a25c7549bbdb8e0b0b1840f6b14b9a885", null ],
    [ "GetReplicaNo", "da/d4f/classG4NavigationLevelRep.html#a823fc04e5054535dd104c6706a08b464", null ],
    [ "GetTransform", "da/d4f/classG4NavigationLevelRep.html#a34dabf475812898fc4b3655c49fe9a12", null ],
    [ "GetTransformPtr", "da/d4f/classG4NavigationLevelRep.html#a3cbfda1e09d62657bb0fcc308760b78f", null ],
    [ "GetVolumeType", "da/d4f/classG4NavigationLevelRep.html#a1f1d8d71d900e65e1672f074ef2e3dae", null ],
    [ "operator delete", "da/d4f/classG4NavigationLevelRep.html#a210a36e50ac8ae202187616700f66c16", null ],
    [ "operator new", "da/d4f/classG4NavigationLevelRep.html#a01c659517678e711e98ce467d59545e5", null ],
    [ "operator=", "da/d4f/classG4NavigationLevelRep.html#a0ee2ba31d47cb56609c233f4d1502c62", null ],
    [ "RemoveAReference", "da/d4f/classG4NavigationLevelRep.html#a953a31fcd4ad9567e20c862965b45056", null ],
    [ "fCountRef", "da/d4f/classG4NavigationLevelRep.html#aefff4f1d65ca32ef013d213cf669bc13", null ],
    [ "sPhysicalVolumePtr", "da/d4f/classG4NavigationLevelRep.html#a4b6e3b06bf814c09783b3a2b21bb5fdc", null ],
    [ "sReplicaNo", "da/d4f/classG4NavigationLevelRep.html#af33532a514e76f78703fa3f006e3889f", null ],
    [ "sTransform", "da/d4f/classG4NavigationLevelRep.html#ab7efa964edf2abc78b8ebfaaa7504a99", null ],
    [ "sVolumeType", "da/d4f/classG4NavigationLevelRep.html#ac0e8f491d8769928f14162af7852475b", null ]
];