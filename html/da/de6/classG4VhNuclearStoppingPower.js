var classG4VhNuclearStoppingPower =
[
    [ "G4VhNuclearStoppingPower", "da/de6/classG4VhNuclearStoppingPower.html#a99a3adbca1440cb4dd510ee19a592916", null ],
    [ "~G4VhNuclearStoppingPower", "da/de6/classG4VhNuclearStoppingPower.html#af9b1cf4bdca22fe51720ae851297d25a", null ],
    [ "G4VhNuclearStoppingPower", "da/de6/classG4VhNuclearStoppingPower.html#a5d7d20c9eeeb15d942315be62f5d53b1", null ],
    [ "NuclearStoppingPower", "da/de6/classG4VhNuclearStoppingPower.html#a5202d83bc9c2c26b7963c4353ef9aed9", null ],
    [ "operator=", "da/de6/classG4VhNuclearStoppingPower.html#a096becea57fb2c84f3693864af00b0f7", null ],
    [ "SetNuclearStoppingFluctuationsOff", "da/de6/classG4VhNuclearStoppingPower.html#aeb5a2a738e779ca923fbf4b982c46f5e", null ],
    [ "SetNuclearStoppingFluctuationsOn", "da/de6/classG4VhNuclearStoppingPower.html#a61498c3d78da9f3818f51f7824393721", null ],
    [ "lossFlucFlag", "da/de6/classG4VhNuclearStoppingPower.html#a3f027b54f803071e6ba6cad2b75a7ecd", null ]
];