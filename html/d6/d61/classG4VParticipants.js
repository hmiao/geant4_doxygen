var classG4VParticipants =
[
    [ "G4VParticipants", "d6/d61/classG4VParticipants.html#a0a5b54bf0881daf09c1db5fd60d5845e", null ],
    [ "~G4VParticipants", "d6/d61/classG4VParticipants.html#a412d91e1cbe43a3e3d10702e03c5bf51", null ],
    [ "G4VParticipants", "d6/d61/classG4VParticipants.html#a2d0be0c4b886778e23692c206464051b", null ],
    [ "GetProjectileNucleus", "d6/d61/classG4VParticipants.html#ae063f78b8c45380b611cf4f9da64759e", null ],
    [ "GetWoundedNucleus", "d6/d61/classG4VParticipants.html#aebcf6089626ee2e64e5db66a02f9983f", null ],
    [ "Init", "d6/d61/classG4VParticipants.html#aa75f65d2aafbe6eefe57d3f631ac7fbb", null ],
    [ "InitProjectileNucleus", "d6/d61/classG4VParticipants.html#a7672734640f9b70a9706be2ef4bd0d66", null ],
    [ "operator!=", "d6/d61/classG4VParticipants.html#ae68efa64ef2cc8b35337c7e737699145", null ],
    [ "operator=", "d6/d61/classG4VParticipants.html#aa48dd9b78e251e576ac9678bc2452a14", null ],
    [ "operator==", "d6/d61/classG4VParticipants.html#a326be003d69901c2c4209dd701ef90b7", null ],
    [ "SetNucleus", "d6/d61/classG4VParticipants.html#ae6da8a49bd5c56670bb39b046617e8d4", null ],
    [ "SetProjectileNucleus", "d6/d61/classG4VParticipants.html#ac9773329d9db6a8c224d49c43b3d9116", null ],
    [ "theNucleus", "d6/d61/classG4VParticipants.html#ae39ed0c63308deda0fc460d6ddf89f50", null ],
    [ "theProjectileNucleus", "d6/d61/classG4VParticipants.html#a006aa39aa4638873a560ee72928dd7ea", null ]
];