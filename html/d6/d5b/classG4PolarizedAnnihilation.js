var classG4PolarizedAnnihilation =
[
    [ "G4PolarizedAnnihilation", "d6/d5b/classG4PolarizedAnnihilation.html#a868041bdd510340c3ce958b4978b8e37", null ],
    [ "~G4PolarizedAnnihilation", "d6/d5b/classG4PolarizedAnnihilation.html#a6d2c7c737d094e49a6458828aa84fc99", null ],
    [ "G4PolarizedAnnihilation", "d6/d5b/classG4PolarizedAnnihilation.html#a195234dc57295d967ccf269f3e9b2ed7", null ],
    [ "BuildAsymmetryTables", "d6/d5b/classG4PolarizedAnnihilation.html#ad8fe5aeb4ef2b9917ac1a9dd794f6769", null ],
    [ "BuildPhysicsTable", "d6/d5b/classG4PolarizedAnnihilation.html#afb116d1cf7ba94f1ecd0742fc856160c", null ],
    [ "CleanTables", "d6/d5b/classG4PolarizedAnnihilation.html#aaddee5851b4485c3b8e3bacc1880d3b8", null ],
    [ "ComputeAsymmetry", "d6/d5b/classG4PolarizedAnnihilation.html#a7ec605b58d94a792d66f211b831647bb", null ],
    [ "ComputeSaturationFactor", "d6/d5b/classG4PolarizedAnnihilation.html#acb5b5cd7d3489c682a25ec1da185ebb5", null ],
    [ "DumpInfo", "d6/d5b/classG4PolarizedAnnihilation.html#a468f26e40843cf771a2d0b2f30c76dbe", null ],
    [ "GetMeanFreePath", "d6/d5b/classG4PolarizedAnnihilation.html#a96669269a71f639d069bbdde5aa4e155", null ],
    [ "operator=", "d6/d5b/classG4PolarizedAnnihilation.html#a0bf85461c0630719157cdcb2e18f6aae", null ],
    [ "PostStepGetPhysicalInteractionLength", "d6/d5b/classG4PolarizedAnnihilation.html#adc65b75cf0db3099c2c09118dded4405", null ],
    [ "ProcessDescription", "d6/d5b/classG4PolarizedAnnihilation.html#a7cc873f23bd7937dc90aef0a2e5c2937", null ],
    [ "fAsymmetryTable", "d6/d5b/classG4PolarizedAnnihilation.html#a46bbb48c1464304d67f2c2f49e0811af", null ],
    [ "fEmModel", "d6/d5b/classG4PolarizedAnnihilation.html#a0fb6136706190df4de895e80c84809b8", null ],
    [ "fTransverseAsymmetryTable", "d6/d5b/classG4PolarizedAnnihilation.html#ae07e935acec90f3c924ea19e56a199b6", null ]
];