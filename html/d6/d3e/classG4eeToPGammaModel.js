var classG4eeToPGammaModel =
[
    [ "G4eeToPGammaModel", "d6/d3e/classG4eeToPGammaModel.html#a576b5d295e1b1adab5d6a65c465d7fe9", null ],
    [ "~G4eeToPGammaModel", "d6/d3e/classG4eeToPGammaModel.html#a1014c5d0271ef57f77c327243cfcb8ea", null ],
    [ "G4eeToPGammaModel", "d6/d3e/classG4eeToPGammaModel.html#a65b1e481b54e633a883c62d33a00696c", null ],
    [ "ComputeCrossSection", "d6/d3e/classG4eeToPGammaModel.html#afe3357cc87de415a42e242dedce26229", null ],
    [ "operator=", "d6/d3e/classG4eeToPGammaModel.html#ace1f55497a7924dd62e77d817ae49d6c", null ],
    [ "PeakEnergy", "d6/d3e/classG4eeToPGammaModel.html#a810cf28fe2f2636ac97d593078967457", null ],
    [ "SampleSecondaries", "d6/d3e/classG4eeToPGammaModel.html#a8e644e5865deecc66fd01b8d46fe8420", null ],
    [ "massP", "d6/d3e/classG4eeToPGammaModel.html#a8bddbe40d015f0802ec7506b2f7facb5", null ],
    [ "massR", "d6/d3e/classG4eeToPGammaModel.html#ab175a10d5703bb81e95a46ed37d95cf9", null ],
    [ "particle", "d6/d3e/classG4eeToPGammaModel.html#a76c89eb2a3edb4b4b9876db1ed4d8bfc", null ],
    [ "pi0", "d6/d3e/classG4eeToPGammaModel.html#a83b9a5167dec6b72f242b79bb2f885f9", null ]
];