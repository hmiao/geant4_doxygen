var structGL2PSpdfgroup =
[
    [ "fontno", "d6/d57/structGL2PSpdfgroup.html#ac18b08cce2f82cd4b3e870a5de018369", null ],
    [ "fontobjno", "d6/d57/structGL2PSpdfgroup.html#a15a0090229f1ef42d04ab4854f890bb2", null ],
    [ "gsno", "d6/d57/structGL2PSpdfgroup.html#a2cfb2eae774342666b632c550da6ed60", null ],
    [ "gsobjno", "d6/d57/structGL2PSpdfgroup.html#af5a9629a4ff0d42c46927fadada346b4", null ],
    [ "imno", "d6/d57/structGL2PSpdfgroup.html#a4129702ebefb954113a539c9e6c84629", null ],
    [ "imobjno", "d6/d57/structGL2PSpdfgroup.html#a6d4c378ecf26fa2eb26ce95c5df3f09b", null ],
    [ "maskshno", "d6/d57/structGL2PSpdfgroup.html#ae4b35a90b5490ed66d2526865012dd6d", null ],
    [ "maskshobjno", "d6/d57/structGL2PSpdfgroup.html#ad1cc1b17423f57a78f09e018eee75667", null ],
    [ "ptrlist", "d6/d57/structGL2PSpdfgroup.html#ae61982c7b5704fb378003afddaa372a0", null ],
    [ "shno", "d6/d57/structGL2PSpdfgroup.html#a328d66530e598942292e1ca5c54cc195", null ],
    [ "shobjno", "d6/d57/structGL2PSpdfgroup.html#a693eda3aa7df89ab96024c75875e0a5f", null ],
    [ "trgroupno", "d6/d57/structGL2PSpdfgroup.html#a31a492158553a24a1f34894aca2e1273", null ],
    [ "trgroupobjno", "d6/d57/structGL2PSpdfgroup.html#a26d86e3c60589a992cb0208f89951c52", null ]
];