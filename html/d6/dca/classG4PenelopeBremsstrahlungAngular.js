var classG4PenelopeBremsstrahlungAngular =
[
    [ "G4PenelopeBremsstrahlungAngular", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#aa347754d9f0195ec21d4aa9c5534600a", null ],
    [ "~G4PenelopeBremsstrahlungAngular", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a4140b61e4022e8b98d1ae553c074a8ed", null ],
    [ "CalculateEffectiveZ", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a4ec7dac116e5d972f36afd59613e4cc9", null ],
    [ "ClearTables", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a67052986f71fb38834c882147debeec1", null ],
    [ "GetVerbosityLevel", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a0e9597ad7ea803cc0e8125a6121c1e1b", null ],
    [ "Initialize", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#afaef0ff8ccd8094319fe9ba157010619", null ],
    [ "PrepareTables", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#ae6e938b740f6b7c46d3cad87a1d21797", null ],
    [ "ReadDataFile", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a198aa1981a15f81d037c84e188039864", null ],
    [ "SampleDirection", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a84202b019dcea39016fc9037e68bf1c5", null ],
    [ "SetVerbosityLevel", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a64b38a6781fb34231045f194ddee226d", null ],
    [ "fDataRead", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#abc283ef78c2d31877a10f8bf86c8e93e", null ],
    [ "fEffectiveZSq", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a73553df4879530721af671d5642c15d0", null ],
    [ "fLorentzTables1", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a0fd59d057f24dbae76ea6956f75f1957", null ],
    [ "fLorentzTables2", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a4afcdffea17cbf15c4c5e744eb150221", null ],
    [ "fNumberofEPoints", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a14b6b9a8c6471cede37106eb1946ed78", null ],
    [ "fNumberofKPoints", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a13d2fec11e082a843d324f33911a9eff", null ],
    [ "fNumberofZPoints", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#aaa7a5ce8be2e9f19fcca16e1754b0e5f", null ],
    [ "fQQ1", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a94ee8750d33eb08eb6d6a04b18fab553", null ],
    [ "fQQ2", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#acdc7e5457c47c9d4cffe493ebed9e4ce", null ],
    [ "fVerbosityLevel", "d6/dca/classG4PenelopeBremsstrahlungAngular.html#a85c18a28225ca1d5a589123c2c4c4dfc", null ]
];