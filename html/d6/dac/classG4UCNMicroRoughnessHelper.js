var classG4UCNMicroRoughnessHelper =
[
    [ "G4UCNMicroRoughnessHelper", "d6/dac/classG4UCNMicroRoughnessHelper.html#a5e2cd77132f1f3aebdb2752a4b16f61c", null ],
    [ "~G4UCNMicroRoughnessHelper", "d6/dac/classG4UCNMicroRoughnessHelper.html#ae63be5a712f39b0ddee4efe640db190a", null ],
    [ "Fmu", "d6/dac/classG4UCNMicroRoughnessHelper.html#aea771c6fce151788d2340c43f1963ad8", null ],
    [ "FmuS", "d6/dac/classG4UCNMicroRoughnessHelper.html#a71f983ebfa55c06c0edb00cc13ee99da", null ],
    [ "GetInstance", "d6/dac/classG4UCNMicroRoughnessHelper.html#add2ed02348713640612e6a8e607acb3b", null ],
    [ "IntIminus", "d6/dac/classG4UCNMicroRoughnessHelper.html#a8eef8ce5c66713c712a68ec523649a80", null ],
    [ "IntIplus", "d6/dac/classG4UCNMicroRoughnessHelper.html#af8e87429766d90930cd8fbdc36deb7df", null ],
    [ "ProbIminus", "d6/dac/classG4UCNMicroRoughnessHelper.html#aef1b6af7abb478acb401b72d8ed55021", null ],
    [ "ProbIplus", "d6/dac/classG4UCNMicroRoughnessHelper.html#a62d01274a70b02200964f1cfdfebc970", null ],
    [ "S2", "d6/dac/classG4UCNMicroRoughnessHelper.html#a9c7ec23f64da4dfe347d9d351e683d52", null ],
    [ "SS2", "d6/dac/classG4UCNMicroRoughnessHelper.html#aac2253be1aae614b0bce0089a04c4b83", null ],
    [ "fpInstance", "d6/dac/classG4UCNMicroRoughnessHelper.html#adbc0df9462fa9bc52d2855c4313c18b7", null ]
];