var classG4PrecoProtonBuilder =
[
    [ "G4PrecoProtonBuilder", "d6/d95/classG4PrecoProtonBuilder.html#a10c3b3cb42f5d2c61f81c8d3df186e87", null ],
    [ "~G4PrecoProtonBuilder", "d6/d95/classG4PrecoProtonBuilder.html#a50f5ab536f564127d3e5abb4e2f21f27", null ],
    [ "Build", "d6/d95/classG4PrecoProtonBuilder.html#ad33108c0e59cd5c31dd6bb1ce640f7f4", null ],
    [ "Build", "d6/d95/classG4PrecoProtonBuilder.html#a7a534415d8d1a3953326b85a9936955e", null ],
    [ "Build", "d6/d95/classG4PrecoProtonBuilder.html#a8f8d3c8c19cfe41faee6377021302a4a", null ],
    [ "Build", "d6/d95/classG4PrecoProtonBuilder.html#a7d7a3fe1fc3f798f594a92694411bb49", null ],
    [ "SetMinEnergy", "d6/d95/classG4PrecoProtonBuilder.html#a594656d12b3c27517aba51fb861ad12e", null ],
    [ "theMax", "d6/d95/classG4PrecoProtonBuilder.html#a8fa48298f52d8ccd27c2b0b74e2adeeb", null ],
    [ "theMin", "d6/d95/classG4PrecoProtonBuilder.html#a6cc3b5422473109bd43b1a4f7ad9ccc8", null ],
    [ "theModel", "d6/d95/classG4PrecoProtonBuilder.html#a40b268a7791dccc6a4a54dc8452e018f", null ]
];