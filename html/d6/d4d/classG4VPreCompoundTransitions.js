var classG4VPreCompoundTransitions =
[
    [ "G4VPreCompoundTransitions", "d6/d4d/classG4VPreCompoundTransitions.html#a852dd785dfbc1e13088b958704338ab6", null ],
    [ "~G4VPreCompoundTransitions", "d6/d4d/classG4VPreCompoundTransitions.html#a1f6d1f40c0339bff405c447041ac2b2e", null ],
    [ "G4VPreCompoundTransitions", "d6/d4d/classG4VPreCompoundTransitions.html#ace9f2a5763d1cfba807ae0642bb7b74c", null ],
    [ "CalculateProbability", "d6/d4d/classG4VPreCompoundTransitions.html#a7ed567872002549398cabe7e16073c6e", null ],
    [ "GetTransitionProb1", "d6/d4d/classG4VPreCompoundTransitions.html#aef4358c8e91dbc4e2860f8b79f525529", null ],
    [ "GetTransitionProb2", "d6/d4d/classG4VPreCompoundTransitions.html#a8c915899d799344c4c781f13f1ba4363", null ],
    [ "GetTransitionProb3", "d6/d4d/classG4VPreCompoundTransitions.html#a7e1dfda2bec05ecde0f82bd8b51c53dd", null ],
    [ "operator!=", "d6/d4d/classG4VPreCompoundTransitions.html#aac8c44889bcfa65c0bae0a992741abdf", null ],
    [ "operator=", "d6/d4d/classG4VPreCompoundTransitions.html#a6a8c7a4731b5b2ef5cb5e3162aaac3be", null ],
    [ "operator==", "d6/d4d/classG4VPreCompoundTransitions.html#ad8602c5381d5186e72c902d9ab7bceb2", null ],
    [ "PerformTransition", "d6/d4d/classG4VPreCompoundTransitions.html#a855bffeba4874f4020510a8d73cf94cc", null ],
    [ "UseCEMtr", "d6/d4d/classG4VPreCompoundTransitions.html#a8f6ab3b278d1955f0802ebb2671e2bbb", null ],
    [ "UseNGB", "d6/d4d/classG4VPreCompoundTransitions.html#ab93c17d2b0a8001d5faed3c99e1e92cb", null ],
    [ "TransitionProb1", "d6/d4d/classG4VPreCompoundTransitions.html#ab3e4ab7130660825ec86d7c8c63ee59d", null ],
    [ "TransitionProb2", "d6/d4d/classG4VPreCompoundTransitions.html#a1010e6297edd3f405b12ad42bc1e15b1", null ],
    [ "TransitionProb3", "d6/d4d/classG4VPreCompoundTransitions.html#a03f2638cbfb4c13ed85b883a7631341c", null ],
    [ "useCEMtr", "d6/d4d/classG4VPreCompoundTransitions.html#a7dc3b4d6d0ff2ac9305c3f82261e6755", null ],
    [ "useNGB", "d6/d4d/classG4VPreCompoundTransitions.html#a9e57aebd4f071de1a3b438214dfc8582", null ]
];