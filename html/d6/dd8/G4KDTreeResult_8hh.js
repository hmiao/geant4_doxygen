var G4KDTreeResult_8hh =
[
    [ "G4KDTreeResult", "d4/de0/classG4KDTreeResult.html", "d4/de0/classG4KDTreeResult" ],
    [ "KDTR_parent", "d6/dd8/G4KDTreeResult_8hh.html#a1806d9b3536423075e58433d55f55e4e", null ],
    [ "G4KDTreeResultHandle", "d6/dd8/G4KDTreeResult_8hh.html#a592d3db8ac5166a8c345a5cf85fb880c", null ],
    [ "ResNodeHandle", "d6/dd8/G4KDTreeResult_8hh.html#a24641698b6cee290162f10dd545f9c77", null ],
    [ "aKDTreeAllocator", "d6/dd8/G4KDTreeResult_8hh.html#a44584488ccd9069fcc667d4e3af6b00c", null ]
];