var Types_8hh =
[
    [ "PTL::api::native", "de/d76/structPTL_1_1api_1_1native.html", null ],
    [ "PTL::api::tbb", "d4/d05/structPTL_1_1api_1_1tbb.html", null ],
    [ "PTL::ScopeDestructor", "d6/d54/structPTL_1_1ScopeDestructor.html", "d6/d54/structPTL_1_1ScopeDestructor" ],
    [ "DLLEXPORT", "d6/dd8/Types_8hh.html#a808e08638be3cba36e36759e5b150de0", null ],
    [ "DLLIMPORT", "d6/dd8/Types_8hh.html#aae8fdf6bcc88c172ca8a75ad80f17a95", null ],
    [ "PTL_DEFAULT_OBJECT", "d6/dd8/Types_8hh.html#a8c13e9c54aa9d3c6e08be5b9663ac71f", null ],
    [ "PTL_DLL", "d6/dd8/Types_8hh.html#ad4f9f980c65dfa935b603079f0f9a67b", null ],
    [ "GetSharedPointerPair", "d6/dd8/Types_8hh.html#a25264d0b7988433bcef200801d2bf3aa", null ],
    [ "GetSharedPointerPairInstance", "d6/dd8/Types_8hh.html#a9e0e90cd7ef31513511e723112fa193b", null ],
    [ "GetSharedPointerPairMasterInstance", "d6/dd8/Types_8hh.html#ae4fe21172239ad21f2b8812021e4411e", null ]
];