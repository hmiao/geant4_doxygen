var classG4LivermoreGammaConversion5DModel =
[
    [ "G4LivermoreGammaConversion5DModel", "d6/d48/classG4LivermoreGammaConversion5DModel.html#a80960b7a8cbf6c0138b45da1545a12bf", null ],
    [ "~G4LivermoreGammaConversion5DModel", "d6/d48/classG4LivermoreGammaConversion5DModel.html#ada99f5312dca9e68fc5ed44b9ac09019", null ],
    [ "G4LivermoreGammaConversion5DModel", "d6/d48/classG4LivermoreGammaConversion5DModel.html#a71ecdd937cec98e64e496e1fe1f5295b", null ],
    [ "ComputeCrossSectionPerAtom", "d6/d48/classG4LivermoreGammaConversion5DModel.html#a90616c539c6359b7c53d41d17cde42fb", null ],
    [ "Initialise", "d6/d48/classG4LivermoreGammaConversion5DModel.html#a5651fd0e08c3f40f5de060d0f72bcdb3", null ],
    [ "InitialiseForElement", "d6/d48/classG4LivermoreGammaConversion5DModel.html#a8cf824fecc8fca716b2c2c1450f906eb", null ],
    [ "operator=", "d6/d48/classG4LivermoreGammaConversion5DModel.html#af85757bdf7a28d6b4808da3511908d86", null ],
    [ "ReadData", "d6/d48/classG4LivermoreGammaConversion5DModel.html#a806da51fc86ff5b1bc9cff2f9e4aca06", null ],
    [ "data", "d6/d48/classG4LivermoreGammaConversion5DModel.html#abab032eb7bdeffa05bd9bdc1ee2bec7a", null ],
    [ "fParticleChange", "d6/d48/classG4LivermoreGammaConversion5DModel.html#a53979f39fc34d353f83162b75cbca04b", null ],
    [ "lowEnergyLimit", "d6/d48/classG4LivermoreGammaConversion5DModel.html#a751ce26268a2c877fc7f51e1ee5925ab", null ],
    [ "maxZ", "d6/d48/classG4LivermoreGammaConversion5DModel.html#a770bee730bbe8dfc68343b8d97e40760", null ],
    [ "verboseLevel", "d6/d48/classG4LivermoreGammaConversion5DModel.html#a2dd0b32b8dbf4d7de5a861e1ac4f6f6b", null ]
];