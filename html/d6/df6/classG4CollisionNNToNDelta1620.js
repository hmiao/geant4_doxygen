var classG4CollisionNNToNDelta1620 =
[
    [ "G4CollisionNNToNDelta1620", "d6/df6/classG4CollisionNNToNDelta1620.html#a716b29a3a45556b1dcf4ac2c7b0522ca", null ],
    [ "~G4CollisionNNToNDelta1620", "d6/df6/classG4CollisionNNToNDelta1620.html#aceff3eaf10a36f31476e9f2a55e95a56", null ],
    [ "G4CollisionNNToNDelta1620", "d6/df6/classG4CollisionNNToNDelta1620.html#a4c67924964762a2f6548ff7a2f89c8a1", null ],
    [ "GetComponents", "d6/df6/classG4CollisionNNToNDelta1620.html#ae6741063084ac95c793d2414de7f18e7", null ],
    [ "GetListOfColliders", "d6/df6/classG4CollisionNNToNDelta1620.html#a8dd9d0a84e9184c2493971683b1e3e45", null ],
    [ "GetName", "d6/df6/classG4CollisionNNToNDelta1620.html#a1d259b449b804a4afa20973005d1811c", null ],
    [ "operator=", "d6/df6/classG4CollisionNNToNDelta1620.html#aa48499663d74abd59beb17f23b797e4a", null ],
    [ "components", "d6/df6/classG4CollisionNNToNDelta1620.html#af42538630200e4ddfdb51529154546cf", null ]
];