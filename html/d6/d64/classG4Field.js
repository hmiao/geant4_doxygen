var classG4Field =
[
    [ "G4Field", "d6/d64/classG4Field.html#afa276dd7f281693465c08c7fd907631f", null ],
    [ "G4Field", "d6/d64/classG4Field.html#a9bef32d72b0576950f10c154eac964b3", null ],
    [ "~G4Field", "d6/d64/classG4Field.html#a15c3f2175531bd25dbf8bf8e86ebda11", null ],
    [ "Clone", "d6/d64/classG4Field.html#ab2c508c981bb672ddcca71b2825be426", null ],
    [ "DoesFieldChangeEnergy", "d6/d64/classG4Field.html#aa1dad6fb04242543c33c02386469f9ba", null ],
    [ "GetFieldValue", "d6/d64/classG4Field.html#a8af787d2ecf139cb1f630ea7a28e63f8", null ],
    [ "IsGravityActive", "d6/d64/classG4Field.html#a9bcf2d2fe87ce0d40e47306a54bd6861", null ],
    [ "operator=", "d6/d64/classG4Field.html#abb4f6f7800d83de91c1e92d55d6d2ba0", null ],
    [ "SetGravityActive", "d6/d64/classG4Field.html#a5ee07a8b16084458dba79d15b5a4c4e0", null ],
    [ "fGravityActive", "d6/d64/classG4Field.html#a1b5ceebdfa0b17c967034bdebe4f05bd", null ],
    [ "MAX_NUMBER_OF_COMPONENTS", "d6/d64/classG4Field.html#a479dc9ca5f4fa02dab7ee625bb1f2acd", null ]
];