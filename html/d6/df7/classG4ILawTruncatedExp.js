var classG4ILawTruncatedExp =
[
    [ "G4ILawTruncatedExp", "d6/df7/classG4ILawTruncatedExp.html#a1fdcffb6a5a2a100673673d04cf5ac13", null ],
    [ "~G4ILawTruncatedExp", "d6/df7/classG4ILawTruncatedExp.html#ae0c021372ca4664c70fe8182765bcac4", null ],
    [ "ComputeEffectiveCrossSectionAt", "d6/df7/classG4ILawTruncatedExp.html#ad4314e859d576cc6b670c4f48617ea28", null ],
    [ "ComputeNonInteractionProbabilityAt", "d6/df7/classG4ILawTruncatedExp.html#abacb3dac27d6f3a29263a9cc3affbb9d", null ],
    [ "GetInteractionDistance", "d6/df7/classG4ILawTruncatedExp.html#aea1c0a3c6c2ca33e808456ad7201caf6", null ],
    [ "GetMaximumDistance", "d6/df7/classG4ILawTruncatedExp.html#aea69d82bd655a8652625ee43a8ce1a72", null ],
    [ "IsSingular", "d6/df7/classG4ILawTruncatedExp.html#ad020ffbfcdea3cec567d2374b9cad0d4", null ],
    [ "SampleInteractionLength", "d6/df7/classG4ILawTruncatedExp.html#a4a7076adecf7a5eb2fdbcb9ae4a53be8", null ],
    [ "SetForceCrossSection", "d6/df7/classG4ILawTruncatedExp.html#ac2d361c38c489bc5f7c17edfc286cd7e", null ],
    [ "SetMaximumDistance", "d6/df7/classG4ILawTruncatedExp.html#a58e42e0dd027b56e0f4247b715b2f744", null ],
    [ "UpdateInteractionLengthForStep", "d6/df7/classG4ILawTruncatedExp.html#a86b683eaa59e2b058a4a44b930824542", null ],
    [ "fCrossSection", "d6/df7/classG4ILawTruncatedExp.html#a68346d1b60cdc6a0c2cc8bb9750cb26c", null ],
    [ "fCrossSectionDefined", "d6/df7/classG4ILawTruncatedExp.html#a1ceb4ba529004449c58b00248e654b0b", null ],
    [ "fInteractionDistance", "d6/df7/classG4ILawTruncatedExp.html#aae77540456fe06b3cb9284f923116183", null ],
    [ "fIsSingular", "d6/df7/classG4ILawTruncatedExp.html#a6332d860e7ebc7b7daa3a08341065184", null ],
    [ "fMaximumDistance", "d6/df7/classG4ILawTruncatedExp.html#abf67d4801729a4d3a79388fe771a6e0a", null ]
];