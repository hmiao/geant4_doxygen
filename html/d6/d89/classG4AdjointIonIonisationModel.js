var classG4AdjointIonIonisationModel =
[
    [ "G4AdjointIonIonisationModel", "d6/d89/classG4AdjointIonIonisationModel.html#a18514db596ddef68c0e1795d75555891", null ],
    [ "~G4AdjointIonIonisationModel", "d6/d89/classG4AdjointIonIonisationModel.html#a9e1352e06b43c121f54357e3dfbc7a47", null ],
    [ "G4AdjointIonIonisationModel", "d6/d89/classG4AdjointIonIonisationModel.html#ab1d4159a875be13d5c16fb944e1187e8", null ],
    [ "CorrectPostStepWeight", "d6/d89/classG4AdjointIonIonisationModel.html#a1b03634074f9344f9595d3709199a3ce", null ],
    [ "DefineProjectileProperty", "d6/d89/classG4AdjointIonIonisationModel.html#ab8d486ef115f8add02058a952f106fed", null ],
    [ "DiffCrossSectionPerAtomPrimToSecond", "d6/d89/classG4AdjointIonIonisationModel.html#a31054362827524771528dfc90c9c0525", null ],
    [ "GetSecondAdjEnergyMaxForProdToProj", "d6/d89/classG4AdjointIonIonisationModel.html#a4609b064e475ac888a7100adf48e3084", null ],
    [ "GetSecondAdjEnergyMaxForScatProjToProj", "d6/d89/classG4AdjointIonIonisationModel.html#aab1adaa022d239f7f7dd69fab71e4965", null ],
    [ "GetSecondAdjEnergyMinForProdToProj", "d6/d89/classG4AdjointIonIonisationModel.html#a9fe5f3eef653fa59a26db3c2d5f847dd", null ],
    [ "GetSecondAdjEnergyMinForScatProjToProj", "d6/d89/classG4AdjointIonIonisationModel.html#af3daab11426c36ecd40497b9ec6fb152", null ],
    [ "operator=", "d6/d89/classG4AdjointIonIonisationModel.html#a40476afce67f36d5ae4decaa63c2d84c", null ],
    [ "SampleSecondaries", "d6/d89/classG4AdjointIonIonisationModel.html#a55457e61103b1bd40c02981fca41eb9b", null ],
    [ "SetIon", "d6/d89/classG4AdjointIonIonisationModel.html#accd43f10d2b06eb8d30478a6b506a1bf", null ],
    [ "SetUseOnlyBragg", "d6/d89/classG4AdjointIonIonisationModel.html#a15976c95ed4141c4b6ec512ddf998eb4", null ],
    [ "fBetheBlochDirectEMModel", "d6/d89/classG4AdjointIonIonisationModel.html#a7c6ffd827ba355232f7dca2a2f601c47", null ],
    [ "fBraggIonDirectEMModel", "d6/d89/classG4AdjointIonIonisationModel.html#af001c02424de43277731a33607b0a9fa", null ],
    [ "fChargeSquare", "d6/d89/classG4AdjointIonIonisationModel.html#a9db6d3aacb9ac2ec11259b7d9a967624", null ],
    [ "fFormFact", "d6/d89/classG4AdjointIonIonisationModel.html#a4e75161c4ca9c498267aca141cab41d3", null ],
    [ "fMagMoment2", "d6/d89/classG4AdjointIonIonisationModel.html#ae347c278f6d0cf98091ea9f1d8c8312e", null ],
    [ "fMass", "d6/d89/classG4AdjointIonIonisationModel.html#a81e4738735b206a13c8fbc2108d735db", null ],
    [ "fMassRatio", "d6/d89/classG4AdjointIonIonisationModel.html#a2d9c59008169bb2873bf4fb55c2998ab", null ],
    [ "fOneMinusRatio2", "d6/d89/classG4AdjointIonIonisationModel.html#a99d0d6597be92432a3117e87de975e6a", null ],
    [ "fOnePlusRatio2", "d6/d89/classG4AdjointIonIonisationModel.html#acaa2c6320168cfbe3dbe46c0e447f223", null ],
    [ "fRatio", "d6/d89/classG4AdjointIonIonisationModel.html#a81ded32540a492077610d357381c31e2", null ],
    [ "fSpin", "d6/d89/classG4AdjointIonIonisationModel.html#a1cfa3ac4c2f3609da1dd411068db0d98", null ],
    [ "fUseOnlyBragg", "d6/d89/classG4AdjointIonIonisationModel.html#adb3cb89543eecadb82b668440373c440", null ]
];