var classG4MuonicAtomHelper =
[
    [ "ConstructMuonicAtom", "d6/d86/classG4MuonicAtomHelper.html#a3524ca12fcfe6d6ee3eba2e2fb5d2b81", null ],
    [ "GetKShellEnergy", "d6/d86/classG4MuonicAtomHelper.html#a5894bae820bdd83be27afc57599b936c", null ],
    [ "GetLinApprox", "d6/d86/classG4MuonicAtomHelper.html#a8ff10f30270a1968492bc4671dca23c5", null ],
    [ "GetMuonCaptureRate", "d6/d86/classG4MuonicAtomHelper.html#a6d7b7ac8555d1c05b557f08012e391b7", null ],
    [ "GetMuonDecayRate", "d6/d86/classG4MuonicAtomHelper.html#ad9e65c4da17195d08c6d452536fc9d39", null ],
    [ "GetMuonZeff", "d6/d86/classG4MuonicAtomHelper.html#af576ec939ed1d86814a35dea3763dc32", null ]
];