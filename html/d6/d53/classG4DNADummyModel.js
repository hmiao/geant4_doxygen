var classG4DNADummyModel =
[
    [ "G4DNADummyModel", "d6/d53/classG4DNADummyModel.html#af833dfb24b9262ff1f20666d32a0fbef", null ],
    [ "~G4DNADummyModel", "d6/d53/classG4DNADummyModel.html#a2f81e8f38a5b9636d7e4d0cfed597614", null ],
    [ "CrossSectionPerVolume", "d6/d53/classG4DNADummyModel.html#a3858e5387c39f54ff28305bc4395119b", null ],
    [ "GetEmModel", "d6/d53/classG4DNADummyModel.html#a4d8e6d73886af56d05c89a1b97b510f2", null ],
    [ "GetEmModel", "d6/d53/classG4DNADummyModel.html#a388a6f56cd77b332c9cbff2769cc0f0a", null ],
    [ "GetNumMoleculePerVolumeUnitForMaterial", "d6/d53/classG4DNADummyModel.html#a97fdd81075ad568be5069c43d725247e", null ],
    [ "Initialise", "d6/d53/classG4DNADummyModel.html#a048ee9f465f839610a2ccb3ca6961215", null ],
    [ "SampleSecondaries", "d6/d53/classG4DNADummyModel.html#a8d2575ee9ca78091a1b828d1ad7f4de1", null ],
    [ "fMaterialMolPerVol", "d6/d53/classG4DNADummyModel.html#af6db2346f41448d99fc94f91c3b098d1", null ],
    [ "fpEmModel", "d6/d53/classG4DNADummyModel.html#a385defb427ca3ce0045d202f70b5c59f", null ],
    [ "fpParticleDef", "d6/d53/classG4DNADummyModel.html#a850ef996dfb1569314f057a23cff6425", null ]
];