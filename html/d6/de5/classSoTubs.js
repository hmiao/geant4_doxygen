var classSoTubs =
[
    [ "SoTubs", "d6/de5/classSoTubs.html#af2c15e69b20055799993f2c12313a928", null ],
    [ "~SoTubs", "d6/de5/classSoTubs.html#a397c939c4fa816670e85cf5c70b43527", null ],
    [ "clearAlternateRep", "d6/de5/classSoTubs.html#a5395fb49901d6f1d6bfbdb7efcd6487c", null ],
    [ "computeBBox", "d6/de5/classSoTubs.html#a3f98f7076b16ab92b4971c0dc8516232", null ],
    [ "generateAlternateRep", "d6/de5/classSoTubs.html#ad6335f3f039657f6ade1a5909279fa08", null ],
    [ "generateChildren", "d6/de5/classSoTubs.html#af03f285f66266613f46737a5ecfc974e", null ],
    [ "generatePrimitives", "d6/de5/classSoTubs.html#a613567030959c1addaef0c1e7170bdbf", null ],
    [ "getChildren", "d6/de5/classSoTubs.html#a2aed430cd870a11cab018fba33f317a3", null ],
    [ "inc", "d6/de5/classSoTubs.html#ad32334f2f9872df1f0b3d8b0c328e9ed", null ],
    [ "initClass", "d6/de5/classSoTubs.html#ae879399cd6b87b0ba824327a9892e43a", null ],
    [ "SO_NODE_HEADER", "d6/de5/classSoTubs.html#a604b73838750cd9a22530b910adec5f8", null ],
    [ "updateChildren", "d6/de5/classSoTubs.html#aea486bd2cbb24831f37b7f6be5f4e845", null ],
    [ "alternateRep", "d6/de5/classSoTubs.html#a60f7efbc0ffc9df9b21a944c0bf29180", null ],
    [ "children", "d6/de5/classSoTubs.html#a8982db61fb3946503e288ce52e8a694c", null ],
    [ "pDPhi", "d6/de5/classSoTubs.html#aa0a45dc46b6fc4bf918ed184cbabaab9", null ],
    [ "pDz", "d6/de5/classSoTubs.html#a504e25e7284af0e20ec3883cf140c15e", null ],
    [ "pRMax", "d6/de5/classSoTubs.html#afd3cf149502cc575074aa52d5741c7a7", null ],
    [ "pRMin", "d6/de5/classSoTubs.html#afc2f7c09f361291498546a872b7bcb67", null ],
    [ "pSPhi", "d6/de5/classSoTubs.html#adf9f8a4cb6631bd754bb639dfcb07c9d", null ]
];