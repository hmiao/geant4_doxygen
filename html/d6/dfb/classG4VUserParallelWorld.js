var classG4VUserParallelWorld =
[
    [ "G4VUserParallelWorld", "d6/dfb/classG4VUserParallelWorld.html#a781f8827d479cd14ab0c781fb607d705", null ],
    [ "~G4VUserParallelWorld", "d6/dfb/classG4VUserParallelWorld.html#ac8b73df3e695e787d122e0d8072b6753", null ],
    [ "Construct", "d6/dfb/classG4VUserParallelWorld.html#af09fd77e13d55a8442c8daca53c4d73d", null ],
    [ "ConstructSD", "d6/dfb/classG4VUserParallelWorld.html#a8ef6d943bfc7ac5248e1a141bcd5c29b", null ],
    [ "GetName", "d6/dfb/classG4VUserParallelWorld.html#a39798dd58ca70a06e58f634b6164f7c6", null ],
    [ "GetWorld", "d6/dfb/classG4VUserParallelWorld.html#a304248e8ea13f78c16014bae37d4a3fe", null ],
    [ "SetSensitiveDetector", "d6/dfb/classG4VUserParallelWorld.html#a552d66cc7b130185a0dd17db87f52865", null ],
    [ "SetSensitiveDetector", "d6/dfb/classG4VUserParallelWorld.html#a88cae79cd9478fee8dab5d39c9454d00", null ],
    [ "fWorldName", "d6/dfb/classG4VUserParallelWorld.html#aa61bab886bc581accda4983a195661d7", null ]
];