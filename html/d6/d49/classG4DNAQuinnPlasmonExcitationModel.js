var classG4DNAQuinnPlasmonExcitationModel =
[
    [ "G4DNAQuinnPlasmonExcitationModel", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#a99d051f84be28200ce0390869f2f3285", null ],
    [ "~G4DNAQuinnPlasmonExcitationModel", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#aeb8a7e8826a4d73bd9a0da322e0a5ec0", null ],
    [ "G4DNAQuinnPlasmonExcitationModel", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#ad09d8ed2257b2f3b7a1e5c2d2a9b1609", null ],
    [ "CrossSectionPerVolume", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#add610622d52f9cb83fd99db1e6885ea4", null ],
    [ "GetCrossSection", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#a623856cb36c3e9141f53a5db5cd159db", null ],
    [ "GetNValenceElectron", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#a068d1393b22c7d5bc3635eb04f56a5e7", null ],
    [ "Initialise", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#a53bc17e320efa56e41b89449640fd4cd", null ],
    [ "operator=", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#a20a32b50d4a3b76f259d4ee1d99d2fdb", null ],
    [ "SampleSecondaries", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#aca84a7734415550ac61125c4d3fcb67f", null ],
    [ "SelectStationary", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#a44c28948cca5cc0fae1c287b94aafb27", null ],
    [ "fHighEnergyLimit", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#ada2ce598652bb5935598709f3f70bcc9", null ],
    [ "fLowEnergyLimit", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#a93f9c9c6d18f0ae327680ef1150d081b", null ],
    [ "fParticleChangeForGamma", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#a7ef2f0e040dc36f6423199e4415c386b", null ],
    [ "fpMaterialDensity", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#ae7f9e78042b41a8c725243929ce37dd3", null ],
    [ "isInitialised", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#aa9aad306eae2af11104f2b3b355a668a", null ],
    [ "nValenceElectron", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#a4310317135eda067886fcd1a67b2b1f5", null ],
    [ "statCode", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#a37dfee3448e211e1b8160a9740cc6af4", null ],
    [ "verboseLevel", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html#a2d4ebc8f7b7964f715848ec427f50b83", null ]
];