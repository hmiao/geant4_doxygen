var classG4AdjointPhotoElectricModel =
[
    [ "G4AdjointPhotoElectricModel", "d6/d9d/classG4AdjointPhotoElectricModel.html#a83bb6ccf6bf7f80f7091e184129195b2", null ],
    [ "~G4AdjointPhotoElectricModel", "d6/d9d/classG4AdjointPhotoElectricModel.html#aaf9706fa0c5857377c5b01870c59c592", null ],
    [ "G4AdjointPhotoElectricModel", "d6/d9d/classG4AdjointPhotoElectricModel.html#a01d5a6657fad8fa985f76a565ce518d7", null ],
    [ "AdjointCrossSection", "d6/d9d/classG4AdjointPhotoElectricModel.html#a95235bf6a9d28b17aa1e29d0115dfbe9", null ],
    [ "AdjointCrossSectionPerAtom", "d6/d9d/classG4AdjointPhotoElectricModel.html#ade42f593c06c3f2253909229e84b3251", null ],
    [ "CorrectPostStepWeight", "d6/d9d/classG4AdjointPhotoElectricModel.html#a0bcfc257211eafae3f2808a1a806707c", null ],
    [ "DefineCurrentMaterialAndElectronEnergy", "d6/d9d/classG4AdjointPhotoElectricModel.html#a2132e89dc7cfe194d60d07901fb34aa5", null ],
    [ "operator=", "d6/d9d/classG4AdjointPhotoElectricModel.html#ad42b1f2a717fc80b6f1c4c0d680873f8", null ],
    [ "SampleSecondaries", "d6/d9d/classG4AdjointPhotoElectricModel.html#aaa23e70bf93e5015e3ebe4711014b752", null ],
    [ "fCurrenteEnergy", "d6/d9d/classG4AdjointPhotoElectricModel.html#a9047dc7b21af1e4e427a41c72fc7da84", null ],
    [ "fFactorCSBiasing", "d6/d9d/classG4AdjointPhotoElectricModel.html#ac5e804d9f0a9ba865b0f7caa22abba4f", null ],
    [ "fIndexElement", "d6/d9d/classG4AdjointPhotoElectricModel.html#a1737e88db186190fdd9b06a8cd3e69fb", null ],
    [ "fPostStepAdjointCS", "d6/d9d/classG4AdjointPhotoElectricModel.html#aa7b8fe0ba3fc9bd9f31b6d108d681c62", null ],
    [ "fPreStepAdjointCS", "d6/d9d/classG4AdjointPhotoElectricModel.html#a4319e52bd3a2f03a354d45f29000ca38", null ],
    [ "fShellProb", "d6/d9d/classG4AdjointPhotoElectricModel.html#a52fba664a9d756435cec73188a0b50be", null ],
    [ "fTotAdjointCS", "d6/d9d/classG4AdjointPhotoElectricModel.html#a108e09aadf132745fb793fbdde999636", null ],
    [ "fXsec", "d6/d9d/classG4AdjointPhotoElectricModel.html#aedb9cc2417161cfa69967e5af468fa94", null ]
];