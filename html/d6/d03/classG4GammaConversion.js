var classG4GammaConversion =
[
    [ "G4GammaConversion", "d6/d03/classG4GammaConversion.html#a979f11c091774556e515a80f4b0f324d", null ],
    [ "~G4GammaConversion", "d6/d03/classG4GammaConversion.html#a74915b01bf74942200c08daf9d07ebb7", null ],
    [ "G4GammaConversion", "d6/d03/classG4GammaConversion.html#a17398987cb0fa7224783f4b8ba06cd6f", null ],
    [ "InitialiseProcess", "d6/d03/classG4GammaConversion.html#a2b05404c6d2f23866067dba612a7a605", null ],
    [ "IsApplicable", "d6/d03/classG4GammaConversion.html#a4fa8cdd9191346147759193f83bfef1e", null ],
    [ "MinPrimaryEnergy", "d6/d03/classG4GammaConversion.html#ae17ae015fc40eadc1fa25ceee9fe9a2d", null ],
    [ "operator=", "d6/d03/classG4GammaConversion.html#aa9c0bd21d683648918c7593183bac472", null ],
    [ "ProcessDescription", "d6/d03/classG4GammaConversion.html#a4baa45aadfb3951af284cf54a27cd95b", null ],
    [ "isInitialised", "d6/d03/classG4GammaConversion.html#ab8b3d476a8a98392bc972e9338c9e30e", null ]
];