var classG4tgrIsotope =
[
    [ "G4tgrIsotope", "d6/da9/classG4tgrIsotope.html#a47c361f275db5245e2b713c1f9c183d7", null ],
    [ "~G4tgrIsotope", "d6/da9/classG4tgrIsotope.html#a93929cf9f72749112f30258c38a54a02", null ],
    [ "G4tgrIsotope", "d6/da9/classG4tgrIsotope.html#a0dd7e7e3100d79f07df963ad20459ec8", null ],
    [ "GetA", "d6/da9/classG4tgrIsotope.html#ac3de90773582b73cea5881c191898946", null ],
    [ "GetN", "d6/da9/classG4tgrIsotope.html#a267d3b21deb0caaaeb0129bf5ed470c5", null ],
    [ "GetName", "d6/da9/classG4tgrIsotope.html#ac37342c48db1c34ea63613ac2867e420", null ],
    [ "GetZ", "d6/da9/classG4tgrIsotope.html#addd8baf567e74781d06c69ec94028e86", null ],
    [ "operator<<", "d6/da9/classG4tgrIsotope.html#a7273033b85cef11dcdf92e511dfbc05c", null ],
    [ "theA", "d6/da9/classG4tgrIsotope.html#af1a7e20de1e10b41eeba9693d8572728", null ],
    [ "theN", "d6/da9/classG4tgrIsotope.html#a070aa6bad981cf573643469d9c510380", null ],
    [ "theName", "d6/da9/classG4tgrIsotope.html#af17c8f53b256e530f48a51bc4991ddfe", null ],
    [ "theZ", "d6/da9/classG4tgrIsotope.html#a288df4058f5103ae3587cc650cba4d62", null ]
];