var classG4GEMProbability =
[
    [ "G4GEMProbability", "d6/de2/classG4GEMProbability.html#a2d126fe526668ebc8fe085d256412be3", null ],
    [ "~G4GEMProbability", "d6/de2/classG4GEMProbability.html#a7c433ce3aa2b9f171b2ad4240bc996b9", null ],
    [ "G4GEMProbability", "d6/de2/classG4GEMProbability.html#a8dd3d64efe4647b80e62e07cc9545c7c", null ],
    [ "G4GEMProbability", "d6/de2/classG4GEMProbability.html#a0aedc713b26fb09b7eaaef92f2d6781a", null ],
    [ "CalcAlphaParam", "d6/de2/classG4GEMProbability.html#aa19464ce680b72f3b3a9f3f00cde5e57", null ],
    [ "CalcBetaParam", "d6/de2/classG4GEMProbability.html#abce4ba7ca27675bdf22381910ba09d26", null ],
    [ "CalcProbability", "d6/de2/classG4GEMProbability.html#a136a36cf4ec88a62c072628f6147089d", null ],
    [ "CCoeficient", "d6/de2/classG4GEMProbability.html#afcd35a4ce65b2ad6471e00c97beac004", null ],
    [ "Dump", "d6/de2/classG4GEMProbability.html#a426ebec3e1b76e34595b1b3c6d330c53", null ],
    [ "EmissionProbability", "d6/de2/classG4GEMProbability.html#abe48ca0e4f40e212118da5cf537c65bc", null ],
    [ "GetCoulombBarrier", "d6/de2/classG4GEMProbability.html#a17605b0afbb5ccab66c8270803ee2a82", null ],
    [ "GetSpin", "d6/de2/classG4GEMProbability.html#abeda99b44742bbc292d0ed84b827fb0c", null ],
    [ "I0", "d6/de2/classG4GEMProbability.html#a91919e99bacd366b3ab3f09f2d4f8c69", null ],
    [ "I1", "d6/de2/classG4GEMProbability.html#acf923c7fad08753d7432ef7e27022773", null ],
    [ "I2", "d6/de2/classG4GEMProbability.html#a7a7aa1210f22e6ea0e24868a18f81ffe", null ],
    [ "I3", "d6/de2/classG4GEMProbability.html#a39dc2add8c7389de856289316fec153b", null ],
    [ "operator!=", "d6/de2/classG4GEMProbability.html#a219a71e2d5868dd88135d1a609cb1eb4", null ],
    [ "operator=", "d6/de2/classG4GEMProbability.html#a2326ed165436c49c60a6b5eaaf8eb121", null ],
    [ "operator==", "d6/de2/classG4GEMProbability.html#a003030fece17a6308e7dfae080be4992", null ],
    [ "SetCoulomBarrier", "d6/de2/classG4GEMProbability.html#aeced3cb369238cef91ec85ab791cf82f", null ],
    [ "ExcitEnergies", "d6/de2/classG4GEMProbability.html#a5d12ced6e0d43f50c8bafb5c13ff1ded", null ],
    [ "ExcitLifetimes", "d6/de2/classG4GEMProbability.html#aa7a5058a2e334894a61a974f85641da5", null ],
    [ "ExcitSpins", "d6/de2/classG4GEMProbability.html#a1801a4778aa1aff6dd9eeea3f8e020d1", null ],
    [ "fG4pow", "d6/de2/classG4GEMProbability.html#ac0ba23f642fc0e5277163b9f7360bbb8", null ],
    [ "fNucData", "d6/de2/classG4GEMProbability.html#abb52779bb4a37a79d5f1a4121e0a1a81", null ],
    [ "fPlanck", "d6/de2/classG4GEMProbability.html#a95721b8242f68bd3b4401eefaba5766c", null ],
    [ "Spin", "d6/de2/classG4GEMProbability.html#a57c482cd38defb0c4cb37f4dd9257f65", null ],
    [ "theCoulombBarrierPtr", "d6/de2/classG4GEMProbability.html#a7eebc931018b24bce5d60421078f2502", null ],
    [ "theEvapLDPptr", "d6/de2/classG4GEMProbability.html#a70c4c4ead6f5fe04b8888c1e31a214d9", null ]
];