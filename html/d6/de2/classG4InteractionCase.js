var classG4InteractionCase =
[
    [ "G4InteractionCase", "d6/de2/classG4InteractionCase.html#a040f6ac5e58c7b91b0b391358863dcf7", null ],
    [ "G4InteractionCase", "d6/de2/classG4InteractionCase.html#ae904afa150db1fe7eb1ba283e0a6e0e5", null ],
    [ "clear", "d6/de2/classG4InteractionCase.html#a56cef599c3a56a5221be3e7e2441586a", null ],
    [ "code", "d6/de2/classG4InteractionCase.html#a40cfc3fdd3f23e98155608dac1b76289", null ],
    [ "getBullet", "d6/de2/classG4InteractionCase.html#a77450771d9ef025b9afdf3fe84429554", null ],
    [ "getTarget", "d6/de2/classG4InteractionCase.html#af7a5abd7f60851f8d3c4182619ef9cf4", null ],
    [ "hadNucleus", "d6/de2/classG4InteractionCase.html#ad9950000d845f49dcb78106bf806603f", null ],
    [ "hadrons", "d6/de2/classG4InteractionCase.html#a40fd37d8920bd5ca1006c3544a969262", null ],
    [ "set", "d6/de2/classG4InteractionCase.html#a7e8364365851fa002aa9458a2cdc6779", null ],
    [ "twoNuclei", "d6/de2/classG4InteractionCase.html#a6f0c65cafe2e2bf87383bc35f13f75a9", null ],
    [ "valid", "d6/de2/classG4InteractionCase.html#a749c81c809d5e63011030d7f8870915c", null ],
    [ "bullet", "d6/de2/classG4InteractionCase.html#af351e2d31c009bc23f4d9075052bf151", null ],
    [ "inter_case", "d6/de2/classG4InteractionCase.html#a5938b76ad7abd131e9877b83d86301a2", null ],
    [ "target", "d6/de2/classG4InteractionCase.html#a035ffbee3d2ccebf82d70db7813ba894", null ]
];