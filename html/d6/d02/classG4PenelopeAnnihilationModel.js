var classG4PenelopeAnnihilationModel =
[
    [ "G4PenelopeAnnihilationModel", "d6/d02/classG4PenelopeAnnihilationModel.html#a73145d41c9b27c042f42f405bd7dc340", null ],
    [ "~G4PenelopeAnnihilationModel", "d6/d02/classG4PenelopeAnnihilationModel.html#ad2fd33c5f4a09dd34be1e83b22eca6ee", null ],
    [ "G4PenelopeAnnihilationModel", "d6/d02/classG4PenelopeAnnihilationModel.html#a748b31343103f1a15224a0c739077a5e", null ],
    [ "ComputeCrossSectionPerAtom", "d6/d02/classG4PenelopeAnnihilationModel.html#ae1ec52434f6afcd935c5233431598f48", null ],
    [ "ComputeCrossSectionPerElectron", "d6/d02/classG4PenelopeAnnihilationModel.html#a329cd9a869ddb94d3a3661cb1d29ad22", null ],
    [ "GetVerbosityLevel", "d6/d02/classG4PenelopeAnnihilationModel.html#a9672706d79554f24c095f7c4ed29561c", null ],
    [ "Initialise", "d6/d02/classG4PenelopeAnnihilationModel.html#adcadc6cc11c9d6767ef5e10d658354f7", null ],
    [ "InitialiseLocal", "d6/d02/classG4PenelopeAnnihilationModel.html#a628e3344a7b98c78a12d00c4d3122b46", null ],
    [ "operator=", "d6/d02/classG4PenelopeAnnihilationModel.html#a39d85173460a4993f84def129b2e3f0c", null ],
    [ "SampleSecondaries", "d6/d02/classG4PenelopeAnnihilationModel.html#a1539db4934ec285d33117d92a16698bb", null ],
    [ "SetParticle", "d6/d02/classG4PenelopeAnnihilationModel.html#a554000145ce9c9bbe90ea76b396ff705", null ],
    [ "SetVerbosityLevel", "d6/d02/classG4PenelopeAnnihilationModel.html#ab8c365112fdff95f8013988a0d2dbbb1", null ],
    [ "fIntrinsicHighEnergyLimit", "d6/d02/classG4PenelopeAnnihilationModel.html#a3d69edaf3bc878764743b3d6e75a69ec", null ],
    [ "fIntrinsicLowEnergyLimit", "d6/d02/classG4PenelopeAnnihilationModel.html#a968fd56b1ecfb013173af28aa0391ca2", null ],
    [ "fIsInitialised", "d6/d02/classG4PenelopeAnnihilationModel.html#abca3a90eebc6af17b1dd3c5d60f13057", null ],
    [ "fParticle", "d6/d02/classG4PenelopeAnnihilationModel.html#a83c5129ec1df03de6ae661e5b36b64f7", null ],
    [ "fParticleChange", "d6/d02/classG4PenelopeAnnihilationModel.html#af211725c92a05c5a7034252bca7eade8", null ],
    [ "fPielr2", "d6/d02/classG4PenelopeAnnihilationModel.html#a186edce92a4ed695e92af54062930772", null ],
    [ "fVerboseLevel", "d6/d02/classG4PenelopeAnnihilationModel.html#aeb5505cd8642291a39088f16a49a3f9a", null ]
];