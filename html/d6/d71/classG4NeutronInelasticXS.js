var classG4NeutronInelasticXS =
[
    [ "G4NeutronInelasticXS", "d6/d71/classG4NeutronInelasticXS.html#a9dae8525f768a01a03c0f1af1294bf3d", null ],
    [ "~G4NeutronInelasticXS", "d6/d71/classG4NeutronInelasticXS.html#ab9be210003ae6f79b14c685e300e9308", null ],
    [ "G4NeutronInelasticXS", "d6/d71/classG4NeutronInelasticXS.html#ad065b1f68063c4c3daeff825d72c294a", null ],
    [ "BuildPhysicsTable", "d6/d71/classG4NeutronInelasticXS.html#aa0a76075e9bbc91a7bee71fa9210e80b", null ],
    [ "CrossSectionDescription", "d6/d71/classG4NeutronInelasticXS.html#ac053092e64a089b436c37e7ac55c8554", null ],
    [ "Default_Name", "d6/d71/classG4NeutronInelasticXS.html#a15e1dd06bb124dd0b84ee2d81673cec2", null ],
    [ "FindDirectoryPath", "d6/d71/classG4NeutronInelasticXS.html#a486dbf13c865ef9027bed90abd06644b", null ],
    [ "GetElementCrossSection", "d6/d71/classG4NeutronInelasticXS.html#ae7da9c4ee659f8c9fab188b0c48ab19a", null ],
    [ "GetIsoCrossSection", "d6/d71/classG4NeutronInelasticXS.html#af5feb07fcd9cccfc9a408d0770a816ed", null ],
    [ "GetPhysicsVector", "d6/d71/classG4NeutronInelasticXS.html#ac9e5adf4f8378e5a3326dd833b6a6525", null ],
    [ "Initialise", "d6/d71/classG4NeutronInelasticXS.html#a6e6823f588076027999fe083e79a9a85", null ],
    [ "InitialiseOnFly", "d6/d71/classG4NeutronInelasticXS.html#ad66ccb2811c160f03b74897bc987a717", null ],
    [ "IsElementApplicable", "d6/d71/classG4NeutronInelasticXS.html#a8f5148326f67726bf8eb730079c19493", null ],
    [ "IsIsoApplicable", "d6/d71/classG4NeutronInelasticXS.html#aa5fd05865c163a55eb6d62a1322e987b", null ],
    [ "IsoCrossSection", "d6/d71/classG4NeutronInelasticXS.html#a762fee1d850c7cc368cb9b8a4e8f677d", null ],
    [ "operator=", "d6/d71/classG4NeutronInelasticXS.html#a1cd655a2565bd01fdf038f36ea940ee2", null ],
    [ "RetrieveVector", "d6/d71/classG4NeutronInelasticXS.html#ad94dabf03ecd4b777ed5ce50cde4163c", null ],
    [ "SelectIsotope", "d6/d71/classG4NeutronInelasticXS.html#adfca71e63175c1b923bcd1da6b9c3cb3", null ],
    [ "coeff", "d6/d71/classG4NeutronInelasticXS.html#ae3fecec44ed1e479af18c77563fa6184", null ],
    [ "data", "d6/d71/classG4NeutronInelasticXS.html#a1f84ce2220c11aeb0882a0e6335de79e", null ],
    [ "gDataDirectory", "d6/d71/classG4NeutronInelasticXS.html#ae07bc6a5bb5b677ceb2a4dd9dcb5fa9b", null ],
    [ "ggXsection", "d6/d71/classG4NeutronInelasticXS.html#ab960790bb051e0324652830333949f67", null ],
    [ "isMaster", "d6/d71/classG4NeutronInelasticXS.html#a8a4fe9eba96a5ec03c2511c63ba73ef1", null ],
    [ "MAXZINEL", "d6/d71/classG4NeutronInelasticXS.html#a6f3f253e4a06deaa7fead3d00c764c99", null ],
    [ "neutron", "d6/d71/classG4NeutronInelasticXS.html#a9c8560f0102a88af6784e752c2a9c8f4", null ],
    [ "temp", "d6/d71/classG4NeutronInelasticXS.html#a9685526cc2f800dd49765cadd9f2d191", null ]
];