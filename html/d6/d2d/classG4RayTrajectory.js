var classG4RayTrajectory =
[
    [ "G4RayTrajectory", "d6/d2d/classG4RayTrajectory.html#a5ea29e0b4a5a68da03d74ef713ccbd61", null ],
    [ "G4RayTrajectory", "d6/d2d/classG4RayTrajectory.html#ae019906f66cbd85c0542fdf8c20ac21d", null ],
    [ "~G4RayTrajectory", "d6/d2d/classG4RayTrajectory.html#ab8f0d582208fe7c45e4d349023a276a5", null ],
    [ "AppendStep", "d6/d2d/classG4RayTrajectory.html#ab224097a11e6442679e60adc8f294393", null ],
    [ "DrawTrajectory", "d6/d2d/classG4RayTrajectory.html#af19a06c4f95c55b367361a9db16a50d0", null ],
    [ "GetCharge", "d6/d2d/classG4RayTrajectory.html#a4a2e3d73e469d4e3f5c4f9467ca9bc6d", null ],
    [ "GetInitialMomentum", "d6/d2d/classG4RayTrajectory.html#ad02bb664da59abe42cc361f0d8947c4d", null ],
    [ "GetParentID", "d6/d2d/classG4RayTrajectory.html#a9d50421ee245e2ba1b9789e22b8f8e23", null ],
    [ "GetParticleName", "d6/d2d/classG4RayTrajectory.html#aebbcb066fdbf962293895bd43777d1b2", null ],
    [ "GetPDGEncoding", "d6/d2d/classG4RayTrajectory.html#a8ba85ae5fcb4a01c4e444b01445d60bf", null ],
    [ "GetPoint", "d6/d2d/classG4RayTrajectory.html#a087db5bc2ad9c75132e282cbf6d8b933", null ],
    [ "GetPointC", "d6/d2d/classG4RayTrajectory.html#a537b3d6a3bbebfdd2429555c04a349f1", null ],
    [ "GetPointEntries", "d6/d2d/classG4RayTrajectory.html#a0f4d85344b1c0d08f9789f7d48500abe", null ],
    [ "GetTrackID", "d6/d2d/classG4RayTrajectory.html#a8cd3e162ad66915bdfe231c49ed3396c", null ],
    [ "MergeTrajectory", "d6/d2d/classG4RayTrajectory.html#aab9a97835e85abe661422cb8e86cebc3", null ],
    [ "operator delete", "d6/d2d/classG4RayTrajectory.html#a56cb4485ed71412a3719a3de894d4bef", null ],
    [ "operator new", "d6/d2d/classG4RayTrajectory.html#a9831657ed1f4f393eeac2f0de15fc1bf", null ],
    [ "operator=", "d6/d2d/classG4RayTrajectory.html#ae18313b526f3b53582c14723a8d767d3", null ],
    [ "ShowTrajectory", "d6/d2d/classG4RayTrajectory.html#a116d2e17c2eacc2e3db1abc9123293b8", null ],
    [ "positionRecord", "d6/d2d/classG4RayTrajectory.html#ae3f7dde83ec85f001ff4bfe8d9e93543", null ]
];