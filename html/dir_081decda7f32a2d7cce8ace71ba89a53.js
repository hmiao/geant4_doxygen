var dir_081decda7f32a2d7cce8ace71ba89a53 =
[
    [ "G4OpAbsorption.hh", "db/deb/G4OpAbsorption_8hh.html", "db/deb/G4OpAbsorption_8hh" ],
    [ "G4OpBoundaryProcess.hh", "d7/dc4/G4OpBoundaryProcess_8hh.html", "d7/dc4/G4OpBoundaryProcess_8hh" ],
    [ "G4OpMieHG.hh", "df/d51/G4OpMieHG_8hh.html", "df/d51/G4OpMieHG_8hh" ],
    [ "G4OpProcessSubType.hh", "d9/d28/G4OpProcessSubType_8hh.html", "d9/d28/G4OpProcessSubType_8hh" ],
    [ "G4OpRayleigh.hh", "d4/d10/G4OpRayleigh_8hh.html", "d4/d10/G4OpRayleigh_8hh" ],
    [ "G4OpWLS.hh", "d4/da8/G4OpWLS_8hh.html", "d4/da8/G4OpWLS_8hh" ],
    [ "G4OpWLS2.hh", "d2/d9f/G4OpWLS2_8hh.html", "d2/d9f/G4OpWLS2_8hh" ],
    [ "G4VWLSTimeGeneratorProfile.hh", "d6/de7/G4VWLSTimeGeneratorProfile_8hh.html", "d6/de7/G4VWLSTimeGeneratorProfile_8hh" ],
    [ "G4WLSTimeGeneratorProfileDelta.hh", "d5/d79/G4WLSTimeGeneratorProfileDelta_8hh.html", "d5/d79/G4WLSTimeGeneratorProfileDelta_8hh" ],
    [ "G4WLSTimeGeneratorProfileExponential.hh", "d3/d6a/G4WLSTimeGeneratorProfileExponential_8hh.html", "d3/d6a/G4WLSTimeGeneratorProfileExponential_8hh" ]
];