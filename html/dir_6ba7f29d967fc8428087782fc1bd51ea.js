var dir_6ba7f29d967fc8428087782fc1bd51ea =
[
    [ "G4MaterialCutsCouple.hh", "dd/dff/G4MaterialCutsCouple_8hh.html", "dd/dff/G4MaterialCutsCouple_8hh" ],
    [ "G4MCCIndexConversionTable.hh", "d1/d2f/G4MCCIndexConversionTable_8hh.html", "d1/d2f/G4MCCIndexConversionTable_8hh" ],
    [ "G4PhysicsTableHelper.hh", "d0/d9d/G4PhysicsTableHelper_8hh.html", "d0/d9d/G4PhysicsTableHelper_8hh" ],
    [ "G4ProductionCuts.hh", "d4/d6a/G4ProductionCuts_8hh.html", "d4/d6a/G4ProductionCuts_8hh" ],
    [ "G4ProductionCutsTable.hh", "dd/d0a/G4ProductionCutsTable_8hh.html", "dd/d0a/G4ProductionCutsTable_8hh" ],
    [ "G4ProductionCutsTableMessenger.hh", "d0/d4a/G4ProductionCutsTableMessenger_8hh.html", "d0/d4a/G4ProductionCutsTableMessenger_8hh" ],
    [ "G4RToEConvForElectron.hh", "d9/dd6/G4RToEConvForElectron_8hh.html", "d9/dd6/G4RToEConvForElectron_8hh" ],
    [ "G4RToEConvForGamma.hh", "d6/da6/G4RToEConvForGamma_8hh.html", "d6/da6/G4RToEConvForGamma_8hh" ],
    [ "G4RToEConvForPositron.hh", "d3/d81/G4RToEConvForPositron_8hh.html", "d3/d81/G4RToEConvForPositron_8hh" ],
    [ "G4RToEConvForProton.hh", "d6/d28/G4RToEConvForProton_8hh.html", "d6/d28/G4RToEConvForProton_8hh" ],
    [ "G4VRangeToEnergyConverter.hh", "d4/dbd/G4VRangeToEnergyConverter_8hh.html", "d4/dbd/G4VRangeToEnergyConverter_8hh" ]
];