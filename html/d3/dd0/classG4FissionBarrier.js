var classG4FissionBarrier =
[
    [ "G4FissionBarrier", "d3/dd0/classG4FissionBarrier.html#a5874c36cf41853eb6168c2ede3c27e47", null ],
    [ "~G4FissionBarrier", "d3/dd0/classG4FissionBarrier.html#a9b01b1dd248ac0fa990c3f5b5bfbab56", null ],
    [ "G4FissionBarrier", "d3/dd0/classG4FissionBarrier.html#ae32c1c804f8f00c36cbf5b412e6e2682", null ],
    [ "BarashenkovFissionBarrier", "d3/dd0/classG4FissionBarrier.html#a45e9492e3eca43422d5090c5bf551035", null ],
    [ "FissionBarrier", "d3/dd0/classG4FissionBarrier.html#a0be74c5f44b18b2a89b3b83a3b818e0b", null ],
    [ "operator!=", "d3/dd0/classG4FissionBarrier.html#a829aadf3cb9d0172041dc4f65a0e5407", null ],
    [ "operator=", "d3/dd0/classG4FissionBarrier.html#a24996965d0705305f3eef3d409437650", null ],
    [ "operator==", "d3/dd0/classG4FissionBarrier.html#a87b724a513563915dac5d74e0d5cdfb9", null ],
    [ "SPtr", "d3/dd0/classG4FissionBarrier.html#a4c52332e77bf370c89202e17bd4b79b3", null ]
];