var classG4tgrMaterialMixture =
[
    [ "G4tgrMaterialMixture", "d3/df7/classG4tgrMaterialMixture.html#a514130a761b500d1d2d08eed7c0f2f63", null ],
    [ "~G4tgrMaterialMixture", "d3/df7/classG4tgrMaterialMixture.html#acde0532f74cfa54db08d0d2bbbb5442f", null ],
    [ "G4tgrMaterialMixture", "d3/df7/classG4tgrMaterialMixture.html#a9bdc0fb6b6e2cebdb74dfac70333ba16", null ],
    [ "GetA", "d3/df7/classG4tgrMaterialMixture.html#a44539b28fa7dadcd49478720e3cb4a94", null ],
    [ "GetComponent", "d3/df7/classG4tgrMaterialMixture.html#a14eb9a3cbc8e8ccfcee5b5d4cfad0af0", null ],
    [ "GetFraction", "d3/df7/classG4tgrMaterialMixture.html#a1a99129377197fe6be2f672b123d92ad", null ],
    [ "GetZ", "d3/df7/classG4tgrMaterialMixture.html#ad3d774778f3d0ad801362ff0212498a1", null ],
    [ "TransformToFractionsByWeight", "d3/df7/classG4tgrMaterialMixture.html#a24dbf5d3a6e1ba2f99d3a7128e41ce1d", null ],
    [ "operator<<", "d3/df7/classG4tgrMaterialMixture.html#a6c64184792db000357dc1a16f8d25b6d", null ],
    [ "theComponents", "d3/df7/classG4tgrMaterialMixture.html#a4060f2e895fedbff3b3d85cc0e42e920", null ],
    [ "theFractions", "d3/df7/classG4tgrMaterialMixture.html#a2503d5e270b4cc3af2602f23c05f40f1", null ]
];