var structG4TNtupleDescription =
[
    [ "G4TNtupleDescription", "d3/dae/structG4TNtupleDescription.html#a2fdf003b0feb709a3a54712708005a4e", null ],
    [ "G4TNtupleDescription", "d3/dae/structG4TNtupleDescription.html#ad71ec21cecbb60b69297bafa9b174ef7", null ],
    [ "~G4TNtupleDescription", "d3/dae/structG4TNtupleDescription.html#a47e00d05f661e102cd98a1e15af1ce57", null ],
    [ "fActivation", "d3/dae/structG4TNtupleDescription.html#ac07bb7fda372bd7d2b3d57e39a41c8c2", null ],
    [ "fFile", "d3/dae/structG4TNtupleDescription.html#a8a7f901ba4fa0b1fdaae34ea6ecd5f56", null ],
    [ "fFileName", "d3/dae/structG4TNtupleDescription.html#adbd9c30757a83bc153e1a568c23b1b2e", null ],
    [ "fHasFill", "d3/dae/structG4TNtupleDescription.html#ad290d2b419a7c92e45220e0b9447b3d5", null ],
    [ "fIsNtupleOwner", "d3/dae/structG4TNtupleDescription.html#ab0f3846ff226a9f2041a633c567be5ec", null ],
    [ "fNtuple", "d3/dae/structG4TNtupleDescription.html#a9506c8ce9f0b766281cbbd5640149e95", null ],
    [ "fNtupleBooking", "d3/dae/structG4TNtupleDescription.html#a2a0b5a8c09d2abd163999f2e69db7d40", null ]
];