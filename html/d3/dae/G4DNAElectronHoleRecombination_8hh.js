var G4DNAElectronHoleRecombination_8hh =
[
    [ "G4DNAElectronHoleRecombination", "db/dfe/classG4DNAElectronHoleRecombination.html", "db/dfe/classG4DNAElectronHoleRecombination" ],
    [ "G4DNAElectronHoleRecombination::ReactantInfo", "de/d73/structG4DNAElectronHoleRecombination_1_1ReactantInfo.html", "de/d73/structG4DNAElectronHoleRecombination_1_1ReactantInfo" ],
    [ "G4DNAElectronHoleRecombination::State", "df/d54/structG4DNAElectronHoleRecombination_1_1State.html", "df/d54/structG4DNAElectronHoleRecombination_1_1State" ]
];