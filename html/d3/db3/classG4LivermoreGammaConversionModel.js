var classG4LivermoreGammaConversionModel =
[
    [ "G4LivermoreGammaConversionModel", "d3/db3/classG4LivermoreGammaConversionModel.html#adb7aab59eac4a9a9523de8f5ae6c605b", null ],
    [ "~G4LivermoreGammaConversionModel", "d3/db3/classG4LivermoreGammaConversionModel.html#aaa998a55172909675b334069aea6a0f8", null ],
    [ "G4LivermoreGammaConversionModel", "d3/db3/classG4LivermoreGammaConversionModel.html#a3126bc7083652bd6c2f2f78490b47df0", null ],
    [ "ComputeCrossSectionPerAtom", "d3/db3/classG4LivermoreGammaConversionModel.html#a79e8cbab8727ab51101dd643e433b574", null ],
    [ "Initialise", "d3/db3/classG4LivermoreGammaConversionModel.html#ac97d156e480247fd56fc5c5210a44b83", null ],
    [ "InitialiseForElement", "d3/db3/classG4LivermoreGammaConversionModel.html#a992d3388d6ef7d1a35058ba9da5f3a29", null ],
    [ "operator=", "d3/db3/classG4LivermoreGammaConversionModel.html#a6eeb447b9d0cab82ecbc5143f38ae45a", null ],
    [ "ReadData", "d3/db3/classG4LivermoreGammaConversionModel.html#abd27b3e3b2eb995dc8092a04c8e0cf64", null ],
    [ "data", "d3/db3/classG4LivermoreGammaConversionModel.html#a0d945c99282b11def0a07eebe97c6ee1", null ],
    [ "fParticleChange", "d3/db3/classG4LivermoreGammaConversionModel.html#a6787d5c8e10b92e515ef3c3afb92ff4d", null ],
    [ "lowEnergyLimit", "d3/db3/classG4LivermoreGammaConversionModel.html#abbd5b14d8235b097b0865f0f6901bef5", null ],
    [ "maxZ", "d3/db3/classG4LivermoreGammaConversionModel.html#ad712833a135be20274568617b3c8c058", null ],
    [ "verboseLevel", "d3/db3/classG4LivermoreGammaConversionModel.html#a82e8de4035680b94caf607195878173c", null ]
];