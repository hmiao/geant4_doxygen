var classG4teoCrossSection =
[
    [ "G4teoCrossSection", "d3/dc8/classG4teoCrossSection.html#af5c68a7bff723d47fed18ee6f4343f92", null ],
    [ "~G4teoCrossSection", "d3/dc8/classG4teoCrossSection.html#ae74bb45b445b8f17f528ce52b45af4a6", null ],
    [ "G4teoCrossSection", "d3/dc8/classG4teoCrossSection.html#a7f714070d806ad9590b1c3349b394125", null ],
    [ "CrossSection", "d3/dc8/classG4teoCrossSection.html#a809fe7fb935907b587d52b031e02f65f", null ],
    [ "GetCrossSection", "d3/dc8/classG4teoCrossSection.html#a9103861268163693abcd87b8f1b26466", null ],
    [ "operator=", "d3/dc8/classG4teoCrossSection.html#a70950357fa90d24719ccc5c78ca9f62e", null ],
    [ "Probabilities", "d3/dc8/classG4teoCrossSection.html#ae5f2ac6b10b5a3bb1f679c588517faf9", null ],
    [ "SetTotalCS", "d3/dc8/classG4teoCrossSection.html#a1cabcc032130fe12df0ae4c0f058a4bf", null ],
    [ "ecpssrShellK", "d3/dc8/classG4teoCrossSection.html#af7deb04e2507a5296df138252c80637e", null ],
    [ "ecpssrShellLi", "d3/dc8/classG4teoCrossSection.html#a53f4e4ff650862e2ff4641f75da9d0d2", null ],
    [ "ecpssrShellMi", "d3/dc8/classG4teoCrossSection.html#a83260164859405f671004991e6e65a73", null ],
    [ "totalCS", "d3/dc8/classG4teoCrossSection.html#a3bcdfd1b8c63032773e75c4946543201", null ]
];