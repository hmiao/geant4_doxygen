var classG4ParameterisationTrdX =
[
    [ "G4ParameterisationTrdX", "d3/d35/classG4ParameterisationTrdX.html#a5a73ee905d5c23f7ed328d16a87ff915", null ],
    [ "~G4ParameterisationTrdX", "d3/d35/classG4ParameterisationTrdX.html#a5daf6df2d83cb86371901def650e40dd", null ],
    [ "ComputeDimensions", "d3/d35/classG4ParameterisationTrdX.html#a05832cac936779a4a2c11dcebb044cc4", null ],
    [ "ComputeDimensions", "d3/d35/classG4ParameterisationTrdX.html#ac5d620d7c65d513e39ba8bb24575797a", null ],
    [ "ComputeDimensions", "d3/d35/classG4ParameterisationTrdX.html#ac65429edf2721147d2b2c259246bc905", null ],
    [ "ComputeDimensions", "d3/d35/classG4ParameterisationTrdX.html#a6170bf3de152dcec9c4d10e2a3b09d82", null ],
    [ "ComputeDimensions", "d3/d35/classG4ParameterisationTrdX.html#a9b0f78eb71d65686a05323f78e95dd47", null ],
    [ "ComputeDimensions", "d3/d35/classG4ParameterisationTrdX.html#a47c226df4724855fd9e2e21727c21dfd", null ],
    [ "ComputeDimensions", "d3/d35/classG4ParameterisationTrdX.html#af8467a91bf9c710fc0bd00edbb003826", null ],
    [ "ComputeDimensions", "d3/d35/classG4ParameterisationTrdX.html#a44f75e2093ac8736e2283e4f249a62e0", null ],
    [ "ComputeDimensions", "d3/d35/classG4ParameterisationTrdX.html#a55f2069f921f857207ec53d3e3e1df7e", null ],
    [ "ComputeDimensions", "d3/d35/classG4ParameterisationTrdX.html#afe03ac252d089ab40f20c532ea2d6ae8", null ],
    [ "ComputeDimensions", "d3/d35/classG4ParameterisationTrdX.html#a8bf05ab82830aa3d6ff9f4a70bbe2952", null ],
    [ "ComputeDimensions", "d3/d35/classG4ParameterisationTrdX.html#a3721c3323c01278fec131eba15f0022d", null ],
    [ "ComputeDimensions", "d3/d35/classG4ParameterisationTrdX.html#a50e6b39c1d8d93d9ce9f4e80822580e4", null ],
    [ "ComputeTransformation", "d3/d35/classG4ParameterisationTrdX.html#a050545e20fe7186094e544d618631353", null ],
    [ "GetMaxParameter", "d3/d35/classG4ParameterisationTrdX.html#a98c55b209a708511f1a7262f8acb7e98", null ]
];