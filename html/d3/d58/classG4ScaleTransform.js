var classG4ScaleTransform =
[
    [ "G4ScaleTransform", "d3/d58/classG4ScaleTransform.html#ab9fec14f1338d265db33edf9d8c7e75a", null ],
    [ "G4ScaleTransform", "d3/d58/classG4ScaleTransform.html#a6fde511409f75d4e05f1fc4e614bad70", null ],
    [ "G4ScaleTransform", "d3/d58/classG4ScaleTransform.html#a4eb6aabd8f890489041fa5c9f15d4dd9", null ],
    [ "G4ScaleTransform", "d3/d58/classG4ScaleTransform.html#acfd83b7be02972391af73bc60b87f137", null ],
    [ "G4ScaleTransform", "d3/d58/classG4ScaleTransform.html#a4f4e3b7b33f44ea37a9d0e7373c8ead1", null ],
    [ "GetInvScale", "d3/d58/classG4ScaleTransform.html#a7d12129070020dbddd07dfd995644a48", null ],
    [ "GetScale", "d3/d58/classG4ScaleTransform.html#a978c11c8ded9593cd88fc2728ab12551", null ],
    [ "Init", "d3/d58/classG4ScaleTransform.html#a3715812260e642392ffc8335c58f84f8", null ],
    [ "InverseTransform", "d3/d58/classG4ScaleTransform.html#ae82ca4b09741edb2465d8236e6dc3008", null ],
    [ "InverseTransform", "d3/d58/classG4ScaleTransform.html#a745ce1cb973d0ccd5f79c86d2fa28ee6", null ],
    [ "InverseTransformDistance", "d3/d58/classG4ScaleTransform.html#afd0e0af64d46b7a3ff2d6c4cdc53d5fd", null ],
    [ "InverseTransformDistance", "d3/d58/classG4ScaleTransform.html#acb7ad51927c54fba81b55018291bc7af", null ],
    [ "InverseTransformNormal", "d3/d58/classG4ScaleTransform.html#af74e1a3099837b9a2c36777c89fa2b8a", null ],
    [ "InverseTransformNormal", "d3/d58/classG4ScaleTransform.html#a3b5288189769d55229beb880b171c806", null ],
    [ "operator=", "d3/d58/classG4ScaleTransform.html#acf893c8760544086bcca8e55d13ed497", null ],
    [ "SetScale", "d3/d58/classG4ScaleTransform.html#a10e8a4e61999bd465e78cea5f2e73f19", null ],
    [ "SetScale", "d3/d58/classG4ScaleTransform.html#af2880308d093df26582b18cd8a77c2fc", null ],
    [ "SetScale", "d3/d58/classG4ScaleTransform.html#a3582fb5b850987f9ec44821b27a4738e", null ],
    [ "Transform", "d3/d58/classG4ScaleTransform.html#a633c61da2e64de31554747199e156fe0", null ],
    [ "Transform", "d3/d58/classG4ScaleTransform.html#add450fe4fd9ab87a2f309c67a1fe803c", null ],
    [ "TransformDistance", "d3/d58/classG4ScaleTransform.html#a8b2b86dd8434c12c8d431b7af007fbd0", null ],
    [ "TransformDistance", "d3/d58/classG4ScaleTransform.html#a2aff521fb0a8d5865e531d4d542810fe", null ],
    [ "TransformNormal", "d3/d58/classG4ScaleTransform.html#a51927f3936f1e20b7cc9d70e98535b81", null ],
    [ "TransformNormal", "d3/d58/classG4ScaleTransform.html#a26a29ef54760f260e9351f54c3f67161", null ],
    [ "fgFactor", "d3/d58/classG4ScaleTransform.html#a7b08ee629c718f9c6a09b8230a0f9435", null ],
    [ "fIScale", "d3/d58/classG4ScaleTransform.html#a79531aee1589aab14f75890d26ecfe70", null ],
    [ "flFactor", "d3/d58/classG4ScaleTransform.html#a8a6495b54e8168a3bdb1d4b205926d4c", null ],
    [ "fScale", "d3/d58/classG4ScaleTransform.html#a1f376c52a41739a3a88d067a687847ce", null ]
];