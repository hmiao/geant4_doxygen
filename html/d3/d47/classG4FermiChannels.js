var classG4FermiChannels =
[
    [ "G4FermiChannels", "d3/d47/classG4FermiChannels.html#a11469694371b0d182ab92077ba69422b", null ],
    [ "G4FermiChannels", "d3/d47/classG4FermiChannels.html#a7aa81d8801de39a4345088c518cb306b", null ],
    [ "AddChannel", "d3/d47/classG4FermiChannels.html#a2e7cdb426ab13b7389c61fdad33c7688", null ],
    [ "GetChannels", "d3/d47/classG4FermiChannels.html#aad1608efb9f9e93f58f8a3168e8f147e", null ],
    [ "GetExcitation", "d3/d47/classG4FermiChannels.html#a1b547186b98abe8f42d440eb2374cd21", null ],
    [ "GetMass", "d3/d47/classG4FermiChannels.html#a2b9c3a8448a85553731b65f5ec9d262b", null ],
    [ "GetNumberOfChannels", "d3/d47/classG4FermiChannels.html#a92f2dc05cb6b51085598eac0f8d2ec77", null ],
    [ "GetPair", "d3/d47/classG4FermiChannels.html#a92eb3124f6aa15170fa3394b2099f7bf", null ],
    [ "GetProbabilities", "d3/d47/classG4FermiChannels.html#afe1306fb3dfe8c8cf3a13ddb85538b20", null ],
    [ "operator!=", "d3/d47/classG4FermiChannels.html#adda56dc6f51b706ea72c51047d88d2cd", null ],
    [ "operator=", "d3/d47/classG4FermiChannels.html#a3a6e07e6df295c20e6d34b147ff36c00", null ],
    [ "operator==", "d3/d47/classG4FermiChannels.html#ab02432a655109faa0de61101109aa362", null ],
    [ "SamplePair", "d3/d47/classG4FermiChannels.html#a2b950019656c9108fa832336a07f7663", null ],
    [ "cum_prob", "d3/d47/classG4FermiChannels.html#adeeb6d3d60e88616be9cb0a246204b9f", null ],
    [ "excitation", "d3/d47/classG4FermiChannels.html#aada217c39ec79a1645a57f569f2dda85", null ],
    [ "fvect", "d3/d47/classG4FermiChannels.html#a55f2313e296b97a38d0e4d556cfb711d", null ],
    [ "ground_mass", "d3/d47/classG4FermiChannels.html#a043a12628fa27a6b14b1a1628aea24bd", null ],
    [ "nch", "d3/d47/classG4FermiChannels.html#a0bb8c64c23d4e64741f04aa0bba28605", null ]
];