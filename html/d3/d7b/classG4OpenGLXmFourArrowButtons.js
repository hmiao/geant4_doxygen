var classG4OpenGLXmFourArrowButtons =
[
    [ "G4OpenGLXmFourArrowButtons", "d3/d7b/classG4OpenGLXmFourArrowButtons.html#afe62828ea001346e385133760c5641ea", null ],
    [ "~G4OpenGLXmFourArrowButtons", "d3/d7b/classG4OpenGLXmFourArrowButtons.html#a8a637d30414f5ef1c430e2b6b5c9b0f9", null ],
    [ "G4OpenGLXmFourArrowButtons", "d3/d7b/classG4OpenGLXmFourArrowButtons.html#a136673dd010ba096e65aa5bdc955cd2c", null ],
    [ "AddYourselfTo", "d3/d7b/classG4OpenGLXmFourArrowButtons.html#a433c2c5c264be4f04bd232667730054e", null ],
    [ "GetName", "d3/d7b/classG4OpenGLXmFourArrowButtons.html#aff0c86cb67afbda1eb8de3aa79667d9d", null ],
    [ "GetPointerToParent", "d3/d7b/classG4OpenGLXmFourArrowButtons.html#a04d3d87fe8c02ef10bc46636e8ff1d23", null ],
    [ "GetPointerToWidget", "d3/d7b/classG4OpenGLXmFourArrowButtons.html#acb48be40127e30dde8195809deceed62", null ],
    [ "operator=", "d3/d7b/classG4OpenGLXmFourArrowButtons.html#a37e542c2299ac119a775613a9b5136d8", null ],
    [ "SetName", "d3/d7b/classG4OpenGLXmFourArrowButtons.html#a9e5af8aa99ec49d9180eb0c46812af8c", null ],
    [ "arrow", "d3/d7b/classG4OpenGLXmFourArrowButtons.html#ac43e2bde87eb591fc5e5e9df23b4e629", null ],
    [ "arrow_form", "d3/d7b/classG4OpenGLXmFourArrowButtons.html#a71c0835447895ec48eff3a0039a716d2", null ],
    [ "callback", "d3/d7b/classG4OpenGLXmFourArrowButtons.html#aee692e119ff79d0064c09bbb1d6d23e7", null ],
    [ "parent", "d3/d7b/classG4OpenGLXmFourArrowButtons.html#aeaa3eb0c02ae794093b5de92e559eda4", null ]
];