var classG4GFlashSpot =
[
    [ "G4GFlashSpot", "d3/d08/classG4GFlashSpot.html#ab163eb45ae7974a3650743943eb4ad6f", null ],
    [ "~G4GFlashSpot", "d3/d08/classG4GFlashSpot.html#a27bae72b9aa0431d20927cbb74b3a8c3", null ],
    [ "GetEnergySpot", "d3/d08/classG4GFlashSpot.html#aa6ba0f02d643811628a73bdba42112cf", null ],
    [ "GetOriginatorTrack", "d3/d08/classG4GFlashSpot.html#a69db86c58e65709cb6528ec47ff3621e", null ],
    [ "GetPosition", "d3/d08/classG4GFlashSpot.html#a1d5e92058a7a9a5d5587689be73c13a8", null ],
    [ "GetTouchableHandle", "d3/d08/classG4GFlashSpot.html#adf6a0be755be82af37eeed36a3a81d92", null ],
    [ "theHandle", "d3/d08/classG4GFlashSpot.html#a287d3a8a58875c2a569c0e8dad6972bd", null ],
    [ "theSpot", "d3/d08/classG4GFlashSpot.html#a805258e2dfd69b773fbcd0dc669aaa13", null ],
    [ "theTrack", "d3/d08/classG4GFlashSpot.html#aa89b052bde3a9ad7911fea214a900467", null ]
];