var classG4FTFParticipants =
[
    [ "G4FTFParticipants", "d3/d32/classG4FTFParticipants.html#a05bb0f5948affbb03556a22f59ba2bcb", null ],
    [ "~G4FTFParticipants", "d3/d32/classG4FTFParticipants.html#abd60db308478a895be742459b6cb758b", null ],
    [ "G4FTFParticipants", "d3/d32/classG4FTFParticipants.html#a611299da020ee6b2727683888303bf6e", null ],
    [ "Clean", "d3/d32/classG4FTFParticipants.html#a13144cbbcd600e3de511c79400a10a52", null ],
    [ "GetBmax2", "d3/d32/classG4FTFParticipants.html#ae0f6ca099d4fed8d7ef4d2c90d3d76bc", null ],
    [ "GetBmin2", "d3/d32/classG4FTFParticipants.html#a25f6d9bef9451f01ae5a3313a7d16907", null ],
    [ "GetImpactParameter", "d3/d32/classG4FTFParticipants.html#aa816366cedb20e590494b1cb273bc89e", null ],
    [ "GetInteraction", "d3/d32/classG4FTFParticipants.html#ae1c672b8b57b2cb3b94952525e9703d1", null ],
    [ "GetList", "d3/d32/classG4FTFParticipants.html#a6321f640dc8dc559fa8efa6de07dbb4e", null ],
    [ "Next", "d3/d32/classG4FTFParticipants.html#ac5115e1513de374664c02f8d4c5359ef", null ],
    [ "operator!=", "d3/d32/classG4FTFParticipants.html#ac5198548c4d5bd8f9cbabac93a781507", null ],
    [ "operator=", "d3/d32/classG4FTFParticipants.html#a5e6560565b9bfdd37f0d4a94ddc69fae", null ],
    [ "operator==", "d3/d32/classG4FTFParticipants.html#ac89d8eeb17ed33fd05c335912112ac55", null ],
    [ "SampleBinInterval", "d3/d32/classG4FTFParticipants.html#a831f4469e4b64793fa60b5eaa9653cb9", null ],
    [ "SetBminBmax", "d3/d32/classG4FTFParticipants.html#a6a61f02b7a37085d5e6d1b014b671ed9", null ],
    [ "SetImpactParameter", "d3/d32/classG4FTFParticipants.html#ac159847474e52a14c6095f6fc30f16f4", null ],
    [ "ShiftInteractionTime", "d3/d32/classG4FTFParticipants.html#a1a12e4119a42185640a83d6e08fb711a", null ],
    [ "SortInteractionsIncT", "d3/d32/classG4FTFParticipants.html#aade31a4fdd89642dba2868aba70dd0a4", null ],
    [ "StartLoop", "d3/d32/classG4FTFParticipants.html#ac939a2cbc20a2cdd1e3500c9c32d48bb", null ],
    [ "Bimpact", "d3/d32/classG4FTFParticipants.html#aaea04b9f79bb5fd562df7ba45039b8e3", null ],
    [ "BinInterval", "d3/d32/classG4FTFParticipants.html#a6e578e9bfe00a5925ecf3add0f5bb546", null ],
    [ "Bmax2", "d3/d32/classG4FTFParticipants.html#a88b92b78708906a5b4a0db50576cb528", null ],
    [ "Bmin2", "d3/d32/classG4FTFParticipants.html#a294c40fd2435ffe052c6c546413e1b3c", null ],
    [ "currentInteraction", "d3/d32/classG4FTFParticipants.html#ae788773ea151fd8b3d14dcfb0bff459c", null ],
    [ "theInteractions", "d3/d32/classG4FTFParticipants.html#a02f4f31ba6c5487c3e95db7e56a2ebd8", null ]
];