var classG4VEntanglementClipBoard =
[
    [ "G4VEntanglementClipBoard", "d3/dc1/classG4VEntanglementClipBoard.html#ad1b4b6cf0fa99a1e33c6a67b1519c836", null ],
    [ "~G4VEntanglementClipBoard", "d3/dc1/classG4VEntanglementClipBoard.html#ad83514423ffd24eb53260ad9a29ec1ed", null ],
    [ "GetParentParticleDefinition", "d3/dc1/classG4VEntanglementClipBoard.html#a0d9587d3e2bd05f7c461ad0a87d2ce51", null ],
    [ "GetTrackA", "d3/dc1/classG4VEntanglementClipBoard.html#ab0674e460e85c549f59eb4624255487b", null ],
    [ "GetTrackB", "d3/dc1/classG4VEntanglementClipBoard.html#a0a67a48521867373050b89d93d8cb25d", null ],
    [ "IsTrack1Measurement", "d3/dc1/classG4VEntanglementClipBoard.html#ab4bd1f714a0ebf01a273f40d183d9165", null ],
    [ "IsTrack2Measurement", "d3/dc1/classG4VEntanglementClipBoard.html#aa6ee57e23ce3b3b566143f96dc5e0240", null ],
    [ "ResetTrack1Measurement", "d3/dc1/classG4VEntanglementClipBoard.html#afcf3fb7db263c41e5e8a9015bf5bfeff", null ],
    [ "ResetTrack2Measurement", "d3/dc1/classG4VEntanglementClipBoard.html#afabceca86efca1d67a549e114dbcf922", null ],
    [ "SetParentParticleDefinition", "d3/dc1/classG4VEntanglementClipBoard.html#a4b17662d2ea8e6f3ef8789c98e5acbc4", null ],
    [ "SetTrackA", "d3/dc1/classG4VEntanglementClipBoard.html#a9b162b3e2766bacbd4cdb4821d6c46c0", null ],
    [ "SetTrackB", "d3/dc1/classG4VEntanglementClipBoard.html#a92523f8548f286f65b6312824c4ca213", null ],
    [ "fpParentParticleDefinition", "d3/dc1/classG4VEntanglementClipBoard.html#a858a947dc68084faef3ebb0870b819f2", null ],
    [ "fTrack1Measurement", "d3/dc1/classG4VEntanglementClipBoard.html#ab3d3c36a01653bb3b4046001a91037b3", null ],
    [ "fTrack2Measurement", "d3/dc1/classG4VEntanglementClipBoard.html#a9835d3e7fe0e38072ee8fec1ab131ef5", null ],
    [ "fTrackA", "d3/dc1/classG4VEntanglementClipBoard.html#a759106ff58b2c9e8f35b42d50649f0c5", null ],
    [ "fTrackB", "d3/dc1/classG4VEntanglementClipBoard.html#a43f0e6aa265bcb2a356987eb19301547", null ]
];