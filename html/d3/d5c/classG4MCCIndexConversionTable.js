var classG4MCCIndexConversionTable =
[
    [ "G4MCCIndexConversionTable", "d3/d5c/classG4MCCIndexConversionTable.html#a136c003327ade0156fe702298ce4fb8f", null ],
    [ "~G4MCCIndexConversionTable", "d3/d5c/classG4MCCIndexConversionTable.html#a55d6a532944d5f702839bf866648883e", null ],
    [ "GetIndex", "d3/d5c/classG4MCCIndexConversionTable.html#a9410568798d89988af01156cca24d5e5", null ],
    [ "IsUsed", "d3/d5c/classG4MCCIndexConversionTable.html#a8a39c19876dc56cb8aec157d940bab9e", null ],
    [ "Reset", "d3/d5c/classG4MCCIndexConversionTable.html#abe630df7daa18f616f2aeed28bfdc31f", null ],
    [ "SetNewIndex", "d3/d5c/classG4MCCIndexConversionTable.html#a3e7866ff49b77a7629578e34b8914c5a", null ],
    [ "size", "d3/d5c/classG4MCCIndexConversionTable.html#a6b1e070227dfb016386e1af49823d0c2", null ],
    [ "vecNewIndex", "d3/d5c/classG4MCCIndexConversionTable.html#a921556e67fc05dfa4a444d01d7d2cded", null ]
];