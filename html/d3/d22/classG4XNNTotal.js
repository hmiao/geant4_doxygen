var classG4XNNTotal =
[
    [ "G4XNNTotal", "d3/d22/classG4XNNTotal.html#a81315e44cc51c58423a719f8b41601ff", null ],
    [ "~G4XNNTotal", "d3/d22/classG4XNNTotal.html#ac84860e4fe0c9c1b4f33b7cf201acd83", null ],
    [ "G4XNNTotal", "d3/d22/classG4XNNTotal.html#a5242fade0ba1109abcb86acdd8c16a55", null ],
    [ "GetComponents", "d3/d22/classG4XNNTotal.html#a9e4f74c492a4362ec81587fe75d58593", null ],
    [ "Name", "d3/d22/classG4XNNTotal.html#ad4998a95eff0845dc85c399549fc617b", null ],
    [ "operator!=", "d3/d22/classG4XNNTotal.html#a29c290773fba60d970a6197152cd0581", null ],
    [ "operator=", "d3/d22/classG4XNNTotal.html#a50fd1c31e92f15a19015fa28f19ab8ba", null ],
    [ "operator==", "d3/d22/classG4XNNTotal.html#aea6fcc0de7e58e652c9a9c9d313e9710", null ],
    [ "components", "d3/d22/classG4XNNTotal.html#aade8704dc27284e13cecaa5d24ae4bc0", null ]
];