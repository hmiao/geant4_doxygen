var classG4DNAIndirectHit =
[
    [ "G4DNAIndirectHit", "d3/d7d/classG4DNAIndirectHit.html#a064d88bab353f5186245a209195135b3", null ],
    [ "~G4DNAIndirectHit", "d3/d7d/classG4DNAIndirectHit.html#a465cde64e98c43760a7609f02592f603", null ],
    [ "GetBaseName", "d3/d7d/classG4DNAIndirectHit.html#a713eaf632d1b8066a2ade4c9d6df7af7", null ],
    [ "GetMolecule", "d3/d7d/classG4DNAIndirectHit.html#a5e0be34d792153b43d054cb8f5b06ca7", null ],
    [ "GetPosition", "d3/d7d/classG4DNAIndirectHit.html#a45d18a7700f2127991d323e3a23fc439", null ],
    [ "GetTime", "d3/d7d/classG4DNAIndirectHit.html#a2d0ff2d72066047d22a2a4e99f28cec4", null ],
    [ "Print", "d3/d7d/classG4DNAIndirectHit.html#a21ec651d4a3bfcfabcbbd7da3f2dd99c", null ],
    [ "fBaseName", "d3/d7d/classG4DNAIndirectHit.html#a57e1f8c13294872c639d3e2ae2167b69", null ],
    [ "fpMolecule", "d3/d7d/classG4DNAIndirectHit.html#a9f1131b77735da103a491742e1e9e90d", null ],
    [ "fPosition", "d3/d7d/classG4DNAIndirectHit.html#af219264c647f4f6b0a4fdcebab5f346f", null ],
    [ "fTime", "d3/d7d/classG4DNAIndirectHit.html#a5258b5906dcb08e61433126ad5f441da", null ]
];