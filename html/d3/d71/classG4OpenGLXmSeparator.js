var classG4OpenGLXmSeparator =
[
    [ "G4OpenGLXmSeparator", "d3/d71/classG4OpenGLXmSeparator.html#a62a43a99f8dcdb1d13a83323ae7e6003", null ],
    [ "~G4OpenGLXmSeparator", "d3/d71/classG4OpenGLXmSeparator.html#acaf64f2c9b61170f7e70248db53b0126", null ],
    [ "G4OpenGLXmSeparator", "d3/d71/classG4OpenGLXmSeparator.html#a2b57b8f89d58fba11563a9b7d16dfb5c", null ],
    [ "AddYourselfTo", "d3/d71/classG4OpenGLXmSeparator.html#a5d5d7c7ec673adec18fa3c078a2f729e", null ],
    [ "GetPointerToParent", "d3/d71/classG4OpenGLXmSeparator.html#acf014828ce8cb9df2757e20eb889b3bb", null ],
    [ "GetPointerToWidget", "d3/d71/classG4OpenGLXmSeparator.html#a0de3b4ab62186101a844ee6a4903fa18", null ],
    [ "operator=", "d3/d71/classG4OpenGLXmSeparator.html#abc1322ff701782e77df35cef1a307661", null ],
    [ "line", "d3/d71/classG4OpenGLXmSeparator.html#ae4ac11f1b93678eccec024287e447428", null ],
    [ "line_type", "d3/d71/classG4OpenGLXmSeparator.html#aa2700af6c0f1b0e8fe1926ad31dcdee2", null ],
    [ "parent", "d3/d71/classG4OpenGLXmSeparator.html#a17f3a5456f727b577e31be80d2dd948d", null ]
];