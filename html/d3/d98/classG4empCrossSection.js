var classG4empCrossSection =
[
    [ "G4empCrossSection", "d3/d98/classG4empCrossSection.html#ada93a36690e549a6af2364f316f52749", null ],
    [ "~G4empCrossSection", "d3/d98/classG4empCrossSection.html#aa6b810973d6576f567c40200cae9695b", null ],
    [ "G4empCrossSection", "d3/d98/classG4empCrossSection.html#ae3a18791f8db367ae8199b2ded0b3fa8", null ],
    [ "CrossSection", "d3/d98/classG4empCrossSection.html#aa09f257405e35da2b46656bcf9b63160", null ],
    [ "GetCrossSection", "d3/d98/classG4empCrossSection.html#a56d00798a01b67728a108ffe9a095999", null ],
    [ "operator=", "d3/d98/classG4empCrossSection.html#a971d0489481b812903da34baf30113fa", null ],
    [ "Probabilities", "d3/d98/classG4empCrossSection.html#a048834a7e87c433139cc91e5ff71f12a", null ],
    [ "SetTotalCS", "d3/d98/classG4empCrossSection.html#a82844441019055a29b6087ce9b22d653", null ],
    [ "flag", "d3/d98/classG4empCrossSection.html#aade8f6550339a9d1e7c05f1f42540095", null ],
    [ "orlicShellLi", "d3/d98/classG4empCrossSection.html#afd0f5f473df537e66507394f7c269d80", null ],
    [ "paulShellK", "d3/d98/classG4empCrossSection.html#a30007f4c7bed7c9673b6ca6e13072327", null ],
    [ "totalCS", "d3/d98/classG4empCrossSection.html#a7901921e99166b2ae6c492e9db2e3048", null ]
];