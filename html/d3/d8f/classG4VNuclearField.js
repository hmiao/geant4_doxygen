var classG4VNuclearField =
[
    [ "G4VNuclearField", "d3/d8f/classG4VNuclearField.html#a94229a84a6780193c7f37637be245e06", null ],
    [ "~G4VNuclearField", "d3/d8f/classG4VNuclearField.html#afa3b252d22b2510157b9ab52614e2f80", null ],
    [ "G4VNuclearField", "d3/d8f/classG4VNuclearField.html#aca866c48ee6ca29eb92bc8a8686a6b83", null ],
    [ "GetBarrier", "d3/d8f/classG4VNuclearField.html#a2b1ce03093bf66d56f9a490ddc7f5867", null ],
    [ "GetCoeff", "d3/d8f/classG4VNuclearField.html#a3b1481400778a56659bff516a8f16155", null ],
    [ "GetField", "d3/d8f/classG4VNuclearField.html#a9416bc4141a9ba4df17fbe19d0d3c338", null ],
    [ "operator!=", "d3/d8f/classG4VNuclearField.html#a1ee444719e8704d8cc1f3faa041ddfea", null ],
    [ "operator=", "d3/d8f/classG4VNuclearField.html#ad0006fd88aa70e6b9dc040b2d7e7bb85", null ],
    [ "operator==", "d3/d8f/classG4VNuclearField.html#ae385e05c99013794d37daf0b7282ed43", null ],
    [ "SetNucleus", "d3/d8f/classG4VNuclearField.html#ab94009dc461a40caa4e26006e727abf8", null ],
    [ "radius", "d3/d8f/classG4VNuclearField.html#abe60b79ee92cf333e3fbda4d01bcb6f3", null ],
    [ "theNucleus", "d3/d8f/classG4VNuclearField.html#a797fabbdd9be999c4d00ebefc3dfda22", null ]
];