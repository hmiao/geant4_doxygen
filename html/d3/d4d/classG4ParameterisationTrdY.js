var classG4ParameterisationTrdY =
[
    [ "G4ParameterisationTrdY", "d3/d4d/classG4ParameterisationTrdY.html#a6f2c6a9dd3919cbf319f80e13f9dc475", null ],
    [ "~G4ParameterisationTrdY", "d3/d4d/classG4ParameterisationTrdY.html#a7f0b16367e26847411e010886507ecc7", null ],
    [ "ComputeDimensions", "d3/d4d/classG4ParameterisationTrdY.html#a3a46a13116077a997ca0ff21457978df", null ],
    [ "ComputeDimensions", "d3/d4d/classG4ParameterisationTrdY.html#a783fdb4275db1044aeb3cdc408b25580", null ],
    [ "ComputeDimensions", "d3/d4d/classG4ParameterisationTrdY.html#aaab4b0782ae4b0f0326fbad5f771b444", null ],
    [ "ComputeDimensions", "d3/d4d/classG4ParameterisationTrdY.html#a52d38f864fd56d6ebdffaafeee4cfca7", null ],
    [ "ComputeDimensions", "d3/d4d/classG4ParameterisationTrdY.html#a9fee2f53add2b6bfacc6b4ac74c51a06", null ],
    [ "ComputeDimensions", "d3/d4d/classG4ParameterisationTrdY.html#a0ff6245dec0c37fea92e8ddb1e741277", null ],
    [ "ComputeDimensions", "d3/d4d/classG4ParameterisationTrdY.html#afda2b36f3692f76e09bd118de61b4a19", null ],
    [ "ComputeDimensions", "d3/d4d/classG4ParameterisationTrdY.html#a9fbc069b3606f1b9819db3d1a743bcab", null ],
    [ "ComputeDimensions", "d3/d4d/classG4ParameterisationTrdY.html#ad66b53ec30d3fbec47c7bf4128c8424a", null ],
    [ "ComputeDimensions", "d3/d4d/classG4ParameterisationTrdY.html#a6136b34fb72824dd32766574bb93a5fa", null ],
    [ "ComputeDimensions", "d3/d4d/classG4ParameterisationTrdY.html#a4a93832a85d74f957ce6825616f2449f", null ],
    [ "ComputeDimensions", "d3/d4d/classG4ParameterisationTrdY.html#ae31333de2acf2de7de64bc03f630e91b", null ],
    [ "ComputeDimensions", "d3/d4d/classG4ParameterisationTrdY.html#a0405290fb1dd044344e0f6e998ac45bd", null ],
    [ "ComputeTransformation", "d3/d4d/classG4ParameterisationTrdY.html#af0b5005680825a5b8859ca5c970a8f6c", null ],
    [ "GetMaxParameter", "d3/d4d/classG4ParameterisationTrdY.html#adf4b148a9ba1c69c059ea9cf2393c563", null ]
];