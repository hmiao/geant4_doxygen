var classG4ElNeutrinoNucleusProcess =
[
    [ "G4ElNeutrinoNucleusProcess", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a98a776e5ffd76be6feb18ce3c94e4749", null ],
    [ "~G4ElNeutrinoNucleusProcess", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a84f2bead87687a504ba28aac49f791be", null ],
    [ "G4ElNeutrinoNucleusProcess", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a159e3f93ae10cf8a11e041b9c5fe0bbb", null ],
    [ "GetMeanFreePath", "d3/d17/classG4ElNeutrinoNucleusProcess.html#aef4ac7a6a7223516ccdf417be4138838", null ],
    [ "operator=", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a14a9a6bfa122d4a08c37dc993eec12f0", null ],
    [ "PostStepDoIt", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a6620ffcdd086e991fd182714394376e6", null ],
    [ "PreparePhysicsTable", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a4e548b36b5b1c900a291cd237eeae5ed", null ],
    [ "ProcessDescription", "d3/d17/classG4ElNeutrinoNucleusProcess.html#adf69ee03ff6374bfac27cbd76e2d890b", null ],
    [ "SetBiasingFactor", "d3/d17/classG4ElNeutrinoNucleusProcess.html#aca05e9e0bd27fc208be00682f510d348", null ],
    [ "SetBiasingFactors", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a760f1f698d4fc918310352cc9bf1b60b", null ],
    [ "SetLowestEnergy", "d3/d17/classG4ElNeutrinoNucleusProcess.html#ae79b9ba8aa6b895a04f93d7ebe8062eb", null ],
    [ "fBiased", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a406eb59a8781e410f6ef30da55de2e47", null ],
    [ "fEnvelope", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a4119d51c0e4bb652cc1c870b91feebe4", null ],
    [ "fEnvelopeName", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a7aaa0d1e583a674a64a82473824af4c2", null ],
    [ "fNuNuclCcBias", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a68a93e57b2923884b15bc2da2131d844", null ],
    [ "fNuNuclNcBias", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a0a32163dfa19fe25d91c25f9429972f5", null ],
    [ "fNuNuclTotXscBias", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a104cb72f72723f05181d1a35743c0fa2", null ],
    [ "fTotXsc", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a4b66a01e72f4dca88bf83f6b0e725050", null ],
    [ "isInitialised", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a6ab8945bbead2686f9574a533d81100c", null ],
    [ "lowestEnergy", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a1ee09b265f61a584fdda17de833ff049", null ],
    [ "safetyHelper", "d3/d17/classG4ElNeutrinoNucleusProcess.html#a54269c52112fe1c3e9727ef9e3039c55", null ]
];