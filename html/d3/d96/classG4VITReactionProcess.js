var classG4VITReactionProcess =
[
    [ "G4VITReactionProcess", "d3/d96/classG4VITReactionProcess.html#aec590ba822c425cc322bc5e886b0fa9f", null ],
    [ "~G4VITReactionProcess", "d3/d96/classG4VITReactionProcess.html#ab3ba09052353f37555a5ff1be708984d", null ],
    [ "G4VITReactionProcess", "d3/d96/classG4VITReactionProcess.html#a0c69cb6a2e752aa9e5131aaa4194dc08", null ],
    [ "FindReaction", "d3/d96/classG4VITReactionProcess.html#ad844ad54ece09ddd4fc4dc1c79d97aa5", null ],
    [ "Initialize", "d3/d96/classG4VITReactionProcess.html#a0b8e25b12de3eb0f1d7471601a4dad38", null ],
    [ "IsApplicable", "d3/d96/classG4VITReactionProcess.html#ad0fc0009fe3075dfef8063d62eaaee1c", null ],
    [ "MakeReaction", "d3/d96/classG4VITReactionProcess.html#a4da3ab57dd73ccfd6d1ec3ac22907e5f", null ],
    [ "operator=", "d3/d96/classG4VITReactionProcess.html#a2babdc7a5f3951b7c654e05a4d227482", null ],
    [ "SetReactionTable", "d3/d96/classG4VITReactionProcess.html#a213849f5022680e56ad07a9430962729", null ],
    [ "TestReactibility", "d3/d96/classG4VITReactionProcess.html#add0f020ae224dab43b3dc477e1c62c3e", null ],
    [ "fName", "d3/d96/classG4VITReactionProcess.html#a84b1f41ce3026c938f3dc984353a2aef", null ],
    [ "fpReactionTable", "d3/d96/classG4VITReactionProcess.html#a3b6721b3b8217cd53d37166e05e578d0", null ]
];