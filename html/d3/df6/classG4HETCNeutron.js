var classG4HETCNeutron =
[
    [ "G4HETCNeutron", "d3/df6/classG4HETCNeutron.html#ac00b9fd48a9293e1c3d87408a5dddf05", null ],
    [ "~G4HETCNeutron", "d3/df6/classG4HETCNeutron.html#abf5de8e0ff10217b34e4b2395f51c73e", null ],
    [ "G4HETCNeutron", "d3/df6/classG4HETCNeutron.html#acd5ccf6d8897ea83d7b10252e7c9924f", null ],
    [ "GetAlpha", "d3/df6/classG4HETCNeutron.html#a4cd112b53949c0eb7c25b364ff4aa8d0", null ],
    [ "GetBeta", "d3/df6/classG4HETCNeutron.html#a6fa96e61f01fb973ced26964d9ec9473", null ],
    [ "GetSpinFactor", "d3/df6/classG4HETCNeutron.html#afb2b7253472a43138fc902899b736c88", null ],
    [ "K", "d3/df6/classG4HETCNeutron.html#a990ba1e9f4243fd2cd6d4ea31a0141a8", null ],
    [ "operator!=", "d3/df6/classG4HETCNeutron.html#a715465d08bed3529deb0a8a3e0c15afa", null ],
    [ "operator=", "d3/df6/classG4HETCNeutron.html#acf676e0903f90e1f52ce6b0e321dd268", null ],
    [ "operator==", "d3/df6/classG4HETCNeutron.html#a242ab0a3762660e2b0c3b85637d2bc44", null ],
    [ "SampleKineticEnergy", "d3/df6/classG4HETCNeutron.html#ab6d82d1f4dff4cff7af7cf7a221f2da4", null ],
    [ "theNeutronCoulombBarrier", "d3/df6/classG4HETCNeutron.html#a73dae37632330dd2d7a4b192bced7e5b", null ]
];