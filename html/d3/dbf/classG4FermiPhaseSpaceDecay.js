var classG4FermiPhaseSpaceDecay =
[
    [ "G4FermiPhaseSpaceDecay", "d3/dbf/classG4FermiPhaseSpaceDecay.html#a752dd98190d8516994c8bc897a75f04f", null ],
    [ "~G4FermiPhaseSpaceDecay", "d3/dbf/classG4FermiPhaseSpaceDecay.html#af50e60f6fedb1c28f8b0b3d50c3aa008", null ],
    [ "G4FermiPhaseSpaceDecay", "d3/dbf/classG4FermiPhaseSpaceDecay.html#a757df14b34dcfe596d8d55375de22e23", null ],
    [ "BetaKopylov", "d3/dbf/classG4FermiPhaseSpaceDecay.html#ac2d7e230b9fc05b7c31ac1f2cfe71f92", null ],
    [ "Decay", "d3/dbf/classG4FermiPhaseSpaceDecay.html#aebbe749de3600edd44278ce7e06c57ef", null ],
    [ "KopylovNBodyDecay", "d3/dbf/classG4FermiPhaseSpaceDecay.html#a881a5959ebebc71c18c207e6bdbd2b45", null ],
    [ "operator!=", "d3/dbf/classG4FermiPhaseSpaceDecay.html#a3211cf1b7d8a2eb4d1645de3ec89ec5a", null ],
    [ "operator=", "d3/dbf/classG4FermiPhaseSpaceDecay.html#a58312059d19cee7531697df20a3cfe18", null ],
    [ "operator==", "d3/dbf/classG4FermiPhaseSpaceDecay.html#ad3692a621ce3e31e3688589e943d15d0", null ],
    [ "PtwoBody", "d3/dbf/classG4FermiPhaseSpaceDecay.html#a372e0ca2c8953473024b080cb86f716b", null ],
    [ "g4calc", "d3/dbf/classG4FermiPhaseSpaceDecay.html#a8b466c5c8e3c5e2de7ae244ad82d11a0", null ]
];