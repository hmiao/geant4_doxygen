var classG4TQuadrupoleMagField =
[
    [ "G4TQuadrupoleMagField", "d3/d9c/classG4TQuadrupoleMagField.html#a65cd5d1ee913af2eb63045cfaee0cdbf", null ],
    [ "G4TQuadrupoleMagField", "d3/d9c/classG4TQuadrupoleMagField.html#a3d7f645c100777fae3a8ec6e7855edd3", null ],
    [ "~G4TQuadrupoleMagField", "d3/d9c/classG4TQuadrupoleMagField.html#a072f8e0f7cada5edb4323b2669a8ed84", null ],
    [ "Clone", "d3/d9c/classG4TQuadrupoleMagField.html#a3c2cfd402c86cf56392d800529667989", null ],
    [ "GetFieldValue", "d3/d9c/classG4TQuadrupoleMagField.html#af7d38ea3217d97c19652b09470900290", null ],
    [ "fGradient", "d3/d9c/classG4TQuadrupoleMagField.html#ad8ef96ba6a02d0e821c0d271c9360dbd", null ],
    [ "fOrigin", "d3/d9c/classG4TQuadrupoleMagField.html#a96a659805db8561d74a11d8c7e6399ae", null ],
    [ "fpMatrix", "d3/d9c/classG4TQuadrupoleMagField.html#ab0db3d2dbdb8222bca7b83ceed52fed1", null ]
];