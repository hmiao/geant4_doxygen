var classG4EmDataHandler =
[
    [ "G4EmDataHandler", "d3/d44/classG4EmDataHandler.html#af9ffe97b3e27153ee9052761502f3794", null ],
    [ "~G4EmDataHandler", "d3/d44/classG4EmDataHandler.html#a015d1d7999adafcfdb8289c2ae981795", null ],
    [ "G4EmDataHandler", "d3/d44/classG4EmDataHandler.html#ac45c520c7d304bf3e568ac35b8a39f27", null ],
    [ "CleanTable", "d3/d44/classG4EmDataHandler.html#a42e0f3d5679f3a6ec3124ca2347651ea", null ],
    [ "GetMasterProcess", "d3/d44/classG4EmDataHandler.html#a8d94926c7c21182a9a703c39b22ea02e", null ],
    [ "GetTable", "d3/d44/classG4EmDataHandler.html#aeef07eaf95d4fc19f1587f7dddcb24be", null ],
    [ "GetTables", "d3/d44/classG4EmDataHandler.html#adddd2a9406b3fe22fd7c5d24cd029412", null ],
    [ "GetVector", "d3/d44/classG4EmDataHandler.html#adc6ae73cc0232ef949f70e0930ec0ef1", null ],
    [ "MakeTable", "d3/d44/classG4EmDataHandler.html#a7135cf5ffc01dfa4f2f223a8e9e293a0", null ],
    [ "operator=", "d3/d44/classG4EmDataHandler.html#a9876f21734636bae9c9e3c6fb6664f9e", null ],
    [ "RetrievePhysicsTable", "d3/d44/classG4EmDataHandler.html#a7f7657ccde1982c099d68163efedfff0", null ],
    [ "SetMasterProcess", "d3/d44/classG4EmDataHandler.html#a0e328fd5274d6f768b4f325e1b38c7d6", null ],
    [ "SetTable", "d3/d44/classG4EmDataHandler.html#ab4816212af1e2fd5adf413c629fcc736", null ],
    [ "StorePhysicsTable", "d3/d44/classG4EmDataHandler.html#afd7d6c55a68295f88e209639ff8c7c4c", null ],
    [ "Table", "d3/d44/classG4EmDataHandler.html#a98afe684f1364a090f25e7e71fc9d859", null ],
    [ "data", "d3/d44/classG4EmDataHandler.html#a5995a6938302a1a4c4c59522f62dbe15", null ],
    [ "masterProcess", "d3/d44/classG4EmDataHandler.html#a769de8bf52ce535bbfbfa43aa3ebaf6a", null ],
    [ "tLength", "d3/d44/classG4EmDataHandler.html#ab21bc25b0cbb2fe0aef26d775b71cf6a", null ]
];