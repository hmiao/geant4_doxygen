var classG4CameronTruranHilfShellCorrections =
[
    [ "G4CameronTruranHilfShellCorrections", "d3/d43/classG4CameronTruranHilfShellCorrections.html#a932027ddf3975e9db84e4ce411a3c606", null ],
    [ "G4CameronTruranHilfShellCorrections", "d3/d43/classG4CameronTruranHilfShellCorrections.html#aa34e227c910fb6a1d58fcf114a44366b", null ],
    [ "GetShellCorrection", "d3/d43/classG4CameronTruranHilfShellCorrections.html#a336f9f5dd2e27570f8a670ac5b0c0f15", null ],
    [ "operator=", "d3/d43/classG4CameronTruranHilfShellCorrections.html#ab379e6456ea13ed64dac4a30c1643b6f", null ],
    [ "ShellNTable", "d3/d43/classG4CameronTruranHilfShellCorrections.html#a802d4e2951b8c9f6064cf10c4b6a92fa", null ],
    [ "ShellZTable", "d3/d43/classG4CameronTruranHilfShellCorrections.html#a36b104aa3aeea137ca7224bc68e47bdb", null ]
];