var classG4AttDef =
[
    [ "G4AttDef", "d3/d75/classG4AttDef.html#a1c878511223b6d507f89f6138d70bb4f", null ],
    [ "G4AttDef", "d3/d75/classG4AttDef.html#ad76cd124eda6048acee03b0ebab84848", null ],
    [ "G4AttDef", "d3/d75/classG4AttDef.html#ac3c0351f5ae306468384cb374fa22a60", null ],
    [ "~G4AttDef", "d3/d75/classG4AttDef.html#a1b9e080a482bdd094fb9e59cb7190e85", null ],
    [ "GetCategory", "d3/d75/classG4AttDef.html#a10ba22dae5ac2347ebbefc00b22c31b0", null ],
    [ "GetDesc", "d3/d75/classG4AttDef.html#a68f3902ef16f9e9c5b387c46687f75b2", null ],
    [ "GetExtra", "d3/d75/classG4AttDef.html#a2fda47f1ccdecaa58fbbf7315ceb5e06", null ],
    [ "GetName", "d3/d75/classG4AttDef.html#abb9bc7fd098dab58ac0d72f53fdbfc4b", null ],
    [ "GetTypeKey", "d3/d75/classG4AttDef.html#a939093d80366b8f08a11f34dbcca1536", null ],
    [ "GetValueType", "d3/d75/classG4AttDef.html#aefb7d78dabd7341e106571d295078998", null ],
    [ "SetCategory", "d3/d75/classG4AttDef.html#a6a0d72bfc98810a09f21ff314873eb9f", null ],
    [ "SetDesc", "d3/d75/classG4AttDef.html#af59067e2807276ebf2a34c385f18f5f5", null ],
    [ "SetExtra", "d3/d75/classG4AttDef.html#ae02e851198e3964c704d063f2ba64bf7", null ],
    [ "SetName", "d3/d75/classG4AttDef.html#aa12360d169d469e292bf01ed2191a4e5", null ],
    [ "SetValueType", "d3/d75/classG4AttDef.html#a35e48145b6f96394b1150e18e8981112", null ],
    [ "m_category", "d3/d75/classG4AttDef.html#ad92c740d3b5ceb2f78382e5bb288fb6b", null ],
    [ "m_desc", "d3/d75/classG4AttDef.html#aa59b23798336caf0dd55a69417a5c805", null ],
    [ "m_extra", "d3/d75/classG4AttDef.html#a8354ef8a011c85e4775692e08b600bd6", null ],
    [ "m_name", "d3/d75/classG4AttDef.html#a2c7371783dcb66bd457263ee72c3bc8a", null ],
    [ "m_typeKey", "d3/d75/classG4AttDef.html#aaa91e68acc5350656418a4b94a699013", null ],
    [ "m_valueType", "d3/d75/classG4AttDef.html#a9da2cd92f1d54eeb5dba93b13f91f4cf", null ]
];