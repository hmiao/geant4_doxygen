var classG4TCachedMagneticField =
[
    [ "G4TCachedMagneticField", "d3/dcf/classG4TCachedMagneticField.html#a27ec1e5c58f1251af5f10bd1a967b23d", null ],
    [ "G4TCachedMagneticField", "d3/dcf/classG4TCachedMagneticField.html#a2b58c0fe6874239e265baec0f2ab5f21", null ],
    [ "~G4TCachedMagneticField", "d3/dcf/classG4TCachedMagneticField.html#a1fe670d2a593506f68a6b82c7d6d83b7", null ],
    [ "ClearCounts", "d3/dcf/classG4TCachedMagneticField.html#a0d57b65e21df985ab0c40709873bf615", null ],
    [ "Clone", "d3/dcf/classG4TCachedMagneticField.html#a2f90c9852fb06b6a4df8be726bb799b8", null ],
    [ "GetConstDistance", "d3/dcf/classG4TCachedMagneticField.html#ace2c6693af68b7d47054fabcc11c822e", null ],
    [ "GetCountCalls", "d3/dcf/classG4TCachedMagneticField.html#a1c5864b2a1815b16420ba7ab349a8d22", null ],
    [ "GetCountEvaluations", "d3/dcf/classG4TCachedMagneticField.html#a8b62b3feaee7748a10d05828eccaf711", null ],
    [ "GetFieldValue", "d3/dcf/classG4TCachedMagneticField.html#a75b39db91c3bdca6bdfca22d664b8ad8", null ],
    [ "operator=", "d3/dcf/classG4TCachedMagneticField.html#a5c3107bfe0b749335c7f15a979905956", null ],
    [ "ReportStatistics", "d3/dcf/classG4TCachedMagneticField.html#a2f304d25904aacdee990375372fae95b", null ],
    [ "SetConstDistance", "d3/dcf/classG4TCachedMagneticField.html#a6eb6e82b3c75c0aabb9b26f353b534b3", null ],
    [ "fCountCalls", "d3/dcf/classG4TCachedMagneticField.html#a7d81bc450c53929bc6fd78be03400140", null ],
    [ "fCountEvaluations", "d3/dcf/classG4TCachedMagneticField.html#a2ee0436ed059c16748dce20ef6c55ab9", null ],
    [ "fDistanceConst", "d3/dcf/classG4TCachedMagneticField.html#ae1c49f774f757a824e46f604da6c133c", null ],
    [ "fLastLocation", "d3/dcf/classG4TCachedMagneticField.html#aaaf970805cbaf55378e7883dacb04117", null ],
    [ "fLastValue", "d3/dcf/classG4TCachedMagneticField.html#a11380fd11a2434175956e11f160ba466", null ],
    [ "fpMagneticField", "d3/dcf/classG4TCachedMagneticField.html#a8a9c42a10f8da5231498e1cce2faa746", null ]
];