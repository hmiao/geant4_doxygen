var classG4GaussXTRadiator =
[
    [ "G4GaussXTRadiator", "d3/d6f/classG4GaussXTRadiator.html#ab7b13a855e926e6d0404810c6e58e301", null ],
    [ "~G4GaussXTRadiator", "d3/d6f/classG4GaussXTRadiator.html#a371654eb75a3bddcf22290c17e0debb0", null ],
    [ "DumpInfo", "d3/d6f/classG4GaussXTRadiator.html#aa9184e9b9c5ccd876f98bd23e2bf2abc", null ],
    [ "GetStackFactor", "d3/d6f/classG4GaussXTRadiator.html#aa7db275f0b1fe1bd53e6ac367d392554", null ],
    [ "ProcessDescription", "d3/d6f/classG4GaussXTRadiator.html#a15d6f677d2f6b6af9146cbd152cd86d4", null ],
    [ "SpectralXTRdEdx", "d3/d6f/classG4GaussXTRadiator.html#a5f19a544e387feac5eb29be116a31e10", null ]
];