var classG4UrbanFluctuation =
[
    [ "G4UrbanFluctuation", "d3/d45/classG4UrbanFluctuation.html#a81e50789662d0d39ce39ff8967c612a3", null ],
    [ "~G4UrbanFluctuation", "d3/d45/classG4UrbanFluctuation.html#a08fbc7014234374d8f28f799b0106e46", null ],
    [ "G4UrbanFluctuation", "d3/d45/classG4UrbanFluctuation.html#a62f0d7c7c005e535afb6e0bac930f409", null ],
    [ "operator=", "d3/d45/classG4UrbanFluctuation.html#aa11cf010f303300796ff048d41bf93c3", null ],
    [ "SampleGlandz", "d3/d45/classG4UrbanFluctuation.html#afbc612d8c19f73e1f12c5e09bd845346", null ],
    [ "e1Fluct", "d3/d45/classG4UrbanFluctuation.html#a312548849d287ec0b484fcacd65d7202", null ],
    [ "e1LogFluct", "d3/d45/classG4UrbanFluctuation.html#a88955c92831bc8d3d9f92ad3cb14df59", null ],
    [ "e2Fluct", "d3/d45/classG4UrbanFluctuation.html#afdad7b809265303043d02549579711b9", null ],
    [ "e2LogFluct", "d3/d45/classG4UrbanFluctuation.html#a9bfd612f6e0e43753200b5250a2327ef", null ],
    [ "esmall", "d3/d45/classG4UrbanFluctuation.html#ada1df7c6cf0aca9e60a602f0396954da", null ],
    [ "f1Fluct", "d3/d45/classG4UrbanFluctuation.html#a28cc6aaab899d1d898604b01fa157efa", null ],
    [ "f2Fluct", "d3/d45/classG4UrbanFluctuation.html#ae34bedd240856c0dfe0b101f1507dbae", null ],
    [ "lastMaterial", "d3/d45/classG4UrbanFluctuation.html#a29e0531e1c5026a43e157cb3cd30fe66", null ]
];