var classG4Physics2DVector =
[
    [ "G4Physics2DVector", "d3/d41/classG4Physics2DVector.html#a50c554903d6f3275ec7d76828e5ecc75", null ],
    [ "G4Physics2DVector", "d3/d41/classG4Physics2DVector.html#a6f561d7c14b42e0072c42b45c3678bb7", null ],
    [ "G4Physics2DVector", "d3/d41/classG4Physics2DVector.html#ad9d7578ecdb6ec6e65eca9cc7cb012dc", null ],
    [ "~G4Physics2DVector", "d3/d41/classG4Physics2DVector.html#a37789ea43d2c40480c8690287757c52d", null ],
    [ "BicubicInterpolation", "d3/d41/classG4Physics2DVector.html#a55bcf5b1845989df1e7eb6254f562341", null ],
    [ "ClearVectors", "d3/d41/classG4Physics2DVector.html#a6cc11d7bf5ecfffb8563729d3d7cf610", null ],
    [ "CopyData", "d3/d41/classG4Physics2DVector.html#a3bca0de6b07209d2325c4e7546e8443b", null ],
    [ "DerivativeX", "d3/d41/classG4Physics2DVector.html#a3c7c66054683e365ad8ee72cb314d169", null ],
    [ "DerivativeXY", "d3/d41/classG4Physics2DVector.html#a6aa133610fba79ad83f866fe0a04ec23", null ],
    [ "DerivativeY", "d3/d41/classG4Physics2DVector.html#adac1c0e4165d903c08dbb1bc39e66e81", null ],
    [ "FindBin", "d3/d41/classG4Physics2DVector.html#a653bcc5263c7d7cd23d9313620acdaae", null ],
    [ "FindBinLocationX", "d3/d41/classG4Physics2DVector.html#a2a020046a9fd034f8a735c4087f8950e", null ],
    [ "FindBinLocationY", "d3/d41/classG4Physics2DVector.html#a39d457fc53f589acba8bea53e55a0540", null ],
    [ "FindLinearX", "d3/d41/classG4Physics2DVector.html#aadc2c1f74d81da49ae9dfdf6840a59c4", null ],
    [ "FindLinearX", "d3/d41/classG4Physics2DVector.html#afe4453de47a7d425f8aff9f108847730", null ],
    [ "GetLengthX", "d3/d41/classG4Physics2DVector.html#a60da8c5a57e73c1be867e3a7e85b218f", null ],
    [ "GetLengthY", "d3/d41/classG4Physics2DVector.html#a945cfe06b2a7bfc40116d60096fdaa27", null ],
    [ "GetType", "d3/d41/classG4Physics2DVector.html#a6d404fb0f2e6adec72c7329c7ed88877", null ],
    [ "GetValue", "d3/d41/classG4Physics2DVector.html#af5ee18f9c5ec3cc1c063577ca67a2fce", null ],
    [ "GetX", "d3/d41/classG4Physics2DVector.html#af7e5d4625cbaf1f9b97792ce822782e7", null ],
    [ "GetY", "d3/d41/classG4Physics2DVector.html#afc90d75dd36fd4a1e85c269113ef3d56", null ],
    [ "InterpolateLinearX", "d3/d41/classG4Physics2DVector.html#a6f3d0e5bb27ca6bc32afbc5296c60b8a", null ],
    [ "operator!=", "d3/d41/classG4Physics2DVector.html#adda436daf70a05b3520149989f6dd046", null ],
    [ "operator=", "d3/d41/classG4Physics2DVector.html#a0f9d036762d32e9faf96e98f939130ad", null ],
    [ "operator==", "d3/d41/classG4Physics2DVector.html#aebf7466a38b406cd60c9e59190c1fa48", null ],
    [ "PrepareVectors", "d3/d41/classG4Physics2DVector.html#a7fb22ad3d21ea31dc512ec7e576e2367", null ],
    [ "PutValue", "d3/d41/classG4Physics2DVector.html#ad5db6767f9647f0f734f967b686f3365", null ],
    [ "PutVectors", "d3/d41/classG4Physics2DVector.html#a4ddd8125bfd0d69c2cac320f1e3ae8d3", null ],
    [ "PutX", "d3/d41/classG4Physics2DVector.html#aed1af77afe213d72bdf24dc76676aaed", null ],
    [ "PutY", "d3/d41/classG4Physics2DVector.html#a1fcebe526d544debf22f2add56d1a42f", null ],
    [ "Retrieve", "d3/d41/classG4Physics2DVector.html#a0961572d703c6236569c2add058e8a84", null ],
    [ "ScaleVector", "d3/d41/classG4Physics2DVector.html#ad0461af48e2bfabc08f7f1ceeff01b79", null ],
    [ "SetBicubicInterpolation", "d3/d41/classG4Physics2DVector.html#ad0f10500632ca13a38a818b43f4bc406", null ],
    [ "SetVerboseLevel", "d3/d41/classG4Physics2DVector.html#a579317161f13b5b75d4c4c6d7c3f0b6a", null ],
    [ "Store", "d3/d41/classG4Physics2DVector.html#a3834eacc93035f24e26ddbf71a444e4c", null ],
    [ "Value", "d3/d41/classG4Physics2DVector.html#ae3681e4d1784c4463e3197b62910581d", null ],
    [ "Value", "d3/d41/classG4Physics2DVector.html#a8acdfccbc587e3b9956d2b3985230cfd", null ],
    [ "numberOfXNodes", "d3/d41/classG4Physics2DVector.html#a74c02fcbf488cdc57822af4a48754172", null ],
    [ "numberOfYNodes", "d3/d41/classG4Physics2DVector.html#aa2944272bb9d26acd17f130b7295cc16", null ],
    [ "type", "d3/d41/classG4Physics2DVector.html#af39c1674d94eb8fc25355479378a21b4", null ],
    [ "useBicubic", "d3/d41/classG4Physics2DVector.html#a6d5f39c6c94c873daaed7608ef38a1da", null ],
    [ "value", "d3/d41/classG4Physics2DVector.html#a06f3187792818ebd01b664c7b6789454", null ],
    [ "verboseLevel", "d3/d41/classG4Physics2DVector.html#aae2dfa65ec769d6a628750e57619e46d", null ],
    [ "xVector", "d3/d41/classG4Physics2DVector.html#a5a1f9ddcd322b4ca667763aac42d4891", null ],
    [ "yVector", "d3/d41/classG4Physics2DVector.html#aef0cc197c9c6e3b9e407c4da3c21ee49", null ]
];