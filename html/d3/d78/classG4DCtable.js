var classG4DCtable =
[
    [ "G4DCtable", "d3/d78/classG4DCtable.html#a66762c5ccac23700414922ef9f8c481f", null ],
    [ "~G4DCtable", "d3/d78/classG4DCtable.html#a4d5255bea375f698655d27154c29ac01", null ],
    [ "entries", "d3/d78/classG4DCtable.html#a10c4bdbb66c4e9303a41b128370efecc", null ],
    [ "GetCollectionID", "d3/d78/classG4DCtable.html#aefe331022a51787a30f323a8cefb1639", null ],
    [ "GetCollectionID", "d3/d78/classG4DCtable.html#ac22afb8fc3e2672bac446433da995803", null ],
    [ "GetDCname", "d3/d78/classG4DCtable.html#a0013cbda069180264ac9c6a5f5a81a5b", null ],
    [ "GetDMname", "d3/d78/classG4DCtable.html#a3eb53ba90c1673025f0b2a47843f5d4b", null ],
    [ "Registor", "d3/d78/classG4DCtable.html#aed21cbef67930601f351f643959dd48b", null ],
    [ "DClist", "d3/d78/classG4DCtable.html#ab82a02f94065ca3ce68afedd1c5482ff", null ],
    [ "DMlist", "d3/d78/classG4DCtable.html#a5176fd2f6dbede317fba244ed7061a39", null ]
];