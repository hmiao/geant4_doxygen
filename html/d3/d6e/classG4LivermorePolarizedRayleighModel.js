var classG4LivermorePolarizedRayleighModel =
[
    [ "G4LivermorePolarizedRayleighModel", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#a75d869ef48830e11b6901f90eb1ac32a", null ],
    [ "~G4LivermorePolarizedRayleighModel", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#a1d22cee2232f1b1714c1583a19ccf05a", null ],
    [ "G4LivermorePolarizedRayleighModel", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#ae0a74f0dfd498dd886074ebac7290a18", null ],
    [ "ComputeCrossSectionPerAtom", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#ac37ac2af5ef57ab30537a2ef280892ab", null ],
    [ "GenerateCosTheta", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#acb1a4a905b04f48656b39cb4cbcb9a8d", null ],
    [ "GeneratePhi", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#a049046a990be997dc77e858d1faf7f4d", null ],
    [ "GeneratePolarizationAngle", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#ac8c173c8372c76a0b8d36e871cab546d", null ],
    [ "GetPhotonPolarization", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#aa2b5dc13199e4148372c600f0f8082f9", null ],
    [ "Initialise", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#aed0086a04bd4602b2d48ce77ead7d871", null ],
    [ "InitialiseForElement", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#a1bf3bb599685516f83130f8179886964", null ],
    [ "InitialiseLocal", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#af47ccfa92382248e6e52585004c74bec", null ],
    [ "operator=", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#a56124594b3b0ccc4ec31312ae1fd750a", null ],
    [ "ReadData", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#a05e6d6f50e71df4bcd4208dd0c36d3db", null ],
    [ "SampleSecondaries", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#ade4b1ee0137c12d0b46854106087be42", null ],
    [ "dataCS", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#a5d92aba2ee12e49611de1342a44894de", null ],
    [ "formFactorData", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#aa3c16346257256f21e17d01577970363", null ],
    [ "fParticleChange", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#a55f25a59de3ac5655ea249ad98604f85", null ],
    [ "isInitialised", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#ae04c05eee29f24db7ec4b32dd24d7bd8", null ],
    [ "lowEnergyLimit", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#ae6c3abb0bf28f224c269fb34ade83697", null ],
    [ "maxZ", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#a0fff4cb6118f1d63ceb9bb95ca61b2c1", null ],
    [ "verboseLevel", "d3/d6e/classG4LivermorePolarizedRayleighModel.html#a410d8fedcb1ed5bef35ebbc10010a3f6", null ]
];