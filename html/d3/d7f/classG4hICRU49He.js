var classG4hICRU49He =
[
    [ "G4hICRU49He", "d3/d7f/classG4hICRU49He.html#acc07e3af68964876ea0cb0abe86470c8", null ],
    [ "~G4hICRU49He", "d3/d7f/classG4hICRU49He.html#af3f1307b4ffc42484c3d918c1705e20b", null ],
    [ "ElectronicStoppingPower", "d3/d7f/classG4hICRU49He.html#a67b3401d800d71a02a02a8acf25bae08", null ],
    [ "HasMaterial", "d3/d7f/classG4hICRU49He.html#aff57ba2c7f79a71e7345fa0a4c0bf556", null ],
    [ "SetMoleculaNumber", "d3/d7f/classG4hICRU49He.html#ad266f6d2e2377d458b06d6a33798710c", null ],
    [ "StoppingPower", "d3/d7f/classG4hICRU49He.html#a61a1672dc7fac44b81f0aeb8cf7e46cc", null ],
    [ "iMolecula", "d3/d7f/classG4hICRU49He.html#a6044eaa424eaecae97864c236c1bf72b", null ],
    [ "rateMass", "d3/d7f/classG4hICRU49He.html#a141fa93beb2b91632380c63167af7220", null ]
];