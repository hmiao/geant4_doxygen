var classG4TwistedTrap =
[
    [ "G4TwistedTrap", "d3/da8/classG4TwistedTrap.html#ac8e51069fe4e5f247a2c4184a1910f03", null ],
    [ "G4TwistedTrap", "d3/da8/classG4TwistedTrap.html#ae1717edb167eda3d4076ee1546703070", null ],
    [ "~G4TwistedTrap", "d3/da8/classG4TwistedTrap.html#abb75eeb78edc55bdc1164172b7920fbe", null ],
    [ "G4TwistedTrap", "d3/da8/classG4TwistedTrap.html#a673cc33afff9b73eeb5014bf87c24c2b", null ],
    [ "G4TwistedTrap", "d3/da8/classG4TwistedTrap.html#a1ccdeba699a67b91a17768f7926fd0b1", null ],
    [ "Clone", "d3/da8/classG4TwistedTrap.html#a19e78f226003395c70c6318c89bffbec", null ],
    [ "GetAzimuthalAnglePhi", "d3/da8/classG4TwistedTrap.html#a64ec5a32a5d2cfdff157ee3d16094081", null ],
    [ "GetEntityType", "d3/da8/classG4TwistedTrap.html#a0523e47ff12d370f3e50d478d6df2f1e", null ],
    [ "GetPhiTwist", "d3/da8/classG4TwistedTrap.html#ae32f314257ae3d6f96dec791195c33ed", null ],
    [ "GetPolarAngleTheta", "d3/da8/classG4TwistedTrap.html#a1b7c79f3468b9dc25a460b9bdf232a90", null ],
    [ "GetTiltAngleAlpha", "d3/da8/classG4TwistedTrap.html#a8192c144a7f2aaa095c53c95db6640e0", null ],
    [ "GetX1HalfLength", "d3/da8/classG4TwistedTrap.html#a9dccbad062555a618277af972612ba97", null ],
    [ "GetX2HalfLength", "d3/da8/classG4TwistedTrap.html#abeaf9cf6d9bbeb40e19e6abfc7dfcabc", null ],
    [ "GetX3HalfLength", "d3/da8/classG4TwistedTrap.html#abfce069b9b0063abe9cb10d4b22b3a4b", null ],
    [ "GetX4HalfLength", "d3/da8/classG4TwistedTrap.html#ac193b34c63f98c3a3ce4e8f69f52f6b4", null ],
    [ "GetY1HalfLength", "d3/da8/classG4TwistedTrap.html#af93a517c04b52fdab65d4ef4c7be4065", null ],
    [ "GetY2HalfLength", "d3/da8/classG4TwistedTrap.html#adcfb8094fa9fedbeb1be169d3d7bff4e", null ],
    [ "GetZHalfLength", "d3/da8/classG4TwistedTrap.html#a8ec765751dcbdc41d4c5598b72953635", null ],
    [ "operator=", "d3/da8/classG4TwistedTrap.html#acc419dcccb25d7674faec3685c74fba0", null ],
    [ "StreamInfo", "d3/da8/classG4TwistedTrap.html#a715fbcc2c1729f36e270d62feff8c70f", null ]
];