var classPTL_1_1Task_3_01void_00_01void_01_4 =
[
    [ "future_type", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#a4c847f848d037576389bd467273b27de", null ],
    [ "packaged_task_type", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#a7928f3e5e0593ab3f2821de816534d4f", null ],
    [ "promise_type", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#ab2e82125bf8dd376a640d8b58d2e3688", null ],
    [ "result_type", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#a8e2b4e88267447cc5d0abd5f92fa000c", null ],
    [ "RetT", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#a81a9977314dce02c7f1fc077b7cbfdf9", null ],
    [ "this_type", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#aa4b3ef74916e881cc6df730ee98f8eea", null ],
    [ "Task", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#a7194d08501e70ad6273fddd346406e18", null ],
    [ "Task", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#a8a31503f55ea3e299b17ff845005965d", null ],
    [ "~Task", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#a6ed4f7503232dfaa36a0c31491b932dd", null ],
    [ "Task", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#aaf9ce65e3669a076b8b7c31bfb05c8cd", null ],
    [ "Task", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#a173f06da8e7c24804a60ea0ddc304488", null ],
    [ "get", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#a5a4eb15763c63990633ddfc5144bd667", null ],
    [ "get_future", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#ad47c3ed95ffa2ec6b1e70fdf9d0997be", null ],
    [ "operator()", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#a9b9cb0e5fd495a36b760408e11386bf7", null ],
    [ "operator=", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#a1108f80cfbcc1cb75d28ee89c11804df", null ],
    [ "operator=", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#a04fa1612e7e046c2557c3418f2900d35", null ],
    [ "wait", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#aaea66e38a32f79837e79cf8afa692615", null ],
    [ "m_ptask", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html#a8434cf704f761fb535047c5508079999", null ]
];