var classG4CollisionComposite =
[
    [ "Register", "d7/df0/structG4CollisionComposite_1_1Register.html", "d7/df0/structG4CollisionComposite_1_1Register" ],
    [ "Resolve", "de/db0/structG4CollisionComposite_1_1Resolve.html", "de/db0/structG4CollisionComposite_1_1Resolve" ],
    [ "G4CollisionComposite", "d3/de4/classG4CollisionComposite.html#a962fbfac81775aba0c2360c75821e432", null ],
    [ "~G4CollisionComposite", "d3/de4/classG4CollisionComposite.html#aa30d19cebd9282b504a2cae2aa5fe315", null ],
    [ "G4CollisionComposite", "d3/de4/classG4CollisionComposite.html#abb042682ffc9308178606e93c07d3306", null ],
    [ "AddComponent", "d3/de4/classG4CollisionComposite.html#ab905743a0ed071b8e58f2537f8d7dea3", null ],
    [ "BufferCrossSection", "d3/de4/classG4CollisionComposite.html#a645852b222bad89d1accd96775416ac4", null ],
    [ "BufferedCrossSection", "d3/de4/classG4CollisionComposite.html#a90c64e02ce5cb263137a4e3c34f77bff", null ],
    [ "CrossSection", "d3/de4/classG4CollisionComposite.html#acc44cebfdee497cda3f0fb85489f6813", null ],
    [ "FinalState", "d3/de4/classG4CollisionComposite.html#ac1f5b980659d6ea7abec2c8f1ee3586c", null ],
    [ "GetAngularDistribution", "d3/de4/classG4CollisionComposite.html#a5cfbf9a892cadd20330152d22b3968f0", null ],
    [ "GetComponents", "d3/de4/classG4CollisionComposite.html#a8401d7c9f41074128bb612ddd962ef67", null ],
    [ "GetCrossSectionSource", "d3/de4/classG4CollisionComposite.html#ab3d98bad80306d19a9a9766af6e94b2c", null ],
    [ "IsInCharge", "d3/de4/classG4CollisionComposite.html#a49ef27822903dd5f5bf8e8235bf71fc3", null ],
    [ "operator=", "d3/de4/classG4CollisionComposite.html#ac661d0334776aa11a5e4a32c358fa87f", null ],
    [ "bufferMutex", "d3/de4/classG4CollisionComposite.html#aafe6adfb7238b17ce141fe6eb93d689d", null ],
    [ "components", "d3/de4/classG4CollisionComposite.html#a0d36999f15ce3592fe518736f292d08a", null ],
    [ "nPoints", "d3/de4/classG4CollisionComposite.html#a67be7f7bbaf8ede330833550edb60729", null ],
    [ "theBuffer", "d3/de4/classG4CollisionComposite.html#a4092bc5b5a71a825982128d813a9c256", null ],
    [ "theT", "d3/de4/classG4CollisionComposite.html#ab7f10bd5cab82209e60ae99037307f05", null ]
];