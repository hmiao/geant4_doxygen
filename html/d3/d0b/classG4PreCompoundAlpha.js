var classG4PreCompoundAlpha =
[
    [ "G4PreCompoundAlpha", "d3/d0b/classG4PreCompoundAlpha.html#a4ca891fdf597c44971629060cd98c37a", null ],
    [ "~G4PreCompoundAlpha", "d3/d0b/classG4PreCompoundAlpha.html#aed2fe83969b3d7ad79cc5aeeb86e6c36", null ],
    [ "G4PreCompoundAlpha", "d3/d0b/classG4PreCompoundAlpha.html#abe5e80d5144be0f388319ab410661a78", null ],
    [ "CoalescenceFactor", "d3/d0b/classG4PreCompoundAlpha.html#ad365a81aa5d8d6918515f715204de230", null ],
    [ "FactorialFactor", "d3/d0b/classG4PreCompoundAlpha.html#a0f0e8c27b14ac3f9cadb0e6cef39cef0", null ],
    [ "GetAlpha", "d3/d0b/classG4PreCompoundAlpha.html#a790d7a1c4cd6b08b3a4af35347542192", null ],
    [ "GetRj", "d3/d0b/classG4PreCompoundAlpha.html#a2321715d4e49a3ad4a38862adc5f4824", null ],
    [ "operator!=", "d3/d0b/classG4PreCompoundAlpha.html#a6da8ca884d32b46c7e49b74fbdb7004d", null ],
    [ "operator=", "d3/d0b/classG4PreCompoundAlpha.html#a776414014e7ce3e7873b318479f017e1", null ],
    [ "operator==", "d3/d0b/classG4PreCompoundAlpha.html#a8906de8f177d77e976afa7f5540c6890", null ],
    [ "theAlphaCoulombBarrier", "d3/d0b/classG4PreCompoundAlpha.html#a737bcee138d7dd26f9732bb3c0d75bc4", null ]
];