var classG4XAqmElastic =
[
    [ "G4XAqmElastic", "d3/dec/classG4XAqmElastic.html#aa829591de1abe649029cce16aaa3421a", null ],
    [ "~G4XAqmElastic", "d3/dec/classG4XAqmElastic.html#a19f953c555ee8399f7bb0e1e8c8f2953", null ],
    [ "G4XAqmElastic", "d3/dec/classG4XAqmElastic.html#a6cf2eb313a69be7f3d5218f747db2eda", null ],
    [ "CrossSection", "d3/dec/classG4XAqmElastic.html#ace333ceac9524bb731206a481c29116f", null ],
    [ "GetComponents", "d3/dec/classG4XAqmElastic.html#a46f356528dee194f7ac74f9defdd6e6a", null ],
    [ "IsValid", "d3/dec/classG4XAqmElastic.html#a6d2eaaa73a9799837a93c933b4029b80", null ],
    [ "Name", "d3/dec/classG4XAqmElastic.html#a3b07952ba42ab38b630ce50c10f4dee0", null ],
    [ "operator!=", "d3/dec/classG4XAqmElastic.html#a31e61feb23f2ae820428ffc935846111", null ],
    [ "operator=", "d3/dec/classG4XAqmElastic.html#af79f8c01bb0484776fae8a5c7b9a422a", null ],
    [ "operator==", "d3/dec/classG4XAqmElastic.html#aa5dbac82fda10dde6d975e001984ba60", null ],
    [ "_highLimit", "d3/dec/classG4XAqmElastic.html#a7aba8eb85002a5324061ed8eb9d99352", null ],
    [ "_lowLimit", "d3/dec/classG4XAqmElastic.html#a89ff19028b662486c726652b74a633b7", null ]
];