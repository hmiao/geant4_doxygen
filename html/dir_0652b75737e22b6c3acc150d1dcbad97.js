var dir_0652b75737e22b6c3acc150d1dcbad97 =
[
    [ "adjoint", "dir_9c3f14b16a1c43566e1fe1430e4027af.html", "dir_9c3f14b16a1c43566e1fe1430e4027af" ],
    [ "bosons", "dir_2c0a867cef174bbc5200cfa8b7cc274c.html", "dir_2c0a867cef174bbc5200cfa8b7cc274c" ],
    [ "hadrons", "dir_7204e8f2f3617270e7f97e426f8855ac.html", "dir_7204e8f2f3617270e7f97e426f8855ac" ],
    [ "leptons", "dir_3aa3a73a8931b70b80896f1082227c30.html", "dir_3aa3a73a8931b70b80896f1082227c30" ],
    [ "management", "dir_9d2f66491138201e97ca665b9bbc4f88.html", "dir_9d2f66491138201e97ca665b9bbc4f88" ],
    [ "shortlived", "dir_3527be370300005cb391f8a3a8c7bd8f.html", "dir_3527be370300005cb391f8a3a8c7bd8f" ],
    [ "utils", "dir_2447374e6234941e7940604cd7337361.html", "dir_2447374e6234941e7940604cd7337361" ]
];