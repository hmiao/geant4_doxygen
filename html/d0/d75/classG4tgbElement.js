var classG4tgbElement =
[
    [ "G4tgbElement", "d0/d75/classG4tgbElement.html#ad448f49b3918e039c0a6561e122e03bb", null ],
    [ "~G4tgbElement", "d0/d75/classG4tgbElement.html#a635c1c5ad84dcb0aec9c8bd68ce58bac", null ],
    [ "G4tgbElement", "d0/d75/classG4tgbElement.html#ab14e0fc010633e994b8c11a7bcaa071f", null ],
    [ "BuildG4ElementFromIsotopes", "d0/d75/classG4tgbElement.html#a7bfa2333d38b8331c98dab916a98dfa9", null ],
    [ "BuildG4ElementSimple", "d0/d75/classG4tgbElement.html#ad8c159b70d031aab7dbc676b27c4b7f5", null ],
    [ "GetName", "d0/d75/classG4tgbElement.html#a56f5bc130def05a2a3ebdde1d4402d3c", null ],
    [ "GetType", "d0/d75/classG4tgbElement.html#aa5196c1ad72fa11b21c540c227e30e9f", null ],
    [ "theG4Elem", "d0/d75/classG4tgbElement.html#a4c8476400f67a5f45e2d6bcf2391457c", null ],
    [ "theTgrElem", "d0/d75/classG4tgbElement.html#aa972ea76e0eee990b40ba9dd2b8c0795", null ]
];