var classG4B12GEMChannel =
[
    [ "G4B12GEMChannel", "d0/df4/classG4B12GEMChannel.html#a110096ce1d2001ce75eeca78e684dcf9", null ],
    [ "~G4B12GEMChannel", "d0/df4/classG4B12GEMChannel.html#a88aed03ed6326352edd604b9c10e29a2", null ],
    [ "G4B12GEMChannel", "d0/df4/classG4B12GEMChannel.html#a9f76acb5c6309006a0101c8fd47bee73", null ],
    [ "operator!=", "d0/df4/classG4B12GEMChannel.html#a3748400c64ff148bd9f01078db6e0d60", null ],
    [ "operator=", "d0/df4/classG4B12GEMChannel.html#a78e557494e83e6f96c2fc7dfef3bb873", null ],
    [ "operator==", "d0/df4/classG4B12GEMChannel.html#a935f123df5828256b7852464c98dc8ba", null ],
    [ "theEvaporationProbability", "d0/df4/classG4B12GEMChannel.html#a97f4852de38bd633201e30e363f29d0d", null ]
];