var gzread_8c =
[
    [ "gz_avail", "d0/df1/gzread_8c.html#a70cabb2e72f364a4eedd6de0e042eb8a", null ],
    [ "gz_decomp", "d0/df1/gzread_8c.html#a61a068e4411705b971cf6e7e14aa4468", null ],
    [ "gz_fetch", "d0/df1/gzread_8c.html#a5ae4f6aafa8572dedbd2ba7444383d32", null ],
    [ "gz_load", "d0/df1/gzread_8c.html#a8dea7f95260f4814e9547572f941b27f", null ],
    [ "gz_look", "d0/df1/gzread_8c.html#ac57c09dbcbc1aff70120ca14d654a176", null ],
    [ "gz_read", "d0/df1/gzread_8c.html#a614e74388244702a46f5aeaadd5507d7", null ],
    [ "gz_skip", "d0/df1/gzread_8c.html#a1f06c8da90399d2498eb0de5b73b64a3", null ],
    [ "gzclose_r", "d0/df1/gzread_8c.html#a74218c3c6e5eddf23a9f3cb2e08f9637", null ],
    [ "gzdirect", "d0/df1/gzread_8c.html#a1f049b9405deb0e7dad1fe97ed4dd8c2", null ],
    [ "gzfread", "d0/df1/gzread_8c.html#acebeb254a3cecff218bdd52bcd90d7cf", null ],
    [ "gzgetc", "d0/df1/gzread_8c.html#a1e9f6a517ed1e04c3765fe24153fb639", null ],
    [ "gzgetc_", "d0/df1/gzread_8c.html#a3bcd38c3aaf6694dadfc7c29d1ae7ddb", null ],
    [ "gzgets", "d0/df1/gzread_8c.html#a89fdd562347c7b7739830b99b988db20", null ],
    [ "gzread", "d0/df1/gzread_8c.html#a2d0d971b446b67bbc4261fa5fef622bd", null ],
    [ "gzungetc", "d0/df1/gzread_8c.html#a0b81c1a6e144a3cc5cd97a26688fed69", null ],
    [ "OF", "d0/df1/gzread_8c.html#a0e692e67dbf2d47bed0fed5a8658f85f", null ],
    [ "OF", "d0/df1/gzread_8c.html#a11bce2e08e338efacef40f1654f957b4", null ],
    [ "OF", "d0/df1/gzread_8c.html#a71adbbff9eac6448d5f5769b73e82eb4", null ],
    [ "OF", "d0/df1/gzread_8c.html#adb9a85dcd7e79c5ae7d954e888302adb", null ]
];