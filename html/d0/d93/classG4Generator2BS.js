var classG4Generator2BS =
[
    [ "G4Generator2BS", "d0/d93/classG4Generator2BS.html#ae2c91ed857d7bd20fe200cbb15af4e6a", null ],
    [ "~G4Generator2BS", "d0/d93/classG4Generator2BS.html#afad769a4e9096a170b17f4aece60621d", null ],
    [ "G4Generator2BS", "d0/d93/classG4Generator2BS.html#a5594dc1e99838b8e70d9940372f927f7", null ],
    [ "operator=", "d0/d93/classG4Generator2BS.html#a755f7d042af7fb17efd876b83f76a57e", null ],
    [ "PrintGeneratorInformation", "d0/d93/classG4Generator2BS.html#a5579765a1062ed8d9417b20039061a52", null ],
    [ "RejectionFunction", "d0/d93/classG4Generator2BS.html#a3a7311d40bdc1c4cf89ab78cdf6a9e20", null ],
    [ "SampleDirection", "d0/d93/classG4Generator2BS.html#a621dd96277ac8fe2e9f86be7b7c8c222", null ],
    [ "delta", "d0/d93/classG4Generator2BS.html#a6592e0336b094ddf5a9dc9ec744eea1b", null ],
    [ "fz", "d0/d93/classG4Generator2BS.html#aca25bbc86c40657b5e4d4ef9a86495c7", null ],
    [ "g4pow", "d0/d93/classG4Generator2BS.html#ab0fa2b42fc4d5f8d7132ddd2fe50f496", null ],
    [ "nwarn", "d0/d93/classG4Generator2BS.html#a061ee3ab00be41576babdc9c21f96519", null ],
    [ "ratio", "d0/d93/classG4Generator2BS.html#a04f639c9eba83381a409b32ec20254b8", null ],
    [ "ratio1", "d0/d93/classG4Generator2BS.html#a1f1ca6a4bf4aa173171a11605ef78ce8", null ],
    [ "ratio2", "d0/d93/classG4Generator2BS.html#a6b4463ea3339e15e0fe851c9f67ffd25", null ]
];