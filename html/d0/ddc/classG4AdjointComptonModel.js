var classG4AdjointComptonModel =
[
    [ "G4AdjointComptonModel", "d0/ddc/classG4AdjointComptonModel.html#a460b6acb0c416f73c8b716dc823b0363", null ],
    [ "~G4AdjointComptonModel", "d0/ddc/classG4AdjointComptonModel.html#ab0201d4f49d628b7de33eacda8a7ba89", null ],
    [ "G4AdjointComptonModel", "d0/ddc/classG4AdjointComptonModel.html#a4564ee5088d63b1c7bdd048376a68d7e", null ],
    [ "AdjointCrossSection", "d0/ddc/classG4AdjointComptonModel.html#a8e158f48f0da0b10a720812b13c132b0", null ],
    [ "DiffCrossSectionPerAtomPrimToScatPrim", "d0/ddc/classG4AdjointComptonModel.html#aeac2658f772a7002e849e041b0678be0", null ],
    [ "DiffCrossSectionPerAtomPrimToSecond", "d0/ddc/classG4AdjointComptonModel.html#a1800a3f74b44c330f97e86e7e774b73e", null ],
    [ "GetSecondAdjEnergyMaxForScatProjToProj", "d0/ddc/classG4AdjointComptonModel.html#aa5e5b73fa4a7eb3cef7a29a3533185fa", null ],
    [ "GetSecondAdjEnergyMinForProdToProj", "d0/ddc/classG4AdjointComptonModel.html#ab03716d72bf846a534083d8fdc76e028", null ],
    [ "operator=", "d0/ddc/classG4AdjointComptonModel.html#a956a768f3b524aad3dc6adfe42168c1e", null ],
    [ "RapidSampleSecondaries", "d0/ddc/classG4AdjointComptonModel.html#a9ca5d6624ef4625868adcecdee1c3b99", null ],
    [ "SampleSecondaries", "d0/ddc/classG4AdjointComptonModel.html#a81c8ee132bb82d28f105827863311c71", null ],
    [ "SetDirectProcess", "d0/ddc/classG4AdjointComptonModel.html#a8e5078b3e9bc4637ba444f0175f13c2a", null ],
    [ "fDirectCS", "d0/ddc/classG4AdjointComptonModel.html#a0f033bf3529e2b4fb1d044b5a5167746", null ],
    [ "fDirectProcess", "d0/ddc/classG4AdjointComptonModel.html#ae317c9393deb793b8d380a7585ef76f5", null ]
];