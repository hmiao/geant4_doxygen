var classG4ParticleHPDeExGammas =
[
    [ "G4ParticleHPDeExGammas", "d0/d4f/classG4ParticleHPDeExGammas.html#af161c70d0876f7189787c9bf94ebe198", null ],
    [ "~G4ParticleHPDeExGammas", "d0/d4f/classG4ParticleHPDeExGammas.html#a4e5e80e07850c8ee1d1f70c6daa9e5fd", null ],
    [ "GetDecayGammas", "d0/d4f/classG4ParticleHPDeExGammas.html#a38ad32cb4bfbd66f269f0cc5f7ccc94f", null ],
    [ "GetLevel", "d0/d4f/classG4ParticleHPDeExGammas.html#a27698f60032d693aa3dfe8ebe6a9beda", null ],
    [ "GetLevelEnergy", "d0/d4f/classG4ParticleHPDeExGammas.html#aa4580ead28c4f0cacf4ef37cb1890e9b", null ],
    [ "GetNumberOfLevels", "d0/d4f/classG4ParticleHPDeExGammas.html#a5eec58350b15a918f7825ddb5cece66c", null ],
    [ "Init", "d0/d4f/classG4ParticleHPDeExGammas.html#aba97ff805bf3bb2e48bb187f9e3a0a58", null ],
    [ "levelSize", "d0/d4f/classG4ParticleHPDeExGammas.html#af11662ce1f83fd0aa3efdd8e6e4bdf12", null ],
    [ "levelStart", "d0/d4f/classG4ParticleHPDeExGammas.html#a4c830ee2339fbb6c777ca5a84b9f6ba7", null ],
    [ "nLevels", "d0/d4f/classG4ParticleHPDeExGammas.html#aa004e80b4f6df79d354ed527c4b8c44f", null ],
    [ "theLevels", "d0/d4f/classG4ParticleHPDeExGammas.html#a3c037a9a295ee9f12624e012e4f23b46", null ]
];