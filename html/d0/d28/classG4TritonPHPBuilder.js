var classG4TritonPHPBuilder =
[
    [ "G4TritonPHPBuilder", "d0/d28/classG4TritonPHPBuilder.html#ae3796aa698409678bf067f59e735d019", null ],
    [ "~G4TritonPHPBuilder", "d0/d28/classG4TritonPHPBuilder.html#a47b308c36ce04656be9fbb848fa3c5e3", null ],
    [ "Build", "d0/d28/classG4TritonPHPBuilder.html#ad692f833a07d6686e9f6c8685d48a56e", null ],
    [ "Build", "d0/d28/classG4TritonPHPBuilder.html#a74b7aaeebb66754c5bdf45955a1a842d", null ],
    [ "Build", "d0/d28/classG4TritonPHPBuilder.html#a4892face87b4c4798bcb4796d2836375", null ],
    [ "Build", "d0/d28/classG4TritonPHPBuilder.html#abd20b8dccb8117b1668ca347294cc96d", null ],
    [ "SetMaxEnergy", "d0/d28/classG4TritonPHPBuilder.html#ae39e2ae1910873222d7f7f21c6c9c312", null ],
    [ "SetMinEnergy", "d0/d28/classG4TritonPHPBuilder.html#a24e2ef0c54014c0314f90ccb3622386e", null ],
    [ "theMax", "d0/d28/classG4TritonPHPBuilder.html#a5728ca57e050186f8cd2457dcc7a7805", null ],
    [ "theMin", "d0/d28/classG4TritonPHPBuilder.html#af3cfc1b33f73c7698eac1c387e42af44", null ]
];