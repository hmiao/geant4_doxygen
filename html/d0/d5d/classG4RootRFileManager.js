var classG4RootRFileManager =
[
    [ "G4RootRFileManager", "d0/d5d/classG4RootRFileManager.html#a1da179f2d0595bfec859ae06d52e8c8a", null ],
    [ "G4RootRFileManager", "d0/d5d/classG4RootRFileManager.html#a40a2b96734a2c713d345b5aa6620899e", null ],
    [ "~G4RootRFileManager", "d0/d5d/classG4RootRFileManager.html#a5450781f832a4f88e0e3012ddfff6081", null ],
    [ "CloseFiles", "d0/d5d/classG4RootRFileManager.html#ac5d75c27af96a6446c9c64163a88f02c", null ],
    [ "GetFileType", "d0/d5d/classG4RootRFileManager.html#ae7bf2fb06f2246f405f5e351ed35a603", null ],
    [ "GetRFile", "d0/d5d/classG4RootRFileManager.html#a5f11ff3892b4e951bd2250d2f8b6507b", null ],
    [ "OpenRFile", "d0/d5d/classG4RootRFileManager.html#a4887e799d52f70b4a5b91a5ec68adbf2", null ],
    [ "fkClass", "d0/d5d/classG4RootRFileManager.html#a91c3e1eb507601f22e5ca9f6e67102d1", null ],
    [ "fRFiles", "d0/d5d/classG4RootRFileManager.html#ad7a7ce4c9c6db2fc07f6c74b0bfc2b58", null ]
];