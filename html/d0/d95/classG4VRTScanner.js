var classG4VRTScanner =
[
    [ "G4VRTScanner", "d0/d95/classG4VRTScanner.html#a61470508aa67acc0145876a0d395e818", null ],
    [ "~G4VRTScanner", "d0/d95/classG4VRTScanner.html#ac0d1cfaba427beb5962822d8db013933", null ],
    [ "Coords", "d0/d95/classG4VRTScanner.html#ab8b9107e9cb2aa6446781bbd9b276cad", null ],
    [ "Draw", "d0/d95/classG4VRTScanner.html#a69458bc54eede8e3f4c1e126bb70af13", null ],
    [ "GetGSName", "d0/d95/classG4VRTScanner.html#a8a8cf154e716dadc5d5158600e1fbf98", null ],
    [ "GetGSNickname", "d0/d95/classG4VRTScanner.html#ac1d9b9b7e15755c0f6b49fe1aed749e9", null ],
    [ "Initialize", "d0/d95/classG4VRTScanner.html#a395fe81b973d419a8c81a6977a141c94", null ]
];