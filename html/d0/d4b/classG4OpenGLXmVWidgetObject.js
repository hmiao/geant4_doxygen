var classG4OpenGLXmVWidgetObject =
[
    [ "G4OpenGLXmVWidgetObject", "d0/d4b/classG4OpenGLXmVWidgetObject.html#ad24dcdc89d44929106911699a2886b08", null ],
    [ "~G4OpenGLXmVWidgetObject", "d0/d4b/classG4OpenGLXmVWidgetObject.html#a54fdf9c2d93dc17d73f63338fa1c7051", null ],
    [ "G4OpenGLXmVWidgetObject", "d0/d4b/classG4OpenGLXmVWidgetObject.html#ac115019b492cfcbb995f4e52b5f34da4", null ],
    [ "GetView", "d0/d4b/classG4OpenGLXmVWidgetObject.html#a115740626805dcb62786e98e46a08a61", null ],
    [ "operator=", "d0/d4b/classG4OpenGLXmVWidgetObject.html#aaa91ac5070348a561e84c645385e0086", null ],
    [ "ProcesspView", "d0/d4b/classG4OpenGLXmVWidgetObject.html#a14fc2d03e9b5c3041ec7ad695c44f262", null ],
    [ "bgnd", "d0/d4b/classG4OpenGLXmVWidgetObject.html#afbad3c82d94fb74b1bc3aa9e28c6a575", null ],
    [ "borcol", "d0/d4b/classG4OpenGLXmVWidgetObject.html#a47b7834c90cbe60247dd01bd28655e82", null ],
    [ "cmap", "d0/d4b/classG4OpenGLXmVWidgetObject.html#a2d684882a6b2814e0cc9a55ed3e654d8", null ],
    [ "depth", "d0/d4b/classG4OpenGLXmVWidgetObject.html#a60ea91c0f8f1e62ca1b8e71d184dc877", null ],
    [ "pView", "d0/d4b/classG4OpenGLXmVWidgetObject.html#af136f10934c3d8590b07d4541b8ab878", null ],
    [ "top", "d0/d4b/classG4OpenGLXmVWidgetObject.html#a3820baf6d2049b26dbb026617f63fce7", null ],
    [ "visual", "d0/d4b/classG4OpenGLXmVWidgetObject.html#ad801341beef3daeb4c1c40a2af16c181", null ]
];