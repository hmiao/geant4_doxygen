var classG4CrossSectionPatch =
[
    [ "G4CrossSectionPatch", "d0/ddb/classG4CrossSectionPatch.html#a4e3856f99e4ae9ffed9b63891ed61917", null ],
    [ "~G4CrossSectionPatch", "d0/ddb/classG4CrossSectionPatch.html#a172b3d8f02f7a5fe6a71f7f54f3d3a7f", null ],
    [ "G4CrossSectionPatch", "d0/ddb/classG4CrossSectionPatch.html#acbe48397dec5e96fe026106eea83153a", null ],
    [ "CrossSection", "d0/ddb/classG4CrossSectionPatch.html#a9010a732127f931f3ad187927788bc7f", null ],
    [ "GetComponents", "d0/ddb/classG4CrossSectionPatch.html#aaa8d46007c3f70dc73bc2588bb01b7b3", null ],
    [ "IsValid", "d0/ddb/classG4CrossSectionPatch.html#a73792b7e0237bfcffed60cebe19cc598", null ],
    [ "operator!=", "d0/ddb/classG4CrossSectionPatch.html#a01747c87570f71b94f9cc8406bb8191b", null ],
    [ "operator=", "d0/ddb/classG4CrossSectionPatch.html#a9815bc597c76b2e42520ee16e07252e0", null ],
    [ "operator==", "d0/ddb/classG4CrossSectionPatch.html#a942ef2c7030aa842a8d1e240bf8bf7b7", null ],
    [ "Transition", "d0/ddb/classG4CrossSectionPatch.html#a495b75b04acf028f0911704ce5b8d905", null ],
    [ "Transition", "d0/ddb/classG4CrossSectionPatch.html#a4d9b30d1f6154248b62a27fd3fa1d7a3", null ]
];