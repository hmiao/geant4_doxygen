var classG4TransparentRegXTRadiator =
[
    [ "G4TransparentRegXTRadiator", "d0/d32/classG4TransparentRegXTRadiator.html#a0d93ae83706901ee91215eed17920294", null ],
    [ "~G4TransparentRegXTRadiator", "d0/d32/classG4TransparentRegXTRadiator.html#ae07f65207cb9b015d2f7ceabb50c57fa", null ],
    [ "DumpInfo", "d0/d32/classG4TransparentRegXTRadiator.html#a4be6e196844367333234df55ea0f3a20", null ],
    [ "GetStackFactor", "d0/d32/classG4TransparentRegXTRadiator.html#ab7cdf32a32aff43031ca7fae510cabd3", null ],
    [ "ProcessDescription", "d0/d32/classG4TransparentRegXTRadiator.html#a321221a85cdb9b60ceb34b594752f12b", null ],
    [ "SpectralXTRdEdx", "d0/d32/classG4TransparentRegXTRadiator.html#a03d4d3931c5a09f4807308e36511ad1b", null ]
];