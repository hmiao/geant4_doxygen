var classG4BOptnChangeCrossSection =
[
    [ "G4BOptnChangeCrossSection", "d0/d17/classG4BOptnChangeCrossSection.html#a35063b86e68001c948cf7e68cb864062", null ],
    [ "~G4BOptnChangeCrossSection", "d0/d17/classG4BOptnChangeCrossSection.html#a0eb0d7db7f1319583234a6a91388a8fe", null ],
    [ "ApplyFinalStateBiasing", "d0/d17/classG4BOptnChangeCrossSection.html#ad9b272200e47bd999daaed4ea14ed3e7", null ],
    [ "DistanceToApplyOperation", "d0/d17/classG4BOptnChangeCrossSection.html#a88f05a9c3d0d2c47e9d03c55e1a06b5f", null ],
    [ "GenerateBiasingFinalState", "d0/d17/classG4BOptnChangeCrossSection.html#a892dbc6dde7c3313e3ee20275a6ade07", null ],
    [ "GetBiasedCrossSection", "d0/d17/classG4BOptnChangeCrossSection.html#a24a6a2c72b48cb607e005b744cdef081", null ],
    [ "GetBiasedExponentialLaw", "d0/d17/classG4BOptnChangeCrossSection.html#ad8ca583fd80da596a7a2e591ee8caef6", null ],
    [ "GetInteractionOccured", "d0/d17/classG4BOptnChangeCrossSection.html#ab46e92dbb21466e7e904dfc8511bc505", null ],
    [ "ProvideOccurenceBiasingInteractionLaw", "d0/d17/classG4BOptnChangeCrossSection.html#a84993ae42d49e269d5f36e30c628b492", null ],
    [ "Sample", "d0/d17/classG4BOptnChangeCrossSection.html#a90ccfbc239b4ac4b2c86f1cfbabcc99e", null ],
    [ "SetBiasedCrossSection", "d0/d17/classG4BOptnChangeCrossSection.html#ad8bb17eb2e42c2753a621fb38b1c9194", null ],
    [ "SetInteractionOccured", "d0/d17/classG4BOptnChangeCrossSection.html#aeefcce4822d6c8cc6f3a7557d9199a83", null ],
    [ "UpdateForStep", "d0/d17/classG4BOptnChangeCrossSection.html#aaa5120bcb5f7323cefe8d3dbcab13f71", null ],
    [ "fBiasedExponentialLaw", "d0/d17/classG4BOptnChangeCrossSection.html#a68ce4e35eace042a6a7b0170d5f6ff07", null ],
    [ "fInteractionOccured", "d0/d17/classG4BOptnChangeCrossSection.html#ab852d7f486b010ff4ecc38bd24a333f8", null ]
];