var classG4ANSTOecpssrKxsModel =
[
    [ "G4ANSTOecpssrKxsModel", "d0/d55/classG4ANSTOecpssrKxsModel.html#a7bcc0c1c86fd63c298e5d3338ede5c4b", null ],
    [ "~G4ANSTOecpssrKxsModel", "d0/d55/classG4ANSTOecpssrKxsModel.html#a85d7b86d6d13096fee97081739f249d0", null ],
    [ "G4ANSTOecpssrKxsModel", "d0/d55/classG4ANSTOecpssrKxsModel.html#a9866a4b50a0b3b9c3aacbba8d26bb763", null ],
    [ "CalculateCrossSection", "d0/d55/classG4ANSTOecpssrKxsModel.html#ae7db1b204547dc4906b107246be7ce8d", null ],
    [ "operator=", "d0/d55/classG4ANSTOecpssrKxsModel.html#af0f03ec3b72395e649f4a25a7b6b75fb", null ],
    [ "alphaDataSetMap", "d0/d55/classG4ANSTOecpssrKxsModel.html#aa262a0e32cf8058d48585d89aad92373", null ],
    [ "carbonDataSetMap", "d0/d55/classG4ANSTOecpssrKxsModel.html#a99fa4b19f7a6f1a64ba1d269ad7766e9", null ],
    [ "interpolation", "d0/d55/classG4ANSTOecpssrKxsModel.html#a9064bf4e01f959d19f37e384082bc75c", null ],
    [ "protonDataSetMap", "d0/d55/classG4ANSTOecpssrKxsModel.html#afe49ad6c528f77c39216475657d5b48f", null ]
];