var classG4EqMagElectricField =
[
    [ "G4EqMagElectricField", "d0/d6f/classG4EqMagElectricField.html#a9bdb175c10a623a81dbb4bf87b348545", null ],
    [ "~G4EqMagElectricField", "d0/d6f/classG4EqMagElectricField.html#a9ef235ccbb26b4074f10ad41d419e704", null ],
    [ "EvaluateRhsGivenB", "d0/d6f/classG4EqMagElectricField.html#a256c18fcf66be93c1e2abf2a130fef73", null ],
    [ "SetChargeMomentumMass", "d0/d6f/classG4EqMagElectricField.html#adcf54bc3981763df1037312eb7b1892c", null ],
    [ "fElectroMagCof", "d0/d6f/classG4EqMagElectricField.html#a87388bce8e5efc5f18932976b0898385", null ],
    [ "fMassCof", "d0/d6f/classG4EqMagElectricField.html#ac1bc045515a89e6e61dfe68089efbaa6", null ]
];