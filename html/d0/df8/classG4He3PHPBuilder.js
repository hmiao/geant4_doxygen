var classG4He3PHPBuilder =
[
    [ "G4He3PHPBuilder", "d0/df8/classG4He3PHPBuilder.html#af888c2ddc8f888bf08ba68b9100dce45", null ],
    [ "~G4He3PHPBuilder", "d0/df8/classG4He3PHPBuilder.html#a705062733713f430147d4acd8e5ac13a", null ],
    [ "Build", "d0/df8/classG4He3PHPBuilder.html#af92b8b458c52f2a8e2f91de6a1c57d88", null ],
    [ "Build", "d0/df8/classG4He3PHPBuilder.html#a92eb630ef0a17dcd98b230dd822e5095", null ],
    [ "Build", "d0/df8/classG4He3PHPBuilder.html#a5911bc5f10d79a9cf0706497f9244b2c", null ],
    [ "Build", "d0/df8/classG4He3PHPBuilder.html#a88cded6003d9507a15062ab819ec4102", null ],
    [ "SetMaxEnergy", "d0/df8/classG4He3PHPBuilder.html#ab53bad7c10fbd29c8304fc521a721fe0", null ],
    [ "SetMinEnergy", "d0/df8/classG4He3PHPBuilder.html#aeffc1b892947e4f6fadf94aac37b5711", null ],
    [ "theMax", "d0/df8/classG4He3PHPBuilder.html#aa22f98a8bc9cf5b7090e75b5323f76fe", null ],
    [ "theMin", "d0/df8/classG4He3PHPBuilder.html#aea70677947ac01cd0a7f250d4c537267", null ],
    [ "theParticlePHPModel", "d0/df8/classG4He3PHPBuilder.html#a528ef2f0485f58b0547f982cfba097ca", null ]
];