var classG4VHighEnergyGenerator =
[
    [ "G4VHighEnergyGenerator", "d0/d72/classG4VHighEnergyGenerator.html#ae5388b6354de735f13009cffcebd0715", null ],
    [ "~G4VHighEnergyGenerator", "d0/d72/classG4VHighEnergyGenerator.html#aaa17a0398adcb41ba80b65c76a286fdf", null ],
    [ "G4VHighEnergyGenerator", "d0/d72/classG4VHighEnergyGenerator.html#a41c9c7f637e5bf1cbf8d8ae7f8368317", null ],
    [ "GetProjectileNucleus", "d0/d72/classG4VHighEnergyGenerator.html#a1aba7082fe9f1f8217c7e4e409b7f954", null ],
    [ "GetWoundedNucleus", "d0/d72/classG4VHighEnergyGenerator.html#aae0c88bd796cfdc3a7588e729b2a6308", null ],
    [ "ModelDescription", "d0/d72/classG4VHighEnergyGenerator.html#a0d667b48e6fead504a96b341fd05b261", null ],
    [ "operator!=", "d0/d72/classG4VHighEnergyGenerator.html#abcbf647826a7f56811147c653b0f0246", null ],
    [ "operator=", "d0/d72/classG4VHighEnergyGenerator.html#aefbaa955db461f4f2ea2f1fd5aa2030a", null ],
    [ "operator==", "d0/d72/classG4VHighEnergyGenerator.html#a198e1c8fbfc8fb1915215733a2f71d87", null ],
    [ "Scatter", "d0/d72/classG4VHighEnergyGenerator.html#ab799a1d789865b4717e2a408c8c4b3c0", null ]
];