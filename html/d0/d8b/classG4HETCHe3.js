var classG4HETCHe3 =
[
    [ "G4HETCHe3", "d0/d8b/classG4HETCHe3.html#a2513a9524fe96286339780130e036f87", null ],
    [ "~G4HETCHe3", "d0/d8b/classG4HETCHe3.html#a4af4da2c53c535381c94d218d420cc65", null ],
    [ "G4HETCHe3", "d0/d8b/classG4HETCHe3.html#ae1404863d0a11b0d177075ef07d64292", null ],
    [ "GetAlpha", "d0/d8b/classG4HETCHe3.html#a2f4058b73a1923acbd6b96d935d4c4da", null ],
    [ "GetBeta", "d0/d8b/classG4HETCHe3.html#ac365850ee1fa49a1d8fdeb3a2e4a5864", null ],
    [ "GetSpinFactor", "d0/d8b/classG4HETCHe3.html#a479f703a7373b07e6cd0805a23e75a65", null ],
    [ "K", "d0/d8b/classG4HETCHe3.html#a51b3ed7828d449ea6787ea671195b251", null ],
    [ "operator!=", "d0/d8b/classG4HETCHe3.html#a8415b61209b5ee62cc87895bee18b94c", null ],
    [ "operator=", "d0/d8b/classG4HETCHe3.html#a3c32b31a5ee20bba51c0ab1216879aa8", null ],
    [ "operator==", "d0/d8b/classG4HETCHe3.html#a620913adae37059c0b413915ddc32319", null ],
    [ "theHe3CoulombBarrier", "d0/d8b/classG4HETCHe3.html#a1772c57f5a67a4e3db99402ffa961f4c", null ]
];