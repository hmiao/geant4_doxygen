var xDataTOM__axes_8cc =
[
    [ "xDataTOM_axes_getInterpolation", "d0/d47/xDataTOM__axes_8cc.html#aa5d96bff6b3b2363b7f37c466edae707", null ],
    [ "xDataTOM_axes_getLabel", "d0/d47/xDataTOM__axes_8cc.html#aa94d9ae2fe65824b38c46c23e398b986", null ],
    [ "xDataTOM_axes_getUnit", "d0/d47/xDataTOM__axes_8cc.html#a1888e8331670c2693423bce1e1d3af7c", null ],
    [ "xDataTOM_axes_initialize", "d0/d47/xDataTOM__axes_8cc.html#aa50d4969537ffe30273ecc7cb3ba35b4", null ],
    [ "xDataTOM_axes_release", "d0/d47/xDataTOM__axes_8cc.html#af7ae7174b614a3d81fa234e716021e0c", null ],
    [ "xDataTOM_axis_frameToString", "d0/d47/xDataTOM__axes_8cc.html#af9bdb0b7ecba7485b09dd591a0a3835d", null ],
    [ "xDataTOM_axis_initialize", "d0/d47/xDataTOM__axes_8cc.html#a870fd846fb0f0d9a5de7dc346cf07ca9", null ],
    [ "xDataTOM_axis_new", "d0/d47/xDataTOM__axes_8cc.html#a54d6661cf87c01d72f2a66095107c74b", null ],
    [ "xDataTOM_axis_release", "d0/d47/xDataTOM__axes_8cc.html#af32007cd8ec969a95e7fed9871e087a9", null ],
    [ "xDataTOM_axis_stringToFrame", "d0/d47/xDataTOM__axes_8cc.html#a928c2aa9edc23e251a04f2605e85329b", null ],
    [ "xDataTOM_subAxes_getLabel", "d0/d47/xDataTOM__axes_8cc.html#a937d5a9bbb0557ddab92768e086c058c", null ],
    [ "xDataTOM_subAxes_getUnit", "d0/d47/xDataTOM__axes_8cc.html#ac01c4a2c72bad221b96d5cc0549f5784", null ],
    [ "xDataTOM_subAxes_initialize", "d0/d47/xDataTOM__axes_8cc.html#ae54e9e4ce71dc47d97ab7d69cf22f02e", null ],
    [ "xDataTOM_subAxes_release", "d0/d47/xDataTOM__axes_8cc.html#a10f296737a2ce3383868a81f21b09bd8", null ],
    [ "xDataTOM_frame_centerOfMassString", "d0/d47/xDataTOM__axes_8cc.html#ab15aedc96b6f0984f31fa4836d502132", null ],
    [ "xDataTOM_frame_invalidString", "d0/d47/xDataTOM__axes_8cc.html#a08dc774c3c3aaeaa0fe3a0da6dc1d369", null ],
    [ "xDataTOM_frame_labString", "d0/d47/xDataTOM__axes_8cc.html#a4f9c71d0eb76d9014c8b5660e3215c58", null ]
];