var classG4NuElNucleusCcModel =
[
    [ "G4NuElNucleusCcModel", "d0/d7c/classG4NuElNucleusCcModel.html#a0c215eca6196cd004a2515c8ce32c7a1", null ],
    [ "~G4NuElNucleusCcModel", "d0/d7c/classG4NuElNucleusCcModel.html#a9f6ca169d5b35394a55b2a17d4f11945", null ],
    [ "ApplyYourself", "d0/d7c/classG4NuElNucleusCcModel.html#a63909abef003fc25495b8f00a0f4c60b", null ],
    [ "GetMinNuElEnergy", "d0/d7c/classG4NuElNucleusCcModel.html#a8d009f4eb6ed27ea599888830c655722", null ],
    [ "InitialiseModel", "d0/d7c/classG4NuElNucleusCcModel.html#a5b885be4c75613b306c20bff50ce9dd1", null ],
    [ "IsApplicable", "d0/d7c/classG4NuElNucleusCcModel.html#a8a8de4b0040b028aac09b22cb2078b11", null ],
    [ "ModelDescription", "d0/d7c/classG4NuElNucleusCcModel.html#a6570e0cb1b3b6cb6f3baaafb7aa7464e", null ],
    [ "SampleLVkr", "d0/d7c/classG4NuElNucleusCcModel.html#a8c5bb492ec0a433fe7a9e489ec91a31c", null ],
    [ "ThresholdEnergy", "d0/d7c/classG4NuElNucleusCcModel.html#a138bcd606c7a8721c5a2deb7923b9f66", null ],
    [ "fData", "d0/d7c/classG4NuElNucleusCcModel.html#adfc666d72fe6be683281db5e822a5900", null ],
    [ "fMaster", "d0/d7c/classG4NuElNucleusCcModel.html#ad978fcb99ccac7d656e4e5fb24996be2", null ],
    [ "fMel", "d0/d7c/classG4NuElNucleusCcModel.html#a8e24bc20d6b77f24060abfabb1922838", null ],
    [ "theElectron", "d0/d7c/classG4NuElNucleusCcModel.html#ab89a0c59aed3e9913a3d5e777ec5cf3e", null ]
];