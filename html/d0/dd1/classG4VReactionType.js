var classG4VReactionType =
[
    [ "G4VReactionType", "d0/dd1/classG4VReactionType.html#a32a89668b9845b8720a3bed25934940c", null ],
    [ "~G4VReactionType", "d0/dd1/classG4VReactionType.html#a4aaaa97441036b2edb657772b3604cf1", null ],
    [ "GeminateRecombinationProbability", "d0/dd1/classG4VReactionType.html#a9159bdd6756d0cc17664ad44bc1a5afa", null ],
    [ "GetTimeToEncounter", "d0/dd1/classG4VReactionType.html#a5de9df819da76139f2f8c874f82087b9", null ]
];