var classGFlashEnergySpot =
[
    [ "GFlashEnergySpot", "d0/d7a/classGFlashEnergySpot.html#a153500d8fe90354cfba19fa1b7a2c482", null ],
    [ "GFlashEnergySpot", "d0/d7a/classGFlashEnergySpot.html#abb60b829eaea110bfa8032a3f757afac", null ],
    [ "~GFlashEnergySpot", "d0/d7a/classGFlashEnergySpot.html#ae1265862887df3331d5c42e153ec9311", null ],
    [ "GetEnergy", "d0/d7a/classGFlashEnergySpot.html#a803f0bbd5796a2f825e9957cebab2115", null ],
    [ "GetPosition", "d0/d7a/classGFlashEnergySpot.html#a485294c8c50ee3e10d6157a1fbe1fb6b", null ],
    [ "SetEnergy", "d0/d7a/classGFlashEnergySpot.html#aab9d99fe6ba70b6dfe3d38e0347e8e6a", null ],
    [ "SetPosition", "d0/d7a/classGFlashEnergySpot.html#a3d766f51cf258b2e29960f164527ec70", null ],
    [ "Energy", "d0/d7a/classGFlashEnergySpot.html#a91ca59d4427efdf6f23b72f2fb709387", null ],
    [ "Point", "d0/d7a/classGFlashEnergySpot.html#adad82806e41031ca377cb6ff14f5647f", null ]
];