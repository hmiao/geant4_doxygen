var classG4Trd =
[
    [ "G4Trd", "d0/d50/classG4Trd.html#a1900f7638339967873ad052cbb0d715d", null ],
    [ "~G4Trd", "d0/d50/classG4Trd.html#abc9a06bbfcc2d2e237e330a60da90a0c", null ],
    [ "G4Trd", "d0/d50/classG4Trd.html#af46a27247301e2d156be0e48c7fd46b7", null ],
    [ "G4Trd", "d0/d50/classG4Trd.html#a8ba0d71231f1c185d83de0feb48d4144", null ],
    [ "ApproxSurfaceNormal", "d0/d50/classG4Trd.html#a23bb64b3a35c0456e85242480b8fc0e3", null ],
    [ "BoundingLimits", "d0/d50/classG4Trd.html#aea7599b86dc2b9907fbe02007ffd1d50", null ],
    [ "CalculateExtent", "d0/d50/classG4Trd.html#a090dbc32db16589ae1aa83ea4ae6fd69", null ],
    [ "CheckParameters", "d0/d50/classG4Trd.html#a03742b2e838504b358b4fdcf422b8fc6", null ],
    [ "Clone", "d0/d50/classG4Trd.html#ab1826f6e13e3987b4e7bf9a6a6c0a723", null ],
    [ "ComputeDimensions", "d0/d50/classG4Trd.html#a6928197acb12f7d874320fc5e53b163c", null ],
    [ "CreatePolyhedron", "d0/d50/classG4Trd.html#ad3e7efd1ede0918b1b5beb7fb962d025", null ],
    [ "DescribeYourselfTo", "d0/d50/classG4Trd.html#a412aceae644b7672dbd792b6f5ec8114", null ],
    [ "DistanceToIn", "d0/d50/classG4Trd.html#a8556b0cf082bc891a14b2b96dc72aa14", null ],
    [ "DistanceToIn", "d0/d50/classG4Trd.html#a2a31f3bd00509c3d5b6837f50feffa13", null ],
    [ "DistanceToOut", "d0/d50/classG4Trd.html#aaf1e5c639d84b3ff28b26293ba88cb11", null ],
    [ "DistanceToOut", "d0/d50/classG4Trd.html#acc8d7662de71b4da0bd7d59d22ba5800", null ],
    [ "GetCubicVolume", "d0/d50/classG4Trd.html#af2845605ddfd331acc43ef2374105e0b", null ],
    [ "GetEntityType", "d0/d50/classG4Trd.html#a90df05e38ee1dca70d5f7b17372469c8", null ],
    [ "GetPointOnSurface", "d0/d50/classG4Trd.html#a77f1f2510aeb7e222b10c56a59d2f0ea", null ],
    [ "GetSurfaceArea", "d0/d50/classG4Trd.html#ac72e9b792feb2775de3b123f08d26f9e", null ],
    [ "GetXHalfLength1", "d0/d50/classG4Trd.html#a8f61099fefbac3e548a3ac63bfe0ac19", null ],
    [ "GetXHalfLength2", "d0/d50/classG4Trd.html#a08668fb24d52baeb8217032070253e6a", null ],
    [ "GetYHalfLength1", "d0/d50/classG4Trd.html#aa81ddb12ab74ebfe1b7c22d1dd4d6f02", null ],
    [ "GetYHalfLength2", "d0/d50/classG4Trd.html#a5a39efee991de3df69b3b559ef6399ad", null ],
    [ "GetZHalfLength", "d0/d50/classG4Trd.html#afd230e143024c85bd0419cd9c861364c", null ],
    [ "Inside", "d0/d50/classG4Trd.html#a1233ccce44b03aacbd53b65fc0d7aee2", null ],
    [ "MakePlanes", "d0/d50/classG4Trd.html#affb312546128edbe240878f0e343a1d6", null ],
    [ "operator=", "d0/d50/classG4Trd.html#a55bfa600ac2b15781f3807bd10e6a873", null ],
    [ "SetAllParameters", "d0/d50/classG4Trd.html#a0403ae4ed1e76b552a6ed3529213c805", null ],
    [ "SetXHalfLength1", "d0/d50/classG4Trd.html#a447fc97925da425a0a5f126bbe1578e0", null ],
    [ "SetXHalfLength2", "d0/d50/classG4Trd.html#a670fb0e56c0beb832704a6f1b3da9372", null ],
    [ "SetYHalfLength1", "d0/d50/classG4Trd.html#a4527d074f9e424d676c730d73a9f624f", null ],
    [ "SetYHalfLength2", "d0/d50/classG4Trd.html#a0081b9fe768ab6d109d4d727a8cf2594", null ],
    [ "SetZHalfLength", "d0/d50/classG4Trd.html#ae3732dd598beb20093900f9cad9a1e37", null ],
    [ "StreamInfo", "d0/d50/classG4Trd.html#a69848e8128b09b483ed44c68bbca8ed2", null ],
    [ "SurfaceNormal", "d0/d50/classG4Trd.html#aa450c4ede6f0488489eec87dca6f0d6a", null ],
    [ "a", "d0/d50/classG4Trd.html#a6b0059564c91ec22baa223081823223a", null ],
    [ "b", "d0/d50/classG4Trd.html#a2234bb74cdabc35865ae4f4ae9ee2a46", null ],
    [ "c", "d0/d50/classG4Trd.html#a9269cd1019e9010b839b07b751d9c796", null ],
    [ "d", "d0/d50/classG4Trd.html#a954c504f9848a7966240ffada487518a", null ],
    [ "fDx1", "d0/d50/classG4Trd.html#a423a7a136338276c491a7beb94f4a409", null ],
    [ "fDx2", "d0/d50/classG4Trd.html#ae780e3eb3409910fe429b52653b11644", null ],
    [ "fDy1", "d0/d50/classG4Trd.html#a36c4e3e9466afcb7d00ec9ff8966aa03", null ],
    [ "fDy2", "d0/d50/classG4Trd.html#a9287e05e3cf4db54feffa548bf43115b", null ],
    [ "fDz", "d0/d50/classG4Trd.html#a60396984fe262dfb3cdab682b2850c14", null ],
    [ "fHx", "d0/d50/classG4Trd.html#a86ef4991fad62cf51d00b51422d32c70", null ],
    [ "fHy", "d0/d50/classG4Trd.html#a95c6c8d8e2810d14e43b558b0e0708a3", null ],
    [ "fPlanes", "d0/d50/classG4Trd.html#ae5a6814934ff023100af1c604c39da0a", null ],
    [ "halfCarTolerance", "d0/d50/classG4Trd.html#a58a5a65f6606f40aa24c360b2d9574fb", null ]
];