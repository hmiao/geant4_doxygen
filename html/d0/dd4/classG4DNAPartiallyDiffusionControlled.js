var classG4DNAPartiallyDiffusionControlled =
[
    [ "G4DNAPartiallyDiffusionControlled", "d0/dd4/classG4DNAPartiallyDiffusionControlled.html#a908f57335083f9bf428c4264e0b9fea5", null ],
    [ "~G4DNAPartiallyDiffusionControlled", "d0/dd4/classG4DNAPartiallyDiffusionControlled.html#a8327d4d14f1fcaa76df8a9df1a92b3f7", null ],
    [ "G4DNAPartiallyDiffusionControlled", "d0/dd4/classG4DNAPartiallyDiffusionControlled.html#abc3ac06df2635396246f79e04a5c973f", null ],
    [ "GeminateRecombinationProbability", "d0/dd4/classG4DNAPartiallyDiffusionControlled.html#af1c8b5878e8fbabde3eee69a5cc11852", null ],
    [ "GetDiffusionCoefficient", "d0/dd4/classG4DNAPartiallyDiffusionControlled.html#ae9232acaad2baa0c413498b2e8beb963", null ],
    [ "GetTimeToEncounter", "d0/dd4/classG4DNAPartiallyDiffusionControlled.html#a3d4cfc631c581958c5a24a8eff930e52", null ],
    [ "operator=", "d0/dd4/classG4DNAPartiallyDiffusionControlled.html#a6f4c641e68b87e306cf7820078cd7d6c", null ]
];