var classG4DeuteronPHPBuilder =
[
    [ "G4DeuteronPHPBuilder", "d0/d98/classG4DeuteronPHPBuilder.html#a33e991354f2b26ba8dd4faaa02e5e08d", null ],
    [ "~G4DeuteronPHPBuilder", "d0/d98/classG4DeuteronPHPBuilder.html#ae9e2d9bddccd1f6f6fe64d8fc75b344c", null ],
    [ "Build", "d0/d98/classG4DeuteronPHPBuilder.html#a670d79f79dda5c99a01320c9b5600df6", null ],
    [ "Build", "d0/d98/classG4DeuteronPHPBuilder.html#a9c6625814c319208f9a6cd03fceccbd8", null ],
    [ "Build", "d0/d98/classG4DeuteronPHPBuilder.html#aee0ca08def164f335da6823001e1f561", null ],
    [ "Build", "d0/d98/classG4DeuteronPHPBuilder.html#abe046cd9f98fa1ebc62b4cbe2ca432a9", null ],
    [ "SetMaxEnergy", "d0/d98/classG4DeuteronPHPBuilder.html#ac40e520cbaf2f3573e74ad6953788f69", null ],
    [ "SetMinEnergy", "d0/d98/classG4DeuteronPHPBuilder.html#a0e9c25a066810cc729a56fb36d50a063", null ],
    [ "theMax", "d0/d98/classG4DeuteronPHPBuilder.html#a372fbbfd891ef1ceb4d8c1e907fadb39", null ],
    [ "theMin", "d0/d98/classG4DeuteronPHPBuilder.html#a05a4c74df169ad37ad607804568585b3", null ],
    [ "theParticlePHPModel", "d0/d98/classG4DeuteronPHPBuilder.html#a7fb21e31dff0da23633fa0390e3e4b4e", null ]
];