var classG4PreCompoundHe3 =
[
    [ "G4PreCompoundHe3", "d0/d1d/classG4PreCompoundHe3.html#a0b39496ee3e88ef0c9cf9cfda5e12057", null ],
    [ "~G4PreCompoundHe3", "d0/d1d/classG4PreCompoundHe3.html#a8f43f103724274901b3398bf491b5c46", null ],
    [ "G4PreCompoundHe3", "d0/d1d/classG4PreCompoundHe3.html#a1a5cb6f7a1acbe34a2c51f00d3145825", null ],
    [ "CoalescenceFactor", "d0/d1d/classG4PreCompoundHe3.html#adf96d40dc1c90618311b08a8375d6ed2", null ],
    [ "FactorialFactor", "d0/d1d/classG4PreCompoundHe3.html#a180e26ab09815014ebe1d8000d32b0a5", null ],
    [ "GetAlpha", "d0/d1d/classG4PreCompoundHe3.html#a77a1f434f8a5aef858b1876329a4fdd6", null ],
    [ "GetRj", "d0/d1d/classG4PreCompoundHe3.html#a99009a9e01614ca817bfcf2fffe8c76d", null ],
    [ "operator!=", "d0/d1d/classG4PreCompoundHe3.html#a81fe7c4ab1df0627664798a76a33006e", null ],
    [ "operator=", "d0/d1d/classG4PreCompoundHe3.html#acdb661dcd7e2212eed2a6ec4968c11cb", null ],
    [ "operator==", "d0/d1d/classG4PreCompoundHe3.html#a310eca28967b0b3015d202f4d4bc3866", null ],
    [ "theHe3CoulombBarrier", "d0/d1d/classG4PreCompoundHe3.html#aa4f91370d5a6305dd6f14e345849851b", null ]
];