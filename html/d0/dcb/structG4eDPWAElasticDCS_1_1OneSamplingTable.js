var structG4eDPWAElasticDCS_1_1OneSamplingTable =
[
    [ "OneSamplingTable", "d0/dcb/structG4eDPWAElasticDCS_1_1OneSamplingTable.html#a07b8fca95bf43f356cc5d44bd6998435", null ],
    [ "SetSize", "d0/dcb/structG4eDPWAElasticDCS_1_1OneSamplingTable.html#a9af6bb44e459e8e9a2a8234e55fa7992", null ],
    [ "fA", "d0/dcb/structG4eDPWAElasticDCS_1_1OneSamplingTable.html#a43259a8f6992f2ab272332e1c914bcfc", null ],
    [ "fB", "d0/dcb/structG4eDPWAElasticDCS_1_1OneSamplingTable.html#a3cd33353ce0abd273671e9f4e3214159", null ],
    [ "fCum", "d0/dcb/structG4eDPWAElasticDCS_1_1OneSamplingTable.html#aef827bdca6a10198a1a85d4ce6f817bc", null ],
    [ "fI", "d0/dcb/structG4eDPWAElasticDCS_1_1OneSamplingTable.html#a01c798d758efc4eaa880bb064f3c093c", null ],
    [ "fN", "d0/dcb/structG4eDPWAElasticDCS_1_1OneSamplingTable.html#a7800cec66723fed103d5c021240c2ca9", null ],
    [ "fScreenParA", "d0/dcb/structG4eDPWAElasticDCS_1_1OneSamplingTable.html#aca36f1f571eae18396fa4187ae3e2f5d", null ],
    [ "fW", "d0/dcb/structG4eDPWAElasticDCS_1_1OneSamplingTable.html#a23d37e8b8564d814395816e5a942875e", null ]
];