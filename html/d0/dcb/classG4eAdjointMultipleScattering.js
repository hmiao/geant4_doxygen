var classG4eAdjointMultipleScattering =
[
    [ "G4eAdjointMultipleScattering", "d0/dcb/classG4eAdjointMultipleScattering.html#a11585b624c4bf86677e7d39347bda0da", null ],
    [ "~G4eAdjointMultipleScattering", "d0/dcb/classG4eAdjointMultipleScattering.html#a7c949821f9c0309829097b2a65f3ca59", null ],
    [ "G4eAdjointMultipleScattering", "d0/dcb/classG4eAdjointMultipleScattering.html#a58ff9a4a9247a58b2929a54b4a511070", null ],
    [ "DumpInfo", "d0/dcb/classG4eAdjointMultipleScattering.html#a1424522feb04c3807b2ea6a2cd034848", null ],
    [ "InitialiseProcess", "d0/dcb/classG4eAdjointMultipleScattering.html#a60b65a44cfd07e9b1fccdb67ed3b834f", null ],
    [ "IsApplicable", "d0/dcb/classG4eAdjointMultipleScattering.html#a363a2efc4835716d573b0599d8a147c1", null ],
    [ "operator=", "d0/dcb/classG4eAdjointMultipleScattering.html#a38b565657628806521bbf4cbb3deef34", null ],
    [ "ProcessDescription", "d0/dcb/classG4eAdjointMultipleScattering.html#a6e92871784cf38d0b02e69e79628a708", null ],
    [ "StartTracking", "d0/dcb/classG4eAdjointMultipleScattering.html#a6bd0a0172c4f18054b80ee9b0f583af4", null ],
    [ "StreamProcessInfo", "d0/dcb/classG4eAdjointMultipleScattering.html#a04c0cd6123fe26fa045b1b59a5da0314", null ],
    [ "fIsInitialized", "d0/dcb/classG4eAdjointMultipleScattering.html#aca86a564da97317e9f6922b1e2e2f8af", null ]
];