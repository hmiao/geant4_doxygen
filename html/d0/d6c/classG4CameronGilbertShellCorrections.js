var classG4CameronGilbertShellCorrections =
[
    [ "G4CameronGilbertShellCorrections", "d0/d6c/classG4CameronGilbertShellCorrections.html#a1b7bdb9a286addd781c143b75ad96f1a", null ],
    [ "G4CameronGilbertShellCorrections", "d0/d6c/classG4CameronGilbertShellCorrections.html#abb14953cfada29f04527bf0791927dfe", null ],
    [ "GetShellCorrection", "d0/d6c/classG4CameronGilbertShellCorrections.html#abd0ff50fecceee613e1e13ca1c1a6a7c", null ],
    [ "operator=", "d0/d6c/classG4CameronGilbertShellCorrections.html#ab6ffc52cdd7ceb07e5fa73e4d324fa4c", null ],
    [ "ShellNTable", "d0/d6c/classG4CameronGilbertShellCorrections.html#a1a6fb0b78cb3ebb54a70d014a0544e07", null ],
    [ "ShellZTable", "d0/d6c/classG4CameronGilbertShellCorrections.html#a88826b0cc6e66617055dbef1624e96b4", null ]
];