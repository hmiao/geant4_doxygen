var classG4VEMDataSet =
[
    [ "G4VEMDataSet", "d0/d71/classG4VEMDataSet.html#aee92469581c79bd10b75dbb1f7eb7efe", null ],
    [ "~G4VEMDataSet", "d0/d71/classG4VEMDataSet.html#aba7f98af6ba0116a1b9b8e4db8ff795d", null ],
    [ "G4VEMDataSet", "d0/d71/classG4VEMDataSet.html#a7926cdc31a0f5eb8d6c2d432c90dc351", null ],
    [ "AddComponent", "d0/d71/classG4VEMDataSet.html#acd810dafa53f9f4d2f78ee146c9ff748", null ],
    [ "FindValue", "d0/d71/classG4VEMDataSet.html#ac4ac23e990269222e7b926e799009638", null ],
    [ "GetComponent", "d0/d71/classG4VEMDataSet.html#a5e92297a9d034e709bc225abed80736a", null ],
    [ "GetData", "d0/d71/classG4VEMDataSet.html#a80b8f9623c92588779890f76fcf6a6a1", null ],
    [ "GetEnergies", "d0/d71/classG4VEMDataSet.html#a9ecdb931151ee120cb15b6f741d0a2fb", null ],
    [ "GetLogData", "d0/d71/classG4VEMDataSet.html#a18cf3a9c768b329654c02df2aa77c118", null ],
    [ "GetLogEnergies", "d0/d71/classG4VEMDataSet.html#a2f083f7bb95ff2c4fdaca5e22ecdf8ca", null ],
    [ "LoadData", "d0/d71/classG4VEMDataSet.html#adbd04f68a1c45bae3b38b4b18cb19ce9", null ],
    [ "LoadNonLogData", "d0/d71/classG4VEMDataSet.html#a6b95fc6be3471892209fb82197405cc8", null ],
    [ "NumberOfComponents", "d0/d71/classG4VEMDataSet.html#ad89ab0d8aba797f91598c59f3ac4234c", null ],
    [ "operator=", "d0/d71/classG4VEMDataSet.html#a349434807ca364b173859a865bb5fd52", null ],
    [ "PrintData", "d0/d71/classG4VEMDataSet.html#afd1d8155652cb46265aeb7e9fa81fee3", null ],
    [ "RandomSelect", "d0/d71/classG4VEMDataSet.html#aa50acae7df3b634db0283d80b63788de", null ],
    [ "SaveData", "d0/d71/classG4VEMDataSet.html#a0cf06ce20e76fa4b0279ccbae1220a61", null ],
    [ "SetEnergiesData", "d0/d71/classG4VEMDataSet.html#a47c4b04c88bc52b3d42dac7049686e48", null ],
    [ "SetLogEnergiesData", "d0/d71/classG4VEMDataSet.html#ab04aab2d288bfeeefbf3b7c2e1aa5050", null ]
];