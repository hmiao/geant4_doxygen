var classG4GaussLegendreQ =
[
    [ "G4GaussLegendreQ", "d0/d31/classG4GaussLegendreQ.html#aa145d2d0390d4018bfc1a90ca1c56395", null ],
    [ "G4GaussLegendreQ", "d0/d31/classG4GaussLegendreQ.html#ac3cefd7b0212d26e0f201621f08c4217", null ],
    [ "G4GaussLegendreQ", "d0/d31/classG4GaussLegendreQ.html#aab5e8c8d004f28b925946e88d1fd90aa", null ],
    [ "AccurateIntegral", "d0/d31/classG4GaussLegendreQ.html#ae98a55f6f0e58ddedabadee339b6f6b1", null ],
    [ "Integral", "d0/d31/classG4GaussLegendreQ.html#a216b2c30a1861f42596ab0aed42baa7b", null ],
    [ "operator=", "d0/d31/classG4GaussLegendreQ.html#a794f28c9904e6f3c18ad012e7e7c38fb", null ],
    [ "QuickIntegral", "d0/d31/classG4GaussLegendreQ.html#a537a47153c4d8249fa53fc98cbde7ec1", null ]
];