var classG4UnitsCategory =
[
    [ "G4UnitsCategory", "d0/dc0/classG4UnitsCategory.html#a7efab08442840bb57b895b1f46622548", null ],
    [ "~G4UnitsCategory", "d0/dc0/classG4UnitsCategory.html#a956ac9f3e93f30847bbe8539cabfb41d", null ],
    [ "G4UnitsCategory", "d0/dc0/classG4UnitsCategory.html#af5a5062ebc2bb37ad380ddd37574d4a1", null ],
    [ "GetName", "d0/dc0/classG4UnitsCategory.html#a0bfa77f33d41e0341d5aac6cdc573e02", null ],
    [ "GetNameMxLen", "d0/dc0/classG4UnitsCategory.html#ac60bba4c33fbdba64cba16cfbc223a00", null ],
    [ "GetSymbMxLen", "d0/dc0/classG4UnitsCategory.html#ac86029d616d62a74443856a2c6fe1046", null ],
    [ "GetUnitsList", "d0/dc0/classG4UnitsCategory.html#a7a2d79d85f6d9232e757785de5817df1", null ],
    [ "operator!=", "d0/dc0/classG4UnitsCategory.html#a87d756a976bca37f9e67a4628135dc9f", null ],
    [ "operator=", "d0/dc0/classG4UnitsCategory.html#a373c33ca8a88d05f50fc0f8f889da1e5", null ],
    [ "operator==", "d0/dc0/classG4UnitsCategory.html#ad9c03dbe2f5ce81a204da81061b2c4c4", null ],
    [ "PrintCategory", "d0/dc0/classG4UnitsCategory.html#adee6842a257ce1a9b3d93947dc774445", null ],
    [ "UpdateNameMxLen", "d0/dc0/classG4UnitsCategory.html#ac2da551166c023148829f40244076ad0", null ],
    [ "UpdateSymbMxLen", "d0/dc0/classG4UnitsCategory.html#a7162fd34d03b41cde1e7ce42bf03e26a", null ],
    [ "Name", "d0/dc0/classG4UnitsCategory.html#a92f596be5df3ab83b059f04396fbd8a3", null ],
    [ "NameMxLen", "d0/dc0/classG4UnitsCategory.html#a2e59cb2e6b0d4d622eb34316935e5623", null ],
    [ "SymbMxLen", "d0/dc0/classG4UnitsCategory.html#abbb26a6edce7a51e40b5cfeab60f2683", null ],
    [ "UnitsList", "d0/dc0/classG4UnitsCategory.html#a4aa1722603f2a86d923fd3b482ae508d", null ]
];