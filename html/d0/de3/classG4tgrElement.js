var classG4tgrElement =
[
    [ "G4tgrElement", "d0/de3/classG4tgrElement.html#addec2ea44f9d92e6fc71890becbb09cd", null ],
    [ "~G4tgrElement", "d0/de3/classG4tgrElement.html#ad90b5b5bf2283594c2a07da15f5cd266", null ],
    [ "GetName", "d0/de3/classG4tgrElement.html#a74ed9f75bf3feb747dfc3107b4201d9a", null ],
    [ "GetSymbol", "d0/de3/classG4tgrElement.html#a5458c97e6e6d9d89eeb65f38fa008b50", null ],
    [ "GetType", "d0/de3/classG4tgrElement.html#a35ce1e44ed20f2dc68b817dcb41f00ad", null ],
    [ "theName", "d0/de3/classG4tgrElement.html#a8ae03de234325f5f6e63b58f6a232ea6", null ],
    [ "theSymbol", "d0/de3/classG4tgrElement.html#aa732a3a6e80a69da0c2be79cbcee3107", null ],
    [ "theType", "d0/de3/classG4tgrElement.html#acd0c0e7ad55d0e7c0f259b9e8d904cc9", null ]
];