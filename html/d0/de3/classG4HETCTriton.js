var classG4HETCTriton =
[
    [ "G4HETCTriton", "d0/de3/classG4HETCTriton.html#aa5ebcc0f56f9dd73c4d3f8361c34063a", null ],
    [ "~G4HETCTriton", "d0/de3/classG4HETCTriton.html#a34da1f65e8d7b0e0dea6fbff681b3478", null ],
    [ "G4HETCTriton", "d0/de3/classG4HETCTriton.html#ad964f7207e9bf407c4269a3a4688e409", null ],
    [ "GetAlpha", "d0/de3/classG4HETCTriton.html#ab11f57be5e5d2ea38fd70e1eda719867", null ],
    [ "GetBeta", "d0/de3/classG4HETCTriton.html#aaba256a31a3c35f9da20e346e7d929b2", null ],
    [ "GetSpinFactor", "d0/de3/classG4HETCTriton.html#a5b806332728e8927027b337c10ab2ccc", null ],
    [ "K", "d0/de3/classG4HETCTriton.html#af67038335e6be8072b1871f9d312b8c7", null ],
    [ "operator!=", "d0/de3/classG4HETCTriton.html#afea79e9d1c601a2bd3895b724b5078bc", null ],
    [ "operator=", "d0/de3/classG4HETCTriton.html#ab7ba143c2dd313133edb0caf197948ae", null ],
    [ "operator==", "d0/de3/classG4HETCTriton.html#a397de7c204580988c54ea17e1002380c", null ],
    [ "theTritonCoulombBarrier", "d0/de3/classG4HETCTriton.html#a64ae9ee4961347bdef43bdd72af15747", null ]
];