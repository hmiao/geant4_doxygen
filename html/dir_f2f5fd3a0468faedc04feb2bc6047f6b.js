var dir_f2f5fd3a0468faedc04feb2bc6047f6b =
[
    [ "G4CompositeDataSet.hh", "d1/d92/G4CompositeDataSet_8hh.html", "d1/d92/G4CompositeDataSet_8hh" ],
    [ "G4DataSet.hh", "d5/d8b/G4DataSet_8hh.html", "d5/d8b/G4DataSet_8hh" ],
    [ "G4hImpactIonisation.hh", "db/d26/G4hImpactIonisation_8hh.html", "db/d26/G4hImpactIonisation_8hh" ],
    [ "G4hRDEnergyLoss.hh", "d8/d87/G4hRDEnergyLoss_8hh.html", "d8/d87/G4hRDEnergyLoss_8hh" ],
    [ "G4IDataSet.hh", "dc/dc0/G4IDataSet_8hh.html", "dc/dc0/G4IDataSet_8hh" ],
    [ "G4IInterpolator.hh", "df/deb/G4IInterpolator_8hh.html", "df/deb/G4IInterpolator_8hh" ],
    [ "G4LinInterpolator.hh", "d2/dc2/G4LinInterpolator_8hh.html", "d2/dc2/G4LinInterpolator_8hh" ],
    [ "G4LogLogInterpolator.hh", "dd/d2e/G4LogLogInterpolator_8hh.html", "dd/d2e/G4LogLogInterpolator_8hh" ],
    [ "G4PixeCrossSectionHandler.hh", "da/d94/G4PixeCrossSectionHandler_8hh.html", "da/d94/G4PixeCrossSectionHandler_8hh" ],
    [ "G4PixeShellDataSet.hh", "d9/dce/G4PixeShellDataSet_8hh.html", "d9/dce/G4PixeShellDataSet_8hh" ]
];