var classG4LowECapture =
[
    [ "G4LowECapture", "de/dcf/classG4LowECapture.html#a8c5d69460feb66e1ac6633595d5ad4d2", null ],
    [ "~G4LowECapture", "de/dcf/classG4LowECapture.html#a677f10d7a93f515d8e67eb8c74b2c089", null ],
    [ "G4LowECapture", "de/dcf/classG4LowECapture.html#af4f459a376359475d1084edc075ff077", null ],
    [ "AddRegion", "de/dcf/classG4LowECapture.html#afb6cfae2cea56a1acc625bcabf717ff9", null ],
    [ "BuildPhysicsTable", "de/dcf/classG4LowECapture.html#a578129b66f22fcf0e350f99406f00edb", null ],
    [ "GetMeanFreePath", "de/dcf/classG4LowECapture.html#aa1624e6971cd8e30d2860347c76e3efe", null ],
    [ "IsApplicable", "de/dcf/classG4LowECapture.html#ac49bfb8437afa33cbceea4b8a3566167", null ],
    [ "operator=", "de/dcf/classG4LowECapture.html#aeb76468e2c4132e815e8cfca3658ed29", null ],
    [ "PostStepDoIt", "de/dcf/classG4LowECapture.html#a7d00a3481d050ea424a647377d98d61f", null ],
    [ "PostStepGetPhysicalInteractionLength", "de/dcf/classG4LowECapture.html#ac8d6fd646b94f105490a827997ede83f", null ],
    [ "SetKinEnergyLimit", "de/dcf/classG4LowECapture.html#a08a7bc85263b081d319ef00a0620c4b6", null ],
    [ "isIon", "de/dcf/classG4LowECapture.html#abf31eb70eeee02b301ec8959518c8962", null ],
    [ "kinEnergyThreshold", "de/dcf/classG4LowECapture.html#ac84c57de1eaa3b043a6bb7d091395fe8", null ],
    [ "nRegions", "de/dcf/classG4LowECapture.html#a77b8181cee04efeb00e006c3a4ce951c", null ],
    [ "region", "de/dcf/classG4LowECapture.html#ace92dc07ef74139652a6d2e0b0d2d631", null ],
    [ "regionName", "de/dcf/classG4LowECapture.html#a49a167b29c9e20eb7fc8b2a65f15e6c5", null ]
];