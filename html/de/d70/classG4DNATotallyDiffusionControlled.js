var classG4DNATotallyDiffusionControlled =
[
    [ "G4DNATotallyDiffusionControlled", "de/d70/classG4DNATotallyDiffusionControlled.html#ad036ae7ba4de8a24e1c2ed105aa3add3", null ],
    [ "~G4DNATotallyDiffusionControlled", "de/d70/classG4DNATotallyDiffusionControlled.html#a6c3c2a6bc9bd6d69aa2bbc742c74ceed", null ],
    [ "G4DNATotallyDiffusionControlled", "de/d70/classG4DNATotallyDiffusionControlled.html#ac60a1b9be0118392cd3fe62c65060fca", null ],
    [ "GeminateRecombinationProbability", "de/d70/classG4DNATotallyDiffusionControlled.html#a931ca19ca81bdd2e8464b469696ceb9f", null ],
    [ "GetDiffusionCoefficient", "de/d70/classG4DNATotallyDiffusionControlled.html#a86c5b7a39a99b5d9252e150aa977001e", null ],
    [ "GetTimeToEncounter", "de/d70/classG4DNATotallyDiffusionControlled.html#afb98135e070e7cf7b2aebba3f09094ee", null ],
    [ "operator=", "de/d70/classG4DNATotallyDiffusionControlled.html#a2bafa1acb50550051b07097eb8e116de", null ]
];