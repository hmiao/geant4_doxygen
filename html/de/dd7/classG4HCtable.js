var classG4HCtable =
[
    [ "G4HCtable", "de/dd7/classG4HCtable.html#afcd34be57a751b3a656b603492c4a751", null ],
    [ "~G4HCtable", "de/dd7/classG4HCtable.html#ae6b2193683a98acf4278f11768cf25b4", null ],
    [ "entries", "de/dd7/classG4HCtable.html#ae9181dc08fa0e2d6dde83cae664ae5cb", null ],
    [ "GetCollectionID", "de/dd7/classG4HCtable.html#a9e1575bd41d15b8369e7ac3623af08b3", null ],
    [ "GetCollectionID", "de/dd7/classG4HCtable.html#a5321908a015710c060e9a04393e19922", null ],
    [ "GetHCname", "de/dd7/classG4HCtable.html#aa835fac966cc5171853e79fff8f46970", null ],
    [ "GetSDname", "de/dd7/classG4HCtable.html#a4d7f6e3024b089a1c84a23f2ddd7decf", null ],
    [ "Registor", "de/dd7/classG4HCtable.html#a3690a802698825fdb174050cd9831120", null ],
    [ "HClist", "de/dd7/classG4HCtable.html#a408d7ba0d2a1710709ce330a2005e4db", null ],
    [ "SDlist", "de/dd7/classG4HCtable.html#a3102b7667a4d96a3a72e6ffe90d39ee5", null ]
];