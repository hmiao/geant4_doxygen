var classG4PolarizedBremsstrahlung =
[
    [ "G4PolarizedBremsstrahlung", "de/dd7/classG4PolarizedBremsstrahlung.html#a780a2f7749c59f046a761049d435b2a9", null ],
    [ "~G4PolarizedBremsstrahlung", "de/dd7/classG4PolarizedBremsstrahlung.html#a7f3a411a473c2034e7676d7f0104104d", null ],
    [ "G4PolarizedBremsstrahlung", "de/dd7/classG4PolarizedBremsstrahlung.html#a9630e497a33d1752349814a74cc1d45a", null ],
    [ "DumpInfo", "de/dd7/classG4PolarizedBremsstrahlung.html#a96314295abeffb78d32397f373d6d4d7", null ],
    [ "InitialiseEnergyLossProcess", "de/dd7/classG4PolarizedBremsstrahlung.html#a57a55ce20bcdf4365ed47c6f49974a9f", null ],
    [ "operator=", "de/dd7/classG4PolarizedBremsstrahlung.html#a16bc197923dd177adaa7f3f67a6533e8", null ],
    [ "ProcessDescription", "de/dd7/classG4PolarizedBremsstrahlung.html#a33dc736e51981fc9c097eb547ac5e940", null ],
    [ "isInitialised", "de/dd7/classG4PolarizedBremsstrahlung.html#a8ae33c191c84803b9d94c2d5bb999460", null ]
];