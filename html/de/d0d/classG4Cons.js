var classG4Cons =
[
    [ "ENorm", "de/d0d/classG4Cons.html#ae5e5c5a870c5a02cdc5e97782bafac67", [
      [ "kNRMin", "de/d0d/classG4Cons.html#ae5e5c5a870c5a02cdc5e97782bafac67ab250a29bfe5a6f27c0a49cdd5ceafaff", null ],
      [ "kNRMax", "de/d0d/classG4Cons.html#ae5e5c5a870c5a02cdc5e97782bafac67aca681faff7350a6293347f3f906e85c7", null ],
      [ "kNSPhi", "de/d0d/classG4Cons.html#ae5e5c5a870c5a02cdc5e97782bafac67abb1bc484a6e51ac95da09331d763e267", null ],
      [ "kNEPhi", "de/d0d/classG4Cons.html#ae5e5c5a870c5a02cdc5e97782bafac67a9567f6d9368c6c3a840710b75866d6f1", null ],
      [ "kNZ", "de/d0d/classG4Cons.html#ae5e5c5a870c5a02cdc5e97782bafac67af8805915589d1356633396a6f7d4d389", null ]
    ] ],
    [ "ESide", "de/d0d/classG4Cons.html#a7a72ae790a619174bd17fc9780c0a660", [
      [ "kNull", "de/d0d/classG4Cons.html#a7a72ae790a619174bd17fc9780c0a660a90b51a3bd468248c92857d20c7b8f628", null ],
      [ "kRMin", "de/d0d/classG4Cons.html#a7a72ae790a619174bd17fc9780c0a660a9ac727ee4de5b9d05bfe81d174cd2199", null ],
      [ "kRMax", "de/d0d/classG4Cons.html#a7a72ae790a619174bd17fc9780c0a660a550e6552f9886f09cc173c4fcb2810e1", null ],
      [ "kSPhi", "de/d0d/classG4Cons.html#a7a72ae790a619174bd17fc9780c0a660a18e8d50096cf78cce58b582975936562", null ],
      [ "kEPhi", "de/d0d/classG4Cons.html#a7a72ae790a619174bd17fc9780c0a660a77a6d6c7cd474124c6c36de509322fb7", null ],
      [ "kPZ", "de/d0d/classG4Cons.html#a7a72ae790a619174bd17fc9780c0a660aebbe1e7809fef3821a07432ccd81f3dd", null ],
      [ "kMZ", "de/d0d/classG4Cons.html#a7a72ae790a619174bd17fc9780c0a660a118c7006d3b3cd5e02a2194de27d0d77", null ]
    ] ],
    [ "G4Cons", "de/d0d/classG4Cons.html#abdbf3bf359bf07f8ed9639b4730f8524", null ],
    [ "~G4Cons", "de/d0d/classG4Cons.html#a9e8ac65768840f0efe6f895030f57735", null ],
    [ "G4Cons", "de/d0d/classG4Cons.html#a06cc793c18a7412c191cd6da2fd40342", null ],
    [ "G4Cons", "de/d0d/classG4Cons.html#a7f6c869784437e5cae77c8f7278bdd38", null ],
    [ "ApproxSurfaceNormal", "de/d0d/classG4Cons.html#ab10eb9c4e5ed1b8e5a49e2f739236cdd", null ],
    [ "BoundingLimits", "de/d0d/classG4Cons.html#a98058505a969d8f961525de67a4cb468", null ],
    [ "CalculateExtent", "de/d0d/classG4Cons.html#a0bcf267a7ae26f7b2670267450f29ad6", null ],
    [ "CheckDPhiAngle", "de/d0d/classG4Cons.html#a5781d2a33e3754bc967f83fd7913db36", null ],
    [ "CheckPhiAngles", "de/d0d/classG4Cons.html#a4df754a5caec95dd974403c073b30460", null ],
    [ "CheckSPhiAngle", "de/d0d/classG4Cons.html#adc44300db5c6ef1c3b20accbc80aa7fd", null ],
    [ "Clone", "de/d0d/classG4Cons.html#a03e0277833a25b2cee032e7720a2fca7", null ],
    [ "ComputeDimensions", "de/d0d/classG4Cons.html#a319199d53ffe784b05dfa96fef29f906", null ],
    [ "CreatePolyhedron", "de/d0d/classG4Cons.html#aade9866745d214246fa8913432fbffd2", null ],
    [ "DescribeYourselfTo", "de/d0d/classG4Cons.html#a42ab3b9af1d24c7614be1ef5e07d0414", null ],
    [ "DistanceToIn", "de/d0d/classG4Cons.html#a368dc7c35cd40bd9ddee95ee80137157", null ],
    [ "DistanceToIn", "de/d0d/classG4Cons.html#aeb07f5945008a8f6ff3913cd83015aba", null ],
    [ "DistanceToOut", "de/d0d/classG4Cons.html#ab9f24f1e992a34b3c063fc7e3f07c55f", null ],
    [ "DistanceToOut", "de/d0d/classG4Cons.html#af2e3ece7a0f006d9b8095b0956b52930", null ],
    [ "GetCosEndPhi", "de/d0d/classG4Cons.html#aaa03b32a2525afb72f6c84421a140aaf", null ],
    [ "GetCosStartPhi", "de/d0d/classG4Cons.html#a15aeb7e01e79ca18a439dda75b7d263e", null ],
    [ "GetCubicVolume", "de/d0d/classG4Cons.html#abcd5a108e7f06305349aaa405816a4b3", null ],
    [ "GetDeltaPhiAngle", "de/d0d/classG4Cons.html#a3985aa938eefcbf54ed0f70450382c9a", null ],
    [ "GetEntityType", "de/d0d/classG4Cons.html#a754df27eb2d345ae45dbb79059629b28", null ],
    [ "GetInnerRadiusMinusZ", "de/d0d/classG4Cons.html#a45e371b2ecf20a857b301b4cc107fc0a", null ],
    [ "GetInnerRadiusPlusZ", "de/d0d/classG4Cons.html#a6fe873d55bca5b066cbf4f11584f8439", null ],
    [ "GetOuterRadiusMinusZ", "de/d0d/classG4Cons.html#abf0087fc8b5b5e1789d2466e6c6e9a32", null ],
    [ "GetOuterRadiusPlusZ", "de/d0d/classG4Cons.html#a0cb6a8144f71cbf7fc607df8747b0c15", null ],
    [ "GetPointOnSurface", "de/d0d/classG4Cons.html#ad30bed6b1b395b9606efe3a1f66ddf36", null ],
    [ "GetSinEndPhi", "de/d0d/classG4Cons.html#ac1057a003146f16c8b6a5b7d6dbc2341", null ],
    [ "GetSinStartPhi", "de/d0d/classG4Cons.html#ac6d5c31ac28ea3ea93aacc688a2fb252", null ],
    [ "GetStartPhiAngle", "de/d0d/classG4Cons.html#a2abe7dc8fa506f6d048c234c6c8d7ab7", null ],
    [ "GetSurfaceArea", "de/d0d/classG4Cons.html#a77e788ea6950abb5afecb5905cd55f71", null ],
    [ "GetZHalfLength", "de/d0d/classG4Cons.html#ad4d366c22fe8ee894d8ffe0e8d5c2c5e", null ],
    [ "Initialize", "de/d0d/classG4Cons.html#a69d76b8999ba79301bdafbafebb9a1ab", null ],
    [ "InitializeTrigonometry", "de/d0d/classG4Cons.html#a74dc6432cb276938e970d419fb38f182", null ],
    [ "Inside", "de/d0d/classG4Cons.html#ad272ff82ac0b39b4007f9cbe8366ee33", null ],
    [ "operator=", "de/d0d/classG4Cons.html#ac1ec4854d3633a18d5c13ae42bc6af51", null ],
    [ "SetDeltaPhiAngle", "de/d0d/classG4Cons.html#a97df918efbaef227388b10fe39482e92", null ],
    [ "SetInnerRadiusMinusZ", "de/d0d/classG4Cons.html#aa824caa16ec6be1ec4c7abb371e08ad3", null ],
    [ "SetInnerRadiusPlusZ", "de/d0d/classG4Cons.html#a28614c982177f80f36ffe068e724f0a4", null ],
    [ "SetOuterRadiusMinusZ", "de/d0d/classG4Cons.html#a44ef02290df88e65fb3048f4b5c3cf91", null ],
    [ "SetOuterRadiusPlusZ", "de/d0d/classG4Cons.html#a4b7fe148c6e2d567e417a976ee4efee5", null ],
    [ "SetStartPhiAngle", "de/d0d/classG4Cons.html#a3ef89f1738950407da490aec3098f9aa", null ],
    [ "SetZHalfLength", "de/d0d/classG4Cons.html#a29156f141f069a914e5d89713a9c64a2", null ],
    [ "StreamInfo", "de/d0d/classG4Cons.html#af6014a8c37d4598750a6b1c4890e0ba7", null ],
    [ "SurfaceNormal", "de/d0d/classG4Cons.html#abec5bb0c5ecef898bac0fa54b53c4de3", null ],
    [ "cosCPhi", "de/d0d/classG4Cons.html#a92ae82ffc6464b6457668e8db1ed9716", null ],
    [ "cosEPhi", "de/d0d/classG4Cons.html#afa9efdecdde6a23ffccc04afdc6a9010", null ],
    [ "cosHDPhi", "de/d0d/classG4Cons.html#a6400fa5b4c61522f25680c510512908d", null ],
    [ "cosHDPhiIT", "de/d0d/classG4Cons.html#a4ca7c730bed929bb6bc985b5f1bd8458", null ],
    [ "cosHDPhiOT", "de/d0d/classG4Cons.html#a6f7538aa85444f6a2e67e56a888ecb7b", null ],
    [ "cosSPhi", "de/d0d/classG4Cons.html#a2647a049666e6e619c4b9de49c8e3bb3", null ],
    [ "fDPhi", "de/d0d/classG4Cons.html#a2b3e790cfc8b73f5d8065dbae7ab52ed", null ],
    [ "fDz", "de/d0d/classG4Cons.html#a331bb0d7c95492d04fb5ba2bbbbcd4fb", null ],
    [ "fPhiFullCone", "de/d0d/classG4Cons.html#aa55f1b1502db1996eb1b436cdbaf77b6", null ],
    [ "fRmax1", "de/d0d/classG4Cons.html#af6321aaa177a99808f90bf541ae001a0", null ],
    [ "fRmax2", "de/d0d/classG4Cons.html#aa32aa973335d78ac0b52aa83244518ec", null ],
    [ "fRmin1", "de/d0d/classG4Cons.html#ae4660a5e3003dbd1556806304be1ddcc", null ],
    [ "fRmin2", "de/d0d/classG4Cons.html#af617cf82f9bd5795811df4a0bd9f302d", null ],
    [ "fSPhi", "de/d0d/classG4Cons.html#a1dcbb1317a476c6f2a539ddee089e93a", null ],
    [ "halfAngTolerance", "de/d0d/classG4Cons.html#ab13a653c4c6d66ee51cdec57824c619a", null ],
    [ "halfCarTolerance", "de/d0d/classG4Cons.html#af5c4fb5ffaeb44872e4ed410498577c4", null ],
    [ "halfRadTolerance", "de/d0d/classG4Cons.html#a40e5cd452146d3abdd94caac8fa6129a", null ],
    [ "kAngTolerance", "de/d0d/classG4Cons.html#a3fa245b6a01293566ae9628d85ad7e82", null ],
    [ "kRadTolerance", "de/d0d/classG4Cons.html#ad6fc1e9c743222cf771d5e5a1763391b", null ],
    [ "sinCPhi", "de/d0d/classG4Cons.html#add719d2c9c849ad6f68f9c5059ed7a6c", null ],
    [ "sinEPhi", "de/d0d/classG4Cons.html#a7dc09d40e6c05f9df620f9419e5b5f20", null ],
    [ "sinSPhi", "de/d0d/classG4Cons.html#ae28aa1af81847e0bc71ea3263288e6db", null ]
];