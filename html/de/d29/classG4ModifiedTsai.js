var classG4ModifiedTsai =
[
    [ "G4ModifiedTsai", "de/d29/classG4ModifiedTsai.html#a43d35da9b89a985d12f535095d827d11", null ],
    [ "~G4ModifiedTsai", "de/d29/classG4ModifiedTsai.html#aaebf4ab53f60beee8ea16f3cf140daec", null ],
    [ "G4ModifiedTsai", "de/d29/classG4ModifiedTsai.html#aef353002c091e17749d72038922d5bf8", null ],
    [ "operator=", "de/d29/classG4ModifiedTsai.html#a2f127881b49d42a475af2ed1103b6a3f", null ],
    [ "PrintGeneratorInformation", "de/d29/classG4ModifiedTsai.html#a82ba06a548b21bb21928b3af47d1a5f1", null ],
    [ "SampleCosTheta", "de/d29/classG4ModifiedTsai.html#a409918b82e98cea75314463ac85ba078", null ],
    [ "SampleDirection", "de/d29/classG4ModifiedTsai.html#a477c7e23d5c6b99ad4badad403df98d1", null ],
    [ "SamplePairDirections", "de/d29/classG4ModifiedTsai.html#ac60a4436d703d37d063f591d84f31126", null ]
];