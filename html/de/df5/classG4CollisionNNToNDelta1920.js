var classG4CollisionNNToNDelta1920 =
[
    [ "G4CollisionNNToNDelta1920", "de/df5/classG4CollisionNNToNDelta1920.html#a57b435d570b833031dbfee92960dbcc3", null ],
    [ "~G4CollisionNNToNDelta1920", "de/df5/classG4CollisionNNToNDelta1920.html#ad17f1cb79fc11aa350c4fc64e756e733", null ],
    [ "G4CollisionNNToNDelta1920", "de/df5/classG4CollisionNNToNDelta1920.html#a070dbe14117f01a3e3d350c79a66270d", null ],
    [ "GetComponents", "de/df5/classG4CollisionNNToNDelta1920.html#a689abba574115de0f8deb271234e1108", null ],
    [ "GetListOfColliders", "de/df5/classG4CollisionNNToNDelta1920.html#a32b3708460ace8b840beba14e77e307b", null ],
    [ "GetName", "de/df5/classG4CollisionNNToNDelta1920.html#ab5d14950b159e8571d36e914de604df6", null ],
    [ "operator=", "de/df5/classG4CollisionNNToNDelta1920.html#a0e4a2877b97d54bd7f41ef1659d42270", null ],
    [ "components", "de/df5/classG4CollisionNNToNDelta1920.html#a45f87c13be6ab1c8eb9d280c10faa679", null ]
];