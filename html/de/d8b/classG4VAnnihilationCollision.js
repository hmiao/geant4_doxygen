var classG4VAnnihilationCollision =
[
    [ "G4VAnnihilationCollision", "de/d8b/classG4VAnnihilationCollision.html#a883f2147cc8f1c102e2032ada38d5e35", null ],
    [ "~G4VAnnihilationCollision", "de/d8b/classG4VAnnihilationCollision.html#a8dd96f875e3863bff81d2dff23fb6cb0", null ],
    [ "BrWigInt0", "de/d8b/classG4VAnnihilationCollision.html#a50de6abecd3f9c657993a96311d939d8", null ],
    [ "BrWigInt1", "de/d8b/classG4VAnnihilationCollision.html#a9a325ad4802480936bf6b5b87aa61827", null ],
    [ "BrWigInv", "de/d8b/classG4VAnnihilationCollision.html#a52bd2fb83b570d5bf2c746a1fd37db84", null ],
    [ "FinalState", "de/d8b/classG4VAnnihilationCollision.html#aa94c37d2b51904829f3beeac2d731f30", null ],
    [ "GetAngularDistribution", "de/d8b/classG4VAnnihilationCollision.html#a6cf94bcc83138b56d966f85521ef566e", null ],
    [ "GetOutgoingParticle", "de/d8b/classG4VAnnihilationCollision.html#a98325cf275537d120c887cdfa5097168", null ],
    [ "operator!=", "de/d8b/classG4VAnnihilationCollision.html#a9289179b102e7b6fa47035ae3b88f2b3", null ],
    [ "operator==", "de/d8b/classG4VAnnihilationCollision.html#ae4d6848d76877f915a5f986429b5772e", null ],
    [ "SampleResonanceMass", "de/d8b/classG4VAnnihilationCollision.html#addd3aa92ded68d281184b177bc848031", null ],
    [ "theAngularDistribution", "de/d8b/classG4VAnnihilationCollision.html#ad7e46d51bd1c5a1853bd7bd626684c5a", null ]
];