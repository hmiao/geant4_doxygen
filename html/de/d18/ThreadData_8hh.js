var ThreadData_8hh =
[
    [ "PTL::tbb::task_group", "dc/df7/classPTL_1_1tbb_1_1task__group.html", "dc/df7/classPTL_1_1tbb_1_1task__group" ],
    [ "PTL::tbb::global_control", "df/d97/classPTL_1_1tbb_1_1global__control.html", "df/d97/classPTL_1_1tbb_1_1global__control" ],
    [ "PTL::tbb::task_arena", "d9/d90/classPTL_1_1tbb_1_1task__arena.html", "d9/d90/classPTL_1_1tbb_1_1task__arena" ],
    [ "PTL::ThreadData", "da/daa/classPTL_1_1ThreadData.html", "da/daa/classPTL_1_1ThreadData" ],
    [ "tbb_global_control_t", "de/d18/ThreadData_8hh.html#ad5723578659bb0ce5e156c977d07aa2d", null ],
    [ "tbb_task_arena_t", "de/d18/ThreadData_8hh.html#adf2a183f47965e970fad42cb04fdbdf7", null ],
    [ "tbb_task_group_t", "de/d18/ThreadData_8hh.html#ac9a86c48440bdd4c9ffc11477935263a", null ]
];