var classG4ANuElNucleusNcModel =
[
    [ "G4ANuElNucleusNcModel", "de/d4c/classG4ANuElNucleusNcModel.html#a206047507c7d283999998d0db48752d3", null ],
    [ "~G4ANuElNucleusNcModel", "de/d4c/classG4ANuElNucleusNcModel.html#a31ea45ef1033cbb69e0ad3eb83e54934", null ],
    [ "ApplyYourself", "de/d4c/classG4ANuElNucleusNcModel.html#ab0ad10712b6e91f42b39a6196ce7137a", null ],
    [ "GetMinNuElEnergy", "de/d4c/classG4ANuElNucleusNcModel.html#af103a289b126ce8673ea4315c0772336", null ],
    [ "InitialiseModel", "de/d4c/classG4ANuElNucleusNcModel.html#ab2308200fde082e528c7e8e9e7a7c55d", null ],
    [ "IsApplicable", "de/d4c/classG4ANuElNucleusNcModel.html#a9f21092a5928b974bbbb45223f24f072", null ],
    [ "ModelDescription", "de/d4c/classG4ANuElNucleusNcModel.html#a862b56fac879fb4191a3844107779cf1", null ],
    [ "SampleLVkr", "de/d4c/classG4ANuElNucleusNcModel.html#a215d8322db120f8738ca5fcea817c1c2", null ],
    [ "ThresholdEnergy", "de/d4c/classG4ANuElNucleusNcModel.html#a8cea10d692d193eb1109815a802d1131", null ],
    [ "fData", "de/d4c/classG4ANuElNucleusNcModel.html#a60ff4f1ade429d01543af5ef5b01f16c", null ],
    [ "fMaster", "de/d4c/classG4ANuElNucleusNcModel.html#ace8f68c52e3457e4a42338c08794de90", null ],
    [ "fMnumu", "de/d4c/classG4ANuElNucleusNcModel.html#abe9d65c9090b8a74009ebb658557c92c", null ],
    [ "theNuE", "de/d4c/classG4ANuElNucleusNcModel.html#a371f7b720be742b76678ce04663891a8", null ]
];