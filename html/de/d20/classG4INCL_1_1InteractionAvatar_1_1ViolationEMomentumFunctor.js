var classG4INCL_1_1InteractionAvatar_1_1ViolationEMomentumFunctor =
[
    [ "ViolationEMomentumFunctor", "de/d20/classG4INCL_1_1InteractionAvatar_1_1ViolationEMomentumFunctor.html#a5446beb37e9ae3cf03f72966a7843a38", null ],
    [ "~ViolationEMomentumFunctor", "de/d20/classG4INCL_1_1InteractionAvatar_1_1ViolationEMomentumFunctor.html#aaa1b7c43360c8e61b188601885cdf51b", null ],
    [ "cleanUp", "de/d20/classG4INCL_1_1InteractionAvatar_1_1ViolationEMomentumFunctor.html#a568ebd769116c061a67a00ce27d32f6c", null ],
    [ "operator()", "de/d20/classG4INCL_1_1InteractionAvatar_1_1ViolationEMomentumFunctor.html#ab24eb7989aacc54bbfecaef4393f795f", null ],
    [ "scaleParticleMomenta", "de/d20/classG4INCL_1_1InteractionAvatar_1_1ViolationEMomentumFunctor.html#aa9526db70a2208a19adf3cee11098d36", null ],
    [ "boostVector", "de/d20/classG4INCL_1_1InteractionAvatar_1_1ViolationEMomentumFunctor.html#a287afdf22621b8714878ae55c47344f7", null ],
    [ "finalParticles", "de/d20/classG4INCL_1_1InteractionAvatar_1_1ViolationEMomentumFunctor.html#a8e4bb6bb6caf90ddc4eb584e19afeb9b", null ],
    [ "initialEnergy", "de/d20/classG4INCL_1_1InteractionAvatar_1_1ViolationEMomentumFunctor.html#afd43d157c18ef56808e6afc96e29afc6", null ],
    [ "particleMomenta", "de/d20/classG4INCL_1_1InteractionAvatar_1_1ViolationEMomentumFunctor.html#ae26b03b6be0b0ada4772a97f8d0e7e09", null ],
    [ "shouldUseLocalEnergy", "de/d20/classG4INCL_1_1InteractionAvatar_1_1ViolationEMomentumFunctor.html#aec05fdac768fd31122a33709264f773a", null ],
    [ "theNucleus", "de/d20/classG4INCL_1_1InteractionAvatar_1_1ViolationEMomentumFunctor.html#af5ac0991b17f0ec55d03b45d07f53268", null ]
];