var structG4Backtrace_1_1actions =
[
    [ "id_entry_t", "de/d7e/structG4Backtrace_1_1actions.html#adc4c776fde8d078441535728ed832ea5", null ],
    [ "id_list_t", "de/d7e/structG4Backtrace_1_1actions.html#aa8e080eb1f6f11e1eb9a578eabdc2bf3", null ],
    [ "current", "de/d7e/structG4Backtrace_1_1actions.html#ac6466fa71b30530c0f11b7c88dc97334", null ],
    [ "exit_actions", "de/d7e/structG4Backtrace_1_1actions.html#a821380bd16c4535fcdd18e505299c63f", null ],
    [ "identifiers", "de/d7e/structG4Backtrace_1_1actions.html#a827f4c681658eea964ab91eff1f9b063", null ],
    [ "is_active", "de/d7e/structG4Backtrace_1_1actions.html#a849a0f44761df26f9ada133b719a7a7d", null ],
    [ "previous", "de/d7e/structG4Backtrace_1_1actions.html#a3246b68d80000278d64672a187bf3635", null ]
];