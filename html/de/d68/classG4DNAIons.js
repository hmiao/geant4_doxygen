var classG4DNAIons =
[
    [ "G4DNAIons", "de/d68/classG4DNAIons.html#a502088934f79a817e3bc3990663c0393", null ],
    [ "G4DNAIons", "de/d68/classG4DNAIons.html#a7a3fe874df02eedd2c298cc91848a658", null ],
    [ "~G4DNAIons", "de/d68/classG4DNAIons.html#ad0134a9a3e33bb4a207bb161aa745ebf", null ],
    [ "GetExcitationEnergy", "de/d68/classG4DNAIons.html#ae5cc7d706a5684243b4d328ea9f9ede8", null ],
    [ "GetIsomerLevel", "de/d68/classG4DNAIons.html#a20b014074a4e002661b8e2bc0ce1cd81", null ],
    [ "Ions", "de/d68/classG4DNAIons.html#a67894b7ae19b331f8448803297f66ebe", null ],
    [ "IonsDefinition", "de/d68/classG4DNAIons.html#a8025be438903b4ecd8d3709115d0f1fc", null ],
    [ "theExcitationEnergy", "de/d68/classG4DNAIons.html#a9a5dfec4f6a541c7aa61c8db139a3725", null ],
    [ "theIsomerLevel", "de/d68/classG4DNAIons.html#a508d5ef9dd35f8c2e86f8e363e7b7657", null ]
];