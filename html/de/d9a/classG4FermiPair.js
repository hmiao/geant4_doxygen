var classG4FermiPair =
[
    [ "G4FermiPair", "de/d9a/classG4FermiPair.html#a6558d74ddf9234e2e6c67e162512c7a2", null ],
    [ "G4FermiPair", "de/d9a/classG4FermiPair.html#add9899223e19be1c4df722fc9ad527d8", null ],
    [ "GetA", "de/d9a/classG4FermiPair.html#ab78b8b145411acd7c36ac586ed8e95a5", null ],
    [ "GetDynamicMinMass", "de/d9a/classG4FermiPair.html#ae66131e9c1f8d9407c472ed38ff57f3f", null ],
    [ "GetExcitationEnergy", "de/d9a/classG4FermiPair.html#a9ff4fe7c976e5db19e40a767994fbd1d", null ],
    [ "GetFragment1", "de/d9a/classG4FermiPair.html#aeaafe6c0255696f8c4ae29f83c3e4a9f", null ],
    [ "GetFragment2", "de/d9a/classG4FermiPair.html#a46320fb6e0165b936bc9ce99fb321911", null ],
    [ "GetMass", "de/d9a/classG4FermiPair.html#a93808ae6e24b49f5955538d4230d4d26", null ],
    [ "GetTotalEnergy", "de/d9a/classG4FermiPair.html#ac4869b4f2f1903bb71c8abf40c37c8cd", null ],
    [ "GetZ", "de/d9a/classG4FermiPair.html#a9ef0e1f38ef19b2319203b457e69a819", null ],
    [ "operator!=", "de/d9a/classG4FermiPair.html#ac880755dd3a64742a6b8062de1bd5202", null ],
    [ "operator=", "de/d9a/classG4FermiPair.html#abc94e2bb1347b6625ebabf184492cb0f", null ],
    [ "operator==", "de/d9a/classG4FermiPair.html#aa2f2f360d782e4736a69f4f3e701d63f", null ],
    [ "excitEnergy", "de/d9a/classG4FermiPair.html#a38246bdabd78a082cbe6b355193f04f9", null ],
    [ "fragment1", "de/d9a/classG4FermiPair.html#a26e176d6544397e6c52310213accba2f", null ],
    [ "fragment2", "de/d9a/classG4FermiPair.html#a576d2b102dd21fb933bdfee4ae5928d6", null ],
    [ "mass", "de/d9a/classG4FermiPair.html#aae7e9236f75b9ef2fb3a2409b4f65c21", null ],
    [ "totalA", "de/d9a/classG4FermiPair.html#adfd6fe10abe3d1faed1b3e959d08828b", null ],
    [ "totalZ", "de/d9a/classG4FermiPair.html#a4f6fcf7f6c3355dffbbe970263ee1116", null ]
];