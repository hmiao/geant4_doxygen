var classG4ParameterisationBoxX =
[
    [ "G4ParameterisationBoxX", "de/d34/classG4ParameterisationBoxX.html#a024f78fa50f9fc03aaf68f19a34b5a11", null ],
    [ "~G4ParameterisationBoxX", "de/d34/classG4ParameterisationBoxX.html#a731944da967a4a88c8561d6a56fe7390", null ],
    [ "ComputeDimensions", "de/d34/classG4ParameterisationBoxX.html#a53cdf15bf071e0beda661f46cd2f8e3f", null ],
    [ "ComputeDimensions", "de/d34/classG4ParameterisationBoxX.html#ad27898ebd88752da9483856f243b649a", null ],
    [ "ComputeDimensions", "de/d34/classG4ParameterisationBoxX.html#a50fdf2c83a93b961bcf48e8be8402edf", null ],
    [ "ComputeDimensions", "de/d34/classG4ParameterisationBoxX.html#a8d40800d3e98354e314dfb73feba3504", null ],
    [ "ComputeDimensions", "de/d34/classG4ParameterisationBoxX.html#a4909c4033f5165aa6623e82a0f814947", null ],
    [ "ComputeDimensions", "de/d34/classG4ParameterisationBoxX.html#af93b637a03d3db41e4c28c0f23513b13", null ],
    [ "ComputeDimensions", "de/d34/classG4ParameterisationBoxX.html#a9598ef34d0b390ffe970d45d64639e2c", null ],
    [ "ComputeDimensions", "de/d34/classG4ParameterisationBoxX.html#a20924bd7d1229594fb05bc9ea170c9a7", null ],
    [ "ComputeDimensions", "de/d34/classG4ParameterisationBoxX.html#a4faaab0bae43162e99633d1fad0380f0", null ],
    [ "ComputeDimensions", "de/d34/classG4ParameterisationBoxX.html#a247dc750f04676339e111d305e81f62f", null ],
    [ "ComputeDimensions", "de/d34/classG4ParameterisationBoxX.html#a5ef533a3d7388af2113e4201f2dbb793", null ],
    [ "ComputeDimensions", "de/d34/classG4ParameterisationBoxX.html#affded37a28a8aa1712e275cbdcc6fff7", null ],
    [ "ComputeDimensions", "de/d34/classG4ParameterisationBoxX.html#a878c78eda4c9fb270a699211ac7cdb66", null ],
    [ "ComputeTransformation", "de/d34/classG4ParameterisationBoxX.html#ad93702e321c4d1b517075be9587c83e3", null ],
    [ "GetMaxParameter", "de/d34/classG4ParameterisationBoxX.html#aba1d365330cf33d5bd18c52b9153bf2a", null ]
];