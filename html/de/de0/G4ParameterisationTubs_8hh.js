var G4ParameterisationTubs_8hh =
[
    [ "G4VParameterisationTubs", "d0/da1/classG4VParameterisationTubs.html", "d0/da1/classG4VParameterisationTubs" ],
    [ "G4ParameterisationTubsRho", "db/d15/classG4ParameterisationTubsRho.html", "db/d15/classG4ParameterisationTubsRho" ],
    [ "G4ParameterisationTubsPhi", "d6/dd3/classG4ParameterisationTubsPhi.html", "d6/dd3/classG4ParameterisationTubsPhi" ],
    [ "G4ParameterisationTubsZ", "d7/d39/classG4ParameterisationTubsZ.html", "d7/d39/classG4ParameterisationTubsZ" ]
];