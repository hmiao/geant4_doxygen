var classG4QTabWidget =
[
    [ "G4QTabWidget", "de/d45/classG4QTabWidget.html#a13a3671701c710e4b6decf3bbe62b308", null ],
    [ "G4QTabWidget", "de/d45/classG4QTabWidget.html#aa6f22fde3305a5a681730027779d23d3", null ],
    [ "isTabSelected", "de/d45/classG4QTabWidget.html#af717edc92f07f4623940591405e82ae4", null ],
    [ "paintEvent", "de/d45/classG4QTabWidget.html#ab437e76498978950fc39c54a1d04041f", null ],
    [ "setLastTabCreated", "de/d45/classG4QTabWidget.html#a7e707532ea15b3887a55b0fa0e3e232e", null ],
    [ "setPreferredSize", "de/d45/classG4QTabWidget.html#a7cbf0ec13705f2e6d4127db41f4c42dc", null ],
    [ "setTabSelected", "de/d45/classG4QTabWidget.html#a4c223cc96029d71865d53e80e62d11b8", null ],
    [ "sizeHint", "de/d45/classG4QTabWidget.html#a339f2d06519aa651f4aa8f783c2aacb6", null ],
    [ "fLastCreated", "de/d45/classG4QTabWidget.html#ae5597f1edc01ba99b7759b60e1b4d01e", null ],
    [ "fPreferedSizeX", "de/d45/classG4QTabWidget.html#a8a351591f6ecc2e7d5bc36155d4bf0cb", null ],
    [ "fPreferedSizeY", "de/d45/classG4QTabWidget.html#ace2aadf07be0cf9dfe2915d3f12e9abc", null ],
    [ "fTabSelected", "de/d45/classG4QTabWidget.html#a02c8d50b30f33d9c882910b85f73d94f", null ]
];