var classG4LivermoreBremsstrahlungModel =
[
    [ "G4LivermoreBremsstrahlungModel", "de/d59/classG4LivermoreBremsstrahlungModel.html#a34a9035dc3a775462f9d023a94b39b76", null ],
    [ "~G4LivermoreBremsstrahlungModel", "de/d59/classG4LivermoreBremsstrahlungModel.html#a41ba4e6402fa7aabf7f90184b7e40ff1", null ],
    [ "G4LivermoreBremsstrahlungModel", "de/d59/classG4LivermoreBremsstrahlungModel.html#a54eb31cd6c4c65494b5a0f444f74a607", null ],
    [ "ComputeDXSectionPerAtom", "de/d59/classG4LivermoreBremsstrahlungModel.html#aae483130ea301ffcb0a979e022c742da", null ],
    [ "DirectoryPath", "de/d59/classG4LivermoreBremsstrahlungModel.html#a682e4a5edba7457d1139a779c46f2394", null ],
    [ "Initialise", "de/d59/classG4LivermoreBremsstrahlungModel.html#a8a5a5b8a9dc5c958af5791fd4a78b1e0", null ],
    [ "InitialiseForElement", "de/d59/classG4LivermoreBremsstrahlungModel.html#a8a8c8930a9c49eebe5454ee3c97913a1", null ],
    [ "operator=", "de/d59/classG4LivermoreBremsstrahlungModel.html#ac0f50211ab2cbf6dcc616bd2868c73b4", null ],
    [ "ReadData", "de/d59/classG4LivermoreBremsstrahlungModel.html#aaa7aff1a8d53ced7de302d5c7699505e", null ],
    [ "SampleSecondaries", "de/d59/classG4LivermoreBremsstrahlungModel.html#a6e6fa93da174c586db00e1b38b265e4d", null ],
    [ "SetBicubicInterpolationFlag", "de/d59/classG4LivermoreBremsstrahlungModel.html#af18a39f1dd2716f735a4929af5e1766b", null ],
    [ "dataSB", "de/d59/classG4LivermoreBremsstrahlungModel.html#a2e3af5342762050a14ca83cd8e721b31", null ],
    [ "expnumlim", "de/d59/classG4LivermoreBremsstrahlungModel.html#abb373fba5958592977805eff3afba2a9", null ],
    [ "idx", "de/d59/classG4LivermoreBremsstrahlungModel.html#ad1eadc285e3b20fd95d907df7e69aff6", null ],
    [ "idy", "de/d59/classG4LivermoreBremsstrahlungModel.html#a5414f0eda1cefe806f32d2f62f95e256", null ],
    [ "nwarn", "de/d59/classG4LivermoreBremsstrahlungModel.html#ae388c3f990897206394499393e33e0ab", null ],
    [ "useBicubicInterpolation", "de/d59/classG4LivermoreBremsstrahlungModel.html#afed81f88ad79b958f5f9856fc116c4c1", null ],
    [ "ylimit", "de/d59/classG4LivermoreBremsstrahlungModel.html#a64f121ef5113e69b5ccf26c78fb1f3c0", null ]
];