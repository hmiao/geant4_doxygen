var classG4VTrackingManager =
[
    [ "~G4VTrackingManager", "de/dac/classG4VTrackingManager.html#a823da783f6c87a06d825742afec529c3", null ],
    [ "BuildPhysicsTable", "de/dac/classG4VTrackingManager.html#a641c0f41937d894a2452a4b164e4c117", null ],
    [ "FlushEvent", "de/dac/classG4VTrackingManager.html#af4d2f90ea2db091fb1f2f3442104db25", null ],
    [ "HandOverOneTrack", "de/dac/classG4VTrackingManager.html#a84a92661d2df7661b5588737fa3ddffe", null ],
    [ "PreparePhysicsTable", "de/dac/classG4VTrackingManager.html#ad5bf790f888d784b37397aaaf45a4b51", null ]
];