var classG4XAqmTotal =
[
    [ "G4XAqmTotal", "de/dc4/classG4XAqmTotal.html#aef56f4c2bee27488e7a57937314f1a06", null ],
    [ "~G4XAqmTotal", "de/dc4/classG4XAqmTotal.html#a597d852e24c4546b2599e2ed5896fad0", null ],
    [ "G4XAqmTotal", "de/dc4/classG4XAqmTotal.html#a71bd78b07233121fde270725ceca5f50", null ],
    [ "CrossSection", "de/dc4/classG4XAqmTotal.html#a4b477db7667a76aae049e47df8e4c0ea", null ],
    [ "GetComponents", "de/dc4/classG4XAqmTotal.html#a65b7ea5750c7f3711887fb762f22c545", null ],
    [ "IsValid", "de/dc4/classG4XAqmTotal.html#a8428b7d6266b51f247f5c4220f335e6d", null ],
    [ "Name", "de/dc4/classG4XAqmTotal.html#a12745deefe2ab6b36e7601c28351b222", null ],
    [ "operator!=", "de/dc4/classG4XAqmTotal.html#a89d117065c1b6ad9cc6f2a4cc65ef725", null ],
    [ "operator=", "de/dc4/classG4XAqmTotal.html#a64cbc14472c513bc6798ff114d19cef3", null ],
    [ "operator==", "de/dc4/classG4XAqmTotal.html#a7386556fba48febf2831c7e50b6e0a05", null ],
    [ "_highLimit", "de/dc4/classG4XAqmTotal.html#a7b88c8fc5bc8f81cadd9f0b1ded95cf1", null ],
    [ "_lowLimit", "de/dc4/classG4XAqmTotal.html#a2d704ccfd72d7563eacb3fa0c77d1eb6", null ]
];