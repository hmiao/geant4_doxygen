var G4VRFileManager_8hh =
[
    [ "G4VRFileManager", "d4/db4/classG4VRFileManager.html", "d4/db4/classG4VRFileManager" ],
    [ "G4VRFileManager::GetHnRFileManager< tools::histo::h1d >", "de/dc6/G4VRFileManager_8hh.html#a1910619732eb5e2e7e326b0a19ff19de", null ],
    [ "G4VRFileManager::GetHnRFileManager< tools::histo::h2d >", "de/dc6/G4VRFileManager_8hh.html#a1b5ed39f749fd0320f035b89e5addbc7", null ],
    [ "G4VRFileManager::GetHnRFileManager< tools::histo::h3d >", "de/dc6/G4VRFileManager_8hh.html#ad178be8894b743857f1eca513ad795bd", null ],
    [ "G4VRFileManager::GetHnRFileManager< tools::histo::p1d >", "de/dc6/G4VRFileManager_8hh.html#a17af420a7fda486831bce3805f0b5064", null ],
    [ "G4VRFileManager::GetHnRFileManager< tools::histo::p2d >", "de/dc6/G4VRFileManager_8hh.html#ad746b9082c90ca50a402ce3451bc1add", null ]
];