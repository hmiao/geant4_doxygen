var classG4PolarizedIonisationMollerXS =
[
    [ "G4PolarizedIonisationMollerXS", "de/d04/classG4PolarizedIonisationMollerXS.html#aa6a4d92644794242824d1f26b3b169d4", null ],
    [ "~G4PolarizedIonisationMollerXS", "de/d04/classG4PolarizedIonisationMollerXS.html#a698e814326492dda447df5cd197ae946", null ],
    [ "G4PolarizedIonisationMollerXS", "de/d04/classG4PolarizedIonisationMollerXS.html#a06c9795161451e04e97bc383b7be9162", null ],
    [ "GetPol2", "de/d04/classG4PolarizedIonisationMollerXS.html#a62be365baf6c94f0fec7d6b36037e78a", null ],
    [ "GetPol3", "de/d04/classG4PolarizedIonisationMollerXS.html#a94c6a750b02d235a251b0111c7ec221f", null ],
    [ "Initialize", "de/d04/classG4PolarizedIonisationMollerXS.html#ab3e8a405cee7e574e4da38dad5917c14", null ],
    [ "operator=", "de/d04/classG4PolarizedIonisationMollerXS.html#af46b33b79de8e45557d1b0691d552838", null ],
    [ "TotalXSection", "de/d04/classG4PolarizedIonisationMollerXS.html#ab8e324064a008d799303e0d90a3b11e7", null ],
    [ "XSection", "de/d04/classG4PolarizedIonisationMollerXS.html#ab692350d181f72ed6aeb565b1e2a33d4", null ],
    [ "fPhi0", "de/d04/classG4PolarizedIonisationMollerXS.html#a96604eb540d7765b807103d30b288474", null ],
    [ "fPhi2", "de/d04/classG4PolarizedIonisationMollerXS.html#ab9a6deafc6b9c2b6d83ebc63c40a9c48", null ],
    [ "fPhi3", "de/d04/classG4PolarizedIonisationMollerXS.html#a0121ce6b97400e20f07379352c670793", null ]
];