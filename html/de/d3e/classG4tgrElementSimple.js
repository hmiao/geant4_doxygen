var classG4tgrElementSimple =
[
    [ "G4tgrElementSimple", "de/d3e/classG4tgrElementSimple.html#a433da4b63520963ea5a9643ff138da26", null ],
    [ "~G4tgrElementSimple", "de/d3e/classG4tgrElementSimple.html#a92a5da8bf0beaf813478e342f1ec815d", null ],
    [ "G4tgrElementSimple", "de/d3e/classG4tgrElementSimple.html#a07fee956d7b9b1902ba9d50ba149197b", null ],
    [ "GetA", "de/d3e/classG4tgrElementSimple.html#a4b11d6ddf94af3aeab1c5008e81e2e7b", null ],
    [ "GetZ", "de/d3e/classG4tgrElementSimple.html#a015a88a6c1614ae2b06fe5b05413f4d4", null ],
    [ "operator<<", "de/d3e/classG4tgrElementSimple.html#a182538d9b7b9e4e3d3652abd74cbd44b", null ],
    [ "theA", "de/d3e/classG4tgrElementSimple.html#a6d867183701c8caa00d0292d9174e6c9", null ],
    [ "theZ", "de/d3e/classG4tgrElementSimple.html#a9918ab16bf528d4a18e9d8db2658d813", null ]
];