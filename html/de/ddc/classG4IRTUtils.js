var classG4IRTUtils =
[
    [ "G4IRTUtils", "de/ddc/classG4IRTUtils.html#a262bdcb9c63bd8b26dd6637982f1443b", null ],
    [ "~G4IRTUtils", "de/ddc/classG4IRTUtils.html#ad7bc9cca41d8853a5436e0991ee030b1", null ],
    [ "EffectiveDistance", "de/ddc/classG4IRTUtils.html#a0a7ae2ca01f443138337a9c02f33146b", null ],
    [ "GetDNADistanceCutOff", "de/ddc/classG4IRTUtils.html#a5cf65fa381f066100ab075a1d79a90f2", null ],
    [ "GetKact", "de/ddc/classG4IRTUtils.html#a2b220847936083bb7e1a7a5aaea973b0", null ],
    [ "GetRCutOff", "de/ddc/classG4IRTUtils.html#ab4f05842283177e2e9b4c8cfb86f7ae3", null ],
    [ "GetRCutOff", "de/ddc/classG4IRTUtils.html#a23e045e701e3a39ebc276a97121875dd", null ]
];