var classG4LivermoreComptonModel =
[
    [ "G4LivermoreComptonModel", "de/dcc/classG4LivermoreComptonModel.html#a43c609760bb050a36fed7929bb8e27f0", null ],
    [ "~G4LivermoreComptonModel", "de/dcc/classG4LivermoreComptonModel.html#ab02be45ee60bed080ccdb4ac90d9d982", null ],
    [ "G4LivermoreComptonModel", "de/dcc/classG4LivermoreComptonModel.html#ab67eff5f7e213e50e750f23ce4f5c6a3", null ],
    [ "ComputeCrossSectionPerAtom", "de/dcc/classG4LivermoreComptonModel.html#a48c4750d6c43a0d0c176da435ee97288", null ],
    [ "ComputeScatteringFunction", "de/dcc/classG4LivermoreComptonModel.html#a912442e0742302b62ce2b68589205723", null ],
    [ "Initialise", "de/dcc/classG4LivermoreComptonModel.html#aac8aeee6690b45cb1c938498cd321b7a", null ],
    [ "InitialiseForElement", "de/dcc/classG4LivermoreComptonModel.html#ab8ba827f75e06d5af55ffa1c5ca9fa85", null ],
    [ "InitialiseLocal", "de/dcc/classG4LivermoreComptonModel.html#a5c6d95d3a28deaee24749ea4f1b4a6fd", null ],
    [ "operator=", "de/dcc/classG4LivermoreComptonModel.html#aa2c37a1045ce368820e3c66fbee2ff7a", null ],
    [ "ReadData", "de/dcc/classG4LivermoreComptonModel.html#a103a89d778b5aa71007e596ac26dffbe", null ],
    [ "SampleSecondaries", "de/dcc/classG4LivermoreComptonModel.html#a7ec5e398371cc7129e3439549058c729", null ],
    [ "data", "de/dcc/classG4LivermoreComptonModel.html#a4d8a05f4914296a2da21e27ba1668cfe", null ],
    [ "fAtomDeexcitation", "de/dcc/classG4LivermoreComptonModel.html#a64058026608975b6fadcae24e5dab106", null ],
    [ "fParticleChange", "de/dcc/classG4LivermoreComptonModel.html#a6861a552f807fd9b094bbb4899a70c91", null ],
    [ "isInitialised", "de/dcc/classG4LivermoreComptonModel.html#a353d181d9988ce7b613dc567aeaf5066", null ],
    [ "maxZ", "de/dcc/classG4LivermoreComptonModel.html#af5ce316be674403a4666856ab0d995fb", null ],
    [ "profileData", "de/dcc/classG4LivermoreComptonModel.html#ad7c754f29e8d53a9bc7b3ee2e6beb80c", null ],
    [ "ScatFuncFitParam", "de/dcc/classG4LivermoreComptonModel.html#a7d57b16ce51ef7b203899c6a00cf2854", null ],
    [ "shellData", "de/dcc/classG4LivermoreComptonModel.html#ac04f8f80e06e5c5eeaedb746ddea1df1", null ],
    [ "verboseLevel", "de/dcc/classG4LivermoreComptonModel.html#a940301f9f8e5c2b077d437ad041222a6", null ]
];