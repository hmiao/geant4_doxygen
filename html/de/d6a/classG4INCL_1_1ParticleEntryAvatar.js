var classG4INCL_1_1ParticleEntryAvatar =
[
    [ "ParticleEntryAvatar", "de/d6a/classG4INCL_1_1ParticleEntryAvatar.html#acea7a67dfaefaf139718b96c1475ba67", null ],
    [ "~ParticleEntryAvatar", "de/d6a/classG4INCL_1_1ParticleEntryAvatar.html#a14b365712287508885801f7a83ec0c91", null ],
    [ "dump", "de/d6a/classG4INCL_1_1ParticleEntryAvatar.html#a890101aa93c7ad9904e548310f629203", null ],
    [ "getChannel", "de/d6a/classG4INCL_1_1ParticleEntryAvatar.html#a8929d625d14760fb58d5a3c5540be700", null ],
    [ "getParticles", "de/d6a/classG4INCL_1_1ParticleEntryAvatar.html#adab2a28b7485169eb6ec5e561557450a", null ],
    [ "postInteraction", "de/d6a/classG4INCL_1_1ParticleEntryAvatar.html#a63e218ee9f9091b9c4fd01ffd59a9ad4", null ],
    [ "preInteraction", "de/d6a/classG4INCL_1_1ParticleEntryAvatar.html#a835825a1b2ce00fa1f2f9e260e79a287", null ],
    [ "theNucleus", "de/d6a/classG4INCL_1_1ParticleEntryAvatar.html#a40bf687f373eb18878fe7afa39f3862c", null ],
    [ "theParticle", "de/d6a/classG4INCL_1_1ParticleEntryAvatar.html#ab744cf307775b2696ccd4afeac291c41", null ]
];