var classG4eDPWACoulombScatteringModel =
[
    [ "G4eDPWACoulombScatteringModel", "de/dd9/classG4eDPWACoulombScatteringModel.html#a28d488b612089407490074eecfe96b6b", null ],
    [ "~G4eDPWACoulombScatteringModel", "de/dd9/classG4eDPWACoulombScatteringModel.html#a0197e49b23f7e4ca6fdddb4ec78b7707", null ],
    [ "ComputeCrossSectionPerAtom", "de/dd9/classG4eDPWACoulombScatteringModel.html#a79de5867449ffde139e2579fd40b3e0f", null ],
    [ "GetTheDCS", "de/dd9/classG4eDPWACoulombScatteringModel.html#a2fa40e8c74ba2dad4460a8c1352b75bf", null ],
    [ "Initialise", "de/dd9/classG4eDPWACoulombScatteringModel.html#ad357b3b9cc49e1931bfa22feb54614fe", null ],
    [ "InitialiseLocal", "de/dd9/classG4eDPWACoulombScatteringModel.html#a3b09489ae2cf8b5107c44bf8186e4872", null ],
    [ "MinPrimaryEnergy", "de/dd9/classG4eDPWACoulombScatteringModel.html#a847cf9d722dd7c0595dfda3f397b314e", null ],
    [ "SampleSecondaries", "de/dd9/classG4eDPWACoulombScatteringModel.html#ad64b9c5c06c94f0b3e9482477219e168", null ],
    [ "SetTheDCS", "de/dd9/classG4eDPWACoulombScatteringModel.html#afffffd43aeca554dbb06bc6cff27f7d9", null ],
    [ "fIsMixedModel", "de/dd9/classG4eDPWACoulombScatteringModel.html#a873828f23893c5027429f64e75382dcd", null ],
    [ "fIsScpCorrection", "de/dd9/classG4eDPWACoulombScatteringModel.html#a87c1cd0942f69f0ecb68b0fba8e2bc47", null ],
    [ "fMuMin", "de/dd9/classG4eDPWACoulombScatteringModel.html#a51a7dd3cda0a71a662c0b8e6e6b99f14", null ],
    [ "fParticleChange", "de/dd9/classG4eDPWACoulombScatteringModel.html#a87b860f4556e9e2c87b2b23fa97d1ff8", null ],
    [ "fTheDCS", "de/dd9/classG4eDPWACoulombScatteringModel.html#ae192e91949e138963b60ad68a6172e28", null ]
];