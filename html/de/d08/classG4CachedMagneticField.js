var classG4CachedMagneticField =
[
    [ "G4CachedMagneticField", "de/d08/classG4CachedMagneticField.html#a0d2d2f8776aa91fc371716c6b85daa20", null ],
    [ "~G4CachedMagneticField", "de/d08/classG4CachedMagneticField.html#af15109c20774ad541f13f751d39b42b0", null ],
    [ "G4CachedMagneticField", "de/d08/classG4CachedMagneticField.html#a76e571144673ecf6dd6f80f0eebc06ee", null ],
    [ "ClearCounts", "de/d08/classG4CachedMagneticField.html#a435bfa842334a1803dd20f8f6e51e7e4", null ],
    [ "Clone", "de/d08/classG4CachedMagneticField.html#aad911301be7e09787edafebc3c48d40a", null ],
    [ "GetConstDistance", "de/d08/classG4CachedMagneticField.html#a1452cc370ce779338f775c79ce140d3b", null ],
    [ "GetCountCalls", "de/d08/classG4CachedMagneticField.html#a962d49b4a32abdf46981a6d64e467bd7", null ],
    [ "GetCountEvaluations", "de/d08/classG4CachedMagneticField.html#abe39b73cf195d1900462f311c82c14a9", null ],
    [ "GetFieldValue", "de/d08/classG4CachedMagneticField.html#a0018389ea51830bae39d39a0ab43ec69", null ],
    [ "operator=", "de/d08/classG4CachedMagneticField.html#ad8cc071046a13a2720f92e2291412a31", null ],
    [ "ReportStatistics", "de/d08/classG4CachedMagneticField.html#ad6e25cc90c46578b91a9d569cf2f0acc", null ],
    [ "SetConstDistance", "de/d08/classG4CachedMagneticField.html#ae7fcaba4adcf15d7e65a24bbd3922f1b", null ],
    [ "fCountCalls", "de/d08/classG4CachedMagneticField.html#aa82db4c05d9c09735764168c2e07ba0d", null ],
    [ "fCountEvaluations", "de/d08/classG4CachedMagneticField.html#ac734ae6f6e81bc157d48ed814a8dd140", null ],
    [ "fDistanceConst", "de/d08/classG4CachedMagneticField.html#ad4d244cc9c9a7c7e3d34f4c71c5b17c3", null ],
    [ "fLastLocation", "de/d08/classG4CachedMagneticField.html#abf4d38fba1c60ea2b8057fc7cc08e5b2", null ],
    [ "fLastValue", "de/d08/classG4CachedMagneticField.html#a6d58ef430e8d1ddd630a94ca87ece879", null ],
    [ "fpMagneticField", "de/d08/classG4CachedMagneticField.html#aa642fcbb87a4fc209c1c009cdb248cbb", null ]
];