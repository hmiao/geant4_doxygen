var dir_a2e2055ed9fcadb4be6b7b18fd754f09 =
[
    [ "G4AnalyticalPolSolver.hh", "d5/d38/G4AnalyticalPolSolver_8hh.html", "d5/d38/G4AnalyticalPolSolver_8hh" ],
    [ "G4ChebyshevApproximation.hh", "d1/d3e/G4ChebyshevApproximation_8hh.html", "d1/d3e/G4ChebyshevApproximation_8hh" ],
    [ "G4ConvergenceTester.hh", "de/daa/G4ConvergenceTester_8hh.html", "de/daa/G4ConvergenceTester_8hh" ],
    [ "G4DataInterpolation.hh", "d9/de0/G4DataInterpolation_8hh.html", "d9/de0/G4DataInterpolation_8hh" ],
    [ "G4GaussChebyshevQ.hh", "d2/d39/G4GaussChebyshevQ_8hh.html", "d2/d39/G4GaussChebyshevQ_8hh" ],
    [ "G4GaussHermiteQ.hh", "d1/d65/G4GaussHermiteQ_8hh.html", "d1/d65/G4GaussHermiteQ_8hh" ],
    [ "G4GaussJacobiQ.hh", "d3/d13/G4GaussJacobiQ_8hh.html", "d3/d13/G4GaussJacobiQ_8hh" ],
    [ "G4GaussLaguerreQ.hh", "d8/d95/G4GaussLaguerreQ_8hh.html", "d8/d95/G4GaussLaguerreQ_8hh" ],
    [ "G4GaussLegendreQ.hh", "d3/d6c/G4GaussLegendreQ_8hh.html", "d3/d6c/G4GaussLegendreQ_8hh" ],
    [ "G4Integrator.hh", "d7/df2/G4Integrator_8hh.html", "d7/df2/G4Integrator_8hh" ],
    [ "G4JTPolynomialSolver.hh", "dd/db8/G4JTPolynomialSolver_8hh.html", "dd/db8/G4JTPolynomialSolver_8hh" ],
    [ "G4PolynomialSolver.hh", "d0/d9b/G4PolynomialSolver_8hh.html", "d0/d9b/G4PolynomialSolver_8hh" ],
    [ "G4SimpleIntegration.hh", "d8/dfb/G4SimpleIntegration_8hh.html", "d8/dfb/G4SimpleIntegration_8hh" ],
    [ "G4SimplexDownhill.hh", "d1/d02/G4SimplexDownhill_8hh.html", "d1/d02/G4SimplexDownhill_8hh" ],
    [ "G4StatAnalysis.hh", "db/d79/G4StatAnalysis_8hh.html", "db/d79/G4StatAnalysis_8hh" ],
    [ "G4StatDouble.hh", "d3/d47/G4StatDouble_8hh.html", "d3/d47/G4StatDouble_8hh" ],
    [ "G4VGaussianQuadrature.hh", "d4/d8a/G4VGaussianQuadrature_8hh.html", "d4/d8a/G4VGaussianQuadrature_8hh" ]
];