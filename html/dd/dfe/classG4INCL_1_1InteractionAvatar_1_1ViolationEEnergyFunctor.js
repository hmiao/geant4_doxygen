var classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor =
[
    [ "ViolationEEnergyFunctor", "dd/dfe/classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor.html#a7c261814942ca92585367cffc1a6a94a", null ],
    [ "~ViolationEEnergyFunctor", "dd/dfe/classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor.html#a015c00046753115f83f18c2b4c1504c1", null ],
    [ "cleanUp", "dd/dfe/classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor.html#a82a2defccd98fb59ed98e2d47839157d", null ],
    [ "operator()", "dd/dfe/classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor.html#a646d8b521c95d1fc0f7e9b832566b84b", null ],
    [ "setParticleEnergy", "dd/dfe/classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor.html#a8538213f12591f95e1ba4e15c3245c4b", null ],
    [ "energyThreshold", "dd/dfe/classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor.html#a2be1c6d8d6beb4f9794b181528ee4548", null ],
    [ "initialEnergy", "dd/dfe/classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor.html#a6bba4ea750df98d19db4b8ae0cbcae04", null ],
    [ "shouldUseLocalEnergy", "dd/dfe/classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor.html#a873079fe58df3f128cbdfdf00b812555", null ],
    [ "theEnergy", "dd/dfe/classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor.html#a8c2edc4abb45c91545f741d50c7c53dc", null ],
    [ "theMomentum", "dd/dfe/classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor.html#a70662a3a96328c92bd7b980c6a99f345", null ],
    [ "theNucleus", "dd/dfe/classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor.html#a8c61f16028459e619536102324c9cba8", null ],
    [ "theParticle", "dd/dfe/classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor.html#aed7d612768f8747ee4166e907e61392b", null ]
];