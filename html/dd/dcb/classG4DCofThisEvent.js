var classG4DCofThisEvent =
[
    [ "G4DCofThisEvent", "dd/dcb/classG4DCofThisEvent.html#aa0649ced32a7264311f410663da6c0fb", null ],
    [ "G4DCofThisEvent", "dd/dcb/classG4DCofThisEvent.html#aa8e3c000b2171f14180ee8be1d6c4746", null ],
    [ "~G4DCofThisEvent", "dd/dcb/classG4DCofThisEvent.html#a7695d018c04f099ae1f69e1d944948f2", null ],
    [ "G4DCofThisEvent", "dd/dcb/classG4DCofThisEvent.html#a89006705200c22e86ed609e088fbe18d", null ],
    [ "AddDigiCollection", "dd/dcb/classG4DCofThisEvent.html#ad6d0726832673bf39a0f5a0fde06f198", null ],
    [ "GetCapacity", "dd/dcb/classG4DCofThisEvent.html#a93a10c3930052cc901c8e394c53f0c7a", null ],
    [ "GetDC", "dd/dcb/classG4DCofThisEvent.html#a6130b501668255274f0c590621d5ef2a", null ],
    [ "GetNumberOfCollections", "dd/dcb/classG4DCofThisEvent.html#aa73764f4521e0a3b3647da0e1b77f74d", null ],
    [ "operator delete", "dd/dcb/classG4DCofThisEvent.html#a667582cae02dbdfd87a57bee1c2fa736", null ],
    [ "operator new", "dd/dcb/classG4DCofThisEvent.html#a7ffed4c8a3f09f2c1cd6a8e42d4cc5b1", null ],
    [ "operator=", "dd/dcb/classG4DCofThisEvent.html#ab3d6d2a61bf77b52692f566f3dbd49ed", null ],
    [ "DC", "dd/dcb/classG4DCofThisEvent.html#aaf233c2bdb61e13b61c451b51bace7c7", null ]
];