var classG4InteractionLawPhysical =
[
    [ "G4InteractionLawPhysical", "dd/d2b/classG4InteractionLawPhysical.html#a166797586f9fba470f056a9b5ea2459b", null ],
    [ "~G4InteractionLawPhysical", "dd/d2b/classG4InteractionLawPhysical.html#a55d366e16874c7ec6b6dd76f26bc405e", null ],
    [ "ComputeEffectiveCrossSectionAt", "dd/d2b/classG4InteractionLawPhysical.html#a935dd5da2f88bba17e3f449095f3bef0", null ],
    [ "ComputeNonInteractionProbabilityAt", "dd/d2b/classG4InteractionLawPhysical.html#a63df1d2b9d473c50cb87d54e824b4d99", null ],
    [ "GetPhysicalCrossSection", "dd/d2b/classG4InteractionLawPhysical.html#ae2ef9d779f498686ee27289b71fd4d04", null ],
    [ "SampleInteractionLength", "dd/d2b/classG4InteractionLawPhysical.html#a07cf0d69757e148b70be1a69ec96192e", null ],
    [ "SetPhysicalCrossSection", "dd/d2b/classG4InteractionLawPhysical.html#a499687e9011d58beda05552e96fe13f3", null ],
    [ "UpdateInteractionLengthForStep", "dd/d2b/classG4InteractionLawPhysical.html#a0ad9ed56c635c49f36e46847c93e80a0", null ],
    [ "fCrossSection", "dd/d2b/classG4InteractionLawPhysical.html#a8629b0b0b6582b9e019b60fecb99cb0d", null ],
    [ "fCrossSectionDefined", "dd/d2b/classG4InteractionLawPhysical.html#a8b39d3ba6e26d11761b6d8d84bcc306d", null ],
    [ "fNumberOfInteractionLength", "dd/d2b/classG4InteractionLawPhysical.html#a429fd2744ace9ca363986a6cd9fc5bd1", null ]
];