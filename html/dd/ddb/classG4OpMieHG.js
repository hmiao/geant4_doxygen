var classG4OpMieHG =
[
    [ "G4OpMieHG", "dd/ddb/classG4OpMieHG.html#a17bd5ba02aa69d2a395343032a08d121", null ],
    [ "~G4OpMieHG", "dd/ddb/classG4OpMieHG.html#af7131902da6460132f16c46b4bfa7724", null ],
    [ "G4OpMieHG", "dd/ddb/classG4OpMieHG.html#a5242c216425d2538d78c94af8b6c4054", null ],
    [ "GetMeanFreePath", "dd/ddb/classG4OpMieHG.html#a08dd9aca7f90c59185bb88526fe72eab", null ],
    [ "Initialise", "dd/ddb/classG4OpMieHG.html#acbab75581eaccff6f6ac0df591f02e51", null ],
    [ "IsApplicable", "dd/ddb/classG4OpMieHG.html#a2554400f19290f974a6e91fe95785689", null ],
    [ "operator=", "dd/ddb/classG4OpMieHG.html#a7d1f83bf8b249358e70139e5edf0a3a6", null ],
    [ "PostStepDoIt", "dd/ddb/classG4OpMieHG.html#ae6dc3e3280a38514f36bac9c82751ab2", null ],
    [ "PreparePhysicsTable", "dd/ddb/classG4OpMieHG.html#a65fbe185e4ae67fa512bb132c602ad3f", null ],
    [ "SetVerboseLevel", "dd/ddb/classG4OpMieHG.html#a85336d2993226ef7096407352df49018", null ],
    [ "idx_mie", "dd/ddb/classG4OpMieHG.html#a8bdd50ced6fd11f1fcdb253db3b78bc6", null ]
];