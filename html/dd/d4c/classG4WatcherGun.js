var classG4WatcherGun =
[
    [ "G4WatcherGun", "dd/d4c/classG4WatcherGun.html#af39c4a82ff62f8a9121bd296a29de942", null ],
    [ "getWatchers", "dd/d4c/classG4WatcherGun.html#af22048b1ee44e4238dcba89fb8406f2f", null ],
    [ "setWatchers", "dd/d4c/classG4WatcherGun.html#a501930412a0a000f21fe6fc9b1919002", null ],
    [ "verboseLevel", "dd/d4c/classG4WatcherGun.html#ad33bd7607e2d69041650d2e8bc4058d5", null ],
    [ "watchers", "dd/d4c/classG4WatcherGun.html#af67619b0f3ed9ae4362ee8125001329d", null ]
];