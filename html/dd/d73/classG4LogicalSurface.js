var classG4LogicalSurface =
[
    [ "~G4LogicalSurface", "dd/d73/classG4LogicalSurface.html#a9f87a9cca9697a9b91bb9fca949e9887", null ],
    [ "G4LogicalSurface", "dd/d73/classG4LogicalSurface.html#ae25a6ffa11060a10729bb4992052254e", null ],
    [ "G4LogicalSurface", "dd/d73/classG4LogicalSurface.html#a8a88441f162cce96699900406f40fb02", null ],
    [ "GetName", "dd/d73/classG4LogicalSurface.html#a40ad66aed262939b050daea94a778944", null ],
    [ "GetSurfaceProperty", "dd/d73/classG4LogicalSurface.html#a8dcc05c3c4b823e3615f03e2799373b8", null ],
    [ "GetTransitionRadiationSurface", "dd/d73/classG4LogicalSurface.html#aa34b67846e24d227dca84c2b7012cc1c", null ],
    [ "operator!=", "dd/d73/classG4LogicalSurface.html#aa1483372327dc1ece578756ca282089b", null ],
    [ "operator=", "dd/d73/classG4LogicalSurface.html#a88a6b7b13b563b0a688ca3472fc789e1", null ],
    [ "operator==", "dd/d73/classG4LogicalSurface.html#ae088373a1241782f4708c016beb103ec", null ],
    [ "SetName", "dd/d73/classG4LogicalSurface.html#a8d8eea1c1dfdc34dec1b3895e28b5a49", null ],
    [ "SetSurfaceProperty", "dd/d73/classG4LogicalSurface.html#acf0d6a240a06297339cd75083bc1c722", null ],
    [ "SetTransitionRadiationSurface", "dd/d73/classG4LogicalSurface.html#a285bec38d789c89a6cda362e1886ce14", null ],
    [ "theName", "dd/d73/classG4LogicalSurface.html#a8a0df40690b5429753442808007376f7", null ],
    [ "theSurfaceProperty", "dd/d73/classG4LogicalSurface.html#ad81c7649a833086119533aa285a69d68", null ],
    [ "theTransRadSurface", "dd/d73/classG4LogicalSurface.html#ad87cf51cc61b7318ec0050178d417172", null ]
];