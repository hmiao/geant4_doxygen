var classG4ProtonCoulombBarrier =
[
    [ "G4ProtonCoulombBarrier", "dd/d73/classG4ProtonCoulombBarrier.html#a1e06f1da765d43d8173e7c1ddd921d0a", null ],
    [ "~G4ProtonCoulombBarrier", "dd/d73/classG4ProtonCoulombBarrier.html#a222c975da4d97af1cc71ed4a06864e05", null ],
    [ "G4ProtonCoulombBarrier", "dd/d73/classG4ProtonCoulombBarrier.html#a538fa39a559f08ab34d4407f56bdde72", null ],
    [ "BarrierPenetrationFactor", "dd/d73/classG4ProtonCoulombBarrier.html#ad54af256dc195bdf9aa74002bee730d9", null ],
    [ "operator!=", "dd/d73/classG4ProtonCoulombBarrier.html#a7f60655d2754107c6c0059a6d31f3a93", null ],
    [ "operator=", "dd/d73/classG4ProtonCoulombBarrier.html#a31e9c7593b1d9ffad0d9b1dbadb2ffd1", null ],
    [ "operator==", "dd/d73/classG4ProtonCoulombBarrier.html#a245bb08bf7b6c933aeca1c1b1473b71a", null ]
];