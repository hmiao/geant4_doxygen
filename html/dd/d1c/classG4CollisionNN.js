var classG4CollisionNN =
[
    [ "G4CollisionNN", "dd/d1c/classG4CollisionNN.html#a82087d69d69e04028008c320dcd68426", null ],
    [ "~G4CollisionNN", "dd/d1c/classG4CollisionNN.html#a128d2f6e81a5522919aa274bf76ba50f", null ],
    [ "G4CollisionNN", "dd/d1c/classG4CollisionNN.html#a4a495c5a7fd37daecbcc0128cdba0da2", null ],
    [ "CrossSection", "dd/d1c/classG4CollisionNN.html#a0142fe8681f80cda507986140c34be75", null ],
    [ "GetAngularDistribution", "dd/d1c/classG4CollisionNN.html#a030b3b492a28f630e2b467ed94342865", null ],
    [ "GetComponents", "dd/d1c/classG4CollisionNN.html#acf38c77d5e74753c94e63874632c8ba5", null ],
    [ "GetCrossSectionSource", "dd/d1c/classG4CollisionNN.html#ab040efd787726660ba4a891c30640ba6", null ],
    [ "GetListOfColliders", "dd/d1c/classG4CollisionNN.html#a469a307da64006aae6189ca7f38c680e", null ],
    [ "GetName", "dd/d1c/classG4CollisionNN.html#afdc5272856c28416d7d19c47a10133f4", null ],
    [ "operator!=", "dd/d1c/classG4CollisionNN.html#ae073ec26c5afcc7280456deffac41c79", null ],
    [ "operator=", "dd/d1c/classG4CollisionNN.html#ae600858982fda7c298d68ef1f56e4c58", null ],
    [ "operator==", "dd/d1c/classG4CollisionNN.html#ac876ba78af0349e3eee2d9539bd6f28e", null ],
    [ "colliders1", "dd/d1c/classG4CollisionNN.html#ab9e26dd4a45ebe1037e2b6bc1235f2a7", null ],
    [ "colliders2", "dd/d1c/classG4CollisionNN.html#a31110943ac5643d092873ce1c7d76f35", null ],
    [ "components", "dd/d1c/classG4CollisionNN.html#a621546379466e3a5f336268ee34b707f", null ],
    [ "crossSectionSource", "dd/d1c/classG4CollisionNN.html#a7a27dcd8600e1deda7dfd2e7d5830e9c", null ]
];