var classG4BinaryHe3Builder =
[
    [ "G4BinaryHe3Builder", "dd/d0c/classG4BinaryHe3Builder.html#aee3dcdd2959832bddcd50aad742711ee", null ],
    [ "~G4BinaryHe3Builder", "dd/d0c/classG4BinaryHe3Builder.html#a1885998224f2eb645376b578b93d5e31", null ],
    [ "Build", "dd/d0c/classG4BinaryHe3Builder.html#a94b662bccc4562b6ffdb22822f825d87", null ],
    [ "Build", "dd/d0c/classG4BinaryHe3Builder.html#a92eb630ef0a17dcd98b230dd822e5095", null ],
    [ "Build", "dd/d0c/classG4BinaryHe3Builder.html#a2a7c501ea893d6c7a608159287fb996c", null ],
    [ "Build", "dd/d0c/classG4BinaryHe3Builder.html#a88cded6003d9507a15062ab819ec4102", null ],
    [ "SetMaxEnergy", "dd/d0c/classG4BinaryHe3Builder.html#aa40f748035159d2ad97b571b22513911", null ],
    [ "SetMinEnergy", "dd/d0c/classG4BinaryHe3Builder.html#ac510f6a55bb2960e0ea40dfe78b3e515", null ],
    [ "theMax", "dd/d0c/classG4BinaryHe3Builder.html#aa7ff6ff47d44d0be0ab1495474efa906", null ],
    [ "theMin", "dd/d0c/classG4BinaryHe3Builder.html#a10672fe59d5801a44f93dbf007f5a48d", null ],
    [ "theModel", "dd/d0c/classG4BinaryHe3Builder.html#a40eac39b0dd77e20f47ba52d711cc4ff", null ]
];