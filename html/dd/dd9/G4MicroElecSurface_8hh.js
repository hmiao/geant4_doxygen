var G4MicroElecSurface_8hh =
[
    [ "G4MicroElecSurface", "d7/dc6/classG4MicroElecSurface.html", "d7/dc6/classG4MicroElecSurface" ],
    [ "G4MicroElecSurfaceStatus", "dd/dd9/G4MicroElecSurface_8hh.html#a9004665ae21600067ca6f16ae03686d2", [
      [ "UndefinedSurf", "dd/dd9/G4MicroElecSurface_8hh.html#a9004665ae21600067ca6f16ae03686d2adbd3c57524a6e12d35c83a99d58ebc01", null ],
      [ "NotAtBoundarySurf", "dd/dd9/G4MicroElecSurface_8hh.html#a9004665ae21600067ca6f16ae03686d2aef83426613343a4979bdf041fd455bfa", null ],
      [ "SameMaterialSurf", "dd/dd9/G4MicroElecSurface_8hh.html#a9004665ae21600067ca6f16ae03686d2a3bd8dd1bb8580e611f20450632f27bba", null ],
      [ "StepTooSmallSurf", "dd/dd9/G4MicroElecSurface_8hh.html#a9004665ae21600067ca6f16ae03686d2a037f2b919b9164a3b2137b831d7bf777", null ]
    ] ]
];