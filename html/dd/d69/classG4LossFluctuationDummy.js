var classG4LossFluctuationDummy =
[
    [ "G4LossFluctuationDummy", "dd/d69/classG4LossFluctuationDummy.html#aa50e9679aa681406a45377abe6185530", null ],
    [ "~G4LossFluctuationDummy", "dd/d69/classG4LossFluctuationDummy.html#ad1e48e520de0abd55f18406643ed1890", null ],
    [ "G4LossFluctuationDummy", "dd/d69/classG4LossFluctuationDummy.html#a33f7b545817a48525922ef6bfbdfa6b9", null ],
    [ "Dispersion", "dd/d69/classG4LossFluctuationDummy.html#a63633a83aa4952e13c24d88a760c002e", null ],
    [ "operator=", "dd/d69/classG4LossFluctuationDummy.html#a518fd7d040419aff58e88760f4ebbe86", null ],
    [ "SampleFluctuations", "dd/d69/classG4LossFluctuationDummy.html#a26b327ab69f712e7f48f65fb969251b7", null ],
    [ "SetParticleAndCharge", "dd/d69/classG4LossFluctuationDummy.html#adc838ccdae86d879399a9335a4158c7f", null ]
];