var classG4tgrPlace =
[
    [ "G4tgrPlace", "dd/dc9/classG4tgrPlace.html#a3a78b2d4fe47a951fc71f04bb7c11138", null ],
    [ "~G4tgrPlace", "dd/dc9/classG4tgrPlace.html#ae6b508fe78421443bb770725cfd51079", null ],
    [ "GetCopyNo", "dd/dc9/classG4tgrPlace.html#ad0ba36aaa9db8fdb6ebf454603fe4cfe", null ],
    [ "GetParentName", "dd/dc9/classG4tgrPlace.html#a742f3be098ce6956f70817d38916b671", null ],
    [ "GetPlacement", "dd/dc9/classG4tgrPlace.html#a1f870dc5e86d041710498d220af7adc1", null ],
    [ "GetType", "dd/dc9/classG4tgrPlace.html#aaca617fbe9f61bfafd5fdd157357f7fd", null ],
    [ "GetVolume", "dd/dc9/classG4tgrPlace.html#ae7fa6043e2ea5a0f6d4b541cd4e056c7", null ],
    [ "SetType", "dd/dc9/classG4tgrPlace.html#a6d0c72ca220ffcdc43670b38aec7e529", null ],
    [ "SetVolume", "dd/dc9/classG4tgrPlace.html#ae079f24e71f062c0c7e07fa1fffef405", null ],
    [ "theCopyNo", "dd/dc9/classG4tgrPlace.html#adf0c8b8649cc79bdb7d85f30735e486c", null ],
    [ "theParentName", "dd/dc9/classG4tgrPlace.html#a47083fd517a45d513d7354b83dd41aaa", null ],
    [ "theType", "dd/dc9/classG4tgrPlace.html#aeb8105d0895721ce956660ddda4d2903", null ],
    [ "theVolume", "dd/dc9/classG4tgrPlace.html#a0341a39230c74a51227be4bba239f89f", null ]
];