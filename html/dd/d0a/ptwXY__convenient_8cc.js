var ptwXY__convenient_8cc =
[
    [ "minEps", "dd/d0a/ptwXY__convenient_8cc.html#a07609e240ac7f0c6db652d17cfea2b2d", null ],
    [ "ptwXY_areDomainsMutual", "dd/d0a/ptwXY__convenient_8cc.html#a6a820d1b37a6f6014ed84c45b8fbca58", null ],
    [ "ptwXY_copyToC_XY", "dd/d0a/ptwXY__convenient_8cc.html#aab1c4747e092f7fc62af7aff6026ee8c", null ],
    [ "ptwXY_createGaussian", "dd/d0a/ptwXY__convenient_8cc.html#a60a8c756df12ab75362f65e3ee26da5a", null ],
    [ "ptwXY_createGaussianCenteredSigma1", "dd/d0a/ptwXY__convenient_8cc.html#af67cd8846e21c9df44f6789996907ecf", null ],
    [ "ptwXY_createGaussianCenteredSigma1_2", "dd/d0a/ptwXY__convenient_8cc.html#aea72ea17d3d1409208c7a89358ce0c2a", null ],
    [ "ptwXY_dullEdges", "dd/d0a/ptwXY__convenient_8cc.html#a4cdaa2fadbecfc6311a9bf17cdc0286f", null ],
    [ "ptwXY_getXArray", "dd/d0a/ptwXY__convenient_8cc.html#a6a96df4a17360d3d6de1a869551fb425", null ],
    [ "ptwXY_intersectionWith_ptwX", "dd/d0a/ptwXY__convenient_8cc.html#a63d340b9a2a58bfd0d818a732898b70b", null ],
    [ "ptwXY_mergeClosePoints", "dd/d0a/ptwXY__convenient_8cc.html#a670d448cdfdb00d90c0fbbb2cc7728dc", null ],
    [ "ptwXY_mutualifyDomains", "dd/d0a/ptwXY__convenient_8cc.html#a4704c0636d800a8a7d86ef9601e06562", null ],
    [ "ptwXY_tweakDomainsToMutualify", "dd/d0a/ptwXY__convenient_8cc.html#a634b1c162b71004fa4947e3944bd6458", null ],
    [ "ptwXY_valueTo_ptwXAndY", "dd/d0a/ptwXY__convenient_8cc.html#af1240c99755eaaf08394c0a023dc934c", null ],
    [ "ptwXY_valueTo_ptwXY", "dd/d0a/ptwXY__convenient_8cc.html#a9603d199f92c66a3ecec71271922b1a7", null ]
];