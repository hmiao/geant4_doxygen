var classG4HadronicAbsorptionFritiofWithBinaryCascade =
[
    [ "G4HadronicAbsorptionFritiofWithBinaryCascade", "dd/da7/classG4HadronicAbsorptionFritiofWithBinaryCascade.html#ae752f0a1d75db7abc99072516729b84c", null ],
    [ "~G4HadronicAbsorptionFritiofWithBinaryCascade", "dd/da7/classG4HadronicAbsorptionFritiofWithBinaryCascade.html#a11426f8a0d91a9e1ef1a6c00c11186c7", null ],
    [ "G4HadronicAbsorptionFritiofWithBinaryCascade", "dd/da7/classG4HadronicAbsorptionFritiofWithBinaryCascade.html#a1d10eb40e4e1001805b17ac133c66328", null ],
    [ "IsApplicable", "dd/da7/classG4HadronicAbsorptionFritiofWithBinaryCascade.html#a46d343d70a3008f526e5f0039cd9d102", null ],
    [ "operator=", "dd/da7/classG4HadronicAbsorptionFritiofWithBinaryCascade.html#a6ac07b01b7b05ca675e9832ca73996f9", null ],
    [ "ProcessDescription", "dd/da7/classG4HadronicAbsorptionFritiofWithBinaryCascade.html#a0239423ca36574f0b5591abd60bbdb19", null ],
    [ "pdefApplicable", "dd/da7/classG4HadronicAbsorptionFritiofWithBinaryCascade.html#ab850c2c36ae7d84db3293f68b46cbf8a", null ]
];