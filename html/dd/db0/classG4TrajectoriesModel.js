var classG4TrajectoriesModel =
[
    [ "G4TrajectoriesModel", "dd/db0/classG4TrajectoriesModel.html#ae0916d41cce15ddb9d75002f81cb42c1", null ],
    [ "~G4TrajectoriesModel", "dd/db0/classG4TrajectoriesModel.html#a348b4772648d43fef634d03ae9757126", null ],
    [ "CreateCurrentAttValues", "dd/db0/classG4TrajectoriesModel.html#a7808c68e077138a6c649ef0438d01cb7", null ],
    [ "DescribeYourselfTo", "dd/db0/classG4TrajectoriesModel.html#a190dd0a2d515bab593798aa1cfc71a6c", null ],
    [ "GetAttDefs", "dd/db0/classG4TrajectoriesModel.html#ae26911b9ec2a6650e30efc2cbf23b11e", null ],
    [ "GetCurrentTrajectory", "dd/db0/classG4TrajectoriesModel.html#a5a7ed940248a9f344499ffb686a0b398", null ],
    [ "SetCurrentTrajectory", "dd/db0/classG4TrajectoriesModel.html#a030c0c0cb9c5115c1436706b762a83ed", null ],
    [ "SetEventID", "dd/db0/classG4TrajectoriesModel.html#a7489e42410e6d0d6a863aba931a8b91a", null ],
    [ "SetRunID", "dd/db0/classG4TrajectoriesModel.html#afdd99f90b8d19d718249286dbd318e1f", null ],
    [ "fEventID", "dd/db0/classG4TrajectoriesModel.html#a710da465d72f882331d0d469fa2d7189", null ],
    [ "fpCurrentTrajectory", "dd/db0/classG4TrajectoriesModel.html#ad070bb104b8b3f213207edb6ee1d2391", null ],
    [ "fRunID", "dd/db0/classG4TrajectoriesModel.html#a26df5f9dd14be79b8781b61505d97c20", null ]
];