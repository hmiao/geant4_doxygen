var classG4NuElNucleusNcModel =
[
    [ "G4NuElNucleusNcModel", "dd/d3a/classG4NuElNucleusNcModel.html#ada639f1bb40912e8b846bcdf242ef315", null ],
    [ "~G4NuElNucleusNcModel", "dd/d3a/classG4NuElNucleusNcModel.html#a7f0fd3c16807df0a331164e9f34b67d4", null ],
    [ "ApplyYourself", "dd/d3a/classG4NuElNucleusNcModel.html#a8e64f6060ef531fa2966816f0fb72cf7", null ],
    [ "GetMinNuElEnergy", "dd/d3a/classG4NuElNucleusNcModel.html#a9f2557bd573ed3e49f2c3b6ccd13cdf3", null ],
    [ "InitialiseModel", "dd/d3a/classG4NuElNucleusNcModel.html#acc113ee8b279da949e92f4eaa8b191e0", null ],
    [ "IsApplicable", "dd/d3a/classG4NuElNucleusNcModel.html#a8421d1b3a41969baba918dda02472195", null ],
    [ "ModelDescription", "dd/d3a/classG4NuElNucleusNcModel.html#a7f48e2c27216c470a89c7e9fab90e8af", null ],
    [ "SampleLVkr", "dd/d3a/classG4NuElNucleusNcModel.html#a5a1889f02511b1937c568b9a11fe05c5", null ],
    [ "ThresholdEnergy", "dd/d3a/classG4NuElNucleusNcModel.html#a95fcd3a02a6b156f3ded253f37fb2975", null ],
    [ "fData", "dd/d3a/classG4NuElNucleusNcModel.html#ad2f128f78cc6a5672e7a8031a132532c", null ],
    [ "fMaster", "dd/d3a/classG4NuElNucleusNcModel.html#ae248931e8851b555b5bc69db5fe36d45", null ],
    [ "fMnumu", "dd/d3a/classG4NuElNucleusNcModel.html#a8da0e0feebb8e978439b455c50f69c4d", null ],
    [ "theNuE", "dd/d3a/classG4NuElNucleusNcModel.html#a6f2dc03ca084df9f5eeba2a19f0dfb87", null ]
];