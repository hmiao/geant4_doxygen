var classG4AdjointhMultipleScattering =
[
    [ "G4AdjointhMultipleScattering", "dd/df4/classG4AdjointhMultipleScattering.html#afda004d4f01ddf83444260237f38186c", null ],
    [ "~G4AdjointhMultipleScattering", "dd/df4/classG4AdjointhMultipleScattering.html#ab059b638ffb9b937283c11d263383f3a", null ],
    [ "G4AdjointhMultipleScattering", "dd/df4/classG4AdjointhMultipleScattering.html#a39a1a5beacf631bbd0efdf3d08a698de", null ],
    [ "DumpInfo", "dd/df4/classG4AdjointhMultipleScattering.html#af9f55d54850c814a5bde94f357898044", null ],
    [ "InitialiseProcess", "dd/df4/classG4AdjointhMultipleScattering.html#ac854fbacc0357d240fce127051130af3", null ],
    [ "IsApplicable", "dd/df4/classG4AdjointhMultipleScattering.html#a9fbfa2e58115753919e662ee293d2e14", null ],
    [ "operator=", "dd/df4/classG4AdjointhMultipleScattering.html#a9c424f9574278446544ad415335ea1f0", null ],
    [ "ProcessDescription", "dd/df4/classG4AdjointhMultipleScattering.html#a458ab673d194b01b8b78d2eb6eb52ec2", null ],
    [ "StreamProcessInfo", "dd/df4/classG4AdjointhMultipleScattering.html#af2f3f0cae0417939dabd431e1c4e78e9", null ],
    [ "fIsInitialized", "dd/df4/classG4AdjointhMultipleScattering.html#adb6354d6fe1e6bd7e4fd010ec527f07e", null ]
];