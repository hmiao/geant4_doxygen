var G4GSMottCorrection_8hh =
[
    [ "G4GSMottCorrection", "d2/dc5/classG4GSMottCorrection.html", "d2/dc5/classG4GSMottCorrection" ],
    [ "G4GSMottCorrection::DataPerDelta", "df/d90/structG4GSMottCorrection_1_1DataPerDelta.html", "df/d90/structG4GSMottCorrection_1_1DataPerDelta" ],
    [ "G4GSMottCorrection::DataPerEkin", "df/d60/structG4GSMottCorrection_1_1DataPerEkin.html", "df/d60/structG4GSMottCorrection_1_1DataPerEkin" ],
    [ "G4GSMottCorrection::DataPerMaterial", "da/d99/structG4GSMottCorrection_1_1DataPerMaterial.html", "da/d99/structG4GSMottCorrection_1_1DataPerMaterial" ]
];