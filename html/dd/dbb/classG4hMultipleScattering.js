var classG4hMultipleScattering =
[
    [ "G4hMultipleScattering", "dd/dbb/classG4hMultipleScattering.html#a5742ecd738b9ddba89060b48a43bd20f", null ],
    [ "~G4hMultipleScattering", "dd/dbb/classG4hMultipleScattering.html#a97576ce2761997aca8166b0ee4881a82", null ],
    [ "G4hMultipleScattering", "dd/dbb/classG4hMultipleScattering.html#a0eb7289bdb70bddc1a87291de6c6db9b", null ],
    [ "InitialiseProcess", "dd/dbb/classG4hMultipleScattering.html#af37380ed0b767d9d216bff4a173ea199", null ],
    [ "IsApplicable", "dd/dbb/classG4hMultipleScattering.html#af9694fa6a01911d7bc3837c258d47431", null ],
    [ "operator=", "dd/dbb/classG4hMultipleScattering.html#a3a2c9483f7a5c6c81bf9c6159d701eaa", null ],
    [ "ProcessDescription", "dd/dbb/classG4hMultipleScattering.html#ae25fa5e9de1d424f5a893c1dc46dcddc", null ],
    [ "StreamProcessInfo", "dd/dbb/classG4hMultipleScattering.html#ad4f49cc893d61e710a8c1da27c056c78", null ],
    [ "isInitialized", "dd/dbb/classG4hMultipleScattering.html#aceb86ecc8595bd2114f1563ade7ba219", null ]
];