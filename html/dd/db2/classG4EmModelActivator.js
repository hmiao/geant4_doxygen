var classG4EmModelActivator =
[
    [ "G4EmModelActivator", "dd/db2/classG4EmModelActivator.html#a161649a62659a22cfd1909205a555854", null ],
    [ "G4EmModelActivator", "dd/db2/classG4EmModelActivator.html#aa58bff409e9cc53062c7e42b5da3d625", null ],
    [ "ActivateEmOptions", "dd/db2/classG4EmModelActivator.html#a9231e6b136b19dea0f84dd8a78042b9f", null ],
    [ "ActivateMicroElec", "dd/db2/classG4EmModelActivator.html#a1f536ca81ae82b0e183a332253e4f1b6", null ],
    [ "ActivatePAI", "dd/db2/classG4EmModelActivator.html#a597ba8f321c0f662e55aa5a956dddf37", null ],
    [ "AddStandardScattering", "dd/db2/classG4EmModelActivator.html#a92e91c6c1ea50cf3fa731b6a0df0b553", null ],
    [ "FindOrAddProcess", "dd/db2/classG4EmModelActivator.html#a392fd22befdb99b756234c6ce099eb72", null ],
    [ "HasMsc", "dd/db2/classG4EmModelActivator.html#aaac32526bcc3096ddac682499726793e", null ],
    [ "operator=", "dd/db2/classG4EmModelActivator.html#a09fdecfc24f68ba71da9a38b5a933674", null ],
    [ "SetMscParameters", "dd/db2/classG4EmModelActivator.html#ae7d26b11657266a05e265078a9eb945f", null ],
    [ "baseName", "dd/db2/classG4EmModelActivator.html#a6605c514e48b1ab3bb92540d78fde993", null ],
    [ "theParameters", "dd/db2/classG4EmModelActivator.html#a2666b20e9df4b8ad4aacfb8946dd85af", null ]
];