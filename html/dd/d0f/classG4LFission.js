var classG4LFission =
[
    [ "G4LFission", "dd/d0f/classG4LFission.html#a1e3f9f54b64db24cf0fc22b10ae0a2bb", null ],
    [ "~G4LFission", "dd/d0f/classG4LFission.html#a1b5523cf9e95b2b905fed3e1c7606d8c", null ],
    [ "ApplyYourself", "dd/d0f/classG4LFission.html#aa57fcfea6c13b917b10c19b36f376f8d", null ],
    [ "Atomas", "dd/d0f/classG4LFission.html#adfc441b97e8316ba8cc0c10a4c3d5515", null ],
    [ "GetFatalEnergyCheckLevels", "dd/d0f/classG4LFission.html#a834295f5f7191a5dc16aac62fc37ab44", null ],
    [ "init", "dd/d0f/classG4LFission.html#ada2086d4bbd99c063f059f9b1fc32eb5", null ],
    [ "ModelDescription", "dd/d0f/classG4LFission.html#a84672a4f42e374deefd5f3b4b65957b6", null ],
    [ "secID", "dd/d0f/classG4LFission.html#a26f9bb3d3f5b6aa1ec9ba9d43186e477", null ],
    [ "spneut", "dd/d0f/classG4LFission.html#a2a4bcb246fa31a2fd7dad0ed78640254", null ]
];