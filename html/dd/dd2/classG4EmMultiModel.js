var classG4EmMultiModel =
[
    [ "G4EmMultiModel", "dd/dd2/classG4EmMultiModel.html#a1c13d30d8993d3d5b36c768e34ea90de", null ],
    [ "~G4EmMultiModel", "dd/dd2/classG4EmMultiModel.html#a84c289e86960dabfd1152652d9ba959f", null ],
    [ "G4EmMultiModel", "dd/dd2/classG4EmMultiModel.html#a37ac1a086e2f5c34762961dd629dfbab", null ],
    [ "AddModel", "dd/dd2/classG4EmMultiModel.html#aa9dc4a9e74bcd49b1ef9b90743b71cf0", null ],
    [ "ComputeCrossSectionPerAtom", "dd/dd2/classG4EmMultiModel.html#a85e4e55b2cad491471f3d7538c571116", null ],
    [ "ComputeDEDXPerVolume", "dd/dd2/classG4EmMultiModel.html#a8149325d0539bdac86487c5c0778daf0", null ],
    [ "Initialise", "dd/dd2/classG4EmMultiModel.html#a87ca92f146a4e0c064eeb085485ff486", null ],
    [ "operator=", "dd/dd2/classG4EmMultiModel.html#a900cb99777143aba9146e9d645f0eab3", null ],
    [ "SampleSecondaries", "dd/dd2/classG4EmMultiModel.html#a2a5899f10024864e3c12a1eb79d6ae33", null ],
    [ "cross_section", "dd/dd2/classG4EmMultiModel.html#a5014001d52665e406efb613357054343", null ],
    [ "model", "dd/dd2/classG4EmMultiModel.html#a42e4558248791200a7171c73014bc030", null ],
    [ "nModels", "dd/dd2/classG4EmMultiModel.html#ae14760cea1681299876d33cfcf01ef9d", null ]
];