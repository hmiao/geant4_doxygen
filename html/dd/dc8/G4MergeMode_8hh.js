var G4MergeMode_8hh =
[
    [ "G4MergeFunction", "dd/dc8/G4MergeMode_8hh.html#abf7068f1de630983c5e238d20de46c51", null ],
    [ "G4MergeMode", "dd/dc8/G4MergeMode_8hh.html#a7a098722b50c17a955e924689ab9c0ea", [
      [ "kAddition", "dd/dc8/G4MergeMode_8hh.html#a7a098722b50c17a955e924689ab9c0eaa393c8e431a6db8fd89174a06e9a62ec2", null ],
      [ "kMultiplication", "dd/dc8/G4MergeMode_8hh.html#a7a098722b50c17a955e924689ab9c0eaa03a7d1283d02cb9b249bea0e42297aa9", null ],
      [ "kMaximum", "dd/dc8/G4MergeMode_8hh.html#a7a098722b50c17a955e924689ab9c0eaa60a57c154d1f0c59bdffe2483aab4698", null ],
      [ "kMinimum", "dd/dc8/G4MergeMode_8hh.html#a7a098722b50c17a955e924689ab9c0eaaace797734bf19015f65ec0e4c44c759d", null ]
    ] ],
    [ "GetMergeFunction", "dd/dc8/G4MergeMode_8hh.html#a82ace4133066d8c30aaca10d790a5d41", null ],
    [ "GetMergeFunction", "dd/dc8/G4MergeMode_8hh.html#a71b2103298ef78f8997bb0f10f12f1b4", null ],
    [ "GetMergeMode", "dd/dc8/G4MergeMode_8hh.html#a8cb0ecf6db7d8de397cbb8177f30820a", null ]
];