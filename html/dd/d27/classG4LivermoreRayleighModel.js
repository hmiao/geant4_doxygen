var classG4LivermoreRayleighModel =
[
    [ "G4LivermoreRayleighModel", "dd/d27/classG4LivermoreRayleighModel.html#a87e2ffe1c196232d29936fa2449f2b26", null ],
    [ "~G4LivermoreRayleighModel", "dd/d27/classG4LivermoreRayleighModel.html#a5ed65407b1b37cf690778a3dae68cf86", null ],
    [ "G4LivermoreRayleighModel", "dd/d27/classG4LivermoreRayleighModel.html#a200680bff49156ba552e8d0c6e9874c2", null ],
    [ "ComputeCrossSectionPerAtom", "dd/d27/classG4LivermoreRayleighModel.html#a05b465e9c8c5f05123f09e90d458e0d9", null ],
    [ "Initialise", "dd/d27/classG4LivermoreRayleighModel.html#a8bbd3b5b3f9f0549e325019181946c85", null ],
    [ "InitialiseForElement", "dd/d27/classG4LivermoreRayleighModel.html#abbe9282845bf2804f9e0a8f068c3f289", null ],
    [ "InitialiseLocal", "dd/d27/classG4LivermoreRayleighModel.html#af31fd0ecbef4077c3a7e6c35934e8fa1", null ],
    [ "operator=", "dd/d27/classG4LivermoreRayleighModel.html#a375b29c79b6597dff210339498e79347", null ],
    [ "ReadData", "dd/d27/classG4LivermoreRayleighModel.html#afed0fea36368dc68dd76d85d11840624", null ],
    [ "SampleSecondaries", "dd/d27/classG4LivermoreRayleighModel.html#a6953f5a63e5bd1a00d642c3808fa25cd", null ],
    [ "SetLowEnergyThreshold", "dd/d27/classG4LivermoreRayleighModel.html#abbb3265154f12cedaa712705778da50c", null ],
    [ "dataCS", "dd/d27/classG4LivermoreRayleighModel.html#a095b1594168411f5da8ee646fa93d008", null ],
    [ "fParticleChange", "dd/d27/classG4LivermoreRayleighModel.html#ae9492aa445909918539e1812b289ed4a", null ],
    [ "isInitialised", "dd/d27/classG4LivermoreRayleighModel.html#ab357b4c95cfd446e0826d0b41d684574", null ],
    [ "lowEnergyLimit", "dd/d27/classG4LivermoreRayleighModel.html#ae992d58bbab42e6eb9505f79d3eb23f3", null ],
    [ "maxZ", "dd/d27/classG4LivermoreRayleighModel.html#aa601dcc37200aa647639654d8cb755ba", null ],
    [ "verboseLevel", "dd/d27/classG4LivermoreRayleighModel.html#a85c2f05a7ae098f5c89efe90ffc8ec3c", null ]
];