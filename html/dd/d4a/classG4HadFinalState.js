var classG4HadFinalState =
[
    [ "G4HadFinalState", "dd/d4a/classG4HadFinalState.html#a880f4fc86e8e29e4a98926d8e3010ef2", null ],
    [ "AddSecondaries", "dd/d4a/classG4HadFinalState.html#adbc944c7681b6a3abdbc3f811e98fdc8", null ],
    [ "AddSecondaries", "dd/d4a/classG4HadFinalState.html#ae0f1824de08cefcd62e42d75a22a0f1a", null ],
    [ "AddSecondaries", "dd/d4a/classG4HadFinalState.html#aae751999e084f7c40b68ba6e03cd335c", null ],
    [ "AddSecondary", "dd/d4a/classG4HadFinalState.html#a1303c3939e2acb9a93e60234d6d17955", null ],
    [ "AddSecondary", "dd/d4a/classG4HadFinalState.html#a65877381727fe2836e02093d5e22936d", null ],
    [ "Clear", "dd/d4a/classG4HadFinalState.html#a584bef16b8241ddcc76e1147b93226aa", null ],
    [ "ClearSecondaries", "dd/d4a/classG4HadFinalState.html#a4fcced95a5098f4c6711c34ba23e3af5", null ],
    [ "GetEnergyChange", "dd/d4a/classG4HadFinalState.html#a26ed16e7457b1d43f0802dfda56fce0b", null ],
    [ "GetLocalEnergyDeposit", "dd/d4a/classG4HadFinalState.html#a894ebad9d0cb14c92371553722e794cd", null ],
    [ "GetMomentumChange", "dd/d4a/classG4HadFinalState.html#a956024927ab691e9fffbe902323ac66b", null ],
    [ "GetNumberOfSecondaries", "dd/d4a/classG4HadFinalState.html#aafa9d5b8043d63d0a8906cdc294dc480", null ],
    [ "GetSecondary", "dd/d4a/classG4HadFinalState.html#ac374b2820481fe0a0a7a99d014567481", null ],
    [ "GetSecondary", "dd/d4a/classG4HadFinalState.html#af72f4bb87f65c9d8db58628f9a2c2401", null ],
    [ "GetStatusChange", "dd/d4a/classG4HadFinalState.html#a533f140aeb55145b7e79441aa2ec9865", null ],
    [ "GetTrafoToLab", "dd/d4a/classG4HadFinalState.html#a433103f59d5190dfa66827f1ac159c86", null ],
    [ "GetWeightChange", "dd/d4a/classG4HadFinalState.html#ae8d949c341ee2966aeb0d9d469d3bbb3", null ],
    [ "SetEnergyChange", "dd/d4a/classG4HadFinalState.html#ab8148d260bb9eadce6f185dad3711fb1", null ],
    [ "SetLocalEnergyDeposit", "dd/d4a/classG4HadFinalState.html#af3a6300687c0b5db4ac91266437a03d4", null ],
    [ "SetMomentumChange", "dd/d4a/classG4HadFinalState.html#adf06912ba5818e24b9e26f433eb6149b", null ],
    [ "SetMomentumChange", "dd/d4a/classG4HadFinalState.html#ac8057a2cd7192c7035f12eef27badd4d", null ],
    [ "SetStatusChange", "dd/d4a/classG4HadFinalState.html#a3c625ad6d42b3e9533024bc0e7f355c7", null ],
    [ "SetTrafoToLab", "dd/d4a/classG4HadFinalState.html#a78b3d16ac682364135a9f91097c54d68", null ],
    [ "SetWeightChange", "dd/d4a/classG4HadFinalState.html#a469e56222c9708276dafa4b84239979a", null ],
    [ "theDirection", "dd/d4a/classG4HadFinalState.html#a49f5d73ebd36272b5baa41a2bd19c8fc", null ],
    [ "theEDep", "dd/d4a/classG4HadFinalState.html#ae32a1f492245137ce68acabf7c80eb41", null ],
    [ "theEnergy", "dd/d4a/classG4HadFinalState.html#a53a79cc87cff51d29b1987c95b5e9efd", null ],
    [ "theSecs", "dd/d4a/classG4HadFinalState.html#afade09e78b245df584403227e162298d", null ],
    [ "theStat", "dd/d4a/classG4HadFinalState.html#a2f2f37c22baed283792d2312557ffe71", null ],
    [ "theT", "dd/d4a/classG4HadFinalState.html#a1e8e610b204bd9545e48e98881616e56", null ],
    [ "theW", "dd/d4a/classG4HadFinalState.html#a1c06a2a7d0bf52922f2d6d8303730d98", null ]
];