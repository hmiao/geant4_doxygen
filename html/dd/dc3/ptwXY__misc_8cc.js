var ptwXY__misc_8cc =
[
    [ "ptwXY_applyFunction", "dd/dc3/ptwXY__misc_8cc.html#ad0001b945b819d8fda59ee968cfa6969", null ],
    [ "ptwXY_applyFunction2", "dd/dc3/ptwXY__misc_8cc.html#a26e3ca39dba31c5adadc53262e36680f", null ],
    [ "ptwXY_applyFunctionZeroCrossing", "dd/dc3/ptwXY__misc_8cc.html#a12bebe5a6cce1bf219ec7ffea38226f0", null ],
    [ "ptwXY_createFromFunction", "dd/dc3/ptwXY__misc_8cc.html#a7d421aa07329c56d9b9c51cb29decf8e", null ],
    [ "ptwXY_createFromFunction2", "dd/dc3/ptwXY__misc_8cc.html#ad053489f3025bc17b9d24c22e428c2b3", null ],
    [ "ptwXY_createFromFunctionBisect", "dd/dc3/ptwXY__misc_8cc.html#ac7b606170384489c45f05c7b50bf7f23", null ],
    [ "ptwXY_createFromFunctionZeroCrossing", "dd/dc3/ptwXY__misc_8cc.html#a7631e0ea4a617c55072af58238cec1dd", null ],
    [ "ptwXY_fromString", "dd/dc3/ptwXY__misc_8cc.html#aa03645470496005c1ff0e58e968fe2a3", null ],
    [ "ptwXY_showInteralStructure", "dd/dc3/ptwXY__misc_8cc.html#a1fcef55077c36e5679db2a664fd1c576", null ],
    [ "ptwXY_simplePrint", "dd/dc3/ptwXY__misc_8cc.html#abb38389e5a809ad9e832e02f3f913f34", null ],
    [ "ptwXY_simpleWrite", "dd/dc3/ptwXY__misc_8cc.html#a13091d6fbecd25387d791f94c5092b9d", null ],
    [ "ptwXY_update_biSectionMax", "dd/dc3/ptwXY__misc_8cc.html#a998a235f3f80a3345a774b5384ee0fb2", null ]
];