var G4Octree_8hh =
[
    [ "G4Octree< Iterator, Extractor, Point >", "d5/d6d/classG4Octree.html", "d5/d6d/classG4Octree" ],
    [ "G4Octree< Iterator, Extractor, Point >::LeafValues", "de/d8e/structG4Octree_1_1LeafValues.html", "de/d8e/structG4Octree_1_1LeafValues" ],
    [ "G4Octree< Iterator, Extractor, Point >::Node", "db/de0/classG4Octree_1_1Node.html", "db/de0/classG4Octree_1_1Node" ],
    [ "G4Octree< Iterator, Extractor, Point >::Node::InnerIterator", "d3/d1a/structG4Octree_1_1Node_1_1InnerIterator.html", "d3/d1a/structG4Octree_1_1Node_1_1InnerIterator" ],
    [ "max_depth", "dd/df9/G4Octree_8hh.html#a631f89d09af0e0afdb81270a4c8d407c", null ],
    [ "max_per_node", "dd/df9/G4Octree_8hh.html#a2b667e90fd56d3eaa628345585a6ef0d", null ]
];