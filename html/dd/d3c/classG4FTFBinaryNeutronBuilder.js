var classG4FTFBinaryNeutronBuilder =
[
    [ "G4FTFBinaryNeutronBuilder", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#a15e3663edae8d53c41255b35f9108707", null ],
    [ "~G4FTFBinaryNeutronBuilder", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#ad0024aae1922d6bc7a42f8a5612479fa", null ],
    [ "Build", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#ae05917232521c7df6bd87220972824c3", null ],
    [ "Build", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#a238843bcd519c608824d522800fbf871", null ],
    [ "Build", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#ad22aba97ae855d64649dce3e11fe9d8b", null ],
    [ "Build", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#af0bbb5ef1c2b46cc293d70e6453f07aa", null ],
    [ "Build", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#a5f77662cfc03070799fe65429d6f2201", null ],
    [ "Build", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#a64b6f48a5a6f37182c3e4bb905aa41ec", null ],
    [ "Build", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#a0349732dc3d8e1ba9b9a1cb144f06587", null ],
    [ "Build", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#a0e838ec760d7c34c450fbe14723df255", null ],
    [ "SetMaxEnergy", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#ad1ea5e63faca42bc9120a9cc4e172af7", null ],
    [ "SetMinEnergy", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#a7c9f85905d5187f5b7ddd56c51ae0c5e", null ],
    [ "theMax", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#affdbcc4bb350eca3e304632b569ec07c", null ],
    [ "theMin", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#a69dcf019be871b6dcc751282efcd54bb", null ],
    [ "theModel", "dd/d3c/classG4FTFBinaryNeutronBuilder.html#a2a45346a634e5525b6390b66a0673fbe", null ]
];