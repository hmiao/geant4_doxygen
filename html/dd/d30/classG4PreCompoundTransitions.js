var classG4PreCompoundTransitions =
[
    [ "G4PreCompoundTransitions", "dd/d30/classG4PreCompoundTransitions.html#a742d051c655b4bf3bb3d4e9123304ac3", null ],
    [ "~G4PreCompoundTransitions", "dd/d30/classG4PreCompoundTransitions.html#a529fbe45019ef5cffee0da9bc32c79d0", null ],
    [ "G4PreCompoundTransitions", "dd/d30/classG4PreCompoundTransitions.html#a7fd8c1f980ab584dcee06137708e40c6", null ],
    [ "CalculateProbability", "dd/d30/classG4PreCompoundTransitions.html#a3a23ce0320063c13d6662747d6d257e0", null ],
    [ "operator!=", "dd/d30/classG4PreCompoundTransitions.html#a82f3d6e0bde011c478dd954679c09d25", null ],
    [ "operator=", "dd/d30/classG4PreCompoundTransitions.html#adb5bb047e3ac91704f1965e3c71326c8", null ],
    [ "operator==", "dd/d30/classG4PreCompoundTransitions.html#a4f38f6d9689df173a8464f871aec8ac2", null ],
    [ "PerformTransition", "dd/d30/classG4PreCompoundTransitions.html#a7637a44a764173a79f73039a4d4b8e9f", null ],
    [ "FermiEnergy", "dd/d30/classG4PreCompoundTransitions.html#a80442d115caf8d9cda63a16d54ddbf36", null ],
    [ "fNuclData", "dd/d30/classG4PreCompoundTransitions.html#a30deccbcfcbab033e016ee3e38e2fcfc", null ],
    [ "proton", "dd/d30/classG4PreCompoundTransitions.html#af5d954199619e16fb63947db514ad71f", null ],
    [ "r0", "dd/d30/classG4PreCompoundTransitions.html#ac7d693d18dba0a6978a319db28df2602", null ]
];