var G4PhysicsVectorType_8hh =
[
    [ "G4PhysicsVectorType", "dd/de7/G4PhysicsVectorType_8hh.html#ab21ec21f7c0bc723f59585d143145f4e", [
      [ "T_G4PhysicsFreeVector", "dd/de7/G4PhysicsVectorType_8hh.html#ab21ec21f7c0bc723f59585d143145f4ea4f469ecca7216447f1c14e698030c8e3", null ],
      [ "T_G4PhysicsLinearVector", "dd/de7/G4PhysicsVectorType_8hh.html#ab21ec21f7c0bc723f59585d143145f4ea936df567891da29563005fcc676481f3", null ],
      [ "T_G4PhysicsLogVector", "dd/de7/G4PhysicsVectorType_8hh.html#ab21ec21f7c0bc723f59585d143145f4eaa5bb80a07973641ee7a82fbdcb293551", null ]
    ] ],
    [ "G4SplineType", "dd/de7/G4PhysicsVectorType_8hh.html#a4194850dabe744d6103cb09a3b894e2e", [
      [ "Simple", "dd/de7/G4PhysicsVectorType_8hh.html#a4194850dabe744d6103cb09a3b894e2ea1fbb1e3943c2c6c560247ac8f9289780", null ],
      [ "Base", "dd/de7/G4PhysicsVectorType_8hh.html#a4194850dabe744d6103cb09a3b894e2ea095a1b43effec73955e31e790438de49", null ],
      [ "FixedEdges", "dd/de7/G4PhysicsVectorType_8hh.html#a4194850dabe744d6103cb09a3b894e2ea7af5ceb036a0df5d20e99136b4588f6d", null ]
    ] ]
];