var classG4IntersectionSolid =
[
    [ "G4IntersectionSolid", "dd/dcd/classG4IntersectionSolid.html#a76c8b6f7d7fcaa31bfab58cba843259d", null ],
    [ "G4IntersectionSolid", "dd/dcd/classG4IntersectionSolid.html#a0b15a580c5991aa47d3489e12d11e586", null ],
    [ "G4IntersectionSolid", "dd/dcd/classG4IntersectionSolid.html#ae3fe8b7a95ae2b323f795a57798a5dcd", null ],
    [ "~G4IntersectionSolid", "dd/dcd/classG4IntersectionSolid.html#a530fab4099836192d82ded000deea7c7", null ],
    [ "G4IntersectionSolid", "dd/dcd/classG4IntersectionSolid.html#a3039e075b770034b9bc9742abbaf31fc", null ],
    [ "G4IntersectionSolid", "dd/dcd/classG4IntersectionSolid.html#a9146d6cdf4e36adc43694342daad2eaf", null ],
    [ "BoundingLimits", "dd/dcd/classG4IntersectionSolid.html#ae4df9ebd632ff38c4c746f0b826f5443", null ],
    [ "CalculateExtent", "dd/dcd/classG4IntersectionSolid.html#a51574f5dcf219fdd4feb230498569bec", null ],
    [ "Clone", "dd/dcd/classG4IntersectionSolid.html#a49b3dc8e57b248e5e78b7c1f126ef812", null ],
    [ "ComputeDimensions", "dd/dcd/classG4IntersectionSolid.html#aa951a9cc05ebdb9499d7cde124f1e444", null ],
    [ "CreatePolyhedron", "dd/dcd/classG4IntersectionSolid.html#a806ee38001cfa9b65863572c5c00ffa2", null ],
    [ "DescribeYourselfTo", "dd/dcd/classG4IntersectionSolid.html#a877ff3a8fafa9e6c2d70d6005b04c289", null ],
    [ "DistanceToIn", "dd/dcd/classG4IntersectionSolid.html#ad4e37ab4504260b213e3c6d0ac38692f", null ],
    [ "DistanceToIn", "dd/dcd/classG4IntersectionSolid.html#a66d379e13395e17b5d4dbda37cf40f32", null ],
    [ "DistanceToOut", "dd/dcd/classG4IntersectionSolid.html#a710d832a0bbd6b2b80c9e03fea806356", null ],
    [ "DistanceToOut", "dd/dcd/classG4IntersectionSolid.html#a30a9eea1ef76ac3c57a3b5df11c7af2c", null ],
    [ "GetEntityType", "dd/dcd/classG4IntersectionSolid.html#a7d1dfd34d6aa502ba081bcb19eb162ac", null ],
    [ "Inside", "dd/dcd/classG4IntersectionSolid.html#a873fd51ca9296856fafcb21ce2bb1ec1", null ],
    [ "operator=", "dd/dcd/classG4IntersectionSolid.html#a746fb288e1dd4f02d58596b82987a2f4", null ],
    [ "SurfaceNormal", "dd/dcd/classG4IntersectionSolid.html#a2985c772617be374606ff79db80ee8d1", null ]
];