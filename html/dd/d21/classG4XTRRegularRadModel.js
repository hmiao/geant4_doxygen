var classG4XTRRegularRadModel =
[
    [ "G4XTRRegularRadModel", "dd/d21/classG4XTRRegularRadModel.html#a86fbbae2e735b057c4d0365ae8416d75", null ],
    [ "~G4XTRRegularRadModel", "dd/d21/classG4XTRRegularRadModel.html#a957a78d32508f3d8172c4eb7e9486f35", null ],
    [ "DumpInfo", "dd/d21/classG4XTRRegularRadModel.html#aea5158395e954092b6fdf6638e0109c1", null ],
    [ "GetStackFactor", "dd/d21/classG4XTRRegularRadModel.html#a9416297c49375b8907be237cf3dc0cac", null ],
    [ "ProcessDescription", "dd/d21/classG4XTRRegularRadModel.html#a810845925ee74f5691f341785f195393", null ],
    [ "SpectralXTRdEdx", "dd/d21/classG4XTRRegularRadModel.html#a71b2495e68c5e72573cac71b484a6336", null ]
];