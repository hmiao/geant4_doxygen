var classG4VAngularDistribution =
[
    [ "G4VAngularDistribution", "d5/d79/classG4VAngularDistribution.html#ae7940aa23b64e9bd705c73746115b259", null ],
    [ "~G4VAngularDistribution", "d5/d79/classG4VAngularDistribution.html#a24128ccc1b70e4ba6aa9666a70ec20f0", null ],
    [ "CosTheta", "d5/d79/classG4VAngularDistribution.html#aa601b9464c0d51d3a6ebb229711b7e25", null ],
    [ "Phi", "d5/d79/classG4VAngularDistribution.html#a858a54e9a6c970fe0ff8165b1b753810", null ]
];