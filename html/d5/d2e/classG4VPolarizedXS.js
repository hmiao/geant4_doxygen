var classG4VPolarizedXS =
[
    [ "G4VPolarizedXS", "d5/d2e/classG4VPolarizedXS.html#a209c22860c66d3afe5c98c4af9a2985b", null ],
    [ "~G4VPolarizedXS", "d5/d2e/classG4VPolarizedXS.html#aae4d7850f848c2218156ef2986804de2", null ],
    [ "G4VPolarizedXS", "d5/d2e/classG4VPolarizedXS.html#a74cbf01d432d3cc9542ad4ccabbb1d9a", null ],
    [ "GetPol2", "d5/d2e/classG4VPolarizedXS.html#ab6b7f4e514dbf8d20dfde949ec207f02", null ],
    [ "GetPol3", "d5/d2e/classG4VPolarizedXS.html#ad13ae8b7b6d33a497b4bae55e43fe880", null ],
    [ "GetXmax", "d5/d2e/classG4VPolarizedXS.html#a9d78afd4bf726e6b19f3f0d1c313d7ba", null ],
    [ "GetXmin", "d5/d2e/classG4VPolarizedXS.html#a89d1286312c7c30585d589b3f24dc12a", null ],
    [ "GetYmin", "d5/d2e/classG4VPolarizedXS.html#a2b7be8bc0459581a9d45531e530ee090", null ],
    [ "Initialize", "d5/d2e/classG4VPolarizedXS.html#a03a19e4b92dcde7b6be5d61e5e28e162", null ],
    [ "operator=", "d5/d2e/classG4VPolarizedXS.html#a0b990310c0baf4c55da370d1502db021", null ],
    [ "SetMaterial", "d5/d2e/classG4VPolarizedXS.html#aea27cb54da8693e555767f2da9132829", null ],
    [ "SetXmax", "d5/d2e/classG4VPolarizedXS.html#a9948993e3bc4ec21ba737d8ba041927c", null ],
    [ "SetXmin", "d5/d2e/classG4VPolarizedXS.html#a83f499ef784b8250d3d9da0f675b71ca", null ],
    [ "SetYmin", "d5/d2e/classG4VPolarizedXS.html#af951c7d3386553e22d72f9fc51f862d0", null ],
    [ "TotalXSection", "d5/d2e/classG4VPolarizedXS.html#a84f4a5f0f9bc30d8b2a6a9d21ca92a17", null ],
    [ "XSection", "d5/d2e/classG4VPolarizedXS.html#a780908e46d1c7c661e7d2e43a7729583", null ],
    [ "fA", "d5/d2e/classG4VPolarizedXS.html#ac0d0ecdccb73b21669b4dfd13ea865ac", null ],
    [ "fCoul", "d5/d2e/classG4VPolarizedXS.html#a4da7497d28a31a394641fa27f97f24a3", null ],
    [ "fXmax", "d5/d2e/classG4VPolarizedXS.html#aa155d2b8baa267e5071fa0ea9b05473f", null ],
    [ "fXmin", "d5/d2e/classG4VPolarizedXS.html#a50f6ae417520767dcea985390a75bc17", null ],
    [ "fYmin", "d5/d2e/classG4VPolarizedXS.html#a1046f7c7e792446df9d095533684965c", null ],
    [ "fZ", "d5/d2e/classG4VPolarizedXS.html#ae2bc92d6120e64fe2c2eed89bc151086", null ]
];