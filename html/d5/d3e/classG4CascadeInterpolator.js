var classG4CascadeInterpolator =
[
    [ "G4CascadeInterpolator", "d5/d3e/classG4CascadeInterpolator.html#a4c4cd4074d5e993be618da35ae399125", null ],
    [ "~G4CascadeInterpolator", "d5/d3e/classG4CascadeInterpolator.html#a07936f71992218510173215f80df107a", null ],
    [ "getBin", "d5/d3e/classG4CascadeInterpolator.html#af225457602a088411980e0b6ee5a9f8d", null ],
    [ "interpolate", "d5/d3e/classG4CascadeInterpolator.html#a5a150c14e526c6fd7128d9ee78b476d7", null ],
    [ "interpolate", "d5/d3e/classG4CascadeInterpolator.html#afa15293098f150582c069ec67f93555c", null ],
    [ "printBins", "d5/d3e/classG4CascadeInterpolator.html#aefeb260f8fba71c1435c11a3dc2c2526", null ],
    [ "doExtrapolation", "d5/d3e/classG4CascadeInterpolator.html#a4e2ab85bdea4430414ada8137abbc80f", null ],
    [ "lastVal", "d5/d3e/classG4CascadeInterpolator.html#ac09a15585ef8cc56b351faf7b09b5642", null ],
    [ "lastX", "d5/d3e/classG4CascadeInterpolator.html#a31f795f6ff738a5b6fe1d9d65bf7e9b5", null ],
    [ "xBins", "d5/d3e/classG4CascadeInterpolator.html#a1d893e76537e857f243d9381fa7094da", null ]
];