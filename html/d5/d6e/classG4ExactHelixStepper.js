var classG4ExactHelixStepper =
[
    [ "G4ExactHelixStepper", "d5/d6e/classG4ExactHelixStepper.html#a0adae7d129cf01e470e1460c33adda2b", null ],
    [ "~G4ExactHelixStepper", "d5/d6e/classG4ExactHelixStepper.html#aae0ffd6f46e2a85cd811c4c3fc3adb6d", null ],
    [ "G4ExactHelixStepper", "d5/d6e/classG4ExactHelixStepper.html#a70a598d837712ac4e4a48d86fdb8a615", null ],
    [ "DistChord", "d5/d6e/classG4ExactHelixStepper.html#afe8fbc20c72126b53c0a1deef3528e37", null ],
    [ "DumbStepper", "d5/d6e/classG4ExactHelixStepper.html#a35555cbea39421ec7bfb9bfbb3bbe3d6", null ],
    [ "IntegratorOrder", "d5/d6e/classG4ExactHelixStepper.html#a0085d2f95cd0008b8b4f580d12f334db", null ],
    [ "operator=", "d5/d6e/classG4ExactHelixStepper.html#aef71c3f6e8af94ae207da6132f7ede3f", null ],
    [ "Stepper", "d5/d6e/classG4ExactHelixStepper.html#aa9d96b6473dac229b8bf7bb81f2ca243", null ],
    [ "fBfieldValue", "d5/d6e/classG4ExactHelixStepper.html#ad51c434683f29f0629c1a6d73cd51dec", null ]
];