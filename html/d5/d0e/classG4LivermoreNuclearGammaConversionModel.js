var classG4LivermoreNuclearGammaConversionModel =
[
    [ "G4LivermoreNuclearGammaConversionModel", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#ad663021f41330d99fd8244188f714220", null ],
    [ "~G4LivermoreNuclearGammaConversionModel", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#a4b3f9c2a161ae462ef4f1d0fc15b4c99", null ],
    [ "G4LivermoreNuclearGammaConversionModel", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#a426a47010acc05f64f9de90865d52d67", null ],
    [ "ComputeCrossSectionPerAtom", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#a7ab702671aece20a644f26491d6cf672", null ],
    [ "Initialise", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#adfeb54d794b9b34cae69dcd906b735c9", null ],
    [ "InitialiseForElement", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#aa238792e64e396ac0e08668b3ed61d23", null ],
    [ "InitialiseLocal", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#ab34bff7834a192b213b872ff2e617f3a", null ],
    [ "MinPrimaryEnergy", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#a8b6f1ba2b20862ff28258e6efa4059f7", null ],
    [ "operator=", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#a6f567607dc345da551c982771fbe25d7", null ],
    [ "ReadData", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#ad161d35623d78390023ce84130023455", null ],
    [ "SampleSecondaries", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#a0ce48875f300c82e57ad303c87ee74bf", null ],
    [ "ScreenFunction1", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#a5bb53afc23036d49cfe5d88c7f1d88a5", null ],
    [ "ScreenFunction2", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#a15ec295457a565d3582bc67263bf623f", null ],
    [ "data", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#a31370f11892b124d45b8ecda67d0fdc2", null ],
    [ "fParticleChange", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#a944dd368f67cad894a50956beae54397", null ],
    [ "isInitialised", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#ad661e10740a0c9ea32898f6a1aeae595", null ],
    [ "lowEnergyLimit", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#aa7fb311a482662ef453e9a70f373dff8", null ],
    [ "maxZ", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#a48c4c59ffa9682bcbe28e65e743766bd", null ],
    [ "smallEnergy", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#a0fe1dacb4b35c7b1a422fd415cf4837d", null ],
    [ "verboseLevel", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html#aca05a6b4e1de7975b71023d2affe0010", null ]
];