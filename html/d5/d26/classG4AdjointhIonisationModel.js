var classG4AdjointhIonisationModel =
[
    [ "G4AdjointhIonisationModel", "d5/d26/classG4AdjointhIonisationModel.html#ad051b177fbfb40e4711a9e6d88a497a5", null ],
    [ "~G4AdjointhIonisationModel", "d5/d26/classG4AdjointhIonisationModel.html#aa2242451e9162715f0c016f92c185f9c", null ],
    [ "G4AdjointhIonisationModel", "d5/d26/classG4AdjointhIonisationModel.html#abe4688dc798e0cbff205b2a9e039a961", null ],
    [ "AdjointCrossSection", "d5/d26/classG4AdjointhIonisationModel.html#a0e5fe172a8fcab233a2446a2ae8efcbe", null ],
    [ "DefineProjectileProperty", "d5/d26/classG4AdjointhIonisationModel.html#a3dd6013d5464c2a7803231eb9285a2d0", null ],
    [ "DiffCrossSectionPerAtomPrimToSecond", "d5/d26/classG4AdjointhIonisationModel.html#a44ed4017932f0b79f58bf96dcb1beaa1", null ],
    [ "GetSecondAdjEnergyMaxForProdToProj", "d5/d26/classG4AdjointhIonisationModel.html#afdddceeeb352cd26a89879c5a40d5e0e", null ],
    [ "GetSecondAdjEnergyMaxForScatProjToProj", "d5/d26/classG4AdjointhIonisationModel.html#a737b3e7b7b5bf58fe9d0e49a08d2367b", null ],
    [ "GetSecondAdjEnergyMinForProdToProj", "d5/d26/classG4AdjointhIonisationModel.html#aa23156a3729a90f6f635588a1defad63", null ],
    [ "GetSecondAdjEnergyMinForScatProjToProj", "d5/d26/classG4AdjointhIonisationModel.html#a1643a7fe538907ac3357ed1c5b26662a", null ],
    [ "operator=", "d5/d26/classG4AdjointhIonisationModel.html#aa0c99533b24279363f0755be89663617", null ],
    [ "RapidSampleSecondaries", "d5/d26/classG4AdjointhIonisationModel.html#a680e7d6b6e4a55c7e3e936a213dc581c", null ],
    [ "SampleSecondaries", "d5/d26/classG4AdjointhIonisationModel.html#a9c6dc2ba0bb164a800f05f1ad6bca0aa", null ],
    [ "fBraggDirectEMModel", "d5/d26/classG4AdjointhIonisationModel.html#af36fb8e416ddd83b868baad6a350c719", null ],
    [ "fFormFact", "d5/d26/classG4AdjointhIonisationModel.html#ab0adfd8f5bdd397c852c0c9f627ad9a9", null ],
    [ "fMagMoment2", "d5/d26/classG4AdjointhIonisationModel.html#abf720a41ef40c13e4507402dc3518c91", null ],
    [ "fMass", "d5/d26/classG4AdjointhIonisationModel.html#a807398df25cf137c2ad8877a9ac9e5b9", null ],
    [ "fMassRatio", "d5/d26/classG4AdjointhIonisationModel.html#abb3a8d857a5f0b910df9817a1ff9d2ed", null ],
    [ "fOneMinusRatio2", "d5/d26/classG4AdjointhIonisationModel.html#a2ce7f2ed57a21947f68a4245810de81e", null ],
    [ "fOnePlusRatio2", "d5/d26/classG4AdjointhIonisationModel.html#a83ec32074ab2ac004f87642f30a75d26", null ],
    [ "fSpin", "d5/d26/classG4AdjointhIonisationModel.html#a4cdc827c2d1f7b0bf1dbf6d21c48aae6", null ]
];