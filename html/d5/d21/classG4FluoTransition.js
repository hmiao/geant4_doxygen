var classG4FluoTransition =
[
    [ "G4FluoTransition", "d5/d21/classG4FluoTransition.html#a0e5548f4d33d8ca2cd3b6da0951f4399", null ],
    [ "~G4FluoTransition", "d5/d21/classG4FluoTransition.html#a3288402e8e302588b0c8b0481ab5c0a0", null ],
    [ "FinalShellId", "d5/d21/classG4FluoTransition.html#af429061bf768422acfe80ce6fbe07fbc", null ],
    [ "OriginatingShellId", "d5/d21/classG4FluoTransition.html#a5d0e65613b0986dc401664eff8a8ef9e", null ],
    [ "OriginatingShellIds", "d5/d21/classG4FluoTransition.html#a2067bb8b0a06e88ad7fab0826b040c92", null ],
    [ "TransitionEnergies", "d5/d21/classG4FluoTransition.html#ad326905750532d82fef40d3e1d453119", null ],
    [ "TransitionEnergy", "d5/d21/classG4FluoTransition.html#ab45dc1f7cc8419274b310688979471f6", null ],
    [ "TransitionProbabilities", "d5/d21/classG4FluoTransition.html#a4cd2636a2840b5104e3a821cccaf07da", null ],
    [ "TransitionProbability", "d5/d21/classG4FluoTransition.html#ac7df5d6f637165c83b1646de12bb8bfc", null ],
    [ "finalShellId", "d5/d21/classG4FluoTransition.html#aec23fdfc22909e167d4a48c3dd942b75", null ],
    [ "originatingShellIds", "d5/d21/classG4FluoTransition.html#a9b7f542443f6c2cc21923582f9fcc7b0", null ],
    [ "transitionEnergies", "d5/d21/classG4FluoTransition.html#aa72530f3eac1698e1d65f512b4a9684c", null ],
    [ "transitionProbabilities", "d5/d21/classG4FluoTransition.html#ad5fb6048c65caedb1b7ea6fb88546cf0", null ]
];