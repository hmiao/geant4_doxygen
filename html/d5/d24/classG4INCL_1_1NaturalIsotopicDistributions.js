var classG4INCL_1_1NaturalIsotopicDistributions =
[
    [ "NaturalIsotopicDistributions", "d5/d24/classG4INCL_1_1NaturalIsotopicDistributions.html#a2fe4388b8830ed5ac044508eacf7b175", null ],
    [ "drawRandomIsotope", "d5/d24/classG4INCL_1_1NaturalIsotopicDistributions.html#a21f710126c24aaaec800b280b42c68ac", null ],
    [ "getIsotopicDistribution", "d5/d24/classG4INCL_1_1NaturalIsotopicDistributions.html#a519f951c2995da546ed74e151982b80c", null ],
    [ "theDistributions", "d5/d24/classG4INCL_1_1NaturalIsotopicDistributions.html#aa8d59fc3ec892622130c3406e16d23a3", null ]
];