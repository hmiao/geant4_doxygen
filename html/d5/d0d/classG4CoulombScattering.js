var classG4CoulombScattering =
[
    [ "G4CoulombScattering", "d5/d0d/classG4CoulombScattering.html#a78d101ba158486ecac8e11c0e00cfcd1", null ],
    [ "~G4CoulombScattering", "d5/d0d/classG4CoulombScattering.html#a95b412716316670dabdf48fa7ff50f6b", null ],
    [ "G4CoulombScattering", "d5/d0d/classG4CoulombScattering.html#af922cfacf18dccb405607942d3348283", null ],
    [ "InitialiseProcess", "d5/d0d/classG4CoulombScattering.html#ae548088a4aad0932c9b17187b8453ce2", null ],
    [ "IsApplicable", "d5/d0d/classG4CoulombScattering.html#a5ac103074f0fdc29a32dbf86c839f7ef", null ],
    [ "MinPrimaryEnergy", "d5/d0d/classG4CoulombScattering.html#adec3f7cd9cf66d5cb6d8d5adf9ee9b7b", null ],
    [ "operator=", "d5/d0d/classG4CoulombScattering.html#a9f53f9ac865db51393e7c1769347112d", null ],
    [ "ProcessDescription", "d5/d0d/classG4CoulombScattering.html#a80cb5336a878e9f247120f80a2c2971d", null ],
    [ "StreamProcessInfo", "d5/d0d/classG4CoulombScattering.html#a8dc64e7dc6b0f9311bbc559a20f86417", null ],
    [ "isInitialised", "d5/d0d/classG4CoulombScattering.html#aad21ea6e7e33bf2b21e88a980a659897", null ],
    [ "q2Max", "d5/d0d/classG4CoulombScattering.html#a56b639c74255760f0b8f804325221d4b", null ]
];