var structE__P__E__isoAng =
[
    [ "E_P_E_isoAng", "d5/d47/structE__P__E__isoAng.html#aa2b1dc68418155d65b6738556d666dd0", null ],
    [ "energy", "d5/d47/structE__P__E__isoAng.html#aa7e78f0302dc64451c92442e0439943b", null ],
    [ "n", "d5/d47/structE__P__E__isoAng.html#a88e4a82c167b437fdfbd516f664e68ec", null ],
    [ "prob", "d5/d47/structE__P__E__isoAng.html#a2a82b4682820e75921e3b593ac94ced4", null ],
    [ "secondary_energy_cdf", "d5/d47/structE__P__E__isoAng.html#add6a4b9a79bb9a1e7028b6de2a3bdf09", null ],
    [ "secondary_energy_cdf_size", "d5/d47/structE__P__E__isoAng.html#acbf8f5c45410a0a7c340c83333011fc5", null ],
    [ "secondary_energy_pdf", "d5/d47/structE__P__E__isoAng.html#a939b6a36c6b262e7085c69c500c894e7", null ],
    [ "secondary_energy_value", "d5/d47/structE__P__E__isoAng.html#ae20da842250865c73a49bf25084b52bb", null ],
    [ "sum_of_probXdEs", "d5/d47/structE__P__E__isoAng.html#a8e17ab8a500849ec8a7cd241e0a05e79", null ],
    [ "vE_isoAngle", "d5/d47/structE__P__E__isoAng.html#a515a9772251ce2e4c56e8c35c3bc365b", null ]
];