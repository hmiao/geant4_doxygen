var classG4ElasticHNScattering =
[
    [ "G4ElasticHNScattering", "d5/dae/classG4ElasticHNScattering.html#adde22a2db77a1e89d5e37b5790a6ccf1", null ],
    [ "~G4ElasticHNScattering", "d5/dae/classG4ElasticHNScattering.html#a062716040e6fa07c38f4c927b567089f", null ],
    [ "G4ElasticHNScattering", "d5/dae/classG4ElasticHNScattering.html#add6364681ebaed50bd4628700de1f55b", null ],
    [ "ElasticScattering", "d5/dae/classG4ElasticHNScattering.html#af8b4658ceaf5a6d9795b7fec08632650", null ],
    [ "GaussianPt", "d5/dae/classG4ElasticHNScattering.html#a50b09853d7eb000cbac8036d32d248d0", null ],
    [ "operator!=", "d5/dae/classG4ElasticHNScattering.html#a616600e8bc1775cfd415bc43c805d6bd", null ],
    [ "operator=", "d5/dae/classG4ElasticHNScattering.html#a59b338099591810762b91531a1a0230a", null ],
    [ "operator==", "d5/dae/classG4ElasticHNScattering.html#a49f5a7e4ccd214c40fbb7cf859dba6cd", null ]
];