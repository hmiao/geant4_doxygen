var classG4OpticalParametersMessenger =
[
    [ "G4OpticalParametersMessenger", "d5/d2b/classG4OpticalParametersMessenger.html#ac027c1248892ac1bcf20880a3a6d4d1e", null ],
    [ "~G4OpticalParametersMessenger", "d5/d2b/classG4OpticalParametersMessenger.html#a456d0bbc6858fdc707e0cb7ec9d6c2ec", null ],
    [ "G4OpticalParametersMessenger", "d5/d2b/classG4OpticalParametersMessenger.html#ae20627c8ba7cbfc9a02b5f8dafce262a", null ],
    [ "G4OpticalParametersMessenger", "d5/d2b/classG4OpticalParametersMessenger.html#ac2769e1dd8796d0b45e03787b2e10622", null ],
    [ "operator=", "d5/d2b/classG4OpticalParametersMessenger.html#a1ad828b076e61a57b230c625a7eb8583", null ],
    [ "SetNewValue", "d5/d2b/classG4OpticalParametersMessenger.html#aa87eecf43f8572672898bd5d96f186e6", null ],
    [ "fAbsDir", "d5/d2b/classG4OpticalParametersMessenger.html#a6318965024c7350b9b1a7db83598b372", null ],
    [ "fAbsorptionVerboseLevelCmd", "d5/d2b/classG4OpticalParametersMessenger.html#ab2adf89753078053f3b4bb63e5a75298", null ],
    [ "fActivateProcessCmd", "d5/d2b/classG4OpticalParametersMessenger.html#a73c04ba83504c638428d7856448569a9", null ],
    [ "fBoundaryDir", "d5/d2b/classG4OpticalParametersMessenger.html#a915fd4dfcfd9643ba254565b2faf5cee", null ],
    [ "fBoundaryInvokeSDCmd", "d5/d2b/classG4OpticalParametersMessenger.html#a02ea215033c09a93e3f66184409562d7", null ],
    [ "fBoundaryVerboseLevelCmd", "d5/d2b/classG4OpticalParametersMessenger.html#aea978cb1e359bd42a423b4d467fa1e30", null ],
    [ "fCerenkovDir", "d5/d2b/classG4OpticalParametersMessenger.html#a5cee9e682fbf799c5371399f6382f72c", null ],
    [ "fCerenkovMaxBetaChangeCmd", "d5/d2b/classG4OpticalParametersMessenger.html#a48d16a74680333f358748d2f9bdd0278", null ],
    [ "fCerenkovMaxPhotonsCmd", "d5/d2b/classG4OpticalParametersMessenger.html#a2844cdd7ce6f2d9fdc606ef38a2a0639", null ],
    [ "fCerenkovStackPhotonsCmd", "d5/d2b/classG4OpticalParametersMessenger.html#ae55e851443aac4dcd7e6af15a2cb149b", null ],
    [ "fCerenkovTrackSecondariesFirstCmd", "d5/d2b/classG4OpticalParametersMessenger.html#a3f6a23b31aca61d930a31502b779567a", null ],
    [ "fCerenkovVerboseLevelCmd", "d5/d2b/classG4OpticalParametersMessenger.html#a5cde5205b177222e54a5c794b8eb5ae4", null ],
    [ "fDir", "d5/d2b/classG4OpticalParametersMessenger.html#ab4f6ff9c13bbe5f5843727733b97160e", null ],
    [ "fDumpCmd", "d5/d2b/classG4OpticalParametersMessenger.html#abceb09b8003beadafbbc6465b4571bc5", null ],
    [ "fMieDir", "d5/d2b/classG4OpticalParametersMessenger.html#a922002345bc77e5b0687146ab02f5361", null ],
    [ "fMieVerboseLevelCmd", "d5/d2b/classG4OpticalParametersMessenger.html#afdaece18b54d986351fbfe59721cc28a", null ],
    [ "fRaylDir", "d5/d2b/classG4OpticalParametersMessenger.html#aa8e2ed1e3343331cc3b014bebb7edf1f", null ],
    [ "fRayleighVerboseLevelCmd", "d5/d2b/classG4OpticalParametersMessenger.html#aff2586d55d69b82365531d3f79b237f5", null ],
    [ "fScintByParticleTypeCmd", "d5/d2b/classG4OpticalParametersMessenger.html#ab26a2590dcbaec11047d96c5964d20aa", null ],
    [ "fScintDir", "d5/d2b/classG4OpticalParametersMessenger.html#a7969c1e25ad362cdb3e140efd1890580", null ],
    [ "fScintFiniteRiseTimeCmd", "d5/d2b/classG4OpticalParametersMessenger.html#a25df9c407b308d28642a6cbafa7bcb39", null ],
    [ "fScintStackPhotonsCmd", "d5/d2b/classG4OpticalParametersMessenger.html#a0b11f8d86ef5fe004b79ae8be8eb0693", null ],
    [ "fScintTrackInfoCmd", "d5/d2b/classG4OpticalParametersMessenger.html#acf148959fbaacbd830c15ad16054b938", null ],
    [ "fScintTrackSecondariesFirstCmd", "d5/d2b/classG4OpticalParametersMessenger.html#a0b3122e7b75b37569fd24224b8ded770", null ],
    [ "fScintVerboseLevelCmd", "d5/d2b/classG4OpticalParametersMessenger.html#ae258b9c19fbb2cdbcb8f81402de92212", null ],
    [ "fVerboseCmd", "d5/d2b/classG4OpticalParametersMessenger.html#a90743562ce5b520730f313b501a5ae6e", null ],
    [ "fWls2Dir", "d5/d2b/classG4OpticalParametersMessenger.html#a92d340d17fd9cbd8d2fe58555629fd48", null ],
    [ "fWLS2TimeProfileCmd", "d5/d2b/classG4OpticalParametersMessenger.html#ae3c230d7da95c7d43b2a5c2a7ba86f75", null ],
    [ "fWLS2VerboseLevelCmd", "d5/d2b/classG4OpticalParametersMessenger.html#a3436aa3fcc3d572807613bad5771e62c", null ],
    [ "fWlsDir", "d5/d2b/classG4OpticalParametersMessenger.html#ad344c5998cff154fd9f953b11b0535f3", null ],
    [ "fWLSTimeProfileCmd", "d5/d2b/classG4OpticalParametersMessenger.html#ad9c42934e7b2e52aa5b5be729a0aca14", null ],
    [ "fWLSVerboseLevelCmd", "d5/d2b/classG4OpticalParametersMessenger.html#ab667e864d5df8782e942cb64357a677d", null ],
    [ "params", "d5/d2b/classG4OpticalParametersMessenger.html#aebfa4dc7bf09d4975170da39fcd69bd8", null ]
];