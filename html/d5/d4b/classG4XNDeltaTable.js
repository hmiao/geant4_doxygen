var classG4XNDeltaTable =
[
    [ "G4XNDeltaTable", "d5/d4b/classG4XNDeltaTable.html#a189f790c5d6db5c81536fc5093f3dbe6", null ],
    [ "~G4XNDeltaTable", "d5/d4b/classG4XNDeltaTable.html#a83e56a7767c35b5ac3661231682a6b53", null ],
    [ "G4XNDeltaTable", "d5/d4b/classG4XNDeltaTable.html#a17a5ba302606bf66fb585ce79140729c", null ],
    [ "CrossSectionTable", "d5/d4b/classG4XNDeltaTable.html#a1443ba313f6f07d6491d7b9ee3ef07cd", null ],
    [ "operator!=", "d5/d4b/classG4XNDeltaTable.html#aa8167844db849b37af91558036a8aeac", null ],
    [ "operator=", "d5/d4b/classG4XNDeltaTable.html#ac2dca16b63074ea3176d1e7198a755bb", null ],
    [ "operator==", "d5/d4b/classG4XNDeltaTable.html#a6c65253f1efa362bc350aed8b5c3b1c5", null ],
    [ "energyTable", "d5/d4b/classG4XNDeltaTable.html#aedcbf20833513c88dfba651f08f9e8d1", null ],
    [ "sigmaND1232", "d5/d4b/classG4XNDeltaTable.html#a133ef0c8ee71c7e9c594143307f0db85", null ],
    [ "size", "d5/d4b/classG4XNDeltaTable.html#a2118a23b1c693be9355eff7208f08afb", null ]
];