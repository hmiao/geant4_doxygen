var classG4GeneralNNCollision =
[
    [ "MakeNNStarToNN", "d9/dc8/structG4GeneralNNCollision_1_1MakeNNStarToNN.html", "d9/dc8/structG4GeneralNNCollision_1_1MakeNNStarToNN" ],
    [ "MakeNNToDeltaDelta", "d5/d93/structG4GeneralNNCollision_1_1MakeNNToDeltaDelta.html", "d5/d93/structG4GeneralNNCollision_1_1MakeNNToDeltaDelta" ],
    [ "MakeNNToDeltaNstar", "d6/d6d/structG4GeneralNNCollision_1_1MakeNNToDeltaNstar.html", "d6/d6d/structG4GeneralNNCollision_1_1MakeNNToDeltaNstar" ],
    [ "MakeNNToNDelta", "d4/dcc/structG4GeneralNNCollision_1_1MakeNNToNDelta.html", "d4/dcc/structG4GeneralNNCollision_1_1MakeNNToNDelta" ],
    [ "MakeNNToNNStar", "d2/d67/structG4GeneralNNCollision_1_1MakeNNToNNStar.html", "d2/d67/structG4GeneralNNCollision_1_1MakeNNToNNStar" ],
    [ "IsInCharge", "d5/d15/classG4GeneralNNCollision.html#adf121854630224c0f56f88d02dfce160", null ]
];