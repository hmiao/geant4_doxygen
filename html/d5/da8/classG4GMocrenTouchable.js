var classG4GMocrenTouchable =
[
    [ "G4GMocrenTouchable", "d5/da8/classG4GMocrenTouchable.html#a7c7b3fd0d21f431f9b522fc58ebe33b2", null ],
    [ "G4GMocrenTouchable", "d5/da8/classG4GMocrenTouchable.html#a0be49c69ab67c375bb37fbca106923fe", null ],
    [ "~G4GMocrenTouchable", "d5/da8/classG4GMocrenTouchable.html#ac97ab8a6a97f0e7f448eefdcd2f05000", null ],
    [ "GetReplicaNumber", "d5/da8/classG4GMocrenTouchable.html#ab28ae6cdd4ae72f5e63b38878d79d0d7", null ],
    [ "GetRotation", "d5/da8/classG4GMocrenTouchable.html#ae146248c7965457c24ab4856231cda6f", null ],
    [ "GetTranslation", "d5/da8/classG4GMocrenTouchable.html#a375554fa820767335ccac82531b13563", null ],
    [ "SetReplicaNumber", "d5/da8/classG4GMocrenTouchable.html#a27b8025d9b13180069e3b04b73c4b42c", null ],
    [ "repno", "d5/da8/classG4GMocrenTouchable.html#aaf56213c6b82b1ed4a628d2515af5604", null ]
];