var classG4ModifiedMephi =
[
    [ "G4ModifiedMephi", "d5/d37/classG4ModifiedMephi.html#a3fd2642a30637c57c2a9d1e7a9636d54", null ],
    [ "~G4ModifiedMephi", "d5/d37/classG4ModifiedMephi.html#af808c9078808b20ce56fb891e86ca4e9", null ],
    [ "G4ModifiedMephi", "d5/d37/classG4ModifiedMephi.html#ac90901f0a2a7c247cc3e6f35fb80c443", null ],
    [ "operator=", "d5/d37/classG4ModifiedMephi.html#a63e97995c23b9f8d9f69519b1f2f26ae", null ],
    [ "PrintGeneratorInformation", "d5/d37/classG4ModifiedMephi.html#a229c848c9760e86e4461ad45539c32d6", null ],
    [ "SampleCosTheta", "d5/d37/classG4ModifiedMephi.html#a315614551a3757ec5935dfb4dbae7d6c", null ],
    [ "SampleDirection", "d5/d37/classG4ModifiedMephi.html#a929312bed6dbf75d82ce9980d1344345", null ],
    [ "SamplePairDirections", "d5/d37/classG4ModifiedMephi.html#ab89265a2143a9b1de730d35ce82fb770", null ]
];