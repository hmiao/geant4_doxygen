var classG4VTwistSurface_1_1Boundary =
[
    [ "Boundary", "d5/d81/classG4VTwistSurface_1_1Boundary.html#afa1fc192565c5b1e736f25f05533ea84", null ],
    [ "~Boundary", "d5/d81/classG4VTwistSurface_1_1Boundary.html#a957ce7e52cd84eea895dbf77f5745ffc", null ],
    [ "GetBoundaryParameters", "d5/d81/classG4VTwistSurface_1_1Boundary.html#acf33c4e5bfa7502e2a33c44b934eb478", null ],
    [ "IsEmpty", "d5/d81/classG4VTwistSurface_1_1Boundary.html#a8a170f08d8c168dcc3a26996f8ab84ce", null ],
    [ "SetFields", "d5/d81/classG4VTwistSurface_1_1Boundary.html#a7337614edc294ba4ab2949f2ed081ef5", null ],
    [ "fBoundaryAcode", "d5/d81/classG4VTwistSurface_1_1Boundary.html#a228702beb290a57c0c7623bda8f23603", null ],
    [ "fBoundaryDirection", "d5/d81/classG4VTwistSurface_1_1Boundary.html#a49763a2074c4833eb59346c3aad80bad", null ],
    [ "fBoundaryType", "d5/d81/classG4VTwistSurface_1_1Boundary.html#a2ab098f365fd90692c68890a1898ecfe", null ],
    [ "fBoundaryX0", "d5/d81/classG4VTwistSurface_1_1Boundary.html#aa5eaf1d7265bb65961ef379515455825", null ]
];