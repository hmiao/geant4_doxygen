var classG4BertiniProtonBuilder =
[
    [ "G4BertiniProtonBuilder", "d5/d1f/classG4BertiniProtonBuilder.html#a58234821a37a0794d901d4325e3de8d0", null ],
    [ "~G4BertiniProtonBuilder", "d5/d1f/classG4BertiniProtonBuilder.html#aec23ea87eb519aa349309c865c4df560", null ],
    [ "Build", "d5/d1f/classG4BertiniProtonBuilder.html#ac6e02c6ee1ad5be0156134b07daa51a6", null ],
    [ "Build", "d5/d1f/classG4BertiniProtonBuilder.html#a7a534415d8d1a3953326b85a9936955e", null ],
    [ "Build", "d5/d1f/classG4BertiniProtonBuilder.html#a55f0d3a64b4cb664ebe6188d1a3213a4", null ],
    [ "Build", "d5/d1f/classG4BertiniProtonBuilder.html#a7d7a3fe1fc3f798f594a92694411bb49", null ],
    [ "SetMaxEnergy", "d5/d1f/classG4BertiniProtonBuilder.html#a5c32ba4c2a583de4458a5e765e3f397d", null ],
    [ "SetMinEnergy", "d5/d1f/classG4BertiniProtonBuilder.html#a451aa19ba49ee34588e07c9b640bd6bc", null ],
    [ "theMax", "d5/d1f/classG4BertiniProtonBuilder.html#a99f367b8ed007870a2eefdc41c0f16f2", null ],
    [ "theMin", "d5/d1f/classG4BertiniProtonBuilder.html#a6f75e268d2673c5dd6cade3be5c0ec92", null ],
    [ "theModel", "d5/d1f/classG4BertiniProtonBuilder.html#a6e30148362e4c998423fdcdfaae3f41c", null ]
];