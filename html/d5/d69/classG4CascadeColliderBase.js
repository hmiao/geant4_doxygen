var classG4CascadeColliderBase =
[
    [ "G4CascadeColliderBase", "d5/d69/classG4CascadeColliderBase.html#abbb4384ef19302946b95f4a677dbf4d8", null ],
    [ "~G4CascadeColliderBase", "d5/d69/classG4CascadeColliderBase.html#ac271f1ce6ab203047cd60b800f3f6bbe", null ],
    [ "G4CascadeColliderBase", "d5/d69/classG4CascadeColliderBase.html#a155f017c3956197a855288c56171cc89", null ],
    [ "inelasticInteractionPossible", "d5/d69/classG4CascadeColliderBase.html#af8110455ceda23869f42583606769e06", null ],
    [ "operator=", "d5/d69/classG4CascadeColliderBase.html#adb957889b27f6bee8867a4a1fc979e01", null ],
    [ "rescatter", "d5/d69/classG4CascadeColliderBase.html#aab9c901f0851a10f15b8ee7d0d4659d9", null ],
    [ "setVerboseLevel", "d5/d69/classG4CascadeColliderBase.html#a33c6c7abd5fc001e95aee28b2942ee26", null ],
    [ "useEPCollider", "d5/d69/classG4CascadeColliderBase.html#a213cfa40211b1e8389c9204ecbec2f96", null ],
    [ "validateOutput", "d5/d69/classG4CascadeColliderBase.html#a097d083de53b7d1cfbf243cc3e41ca03", null ],
    [ "validateOutput", "d5/d69/classG4CascadeColliderBase.html#a75bc1843d9bcfd2d8103fdda9e6d59f6", null ],
    [ "validateOutput", "d5/d69/classG4CascadeColliderBase.html#ad614f87a3baca1c4f80b7ddd14d977bb", null ],
    [ "balance", "d5/d69/classG4CascadeColliderBase.html#af0f6898b39128f22e6703e75d0e28f85", null ],
    [ "interCase", "d5/d69/classG4CascadeColliderBase.html#ac30a43daf5b472cac97896b7ddabeb4f", null ]
];