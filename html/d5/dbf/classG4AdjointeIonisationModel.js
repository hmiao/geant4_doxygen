var classG4AdjointeIonisationModel =
[
    [ "G4AdjointeIonisationModel", "d5/dbf/classG4AdjointeIonisationModel.html#ab907c517e6a93df197884858d701afa7", null ],
    [ "~G4AdjointeIonisationModel", "d5/dbf/classG4AdjointeIonisationModel.html#a17fee371dbf4ecd79490e0f18175a142", null ],
    [ "G4AdjointeIonisationModel", "d5/dbf/classG4AdjointeIonisationModel.html#a91da82d763b1f998a73e47892eb7b3cf", null ],
    [ "DiffCrossSectionMoller", "d5/dbf/classG4AdjointeIonisationModel.html#a9782bafb281275efd47ed30397a74c1e", null ],
    [ "DiffCrossSectionPerAtomPrimToSecond", "d5/dbf/classG4AdjointeIonisationModel.html#a13f0e05ab388832911db38d3531b876d", null ],
    [ "operator=", "d5/dbf/classG4AdjointeIonisationModel.html#a51b3a46534c3c496ca88dd9f536b2a8f", null ],
    [ "SampleSecondaries", "d5/dbf/classG4AdjointeIonisationModel.html#a48e6c47c5e6090f4cfa36c4beb887416", null ],
    [ "fWithRapidSampling", "d5/dbf/classG4AdjointeIonisationModel.html#a027c99ecb8cbbbcaade95536e0001515", null ]
];