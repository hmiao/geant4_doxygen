var classG4String =
[
    [ "caseCompare", "d5/d72/classG4String.html#a148962f61818314264120706083d108d", [
      [ "exact", "d5/d72/classG4String.html#a148962f61818314264120706083d108dad038bd01a22bfa55bfdf3fe1294c5b7e", null ],
      [ "ignoreCase", "d5/d72/classG4String.html#a148962f61818314264120706083d108da1b273c504eac9476d7d68a96e7cdcecc", null ]
    ] ],
    [ "stripType", "d5/d72/classG4String.html#a1ccde4ccb092b06841d72fd983b76b15", [
      [ "leading", "d5/d72/classG4String.html#a1ccde4ccb092b06841d72fd983b76b15ae355f62ac7e493651a9017dda75378ad", null ],
      [ "trailing", "d5/d72/classG4String.html#a1ccde4ccb092b06841d72fd983b76b15a2ca36cbcab3712fff511f5cf7df1a68f", null ],
      [ "both", "d5/d72/classG4String.html#a1ccde4ccb092b06841d72fd983b76b15ab7d28dede8b2d2e2949f4bf6afdea2cf", null ]
    ] ],
    [ "G4String", "d5/d72/classG4String.html#ae7ace90e179b57f6f2260a081fd164bc", null ],
    [ "G4String", "d5/d72/classG4String.html#ad1b9ed73408f6c834f6b38082f2e23e5", null ],
    [ "G4String", "d5/d72/classG4String.html#aa0f8868c915556a1fb5028c2ce066f49", null ],
    [ "G4String", "d5/d72/classG4String.html#a6344c860043f56293e1c99089b1d7489", null ],
    [ "G4String", "d5/d72/classG4String.html#a0b51df592cbd58e612f2a8ca090c46fe", null ],
    [ "compareTo", "d5/d72/classG4String.html#a08ade81165ae8f01cb0b2e8317ddb098", null ],
    [ "contains", "d5/d72/classG4String.html#a50c9401cac49d232246eb6ebd66b2e2e", null ],
    [ "contains", "d5/d72/classG4String.html#a7a1485f4331c07a53f2d1d55efe71621", null ],
    [ "operator const char *", "d5/d72/classG4String.html#a505a6d6ec79a1bcfd310b24580015cd1", null ],
    [ "operator=", "d5/d72/classG4String.html#a6a0abf72d00fa0ed5431cb32eda68a7e", null ],
    [ "operator=", "d5/d72/classG4String.html#adf8584676f5af0bbf6dc2966c9c200e9", null ],
    [ "operator[]", "d5/d72/classG4String.html#aefbaafd354a631a9331a5dd76b348117", null ],
    [ "operator[]", "d5/d72/classG4String.html#a0fa066164e5f6f16f21b86f48be1fad5", null ],
    [ "readLine", "d5/d72/classG4String.html#ace28d7ff4528acf219e6b7a2235028de", null ],
    [ "remove", "d5/d72/classG4String.html#adc6da273d877d15aa3eec2f599d20b9d", null ],
    [ "strip", "d5/d72/classG4String.html#ac724b95d0c1b55db54bb02e08b2bad2c", null ],
    [ "toLower", "d5/d72/classG4String.html#ab80ed6fd5fbff1a7d6667435f73d59be", null ],
    [ "toUpper", "d5/d72/classG4String.html#a88e7790d9a2eb3b1460adfb61d5d2e8e", null ]
];