var classG4CsvRFileManager =
[
    [ "G4CsvRFileManager", "d5/de7/classG4CsvRFileManager.html#a7e646d319d162bd0600fcefed363beb2", null ],
    [ "G4CsvRFileManager", "d5/de7/classG4CsvRFileManager.html#adee71891d35ad02763dad521a3ee2730", null ],
    [ "~G4CsvRFileManager", "d5/de7/classG4CsvRFileManager.html#aed214063147c8aa0e8c5d52ef9f575b9", null ],
    [ "CloseFiles", "d5/de7/classG4CsvRFileManager.html#a49fce7685fcd828831e276b4af3532b4", null ],
    [ "GetFileType", "d5/de7/classG4CsvRFileManager.html#a5c3b2560d85ff06e76a76a57049102b0", null ],
    [ "GetRFile", "d5/de7/classG4CsvRFileManager.html#a9b0f40c81f7fd18446b6b29c54dca607", null ],
    [ "OpenRFile", "d5/de7/classG4CsvRFileManager.html#add9ad7b8869c7acde46cb1bb04f4733c", null ],
    [ "fkClass", "d5/de7/classG4CsvRFileManager.html#a71bf8e44a9b64ea4a7f0cbb62042dbd1", null ],
    [ "fRFiles", "d5/de7/classG4CsvRFileManager.html#aba73f13e7443567fde46f395b4c850b7", null ]
];