var classG4PenelopeSamplingData =
[
    [ "G4PenelopeSamplingData", "d5/de1/classG4PenelopeSamplingData.html#a64c6092f17c3f11e7d31e4c6852f3b36", null ],
    [ "~G4PenelopeSamplingData", "d5/de1/classG4PenelopeSamplingData.html#ab2207d2fbd21706eb8e1537623db0b2c", null ],
    [ "G4PenelopeSamplingData", "d5/de1/classG4PenelopeSamplingData.html#acb300714fa11685bc7c04a6bc06b973b", null ],
    [ "AddPoint", "d5/de1/classG4PenelopeSamplingData.html#a6200dbf0507531d4e379554784f3cc73", null ],
    [ "Clear", "d5/de1/classG4PenelopeSamplingData.html#a7315d308f0ff1a198a1adcd6804f414b", null ],
    [ "DumpTable", "d5/de1/classG4PenelopeSamplingData.html#a585bef127578e99b12370dd8c8fd0d4a", null ],
    [ "GetA", "d5/de1/classG4PenelopeSamplingData.html#a1bbe6fb002ffbef3babe6e70c109a497", null ],
    [ "GetB", "d5/de1/classG4PenelopeSamplingData.html#af64bf82c7adafcf7a2a2fb5d25ca2e08", null ],
    [ "GetNumberOfStoredPoints", "d5/de1/classG4PenelopeSamplingData.html#a5b112325731e4f8c1cfba3918e0eb6df", null ],
    [ "GetPAC", "d5/de1/classG4PenelopeSamplingData.html#a4421171bc3c63363b39cbcbde9fc7f7f", null ],
    [ "GetX", "d5/de1/classG4PenelopeSamplingData.html#a6275b134eb9ad492b77f2c26bcfb42c5", null ],
    [ "operator=", "d5/de1/classG4PenelopeSamplingData.html#a1de0bf384b3344ad1c7a338cdfa4ff8a", null ],
    [ "SampleValue", "d5/de1/classG4PenelopeSamplingData.html#ac38e942a20e81a5ce6e27f1d38433d98", null ],
    [ "fA", "d5/de1/classG4PenelopeSamplingData.html#a94476d4a3ab2191604bbb84c1e62cc20", null ],
    [ "fB", "d5/de1/classG4PenelopeSamplingData.html#a4d5f2e0526446ab7d100bb4c9a2bf13f", null ],
    [ "fITTL", "d5/de1/classG4PenelopeSamplingData.html#afae1c9cb0ffed6db525496baaa40a829", null ],
    [ "fITTU", "d5/de1/classG4PenelopeSamplingData.html#a19c92d971dabb8a2537ed6e06510ad40", null ],
    [ "fNP", "d5/de1/classG4PenelopeSamplingData.html#ae9e59b389d5cecd65a97d3a998269b92", null ],
    [ "fPAC", "d5/de1/classG4PenelopeSamplingData.html#a91f23b775ab68bdbe8c04bf751b474ed", null ],
    [ "fX", "d5/de1/classG4PenelopeSamplingData.html#a451f21fba1a8467ae7b668bead1ba800", null ]
];