var classG4PlotManager =
[
    [ "G4PlotManager", "d5/d87/classG4PlotManager.html#a54d6690c19ba3548d30af8a2142bdca7", null ],
    [ "~G4PlotManager", "d5/d87/classG4PlotManager.html#a483a3c0761ba4652a9d59111cf36d65b", null ],
    [ "G4PlotManager", "d5/d87/classG4PlotManager.html#a51d42cde615aaccead349fd415371c54", null ],
    [ "G4PlotManager", "d5/d87/classG4PlotManager.html#ae0c27d3796a990d35a063716679aee91", null ],
    [ "CloseFile", "d5/d87/classG4PlotManager.html#a576c435f7d2139d19d54f15154080679", null ],
    [ "GetNofPlotsPerPage", "d5/d87/classG4PlotManager.html#a6f31b801fdb845ddc4ea0716338ae79d", null ],
    [ "OpenFile", "d5/d87/classG4PlotManager.html#a3c17a734c50daf5cff32750ed1a70171", null ],
    [ "operator=", "d5/d87/classG4PlotManager.html#a546afe8729eceb2a38b560dd6ad3cd8f", null ],
    [ "PlotAndWrite", "d5/d87/classG4PlotManager.html#ae1d6f50089c910065f69bed4661c0dff", null ],
    [ "WritePage", "d5/d87/classG4PlotManager.html#a25b3ec662e2be7a047cf34e64d0ce580", null ],
    [ "fFileName", "d5/d87/classG4PlotManager.html#a72161d9271e0ad54e1a9850c08eeca75", null ],
    [ "fkClass", "d5/d87/classG4PlotManager.html#a950f106eb84c25c5f01f5e85da43c69c", null ],
    [ "fPlotParameters", "d5/d87/classG4PlotManager.html#a4fedd4701d65e61668d8307db859fec6", null ],
    [ "fState", "d5/d87/classG4PlotManager.html#a305a3f61ffc47ef2f4166b5b6264f974", null ],
    [ "fViewer", "d5/d87/classG4PlotManager.html#a3399d478b8ddd1cff54996a022b89f42", null ]
];