var classG4OpenGLXmTopLevelShell =
[
    [ "G4OpenGLXmTopLevelShell", "d5/d01/classG4OpenGLXmTopLevelShell.html#ac079ebaeb99d4150f0c2527b9ea40521", null ],
    [ "~G4OpenGLXmTopLevelShell", "d5/d01/classG4OpenGLXmTopLevelShell.html#a6fe9b0518dfa65dff4f4de8efa34a351", null ],
    [ "G4OpenGLXmTopLevelShell", "d5/d01/classG4OpenGLXmTopLevelShell.html#a973e4ac850f9932745e3112c7f67a460", null ],
    [ "AddChild", "d5/d01/classG4OpenGLXmTopLevelShell.html#aff1b21e06885ea31098425bb08d41d09", null ],
    [ "GetName", "d5/d01/classG4OpenGLXmTopLevelShell.html#aea1addad7dc882e59c2e31c5db0d1d28", null ],
    [ "GetPointerToWidget", "d5/d01/classG4OpenGLXmTopLevelShell.html#a7df3979d2a194afbd1a09dfc58e02628", null ],
    [ "operator=", "d5/d01/classG4OpenGLXmTopLevelShell.html#af35bfd9db334849e1145abe77fbdf788", null ],
    [ "Realize", "d5/d01/classG4OpenGLXmTopLevelShell.html#a449fc052a16f89a675176079a6fafe13", null ],
    [ "frame", "d5/d01/classG4OpenGLXmTopLevelShell.html#aaf5a8bc2e046006ca3559cea14aa6c4e", null ],
    [ "name", "d5/d01/classG4OpenGLXmTopLevelShell.html#a09e8dc1cf9f4826af1fc11b26b201f64", null ],
    [ "top_box", "d5/d01/classG4OpenGLXmTopLevelShell.html#acffd10f0a4cce4afe5a2fa327f6e7822", null ],
    [ "toplevel", "d5/d01/classG4OpenGLXmTopLevelShell.html#a939e2323bda97d0518dbb531114662cd", null ]
];