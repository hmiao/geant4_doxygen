var dir_512f553a853d127e686cbe4f6ea2bd2d =
[
    [ "G4BiasingAppliedCase.hh", "d1/d40/G4BiasingAppliedCase_8hh.html", "d1/d40/G4BiasingAppliedCase_8hh" ],
    [ "G4BiasingOperationManager.hh", "d6/d4e/G4BiasingOperationManager_8hh.html", "d6/d4e/G4BiasingOperationManager_8hh" ],
    [ "G4ProcessPlacer.hh", "dc/dd7/G4ProcessPlacer_8hh.html", "dc/dd7/G4ProcessPlacer_8hh" ],
    [ "G4VBiasingInteractionLaw.hh", "d8/dbc/G4VBiasingInteractionLaw_8hh.html", "d8/dbc/G4VBiasingInteractionLaw_8hh" ],
    [ "G4VBiasingOperation.hh", "d8/d3a/G4VBiasingOperation_8hh.html", "d8/d3a/G4VBiasingOperation_8hh" ],
    [ "G4VBiasingOperator.hh", "d0/d56/G4VBiasingOperator_8hh.html", "d0/d56/G4VBiasingOperator_8hh" ],
    [ "G4VProcessPlacer.hh", "d4/da3/G4VProcessPlacer_8hh.html", "d4/da3/G4VProcessPlacer_8hh" ]
];