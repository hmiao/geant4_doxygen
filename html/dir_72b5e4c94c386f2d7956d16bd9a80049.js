var dir_72b5e4c94c386f2d7956d16bd9a80049 =
[
    [ "G4Cerenkov.hh", "dd/db3/G4Cerenkov_8hh.html", "dd/db3/G4Cerenkov_8hh" ],
    [ "G4ForwardXrayTR.hh", "d8/de7/G4ForwardXrayTR_8hh.html", "d8/de7/G4ForwardXrayTR_8hh" ],
    [ "G4GammaXTRadiator.hh", "d8/d50/G4GammaXTRadiator_8hh.html", "d8/d50/G4GammaXTRadiator_8hh" ],
    [ "G4GaussXTRadiator.hh", "df/dfe/G4GaussXTRadiator_8hh.html", "df/dfe/G4GaussXTRadiator_8hh" ],
    [ "G4RegularXTRadiator.hh", "d1/de2/G4RegularXTRadiator_8hh.html", "d1/de2/G4RegularXTRadiator_8hh" ],
    [ "G4Scintillation.hh", "da/d71/G4Scintillation_8hh.html", "da/d71/G4Scintillation_8hh" ],
    [ "G4ScintillationTrackInformation.hh", "d1/d9e/G4ScintillationTrackInformation_8hh.html", "d1/d9e/G4ScintillationTrackInformation_8hh" ],
    [ "G4StrawTubeXTRadiator.hh", "d3/d0f/G4StrawTubeXTRadiator_8hh.html", "d3/d0f/G4StrawTubeXTRadiator_8hh" ],
    [ "G4SynchrotronRadiation.hh", "de/d36/G4SynchrotronRadiation_8hh.html", "de/d36/G4SynchrotronRadiation_8hh" ],
    [ "G4SynchrotronRadiationInMat.hh", "db/ddf/G4SynchrotronRadiationInMat_8hh.html", "db/ddf/G4SynchrotronRadiationInMat_8hh" ],
    [ "G4TransitionRadiation.hh", "df/dd5/G4TransitionRadiation_8hh.html", "df/dd5/G4TransitionRadiation_8hh" ],
    [ "G4TransparentRegXTRadiator.hh", "dc/d2b/G4TransparentRegXTRadiator_8hh.html", "dc/d2b/G4TransparentRegXTRadiator_8hh" ],
    [ "G4VTransitionRadiation.hh", "d0/dde/G4VTransitionRadiation_8hh.html", "d0/dde/G4VTransitionRadiation_8hh" ],
    [ "G4VTRModel.hh", "d5/d91/G4VTRModel_8hh.html", "d5/d91/G4VTRModel_8hh" ],
    [ "G4VXTRenergyLoss.hh", "da/d2e/G4VXTRenergyLoss_8hh.html", "da/d2e/G4VXTRenergyLoss_8hh" ],
    [ "G4XTRGammaRadModel.hh", "de/d8a/G4XTRGammaRadModel_8hh.html", "de/d8a/G4XTRGammaRadModel_8hh" ],
    [ "G4XTRRegularRadModel.hh", "da/d95/G4XTRRegularRadModel_8hh.html", "da/d95/G4XTRRegularRadModel_8hh" ],
    [ "G4XTRTransparentRegRadModel.hh", "df/d4a/G4XTRTransparentRegRadModel_8hh.html", "df/d4a/G4XTRTransparentRegRadModel_8hh" ]
];