var dir_a45397194edcfe79d57cb1d68ac86ffc =
[
    [ "G4CompositeDataSet.cc", "d9/dbd/G4CompositeDataSet_8cc.html", null ],
    [ "G4DataSet.cc", "d9/d88/G4DataSet_8cc.html", null ],
    [ "G4hImpactIonisation.cc", "dd/d7e/G4hImpactIonisation_8cc.html", null ],
    [ "G4hRDEnergyLoss.cc", "df/d22/G4hRDEnergyLoss_8cc.html", null ],
    [ "G4LinInterpolator.cc", "d8/d64/G4LinInterpolator_8cc.html", null ],
    [ "G4LogLogInterpolator.cc", "d4/d73/G4LogLogInterpolator_8cc.html", null ],
    [ "G4PixeCrossSectionHandler.cc", "d2/d3f/G4PixeCrossSectionHandler_8cc.html", null ],
    [ "G4PixeShellDataSet.cc", "d2/dfe/G4PixeShellDataSet_8cc.html", null ]
];