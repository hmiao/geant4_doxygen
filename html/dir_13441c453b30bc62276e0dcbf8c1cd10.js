var dir_13441c453b30bc62276e0dcbf8c1cd10 =
[
    [ "G4AlphaCoulombBarrier.hh", "d0/de5/G4AlphaCoulombBarrier_8hh.html", "d0/de5/G4AlphaCoulombBarrier_8hh" ],
    [ "G4CameronGilbertPairingCorrections.hh", "d0/d3b/G4CameronGilbertPairingCorrections_8hh.html", "d0/d3b/G4CameronGilbertPairingCorrections_8hh" ],
    [ "G4CameronGilbertShellCorrections.hh", "dc/d7b/G4CameronGilbertShellCorrections_8hh.html", "dc/d7b/G4CameronGilbertShellCorrections_8hh" ],
    [ "G4CameronShellPlusPairingCorrections.hh", "db/d42/G4CameronShellPlusPairingCorrections_8hh.html", "db/d42/G4CameronShellPlusPairingCorrections_8hh" ],
    [ "G4CameronTruranHilfPairingCorrections.hh", "d5/d02/G4CameronTruranHilfPairingCorrections_8hh.html", "d5/d02/G4CameronTruranHilfPairingCorrections_8hh" ],
    [ "G4CameronTruranHilfShellCorrections.hh", "d4/d25/G4CameronTruranHilfShellCorrections_8hh.html", "d4/d25/G4CameronTruranHilfShellCorrections_8hh" ],
    [ "G4ChatterjeeCrossSection.hh", "d8/dd7/G4ChatterjeeCrossSection_8hh.html", "d8/dd7/G4ChatterjeeCrossSection_8hh" ],
    [ "G4ConstantLevelDensityParameter.hh", "df/d40/G4ConstantLevelDensityParameter_8hh.html", "df/d40/G4ConstantLevelDensityParameter_8hh" ],
    [ "G4CookPairingCorrections.hh", "d8/db2/G4CookPairingCorrections_8hh.html", "d8/db2/G4CookPairingCorrections_8hh" ],
    [ "G4CookShellCorrections.hh", "db/d2a/G4CookShellCorrections_8hh.html", "db/d2a/G4CookShellCorrections_8hh" ],
    [ "G4CoulombBarrier.hh", "de/d85/G4CoulombBarrier_8hh.html", "de/d85/G4CoulombBarrier_8hh" ],
    [ "G4DeuteronCoulombBarrier.hh", "d2/d9e/G4DeuteronCoulombBarrier_8hh.html", "d2/d9e/G4DeuteronCoulombBarrier_8hh" ],
    [ "G4He3CoulombBarrier.hh", "d1/dc0/G4He3CoulombBarrier_8hh.html", "d1/dc0/G4He3CoulombBarrier_8hh" ],
    [ "G4KalbachCrossSection.hh", "dd/dc5/G4KalbachCrossSection_8hh.html", "dd/dc5/G4KalbachCrossSection_8hh" ],
    [ "G4NeutronCoulombBarrier.hh", "da/d3b/G4NeutronCoulombBarrier_8hh.html", "da/d3b/G4NeutronCoulombBarrier_8hh" ],
    [ "G4PairingCorrection.hh", "df/d4b/G4PairingCorrection_8hh.html", "df/d4b/G4PairingCorrection_8hh" ],
    [ "G4ProtonCoulombBarrier.hh", "d3/d16/G4ProtonCoulombBarrier_8hh.html", "d3/d16/G4ProtonCoulombBarrier_8hh" ],
    [ "G4ShellCorrection.hh", "dc/da7/G4ShellCorrection_8hh.html", "dc/da7/G4ShellCorrection_8hh" ],
    [ "G4TritonCoulombBarrier.hh", "de/d3b/G4TritonCoulombBarrier_8hh.html", "de/d3b/G4TritonCoulombBarrier_8hh" ],
    [ "G4VCoulombBarrier.hh", "d7/d03/G4VCoulombBarrier_8hh.html", "d7/d03/G4VCoulombBarrier_8hh" ],
    [ "G4VLevelDensityParameter.hh", "d8/d36/G4VLevelDensityParameter_8hh.html", "d8/d36/G4VLevelDensityParameter_8hh" ]
];