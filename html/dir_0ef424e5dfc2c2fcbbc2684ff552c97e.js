var dir_0ef424e5dfc2c2fcbbc2684ff552c97e =
[
    [ "G4EnergyLossForExtrapolator.hh", "d9/d3c/G4EnergyLossForExtrapolator_8hh.html", "d9/d3c/G4EnergyLossForExtrapolator_8hh" ],
    [ "G4ePairProduction.hh", "d1/dcf/G4ePairProduction_8hh.html", "d1/dcf/G4ePairProduction_8hh" ],
    [ "G4ErrorEnergyLoss.hh", "d0/d2d/G4ErrorEnergyLoss_8hh.html", "d0/d2d/G4ErrorEnergyLoss_8hh" ],
    [ "G4ModifiedMephi.hh", "d6/df6/G4ModifiedMephi_8hh.html", "d6/df6/G4ModifiedMephi_8hh" ],
    [ "G4MuBetheBlochModel.hh", "d4/d04/G4MuBetheBlochModel_8hh.html", "d4/d04/G4MuBetheBlochModel_8hh" ],
    [ "G4MuBremsstrahlung.hh", "de/dbe/G4MuBremsstrahlung_8hh.html", "de/dbe/G4MuBremsstrahlung_8hh" ],
    [ "G4MuBremsstrahlungModel.hh", "df/db8/G4MuBremsstrahlungModel_8hh.html", "df/db8/G4MuBremsstrahlungModel_8hh" ],
    [ "G4MuIonisation.hh", "dc/dd1/G4MuIonisation_8hh.html", "dc/dd1/G4MuIonisation_8hh" ],
    [ "G4MuMultipleScattering.hh", "d3/d61/G4MuMultipleScattering_8hh.html", "d3/d61/G4MuMultipleScattering_8hh" ],
    [ "G4MuPairProduction.hh", "d8/d2e/G4MuPairProduction_8hh.html", "d8/d2e/G4MuPairProduction_8hh" ],
    [ "G4MuPairProductionModel.hh", "da/dd9/G4MuPairProductionModel_8hh.html", "da/dd9/G4MuPairProductionModel_8hh" ],
    [ "G4TablesForExtrapolator.hh", "d1/dc0/G4TablesForExtrapolator_8hh.html", "d1/dc0/G4TablesForExtrapolator_8hh" ]
];