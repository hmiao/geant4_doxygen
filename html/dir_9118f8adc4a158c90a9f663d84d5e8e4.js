var dir_9118f8adc4a158c90a9f663d84d5e8e4 =
[
    [ "G4ElectronNuclearProcess.hh", "d0/d60/G4ElectronNuclearProcess_8hh.html", "d0/d60/G4ElectronNuclearProcess_8hh" ],
    [ "G4ElNeutrinoNucleusProcess.hh", "df/db8/G4ElNeutrinoNucleusProcess_8hh.html", "df/db8/G4ElNeutrinoNucleusProcess_8hh" ],
    [ "G4HadronElasticProcess.hh", "da/d15/G4HadronElasticProcess_8hh.html", "da/d15/G4HadronElasticProcess_8hh" ],
    [ "G4HadronInelasticProcess.hh", "d0/ded/G4HadronInelasticProcess_8hh.html", "d0/ded/G4HadronInelasticProcess_8hh" ],
    [ "G4MuNeutrinoNucleusProcess.hh", "d3/d26/G4MuNeutrinoNucleusProcess_8hh.html", "d3/d26/G4MuNeutrinoNucleusProcess_8hh" ],
    [ "G4MuonNuclearProcess.hh", "de/de9/G4MuonNuclearProcess_8hh.html", "de/de9/G4MuonNuclearProcess_8hh" ],
    [ "G4NeutrinoElectronProcess.hh", "d9/d30/G4NeutrinoElectronProcess_8hh.html", "d9/d30/G4NeutrinoElectronProcess_8hh" ],
    [ "G4NeutronCaptureProcess.hh", "d4/d57/G4NeutronCaptureProcess_8hh.html", "d4/d57/G4NeutronCaptureProcess_8hh" ],
    [ "G4NeutronFissionProcess.hh", "d2/d50/G4NeutronFissionProcess_8hh.html", "d2/d50/G4NeutronFissionProcess_8hh" ],
    [ "G4PositronNuclearProcess.hh", "db/dfa/G4PositronNuclearProcess_8hh.html", "db/dfa/G4PositronNuclearProcess_8hh" ],
    [ "G4UCNAbsorption.hh", "db/d98/G4UCNAbsorption_8hh.html", "db/d98/G4UCNAbsorption_8hh" ],
    [ "G4UCNBoundaryProcess.hh", "d1/dc7/G4UCNBoundaryProcess_8hh.html", "d1/dc7/G4UCNBoundaryProcess_8hh" ],
    [ "G4UCNBoundaryProcessMessenger.hh", "dd/dcf/G4UCNBoundaryProcessMessenger_8hh.html", "dd/dcf/G4UCNBoundaryProcessMessenger_8hh" ],
    [ "G4UCNLoss.hh", "d7/d08/G4UCNLoss_8hh.html", "d7/d08/G4UCNLoss_8hh" ],
    [ "G4UCNMultiScattering.hh", "d6/d9d/G4UCNMultiScattering_8hh.html", "d6/d9d/G4UCNMultiScattering_8hh" ],
    [ "G4UCNProcessSubType.hh", "d5/d0a/G4UCNProcessSubType_8hh.html", "d5/d0a/G4UCNProcessSubType_8hh" ]
];