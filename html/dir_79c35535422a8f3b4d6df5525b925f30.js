var dir_79c35535422a8f3b4d6df5525b925f30 =
[
    [ "G4ChargeExchangePhysics.hh", "dc/d0f/G4ChargeExchangePhysics_8hh.html", "dc/d0f/G4ChargeExchangePhysics_8hh" ],
    [ "G4HadronDElasticPhysics.hh", "d1/dfd/G4HadronDElasticPhysics_8hh.html", "d1/dfd/G4HadronDElasticPhysics_8hh" ],
    [ "G4HadronElasticPhysics.hh", "d0/dd7/G4HadronElasticPhysics_8hh.html", "d0/dd7/G4HadronElasticPhysics_8hh" ],
    [ "G4HadronElasticPhysicsHP.hh", "d1/de8/G4HadronElasticPhysicsHP_8hh.html", "d1/de8/G4HadronElasticPhysicsHP_8hh" ],
    [ "G4HadronElasticPhysicsLEND.hh", "df/df3/G4HadronElasticPhysicsLEND_8hh.html", "df/df3/G4HadronElasticPhysicsLEND_8hh" ],
    [ "G4HadronElasticPhysicsPHP.hh", "d4/dc3/G4HadronElasticPhysicsPHP_8hh.html", "d4/dc3/G4HadronElasticPhysicsPHP_8hh" ],
    [ "G4HadronElasticPhysicsXS.hh", "dd/d68/G4HadronElasticPhysicsXS_8hh.html", "dd/d68/G4HadronElasticPhysicsXS_8hh" ],
    [ "G4HadronHElasticPhysics.hh", "d4/d39/G4HadronHElasticPhysics_8hh.html", "d4/d39/G4HadronHElasticPhysics_8hh" ],
    [ "G4IonElasticPhysics.hh", "df/dbf/G4IonElasticPhysics_8hh.html", "df/dbf/G4IonElasticPhysics_8hh" ],
    [ "G4ThermalNeutrons.hh", "dc/d99/G4ThermalNeutrons_8hh.html", "dc/d99/G4ThermalNeutrons_8hh" ]
];