var classG4TrajectoryEncounteredVolumeFilter =
[
    [ "G4TrajectoryEncounteredVolumeFilter", "df/dab/classG4TrajectoryEncounteredVolumeFilter.html#ab8a65fe91c86a0e8f3aab2268f940984", null ],
    [ "~G4TrajectoryEncounteredVolumeFilter", "df/dab/classG4TrajectoryEncounteredVolumeFilter.html#a0d55566aba3610fdf745cf563381ece4", null ],
    [ "Add", "df/dab/classG4TrajectoryEncounteredVolumeFilter.html#a1ca8e31e9f84281cff0594f8c8fa572a", null ],
    [ "Clear", "df/dab/classG4TrajectoryEncounteredVolumeFilter.html#aecdcb8359634570d54f461bac2eb16e6", null ],
    [ "Evaluate", "df/dab/classG4TrajectoryEncounteredVolumeFilter.html#a087ed41c1a9a5de7f4275c8b0c3f963d", null ],
    [ "Print", "df/dab/classG4TrajectoryEncounteredVolumeFilter.html#a95f312b5b2324c8554edb00d8bf2ce00", null ],
    [ "fVolumes", "df/dab/classG4TrajectoryEncounteredVolumeFilter.html#a57a9f950d358361ff13dffcadc64a2d0", null ]
];