var classPTL_1_1tbb_1_1global__control =
[
    [ "parameter", "df/d97/classPTL_1_1tbb_1_1global__control.html#a030c3d8c65f24736568d63418dc1d7a7", [
      [ "max_allowed_parallelism", "df/d97/classPTL_1_1tbb_1_1global__control.html#a030c3d8c65f24736568d63418dc1d7a7aa9c8693a97c9fe313e9ded02aed2c47d", null ],
      [ "thread_stack_size", "df/d97/classPTL_1_1tbb_1_1global__control.html#a030c3d8c65f24736568d63418dc1d7a7ada980949c830ab89978819f4d21ac49f", null ]
    ] ],
    [ "global_control", "df/d97/classPTL_1_1tbb_1_1global__control.html#a6c6f8325722edb96be1fdeab8b4508c7", null ],
    [ "~global_control", "df/d97/classPTL_1_1tbb_1_1global__control.html#acb864bab282ce85544afd058b5e2889f", null ],
    [ "active_value", "df/d97/classPTL_1_1tbb_1_1global__control.html#aec802a90f17fd3418332973792872440", null ]
];