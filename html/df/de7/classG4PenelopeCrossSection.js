var classG4PenelopeCrossSection =
[
    [ "G4PenelopeCrossSection", "df/de7/classG4PenelopeCrossSection.html#afd1855b3f08dfd82098245c7db7d9855", null ],
    [ "~G4PenelopeCrossSection", "df/de7/classG4PenelopeCrossSection.html#adbe96b35a7f5e85c375c74c9321303df", null ],
    [ "G4PenelopeCrossSection", "df/de7/classG4PenelopeCrossSection.html#acb3dab40d7c64a587f219d1dbe2d60d3", null ],
    [ "AddCrossSectionPoint", "df/de7/classG4PenelopeCrossSection.html#adfa2a3bbd7eff8e819ed9d58bf9c005a", null ],
    [ "AddShellCrossSectionPoint", "df/de7/classG4PenelopeCrossSection.html#a0412213ceaeb9d0319db0713386057b1", null ],
    [ "GetHardCrossSection", "df/de7/classG4PenelopeCrossSection.html#a6e6b263d2e9e9eceb7d651514c8cbe8b", null ],
    [ "GetNormalizedShellCrossSection", "df/de7/classG4PenelopeCrossSection.html#ad4bbd76aec2ddcac889620aedab0ac92", null ],
    [ "GetNumberOfShells", "df/de7/classG4PenelopeCrossSection.html#a08ab65c301189fd482144d0b520b7963", null ],
    [ "GetShellCrossSection", "df/de7/classG4PenelopeCrossSection.html#a51532a46c59bea64af9946d4070c6485", null ],
    [ "GetSoftStoppingPower", "df/de7/classG4PenelopeCrossSection.html#a436729a89cac957f4e3664acb0d3ab45", null ],
    [ "GetTotalCrossSection", "df/de7/classG4PenelopeCrossSection.html#a2dd498ed2ab499edf660600ebecd55e2", null ],
    [ "NormalizeShellCrossSections", "df/de7/classG4PenelopeCrossSection.html#ac9ab4d7c88e7609f9b9e49e8721a02e1", null ],
    [ "operator=", "df/de7/classG4PenelopeCrossSection.html#ae420a66d2793cba80b5d392a571f2719", null ],
    [ "fHardCrossSections", "df/de7/classG4PenelopeCrossSection.html#ad34e196196262bdc4bda0d4e2d5828b0", null ],
    [ "fIsNormalized", "df/de7/classG4PenelopeCrossSection.html#a719959df7d16988d89380e9eddfdf9eb", null ],
    [ "fNumberOfEnergyPoints", "df/de7/classG4PenelopeCrossSection.html#a31e94721b059636c9fd63bcb6528049f", null ],
    [ "fNumberOfShells", "df/de7/classG4PenelopeCrossSection.html#af67b53cbceea3dae2b7c14649e19fc8d", null ],
    [ "fShellCrossSections", "df/de7/classG4PenelopeCrossSection.html#a5b6b717f505c6f9c99b6e9b9326f77ed", null ],
    [ "fShellNormalizedCrossSections", "df/de7/classG4PenelopeCrossSection.html#a21d3656183bd350183e812952171a3d3", null ],
    [ "fSoftCrossSections", "df/de7/classG4PenelopeCrossSection.html#a825e1bbbd4f9143f0d594a5bebbd2ee6", null ]
];