var classG4PlotterModel =
[
    [ "G4PlotterModel", "df/db9/classG4PlotterModel.html#a5a712b414f7d12c6ed1da80a745402f3", null ],
    [ "~G4PlotterModel", "df/db9/classG4PlotterModel.html#ac34be81eedaeafe80307d588cad13a5c", null ],
    [ "G4PlotterModel", "df/db9/classG4PlotterModel.html#a63f70c58a9a25f3945d302dbc53cba53", null ],
    [ "DescribeYourselfTo", "df/db9/classG4PlotterModel.html#a51bfefa7c621da47bd2868f8f6b8dbef", null ],
    [ "operator=", "df/db9/classG4PlotterModel.html#aca54833823ad735c55605311a2cb4d7c", null ],
    [ "plotter", "df/db9/classG4PlotterModel.html#a70c04b2175448d98ddbe94b1877804b0", null ],
    [ "fPlotter", "df/db9/classG4PlotterModel.html#a58f6f16e1be1875b76bd342e6fa5afd0", null ],
    [ "fTransform", "df/db9/classG4PlotterModel.html#a69c718442ff8b3cd1c1843e6a96673ed", null ]
];