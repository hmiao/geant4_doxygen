var classG4CascadeFunctions =
[
    [ "G4CascadeFunctions", "df/d30/classG4CascadeFunctions.html#a3b9ddf1859a13ea8fc820ffc6893917a", null ],
    [ "~G4CascadeFunctions", "df/d30/classG4CascadeFunctions.html#ac89935842c4bddc886a72f374c427778", null ],
    [ "getCrossSection", "df/d30/classG4CascadeFunctions.html#a9af46106b0b37c372d77d657b40bbe3e", null ],
    [ "getCrossSectionSum", "df/d30/classG4CascadeFunctions.html#a3112c8d86e43b926556dd36d0659f8b0", null ],
    [ "getMultiplicity", "df/d30/classG4CascadeFunctions.html#a673b3b7031b12e3dbb4bc364d103e1d6", null ],
    [ "getOutgoingParticleTypes", "df/d30/classG4CascadeFunctions.html#ad6a83c0d74ef9b0afac5cfe67f86f174", null ],
    [ "printTable", "df/d30/classG4CascadeFunctions.html#a2f7bfd02df45e499fc762e87eed54724", null ]
];