var classG4AllocatorBase =
[
    [ "G4AllocatorBase", "df/dda/classG4AllocatorBase.html#ade032cfa69a7ffb36785bd2fd96da92f", null ],
    [ "~G4AllocatorBase", "df/dda/classG4AllocatorBase.html#aebc5ff4d6fb0bc76410a5cbedbe1fa94", null ],
    [ "GetAllocatedSize", "df/dda/classG4AllocatorBase.html#a6c2a328780ab50445d5caf565c94a1a8", null ],
    [ "GetNoPages", "df/dda/classG4AllocatorBase.html#ad7bef0c526e04717288449ca58a1aed5", null ],
    [ "GetPageSize", "df/dda/classG4AllocatorBase.html#af85fd072158af370109cb3fe30e47df0", null ],
    [ "GetPoolType", "df/dda/classG4AllocatorBase.html#ab18830ed6b6e7722a79df69ab9bd4b9d", null ],
    [ "IncreasePageSize", "df/dda/classG4AllocatorBase.html#a02b3f559669e88f5a77819b698baf782", null ],
    [ "ResetStorage", "df/dda/classG4AllocatorBase.html#abc82d09e4c5d88584f221e98f510fad0", null ]
];