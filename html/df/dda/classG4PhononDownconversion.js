var classG4PhononDownconversion =
[
    [ "G4PhononDownconversion", "df/dda/classG4PhononDownconversion.html#a70cc28462215ba971e544a43db5f3e30", null ],
    [ "~G4PhononDownconversion", "df/dda/classG4PhononDownconversion.html#af9fd375547907697784e1cedbff6f543", null ],
    [ "G4PhononDownconversion", "df/dda/classG4PhononDownconversion.html#af4da0c8b2bcfbac3abfe7cb96918e7a8", null ],
    [ "GetLTDecayProb", "df/dda/classG4PhononDownconversion.html#aad989caca818759c642e2fc95b2922ae", null ],
    [ "GetMeanFreePath", "df/dda/classG4PhononDownconversion.html#a42ed01dcba8e37d66ca596ec99508160", null ],
    [ "GetTTDecayProb", "df/dda/classG4PhononDownconversion.html#aaeda96a6fb6cb0b034601839cf34d303", null ],
    [ "IsApplicable", "df/dda/classG4PhononDownconversion.html#a441caccf87451b85f4f06e1f92343796", null ],
    [ "MakeLDeviation", "df/dda/classG4PhononDownconversion.html#aa112d1b44b24af35348b8836db3bf871", null ],
    [ "MakeLTSecondaries", "df/dda/classG4PhononDownconversion.html#a2bbab9e7109140ab06b793cbf4d47c3b", null ],
    [ "MakeTDeviation", "df/dda/classG4PhononDownconversion.html#a050551cccdb6d2ce6c7983b3fcbb9251", null ],
    [ "MakeTTDeviation", "df/dda/classG4PhononDownconversion.html#a980089a49b465c2296fafc40d7ce2641", null ],
    [ "MakeTTSecondaries", "df/dda/classG4PhononDownconversion.html#a9c1270e87b1ed381738dbb768a8ce58f", null ],
    [ "operator=", "df/dda/classG4PhononDownconversion.html#a542f0262748498bb94c8a020f6fe0a45", null ],
    [ "PostStepDoIt", "df/dda/classG4PhononDownconversion.html#a77f1184cf0f08eae7dc265d71873759e", null ],
    [ "fBeta", "df/dda/classG4PhononDownconversion.html#a2a70c75ffc21d449099718b6dab158ed", null ],
    [ "fGamma", "df/dda/classG4PhononDownconversion.html#abea7c88a03307b9dda276ca3dfdb1687", null ],
    [ "fLambda", "df/dda/classG4PhononDownconversion.html#ab809c76acb86a894775fa1c9bec0babf", null ],
    [ "fMu", "df/dda/classG4PhononDownconversion.html#ab4e6bc9827e45a74beca28f96b6cd4db", null ]
];