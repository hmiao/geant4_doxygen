var classG4PreCompoundIon =
[
    [ "G4PreCompoundIon", "df/df2/classG4PreCompoundIon.html#a968cd3c813b2202e3e21dcee4d5ab4c8", null ],
    [ "~G4PreCompoundIon", "df/df2/classG4PreCompoundIon.html#aa002f7a656761ddbb185a8493929cf56", null ],
    [ "G4PreCompoundIon", "df/df2/classG4PreCompoundIon.html#a478188a61cac80d0d2224b359c61eaa2", null ],
    [ "CoalescenceFactor", "df/df2/classG4PreCompoundIon.html#ad3f76963db329fa50e75ab7430f28cc6", null ],
    [ "FactorialFactor", "df/df2/classG4PreCompoundIon.html#a413ba1ed7d01ff01cff842f61d58879c", null ],
    [ "GetBeta", "df/df2/classG4PreCompoundIon.html#a0ad8767af11c0924022155f0e7bed569", null ],
    [ "GetRj", "df/df2/classG4PreCompoundIon.html#adcc07546a1be7af4f356a884f9ef551e", null ],
    [ "operator!=", "df/df2/classG4PreCompoundIon.html#a17122882db7d39fc04363108da74e046", null ],
    [ "operator=", "df/df2/classG4PreCompoundIon.html#a1368ff7aa6f0c2be050728c89a82b6fc", null ],
    [ "operator==", "df/df2/classG4PreCompoundIon.html#a53f699f654ca7746da93adbb1b7a821a", null ],
    [ "ProbabilityDistributionFunction", "df/df2/classG4PreCompoundIon.html#add9d90cfbf215aa26e39c45d3c08a188", null ],
    [ "fact", "df/df2/classG4PreCompoundIon.html#adc6f83eb4b60280f0a3c7283b0ed8493", null ]
];