var classG4BinaryAlphaBuilder =
[
    [ "G4BinaryAlphaBuilder", "df/da9/classG4BinaryAlphaBuilder.html#a6c6af30b2b79fea9936c453f8a43045a", null ],
    [ "~G4BinaryAlphaBuilder", "df/da9/classG4BinaryAlphaBuilder.html#a8e84eb1ff3a6efbb374cb3a9695cfe61", null ],
    [ "Build", "df/da9/classG4BinaryAlphaBuilder.html#a239fc0bfee75be3847ac835e91bc6d98", null ],
    [ "Build", "df/da9/classG4BinaryAlphaBuilder.html#acfc76ac7537536f00c766eea256419a1", null ],
    [ "Build", "df/da9/classG4BinaryAlphaBuilder.html#aaa0a2d34a37bbb07ab7b97c55aa06a5f", null ],
    [ "Build", "df/da9/classG4BinaryAlphaBuilder.html#aaa7dd55ee04b0c8f0e5e5e8dff304123", null ],
    [ "SetMaxEnergy", "df/da9/classG4BinaryAlphaBuilder.html#acc17dd9454adcc6795d0eda02a0a9a23", null ],
    [ "SetMinEnergy", "df/da9/classG4BinaryAlphaBuilder.html#afc66b29a8e13c51850e48f1397281f08", null ],
    [ "theMax", "df/da9/classG4BinaryAlphaBuilder.html#a1c1c14a12b2c382063039b668d3beab8", null ],
    [ "theMin", "df/da9/classG4BinaryAlphaBuilder.html#a62d94ce03bdfbd525c8299cc22f07342", null ],
    [ "theModel", "df/da9/classG4BinaryAlphaBuilder.html#a0629476ba6e3a6245c1072d0bb02b9fd", null ]
];