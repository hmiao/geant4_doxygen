var G4OpticalParameters_8hh =
[
    [ "G4OpticalParameters", "d5/d9a/classG4OpticalParameters.html", "d5/d9a/classG4OpticalParameters" ],
    [ "G4OpticalProcessIndex", "df/d1e/G4OpticalParameters_8hh.html#a0c8e58cf1dbdd779ea845962efc87571", [
      [ "kCerenkov", "df/d1e/G4OpticalParameters_8hh.html#a0c8e58cf1dbdd779ea845962efc87571addeb9cdbcbaa5a1783bd213559a60ca9", null ],
      [ "kScintillation", "df/d1e/G4OpticalParameters_8hh.html#a0c8e58cf1dbdd779ea845962efc87571a323113f5d10df7be52c4fa0d3d970186", null ],
      [ "kAbsorption", "df/d1e/G4OpticalParameters_8hh.html#a0c8e58cf1dbdd779ea845962efc87571a87747de320f118511e9ac8d7a97d4aa9", null ],
      [ "kRayleigh", "df/d1e/G4OpticalParameters_8hh.html#a0c8e58cf1dbdd779ea845962efc87571a712a52fbeb2e155d194e15fb97f750a9", null ],
      [ "kMieHG", "df/d1e/G4OpticalParameters_8hh.html#a0c8e58cf1dbdd779ea845962efc87571af72e02e49b20d3cb9eb8ad9833bd02cf", null ],
      [ "kBoundary", "df/d1e/G4OpticalParameters_8hh.html#a0c8e58cf1dbdd779ea845962efc87571a93a29d25eca85e55bb259ffd3d349d03", null ],
      [ "kWLS", "df/d1e/G4OpticalParameters_8hh.html#a0c8e58cf1dbdd779ea845962efc87571a1f50dc3e53a96460bc26579d9e053dd6", null ],
      [ "kWLS2", "df/d1e/G4OpticalParameters_8hh.html#a0c8e58cf1dbdd779ea845962efc87571a4d8395b7b44373c6213ed15911efeea6", null ],
      [ "kNoProcess", "df/d1e/G4OpticalParameters_8hh.html#a0c8e58cf1dbdd779ea845962efc87571aab0331ff22148a2facb4c53466676150", null ]
    ] ],
    [ "G4OpticalProcessName", "df/d1e/G4OpticalParameters_8hh.html#aefa0318d973cd102ddf94741372b4e42", null ]
];