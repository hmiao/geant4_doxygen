var classG4XResonance =
[
    [ "G4XResonance", "df/d1e/classG4XResonance.html#a403105f05bf8af0a8e91ef04f7d5313e", null ],
    [ "~G4XResonance", "df/d1e/classG4XResonance.html#aff05c7a514f55d03582721af7a826e0b", null ],
    [ "G4XResonance", "df/d1e/classG4XResonance.html#ae73faf758ba468e227c3b50633a3a809", null ],
    [ "CrossSection", "df/d1e/classG4XResonance.html#aba09ac2fb3b65b07e4996120fccf91ed", null ],
    [ "GetComponents", "df/d1e/classG4XResonance.html#ac72b35e41874bc28cae525849e4a8b26", null ],
    [ "Name", "df/d1e/classG4XResonance.html#aa8eac6133e612298728a7f5e3a5d253e", null ],
    [ "operator!=", "df/d1e/classG4XResonance.html#a7740f8f0953b92b8ffa42d7d181bbbc4", null ],
    [ "operator=", "df/d1e/classG4XResonance.html#a16c8c8a4f592f1c3192f757846b87105", null ],
    [ "operator==", "df/d1e/classG4XResonance.html#aa7ef6f7480132f16bd69e61802324f82", null ],
    [ "isoOut1", "df/d1e/classG4XResonance.html#a99b469a45988504d3ed87b810a8327ec", null ],
    [ "isoOut2", "df/d1e/classG4XResonance.html#a26e81ddf4df78e9b04dddcefe4e956c5", null ],
    [ "iSpinOut1", "df/d1e/classG4XResonance.html#aa389cf06dc9bcacd782aab623b3be0b0", null ],
    [ "iSpinOut2", "df/d1e/classG4XResonance.html#a925c66a0c556241c6b4c4b2c87dcb963", null ],
    [ "mOut1", "df/d1e/classG4XResonance.html#a1cd02d96bdd772e6713425ce5c29d86e", null ],
    [ "mOut2", "df/d1e/classG4XResonance.html#a9b46205a80515e7dae620d1bf0da4415", null ],
    [ "name", "df/d1e/classG4XResonance.html#a88b0aca2d514459f52b77fef00342ae6", null ],
    [ "table", "df/d1e/classG4XResonance.html#a83ed3bc67858e12cabc0e79faa7ad2a8", null ]
];