var classG4PolarizedGammaConversion =
[
    [ "G4PolarizedGammaConversion", "df/dcd/classG4PolarizedGammaConversion.html#a77cb9fbd089b98da6381edae218739b3", null ],
    [ "~G4PolarizedGammaConversion", "df/dcd/classG4PolarizedGammaConversion.html#a659f4f8d9bd1e969745e9539a398f1dc", null ],
    [ "G4PolarizedGammaConversion", "df/dcd/classG4PolarizedGammaConversion.html#ae976772245db2ce0a16ff4e7450959e6", null ],
    [ "DumpInfo", "df/dcd/classG4PolarizedGammaConversion.html#a2cb74b739b605062bc6ac9503aa2f2e3", null ],
    [ "InitialiseProcess", "df/dcd/classG4PolarizedGammaConversion.html#ae3aafd3605b6c4e72dbbb5271d274e74", null ],
    [ "IsApplicable", "df/dcd/classG4PolarizedGammaConversion.html#acd042bc0bd865292a70c17b28173df7c", null ],
    [ "operator=", "df/dcd/classG4PolarizedGammaConversion.html#ae47d82ba849cc2db14627df5756cbd4f", null ],
    [ "ProcessDescription", "df/dcd/classG4PolarizedGammaConversion.html#ab289377a660d85a415bf3a7d29bb3d4e", null ],
    [ "fIsInitialised", "df/dcd/classG4PolarizedGammaConversion.html#adfb4ee3dc979af7db4bf3de2f409f557", null ]
];