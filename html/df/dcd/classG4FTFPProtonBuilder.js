var classG4FTFPProtonBuilder =
[
    [ "G4FTFPProtonBuilder", "df/dcd/classG4FTFPProtonBuilder.html#a9803150930521fa277eacf9a9d3b22a1", null ],
    [ "~G4FTFPProtonBuilder", "df/dcd/classG4FTFPProtonBuilder.html#a47b61f7de655e0623715804cb667dd10", null ],
    [ "Build", "df/dcd/classG4FTFPProtonBuilder.html#aef3bbcfcca6cc3d822c7dd70f7aabdf2", null ],
    [ "Build", "df/dcd/classG4FTFPProtonBuilder.html#a7a534415d8d1a3953326b85a9936955e", null ],
    [ "Build", "df/dcd/classG4FTFPProtonBuilder.html#a82366f71fa408e12169e31c116f93410", null ],
    [ "Build", "df/dcd/classG4FTFPProtonBuilder.html#a7d7a3fe1fc3f798f594a92694411bb49", null ],
    [ "SetMaxEnergy", "df/dcd/classG4FTFPProtonBuilder.html#aa5bf0a47d2d64bf3c51a11bbfa826ff3", null ],
    [ "SetMinEnergy", "df/dcd/classG4FTFPProtonBuilder.html#af513f705f53544e12da40832ab9d5375", null ],
    [ "theMax", "df/dcd/classG4FTFPProtonBuilder.html#a66e33e8c3b039f4914369498a46ac07f", null ],
    [ "theMin", "df/dcd/classG4FTFPProtonBuilder.html#a72b85653f785db052ac9aaaef1a1dda0", null ],
    [ "theModel", "df/dcd/classG4FTFPProtonBuilder.html#af2004882723c2e53d65a7d2e9f5a7f6b", null ]
];