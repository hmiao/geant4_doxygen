var classG4PrimaryParticle =
[
    [ "G4PrimaryParticle", "df/d7e/classG4PrimaryParticle.html#a267778cb22e8b4a32ceb3d4cbbf2db0a", null ],
    [ "G4PrimaryParticle", "df/d7e/classG4PrimaryParticle.html#a128c9e863a852dd71fb56e0385fe204c", null ],
    [ "G4PrimaryParticle", "df/d7e/classG4PrimaryParticle.html#adbf3da2a6a55fb4ebb5ee9691fa6171b", null ],
    [ "G4PrimaryParticle", "df/d7e/classG4PrimaryParticle.html#aed90b2a0ddab4d87c881a1eada77d888", null ],
    [ "G4PrimaryParticle", "df/d7e/classG4PrimaryParticle.html#ad65b677bd0d5b85d67643d0e05b70659", null ],
    [ "G4PrimaryParticle", "df/d7e/classG4PrimaryParticle.html#a650a363e79b48d6281252c4dad243552", null ],
    [ "G4PrimaryParticle", "df/d7e/classG4PrimaryParticle.html#a4d1456feba943eef773fe82fcc245103", null ],
    [ "~G4PrimaryParticle", "df/d7e/classG4PrimaryParticle.html#adff31652163a547a498553013b952208", null ],
    [ "G4PrimaryParticle", "df/d7e/classG4PrimaryParticle.html#a1737dc6c88d9c97fb5172961ed9856fc", null ],
    [ "ClearNext", "df/d7e/classG4PrimaryParticle.html#a3da78fa685d5a47bb8d1ca50c87437d3", null ],
    [ "GetCharge", "df/d7e/classG4PrimaryParticle.html#a1252f06848f9bf9e648049bf496b3ec6", null ],
    [ "GetDaughter", "df/d7e/classG4PrimaryParticle.html#ad6169ffec803f06f2ff944d242fe8cab", null ],
    [ "GetG4code", "df/d7e/classG4PrimaryParticle.html#aff4fc3d4d8e9fd8921c921ec57ba9bbe", null ],
    [ "GetKineticEnergy", "df/d7e/classG4PrimaryParticle.html#a2065262303a42362ee7f546d585a2796", null ],
    [ "GetMass", "df/d7e/classG4PrimaryParticle.html#a870d96657abdae35c1341406e5294b65", null ],
    [ "GetMomentum", "df/d7e/classG4PrimaryParticle.html#a96e61950aefa53c63bf07a18b6d8f880", null ],
    [ "GetMomentumDirection", "df/d7e/classG4PrimaryParticle.html#a40493cef343770f819e5dcac11ace0e6", null ],
    [ "GetNext", "df/d7e/classG4PrimaryParticle.html#a5f137fdf1b4af4fecd2c4c942cec90cd", null ],
    [ "GetParticleDefinition", "df/d7e/classG4PrimaryParticle.html#af3ffebf1efb031b7ee138520e74fff0f", null ],
    [ "GetPDGcode", "df/d7e/classG4PrimaryParticle.html#a97ef20ba5c0c6dc277028846e1c3dce0", null ],
    [ "GetPolarization", "df/d7e/classG4PrimaryParticle.html#a5ddef909ddacc5156b2cea5d3b2f9bf5", null ],
    [ "GetPolX", "df/d7e/classG4PrimaryParticle.html#ace1b8f0c1454be24de5c3c0ea18a3bcc", null ],
    [ "GetPolY", "df/d7e/classG4PrimaryParticle.html#a4faf95234abc8f520b5314177a012578", null ],
    [ "GetPolZ", "df/d7e/classG4PrimaryParticle.html#aaa3b2553f7fbc57d04e332a47fe88c58", null ],
    [ "GetProperTime", "df/d7e/classG4PrimaryParticle.html#a3e15aadf3d9b55fee417d885f1cb8829", null ],
    [ "GetPx", "df/d7e/classG4PrimaryParticle.html#aa97f4737ffd49c4d397d1c1c64affacf", null ],
    [ "GetPy", "df/d7e/classG4PrimaryParticle.html#a71adc0b61ba291dc0f7afc9a8dae2e04", null ],
    [ "GetPz", "df/d7e/classG4PrimaryParticle.html#a9dfb36c060777241f41bf677b9c00071", null ],
    [ "GetTotalEnergy", "df/d7e/classG4PrimaryParticle.html#a03909e1b6025acde5a48036960747224", null ],
    [ "GetTotalMomentum", "df/d7e/classG4PrimaryParticle.html#a9482c1ead6f04bf94520f8c7162d1de8", null ],
    [ "GetTrackID", "df/d7e/classG4PrimaryParticle.html#a830adaefe2c7d7ae6614ed44b93d5633", null ],
    [ "GetUserInformation", "df/d7e/classG4PrimaryParticle.html#a39e6d090b249204a06c3db343216b37f", null ],
    [ "GetWeight", "df/d7e/classG4PrimaryParticle.html#a047a2a3ebeffa9ce0de5793650d252cc", null ],
    [ "operator delete", "df/d7e/classG4PrimaryParticle.html#abf02fe31ace2b9363bf6cdca4984905a", null ],
    [ "operator new", "df/d7e/classG4PrimaryParticle.html#a495df3498f34b9ee04205a3fdb23f97d", null ],
    [ "operator!=", "df/d7e/classG4PrimaryParticle.html#a7166bc6a04afc91b139faf47e96f36dc", null ],
    [ "operator=", "df/d7e/classG4PrimaryParticle.html#a64c623cdda61a12c59d0d8329030e30d", null ],
    [ "operator==", "df/d7e/classG4PrimaryParticle.html#a23d93f63a0be02d7f7fdbe3669444ded", null ],
    [ "Print", "df/d7e/classG4PrimaryParticle.html#a58c06bd5e8cdd336cc188a43491f97d8", null ],
    [ "Set4Momentum", "df/d7e/classG4PrimaryParticle.html#a71a3aba03097967a260568aedb98a44c", null ],
    [ "SetCharge", "df/d7e/classG4PrimaryParticle.html#a2c742ead33c25cd673e069fe99dd57d1", null ],
    [ "SetDaughter", "df/d7e/classG4PrimaryParticle.html#ab407ff15ef7ff15021b9e7bc83f6a23b", null ],
    [ "SetG4code", "df/d7e/classG4PrimaryParticle.html#a1bc43584242085b214a98e2cba4bfa87", null ],
    [ "SetKineticEnergy", "df/d7e/classG4PrimaryParticle.html#a6bf5d884c9134b54923afa830ab0cae4", null ],
    [ "SetMass", "df/d7e/classG4PrimaryParticle.html#a9a1323191192c663fffe38cbefa315b1", null ],
    [ "SetMomentum", "df/d7e/classG4PrimaryParticle.html#a796d75338e5a296f61176f7711744723", null ],
    [ "SetMomentumDirection", "df/d7e/classG4PrimaryParticle.html#a95efb520a8f56abc60d22d891822ab4d", null ],
    [ "SetNext", "df/d7e/classG4PrimaryParticle.html#a5cc6ce76c958eaaccd8621cbf5328d69", null ],
    [ "SetParticleDefinition", "df/d7e/classG4PrimaryParticle.html#ab28d2f20d64f4582e4bbcc8e2415bd64", null ],
    [ "SetPDGcode", "df/d7e/classG4PrimaryParticle.html#a015b7595ba195ca90c5657ca12a201f9", null ],
    [ "SetPolarization", "df/d7e/classG4PrimaryParticle.html#a53b70d9a9f83c52bbbc267f2b458c8ed", null ],
    [ "SetPolarization", "df/d7e/classG4PrimaryParticle.html#a7391622a397223d476a10841b03bd91f", null ],
    [ "SetProperTime", "df/d7e/classG4PrimaryParticle.html#aa285e0e34e0d64372131c762b6bd2956", null ],
    [ "SetTotalEnergy", "df/d7e/classG4PrimaryParticle.html#a06fee60e95d774f6eb6a44a93642d878", null ],
    [ "SetTrackID", "df/d7e/classG4PrimaryParticle.html#a582072922d0938b1da7b2d5eac617e1d", null ],
    [ "SetUserInformation", "df/d7e/classG4PrimaryParticle.html#aa4a5782ed993b918dc85d6e4e668c581", null ],
    [ "SetWeight", "df/d7e/classG4PrimaryParticle.html#a718305b97cc3f3b04dad3c48a89fc0b7", null ],
    [ "charge", "df/d7e/classG4PrimaryParticle.html#a29e56ecfaba74a78d27b4aacc452208a", null ],
    [ "daughterParticle", "df/d7e/classG4PrimaryParticle.html#a47ebfafb2e612d38eeb6dab1f51e2e51", null ],
    [ "direction", "df/d7e/classG4PrimaryParticle.html#a8797e6b0cd306026cfb293797d9a24f1", null ],
    [ "G4code", "df/d7e/classG4PrimaryParticle.html#a52caa177bc0f8d57d24e5fd60109d5ac", null ],
    [ "kinE", "df/d7e/classG4PrimaryParticle.html#ac05095932e22b9d9b889ec81a4c9f817", null ],
    [ "mass", "df/d7e/classG4PrimaryParticle.html#a472bf66faadfb449abdf090fd4973ad7", null ],
    [ "nextParticle", "df/d7e/classG4PrimaryParticle.html#afe52c3384a6a45e7da07f71d52f412fe", null ],
    [ "PDGcode", "df/d7e/classG4PrimaryParticle.html#aa5ff7940ee7d203bf2762aad00df1442", null ],
    [ "polX", "df/d7e/classG4PrimaryParticle.html#a1939914cc01eb8898ac9fdc7effa0e42", null ],
    [ "polY", "df/d7e/classG4PrimaryParticle.html#aa6b6b3bde05f7e2aff4051b685224da5", null ],
    [ "polZ", "df/d7e/classG4PrimaryParticle.html#a8f53fc4ba279b3789297238a68db80d8", null ],
    [ "properTime", "df/d7e/classG4PrimaryParticle.html#a9490eaa18ef5680f99f98c9b9bbc26aa", null ],
    [ "trackID", "df/d7e/classG4PrimaryParticle.html#a5e16908831ab1bdb0e26176ed44afc83", null ],
    [ "userInfo", "df/d7e/classG4PrimaryParticle.html#a35aefadfd56b51536fcda7be250c491f", null ],
    [ "Weight0", "df/d7e/classG4PrimaryParticle.html#a48621e43d02a30368aff4d0c1b45b327", null ]
];