var classG4ProtonPHPBuilder =
[
    [ "G4ProtonPHPBuilder", "df/da8/classG4ProtonPHPBuilder.html#a2a236df6906c21a1aa0addf9e6886c44", null ],
    [ "~G4ProtonPHPBuilder", "df/da8/classG4ProtonPHPBuilder.html#a98649f1d374f05978b4bbc93a4995530", null ],
    [ "Build", "df/da8/classG4ProtonPHPBuilder.html#a88a806018cc615101737b0e9353f7ac3", null ],
    [ "Build", "df/da8/classG4ProtonPHPBuilder.html#a7a534415d8d1a3953326b85a9936955e", null ],
    [ "Build", "df/da8/classG4ProtonPHPBuilder.html#a55dcce7aa4dc3161b6ae59ed5a7dab81", null ],
    [ "Build", "df/da8/classG4ProtonPHPBuilder.html#a7d7a3fe1fc3f798f594a92694411bb49", null ],
    [ "SetMaxEnergy", "df/da8/classG4ProtonPHPBuilder.html#a5c03d8659da8c6b0a1638904a32498bf", null ],
    [ "SetMinEnergy", "df/da8/classG4ProtonPHPBuilder.html#a1b9ada9d09520436ac3e530c2a8b02b6", null ],
    [ "theMax", "df/da8/classG4ProtonPHPBuilder.html#a3045b0c69132f4af42ca1b03e92b94a4", null ],
    [ "theMin", "df/da8/classG4ProtonPHPBuilder.html#acd2f3c40011b45744d931ad9bd94f3a6", null ]
];