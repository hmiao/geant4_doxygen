var classG4NeutronGEMChannel =
[
    [ "G4NeutronGEMChannel", "df/dad/classG4NeutronGEMChannel.html#a563098253786deecb8a5fc563e998979", null ],
    [ "~G4NeutronGEMChannel", "df/dad/classG4NeutronGEMChannel.html#a950cb50fba5f188f395e8eb197f8ac04", null ],
    [ "G4NeutronGEMChannel", "df/dad/classG4NeutronGEMChannel.html#a408aae9d8c8631091494a70c495f1f5b", null ],
    [ "operator!=", "df/dad/classG4NeutronGEMChannel.html#a09bd2684b440dc2322090255f2893063", null ],
    [ "operator=", "df/dad/classG4NeutronGEMChannel.html#afbb22105e93d500bd5da7c6ac84f3a11", null ],
    [ "operator==", "df/dad/classG4NeutronGEMChannel.html#a14007e73d213ca2361ea4c4e33c5e55f", null ],
    [ "theEvaporationProbability", "df/dad/classG4NeutronGEMChannel.html#a412d824fcabda77f50e580d9e12a7ee8", null ]
];