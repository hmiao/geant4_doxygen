var classG4ParameterisationBoxY =
[
    [ "G4ParameterisationBoxY", "df/d37/classG4ParameterisationBoxY.html#a2b9dd1d0b1951e6ff3e222251230d6d9", null ],
    [ "~G4ParameterisationBoxY", "df/d37/classG4ParameterisationBoxY.html#acfa4687da0b9bd776a80a1665e47f4ac", null ],
    [ "ComputeDimensions", "df/d37/classG4ParameterisationBoxY.html#ad95794be2500421eee1649562d2f9c5e", null ],
    [ "ComputeDimensions", "df/d37/classG4ParameterisationBoxY.html#ac071b67697573dd8d9b32b6db577f043", null ],
    [ "ComputeDimensions", "df/d37/classG4ParameterisationBoxY.html#a92b00fa181633e0722eb64d1c14731b2", null ],
    [ "ComputeDimensions", "df/d37/classG4ParameterisationBoxY.html#a9fe877c0628555fcf151d83f733b4693", null ],
    [ "ComputeDimensions", "df/d37/classG4ParameterisationBoxY.html#a512da0fd644b4c766255ff0bb2b4af84", null ],
    [ "ComputeDimensions", "df/d37/classG4ParameterisationBoxY.html#aa2db0e3da185a282a40261fefc7d1f8e", null ],
    [ "ComputeDimensions", "df/d37/classG4ParameterisationBoxY.html#a72e196a2b8144c5509b40946e71b3427", null ],
    [ "ComputeDimensions", "df/d37/classG4ParameterisationBoxY.html#a25b0a236531fc12c83c80511036c9775", null ],
    [ "ComputeDimensions", "df/d37/classG4ParameterisationBoxY.html#a59d14c83d5fdacf3d08008116cdfd389", null ],
    [ "ComputeDimensions", "df/d37/classG4ParameterisationBoxY.html#a8736db32a7052a574f4e007648bb31cc", null ],
    [ "ComputeDimensions", "df/d37/classG4ParameterisationBoxY.html#a9bc3a0325f5b767da4505fab027ab613", null ],
    [ "ComputeDimensions", "df/d37/classG4ParameterisationBoxY.html#ace834d7f1124ffa50efa12bd774952c6", null ],
    [ "ComputeDimensions", "df/d37/classG4ParameterisationBoxY.html#afb001a2640ba5913a7548e6742101632", null ],
    [ "ComputeTransformation", "df/d37/classG4ParameterisationBoxY.html#a230f47f2de094acef3751708f5338c4b", null ],
    [ "GetMaxParameter", "df/d37/classG4ParameterisationBoxY.html#ac33b1488cc7c8c9c6184949c1e9155e0", null ]
];