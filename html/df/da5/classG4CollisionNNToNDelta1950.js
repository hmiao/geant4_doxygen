var classG4CollisionNNToNDelta1950 =
[
    [ "G4CollisionNNToNDelta1950", "df/da5/classG4CollisionNNToNDelta1950.html#adea2eac8ad6555176b6e52e22d28cbe8", null ],
    [ "~G4CollisionNNToNDelta1950", "df/da5/classG4CollisionNNToNDelta1950.html#aa0780c9d8abf6bacd555d49f4d4a8d24", null ],
    [ "G4CollisionNNToNDelta1950", "df/da5/classG4CollisionNNToNDelta1950.html#a07755ae435dc8a8d9d6c6d6fd79028c3", null ],
    [ "GetComponents", "df/da5/classG4CollisionNNToNDelta1950.html#ab9585d84c549b234ba46a66d2169a97d", null ],
    [ "GetListOfColliders", "df/da5/classG4CollisionNNToNDelta1950.html#ac9552f100234642cd94f0560835fe88e", null ],
    [ "GetName", "df/da5/classG4CollisionNNToNDelta1950.html#a56e97775bf5bf40a940264d79b1bd5f1", null ],
    [ "operator=", "df/da5/classG4CollisionNNToNDelta1950.html#a9725bfef646441ff49e2801628f4c662", null ],
    [ "components", "df/da5/classG4CollisionNNToNDelta1950.html#a74714d7bd956257d94a7c87a55922bfd", null ]
];