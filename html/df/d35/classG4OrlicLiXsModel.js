var classG4OrlicLiXsModel =
[
    [ "G4OrlicLiXsModel", "df/d35/classG4OrlicLiXsModel.html#ac20cb3648af68ee2cc70f4fb61d811fe", null ],
    [ "~G4OrlicLiXsModel", "df/d35/classG4OrlicLiXsModel.html#add92f44e2838290ea513fa730781dff8", null ],
    [ "G4OrlicLiXsModel", "df/d35/classG4OrlicLiXsModel.html#ae385c04d23d9f8a429df3258a40494c2", null ],
    [ "CalculateL1CrossSection", "df/d35/classG4OrlicLiXsModel.html#a739403f053900b4cc4e15f38a05f20dc", null ],
    [ "CalculateL2CrossSection", "df/d35/classG4OrlicLiXsModel.html#a703abf13988f4fca59a7a77bf7d958b5", null ],
    [ "CalculateL3CrossSection", "df/d35/classG4OrlicLiXsModel.html#aac3f2864d76af144da1ceccde8330627", null ],
    [ "operator=", "df/d35/classG4OrlicLiXsModel.html#a1fb26efd10d2e100043967c2f4fc0e65", null ],
    [ "transitionManager", "df/d35/classG4OrlicLiXsModel.html#aa94889971cdfafa7680eb3700fa27224", null ]
];