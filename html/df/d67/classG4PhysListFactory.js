var classG4PhysListFactory =
[
    [ "G4PhysListFactory", "df/d67/classG4PhysListFactory.html#a84acaba09781e8038b08cd98603ecf36", null ],
    [ "~G4PhysListFactory", "df/d67/classG4PhysListFactory.html#ac48ddf6eba98a247238e167fbb04dbcb", null ],
    [ "AvailablePhysLists", "df/d67/classG4PhysListFactory.html#aef2c788438ea1e2bde9a8bbb97163fc5", null ],
    [ "AvailablePhysListsEM", "df/d67/classG4PhysListFactory.html#a3f2046ba2ce4cf936f091fcec598bcb4", null ],
    [ "GetReferencePhysList", "df/d67/classG4PhysListFactory.html#ad393640a0853572ed788fa0f41ac311e", null ],
    [ "IsReferencePhysList", "df/d67/classG4PhysListFactory.html#a7cd7d8d1c46c29e02b7a145c0741bbf6", null ],
    [ "ReferencePhysList", "df/d67/classG4PhysListFactory.html#a91af5f7d1e55451805e8d588532b29e4", null ],
    [ "SetVerbose", "df/d67/classG4PhysListFactory.html#a2d6cd6e3bc0bf9b0c6bc32f1d10dd975", null ],
    [ "defName", "df/d67/classG4PhysListFactory.html#a9ecf589aa1396cb1f5c441642738d8d0", null ],
    [ "listnames_em", "df/d67/classG4PhysListFactory.html#a5981128c0804a2f40ca5143a0594cdb7", null ],
    [ "listnames_hadr", "df/d67/classG4PhysListFactory.html#a036367db716953c3775db669eefc70c4", null ],
    [ "nlists_em", "df/d67/classG4PhysListFactory.html#a0b354948c80b4b0de6e76f64eb205f45", null ],
    [ "nlists_hadr", "df/d67/classG4PhysListFactory.html#aeabc32658b46a7dfa3338ca969be274a", null ],
    [ "theMessenger", "df/d67/classG4PhysListFactory.html#a4dc8c2f762f5e8ba98d7c57163cd6fdb", null ],
    [ "verbose", "df/d67/classG4PhysListFactory.html#a3d880c839a10d7e62dede27560218f1c", null ]
];