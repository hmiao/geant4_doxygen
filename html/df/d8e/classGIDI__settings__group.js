var classGIDI__settings__group =
[
    [ "GIDI_settings_group", "df/d8e/classGIDI__settings__group.html#ad211736334bd5ba0a6493bfd57edf7be", null ],
    [ "GIDI_settings_group", "df/d8e/classGIDI__settings__group.html#a22d380ab4f1d4d2dfbc03fca556b4852", null ],
    [ "GIDI_settings_group", "df/d8e/classGIDI__settings__group.html#aeac5a987e72e475df92dc0fb62ea8507", null ],
    [ "GIDI_settings_group", "df/d8e/classGIDI__settings__group.html#acc554987fc3ada5e14959677b942365d", null ],
    [ "~GIDI_settings_group", "df/d8e/classGIDI__settings__group.html#a70ea0a2650e44064c1c9021b2e152359", null ],
    [ "getGroupIndexFromEnergy", "df/d8e/classGIDI__settings__group.html#aedadd54a75cc31c024bdfa924db5db6b", null ],
    [ "getLabel", "df/d8e/classGIDI__settings__group.html#ad63c15516172e4a2d32eb00454a94927", null ],
    [ "getNumberOfGroups", "df/d8e/classGIDI__settings__group.html#aaaf51f36cebed61865640e4267681565", null ],
    [ "initialize", "df/d8e/classGIDI__settings__group.html#a01201206086010337b3d6ec32a2d61c2", null ],
    [ "isLabel", "df/d8e/classGIDI__settings__group.html#a1ec4536d3a45ad6bc22a16829843c535", null ],
    [ "operator=", "df/d8e/classGIDI__settings__group.html#a80600f909c56681f312ffdccbb89307b", null ],
    [ "operator[]", "df/d8e/classGIDI__settings__group.html#a4dada278c3d72c0b08de37803f815013", null ],
    [ "pointer", "df/d8e/classGIDI__settings__group.html#a8e667e80801f5826ed7819b56977f825", null ],
    [ "print", "df/d8e/classGIDI__settings__group.html#aa607f00eb9eaf47740863358d58119f1", null ],
    [ "setFromCDoubleArray", "df/d8e/classGIDI__settings__group.html#aefe220a0d79d151bcd29eba20760f16b", null ],
    [ "size", "df/d8e/classGIDI__settings__group.html#a91bf53d8b904bb528555bef122c43886", null ],
    [ "mBoundaries", "df/d8e/classGIDI__settings__group.html#a48bd2619d71bd47cf5009698cda176bd", null ],
    [ "mLabel", "df/d8e/classGIDI__settings__group.html#a8463759596d5ba65687c075a79d25e33", null ]
];