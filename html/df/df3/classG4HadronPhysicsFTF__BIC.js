var classG4HadronPhysicsFTF__BIC =
[
    [ "G4HadronPhysicsFTF_BIC", "df/df3/classG4HadronPhysicsFTF__BIC.html#a07248f197bc80d39a5747cff69d1de2c", null ],
    [ "G4HadronPhysicsFTF_BIC", "df/df3/classG4HadronPhysicsFTF__BIC.html#ab72bab59a7fc450fc6c901ca607e3e77", null ],
    [ "~G4HadronPhysicsFTF_BIC", "df/df3/classG4HadronPhysicsFTF__BIC.html#a83a9fa0adacca264b4d55dd750c07982", null ],
    [ "G4HadronPhysicsFTF_BIC", "df/df3/classG4HadronPhysicsFTF__BIC.html#afb81a83c2a7726f6ab4be0f160c9f71f", null ],
    [ "Kaon", "df/df3/classG4HadronPhysicsFTF__BIC.html#af256fc731fa01c0312e1ea4676073d80", null ],
    [ "Neutron", "df/df3/classG4HadronPhysicsFTF__BIC.html#a7ae8b5186595a3b2e7723f29462d69ae", null ],
    [ "operator=", "df/df3/classG4HadronPhysicsFTF__BIC.html#abf5693e572bce403b076eaffe17c5f0e", null ],
    [ "Pion", "df/df3/classG4HadronPhysicsFTF__BIC.html#af040c83b5f3e78c1783d837a9edb6276", null ],
    [ "Proton", "df/df3/classG4HadronPhysicsFTF__BIC.html#aac2a760cc55487a015389b46c3441add", null ],
    [ "maxBIC_pion", "df/df3/classG4HadronPhysicsFTF__BIC.html#a93b71664730e693889cb31fae9662e5e", null ],
    [ "minBERT_pion", "df/df3/classG4HadronPhysicsFTF__BIC.html#a8f334e87d17c1456c5417ea732531f74", null ]
];