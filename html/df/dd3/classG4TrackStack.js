var classG4TrackStack =
[
    [ "G4TrackStack", "df/dd3/classG4TrackStack.html#a0f104d281001795e82cbc62ab3bfae1b", null ],
    [ "G4TrackStack", "df/dd3/classG4TrackStack.html#ac8f31e585634049283e5957b5035ea36", null ],
    [ "~G4TrackStack", "df/dd3/classG4TrackStack.html#aebfc8edad01bba1c594df4b4c859d4ce", null ],
    [ "clearAndDestroy", "df/dd3/classG4TrackStack.html#a25935ffde741c820dfdc133471e3ab25", null ],
    [ "GetMaxNTrack", "df/dd3/classG4TrackStack.html#aefdde22937ca0402d33c2c9f87570ccf", null ],
    [ "GetNStick", "df/dd3/classG4TrackStack.html#af7286e0e2f95b8d956084e4a4d710d87", null ],
    [ "GetNTrack", "df/dd3/classG4TrackStack.html#acc61afe9ac432824027760a4a6e9e9bb", null ],
    [ "GetSafetyValue1", "df/dd3/classG4TrackStack.html#aeec3dddeb718e0002a7cd41b1ff64482", null ],
    [ "GetSafetyValue2", "df/dd3/classG4TrackStack.html#a4162e71c3a0121bfaa0af2ec82443b81", null ],
    [ "getTotalEnergy", "df/dd3/classG4TrackStack.html#a80a6875e8ec7b8946d8b5a3b71c42cd0", null ],
    [ "operator!=", "df/dd3/classG4TrackStack.html#a27a45c57effcd0389f5f4090254d841b", null ],
    [ "operator=", "df/dd3/classG4TrackStack.html#ae274980710c79eb4a7fef4b9643f69b2", null ],
    [ "operator==", "df/dd3/classG4TrackStack.html#a858a20bb950c262ff6da83ca3aded965", null ],
    [ "PopFromStack", "df/dd3/classG4TrackStack.html#aed5885ad54aae2c0666754aac1a78786", null ],
    [ "PushToStack", "df/dd3/classG4TrackStack.html#a179fd4c2bd5e101ed96ddc24eb55b83d", null ],
    [ "SetSafetyValue2", "df/dd3/classG4TrackStack.html#a841e546db7b321a0bb47bdf05e074623", null ],
    [ "TransferTo", "df/dd3/classG4TrackStack.html#a252d89a90d8f5fd5acbc5b1111336ac6", null ],
    [ "TransferTo", "df/dd3/classG4TrackStack.html#a787705bc344a74d83b27becb098c0c91", null ],
    [ "nstick", "df/dd3/classG4TrackStack.html#a14f91c3939d0940ddae11caa186aa112", null ],
    [ "safetyValue1", "df/dd3/classG4TrackStack.html#aac317f20a984a5798b825a9a30691bc9", null ],
    [ "safetyValue2", "df/dd3/classG4TrackStack.html#a531cb4fa996cc6a0fe6f0e96362c0d17", null ]
];