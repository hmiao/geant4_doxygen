var classG4ShellCorrection =
[
    [ "G4ShellCorrection", "df/d24/classG4ShellCorrection.html#a8f5309c0a663476fab447e0e537ed3a9", null ],
    [ "~G4ShellCorrection", "df/d24/classG4ShellCorrection.html#a995f19becdb43c2407a6018e5c2ddef7", null ],
    [ "GetCameronShellPlusPairingCorrections", "df/d24/classG4ShellCorrection.html#a639d04b49ceb5bbfc0b2c5192388a0cd", null ],
    [ "GetCameronTruranHilfShellCorrections", "df/d24/classG4ShellCorrection.html#a51153720cd41c5ad921b78e4b3c0a848", null ],
    [ "GetShellCorrection", "df/d24/classG4ShellCorrection.html#afe24e718eea79b325679893a7dc00548", null ],
    [ "theCameronGilbertShellCorrections", "df/d24/classG4ShellCorrection.html#abcbe9046a5fed9d9bcb3345ac9bb12e0", null ],
    [ "theCameronShellPlusPairingCorrections", "df/d24/classG4ShellCorrection.html#a14c3bfb0af0907004720ac0296ba12db", null ],
    [ "theCameronTruranHilfShellCorrections", "df/d24/classG4ShellCorrection.html#afd4d1e192b46d20aeb977e3dfc13b8ed", null ],
    [ "theCookShellCorrections", "df/d24/classG4ShellCorrection.html#ac7df5afc0bf1de4a932363abd00a9e8f", null ]
];