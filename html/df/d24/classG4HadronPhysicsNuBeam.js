var classG4HadronPhysicsNuBeam =
[
    [ "G4HadronPhysicsNuBeam", "df/d24/classG4HadronPhysicsNuBeam.html#a29c5e4320c903ed063cfa5ca9b2fa579", null ],
    [ "G4HadronPhysicsNuBeam", "df/d24/classG4HadronPhysicsNuBeam.html#a60c064d4003d8aa34c13b8bf7449ab87", null ],
    [ "~G4HadronPhysicsNuBeam", "df/d24/classG4HadronPhysicsNuBeam.html#a72ab7a588327027d18daba905823b988", null ],
    [ "G4HadronPhysicsNuBeam", "df/d24/classG4HadronPhysicsNuBeam.html#a827dc3a1710f71ad759d0b9c30ae0015", null ],
    [ "ConstructProcess", "df/d24/classG4HadronPhysicsNuBeam.html#a5160103dcbc7bb75d5b36e2b31e5ece7", null ],
    [ "operator=", "df/d24/classG4HadronPhysicsNuBeam.html#a278bee0bf099c8bec9f1d9fb79752bec", null ],
    [ "Proton", "df/d24/classG4HadronPhysicsNuBeam.html#a32d81e563061bb17b649fb95d886d410", null ],
    [ "maxFTFP_proton", "df/d24/classG4HadronPhysicsNuBeam.html#a0f1236088e2c92ccd01d895507397d9b", null ]
];