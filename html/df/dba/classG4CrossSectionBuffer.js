var classG4CrossSectionBuffer =
[
    [ "G4CrossSectionBuffer", "df/dba/classG4CrossSectionBuffer.html#abe16b456077670d0b56cee5c108d8f8a", null ],
    [ "CrossSection", "df/dba/classG4CrossSectionBuffer.html#ad46391fec30f17d7c10935d5936949dc", null ],
    [ "InCharge", "df/dba/classG4CrossSectionBuffer.html#a6ecc1d6f1898f2bebe2f391f371c094b", null ],
    [ "Print", "df/dba/classG4CrossSectionBuffer.html#ab7755b3cce55a008072b18cceed1eb37", null ],
    [ "push_back", "df/dba/classG4CrossSectionBuffer.html#a8c273f301f4bed7d9c88d83b7be99553", null ],
    [ "theA", "df/dba/classG4CrossSectionBuffer.html#ab494cb69a47d86e53fe513c917c42328", null ],
    [ "theB", "df/dba/classG4CrossSectionBuffer.html#a9e2f1b4a0f6b6a85ffd83625d809a951", null ],
    [ "theData", "df/dba/classG4CrossSectionBuffer.html#a9950fb1d48f9c0ae6855c28c1dbfd82e", null ]
];