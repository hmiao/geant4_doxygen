var classG4HETCChargedFragment =
[
    [ "G4HETCChargedFragment", "df/d63/classG4HETCChargedFragment.html#aa86f5d9d4aeb4ce968c5c6df277126c0", null ],
    [ "~G4HETCChargedFragment", "df/d63/classG4HETCChargedFragment.html#a27e2f897693c8b4f50eb3c6c4f60489f", null ],
    [ "G4HETCChargedFragment", "df/d63/classG4HETCChargedFragment.html#a911a9250bd6114504f87f880d5cd6269", null ],
    [ "G4HETCChargedFragment", "df/d63/classG4HETCChargedFragment.html#a310a708405134955ab135441a5201e46", null ],
    [ "operator!=", "df/d63/classG4HETCChargedFragment.html#ad8f0f97a0bdb510814a53444d3729017", null ],
    [ "operator=", "df/d63/classG4HETCChargedFragment.html#a2235e72c0bfe7087650c0f1c81ad6af5", null ],
    [ "operator==", "df/d63/classG4HETCChargedFragment.html#a85dc7762b0c8ec439f34b776c0200b67", null ],
    [ "SampleKineticEnergy", "df/d63/classG4HETCChargedFragment.html#a633c56eea9c986ae85b63bcdf1f413ba", null ]
];