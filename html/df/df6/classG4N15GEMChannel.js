var classG4N15GEMChannel =
[
    [ "G4N15GEMChannel", "df/df6/classG4N15GEMChannel.html#afe801becfea35d324e3e64b6d5eb504c", null ],
    [ "~G4N15GEMChannel", "df/df6/classG4N15GEMChannel.html#a44948899dd80aaaad2e8469cf19709b4", null ],
    [ "G4N15GEMChannel", "df/df6/classG4N15GEMChannel.html#a083406253255b888d809042f9a7eb632", null ],
    [ "operator!=", "df/df6/classG4N15GEMChannel.html#ac515aaadbd326971d4645bc7ec20ee66", null ],
    [ "operator=", "df/df6/classG4N15GEMChannel.html#a43db0e4defbde82729c91223c76884ab", null ],
    [ "operator==", "df/df6/classG4N15GEMChannel.html#aed29796c18715ad93f8412438e357599", null ],
    [ "theEvaporationProbability", "df/df6/classG4N15GEMChannel.html#a3c8cc93297b84e8c6f55f65797072f12", null ]
];