var classG4VFieldModel =
[
    [ "Representation", "df/d78/classG4VFieldModel.html#a5e23c542ddbefc8221f0331b492478a2", [
      [ "fullArrow", "df/d78/classG4VFieldModel.html#a5e23c542ddbefc8221f0331b492478a2aa4c827dcbd0cbf5cb297ad476c2478c7", null ],
      [ "lightArrow", "df/d78/classG4VFieldModel.html#a5e23c542ddbefc8221f0331b492478a2a3313459f5e6c987ce2a65e73503fa1d1", null ]
    ] ],
    [ "G4VFieldModel", "df/d78/classG4VFieldModel.html#ae82f7a5318e91d34334be98f0d1252b0", null ],
    [ "~G4VFieldModel", "df/d78/classG4VFieldModel.html#a1fba28fa8105557fb20ac88b7b080c7b", null ],
    [ "G4VFieldModel", "df/d78/classG4VFieldModel.html#a06172f0278cf4ce23564e7c88d24ca37", null ],
    [ "DescribeYourselfTo", "df/d78/classG4VFieldModel.html#ac1de9cb75da970cb698ce9beb85b3199", null ],
    [ "GetFieldAtLocation", "df/d78/classG4VFieldModel.html#a3d9c057b6ecb2e46b8190f45475d505e", null ],
    [ "operator=", "df/d78/classG4VFieldModel.html#ad78fd18a4c4186198bc83f6a105a0648", null ],
    [ "fArrow3DLineSegmentsPerCircle", "df/d78/classG4VFieldModel.html#ac83f4fd254db60835438a32c99df7587", null ],
    [ "fArrowPrefix", "df/d78/classG4VFieldModel.html#a0829bf1b1fcde59f429472aa4ae905cb", null ],
    [ "fExtentForField", "df/d78/classG4VFieldModel.html#a7b707405f7ff852d32f6daeff445af14", null ],
    [ "fNDataPointsPerMaxHalfExtent", "df/d78/classG4VFieldModel.html#a48263b7a3ff0a8ee3b1aa6a0ab69698f", null ],
    [ "fPVFindings", "df/d78/classG4VFieldModel.html#a98f3549006e361a455722ba01d079e60", null ],
    [ "fRepresentation", "df/d78/classG4VFieldModel.html#a31ffd60516d58a15881e74bfdad00c21", null ],
    [ "fTypeOfField", "df/d78/classG4VFieldModel.html#a553820b8c9815e5d8ef97da8fbd7828d", null ]
];