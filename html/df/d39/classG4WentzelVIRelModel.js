var classG4WentzelVIRelModel =
[
    [ "G4WentzelVIRelModel", "df/d39/classG4WentzelVIRelModel.html#adb522153fc0f75e3321a322762973f66", null ],
    [ "~G4WentzelVIRelModel", "df/d39/classG4WentzelVIRelModel.html#a1a2b1e567433daadebe43223dce98fd0", null ],
    [ "G4WentzelVIRelModel", "df/d39/classG4WentzelVIRelModel.html#afeb3e13e7f4a4a743273f47f2b0b355f", null ],
    [ "ComputeCrossSectionPerAtom", "df/d39/classG4WentzelVIRelModel.html#a37473d4a85748970480f8fd0c4e8da6c", null ],
    [ "ComputeEffectiveMass", "df/d39/classG4WentzelVIRelModel.html#a08563a3c5537f16622c2c300b2eed6d6", null ],
    [ "DefineMaterial", "df/d39/classG4WentzelVIRelModel.html#a2416d56d933202b8b2be8e80238bf55c", null ],
    [ "Initialise", "df/d39/classG4WentzelVIRelModel.html#a99f0b7c6cf20e6b6e55e9fb81eb6647a", null ],
    [ "operator=", "df/d39/classG4WentzelVIRelModel.html#af06d999116a1505706c2758cdc83b5ef", null ],
    [ "effMass", "df/d39/classG4WentzelVIRelModel.html#a35d003f246bcdd71b5252c3916e9b730", null ],
    [ "fNistManager", "df/d39/classG4WentzelVIRelModel.html#a34d6eda2f9e3b86266e4d410d2049076", null ]
];