var classG4INCLXXNeutronBuilder =
[
    [ "G4INCLXXNeutronBuilder", "df/dca/classG4INCLXXNeutronBuilder.html#a0ec82c259fdd61e1cbee993581c483e5", null ],
    [ "~G4INCLXXNeutronBuilder", "df/dca/classG4INCLXXNeutronBuilder.html#a834773c17f4ca9621f78fbc9515dd318", null ],
    [ "Build", "df/dca/classG4INCLXXNeutronBuilder.html#ad0a93eddd8d5ee85a4001d05e05df635", null ],
    [ "Build", "df/dca/classG4INCLXXNeutronBuilder.html#a238843bcd519c608824d522800fbf871", null ],
    [ "Build", "df/dca/classG4INCLXXNeutronBuilder.html#ac93e652bb6307745e3c2d7853f851b2c", null ],
    [ "Build", "df/dca/classG4INCLXXNeutronBuilder.html#af0bbb5ef1c2b46cc293d70e6453f07aa", null ],
    [ "Build", "df/dca/classG4INCLXXNeutronBuilder.html#aec36d6f00e82f8c6c55392c03f7a0bd2", null ],
    [ "Build", "df/dca/classG4INCLXXNeutronBuilder.html#a64b6f48a5a6f37182c3e4bb905aa41ec", null ],
    [ "Build", "df/dca/classG4INCLXXNeutronBuilder.html#aba1c1d2c6f8e0e6a860f951947abdec2", null ],
    [ "Build", "df/dca/classG4INCLXXNeutronBuilder.html#a0e838ec760d7c34c450fbe14723df255", null ],
    [ "SetMaxEnergy", "df/dca/classG4INCLXXNeutronBuilder.html#a9803bbce4579e4de0aedc6ba098519b5", null ],
    [ "SetMaxPreCompoundEnergy", "df/dca/classG4INCLXXNeutronBuilder.html#a381502f398fd6e47573cc77fae8b944d", null ],
    [ "SetMinEnergy", "df/dca/classG4INCLXXNeutronBuilder.html#a671a6573d6f3e024cd95154411c7b359", null ],
    [ "SetMinPreCompoundEnergy", "df/dca/classG4INCLXXNeutronBuilder.html#a3fd948fe632fb679a90f486e914f3abf", null ],
    [ "UsePreCompound", "df/dca/classG4INCLXXNeutronBuilder.html#a46a6ef717e9bf3f1a29954c567aee065", null ],
    [ "theMax", "df/dca/classG4INCLXXNeutronBuilder.html#a30f885b7b37a6ae2e322b629b97187cf", null ],
    [ "theMin", "df/dca/classG4INCLXXNeutronBuilder.html#a69a12404bafc883d192af69c171b2abc", null ],
    [ "theModel", "df/dca/classG4INCLXXNeutronBuilder.html#af52ca3d89b9036adaea15a800763d5ec", null ],
    [ "thePreCompoundMax", "df/dca/classG4INCLXXNeutronBuilder.html#a3fdff86f4fa215d4f633ce13fc74935b", null ],
    [ "thePreCompoundMin", "df/dca/classG4INCLXXNeutronBuilder.html#ad405af63a1457c2d6a9ecbb81e7fd662", null ],
    [ "thePreCompoundModel", "df/dca/classG4INCLXXNeutronBuilder.html#ae3d6bf58a78a3fc6ceb2fb5bfea3313c", null ],
    [ "withPreCompound", "df/dca/classG4INCLXXNeutronBuilder.html#a69cd84e5f784ad0ac9b2e6de94b74767", null ]
];