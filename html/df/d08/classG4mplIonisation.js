var classG4mplIonisation =
[
    [ "G4mplIonisation", "df/d08/classG4mplIonisation.html#aeebbeab5220c07b26f53e40ede169c45", null ],
    [ "~G4mplIonisation", "df/d08/classG4mplIonisation.html#adcf3b102f301b4c2829850fd727be57b", null ],
    [ "G4mplIonisation", "df/d08/classG4mplIonisation.html#ae7f53484b55bcf9cd3d743d668e89fc5", null ],
    [ "InitialiseEnergyLossProcess", "df/d08/classG4mplIonisation.html#ad2f73bcc09ef5d3c15fc0dd02ea89a07", null ],
    [ "IsApplicable", "df/d08/classG4mplIonisation.html#a87f6b8c9fae84524cc630583e1f3cab2", null ],
    [ "MinPrimaryEnergy", "df/d08/classG4mplIonisation.html#a0c77fc9e7780b65a058abd91b424db83", null ],
    [ "operator=", "df/d08/classG4mplIonisation.html#a1d591d50542f1db74c421b5a1db32155", null ],
    [ "ProcessDescription", "df/d08/classG4mplIonisation.html#a3bac806e28807eda1df3dcb5201e5d84", null ],
    [ "isInitialised", "df/d08/classG4mplIonisation.html#af1141f8b07f88ef6d1ed79dfabca0782", null ],
    [ "magneticCharge", "df/d08/classG4mplIonisation.html#ad08671a46f743df1794b10b2e9eff2f5", null ]
];