var classG4SPPartonInfo =
[
    [ "G4SPPartonInfo", "df/d08/classG4SPPartonInfo.html#acd8907d762c69799626d46cc7af0a372", null ],
    [ "GetDiQuark", "df/d08/classG4SPPartonInfo.html#a114801223bc520b47c54308f79f4f082", null ],
    [ "GetProbability", "df/d08/classG4SPPartonInfo.html#aa22d3e49cb76cf2136eae0b4007dc32a", null ],
    [ "GetQuark", "df/d08/classG4SPPartonInfo.html#a650e55b8ce8858c00b42580d1036f228", null ],
    [ "operator==", "df/d08/classG4SPPartonInfo.html#a2f48bbef83b7d5dbcb234363967ef063", null ],
    [ "diQuarkPDGCode", "df/d08/classG4SPPartonInfo.html#ae70f5082cb8cabf5bd88f9e3f5833245", null ],
    [ "probability", "df/d08/classG4SPPartonInfo.html#a93f5db3295b4d4030b581b9e082dddcf", null ],
    [ "quarkPDGCode", "df/d08/classG4SPPartonInfo.html#a08f4e91553156312a0f88e94fffb41f7", null ]
];