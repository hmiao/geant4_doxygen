var classG4MolecularDissociationTable =
[
    [ "G4MolecularDissociationTable", "df/d76/classG4MolecularDissociationTable.html#a9707f4252245dbaf73b61c025f779af5", null ],
    [ "~G4MolecularDissociationTable", "df/d76/classG4MolecularDissociationTable.html#a64da9f88195bdb2f95afde79be82832a", null ],
    [ "G4MolecularDissociationTable", "df/d76/classG4MolecularDissociationTable.html#af6436f4d6ffae32120484cbda6922c78", null ],
    [ "AddChannel", "df/d76/classG4MolecularDissociationTable.html#ad6bf8eb231b7a1ffeda620d690f5718f", null ],
    [ "CheckDataConsistency", "df/d76/classG4MolecularDissociationTable.html#ad4a2f0acdb86e111809a80126d65050c", null ],
    [ "GetDecayChannels", "df/d76/classG4MolecularDissociationTable.html#a737fb4501f13f4c09fe4a6e816fa1f34", null ],
    [ "GetDecayChannels", "df/d76/classG4MolecularDissociationTable.html#acaf59d9bab5a0e23d247b52347e8f214", null ],
    [ "operator=", "df/d76/classG4MolecularDissociationTable.html#ac1ce5018aecdbe2e2c2927990627c4f3", null ],
    [ "Serialize", "df/d76/classG4MolecularDissociationTable.html#a264d8d487f7e1f3fe77b215c8b6f1ee7", null ],
    [ "fDissociationChannels", "df/d76/classG4MolecularDissociationTable.html#a00e60f8aed37747c047a09657e635fb0", null ]
];