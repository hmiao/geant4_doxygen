var classG4DopplerProfile =
[
    [ "G4DopplerProfile", "df/dfb/classG4DopplerProfile.html#aa24e18d7eb9d0cb94fcf6cc38e1c46db", null ],
    [ "~G4DopplerProfile", "df/dfb/classG4DopplerProfile.html#a34d308095276029f4637f50f94212aab", null ],
    [ "G4DopplerProfile", "df/dfb/classG4DopplerProfile.html#a4722cefc7a133ecd5e414a3ede0d5ca7", null ],
    [ "LoadBiggsP", "df/dfb/classG4DopplerProfile.html#a9a8c9ee154c6653be4f91e55615963a7", null ],
    [ "LoadProfile", "df/dfb/classG4DopplerProfile.html#aa9c4d53e69531fbe741688c543a61fc5", null ],
    [ "NumberOfProfiles", "df/dfb/classG4DopplerProfile.html#a99346d5da2bf49bd224fdae31c74e088", null ],
    [ "operator=", "df/dfb/classG4DopplerProfile.html#a92c70545e2bdd34cfd2031a3ae07a934", null ],
    [ "PrintData", "df/dfb/classG4DopplerProfile.html#a4a62eed38491da91f383b0d1fa59bac6", null ],
    [ "Profile", "df/dfb/classG4DopplerProfile.html#ab103712678e507210675edd4dc83f83e", null ],
    [ "Profiles", "df/dfb/classG4DopplerProfile.html#a475c0395cceb73c2b40d20b021212c42", null ],
    [ "RandomSelectMomentum", "df/dfb/classG4DopplerProfile.html#ac3bb304e9a91cdbb879c347d32e5e636", null ],
    [ "biggsP", "df/dfb/classG4DopplerProfile.html#ac59d0526d146e8916647310f940b89dd", null ],
    [ "nBiggs", "df/dfb/classG4DopplerProfile.html#a0619163086324906da0ad56bffe3b726", null ],
    [ "nShells", "df/dfb/classG4DopplerProfile.html#af02bbb2d2dcf1c11e7d0750bf1f665a9", null ],
    [ "profileMap", "df/dfb/classG4DopplerProfile.html#add74147d59abcac3fdd75a912d417082", null ],
    [ "zMax", "df/dfb/classG4DopplerProfile.html#accc108f021c859785ca208ce2bf5219d", null ],
    [ "zMin", "df/dfb/classG4DopplerProfile.html#a5f459d993b3a78eb988a27759543093c", null ]
];