var G4DNAMolecule_8hh =
[
    [ "G4DamagedDeoxyribose", "d0/da3/classG4DamagedDeoxyribose.html", "d0/da3/classG4DamagedDeoxyribose" ],
    [ "G4DamagedAdenine", "d0/dfa/classG4DamagedAdenine.html", "d0/dfa/classG4DamagedAdenine" ],
    [ "G4DamagedGuanine", "d5/dd4/classG4DamagedGuanine.html", "d5/dd4/classG4DamagedGuanine" ],
    [ "G4DamagedThymine", "da/d5f/classG4DamagedThymine.html", "da/d5f/classG4DamagedThymine" ],
    [ "G4DamagedCytosine", "dc/dbb/classG4DamagedCytosine.html", "dc/dbb/classG4DamagedCytosine" ],
    [ "G4Deoxyribose", "d2/dc7/classG4Deoxyribose.html", "d2/dc7/classG4Deoxyribose" ],
    [ "G4Phosphate", "d6/d4a/classG4Phosphate.html", "d6/d4a/classG4Phosphate" ],
    [ "G4Adenine", "d9/db5/classG4Adenine.html", "d9/db5/classG4Adenine" ],
    [ "G4Guanine", "d3/d37/classG4Guanine.html", "d3/d37/classG4Guanine" ],
    [ "G4Thymine", "de/df0/classG4Thymine.html", "de/df0/classG4Thymine" ],
    [ "G4Cytosine", "dd/d56/classG4Cytosine.html", "dd/d56/classG4Cytosine" ],
    [ "G4ModifiedHistone", "dc/df7/classG4ModifiedHistone.html", "dc/df7/classG4ModifiedHistone" ],
    [ "G4Histone", "d5/db4/classG4Histone.html", "d5/db4/classG4Histone" ]
];