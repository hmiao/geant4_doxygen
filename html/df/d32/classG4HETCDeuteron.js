var classG4HETCDeuteron =
[
    [ "G4HETCDeuteron", "df/d32/classG4HETCDeuteron.html#a67143189244c4e1f800a759be6b5da09", null ],
    [ "~G4HETCDeuteron", "df/d32/classG4HETCDeuteron.html#ab876395885085555326a55416b9e0a48", null ],
    [ "G4HETCDeuteron", "df/d32/classG4HETCDeuteron.html#a54fb5cd602cc503082627371b3b840bc", null ],
    [ "GetAlpha", "df/d32/classG4HETCDeuteron.html#ae258fd3764673592a4c0d70a68aa7865", null ],
    [ "GetBeta", "df/d32/classG4HETCDeuteron.html#a7f2762582c0756f9b916d33a52070086", null ],
    [ "GetSpinFactor", "df/d32/classG4HETCDeuteron.html#ae4b26d49e2a2a7a76ff2fc7073ac623a", null ],
    [ "K", "df/d32/classG4HETCDeuteron.html#a045c594bdc3f3d8afc00be93f29c12ce", null ],
    [ "operator!=", "df/d32/classG4HETCDeuteron.html#ade839125cf5da2ab704c0fb172b50f9d", null ],
    [ "operator=", "df/d32/classG4HETCDeuteron.html#ab4c356617511111627463fcd0b71cff8", null ],
    [ "operator==", "df/d32/classG4HETCDeuteron.html#a8915eb8896546ff4ce45807db212899e", null ],
    [ "theDeuteronCoulombBarrier", "df/d32/classG4HETCDeuteron.html#ad2e8e5f7f8456a1e103d373445555fea", null ]
];