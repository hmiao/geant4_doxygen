var classG4Hdf5AnalysisReader =
[
    [ "~G4Hdf5AnalysisReader", "df/d9c/classG4Hdf5AnalysisReader.html#a55bf964702acfad90b03e5da89cc41f2", null ],
    [ "G4Hdf5AnalysisReader", "df/d9c/classG4Hdf5AnalysisReader.html#a96088e972b5ca632cfa9cba5dd8ffc8d", null ],
    [ "CloseFilesImpl", "df/d9c/classG4Hdf5AnalysisReader.html#a0b2b509af43eb9f480f05271d69022b7", null ],
    [ "GetNtuple", "df/d9c/classG4Hdf5AnalysisReader.html#a8d55611028bdcbfb3b742a94b1204942", null ],
    [ "GetNtuple", "df/d9c/classG4Hdf5AnalysisReader.html#abbb85d623bd3215dfc51c42b90525840", null ],
    [ "GetNtuple", "df/d9c/classG4Hdf5AnalysisReader.html#aa8a9df51caa0df7464e9c368cc680674", null ],
    [ "Instance", "df/d9c/classG4Hdf5AnalysisReader.html#a10b215abf3d62c25172f4cbea2c37c93", null ],
    [ "Reset", "df/d9c/classG4Hdf5AnalysisReader.html#af0c8687be411836b0d717eba7f2b05ce", null ],
    [ "G4ThreadLocalSingleton< G4Hdf5AnalysisReader >", "df/d9c/classG4Hdf5AnalysisReader.html#afc58e8417135a04de95ac02e1d863cc4", null ],
    [ "fFileManager", "df/d9c/classG4Hdf5AnalysisReader.html#a4047253b1e430f5f0d73023f8162e2c0", null ],
    [ "fgMasterInstance", "df/d9c/classG4Hdf5AnalysisReader.html#a8793786d56141a33e9721181b9732c0d", null ],
    [ "fkClass", "df/d9c/classG4Hdf5AnalysisReader.html#a20d143f7cced92db8a7683f8543005bc", null ],
    [ "fNtupleManager", "df/d9c/classG4Hdf5AnalysisReader.html#aae0dffa360a317cd0a3c591ed2cdc42c", null ]
];