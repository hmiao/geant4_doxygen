var classG4eIonisationParameters =
[
    [ "G4eIonisationParameters", "df/d4b/classG4eIonisationParameters.html#a11abec8e635fec837810d5a5c631f786", null ],
    [ "~G4eIonisationParameters", "df/d4b/classG4eIonisationParameters.html#a7507a1084d2988d97412b018b755147b", null ],
    [ "G4eIonisationParameters", "df/d4b/classG4eIonisationParameters.html#ad8b4b220640b4c78123e8f562666a13e", null ],
    [ "Excitation", "df/d4b/classG4eIonisationParameters.html#a681fa3dba5e58c0e7d156ce4668bc73c", null ],
    [ "LoadData", "df/d4b/classG4eIonisationParameters.html#ae4c0541225703972ea4cb6ff632a27d1", null ],
    [ "operator=", "df/d4b/classG4eIonisationParameters.html#a8dbaed943788afcc66d3f59604137535", null ],
    [ "Parameter", "df/d4b/classG4eIonisationParameters.html#a8ab045c90debf63a7a1ed90ce5ed88e7", null ],
    [ "PrintData", "df/d4b/classG4eIonisationParameters.html#aec5f95389143196fa5172351de7f5c33", null ],
    [ "activeZ", "df/d4b/classG4eIonisationParameters.html#a830fe2adf31c4039385a7b384b6a51f9", null ],
    [ "excit", "df/d4b/classG4eIonisationParameters.html#ae1810cc056592946a22e35a64650586e", null ],
    [ "length", "df/d4b/classG4eIonisationParameters.html#a638e2c2d0df9c3d0b78ddb7bb01f34f8", null ],
    [ "param", "df/d4b/classG4eIonisationParameters.html#a296116338cdb8bf3308053c478a0fb74", null ]
];