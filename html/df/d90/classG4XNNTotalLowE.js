var classG4XNNTotalLowE =
[
    [ "LowEMap", "df/d90/classG4XNNTotalLowE.html#ab708e76137fbb190b9fc7dcf7ec92d16", null ],
    [ "G4XNNTotalLowE", "df/d90/classG4XNNTotalLowE.html#a130b411be09c475c1a9157ecab9d8b1a", null ],
    [ "~G4XNNTotalLowE", "df/d90/classG4XNNTotalLowE.html#a9469f64eb5a69338f921aa43a286bfdf", null ],
    [ "G4XNNTotalLowE", "df/d90/classG4XNNTotalLowE.html#a599c0918cc1da7a9d059f7b2bb6465f6", null ],
    [ "CrossSection", "df/d90/classG4XNNTotalLowE.html#a980d1b775be7c8b12c1b02069da2d983", null ],
    [ "GetComponents", "df/d90/classG4XNNTotalLowE.html#aba849baa83e463f9b73410ed460f3d87", null ],
    [ "HighLimit", "df/d90/classG4XNNTotalLowE.html#ab4d7d15027e9d3719910cfbac09e11a4", null ],
    [ "IsValid", "df/d90/classG4XNNTotalLowE.html#ac17d10e34899641ee9264a28f31e59a8", null ],
    [ "Name", "df/d90/classG4XNNTotalLowE.html#a770da126e6744bd9f8829989f17bb886", null ],
    [ "operator=", "df/d90/classG4XNNTotalLowE.html#a1dab12cb2c52c9f688506e0f47d16d1a", null ],
    [ "npTot", "df/d90/classG4XNNTotalLowE.html#afd32aeab91a7e8d0aae5b4e4214a77e4", null ],
    [ "ppTot", "df/d90/classG4XNNTotalLowE.html#a51df7874ee8173b6119e284e1a3bdf9d", null ],
    [ "ss", "df/d90/classG4XNNTotalLowE.html#adc1e506bc64206527865ac94fea1b41a", null ],
    [ "tableSize", "df/d90/classG4XNNTotalLowE.html#a8f0680dd900ffa0d658a6a182d51e586", null ],
    [ "theCrossSections", "df/d90/classG4XNNTotalLowE.html#a8979eb9f3b9a7e1d583d0291d15f1ab7", null ]
];