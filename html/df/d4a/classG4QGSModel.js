var classG4QGSModel =
[
    [ "G4QGSModel", "df/d4a/classG4QGSModel.html#a557f1eb6541cf326732c92d544418962", null ],
    [ "~G4QGSModel", "df/d4a/classG4QGSModel.html#a8ef8950a9ec2ddf743c112f7b9ad6b9b", null ],
    [ "G4QGSModel", "df/d4a/classG4QGSModel.html#a7e25b51c7e502bb5dcf497f21ee09a22", null ],
    [ "GetProjectileNucleus", "df/d4a/classG4QGSModel.html#ae85cb359c48e4506f2b6bec19f16c845", null ],
    [ "GetStrings", "df/d4a/classG4QGSModel.html#a57e19649c002200c1651380d27ce4bfc", null ],
    [ "GetWoundedNucleus", "df/d4a/classG4QGSModel.html#a0add486349d4866a8812c997b5d114c8", null ],
    [ "Init", "df/d4a/classG4QGSModel.html#abbc2bc04f095464fc74b877fc3628d9c", null ],
    [ "ModelDescription", "df/d4a/classG4QGSModel.html#acfa7a034c8c6cb8acfb3e7a41af900cf", null ],
    [ "operator!=", "df/d4a/classG4QGSModel.html#a0ebcb50987b78fc901494b2b96b7dc61", null ],
    [ "operator=", "df/d4a/classG4QGSModel.html#af251afb4c531d730c4542277605c5ce4", null ],
    [ "operator==", "df/d4a/classG4QGSModel.html#a2100ad9cf73e77a19d47fd1407d8e271", null ],
    [ "theDiffractiveStringBuilder", "df/d4a/classG4QGSModel.html#a8beef740c6e0663844aac163a8e6e9b2", null ],
    [ "theParticipants", "df/d4a/classG4QGSModel.html#a2552de189443626e2c8e7fc2ae2a86ee", null ],
    [ "theSoftStringBuilder", "df/d4a/classG4QGSModel.html#a1bdb02c1ebe23b35bb43863275d97a14", null ]
];