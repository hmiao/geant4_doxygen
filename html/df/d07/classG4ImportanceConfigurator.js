var classG4ImportanceConfigurator =
[
    [ "G4ImportanceConfigurator", "df/d07/classG4ImportanceConfigurator.html#a0b93dc36b727a46f8f64d18c8529c5e3", null ],
    [ "G4ImportanceConfigurator", "df/d07/classG4ImportanceConfigurator.html#a0be97b604c8aec839dee3fc9f12309ce", null ],
    [ "~G4ImportanceConfigurator", "df/d07/classG4ImportanceConfigurator.html#aee73f64629cf6840eff6c09b1a3dbdef", null ],
    [ "G4ImportanceConfigurator", "df/d07/classG4ImportanceConfigurator.html#a556007fc4c210819ae28a51d7b07954f", null ],
    [ "Configure", "df/d07/classG4ImportanceConfigurator.html#aa0b7a1ca0aad00b0c9191c0974f20ea2", null ],
    [ "GetTrackTerminator", "df/d07/classG4ImportanceConfigurator.html#ac63e9b0ced95b860c44686c84c99fc1f", null ],
    [ "operator=", "df/d07/classG4ImportanceConfigurator.html#a3cd23b1b476b21c862cea5efbafc1e89", null ],
    [ "SetWorldName", "df/d07/classG4ImportanceConfigurator.html#a3ea0d0820f32c7123caa22df9038398b", null ],
    [ "fDeleteIalg", "df/d07/classG4ImportanceConfigurator.html#a57112cb7349dc9d018bacceae646f13d", null ],
    [ "fIalgorithm", "df/d07/classG4ImportanceConfigurator.html#a592fa3b1f98daa0102d799d738279fb8", null ],
    [ "fImportanceProcess", "df/d07/classG4ImportanceConfigurator.html#a828135c5bbb7288ac22342195d2e1f00", null ],
    [ "fIStore", "df/d07/classG4ImportanceConfigurator.html#a1809d737159bacbb6eae7475dfa895f7", null ],
    [ "fPlacer", "df/d07/classG4ImportanceConfigurator.html#a89f123fc989374a88d48a7ba9ec7f093", null ],
    [ "fWorld", "df/d07/classG4ImportanceConfigurator.html#a1deb8f719d9245dc5d49b63ac8485131", null ],
    [ "fWorldName", "df/d07/classG4ImportanceConfigurator.html#aa6ec5cbc64c8cbb99c04dfd3e97561d6", null ],
    [ "paraflag", "df/d07/classG4ImportanceConfigurator.html#acb44746be9c6735978e0fe169822c764", null ]
];