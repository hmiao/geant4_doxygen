var dir_6ae10bad0ce88f46bdc4b920f4a33a13 =
[
    [ "G4DiffractiveExcitation.hh", "d4/d49/G4DiffractiveExcitation_8hh.html", "d4/d49/G4DiffractiveExcitation_8hh" ],
    [ "G4DiffractiveSplitableHadron.hh", "d8/dcd/G4DiffractiveSplitableHadron_8hh.html", "d8/dcd/G4DiffractiveSplitableHadron_8hh" ],
    [ "G4ElasticHNScattering.hh", "dd/d81/G4ElasticHNScattering_8hh.html", "dd/d81/G4ElasticHNScattering_8hh" ],
    [ "G4FTFAnnihilation.hh", "d6/d00/G4FTFAnnihilation_8hh.html", "d6/d00/G4FTFAnnihilation_8hh" ],
    [ "G4FTFModel.hh", "d5/d02/G4FTFModel_8hh.html", "d5/d02/G4FTFModel_8hh" ],
    [ "G4FTFParameters.hh", "d4/d8f/G4FTFParameters_8hh.html", "d4/d8f/G4FTFParameters_8hh" ],
    [ "G4FTFParticipants.hh", "d9/d99/G4FTFParticipants_8hh.html", "d9/d99/G4FTFParticipants_8hh" ]
];