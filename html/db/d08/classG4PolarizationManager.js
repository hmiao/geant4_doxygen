var classG4PolarizationManager =
[
    [ "~G4PolarizationManager", "db/d08/classG4PolarizationManager.html#a6be11a6bc22a7fbc66a69e059e87768a", null ],
    [ "G4PolarizationManager", "db/d08/classG4PolarizationManager.html#addd1df29d021ef68795e7d256060ca1d", null ],
    [ "G4PolarizationManager", "db/d08/classG4PolarizationManager.html#a8d437cdd4ea7c3f7c0ad7f37aa31118d", null ],
    [ "Clean", "db/d08/classG4PolarizationManager.html#a51d7e7d1b628b11973101aa6de78ef34", null ],
    [ "Dispose", "db/d08/classG4PolarizationManager.html#a0f90479f8c8fb56ef067e908b3f63cb9", null ],
    [ "GetInstance", "db/d08/classG4PolarizationManager.html#a6f270796e6d31801a3bee612890e1347", null ],
    [ "GetVerbose", "db/d08/classG4PolarizationManager.html#aba43de8d425975d29d069d953aa2e87c", null ],
    [ "GetVolumePolarization", "db/d08/classG4PolarizationManager.html#a35bc83e5eb7d94f5ace90dede720f405", null ],
    [ "IsActivated", "db/d08/classG4PolarizationManager.html#a39fa50dc7797e5c5ed63ee939905a015", null ],
    [ "IsPolarized", "db/d08/classG4PolarizationManager.html#a08182508b7655aaff2edff1a8011012f", null ],
    [ "ListVolumes", "db/d08/classG4PolarizationManager.html#a6800699d78a9cdb9f97cfb5c758cc2a9", null ],
    [ "operator=", "db/d08/classG4PolarizationManager.html#a12e3f0e8852e4614c6dffffa4abe557d", null ],
    [ "SetActivated", "db/d08/classG4PolarizationManager.html#ae5de076cdfbe8999426fe3e1b606ee5c", null ],
    [ "SetVerbose", "db/d08/classG4PolarizationManager.html#adb60fd745a1a248679389f2a5472f005", null ],
    [ "SetVolumePolarization", "db/d08/classG4PolarizationManager.html#a32a073ac1cd800e93cbb100b7ce46f07", null ],
    [ "SetVolumePolarization", "db/d08/classG4PolarizationManager.html#a2862bafe4fb6cc49abcc1edbf64c85b0", null ],
    [ "fActivated", "db/d08/classG4PolarizationManager.html#a2eadf82fd38a0824d0de2dcfb9e344c2", null ],
    [ "fInstance", "db/d08/classG4PolarizationManager.html#af48495659caf9a30df86318415441481", null ],
    [ "fMessenger", "db/d08/classG4PolarizationManager.html#a2c2906cc324236595b8672b8a458b60a", null ],
    [ "fVerboseLevel", "db/d08/classG4PolarizationManager.html#a7ab93c6cd360547f8d085d4380b92fc1", null ],
    [ "fVolumePolarizations", "db/d08/classG4PolarizationManager.html#aaaef84bd308182539abaf7f8851ddb2b", null ]
];