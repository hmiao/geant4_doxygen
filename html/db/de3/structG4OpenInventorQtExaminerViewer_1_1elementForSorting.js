var structG4OpenInventorQtExaminerViewer_1_1elementForSorting =
[
    [ "operator<", "db/de3/structG4OpenInventorQtExaminerViewer_1_1elementForSorting.html#a897a9c2f1622eb38f4b8f1391686103d", null ],
    [ "closestPoint", "db/de3/structG4OpenInventorQtExaminerViewer_1_1elementForSorting.html#a07db5cea76c7d6b93fa72b4cad535363", null ],
    [ "closestPointZCoord", "db/de3/structG4OpenInventorQtExaminerViewer_1_1elementForSorting.html#a1e63c88686932e0043b0e8b109fbb1b8", null ],
    [ "distanceToBeamlineStart", "db/de3/structG4OpenInventorQtExaminerViewer_1_1elementForSorting.html#ae6775fa55df8c2d2834a04a79debf335", null ],
    [ "name", "db/de3/structG4OpenInventorQtExaminerViewer_1_1elementForSorting.html#af7779e9644675b88fb6a6bf96d3f3e0c", null ],
    [ "smallestDistance", "db/de3/structG4OpenInventorQtExaminerViewer_1_1elementForSorting.html#a115abf73c41b15a89fbcc4b808d205ea", null ]
];