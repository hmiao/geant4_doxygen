var classG4ExtendedMaterial =
[
    [ "G4ExtendedMaterial", "db/d87/classG4ExtendedMaterial.html#a0f5da42ef7c8e3d0757a91088591d161", null ],
    [ "G4ExtendedMaterial", "db/d87/classG4ExtendedMaterial.html#a6d62d61cca360899cd8bc218b7f9c0bc", null ],
    [ "G4ExtendedMaterial", "db/d87/classG4ExtendedMaterial.html#ab7f01939ff5ad7c2f7b39b99d34ab99b", null ],
    [ "G4ExtendedMaterial", "db/d87/classG4ExtendedMaterial.html#a82a82537ed98c3a0fd9329e44efa4fd3", null ],
    [ "~G4ExtendedMaterial", "db/d87/classG4ExtendedMaterial.html#a6afaf346e86bad0aa6e734d9874bc978", null ],
    [ "begin", "db/d87/classG4ExtendedMaterial.html#aa84581200b11d4a2b37d5031a2810368", null ],
    [ "cbegin", "db/d87/classG4ExtendedMaterial.html#adec83b688d3a5197991150a97fe33b98", null ],
    [ "cend", "db/d87/classG4ExtendedMaterial.html#aef1e18b4cbb481cd365e8810bffd617d", null ],
    [ "end", "db/d87/classG4ExtendedMaterial.html#a4c5810982d8b4df712e4aab6b1517304", null ],
    [ "GetNumberOfExtensions", "db/d87/classG4ExtendedMaterial.html#a1d5fd6b40e8e1ec5e373e9796ac69767", null ],
    [ "IsExtended", "db/d87/classG4ExtendedMaterial.html#aa2919ecf86ccbe595b65b3cf69570adf", null ],
    [ "Print", "db/d87/classG4ExtendedMaterial.html#a199536a3fb54d1b3334cce27c7e435c1", null ],
    [ "RegisterExtension", "db/d87/classG4ExtendedMaterial.html#a291182f10ab71e6246ec8b559474240a", null ],
    [ "RetrieveExtension", "db/d87/classG4ExtendedMaterial.html#abfa0ac32d3227be57ee0c9e105a52ef4", null ],
    [ "fExtensionMap", "db/d87/classG4ExtendedMaterial.html#aca63781ec2269a2f67d182965cef92ee", null ]
];