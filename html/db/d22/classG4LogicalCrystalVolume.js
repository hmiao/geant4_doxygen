var classG4LogicalCrystalVolume =
[
    [ "G4LogicalCrystalVolume", "db/d22/classG4LogicalCrystalVolume.html#a05add36de114768b3882eda0fb280595", null ],
    [ "~G4LogicalCrystalVolume", "db/d22/classG4LogicalCrystalVolume.html#ae4dbb71528bf6224941996421c97a384", null ],
    [ "GetBasis", "db/d22/classG4LogicalCrystalVolume.html#a36e7106c53bcabd709737e248245af7c", null ],
    [ "GetCrystal", "db/d22/classG4LogicalCrystalVolume.html#ab580c2787734f41310c7f3c826139fd6", null ],
    [ "IsExtended", "db/d22/classG4LogicalCrystalVolume.html#a1f7ef0a80421adfdc0566e48010dd3a0", null ],
    [ "IsLattice", "db/d22/classG4LogicalCrystalVolume.html#a3379b638983d071f616e7327e78a5b19", null ],
    [ "RotateToLattice", "db/d22/classG4LogicalCrystalVolume.html#a7695fea47f702aee2e5435e0501ad428", null ],
    [ "RotateToSolid", "db/d22/classG4LogicalCrystalVolume.html#a2fa1bf0008df72e8ad31fbf92b721648", null ],
    [ "SetMillerOrientation", "db/d22/classG4LogicalCrystalVolume.html#ac243a41dd070060e5734c702c2c7e2a1", null ],
    [ "SetVerbose", "db/d22/classG4LogicalCrystalVolume.html#a83dae19b0bc9ea02de2290351c663835", null ],
    [ "fInverse", "db/d22/classG4LogicalCrystalVolume.html#a8df7a90116a60148f802fa18e5eec113", null ],
    [ "fLCVvec", "db/d22/classG4LogicalCrystalVolume.html#a8fb3b9c36ecdf9187c628899671df1b1", null ],
    [ "fOrient", "db/d22/classG4LogicalCrystalVolume.html#a2a38b79e9351c8ad1cfc700d4160ceb6", null ],
    [ "fRot", "db/d22/classG4LogicalCrystalVolume.html#a03bbe94b777e4f86af3256f85dee7728", null ],
    [ "hMiller", "db/d22/classG4LogicalCrystalVolume.html#ac7c01e254348ef1ec618d54bc0ba1fda", null ],
    [ "kMiller", "db/d22/classG4LogicalCrystalVolume.html#a7da98b1462b98fb40d947db6ccc15869", null ],
    [ "lMiller", "db/d22/classG4LogicalCrystalVolume.html#aee530fd70ddf0c37b2410b65fb8a6bb6", null ],
    [ "verboseLevel", "db/d22/classG4LogicalCrystalVolume.html#af82a635a98e3005921f125a8e9eff123", null ]
];