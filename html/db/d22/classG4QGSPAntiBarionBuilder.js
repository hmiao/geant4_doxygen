var classG4QGSPAntiBarionBuilder =
[
    [ "G4QGSPAntiBarionBuilder", "db/d22/classG4QGSPAntiBarionBuilder.html#a0b873c68d443b827bef379d8b2a63c7e", null ],
    [ "~G4QGSPAntiBarionBuilder", "db/d22/classG4QGSPAntiBarionBuilder.html#ac6ec5477142e3d131c65493f75e50260", null ],
    [ "Build", "db/d22/classG4QGSPAntiBarionBuilder.html#a2797bde3ab7a3bd81dd6d960b7a95ea5", null ],
    [ "Build", "db/d22/classG4QGSPAntiBarionBuilder.html#aa86948a8a2749f8ac0ebfd2b809f0485", null ],
    [ "Build", "db/d22/classG4QGSPAntiBarionBuilder.html#ae15e537964f54d5aa6055956422bc0bc", null ],
    [ "Build", "db/d22/classG4QGSPAntiBarionBuilder.html#a54c821941e175af90a64764e6ed16f27", null ],
    [ "SetMaxEnergy", "db/d22/classG4QGSPAntiBarionBuilder.html#a2497769201a2b371087116af46682d5b", null ],
    [ "SetMinEnergy", "db/d22/classG4QGSPAntiBarionBuilder.html#a47558cf72a4c469e57d9ac385dbb708a", null ],
    [ "theAntiNucleonData", "db/d22/classG4QGSPAntiBarionBuilder.html#a91e20d117dfd22c72975cf8958958673", null ],
    [ "theFTFmodel", "db/d22/classG4QGSPAntiBarionBuilder.html#a3d5331b6bb53e87515af158db464736b", null ],
    [ "theMax", "db/d22/classG4QGSPAntiBarionBuilder.html#a107cb4a93802b48c6482519cb8085e90", null ],
    [ "theMin", "db/d22/classG4QGSPAntiBarionBuilder.html#a851e051e84396292f1d9776752c6e15d", null ],
    [ "theQGSmodel", "db/d22/classG4QGSPAntiBarionBuilder.html#a6f9ae492fad39cee416855eb0627c233", null ]
];