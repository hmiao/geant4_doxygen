var nf__Legendre_8cc =
[
    [ "nf_Legendre_from_ptwXY_callback_s", "d0/dfb/structnf__Legendre__from__ptwXY__callback__s.html", "d0/dfb/structnf__Legendre__from__ptwXY__callback__s" ],
    [ "nf_Legendre_allocated", "db/d20/nf__Legendre_8cc.html#a3fab45b84ab6529f7c2a23f545e4ba05", null ],
    [ "nf_Legendre_clone", "db/d20/nf__Legendre_8cc.html#a1a18f9b620c74002528dd79e8cc548b5", null ],
    [ "nf_Legendre_evauluateAtMu", "db/d20/nf__Legendre_8cc.html#a1253c56deaa9671906462a3ad956de8b", null ],
    [ "nf_Legendre_free", "db/d20/nf__Legendre_8cc.html#a54741ab27d39fec3eec83edb528166ec", null ],
    [ "nf_Legendre_from_ptwXY", "db/d20/nf__Legendre_8cc.html#af804491e7f74b11ca4d0c1e5ecf4bb82", null ],
    [ "nf_Legendre_from_ptwXY_callback", "db/d20/nf__Legendre_8cc.html#a355d288d4402840032141289c571bde3", null ],
    [ "nf_Legendre_getCl", "db/d20/nf__Legendre_8cc.html#a341033a74c60af4954818da3c8605dea", null ],
    [ "nf_Legendre_maxOrder", "db/d20/nf__Legendre_8cc.html#aae0d40cf9a6b514977c501b2405d2192", null ],
    [ "nf_Legendre_new", "db/d20/nf__Legendre_8cc.html#a6f6004300ef4d8aca890f60d1c000510", null ],
    [ "nf_Legendre_normalize", "db/d20/nf__Legendre_8cc.html#ab8b96b2bc2c79d0117a1c66c9ae9c837", null ],
    [ "nf_Legendre_PofL_atMu", "db/d20/nf__Legendre_8cc.html#a1a9bcae4605cba9d2c3708bd6bc44dbf", null ],
    [ "nf_Legendre_reallocateCls", "db/d20/nf__Legendre_8cc.html#a568d0bf5a0a2a62b8e4d1f7287ee7ae9", null ],
    [ "nf_Legendre_release", "db/d20/nf__Legendre_8cc.html#a87a56f5e98ce85e7f2230f913063d250", null ],
    [ "nf_Legendre_setCl", "db/d20/nf__Legendre_8cc.html#ac3539ccd2ca7bc1dfb8caac328afc4dd", null ],
    [ "nf_Legendre_setup", "db/d20/nf__Legendre_8cc.html#aa7e09ad25692496ef212936bb9250a6a", null ],
    [ "nf_Legendre_to_ptwXY", "db/d20/nf__Legendre_8cc.html#a11f16d36f21604092e4c4ea2099959b6", null ],
    [ "nf_Legendre_to_ptwXY2", "db/d20/nf__Legendre_8cc.html#a27ff9603b50a7321e6ad2d55d208f534", null ]
];