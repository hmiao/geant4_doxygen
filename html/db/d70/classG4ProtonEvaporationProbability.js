var classG4ProtonEvaporationProbability =
[
    [ "G4ProtonEvaporationProbability", "db/d70/classG4ProtonEvaporationProbability.html#ae1ee55f01e7f6022d526d078e68d6259", null ],
    [ "~G4ProtonEvaporationProbability", "db/d70/classG4ProtonEvaporationProbability.html#ace0ebae077b28801bb5fccc8e380a88c", null ],
    [ "G4ProtonEvaporationProbability", "db/d70/classG4ProtonEvaporationProbability.html#a6defe0d60c2b3983d0fee84b3ffdc169", null ],
    [ "CalcAlphaParam", "db/d70/classG4ProtonEvaporationProbability.html#a8a931dc41e5ef7060513485570f18c8f", null ],
    [ "CalcBetaParam", "db/d70/classG4ProtonEvaporationProbability.html#ad3980e87c06cdfd75023c1acadd733a7", null ],
    [ "operator!=", "db/d70/classG4ProtonEvaporationProbability.html#aa7c20ec268270e1258f5f404c6201fe6", null ],
    [ "operator=", "db/d70/classG4ProtonEvaporationProbability.html#aed9b6ebf34283483ba8149f9cb5ad94f", null ],
    [ "operator==", "db/d70/classG4ProtonEvaporationProbability.html#a0f4ff2cdda5a7ee38e55c496fd3c4ae5", null ]
];