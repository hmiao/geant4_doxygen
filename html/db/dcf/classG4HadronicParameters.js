var classG4HadronicParameters =
[
    [ "~G4HadronicParameters", "db/dcf/classG4HadronicParameters.html#a5aa35639e0d0ddd6730356122a9228a2", null ],
    [ "G4HadronicParameters", "db/dcf/classG4HadronicParameters.html#a803e994bec7695f618cea7e7bd9e8b09", null ],
    [ "ApplyFactorXS", "db/dcf/classG4HadronicParameters.html#a0bed66fe3524604568305d9baa700462", null ],
    [ "EnableBCParticles", "db/dcf/classG4HadronicParameters.html#a9cc0bcf3c7b1a3d79c955faf3f184a69", null ],
    [ "EnableCRCoalescence", "db/dcf/classG4HadronicParameters.html#a96dc93d3a1fa91daca44024c28fe0cd4", null ],
    [ "EnableHyperNuclei", "db/dcf/classG4HadronicParameters.html#a7b0bb2acdd3daa7c63b869e846ba5a9e", null ],
    [ "EnergyThresholdForHeavyHadrons", "db/dcf/classG4HadronicParameters.html#aa6553dbeef14cf15f1f3518fa4d656c0", null ],
    [ "GetMaxEnergy", "db/dcf/classG4HadronicParameters.html#af523282c5fb61a2889e6515986940557", null ],
    [ "GetMaxEnergyTransitionFTF_Cascade", "db/dcf/classG4HadronicParameters.html#a829b57ea4a40206859c06b2dd39d3f3a", null ],
    [ "GetMaxEnergyTransitionQGS_FTF", "db/dcf/classG4HadronicParameters.html#ac33cc0f4439043f0fe3830e7b48a2a77", null ],
    [ "GetMinEnergyTransitionFTF_Cascade", "db/dcf/classG4HadronicParameters.html#a5df973a8ea13335806189d219fd9174f", null ],
    [ "GetMinEnergyTransitionQGS_FTF", "db/dcf/classG4HadronicParameters.html#a7bf71cf5e4a453e9ffb83d0d0470e339", null ],
    [ "GetVerboseLevel", "db/dcf/classG4HadronicParameters.html#a4e3947955a79de044fe07a091cf54e86", null ],
    [ "Instance", "db/dcf/classG4HadronicParameters.html#a374ad858a19ae100f6e7cb5af14b3a95", null ],
    [ "IsLocked", "db/dcf/classG4HadronicParameters.html#a8da71c7a45f834f70fff966df293ab0c", null ],
    [ "SetApplyFactorXS", "db/dcf/classG4HadronicParameters.html#ab8333c7e091ec73cea9c3a83bd8aac23", null ],
    [ "SetEnableBCParticles", "db/dcf/classG4HadronicParameters.html#ac125b2b70d94815d21ee17fb70414ce9", null ],
    [ "SetEnableCRCoalescence", "db/dcf/classG4HadronicParameters.html#ac9640475e7abca82b6626209a96eacc0", null ],
    [ "SetEnableHyperNuclei", "db/dcf/classG4HadronicParameters.html#a9d81d4e309bf1f88ea77ebd97bd9ef01", null ],
    [ "SetEnergyThresholdForHeavyHadrons", "db/dcf/classG4HadronicParameters.html#abac2d635807af14e8a45e7beede02723", null ],
    [ "SetMaxEnergy", "db/dcf/classG4HadronicParameters.html#aabdc4aee02e1680b8da220bf837aa11b", null ],
    [ "SetMaxEnergyTransitionFTF_Cascade", "db/dcf/classG4HadronicParameters.html#ade6ca6e5ef9486963b3f59a2cc108486", null ],
    [ "SetMaxEnergyTransitionQGS_FTF", "db/dcf/classG4HadronicParameters.html#aca525843369fd25888036895bc7d1033", null ],
    [ "SetMinEnergyTransitionFTF_Cascade", "db/dcf/classG4HadronicParameters.html#abcb261db3cf3ef1a216288fbfc24f3a7", null ],
    [ "SetMinEnergyTransitionQGS_FTF", "db/dcf/classG4HadronicParameters.html#abb9c4043044d807154b171f09dc72b95", null ],
    [ "SetVerboseLevel", "db/dcf/classG4HadronicParameters.html#a84ae96b3dc8a7d815eb51ef5602cf64e", null ],
    [ "SetXSFactorEM", "db/dcf/classG4HadronicParameters.html#ac7365d4049734a038b634803374c8f41", null ],
    [ "SetXSFactorHadronElastic", "db/dcf/classG4HadronicParameters.html#ac417012d6017e12b28345eedb447efb5", null ],
    [ "SetXSFactorHadronInelastic", "db/dcf/classG4HadronicParameters.html#acdf642051c485a2d6246ce2e3e85a394", null ],
    [ "SetXSFactorNucleonElastic", "db/dcf/classG4HadronicParameters.html#ad61a7c30562e5f6c028746857e1488b2", null ],
    [ "SetXSFactorNucleonInelastic", "db/dcf/classG4HadronicParameters.html#a6d0f89abda3ca99d7848c081fd59186f", null ],
    [ "SetXSFactorPionElastic", "db/dcf/classG4HadronicParameters.html#a95e67e088097a204323e14521145c2fc", null ],
    [ "SetXSFactorPionInelastic", "db/dcf/classG4HadronicParameters.html#a7c03ff47889b2557ec1ab9b1ab8df6f0", null ],
    [ "XSFactorEM", "db/dcf/classG4HadronicParameters.html#a967267828e461234a160a48dbd6dc734", null ],
    [ "XSFactorHadronElastic", "db/dcf/classG4HadronicParameters.html#af18178eaf39b5cde498c9ff628a2cea6", null ],
    [ "XSFactorHadronInelastic", "db/dcf/classG4HadronicParameters.html#ac07a8ce7a2b93e265eaffcd4b6336358", null ],
    [ "XSFactorNucleonElastic", "db/dcf/classG4HadronicParameters.html#a541f444fc5fa28e9e34c1fed43438321", null ],
    [ "XSFactorNucleonInelastic", "db/dcf/classG4HadronicParameters.html#af86d36c6e66a84a7e5211b326636e000", null ],
    [ "XSFactorPionElastic", "db/dcf/classG4HadronicParameters.html#a1ba7797e243ed9b317c4a0f23ed16ae4", null ],
    [ "XSFactorPionInelastic", "db/dcf/classG4HadronicParameters.html#aefd6a072ee1fdcb90d10d7c069b03ca4", null ],
    [ "fApplyFactorXS", "db/dcf/classG4HadronicParameters.html#a97196ba5e660dbaf0c51283da33a05af", null ],
    [ "fEnableBC", "db/dcf/classG4HadronicParameters.html#a579d7e624588dcdb3e2a5850460e66ff", null ],
    [ "fEnableCRCoalescence", "db/dcf/classG4HadronicParameters.html#aed68d817438426c124e4f341d61c39bf", null ],
    [ "fEnableHyperNuclei", "db/dcf/classG4HadronicParameters.html#a83356c9f457342e1a1e1cf633b6de10b", null ],
    [ "fEnergyThresholdForHeavyHadrons", "db/dcf/classG4HadronicParameters.html#ab8c5330a7163751276a791649f43b17f", null ],
    [ "fMaxEnergy", "db/dcf/classG4HadronicParameters.html#a8370acc5ad35241171e905d1c636e631", null ],
    [ "fMaxEnergyTransitionFTF_Cascade", "db/dcf/classG4HadronicParameters.html#a87d633f3cb17ffda4a3f198caf118c08", null ],
    [ "fMaxEnergyTransitionQGS_FTF", "db/dcf/classG4HadronicParameters.html#a879f96d76c11f8a0838060cf81c94386", null ],
    [ "fMessenger", "db/dcf/classG4HadronicParameters.html#af94194b6c549888ddd973a7f24044f22", null ],
    [ "fMinEnergyTransitionFTF_Cascade", "db/dcf/classG4HadronicParameters.html#a440a00ecab0a44ef412e930e2eeb5deb", null ],
    [ "fMinEnergyTransitionQGS_FTF", "db/dcf/classG4HadronicParameters.html#ac351cfb439f6b324899b5e667397c8ec", null ],
    [ "fVerboseLevel", "db/dcf/classG4HadronicParameters.html#a25dd97f56d4b98263d650861c1074756", null ],
    [ "fXSFactorEM", "db/dcf/classG4HadronicParameters.html#a14bdfc84d60847d6073135b8436fb741", null ],
    [ "fXSFactorHadronElastic", "db/dcf/classG4HadronicParameters.html#ad7b433b8c58b2739843926815c8f4712", null ],
    [ "fXSFactorHadronInelastic", "db/dcf/classG4HadronicParameters.html#a88a2e9e0cee93c1a7a38d2431c1643dc", null ],
    [ "fXSFactorLimit", "db/dcf/classG4HadronicParameters.html#a1dea634dd08013bd93b64a042eb03311", null ],
    [ "fXSFactorNucleonElastic", "db/dcf/classG4HadronicParameters.html#a681eb5a246b1a47ce7309602a94925d5", null ],
    [ "fXSFactorNucleonInelastic", "db/dcf/classG4HadronicParameters.html#ab619ce2716af879e651390de4183852d", null ],
    [ "fXSFactorPionElastic", "db/dcf/classG4HadronicParameters.html#af8c2a58474723b329900749a97abad26", null ],
    [ "fXSFactorPionInelastic", "db/dcf/classG4HadronicParameters.html#a43dd3e63421fe86d8567620e88b3b01e", null ],
    [ "sInstance", "db/dcf/classG4HadronicParameters.html#a0962a69468d368bd06123549c0208213", null ]
];