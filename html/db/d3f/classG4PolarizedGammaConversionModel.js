var classG4PolarizedGammaConversionModel =
[
    [ "G4PolarizedGammaConversionModel", "db/d3f/classG4PolarizedGammaConversionModel.html#a2db50df60aa19e096e645a026d4ad277", null ],
    [ "~G4PolarizedGammaConversionModel", "db/d3f/classG4PolarizedGammaConversionModel.html#aa6d95ba0ad886b067f4d22dafdb30c2d", null ],
    [ "G4PolarizedGammaConversionModel", "db/d3f/classG4PolarizedGammaConversionModel.html#a40bb9bf693e3f1f5e8497d6bbbedbc88", null ],
    [ "Initialise", "db/d3f/classG4PolarizedGammaConversionModel.html#a796c5f9d897d1b25e8e18e72d9ba6445", null ],
    [ "operator=", "db/d3f/classG4PolarizedGammaConversionModel.html#a89376a0550763594c48b406d6151dee5", null ],
    [ "SampleSecondaries", "db/d3f/classG4PolarizedGammaConversionModel.html#a4fc062964a159bc31c8b730d4188c399", null ],
    [ "SelectedAtom", "db/d3f/classG4PolarizedGammaConversionModel.html#ab6d51449f93a9d4eaa7dbc20b66cee36", null ],
    [ "fCrossSectionCalculator", "db/d3f/classG4PolarizedGammaConversionModel.html#af1ff1fd007932c91c8ad5d8a45f3e36e", null ]
];