var classG4VSubCutProducer =
[
    [ "G4VSubCutProducer", "db/da3/classG4VSubCutProducer.html#a264897f7c2c78d56fa8f0fc6464cabe7", null ],
    [ "~G4VSubCutProducer", "db/da3/classG4VSubCutProducer.html#af3eb9225c3ab7ae380552621fae11b81", null ],
    [ "G4VSubCutProducer", "db/da3/classG4VSubCutProducer.html#a64080bac193a2315acafb77ee9500c73", null ],
    [ "GetName", "db/da3/classG4VSubCutProducer.html#ae56f5a0593f3ee9ca6e5344e2a36985d", null ],
    [ "operator=", "db/da3/classG4VSubCutProducer.html#a5cc30fc378b4a421214d4fc047805b6c", null ],
    [ "SampleSecondaries", "db/da3/classG4VSubCutProducer.html#a32f3f6fb0500d967897a01ab31646d13", null ],
    [ "fName", "db/da3/classG4VSubCutProducer.html#a131b2540bf9065f0d24adf1a76532c59", null ]
];