var classG4XTRTransparentRegRadModel =
[
    [ "G4XTRTransparentRegRadModel", "db/da3/classG4XTRTransparentRegRadModel.html#a013b8e64605a190334e7dcdca6365a40", null ],
    [ "~G4XTRTransparentRegRadModel", "db/da3/classG4XTRTransparentRegRadModel.html#a4b7a197a559f81a37c97b61abd8151d3", null ],
    [ "DumpInfo", "db/da3/classG4XTRTransparentRegRadModel.html#ac01b1ed1b2b97c603504c93c4953463b", null ],
    [ "GetStackFactor", "db/da3/classG4XTRTransparentRegRadModel.html#a60b5514016adec9cf498ef2c7c9cf84f", null ],
    [ "ProcessDescription", "db/da3/classG4XTRTransparentRegRadModel.html#a658aaa211bb8d2032b8d79c60f226655", null ],
    [ "SpectralXTRdEdx", "db/da3/classG4XTRTransparentRegRadModel.html#aadd8934404c47e0116c297b5b8c38157", null ]
];