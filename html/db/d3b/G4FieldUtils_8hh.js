var G4FieldUtils_8hh =
[
    [ "ShortState", "db/d3b/G4FieldUtils_8hh.html#af6458a27c668e0abcfdad21041401d35", null ],
    [ "State", "db/d3b/G4FieldUtils_8hh.html#a32653b5bd29ad763f5114b7ff3300a22", null ],
    [ "Value1D", "db/d3b/G4FieldUtils_8hh.html#a50204ec47152fc4458a587478a718d40", [
      [ "KineticEnergy", "db/d3b/G4FieldUtils_8hh.html#a50204ec47152fc4458a587478a718d40a565c8ed0d027f8a44542443450feabda", null ],
      [ "LabTime", "db/d3b/G4FieldUtils_8hh.html#a50204ec47152fc4458a587478a718d40a9cd026e3cd904845ee736f60a0f07600", null ],
      [ "ProperTime", "db/d3b/G4FieldUtils_8hh.html#a50204ec47152fc4458a587478a718d40ad140942b680df8fc36919064027de8f3", null ]
    ] ],
    [ "Value3D", "db/d3b/G4FieldUtils_8hh.html#a976cc7f301f125400f7eee4909290b07", [
      [ "Position", "db/d3b/G4FieldUtils_8hh.html#a976cc7f301f125400f7eee4909290b07a52f5e0bc3859bc5f5e25130b6c7e8881", null ],
      [ "Momentum", "db/d3b/G4FieldUtils_8hh.html#a976cc7f301f125400f7eee4909290b07a37323780191d51a33d295ac10fb98f06", null ],
      [ "Spin", "db/d3b/G4FieldUtils_8hh.html#a976cc7f301f125400f7eee4909290b07af5a6a925d4084ae58bd71a8a95a84ba7", null ]
    ] ],
    [ "absoluteError", "db/d3b/G4FieldUtils_8hh.html#a3397ef3cf252b11d7462e6f28047fa07", null ],
    [ "clamp", "db/d3b/G4FieldUtils_8hh.html#ad07703b0a4ebe404be6adba1a8c2ad5a", null ],
    [ "copy", "db/d3b/G4FieldUtils_8hh.html#a729e76ec3e56f739a78ca68e20dde00f", null ],
    [ "getValue", "db/d3b/G4FieldUtils_8hh.html#a9f2877e4d2fb01d08bc34bfa3662d1f7", null ],
    [ "getValue", "db/d3b/G4FieldUtils_8hh.html#a3b7a59d9bb9154dc10a4bf29898286f5", null ],
    [ "getValue2", "db/d3b/G4FieldUtils_8hh.html#ac2b405e5ebb8a47bfd7317927414e5d6", null ],
    [ "getValue2", "db/d3b/G4FieldUtils_8hh.html#a79847e626921bd52f0dc6cd73df17ffd", null ],
    [ "inverseCurvatureRadius", "db/d3b/G4FieldUtils_8hh.html#a72cac75612ed4731394fa4b2f7200272", null ],
    [ "makeVector", "db/d3b/G4FieldUtils_8hh.html#a9fe37920450ae9d70c9cee40462100f8", null ],
    [ "relativeError", "db/d3b/G4FieldUtils_8hh.html#a0f167274476eb070ab5d97b21c012ae4", null ],
    [ "relativeError2", "db/d3b/G4FieldUtils_8hh.html#a68e815d6f9496326e28464933a6ac63a", null ],
    [ "setValue", "db/d3b/G4FieldUtils_8hh.html#af0a198ef3fcd32be8e9bd50bb45f9ad3", null ],
    [ "setValue", "db/d3b/G4FieldUtils_8hh.html#a771051760c51fe72fef388384e179111", null ]
];