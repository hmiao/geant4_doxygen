var classG4CollisionNNToNDelta1930 =
[
    [ "G4CollisionNNToNDelta1930", "db/d15/classG4CollisionNNToNDelta1930.html#af9c5dcf6e73aa809e68c381f764ad5ea", null ],
    [ "~G4CollisionNNToNDelta1930", "db/d15/classG4CollisionNNToNDelta1930.html#a2334cf3fdaee88a1d4aabb3039608dd4", null ],
    [ "G4CollisionNNToNDelta1930", "db/d15/classG4CollisionNNToNDelta1930.html#a2dafaae027cda6fcf8c1d226f799dc3e", null ],
    [ "GetComponents", "db/d15/classG4CollisionNNToNDelta1930.html#a83ffb2d95187599437e7c7b6381d32f6", null ],
    [ "GetListOfColliders", "db/d15/classG4CollisionNNToNDelta1930.html#a4dc4a672680c2f947d7080cbaaaef21c", null ],
    [ "GetName", "db/d15/classG4CollisionNNToNDelta1930.html#ab669b6825c4b9a54384ce5bce14f9bc7", null ],
    [ "operator=", "db/d15/classG4CollisionNNToNDelta1930.html#aea90257af867fc2c2232c4798311e785", null ],
    [ "components", "db/d15/classG4CollisionNNToNDelta1930.html#af54c73702983684487bda05f8beded7b", null ]
];