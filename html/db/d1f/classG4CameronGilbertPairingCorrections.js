var classG4CameronGilbertPairingCorrections =
[
    [ "G4CameronGilbertPairingCorrections", "db/d1f/classG4CameronGilbertPairingCorrections.html#a42fbaf670290bfe3db5d4b1e75939ea7", null ],
    [ "G4CameronGilbertPairingCorrections", "db/d1f/classG4CameronGilbertPairingCorrections.html#a934d2e3633eb83b5fda6632ff6e49390", null ],
    [ "GetPairingCorrection", "db/d1f/classG4CameronGilbertPairingCorrections.html#a7b86787ae046d867c015a5ff51d42d2d", null ],
    [ "operator=", "db/d1f/classG4CameronGilbertPairingCorrections.html#a8802f96a7ffd1b59f570c5480e8d9770", null ],
    [ "PairingNTable", "db/d1f/classG4CameronGilbertPairingCorrections.html#a87aa459a079d26049a36f125b8d38df2", null ],
    [ "PairingZTable", "db/d1f/classG4CameronGilbertPairingCorrections.html#a186919896f9813dc20485f0215e26892", null ]
];