var classG4VoxelSafety =
[
    [ "G4VoxelSafety", "db/de4/classG4VoxelSafety.html#aad084f93221d23166ab3b8a3a5a20398", null ],
    [ "~G4VoxelSafety", "db/de4/classG4VoxelSafety.html#aca1b8769df8fc6e78b569c52d4de7202", null ],
    [ "ComputeSafety", "db/de4/classG4VoxelSafety.html#a7032c1d0c2ecc76932c78ab04b32a92c", null ],
    [ "GetVerboseLevel", "db/de4/classG4VoxelSafety.html#a2de7c5b413dd8abee54343e6ea14547f", null ],
    [ "SafetyForVoxelHeader", "db/de4/classG4VoxelSafety.html#a9819e6fd313edbdb8ea6301756b1efdd", null ],
    [ "SafetyForVoxelNode", "db/de4/classG4VoxelSafety.html#abce4a8d261a56703b615cd5118f09e1f", null ],
    [ "SetVerboseLevel", "db/de4/classG4VoxelSafety.html#a39f09025f454d8610ace5a8a5ab97290", null ],
    [ "VoxelLocate", "db/de4/classG4VoxelSafety.html#a05030ac02558e021d16e471c3618e306", null ],
    [ "VoxelLocateLight", "db/de4/classG4VoxelSafety.html#aea95158fe35eb97ce419fe793d634103", null ],
    [ "fBlockList", "db/de4/classG4VoxelSafety.html#abd5548bc2c26c4523956baa05c0fafef", null ],
    [ "fCheck", "db/de4/classG4VoxelSafety.html#a7f46198a4f76681e08a7d2b1427ee3b8", null ],
    [ "fpMotherLogical", "db/de4/classG4VoxelSafety.html#a2fdc551f89c87d04300fdab8df1410da", null ],
    [ "fVerbose", "db/de4/classG4VoxelSafety.html#a7f4c0ab77235e5e3cbe3d148bb7a5f48", null ],
    [ "fVoxelAxisStack", "db/de4/classG4VoxelSafety.html#a38f8dfb5498b873194def15bd5cb8e2f", null ],
    [ "fVoxelDepth", "db/de4/classG4VoxelSafety.html#aa46d03fe3329207347d4242abfc01603", null ],
    [ "fVoxelHeaderStack", "db/de4/classG4VoxelSafety.html#af540cd8c5ab3131f335c3ee0ffc09189", null ],
    [ "fVoxelNode", "db/de4/classG4VoxelSafety.html#a1a372aee232387c319806f0635832924", null ],
    [ "fVoxelNodeNoStack", "db/de4/classG4VoxelSafety.html#aca9cf6370cb46d9490e209108fe53dd9", null ],
    [ "fVoxelNoSlicesStack", "db/de4/classG4VoxelSafety.html#a9841d2a65bb60f31f6168f4682d48c45", null ],
    [ "fVoxelSliceWidthStack", "db/de4/classG4VoxelSafety.html#ac3d47797a9bf2f98a869e2acf9856c6d", null ],
    [ "kCarTolerance", "db/de4/classG4VoxelSafety.html#a470f8a5d4acd74c7876e0e3541494313", null ]
];