var classG4QuarkExchange =
[
    [ "G4QuarkExchange", "db/d5f/classG4QuarkExchange.html#af0969d269dd9a1ac92272da21e4f1329", null ],
    [ "~G4QuarkExchange", "db/d5f/classG4QuarkExchange.html#ac723b19b4ef59008d10aadd439d8fd1c", null ],
    [ "G4QuarkExchange", "db/d5f/classG4QuarkExchange.html#a70d5eae4e7eae62cd25a2612612cf141", null ],
    [ "ExciteParticipants", "db/d5f/classG4QuarkExchange.html#a369000a20a93609b0f827c77390a9374", null ],
    [ "GaussianPt", "db/d5f/classG4QuarkExchange.html#acb911984b2d96e3a8e8a3df9600566cb", null ],
    [ "operator!=", "db/d5f/classG4QuarkExchange.html#afc48968f7d9896b12deb3f9fd2301f2e", null ],
    [ "operator=", "db/d5f/classG4QuarkExchange.html#a3788086055ab4c47d1047eb1efaddb7c", null ],
    [ "operator==", "db/d5f/classG4QuarkExchange.html#ae04dfbb9f1835e72feaac8d38da50b5c", null ],
    [ "StrangeSuppress", "db/d5f/classG4QuarkExchange.html#a2e1135080af522052b7a8d299b7c60d2", null ]
];