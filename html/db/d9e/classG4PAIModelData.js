var classG4PAIModelData =
[
    [ "G4PAIModelData", "db/d9e/classG4PAIModelData.html#a975d7327b0dedd1dc9578574daabcdd2", null ],
    [ "~G4PAIModelData", "db/d9e/classG4PAIModelData.html#a5e9dae557ae13fde8345057b0eadb272", null ],
    [ "G4PAIModelData", "db/d9e/classG4PAIModelData.html#ad061f3928d96d2862b02ce393a5115d9", null ],
    [ "CrossSectionPerVolume", "db/d9e/classG4PAIModelData.html#aa522656ae4a2748161b3758f945af3ac", null ],
    [ "DEDXPerVolume", "db/d9e/classG4PAIModelData.html#a121098c8321d9fc7fd463fd020a850fd", null ],
    [ "GetEnergyTransfer", "db/d9e/classG4PAIModelData.html#a64635ce1dab47468e1cb1bb4496304d8", null ],
    [ "Initialise", "db/d9e/classG4PAIModelData.html#a1f4d2146ab4b2d90b64dd8486c6a8cc8", null ],
    [ "operator=", "db/d9e/classG4PAIModelData.html#ac6da90f519f373233b46941d32fc4199", null ],
    [ "SampleAlongStepTransfer", "db/d9e/classG4PAIModelData.html#aa3a3648f48772c064fe65005a4422d3a", null ],
    [ "SamplePostStepTransfer", "db/d9e/classG4PAIModelData.html#a4a4bdf90ffdb5c9ab2d38956ede73701", null ],
    [ "fdEdxTable", "db/d9e/classG4PAIModelData.html#a5a56312b06a7ee3ae113f62930873686", null ],
    [ "fHighestKineticEnergy", "db/d9e/classG4PAIModelData.html#a53a2172054d7cbf32e78d71393802ecc", null ],
    [ "fLowestKineticEnergy", "db/d9e/classG4PAIModelData.html#a162b50040bc555c20940e91fe58e7a35", null ],
    [ "fPAIdEdxBank", "db/d9e/classG4PAIModelData.html#a3be15affb0348b40ccf52b924114586d", null ],
    [ "fPAIxscBank", "db/d9e/classG4PAIModelData.html#a1c0be89120f03e7a2c4236d8868afc95", null ],
    [ "fPAIySection", "db/d9e/classG4PAIModelData.html#a6c66da482189321118175c02185839bb", null ],
    [ "fParticleEnergyVector", "db/d9e/classG4PAIModelData.html#a73abe8af5e180803f7e1e811c86ed9a4", null ],
    [ "fSandia", "db/d9e/classG4PAIModelData.html#a3b2bc9c607731f0c5eae85d575100357", null ],
    [ "fTotBin", "db/d9e/classG4PAIModelData.html#a076e51684691d29f9865001c96ebb0d7", null ]
];