var classG4RegionModels =
[
    [ "G4RegionModels", "db/d1d/classG4RegionModels.html#a20f8423ea1fe3b4ed7247c2a1daa2170", null ],
    [ "~G4RegionModels", "db/d1d/classG4RegionModels.html#a8288f962b537b0d90a432082e07484f6", null ],
    [ "G4RegionModels", "db/d1d/classG4RegionModels.html#a4ceeb1a1f97f0cf66ba69cf9b3f5d7c6", null ],
    [ "LowEdgeEnergy", "db/d1d/classG4RegionModels.html#ae581c583336eac9e6311cc9079016b70", null ],
    [ "ModelIndex", "db/d1d/classG4RegionModels.html#a8e411dea43b45ad276ffccb3c930a9e3", null ],
    [ "NumberOfModels", "db/d1d/classG4RegionModels.html#ac3a68e4cd50fb830ce5ada5797fa7a3d", null ],
    [ "operator=", "db/d1d/classG4RegionModels.html#a8c0fbe62141cdbd03c4dc785ceef0ff6", null ],
    [ "Region", "db/d1d/classG4RegionModels.html#a4a64b5f2470e7be55d4f971d6667d90c", null ],
    [ "SelectIndex", "db/d1d/classG4RegionModels.html#af1209d17ad535e89143dfc2e4e4eec90", null ],
    [ "G4EmModelManager", "db/d1d/classG4RegionModels.html#a65a3b06b6afe5cf9fe0b084509831f1a", null ],
    [ "lowKineticEnergy", "db/d1d/classG4RegionModels.html#a5245ceb1764fa0a10e6c95eba8ccf6c8", null ],
    [ "nModelsForRegion", "db/d1d/classG4RegionModels.html#a0663e4884714841a4dee5e021ce3c27d", null ],
    [ "theListOfModelIndexes", "db/d1d/classG4RegionModels.html#a3b88445bac42d5f3e3d686f1ef1b4f18", null ],
    [ "theRegion", "db/d1d/classG4RegionModels.html#a3ad1ac647f4be55f693045262dcb97e9", null ]
];