var classG4AngularDistributionNP =
[
    [ "G4AngularDistributionNP", "db/d8b/classG4AngularDistributionNP.html#ab5f5ae1a5edef1c676e7e8504bd35cd3", null ],
    [ "~G4AngularDistributionNP", "db/d8b/classG4AngularDistributionNP.html#a56d442ea0b490552898c25be36f64641", null ],
    [ "CosTheta", "db/d8b/classG4AngularDistributionNP.html#a3ab848e4b4feb5cd9ea64e1a0bfcc3a5", null ],
    [ "Phi", "db/d8b/classG4AngularDistributionNP.html#a84706ebe752db2a72cb1c87adb3ec0e9", null ],
    [ "dsigmax", "db/d8b/classG4AngularDistributionNP.html#a09f7828940a9a497fd60d8072e434190", null ],
    [ "elab", "db/d8b/classG4AngularDistributionNP.html#aeb774b2f1302593e8d2418e3d71b44c4", null ],
    [ "pcm", "db/d8b/classG4AngularDistributionNP.html#a739365ffce879c9c2a0e10ca5b1db710", null ],
    [ "sig", "db/d8b/classG4AngularDistributionNP.html#a3371bbe6f642645dcdb4c3f51d1ba3c2", null ],
    [ "sigtot", "db/d8b/classG4AngularDistributionNP.html#a34102fc0faa589626866635e9956b36d", null ]
];