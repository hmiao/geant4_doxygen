var classG4PSPassageCellFluxForCylinder3D =
[
    [ "G4PSPassageCellFluxForCylinder3D", "db/dd5/classG4PSPassageCellFluxForCylinder3D.html#ab8c2e52f4d69314449ab8f973b7b1645", null ],
    [ "G4PSPassageCellFluxForCylinder3D", "db/dd5/classG4PSPassageCellFluxForCylinder3D.html#a368a95cf3992e97c3f0eed33786c96df", null ],
    [ "~G4PSPassageCellFluxForCylinder3D", "db/dd5/classG4PSPassageCellFluxForCylinder3D.html#a728c5f60937a4a9248751933168ef15e", null ],
    [ "ComputeVolume", "db/dd5/classG4PSPassageCellFluxForCylinder3D.html#a96db9cb15db39dba1154735a120cf54b", null ],
    [ "SetCylinderSize", "db/dd5/classG4PSPassageCellFluxForCylinder3D.html#a237974f09ad493ab903e4d5ddc9c8b1e", null ],
    [ "SetNumberOfSegments", "db/dd5/classG4PSPassageCellFluxForCylinder3D.html#a868736748e62a2fff1d168bb49601d15", null ],
    [ "cylinderSize", "db/dd5/classG4PSPassageCellFluxForCylinder3D.html#ad53cc94f91dc312115d2ec685cf5abcf", null ],
    [ "fAngle", "db/dd5/classG4PSPassageCellFluxForCylinder3D.html#a6f374af20da38b88a0c3aa2463ab1c64", null ],
    [ "nSegment", "db/dd5/classG4PSPassageCellFluxForCylinder3D.html#a345d79edc96c77ee241f9855de061583", null ]
];