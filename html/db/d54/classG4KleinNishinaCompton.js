var classG4KleinNishinaCompton =
[
    [ "G4KleinNishinaCompton", "db/d54/classG4KleinNishinaCompton.html#a87d5fd9ea366282f0d060a982b2445c2", null ],
    [ "~G4KleinNishinaCompton", "db/d54/classG4KleinNishinaCompton.html#af6b486d0e262ec3aeb527f0fcb3fd5a6", null ],
    [ "G4KleinNishinaCompton", "db/d54/classG4KleinNishinaCompton.html#a11e6b40e94257a1d7b70c71cabc48af5", null ],
    [ "ComputeCrossSectionPerAtom", "db/d54/classG4KleinNishinaCompton.html#a31017db0017829555aff9945d64acdfd", null ],
    [ "Initialise", "db/d54/classG4KleinNishinaCompton.html#a1ce8cc97540c7f2b4553019dc3974875", null ],
    [ "InitialiseLocal", "db/d54/classG4KleinNishinaCompton.html#a6480068b06b1482e1f1d6c6b54246f12", null ],
    [ "operator=", "db/d54/classG4KleinNishinaCompton.html#afb9681ed3d2c30058c62b6f38abb5b21", null ],
    [ "SampleSecondaries", "db/d54/classG4KleinNishinaCompton.html#af02fe8b3c1f54c54e3ec0d009c5f001e", null ],
    [ "fParticleChange", "db/d54/classG4KleinNishinaCompton.html#aaf66aa8b93029d7df979fceaa43b5b83", null ],
    [ "lowestSecondaryEnergy", "db/d54/classG4KleinNishinaCompton.html#a1e5696284b3a1d21eb79b6c08809f524", null ],
    [ "theElectron", "db/d54/classG4KleinNishinaCompton.html#aee8e16b265c5fdb4f9962a2a2e1229d4", null ],
    [ "theGamma", "db/d54/classG4KleinNishinaCompton.html#aa5956093250e74e147777726e4c1cb1e", null ]
];