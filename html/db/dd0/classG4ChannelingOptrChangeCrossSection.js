var classG4ChannelingOptrChangeCrossSection =
[
    [ "G4ChannelingOptrChangeCrossSection", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#afa09fd1c2eb73d7425fd791a9432018a", null ],
    [ "~G4ChannelingOptrChangeCrossSection", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#a59b97d35127d22fa44af4b8c3e6242c2", null ],
    [ "OperationApplied", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#a25a0a35bc02747957669f357bf363b3d", null ],
    [ "OperationApplied", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#a2fa52a6b34df76bdfe54bde845454b3e", null ],
    [ "OperationApplied", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#a3ac6796c69eff4d52c64afc10162ec49", null ],
    [ "ProposeFinalStateBiasingOperation", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#a354a14db7a55d9df1668136bca06c701", null ],
    [ "ProposeNonPhysicsBiasingOperation", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#ac1187e23e980400610b4db87a587fb4f", null ],
    [ "ProposeOccurenceBiasingOperation", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#a67dd1e1aa064615d6f42d2a0372d1c8b", null ],
    [ "StartRun", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#aabed7ae4883ae9733b3901ace5ab4192", null ],
    [ "fChangeCrossSectionOperations", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#afeef889c055ed16c1c99c8f4d7bf549c", null ],
    [ "fChannelingID", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#ab1973e83e16d62c0b8f57ab89eb4f14f", null ],
    [ "fParticleToBias", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#af64e399b6f28c8e82086d600dcd02470", null ],
    [ "fProcessToDensity", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#aec2f4f9187a1cd1cda5c5f6289f177c9", null ],
    [ "fSetup", "db/dd0/classG4ChannelingOptrChangeCrossSection.html#ae1d6464d6c8d773308ceba7d6357ff2d", null ]
];