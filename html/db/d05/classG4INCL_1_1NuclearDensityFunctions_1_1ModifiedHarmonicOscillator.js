var classG4INCL_1_1NuclearDensityFunctions_1_1ModifiedHarmonicOscillator =
[
    [ "ModifiedHarmonicOscillator", "db/d05/classG4INCL_1_1NuclearDensityFunctions_1_1ModifiedHarmonicOscillator.html#a5b00e841940d6172aa56c9ff9aaaaebe", null ],
    [ "getDiffusenessParameter", "db/d05/classG4INCL_1_1NuclearDensityFunctions_1_1ModifiedHarmonicOscillator.html#a1f9c5ab8e1eef1a2025242df3f40eaa3", null ],
    [ "getRadiusParameter", "db/d05/classG4INCL_1_1NuclearDensityFunctions_1_1ModifiedHarmonicOscillator.html#a39562683f0428e3ce5b043662c421373", null ],
    [ "operator()", "db/d05/classG4INCL_1_1NuclearDensityFunctions_1_1ModifiedHarmonicOscillator.html#ae50db261a28fc3fe7fe75290057ed3b5", null ],
    [ "setDiffusenessParameter", "db/d05/classG4INCL_1_1NuclearDensityFunctions_1_1ModifiedHarmonicOscillator.html#a16f880cc5d4c84c94cee1cb6413bf522", null ],
    [ "setRadiusParameter", "db/d05/classG4INCL_1_1NuclearDensityFunctions_1_1ModifiedHarmonicOscillator.html#a4750a81870db2841928d13bada809ef1", null ],
    [ "normalisation", "db/d05/classG4INCL_1_1NuclearDensityFunctions_1_1ModifiedHarmonicOscillator.html#aa9bc9d356a67875e3bd1e1b03d5e32c5", null ],
    [ "theDiffusenessParameter", "db/d05/classG4INCL_1_1NuclearDensityFunctions_1_1ModifiedHarmonicOscillator.html#ac16da8d733ae3ebfb79c36d0b26f768d", null ],
    [ "theRadiusParameter", "db/d05/classG4INCL_1_1NuclearDensityFunctions_1_1ModifiedHarmonicOscillator.html#aa5caddeb38a2917c7c634fe45f1bcb4a", null ]
];