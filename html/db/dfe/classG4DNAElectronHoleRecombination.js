var classG4DNAElectronHoleRecombination =
[
    [ "ReactantInfo", "de/d73/structG4DNAElectronHoleRecombination_1_1ReactantInfo.html", "de/d73/structG4DNAElectronHoleRecombination_1_1ReactantInfo" ],
    [ "State", "df/d54/structG4DNAElectronHoleRecombination_1_1State.html", "df/d54/structG4DNAElectronHoleRecombination_1_1State" ],
    [ "G4DNAElectronHoleRecombination", "db/dfe/classG4DNAElectronHoleRecombination.html#a4e70e466f565d42de3f64d7afd121be8", null ],
    [ "~G4DNAElectronHoleRecombination", "db/dfe/classG4DNAElectronHoleRecombination.html#acfa1ea0e65fdd40b9d246e6087568263", null ],
    [ "AtRestDoIt", "db/dfe/classG4DNAElectronHoleRecombination.html#aa8b7764bb30ad67c5df5e6a7fd7acfb5", null ],
    [ "BuildPhysicsTable", "db/dfe/classG4DNAElectronHoleRecombination.html#a04fd6db6d6eb90c4bc738ab9ba31794b", null ],
    [ "Create", "db/dfe/classG4DNAElectronHoleRecombination.html#a614e125b7ddced5139273e46b57cdb44", null ],
    [ "FindReactant", "db/dfe/classG4DNAElectronHoleRecombination.html#af91672c536bc59fa447dcf43b0e3b7c6", null ],
    [ "GetMeanFreePath", "db/dfe/classG4DNAElectronHoleRecombination.html#ae137d2cd020d0c97b15f3dbf46ec258e", null ],
    [ "GetMeanLifeTime", "db/dfe/classG4DNAElectronHoleRecombination.html#a0cfb830150f17ff940d724db900cce97", null ],
    [ "IsApplicable", "db/dfe/classG4DNAElectronHoleRecombination.html#a604885c5b7d37b4b7e673750afec32c7", null ],
    [ "MakeReaction", "db/dfe/classG4DNAElectronHoleRecombination.html#a76c636d691e40716ec37549615aac305", null ],
    [ "PostStepDoIt", "db/dfe/classG4DNAElectronHoleRecombination.html#a4dc146049f34b3009629f7db2c205d72", null ],
    [ "StartTracking", "db/dfe/classG4DNAElectronHoleRecombination.html#a8f8c6ad70988c13c2e73226e9f4b716f", null ],
    [ "fIsInitialized", "db/dfe/classG4DNAElectronHoleRecombination.html#a239e3f60f7ccf589db9c1022ed914474", null ],
    [ "fOnsagerRadiusPerMaterial", "db/dfe/classG4DNAElectronHoleRecombination.html#abb4f9a9e612b4186442cf102a90e30a8", null ],
    [ "fParticleChange", "db/dfe/classG4DNAElectronHoleRecombination.html#a5cd04ad0bc64d3b62310ff518afacd4a", null ],
    [ "fpMoleculeDensity", "db/dfe/classG4DNAElectronHoleRecombination.html#a67485f01f62837b9ef0fb416809a20ed", null ]
];