var classG4XnpTotalLowE =
[
    [ "G4XnpTotalLowE", "db/da5/classG4XnpTotalLowE.html#adbf40b434ddedfac9298d721b105470e", null ],
    [ "~G4XnpTotalLowE", "db/da5/classG4XnpTotalLowE.html#a31cf76037c19c4b57f9903407884123d", null ],
    [ "G4XnpTotalLowE", "db/da5/classG4XnpTotalLowE.html#afbcbbfb0dc6a774b8030fae0d74eeb1f", null ],
    [ "CrossSection", "db/da5/classG4XnpTotalLowE.html#a11aaf81b711b6f87279d8bbc2c477300", null ],
    [ "GetComponents", "db/da5/classG4XnpTotalLowE.html#ab33994866d0b7ddb9f22a5b10f4a77fe", null ],
    [ "HighLimit", "db/da5/classG4XnpTotalLowE.html#a932f7d48a4c93e0d9415fe75822cfa90", null ],
    [ "IsValid", "db/da5/classG4XnpTotalLowE.html#a73d38feed4c2a9fc9e071f3786d9f69b", null ],
    [ "Name", "db/da5/classG4XnpTotalLowE.html#af53834933e84caef38a0afd7cfee721d", null ],
    [ "operator!=", "db/da5/classG4XnpTotalLowE.html#aaa6dd2c6aa65903e5d8b0be630d46458", null ],
    [ "operator=", "db/da5/classG4XnpTotalLowE.html#a8fce4e49ccce0ecdc228fe3b4faba16f", null ],
    [ "operator==", "db/da5/classG4XnpTotalLowE.html#ab67f03a0204a3f7ed8fe47ca4efcf548", null ],
    [ "Print", "db/da5/classG4XnpTotalLowE.html#a02c11436960a317cbcd49f5aeb965c62", null ],
    [ "_eMax", "db/da5/classG4XnpTotalLowE.html#a34e013f7170fafadaf41ded3882bf645", null ],
    [ "_eMin", "db/da5/classG4XnpTotalLowE.html#af9c4f9d2e8c8cb0ad3612949a0f50988", null ],
    [ "_eMinTable", "db/da5/classG4XnpTotalLowE.html#a38ad1de85f50812a63fd0232c157a5f4", null ],
    [ "_eStepLog", "db/da5/classG4XnpTotalLowE.html#aa6cb7c0ad6f848577cb8047a4c7ccd38", null ],
    [ "_highLimit", "db/da5/classG4XnpTotalLowE.html#a8131eafca06873723f73c164ccb61956", null ],
    [ "_lowLimit", "db/da5/classG4XnpTotalLowE.html#afc34be861eddb9a403def4f76d972dbe", null ],
    [ "_sigma", "db/da5/classG4XnpTotalLowE.html#aa45d41477f8844c2a4bf7da5c670e471", null ],
    [ "_sigmaTable", "db/da5/classG4XnpTotalLowE.html#a6d80d9ede6e8ea98ac59749554ca9fdc", null ],
    [ "_tableSize", "db/da5/classG4XnpTotalLowE.html#af69f0055382a294978dab3ce9d497bb3", null ]
];