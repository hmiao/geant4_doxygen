var classG4ReduciblePolygonIterator =
[
    [ "G4ReduciblePolygonIterator", "db/dd6/classG4ReduciblePolygonIterator.html#ab4d05790c7f8f8ea673b2eb71a28f486", null ],
    [ "Begin", "db/dd6/classG4ReduciblePolygonIterator.html#ab42839af6155e8c76a6a19f138521bb3", null ],
    [ "GetA", "db/dd6/classG4ReduciblePolygonIterator.html#ad0f43ad2c5afc61051bfcea504166697", null ],
    [ "GetB", "db/dd6/classG4ReduciblePolygonIterator.html#a8c6f0c2ec46b65cbdb9078dac00e6657", null ],
    [ "Next", "db/dd6/classG4ReduciblePolygonIterator.html#a2cff3449e59ceca42206b2f066fc7ab5", null ],
    [ "Valid", "db/dd6/classG4ReduciblePolygonIterator.html#ab181d8880380c0ed7f03aad23033fdaf", null ],
    [ "current", "db/dd6/classG4ReduciblePolygonIterator.html#a56b4670337a83d8f2fd110fafc28e22a", null ],
    [ "subject", "db/dd6/classG4ReduciblePolygonIterator.html#acca83b0be5c18b3672f7ad3249bcf400", null ]
];