var classG4CollisionNNElastic =
[
    [ "G4CollisionNNElastic", "d4/de5/classG4CollisionNNElastic.html#a4f509673fd1315678ceb109e89bd6687", null ],
    [ "~G4CollisionNNElastic", "d4/de5/classG4CollisionNNElastic.html#a050bfb7367990b06cf47396f0164250d", null ],
    [ "G4CollisionNNElastic", "d4/de5/classG4CollisionNNElastic.html#af816126f3b67b607a105f2324b620841", null ],
    [ "GetAngularDistribution", "d4/de5/classG4CollisionNNElastic.html#ad7d6803f395d1b89452a613fd776847b", null ],
    [ "GetCrossSectionSource", "d4/de5/classG4CollisionNNElastic.html#a7d481e21d9051efcc019c317c465dd87", null ],
    [ "GetListOfColliders", "d4/de5/classG4CollisionNNElastic.html#ab22d27c313892b051e0ff7bff3246c2f", null ],
    [ "GetName", "d4/de5/classG4CollisionNNElastic.html#a6c825fa596db115183679d566fc7c699", null ],
    [ "IsInCharge", "d4/de5/classG4CollisionNNElastic.html#aa0e94eda9e795efd566d28c54d259570", null ],
    [ "operator!=", "d4/de5/classG4CollisionNNElastic.html#a8fe9bc705adb763ccc5ff24f3b555c8c", null ],
    [ "operator=", "d4/de5/classG4CollisionNNElastic.html#a9bd1ee799e74782796680f63d5e63ff7", null ],
    [ "operator==", "d4/de5/classG4CollisionNNElastic.html#a3066dd2caffaaa6332398d2c85bca365", null ],
    [ "angularDistribution", "d4/de5/classG4CollisionNNElastic.html#afdf67d02fc0f65cea3f4ba7ab613a3a0", null ],
    [ "colliders1", "d4/de5/classG4CollisionNNElastic.html#ad44417aab6f6d8133872127b1537e028", null ],
    [ "colliders2", "d4/de5/classG4CollisionNNElastic.html#a62bf6ca91866ca483bee4660a4a8d3fb", null ],
    [ "crossSectionSource", "d4/de5/classG4CollisionNNElastic.html#a6808ed2ec9c1f3df5a41321c6d81efa7", null ]
];