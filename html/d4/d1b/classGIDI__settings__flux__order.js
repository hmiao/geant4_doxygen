var classGIDI__settings__flux__order =
[
    [ "GIDI_settings_flux_order", "d4/d1b/classGIDI__settings__flux__order.html#a2c5ece8ea15c824bf637d94a2362e445", null ],
    [ "GIDI_settings_flux_order", "d4/d1b/classGIDI__settings__flux__order.html#a2158beeef4ba2e31cf35827922e1a76f", null ],
    [ "GIDI_settings_flux_order", "d4/d1b/classGIDI__settings__flux__order.html#a2386b55d52cad57a6837f6bcf94f139a", null ],
    [ "GIDI_settings_flux_order", "d4/d1b/classGIDI__settings__flux__order.html#ab7a16d87b957530931586f21bb078711", null ],
    [ "~GIDI_settings_flux_order", "d4/d1b/classGIDI__settings__flux__order.html#a1618f9216354aa0138b6f81fb8023df4", null ],
    [ "getEnergies", "d4/d1b/classGIDI__settings__flux__order.html#a4561671f4ede91065a5f271afe78795f", null ],
    [ "getFluxes", "d4/d1b/classGIDI__settings__flux__order.html#ac0fe8d55f71ed4b07fddec7cb8931a62", null ],
    [ "getOrder", "d4/d1b/classGIDI__settings__flux__order.html#ad953febc9e8ae000b3aecf082d54bdde", null ],
    [ "initialize", "d4/d1b/classGIDI__settings__flux__order.html#a0afc37704f0898448ecbcb51adcfe125", null ],
    [ "operator=", "d4/d1b/classGIDI__settings__flux__order.html#a10fe11316873562074cb0c16a9a8575c", null ],
    [ "print", "d4/d1b/classGIDI__settings__flux__order.html#a9b6bbe73324267693b38c6a8957b20cb", null ],
    [ "size", "d4/d1b/classGIDI__settings__flux__order.html#ac36ff2d267cc0e3fb4d36942717ecfd4", null ],
    [ "mEnergies", "d4/d1b/classGIDI__settings__flux__order.html#ae7c7d1c1e82c9581d16014de56b952e3", null ],
    [ "mFluxes", "d4/d1b/classGIDI__settings__flux__order.html#a90f43631ae8df499755e3712c7621765", null ],
    [ "mOrder", "d4/d1b/classGIDI__settings__flux__order.html#acc132472167329bc3a99af5e79bbb302", null ]
];