var classG4PSTARStopping =
[
    [ "G4PSTARStopping", "d4/dd1/classG4PSTARStopping.html#a66a3a83872044cbe08335434243da1b4", null ],
    [ "~G4PSTARStopping", "d4/dd1/classG4PSTARStopping.html#a48d905c0c54f1419e294fdf4b23fc813", null ],
    [ "G4PSTARStopping", "d4/dd1/classG4PSTARStopping.html#ad03348eefa20422a4edf0405b7d642c2", null ],
    [ "AddData", "d4/dd1/classG4PSTARStopping.html#ac7ff2c410f1669a55ebecadadecc5b20", null ],
    [ "FindData", "d4/dd1/classG4PSTARStopping.html#ab2ba92641e66d11b7ce716e5903ee39b", null ],
    [ "GetElectronicDEDX", "d4/dd1/classG4PSTARStopping.html#ae556660ce92dae01c615736c4c860c6e", null ],
    [ "GetElectronicDEDX", "d4/dd1/classG4PSTARStopping.html#a33acf3ea6997d153040d5d58e9c778ee", null ],
    [ "GetIndex", "d4/dd1/classG4PSTARStopping.html#a09e8bd258e4eea6317e16d29b6fac6d1", null ],
    [ "GetIndex", "d4/dd1/classG4PSTARStopping.html#a9a84508f247621f9c50fee66b883ddaf", null ],
    [ "Initialise", "d4/dd1/classG4PSTARStopping.html#a6707df0b29a194ed8f691e95bddd93b3", null ],
    [ "operator=", "d4/dd1/classG4PSTARStopping.html#acbe6ef1c4fee95691bdc1c0c9b359a2c", null ],
    [ "PrintWarning", "d4/dd1/classG4PSTARStopping.html#a7fafc7a5fb9a78113b92227e3c0c64d8", null ],
    [ "emin", "d4/dd1/classG4PSTARStopping.html#a7c41bbce9c8c95957a07a35a14a09f06", null ],
    [ "materials", "d4/dd1/classG4PSTARStopping.html#a4960ef835d3049016e16c14dea0b5822", null ],
    [ "nvectors", "d4/dd1/classG4PSTARStopping.html#a59ceafac379a941e3c7452d84b6f3deb", null ],
    [ "sdata", "d4/dd1/classG4PSTARStopping.html#aa8f8e62b77da0bb61ee4e8b4714218f1", null ]
];