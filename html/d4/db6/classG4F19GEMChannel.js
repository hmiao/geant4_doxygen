var classG4F19GEMChannel =
[
    [ "G4F19GEMChannel", "d4/db6/classG4F19GEMChannel.html#aaa91f52dbb6d8b09b1d6cffb4ac633d0", null ],
    [ "~G4F19GEMChannel", "d4/db6/classG4F19GEMChannel.html#ac723900787011572903584497fd7d6cc", null ],
    [ "G4F19GEMChannel", "d4/db6/classG4F19GEMChannel.html#a7d636ee1283472400e83d543b6b14c95", null ],
    [ "operator!=", "d4/db6/classG4F19GEMChannel.html#a6374b83212715931745b2c5d40028fe8", null ],
    [ "operator=", "d4/db6/classG4F19GEMChannel.html#aa8ce51e1e06606ac3eac3c430a9c79f9", null ],
    [ "operator==", "d4/db6/classG4F19GEMChannel.html#a1938409943bfa88beaff73b1b035ec9c", null ],
    [ "theEvaporationProbability", "d4/db6/classG4F19GEMChannel.html#ac8b72707ed33bd67aeefe3b8d90d5498", null ]
];