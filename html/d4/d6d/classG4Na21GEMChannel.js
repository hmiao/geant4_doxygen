var classG4Na21GEMChannel =
[
    [ "G4Na21GEMChannel", "d4/d6d/classG4Na21GEMChannel.html#a5339afca114e2065db1d534d49aa8f89", null ],
    [ "~G4Na21GEMChannel", "d4/d6d/classG4Na21GEMChannel.html#aad220e461ec2c018adb920c76e3e9bb7", null ],
    [ "G4Na21GEMChannel", "d4/d6d/classG4Na21GEMChannel.html#ab4299e0dcf784f8688010e199d65fe04", null ],
    [ "operator!=", "d4/d6d/classG4Na21GEMChannel.html#a47ca274154e84486f54bdf0caf4932fb", null ],
    [ "operator=", "d4/d6d/classG4Na21GEMChannel.html#a33df591cdbcd989835d47eabb0894d7d", null ],
    [ "operator==", "d4/d6d/classG4Na21GEMChannel.html#ac5d2b55f2c69a0f43ddce28f1f536be6", null ],
    [ "theEvaporationProbability", "d4/d6d/classG4Na21GEMChannel.html#af0b56a312487b1512a06a05fc405b23f", null ]
];