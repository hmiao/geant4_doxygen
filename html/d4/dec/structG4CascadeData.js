var structG4CascadeData =
[
    [ "G4CascadeData", "d4/dec/structG4CascadeData.html#a1cd6c6f62c94f7cf71a33ea638f81910", null ],
    [ "G4CascadeData", "d4/dec/structG4CascadeData.html#a73cf8e73e968057f85139993c80e5b7e", null ],
    [ "G4CascadeData", "d4/dec/structG4CascadeData.html#a648f89286913d78304dda142598d77e3", null ],
    [ "G4CascadeData", "d4/dec/structG4CascadeData.html#a0b90b9e9b7bd823e037f387e58c44b7f", null ],
    [ "initialize", "d4/dec/structG4CascadeData.html#a4d439c5a763b463d421a129fe02efb09", null ],
    [ "maxMultiplicity", "d4/dec/structG4CascadeData.html#a4f1e81555dbc6e75c39c8bb027b69704", null ],
    [ "print", "d4/dec/structG4CascadeData.html#ad5a03c0cb1fb417ec3b8e0c463b8945e", null ],
    [ "print", "d4/dec/structG4CascadeData.html#a1db750b103d1f56c2402f1ef837d54c6", null ],
    [ "printXsec", "d4/dec/structG4CascadeData.html#aac392f0b10d2ffea5784b5764e918127", null ],
    [ "crossSections", "d4/dec/structG4CascadeData.html#a89620b50f483d5df706fbef38bbd3393", null ],
    [ "empty8bfs", "d4/dec/structG4CascadeData.html#a1080df0806c83870c21b82c0c3747a8b", null ],
    [ "empty9bfs", "d4/dec/structG4CascadeData.html#ae7a50f238d2e66206d9a557e1959ad3a", null ],
    [ "index", "d4/dec/structG4CascadeData.html#ad5c9a2f1e54239447d44a442ba906de4", null ],
    [ "inelastic", "d4/dec/structG4CascadeData.html#a686c30ec7351d5af7e4e919e342efbc2", null ],
    [ "initialState", "d4/dec/structG4CascadeData.html#a68923dae20d3ac42ca6b951379ac8739", null ],
    [ "multiplicities", "d4/dec/structG4CascadeData.html#a6e543dcd4680224a0b53710433da9683", null ],
    [ "name", "d4/dec/structG4CascadeData.html#adeb9b30fc10ef921c1306a19e896db58", null ],
    [ "sum", "d4/dec/structG4CascadeData.html#a0eacbe07d1b7759c28a812ade854c4de", null ],
    [ "tot", "d4/dec/structG4CascadeData.html#a98bf19b7346ccdd771a7069bbbf18b6e", null ],
    [ "x2bfs", "d4/dec/structG4CascadeData.html#af9c8cabcac149ab035b67effaf8e6c31", null ],
    [ "x3bfs", "d4/dec/structG4CascadeData.html#acca54ca59eab95fdfc7f219d0546b6ac", null ],
    [ "x4bfs", "d4/dec/structG4CascadeData.html#afc89413a2a1113552e992d22eb6947e0", null ],
    [ "x5bfs", "d4/dec/structG4CascadeData.html#a9397338647f56d1762f05560c702426c", null ],
    [ "x6bfs", "d4/dec/structG4CascadeData.html#af35c33e7d5ec97b5ef26967e5f251c83", null ],
    [ "x7bfs", "d4/dec/structG4CascadeData.html#a9427b7e0555bbb9df0a1a5eaf77f94aa", null ],
    [ "x8bfs", "d4/dec/structG4CascadeData.html#ae7ff1bb6ed08d6fec7ea04b4cc0b010f", null ],
    [ "x9bfs", "d4/dec/structG4CascadeData.html#a5e671c6f4b7bd90cadd05a2aedb48263", null ]
];