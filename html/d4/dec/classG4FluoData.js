var classG4FluoData =
[
    [ "G4FluoData", "d4/dec/classG4FluoData.html#ac9101cb691c2633782b769b0e638cb3b", null ],
    [ "~G4FluoData", "d4/dec/classG4FluoData.html#ac05829ac80d7f53feca95fe17b45dd9b", null ],
    [ "G4FluoData", "d4/dec/classG4FluoData.html#a13b75307ed610914993d8c8a164c5ca3", null ],
    [ "LoadData", "d4/dec/classG4FluoData.html#a9f57ff3309e3a585d864e813caec9bb4", null ],
    [ "NumberOfTransitions", "d4/dec/classG4FluoData.html#a49ecd583ec1729ce941b018fd8b23534", null ],
    [ "NumberOfVacancies", "d4/dec/classG4FluoData.html#a666a084228fb1e489e73a335e458065a", null ],
    [ "operator=", "d4/dec/classG4FluoData.html#ae40e5db7d49e9e40cb7f5b61fbfb7655", null ],
    [ "PrintData", "d4/dec/classG4FluoData.html#a64de1e71e414190b8f86164af2321236", null ],
    [ "StartShellEnergy", "d4/dec/classG4FluoData.html#a1af25f21bd291d23ecfc799e510d035d", null ],
    [ "StartShellId", "d4/dec/classG4FluoData.html#ada9ed409c6a272874f4620e8116954fd", null ],
    [ "StartShellProb", "d4/dec/classG4FluoData.html#af503ffd05e8a73356c2934c8f5677fb6", null ],
    [ "VacancyId", "d4/dec/classG4FluoData.html#a5a76e41544fa0dc197782e9127615a7d", null ],
    [ "energyMap", "d4/dec/classG4FluoData.html#a12eb77af8acb71b409f6784976079270", null ],
    [ "fluoDirectory", "d4/dec/classG4FluoData.html#afbb030765580906c811d6da0dc19afd0", null ],
    [ "fluoTransitionTable", "d4/dec/classG4FluoData.html#a405fc6d32e7e13b089d8e7861a506685", null ],
    [ "idMap", "d4/dec/classG4FluoData.html#a1c8bcf59f6b42f3b451e057ada617b81", null ],
    [ "nInitShells", "d4/dec/classG4FluoData.html#a134f294c593172194c2d85b18a7a6f63", null ],
    [ "numberOfVacancies", "d4/dec/classG4FluoData.html#a42bf95c911ca591042d1a4eada2ae8e2", null ],
    [ "probabilityMap", "d4/dec/classG4FluoData.html#a4ebf26c8d1abc571734e158883d917af", null ]
];