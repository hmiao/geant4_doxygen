var structG4UrbanMscModel_1_1mscData =
[
    [ "coeffc1", "d4/d10/structG4UrbanMscModel_1_1mscData.html#adb11367ab18ec1c533a50c3b00b3fe67", null ],
    [ "coeffc2", "d4/d10/structG4UrbanMscModel_1_1mscData.html#ab7a63a447519189d17a5caa681ae7130", null ],
    [ "coeffc3", "d4/d10/structG4UrbanMscModel_1_1mscData.html#abd24b053d9d7d8d6119a1758d03f1687", null ],
    [ "coeffc4", "d4/d10/structG4UrbanMscModel_1_1mscData.html#a66cb253315a434c276c6c1f172455c2a", null ],
    [ "coeffth1", "d4/d10/structG4UrbanMscModel_1_1mscData.html#a4841dfcaf2b88b5c5dd05d6d34b36f50", null ],
    [ "coeffth2", "d4/d10/structG4UrbanMscModel_1_1mscData.html#a7ad90af9092e8b8118632db84890065b", null ],
    [ "doverra", "d4/d10/structG4UrbanMscModel_1_1mscData.html#a41daa71014704de2178e941c6cae9834", null ],
    [ "doverrb", "d4/d10/structG4UrbanMscModel_1_1mscData.html#ad7e2c373bf8085929f93788a9c0e400a", null ],
    [ "ecut", "d4/d10/structG4UrbanMscModel_1_1mscData.html#ae1ce94175ba24a1699847a6ad90ef4e2", null ],
    [ "sqrtZ", "d4/d10/structG4UrbanMscModel_1_1mscData.html#afdf2b7c49056c18105774f2b91db4612", null ],
    [ "stepmina", "d4/d10/structG4UrbanMscModel_1_1mscData.html#afffb2147266ee3324fcc38345d4cd915", null ],
    [ "stepminb", "d4/d10/structG4UrbanMscModel_1_1mscData.html#a4ae89fdeccca4ebb6e7e83a5968a7b54", null ],
    [ "Z23", "d4/d10/structG4UrbanMscModel_1_1mscData.html#a389ca7371ec0c473afec26abbfa26bbf", null ],
    [ "Zeff", "d4/d10/structG4UrbanMscModel_1_1mscData.html#a7bd7c7d992e9211de795b21813b9a482", null ]
];