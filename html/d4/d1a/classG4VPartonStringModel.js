var classG4VPartonStringModel =
[
    [ "G4VPartonStringModel", "d4/d1a/classG4VPartonStringModel.html#a3ac9a920bb2b6fefdfbd51544d63719b", null ],
    [ "~G4VPartonStringModel", "d4/d1a/classG4VPartonStringModel.html#ac4e10e94debf3ac83418498eccae20ab", null ],
    [ "G4VPartonStringModel", "d4/d1a/classG4VPartonStringModel.html#a19b3e16ad33b56de2e32a308c326cad6", null ],
    [ "EnergyAndMomentumCorrector", "d4/d1a/classG4VPartonStringModel.html#ad780b32bf2f16658e65c1b39bd7ca43b", null ],
    [ "GetProjectileNucleus", "d4/d1a/classG4VPartonStringModel.html#a9ef9d033380723483ca2cfd2301d542e", null ],
    [ "GetStrings", "d4/d1a/classG4VPartonStringModel.html#a9d192b664a55244e2d5e7eee8674c7ee", null ],
    [ "Init", "d4/d1a/classG4VPartonStringModel.html#a6dc00a5897f5c714baa1c3ff9ee53093", null ],
    [ "ModelDescription", "d4/d1a/classG4VPartonStringModel.html#aabe194077f559472732cc0e80209bda1", null ],
    [ "operator!=", "d4/d1a/classG4VPartonStringModel.html#a71ee78f4bd2c9f8ea3f57efbd2e84f39", null ],
    [ "operator=", "d4/d1a/classG4VPartonStringModel.html#a078c5540ebdc39673a06cf9dae31ac9d", null ],
    [ "operator==", "d4/d1a/classG4VPartonStringModel.html#af517a4badc63ffa5aa7efb2ba8e525e0", null ],
    [ "Scatter", "d4/d1a/classG4VPartonStringModel.html#acd6e6e5c5afc7aa536cf5dee17573e99", null ],
    [ "SetFragmentationModel", "d4/d1a/classG4VPartonStringModel.html#a2fd1f6309e6312916db68e6574040b5d", null ],
    [ "stringFragmentationModel", "d4/d1a/classG4VPartonStringModel.html#a682a213dbaa14b6795ddf4edb01d59a3", null ]
];