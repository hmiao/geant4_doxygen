var namespaceDNA_1_1Penetration =
[
    [ "Kreipl2009", "dd/d72/structDNA_1_1Penetration_1_1Kreipl2009.html", "dd/d72/structDNA_1_1Penetration_1_1Kreipl2009" ],
    [ "Meesungnoen2002", "d9/d41/structDNA_1_1Penetration_1_1Meesungnoen2002.html", "d9/d41/structDNA_1_1Penetration_1_1Meesungnoen2002" ],
    [ "Meesungnoen2002_amorphous", "d5/d93/structDNA_1_1Penetration_1_1Meesungnoen2002__amorphous.html", "d5/d93/structDNA_1_1Penetration_1_1Meesungnoen2002__amorphous" ],
    [ "Ritchie1994", "d8/d3f/structDNA_1_1Penetration_1_1Ritchie1994.html", "d8/d3f/structDNA_1_1Penetration_1_1Ritchie1994" ],
    [ "Terrisol1990", "d2/dec/structDNA_1_1Penetration_1_1Terrisol1990.html", "d2/dec/structDNA_1_1Penetration_1_1Terrisol1990" ],
    [ "GetGaussianPenetrationFromRmean3D", "d4/dbb/namespaceDNA_1_1Penetration.html#af4f15f66da75af9565d5141b86d967ac", null ]
];