var classG4ParticleHPData =
[
    [ "G4ParticleHPData", "d4/d68/classG4ParticleHPData.html#af6b0f7d92fd3715298276222b33db111", null ],
    [ "~G4ParticleHPData", "d4/d68/classG4ParticleHPData.html#a742dbb54a2905c5fc432dbb0addf2568", null ],
    [ "addPhysicsVector", "d4/d68/classG4ParticleHPData.html#a192f1483837ad0c4d18b670bddcf4378", null ],
    [ "DoPhysicsVector", "d4/d68/classG4ParticleHPData.html#a570325ca818f17a99394bc41d3b7b4fe", null ],
    [ "Instance", "d4/d68/classG4ParticleHPData.html#a074420c234f19d5088af1b6bf3e71901", null ],
    [ "MakePhysicsVector", "d4/d68/classG4ParticleHPData.html#a24041bff525b098907408fb96189c927", null ],
    [ "MakePhysicsVector", "d4/d68/classG4ParticleHPData.html#ae992745599f125e44fc4fd5a0d73bca1", null ],
    [ "MakePhysicsVector", "d4/d68/classG4ParticleHPData.html#aeb2768075112e13d854bc02ff16e23ef", null ],
    [ "MakePhysicsVector", "d4/d68/classG4ParticleHPData.html#ad6a4a59e3adee2ea6af033bed56f1abd", null ],
    [ "numEle", "d4/d68/classG4ParticleHPData.html#abf19e84b4f91cc2498f052fc786f65e6", null ],
    [ "theData", "d4/d68/classG4ParticleHPData.html#a97d01a1e47b5773304714e071a69b9a3", null ],
    [ "theDataDirVariable", "d4/d68/classG4ParticleHPData.html#a8e544c3923a2d9d9e77b91b4ef06c44a", null ],
    [ "theProjectile", "d4/d68/classG4ParticleHPData.html#a6a5352fa286ced7435ce7f1d31c61552", null ]
];