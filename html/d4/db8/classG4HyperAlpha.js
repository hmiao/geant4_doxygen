var classG4HyperAlpha =
[
    [ "G4HyperAlpha", "d4/db8/classG4HyperAlpha.html#a3be04bc719caba85ed056067ff57b2fb", null ],
    [ "~G4HyperAlpha", "d4/db8/classG4HyperAlpha.html#a0f8a367879cdcbf7dc0a0cb596f4771c", null ],
    [ "Definition", "d4/db8/classG4HyperAlpha.html#adbcf54ad9cca3f89b7c88c4be879d0c3", null ],
    [ "HyperAlpha", "d4/db8/classG4HyperAlpha.html#a0c95df1ddb0f20c68e6ff5e6144a44ff", null ],
    [ "HyperAlphaDefinition", "d4/db8/classG4HyperAlpha.html#ad09dd7f34c74393408f3da7f6ccad5ba", null ],
    [ "theInstance", "d4/db8/classG4HyperAlpha.html#a1977e5e19b8f931446a24751e614775b", null ]
];