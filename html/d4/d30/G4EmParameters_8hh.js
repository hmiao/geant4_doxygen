var G4EmParameters_8hh =
[
    [ "G4EmParameters", "dc/d54/classG4EmParameters.html", "dc/d54/classG4EmParameters" ],
    [ "G4eSingleScatteringType", "d4/d30/G4EmParameters_8hh.html#a69916ef6613d39271e56ae37ffee3a08", [
      [ "fWVI", "d4/d30/G4EmParameters_8hh.html#a69916ef6613d39271e56ae37ffee3a08a0a684fe2eab199a10819c6cbb6bbbd8d", null ],
      [ "fMott", "d4/d30/G4EmParameters_8hh.html#a69916ef6613d39271e56ae37ffee3a08a2bcfb1071b3fddb06dd5701d5e269b7e", null ],
      [ "fDPWA", "d4/d30/G4EmParameters_8hh.html#a69916ef6613d39271e56ae37ffee3a08ad54de561bf3aacc729352ae6def9304b", null ]
    ] ]
];