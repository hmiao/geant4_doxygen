var classG4ANSTOecpssrLixsModel =
[
    [ "G4ANSTOecpssrLixsModel", "d4/d0b/classG4ANSTOecpssrLixsModel.html#af906a7729586254580508d8000ca69d1", null ],
    [ "~G4ANSTOecpssrLixsModel", "d4/d0b/classG4ANSTOecpssrLixsModel.html#a88704f251c3482ab1a16f7bda2ce66c3", null ],
    [ "G4ANSTOecpssrLixsModel", "d4/d0b/classG4ANSTOecpssrLixsModel.html#af8f02639352f77d24c84cca01671f67a", null ],
    [ "CalculateL1CrossSection", "d4/d0b/classG4ANSTOecpssrLixsModel.html#a4b95c21c9cc0075584a2f1812e43fa15", null ],
    [ "CalculateL2CrossSection", "d4/d0b/classG4ANSTOecpssrLixsModel.html#abb5a9ba75525600cdd29083c69b75782", null ],
    [ "CalculateL3CrossSection", "d4/d0b/classG4ANSTOecpssrLixsModel.html#a663199358037b44b957f3fdbfb93e966", null ],
    [ "operator=", "d4/d0b/classG4ANSTOecpssrLixsModel.html#af94c8d76ba94cfc515f678eca7e2eb83", null ],
    [ "alphaL1DataSetMap", "d4/d0b/classG4ANSTOecpssrLixsModel.html#a7996db281a276729a4fa061d94999f3f", null ],
    [ "alphaL2DataSetMap", "d4/d0b/classG4ANSTOecpssrLixsModel.html#adb602d5b43f2292790099ba985f07d23", null ],
    [ "alphaL3DataSetMap", "d4/d0b/classG4ANSTOecpssrLixsModel.html#a134af6f550ef3da42d876653a5015001", null ],
    [ "carbonL1DataSetMap", "d4/d0b/classG4ANSTOecpssrLixsModel.html#a424415a2cad2df027bd53cb073a0d829", null ],
    [ "carbonL2DataSetMap", "d4/d0b/classG4ANSTOecpssrLixsModel.html#a34487f32f685bdcfdd6c32478b2b0cfa", null ],
    [ "carbonL3DataSetMap", "d4/d0b/classG4ANSTOecpssrLixsModel.html#a20a27f7c0cd8640487db0b84730bd7c2", null ],
    [ "interpolation", "d4/d0b/classG4ANSTOecpssrLixsModel.html#aaef273529d52a02344bfd10fa68d4766", null ],
    [ "protonL1DataSetMap", "d4/d0b/classG4ANSTOecpssrLixsModel.html#af1bc7a37f66a3e09c866dfca1c583c57", null ],
    [ "protonL2DataSetMap", "d4/d0b/classG4ANSTOecpssrLixsModel.html#a6d869440d28e071cabc8a7da5a6e750b", null ],
    [ "protonL3DataSetMap", "d4/d0b/classG4ANSTOecpssrLixsModel.html#aa33be69b66e9099b45aa3db7d77bc9ab", null ]
];