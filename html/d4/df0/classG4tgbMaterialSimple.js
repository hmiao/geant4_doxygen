var classG4tgbMaterialSimple =
[
    [ "G4tgbMaterialSimple", "d4/df0/classG4tgbMaterialSimple.html#a3f4d947e3f5a9427e9e944cc7dd1f6ad", null ],
    [ "~G4tgbMaterialSimple", "d4/df0/classG4tgbMaterialSimple.html#a9d511f116712d4fc0ed3060119f02ff1", null ],
    [ "G4tgbMaterialSimple", "d4/df0/classG4tgbMaterialSimple.html#a9fd8af0a8309bc690d6b75eb42ddf3d7", null ],
    [ "BuildG4Material", "d4/df0/classG4tgbMaterialSimple.html#a5345e8bd6a6c47aafc6be98ced0f4b4e", null ],
    [ "GetA", "d4/df0/classG4tgbMaterialSimple.html#a865cc1f90648959c547446ae952b0ac9", null ],
    [ "GetZ", "d4/df0/classG4tgbMaterialSimple.html#a31b9ae6fd7dccf466736ca70f481ab67", null ],
    [ "operator<<", "d4/df0/classG4tgbMaterialSimple.html#a60cb7b24c9ca6ed97b2ec6cabf5801ac", null ],
    [ "theA", "d4/df0/classG4tgbMaterialSimple.html#a3d5cd387c6da250b20b82e3a2d95ba79", null ],
    [ "theZ", "d4/df0/classG4tgbMaterialSimple.html#a29f851fee4bbf9fbc38f99253b1228a5", null ]
];