var classG4OpWLS =
[
    [ "G4OpWLS", "d4/dee/classG4OpWLS.html#acd2900f005ca2b110eff724990f0ce17", null ],
    [ "~G4OpWLS", "d4/dee/classG4OpWLS.html#ac480ccf44b4e3abfe312d9cca7340c3b", null ],
    [ "G4OpWLS", "d4/dee/classG4OpWLS.html#a586d40af8d59cc45cd56c13c981cf7d2", null ],
    [ "BuildPhysicsTable", "d4/dee/classG4OpWLS.html#a09ea6b5c4f138acd1ebf2cd64e320ffb", null ],
    [ "DumpPhysicsTable", "d4/dee/classG4OpWLS.html#a46827a47f69214085252849c302ab572", null ],
    [ "GetIntegralTable", "d4/dee/classG4OpWLS.html#a1f1c9099512ea7b3344d0aafdd701d83", null ],
    [ "GetMeanFreePath", "d4/dee/classG4OpWLS.html#a4beb3ae7216e3a59a57610d773d2e2ed", null ],
    [ "Initialise", "d4/dee/classG4OpWLS.html#a0e4b0a28eb9669cdd91dfce8335acdfc", null ],
    [ "IsApplicable", "d4/dee/classG4OpWLS.html#a11bd5339673903b45268baf670cf4c3c", null ],
    [ "operator=", "d4/dee/classG4OpWLS.html#a132c3016a6007370c3558c585056b19f", null ],
    [ "PostStepDoIt", "d4/dee/classG4OpWLS.html#aa901a42c3aaf37f3a2d68e4500885ae5", null ],
    [ "PreparePhysicsTable", "d4/dee/classG4OpWLS.html#aef41ca5a36e94400a1ddb00f6367d6ee", null ],
    [ "SetVerboseLevel", "d4/dee/classG4OpWLS.html#a2474457a40f18591cb1e1341e90a725c", null ],
    [ "UseTimeProfile", "d4/dee/classG4OpWLS.html#adc4cf1d3fa6187112819afea14933e21", null ],
    [ "idx_wls", "d4/dee/classG4OpWLS.html#a441211095af921550155cd6b977c2b85", null ],
    [ "theIntegralTable", "d4/dee/classG4OpWLS.html#a8a050a738f3a9b8befef26fe9e4e9ce6", null ],
    [ "WLSTimeGeneratorProfile", "d4/dee/classG4OpWLS.html#ac83f48ca5f8198872fc181e5edf365ad", null ]
];