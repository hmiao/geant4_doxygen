var classG4XDeltaDeltaTable =
[
    [ "G4XDeltaDeltaTable", "d4/dee/classG4XDeltaDeltaTable.html#ad8d215a584095e4ac6a9e8d685d3f464", null ],
    [ "~G4XDeltaDeltaTable", "d4/dee/classG4XDeltaDeltaTable.html#aff888cf45a5173ee771d549e33e82c64", null ],
    [ "G4XDeltaDeltaTable", "d4/dee/classG4XDeltaDeltaTable.html#a6f050f511b7af609588dffbcc11468a9", null ],
    [ "CrossSectionTable", "d4/dee/classG4XDeltaDeltaTable.html#a520c22c2c953c593eef49d510d4a7d2c", null ],
    [ "operator!=", "d4/dee/classG4XDeltaDeltaTable.html#a4e7ee9870e2c3dcf9912a0125809e8ae", null ],
    [ "operator=", "d4/dee/classG4XDeltaDeltaTable.html#a6bc9e395e62a1e7ab326f3feb907bcdc", null ],
    [ "operator==", "d4/dee/classG4XDeltaDeltaTable.html#ab88765752f079c81abb2fe1352322f29", null ],
    [ "energyTable", "d4/dee/classG4XDeltaDeltaTable.html#a12887f167f6abcf49efd8ca2b1d8dfd8", null ],
    [ "sigmaDD1232", "d4/dee/classG4XDeltaDeltaTable.html#aa165f733d56050c624653cbb8f441cda", null ],
    [ "size", "d4/dee/classG4XDeltaDeltaTable.html#a14424aacff2c6fd4b32994539e189b7c", null ],
    [ "sizeDeltaDelta", "d4/dee/classG4XDeltaDeltaTable.html#a771434eb0ab186f27c782ec83fb74710", null ]
];