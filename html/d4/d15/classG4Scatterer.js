var classG4Scatterer =
[
    [ "Register", "df/d8a/structG4Scatterer_1_1Register.html", "df/d8a/structG4Scatterer_1_1Register" ],
    [ "G4Scatterer", "d4/d15/classG4Scatterer.html#a5f490a45da863de1b761ba188ab53e31", null ],
    [ "~G4Scatterer", "d4/d15/classG4Scatterer.html#af88d73606eab00d5f2c2038a7836b7eb", null ],
    [ "FindCollision", "d4/d15/classG4Scatterer.html#a34616ec2b1e7064bf11902b2845d73ad", null ],
    [ "GetCollisions", "d4/d15/classG4Scatterer.html#a97bf1db0f4761190167eccddfdecd342", null ],
    [ "GetCrossSection", "d4/d15/classG4Scatterer.html#a4a70eae0846dc0eeb5b4be2bb4be8d7c", null ],
    [ "GetFinalState", "d4/d15/classG4Scatterer.html#ab92e83d5ddc8d4162ed11869e36c52ee", null ],
    [ "GetTimeToInteraction", "d4/d15/classG4Scatterer.html#a9a1b4a9a8449c49c1bb35bb4345fbfd8", null ],
    [ "Scatter", "d4/d15/classG4Scatterer.html#acc358341d2ebd0685db04a85ae74d5b3", null ],
    [ "collisions", "d4/d15/classG4Scatterer.html#aea32b4be4962ff366185c60a2b1908f1", null ],
    [ "theCollisions", "d4/d15/classG4Scatterer.html#a0a2cfa737e0fcc772a2445ba217de02c", null ]
];