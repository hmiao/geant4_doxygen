var classG4BinaryTritonBuilder =
[
    [ "G4BinaryTritonBuilder", "d4/df2/classG4BinaryTritonBuilder.html#a210cb595c5ecbb7e414f4f2567b60f69", null ],
    [ "~G4BinaryTritonBuilder", "d4/df2/classG4BinaryTritonBuilder.html#a5b82bc94f0dcf1498206cc944ec2c84b", null ],
    [ "Build", "d4/df2/classG4BinaryTritonBuilder.html#a26c88daed9b25bfeefccbc7ce15b117b", null ],
    [ "Build", "d4/df2/classG4BinaryTritonBuilder.html#a74b7aaeebb66754c5bdf45955a1a842d", null ],
    [ "Build", "d4/df2/classG4BinaryTritonBuilder.html#a2874e4a19ccc1e14c6c7d55ec910a7ea", null ],
    [ "Build", "d4/df2/classG4BinaryTritonBuilder.html#abd20b8dccb8117b1668ca347294cc96d", null ],
    [ "SetMaxEnergy", "d4/df2/classG4BinaryTritonBuilder.html#a3e123618df5e291d6cde5f78053ed317", null ],
    [ "SetMinEnergy", "d4/df2/classG4BinaryTritonBuilder.html#a8eb06aac5d8e4fa73f60de2e4315f869", null ],
    [ "theMax", "d4/df2/classG4BinaryTritonBuilder.html#af762359f7595b822fc94a357613e36b8", null ],
    [ "theMin", "d4/df2/classG4BinaryTritonBuilder.html#a636fc6a0c46515f29880a480a9ca6f52", null ],
    [ "theModel", "d4/df2/classG4BinaryTritonBuilder.html#a9622ad75cee8a2ab540becdbc9836fb4", null ]
];