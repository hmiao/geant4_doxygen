var G4RootNtupleFileManager_8hh =
[
    [ "G4RootNtupleFileManager", "db/d71/classG4RootNtupleFileManager.html", "db/d71/classG4RootNtupleFileManager" ],
    [ "G4NtupleMergeMode", "d4/d4f/G4RootNtupleFileManager_8hh.html#a38b3f675019a8795dcbbbe0e7e30e01c", [
      [ "kNone", "d4/d4f/G4RootNtupleFileManager_8hh.html#a38b3f675019a8795dcbbbe0e7e30e01ca35c3ace1970663a16e5c65baa5941b13", null ],
      [ "kMain", "d4/d4f/G4RootNtupleFileManager_8hh.html#a38b3f675019a8795dcbbbe0e7e30e01ca04ed0d17680a76a1a906aed4ff47cd07", null ],
      [ "kSlave", "d4/d4f/G4RootNtupleFileManager_8hh.html#a38b3f675019a8795dcbbbe0e7e30e01ca711876d1003646469de6eb5a7811927b", null ]
    ] ]
];