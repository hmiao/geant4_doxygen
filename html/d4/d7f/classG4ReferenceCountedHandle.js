var classG4ReferenceCountedHandle =
[
    [ "G4ReferenceCountedHandle", "d4/d7f/classG4ReferenceCountedHandle.html#a87611f9fd8e250b9409d399278492d71", null ],
    [ "G4ReferenceCountedHandle", "d4/d7f/classG4ReferenceCountedHandle.html#a9878bfee2916d87b441122c188075f3b", null ],
    [ "~G4ReferenceCountedHandle", "d4/d7f/classG4ReferenceCountedHandle.html#ac3795d6e9263bba810996a7402a9cd47", null ],
    [ "Count", "d4/d7f/classG4ReferenceCountedHandle.html#a3eb6d88b1de768be5cec9952cb3046ab", null ],
    [ "operator bool", "d4/d7f/classG4ReferenceCountedHandle.html#a697d42bb550324adaeb9255da3c9448d", null ],
    [ "operator delete", "d4/d7f/classG4ReferenceCountedHandle.html#a8acf422ed332ca478c0703384749cc75", null ],
    [ "operator new", "d4/d7f/classG4ReferenceCountedHandle.html#ab9a66efcf2431c62ff2d119065efbd70", null ],
    [ "operator!", "d4/d7f/classG4ReferenceCountedHandle.html#ae3551cdff17ba41b0d2265a039cb2a08", null ],
    [ "operator()", "d4/d7f/classG4ReferenceCountedHandle.html#afd0d7e9000be52be32b373c026d50fde", null ],
    [ "operator->", "d4/d7f/classG4ReferenceCountedHandle.html#a99ea5606062b23019c516cbd343c70d6", null ],
    [ "operator=", "d4/d7f/classG4ReferenceCountedHandle.html#a6335045cdf6a039afca94aaa66b6a64d", null ],
    [ "operator=", "d4/d7f/classG4ReferenceCountedHandle.html#a0cbd7efb2deb3e99f12410b36fe0d17f", null ],
    [ "fObj", "d4/d7f/classG4ReferenceCountedHandle.html#aec66d0648261af717248f1582da9c931", null ]
];