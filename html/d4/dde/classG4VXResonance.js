var classG4VXResonance =
[
    [ "G4VXResonance", "d4/dde/classG4VXResonance.html#a04b40131b6f97e33f0d4473c35813b44", null ],
    [ "~G4VXResonance", "d4/dde/classG4VXResonance.html#ab7fe1997bdb24c0a02b41a271552dba5", null ],
    [ "G4VXResonance", "d4/dde/classG4VXResonance.html#a3a69b956cdc15b44dc85cd84a4927998", null ],
    [ "DegeneracyFactor", "d4/dde/classG4VXResonance.html#a5a2531b2f08c1a24965066052cb2d0c3", null ],
    [ "DetailedBalance", "d4/dde/classG4VXResonance.html#a3c48160f0f4d057ee955740bbfa78642", null ],
    [ "IsospinCorrection", "d4/dde/classG4VXResonance.html#adc2c2c2244e7d09f3e95457c38206596", null ],
    [ "operator!=", "d4/dde/classG4VXResonance.html#a038bca5ae2c176036f699dd39cc1491b", null ],
    [ "operator=", "d4/dde/classG4VXResonance.html#a41944a7f495131734fb80dd8c0e34989", null ],
    [ "operator==", "d4/dde/classG4VXResonance.html#af22bd6530deb7c7d68da39123240bfbc", null ],
    [ "clebsch", "d4/dde/classG4VXResonance.html#a26d0d0d9119e196d397a4a32b95239bd", null ]
];