var classG4VIonDEDXTable =
[
    [ "G4VIonDEDXTable", "d4/d83/classG4VIonDEDXTable.html#a91047fc1b0c5f213d4c821ea7f0ddbc4", null ],
    [ "~G4VIonDEDXTable", "d4/d83/classG4VIonDEDXTable.html#a18013ec4a13feaf7800f6e6247200ffc", null ],
    [ "G4VIonDEDXTable", "d4/d83/classG4VIonDEDXTable.html#af6da17a57e124879f390841cbae8e222", null ],
    [ "BuildPhysicsVector", "d4/d83/classG4VIonDEDXTable.html#a3e31f6657e958350736d024cce8b7c2e", null ],
    [ "BuildPhysicsVector", "d4/d83/classG4VIonDEDXTable.html#ae36f8db32bb770785de7b9f96c0ded78", null ],
    [ "GetPhysicsVector", "d4/d83/classG4VIonDEDXTable.html#ad33f90d0de01a3d94b0bb284902a1956", null ],
    [ "GetPhysicsVector", "d4/d83/classG4VIonDEDXTable.html#af78c21913d4294601e246e36de72928d", null ],
    [ "IsApplicable", "d4/d83/classG4VIonDEDXTable.html#a6254ed37049b1364821725dc8ab7551c", null ],
    [ "IsApplicable", "d4/d83/classG4VIonDEDXTable.html#a25e19b9a51884a7657ac14db15c8a539", null ],
    [ "operator=", "d4/d83/classG4VIonDEDXTable.html#a453f8b96ad7e7d709563e166b33fb9b9", null ]
];