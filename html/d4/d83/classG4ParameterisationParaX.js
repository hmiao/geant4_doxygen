var classG4ParameterisationParaX =
[
    [ "G4ParameterisationParaX", "d4/d83/classG4ParameterisationParaX.html#a39e99972be067358e9c1bf475ad4e231", null ],
    [ "~G4ParameterisationParaX", "d4/d83/classG4ParameterisationParaX.html#ab4d7bfde03e6b2f67323df37edc2e013", null ],
    [ "ComputeDimensions", "d4/d83/classG4ParameterisationParaX.html#a8981d9e301abc779957d3ded849e481b", null ],
    [ "ComputeDimensions", "d4/d83/classG4ParameterisationParaX.html#ab777a1e9db75dfc648caa8c70848e5c2", null ],
    [ "ComputeDimensions", "d4/d83/classG4ParameterisationParaX.html#a1195c7ab6366acddcc8670c315c45539", null ],
    [ "ComputeDimensions", "d4/d83/classG4ParameterisationParaX.html#aedd8c7cffd0d3673e49bfb66203e02e3", null ],
    [ "ComputeDimensions", "d4/d83/classG4ParameterisationParaX.html#a00e2831b73e78974e945456aba84728f", null ],
    [ "ComputeDimensions", "d4/d83/classG4ParameterisationParaX.html#a345c2a34b39ca8fc4a67b3be2494a04d", null ],
    [ "ComputeDimensions", "d4/d83/classG4ParameterisationParaX.html#af41b1573c11351d2f9a6119bc0764770", null ],
    [ "ComputeDimensions", "d4/d83/classG4ParameterisationParaX.html#a2bdc60f5099d881943328e3de9663f26", null ],
    [ "ComputeDimensions", "d4/d83/classG4ParameterisationParaX.html#aa092260ab510a88b99638950f5e896ff", null ],
    [ "ComputeDimensions", "d4/d83/classG4ParameterisationParaX.html#a2a367ec532a7891af80a9a1fe779f296", null ],
    [ "ComputeDimensions", "d4/d83/classG4ParameterisationParaX.html#a2eb1a2e4f1839928ea2467b90c59474b", null ],
    [ "ComputeDimensions", "d4/d83/classG4ParameterisationParaX.html#a1403459824b3ed793ec55ff34ba128b3", null ],
    [ "ComputeDimensions", "d4/d83/classG4ParameterisationParaX.html#affab501ce82f0429bdaefcda42a0205d", null ],
    [ "ComputeTransformation", "d4/d83/classG4ParameterisationParaX.html#a259a7501fbc78248c562a94cd11d5e35", null ],
    [ "GetMaxParameter", "d4/d83/classG4ParameterisationParaX.html#acedd50a7bbfb99a9d8ccdd53141e51c5", null ]
];