var classG4LowEIonFragmentation =
[
    [ "G4LowEIonFragmentation", "d4/d56/classG4LowEIonFragmentation.html#a6cfee84eeb7134bd5c924a3584942e4d", null ],
    [ "~G4LowEIonFragmentation", "d4/d56/classG4LowEIonFragmentation.html#a853dd003c4ed45084b936054fc712208", null ],
    [ "G4LowEIonFragmentation", "d4/d56/classG4LowEIonFragmentation.html#a5dd733caae12d373295961e2c1fe2ffc", null ],
    [ "ApplyYourself", "d4/d56/classG4LowEIonFragmentation.html#a859e7dbd68cf522ee247b08c7da82b45", null ],
    [ "GetCrossSection", "d4/d56/classG4LowEIonFragmentation.html#ad90a8940615da32f80db15c159e58eb6", null ],
    [ "operator!=", "d4/d56/classG4LowEIonFragmentation.html#a04e4f73ad3c06c0418b430775a8a0fff", null ],
    [ "operator=", "d4/d56/classG4LowEIonFragmentation.html#ab13227d39e1e40d39d7f2ed2a864ae76", null ],
    [ "operator==", "d4/d56/classG4LowEIonFragmentation.html#a1939200ff49f0b301a6953ee3ddd554e", null ],
    [ "area", "d4/d56/classG4LowEIonFragmentation.html#a21527b1c623fe0f7caa9c677677b21cc", null ],
    [ "hits", "d4/d56/classG4LowEIonFragmentation.html#a4b3019a2f635648eaed5f8be0943eebd", null ],
    [ "proton", "d4/d56/classG4LowEIonFragmentation.html#ac094aaca31e650e8884e416f0fe3d6de", null ],
    [ "secID", "d4/d56/classG4LowEIonFragmentation.html#a55c6a3391f0ed07c2bae6eeb3bb631ae", null ],
    [ "theHandler", "d4/d56/classG4LowEIonFragmentation.html#a4cb0282a9c69c12f2e0baf9403c948c5", null ],
    [ "theModel", "d4/d56/classG4LowEIonFragmentation.html#a0c4b191f9b0b5d2995f336dbe7ace389", null ],
    [ "theResult", "d4/d56/classG4LowEIonFragmentation.html#aa5e695c10442ac17f0913005650954ae", null ],
    [ "totalTries", "d4/d56/classG4LowEIonFragmentation.html#a5b87d3d2a04f563a1cdd924f81de6342", null ]
];