var classG4HadronHElasticPhysics =
[
    [ "G4HadronHElasticPhysics", "d4/d65/classG4HadronHElasticPhysics.html#adda5b1521b3b5efd80cd615425a7a17b", null ],
    [ "~G4HadronHElasticPhysics", "d4/d65/classG4HadronHElasticPhysics.html#a9b1f2c5b7dc22558c23629fb05e98e6d", null ],
    [ "G4HadronHElasticPhysics", "d4/d65/classG4HadronHElasticPhysics.html#a636734beb5fa413c7e2059ca50242210", null ],
    [ "ConstructProcess", "d4/d65/classG4HadronHElasticPhysics.html#a1529da4e48697052f4b9ca084ace7f99", null ],
    [ "operator=", "d4/d65/classG4HadronHElasticPhysics.html#ade84f00b8bfc1e406d568646f9adadc4", null ],
    [ "SetDiffraction", "d4/d65/classG4HadronHElasticPhysics.html#ae87eb90b3dbe1d209bb5035864f8a0c0", null ],
    [ "fDiffraction", "d4/d65/classG4HadronHElasticPhysics.html#a7ec28ec7b0a60325644c1967a376adb4", null ]
];