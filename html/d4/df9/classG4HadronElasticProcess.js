var classG4HadronElasticProcess =
[
    [ "G4HadronElasticProcess", "d4/df9/classG4HadronElasticProcess.html#a9c1a8a2e104ea0706e6af18e378eb6bd", null ],
    [ "~G4HadronElasticProcess", "d4/df9/classG4HadronElasticProcess.html#a5863965c0247b5247c3985846512d97e", null ],
    [ "G4HadronElasticProcess", "d4/df9/classG4HadronElasticProcess.html#a7b21223966f505ac77b8264537e46973", null ],
    [ "operator=", "d4/df9/classG4HadronElasticProcess.html#aa312dbd11b1fade5a7d7f035d10e6361", null ],
    [ "PostStepDoIt", "d4/df9/classG4HadronElasticProcess.html#a9c582161b72c6b21b0288eb1eb07bd18", null ],
    [ "PrintWarning", "d4/df9/classG4HadronElasticProcess.html#af972e255d4745ae3bfe5d17ecdbb5b68", null ],
    [ "ProcessDescription", "d4/df9/classG4HadronElasticProcess.html#aaa97af74efac4062ce55d7239fb48426", null ],
    [ "SetDiffraction", "d4/df9/classG4HadronElasticProcess.html#a7a5a58564f93b76ee85d6a698a786dae", null ],
    [ "SetLowestEnergy", "d4/df9/classG4HadronElasticProcess.html#abfafa195db097db12eef5126097d7459", null ],
    [ "SetLowestEnergyNeutron", "d4/df9/classG4HadronElasticProcess.html#af2aedbcb6570f747c468642b282dbc82", null ],
    [ "fDiffraction", "d4/df9/classG4HadronElasticProcess.html#a9711cd51ec208b19afa04da91952185f", null ],
    [ "fDiffractionRatio", "d4/df9/classG4HadronElasticProcess.html#a13675d2e53b5a2284d0b16da7860d869", null ]
];