var classG4hIonEffChargeSquare =
[
    [ "G4hIonEffChargeSquare", "d4/ddb/classG4hIonEffChargeSquare.html#a6d26f5a4ac0b89e65ee7bd3765f74ae4", null ],
    [ "~G4hIonEffChargeSquare", "d4/ddb/classG4hIonEffChargeSquare.html#a0827b72b988fb01a0f6f18b81564b1eb", null ],
    [ "HighEnergyLimit", "d4/ddb/classG4hIonEffChargeSquare.html#a288b8d1dce34148c63125f49b7498467", null ],
    [ "HighEnergyLimit", "d4/ddb/classG4hIonEffChargeSquare.html#aa8a3b6cd047b241273a87a8f484f260d", null ],
    [ "IonEffChargeSquare", "d4/ddb/classG4hIonEffChargeSquare.html#a6090b4b489fccc2d71f6787fb83bca76", null ],
    [ "IsInCharge", "d4/ddb/classG4hIonEffChargeSquare.html#aa5d7d7fa364178f977a6e541c6fbeb71", null ],
    [ "IsInCharge", "d4/ddb/classG4hIonEffChargeSquare.html#a831a88245a328e891867e6aa0bf7f246", null ],
    [ "LowEnergyLimit", "d4/ddb/classG4hIonEffChargeSquare.html#a4372ce75fdc1c1c73517ecc5bc140527", null ],
    [ "LowEnergyLimit", "d4/ddb/classG4hIonEffChargeSquare.html#a7fbac7155d755af8568051df568cc30a", null ],
    [ "TheValue", "d4/ddb/classG4hIonEffChargeSquare.html#ad40d260ed16e264b372c8043b0af4222", null ],
    [ "TheValue", "d4/ddb/classG4hIonEffChargeSquare.html#a3e87882290b0c1f2bc0d0a3d2569fe57", null ],
    [ "theHeMassAMU", "d4/ddb/classG4hIonEffChargeSquare.html#a5443840fe3695182b7187717158a9395", null ]
];