var G4TaskSingletonDelegator_8hh =
[
    [ "G4Traits::TaskSingletonKey< T >", "da/d7d/structG4Traits_1_1TaskSingletonKey.html", "da/d7d/structG4Traits_1_1TaskSingletonKey" ],
    [ "G4TaskSingletonData< T >", "d9/da9/classG4TaskSingletonData.html", "d9/da9/classG4TaskSingletonData" ],
    [ "G4TaskSingletonEvaluator< T >", "d7/db1/structG4TaskSingletonEvaluator.html", "d7/db1/structG4TaskSingletonEvaluator" ],
    [ "G4TaskSingletonDelegator< T >", "dc/dd1/classG4TaskSingletonDelegator.html", "dc/dd1/classG4TaskSingletonDelegator" ]
];