var classG4VRFileManager =
[
    [ "G4VRFileManager", "d4/db4/classG4VRFileManager.html#abb81ab07ab880f56bc9dd1005183dc06", null ],
    [ "G4VRFileManager", "d4/db4/classG4VRFileManager.html#aa9c6cef7d2e434ea0eac57bbcf7d8354", null ],
    [ "~G4VRFileManager", "d4/db4/classG4VRFileManager.html#a5675e0f87b0f1d153a6936f5d3c498b0", null ],
    [ "CloseFiles", "d4/db4/classG4VRFileManager.html#a38d9f816461a9aced3cb9fe5d442ab3f", null ],
    [ "GetHnRFileManager", "d4/db4/classG4VRFileManager.html#ac074633118ce57093a4095a1b874f506", null ],
    [ "fH1RFileManager", "d4/db4/classG4VRFileManager.html#af937639ef2210b035d072515eac335af", null ],
    [ "fH2RFileManager", "d4/db4/classG4VRFileManager.html#a13c3df01bf7b0d5daae0354e7d58275e", null ],
    [ "fH3RFileManager", "d4/db4/classG4VRFileManager.html#aa4032fc2addac9949f777c8796242218", null ],
    [ "fkClass", "d4/db4/classG4VRFileManager.html#aedb946eed5686ba596507e7afc95c205", null ],
    [ "fP1RFileManager", "d4/db4/classG4VRFileManager.html#aea54548ff45fd50b759cbe3507eeab84", null ],
    [ "fP2RFileManager", "d4/db4/classG4VRFileManager.html#a59b3495a6cc3fae1052d823308436b25", null ]
];