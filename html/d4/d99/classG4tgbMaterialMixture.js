var classG4tgbMaterialMixture =
[
    [ "G4tgbMaterialMixture", "d4/d99/classG4tgbMaterialMixture.html#a028fb97223d00ac611c85ec309a0bb19", null ],
    [ "~G4tgbMaterialMixture", "d4/d99/classG4tgbMaterialMixture.html#a1e30d3dc1d77143ff9bd288c2157646b", null ],
    [ "GetComponent", "d4/d99/classG4tgbMaterialMixture.html#a8a547b432a4ce652978f367d9377b49f", null ],
    [ "GetFraction", "d4/d99/classG4tgbMaterialMixture.html#a13c5936fe2681db38851ab030e71e8cd", null ],
    [ "operator=", "d4/d99/classG4tgbMaterialMixture.html#a1429e403d690d5a1c64c122888ce62d0", null ],
    [ "TransformToFractionsByWeight", "d4/d99/classG4tgbMaterialMixture.html#ae8aa8edfc2e41b424d29ee3b8170499b", null ]
];