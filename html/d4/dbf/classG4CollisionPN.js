var classG4CollisionPN =
[
    [ "G4CollisionPN", "d4/dbf/classG4CollisionPN.html#a320bb68ffee5850f36f52d51acf9cc5e", null ],
    [ "~G4CollisionPN", "d4/dbf/classG4CollisionPN.html#a9a1d7ccacc2974038e6e1091bb9f313f", null ],
    [ "G4CollisionPN", "d4/dbf/classG4CollisionPN.html#ad9e61526afc486ba7a942f03181f0edf", null ],
    [ "GetAngularDistribution", "d4/dbf/classG4CollisionPN.html#a7d520364b648d19cad853b783b908f8a", null ],
    [ "GetCrossSectionSource", "d4/dbf/classG4CollisionPN.html#a0cad6aa1bd4453daa883aaed32ed6b61", null ],
    [ "GetListOfColliders", "d4/dbf/classG4CollisionPN.html#a162063ade03c5e57b68237d8f4fe0d83", null ],
    [ "GetName", "d4/dbf/classG4CollisionPN.html#a3b48d76302281f2297a9beb7a24bb29b", null ],
    [ "operator!=", "d4/dbf/classG4CollisionPN.html#a1d8a2408512beaa4f2f67e52b70e5ecb", null ],
    [ "operator=", "d4/dbf/classG4CollisionPN.html#a17c61d1380d09bffd106b9419accfdd2", null ],
    [ "operator==", "d4/dbf/classG4CollisionPN.html#ac0031db06c347ddd81dcac38aa5443d9", null ],
    [ "colliders1", "d4/dbf/classG4CollisionPN.html#a1a586d1d95784d165f26466987de7b15", null ],
    [ "colliders2", "d4/dbf/classG4CollisionPN.html#a841b1cfde7a855616f193ea119c4efc3", null ],
    [ "crossSectionSource", "d4/dbf/classG4CollisionPN.html#ad5b7b64dc9ba741c0b173af84535269c", null ]
];