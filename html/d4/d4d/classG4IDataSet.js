var classG4IDataSet =
[
    [ "G4IDataSet", "d4/d4d/classG4IDataSet.html#a6c50e75efefe4ccb1cb40284f0412572", null ],
    [ "~G4IDataSet", "d4/d4d/classG4IDataSet.html#a34dde01d72b45fbc303b543008647004", null ],
    [ "G4IDataSet", "d4/d4d/classG4IDataSet.html#a733eace12c506ef5a441fa72bad51c23", null ],
    [ "AddComponent", "d4/d4d/classG4IDataSet.html#ac5ca0f8f31b32ceffbf76cddb5181a3e", null ],
    [ "FindValue", "d4/d4d/classG4IDataSet.html#a8cebf9efb8ae9128f71b6177cf8e7e5b", null ],
    [ "GetComponent", "d4/d4d/classG4IDataSet.html#a10def2e3cea5918c477e684d96a56946", null ],
    [ "GetData", "d4/d4d/classG4IDataSet.html#adafff6a877827f03f4ec079406ee54d3", null ],
    [ "GetEnergies", "d4/d4d/classG4IDataSet.html#a43bb4688cee3317f99cd06a681b42981", null ],
    [ "LoadData", "d4/d4d/classG4IDataSet.html#a1d7fbc268f6d6e845bcf957aeb36eb1a", null ],
    [ "NumberOfComponents", "d4/d4d/classG4IDataSet.html#a2668d99d6cf7ca9bae1ff9d1591da265", null ],
    [ "operator=", "d4/d4d/classG4IDataSet.html#a6a575984004f14bc14d4820a49c36804", null ],
    [ "PrintData", "d4/d4d/classG4IDataSet.html#ac612845ad9522f4929ea37b98e333023", null ],
    [ "RandomSelect", "d4/d4d/classG4IDataSet.html#a3c6a5e7a4420bf43388469115bd8a674", null ],
    [ "SaveData", "d4/d4d/classG4IDataSet.html#ad634fc936b857ddea154ef02583472d8", null ],
    [ "SetEnergiesData", "d4/d4d/classG4IDataSet.html#a0da28c2d0dbc8b05be7d436a490bbf02", null ]
];