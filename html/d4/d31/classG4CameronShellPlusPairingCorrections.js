var classG4CameronShellPlusPairingCorrections =
[
    [ "G4CameronShellPlusPairingCorrections", "d4/d31/classG4CameronShellPlusPairingCorrections.html#a0eb6a90c04a51be8409635c9ffd0de3f", null ],
    [ "G4CameronShellPlusPairingCorrections", "d4/d31/classG4CameronShellPlusPairingCorrections.html#ae15186ac6b446d5128876ff75ebf9d85", null ],
    [ "GetPairingCorrection", "d4/d31/classG4CameronShellPlusPairingCorrections.html#a0736a1691d18dcb416cf512e8255fd11", null ],
    [ "operator=", "d4/d31/classG4CameronShellPlusPairingCorrections.html#ae6cb7dcb7d7150456be03a259b7039e3", null ],
    [ "SPNTable", "d4/d31/classG4CameronShellPlusPairingCorrections.html#a9b588edb4b7127c3c0d5db9c265b63ce", null ],
    [ "SPZTable", "d4/d31/classG4CameronShellPlusPairingCorrections.html#a96a2f1cc2dabe817462b7cb9ec428e41", null ]
];