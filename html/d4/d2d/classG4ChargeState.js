var classG4ChargeState =
[
    [ "G4ChargeState", "d4/d2d/classG4ChargeState.html#a42744ee0070ed0b01eec992f29aaba67", null ],
    [ "G4ChargeState", "d4/d2d/classG4ChargeState.html#a29976d53663e68ce594c32ec44332347", null ],
    [ "ElectricDipoleMoment", "d4/d2d/classG4ChargeState.html#a75ffeb16c2bb84a69c31da514a3afbf1", null ],
    [ "GetCharge", "d4/d2d/classG4ChargeState.html#a6e073446ac215429ae58d204d4b3b7e9", null ],
    [ "GetMagneticDipoleMoment", "d4/d2d/classG4ChargeState.html#a87a4c2ff4fba7dfe53299bac716fc059", null ],
    [ "GetPDGSpin", "d4/d2d/classG4ChargeState.html#aaa76b9e09675021f628d8ca33b0d6744", null ],
    [ "GetSpin", "d4/d2d/classG4ChargeState.html#aaed582c1d181e46ec7b1317a8826d3f8", null ],
    [ "MagneticCharge", "d4/d2d/classG4ChargeState.html#ab842961be2ac672e5f2a6fcdea4a3f19", null ],
    [ "operator=", "d4/d2d/classG4ChargeState.html#a02cdc0629e8a9f254ac67d9ec4f6ec28", null ],
    [ "SetCharge", "d4/d2d/classG4ChargeState.html#ac3b7da2e5715b83e8827c447fec09778", null ],
    [ "SetChargeDipoleMoments", "d4/d2d/classG4ChargeState.html#ab07ef2e6d6a961ccc9ff3c500b198296", null ],
    [ "SetChargeMdm", "d4/d2d/classG4ChargeState.html#a82c3dd4aebaf10995c8e1a431625f9ca", null ],
    [ "SetChargeMdmSpin", "d4/d2d/classG4ChargeState.html#a96f7023401cb0888efa1b6611e6a0940", null ],
    [ "SetChargesAndMoments", "d4/d2d/classG4ChargeState.html#a63144662f4f9c7ed006aaf45996b6cf5", null ],
    [ "SetChargeSpin", "d4/d2d/classG4ChargeState.html#a6662ae3a6e85050c44682a6d78f63294", null ],
    [ "SetChargeSpinMoments", "d4/d2d/classG4ChargeState.html#a54abe78bf2d55d51565111e575ef5abf", null ],
    [ "SetElectricDipoleMoment", "d4/d2d/classG4ChargeState.html#a83503969a06942649d59912ebabd381a", null ],
    [ "SetMagneticCharge", "d4/d2d/classG4ChargeState.html#a3f4cee4723db78b65492a531248cba6c", null ],
    [ "SetMagneticDipoleMoment", "d4/d2d/classG4ChargeState.html#af407e1aaaa087e49e58c7c81a4d712d7", null ],
    [ "SetPDGSpin", "d4/d2d/classG4ChargeState.html#a7484a2238dbc34647bd466dbbb9d0fc3", null ],
    [ "SetSpin", "d4/d2d/classG4ChargeState.html#a4c5f40c339232db9d8131e3272daa748", null ],
    [ "fCharge", "d4/d2d/classG4ChargeState.html#aae8e3836ab9a5a53cc146f82c84fc115", null ],
    [ "fElec_dipole", "d4/d2d/classG4ChargeState.html#a38b47b9933d960e9b503b6f15dea44df", null ],
    [ "fMagn_dipole", "d4/d2d/classG4ChargeState.html#a8eee9a776c84ca02f915467792189943", null ],
    [ "fMagneticCharge", "d4/d2d/classG4ChargeState.html#a2e4987e18b88f5f64286eadd39abd53f", null ],
    [ "fSpin", "d4/d2d/classG4ChargeState.html#a9dfabbe0a426defaaeb0554ea526db4c", null ]
];