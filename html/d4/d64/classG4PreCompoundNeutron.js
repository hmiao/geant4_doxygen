var classG4PreCompoundNeutron =
[
    [ "G4PreCompoundNeutron", "d4/d64/classG4PreCompoundNeutron.html#a26b47a736ab2a90cc3b41466c0b0596b", null ],
    [ "~G4PreCompoundNeutron", "d4/d64/classG4PreCompoundNeutron.html#a2a08153bd5e181c0f398f799bff81b30", null ],
    [ "G4PreCompoundNeutron", "d4/d64/classG4PreCompoundNeutron.html#a04225305817d4b9850b59a8d635d4ea4", null ],
    [ "GetAlpha", "d4/d64/classG4PreCompoundNeutron.html#abfb0ef4384361434062626ed9323b5e0", null ],
    [ "GetBeta", "d4/d64/classG4PreCompoundNeutron.html#a3a39153e18999e19cd9da39cef6710d5", null ],
    [ "GetRj", "d4/d64/classG4PreCompoundNeutron.html#a9ad0ab8f7dd0dc1a649336b9a4f0707c", null ],
    [ "operator!=", "d4/d64/classG4PreCompoundNeutron.html#aae2f40b5674935aa9d9261829aa37d8b", null ],
    [ "operator=", "d4/d64/classG4PreCompoundNeutron.html#a7bc16a315728a1ec5780c8f1c0a0642b", null ],
    [ "operator==", "d4/d64/classG4PreCompoundNeutron.html#aa53e5e44fa83be0e4e2a8326b981532b", null ],
    [ "theNeutronCoulombBarrier", "d4/d64/classG4PreCompoundNeutron.html#a5dfc37e0d14e2dabe1785b0fcb3fe709", null ]
];