var classG4PolarizedPhotoElectricXS =
[
    [ "G4PolarizedPhotoElectricXS", "d4/d6f/classG4PolarizedPhotoElectricXS.html#a38afd4132dc050933e92f9d4b4556b42", null ],
    [ "~G4PolarizedPhotoElectricXS", "d4/d6f/classG4PolarizedPhotoElectricXS.html#a8deab4cc1925bf645d3e9f7efeed519c", null ],
    [ "G4PolarizedPhotoElectricXS", "d4/d6f/classG4PolarizedPhotoElectricXS.html#a6ebb4ec6336ec8ac8e81beabbf5dba9d", null ],
    [ "GetPol2", "d4/d6f/classG4PolarizedPhotoElectricXS.html#aa6be5fa2f31fef58bdb9fabbea73ca2a", null ],
    [ "GetPol3", "d4/d6f/classG4PolarizedPhotoElectricXS.html#aca2de7fda34afe487fcfd0a82c31057c", null ],
    [ "Initialize", "d4/d6f/classG4PolarizedPhotoElectricXS.html#a37633e0d26feb254809c3847a5134f4a", null ],
    [ "operator=", "d4/d6f/classG4PolarizedPhotoElectricXS.html#a85df88c66b94e73c724b908336806b81", null ],
    [ "XSection", "d4/d6f/classG4PolarizedPhotoElectricXS.html#a91712df54cde2a9ae07e39fe0b57a671", null ],
    [ "fFinalElectronPolarization", "d4/d6f/classG4PolarizedPhotoElectricXS.html#aa548df1b9ae66c9d5fc730fa22a17e87", null ]
];