var classG4ComptonScattering =
[
    [ "G4ComptonScattering", "d4/d82/classG4ComptonScattering.html#ae33836ea080e9bc7b6ee246a0ea27fe8", null ],
    [ "~G4ComptonScattering", "d4/d82/classG4ComptonScattering.html#a490d895b73aa9d67aa65a48d4a1556f6", null ],
    [ "G4ComptonScattering", "d4/d82/classG4ComptonScattering.html#a20c88e81061e75e727b724de24e88ec7", null ],
    [ "InitialiseProcess", "d4/d82/classG4ComptonScattering.html#a9c19c3bd0a809fa1beafea9f028bf20f", null ],
    [ "IsApplicable", "d4/d82/classG4ComptonScattering.html#ad1519954939547eda21d4bb5fd9c7b84", null ],
    [ "operator=", "d4/d82/classG4ComptonScattering.html#a87176c0fabde72dc09718fcee3e2e920", null ],
    [ "ProcessDescription", "d4/d82/classG4ComptonScattering.html#a793d7425d47ec6e2b6c9d79934430e43", null ],
    [ "isInitialised", "d4/d82/classG4ComptonScattering.html#aca5a210057f07161df7522e06206846b", null ]
];