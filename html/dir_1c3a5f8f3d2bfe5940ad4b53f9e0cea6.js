var dir_1c3a5f8f3d2bfe5940ad4b53f9e0cea6 =
[
    [ "G4DNAAttachment.cc", "d4/d52/G4DNAAttachment_8cc.html", null ],
    [ "G4DNABrownianTransportation.cc", "d6/de0/G4DNABrownianTransportation_8cc.html", "d6/de0/G4DNABrownianTransportation_8cc" ],
    [ "G4DNAChargeDecrease.cc", "d5/d1e/G4DNAChargeDecrease_8cc.html", null ],
    [ "G4DNAChargeIncrease.cc", "d1/d90/G4DNAChargeIncrease_8cc.html", null ],
    [ "G4DNADissociation.cc", "df/d6b/G4DNADissociation_8cc.html", null ],
    [ "G4DNAElastic.cc", "d6/d2a/G4DNAElastic_8cc.html", null ],
    [ "G4DNAElectronHoleRecombination.cc", "d8/d78/G4DNAElectronHoleRecombination_8cc.html", "d8/d78/G4DNAElectronHoleRecombination_8cc" ],
    [ "G4DNAElectronSolvation.cc", "da/d53/G4DNAElectronSolvation_8cc.html", null ],
    [ "G4DNAExcitation.cc", "d1/d83/G4DNAExcitation_8cc.html", null ],
    [ "G4DNAIonisation.cc", "d4/ddb/G4DNAIonisation_8cc.html", null ],
    [ "G4DNAMolecularDissociation.cc", "d3/d6c/G4DNAMolecularDissociation_8cc.html", null ],
    [ "G4DNAPlasmonExcitation.cc", "df/d14/G4DNAPlasmonExcitation_8cc.html", null ],
    [ "G4DNAPositronium.cc", "d2/da9/G4DNAPositronium_8cc.html", null ],
    [ "G4DNARotExcitation.cc", "d4/d5e/G4DNARotExcitation_8cc.html", null ],
    [ "G4DNAScavengerProcess.cc", "db/dc0/G4DNAScavengerProcess_8cc.html", "db/dc0/G4DNAScavengerProcess_8cc" ],
    [ "G4DNASecondOrderReaction.cc", "d2/d07/G4DNASecondOrderReaction_8cc.html", "d2/d07/G4DNASecondOrderReaction_8cc" ],
    [ "G4DNAVibExcitation.cc", "d4/d68/G4DNAVibExcitation_8cc.html", null ],
    [ "G4DNAWaterDissociationDisplacer.cc", "d5/d33/G4DNAWaterDissociationDisplacer_8cc.html", "d5/d33/G4DNAWaterDissociationDisplacer_8cc" ]
];