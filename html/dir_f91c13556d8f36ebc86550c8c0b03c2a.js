var dir_f91c13556d8f36ebc86550c8c0b03c2a =
[
    [ "G4LatticeManager.hh", "d5/df0/G4LatticeManager_8hh.html", "d5/df0/G4LatticeManager_8hh" ],
    [ "G4LatticeReader.hh", "df/d17/G4LatticeReader_8hh.html", "df/d17/G4LatticeReader_8hh" ],
    [ "G4PhononDownconversion.hh", "d0/d55/G4PhononDownconversion_8hh.html", "d0/d55/G4PhononDownconversion_8hh" ],
    [ "G4PhononPolarization.hh", "dc/d9a/G4PhononPolarization_8hh.html", "dc/d9a/G4PhononPolarization_8hh" ],
    [ "G4PhononReflection.hh", "dd/d0e/G4PhononReflection_8hh.html", "dd/d0e/G4PhononReflection_8hh" ],
    [ "G4PhononScattering.hh", "d6/d2d/G4PhononScattering_8hh.html", "d6/d2d/G4PhononScattering_8hh" ],
    [ "G4PhononTrackMap.hh", "d5/d24/G4PhononTrackMap_8hh.html", "d5/d24/G4PhononTrackMap_8hh" ],
    [ "G4VPhononProcess.hh", "d5/df6/G4VPhononProcess_8hh.html", "d5/df6/G4VPhononProcess_8hh" ]
];