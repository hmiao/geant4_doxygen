var dir_f9eb979e16e56ea988a1881e4f18babd =
[
    [ "G4HtmlPPReporter.hh", "d9/da0/G4HtmlPPReporter_8hh.html", "d9/da0/G4HtmlPPReporter_8hh" ],
    [ "G4IsotopeMagneticMomentTable.hh", "d6/d70/G4IsotopeMagneticMomentTable_8hh.html", "d6/d70/G4IsotopeMagneticMomentTable_8hh" ],
    [ "G4SimplePPReporter.hh", "da/df7/G4SimplePPReporter_8hh.html", "da/df7/G4SimplePPReporter_8hh" ],
    [ "G4TextPPReporter.hh", "de/db6/G4TextPPReporter_8hh.html", "de/db6/G4TextPPReporter_8hh" ],
    [ "G4TextPPRetriever.hh", "d4/db5/G4TextPPRetriever_8hh.html", "d4/db5/G4TextPPRetriever_8hh" ],
    [ "G4VParticlePropertyReporter.hh", "d5/d52/G4VParticlePropertyReporter_8hh.html", "d5/d52/G4VParticlePropertyReporter_8hh" ],
    [ "G4VParticlePropertyRetriever.hh", "d9/d1a/G4VParticlePropertyRetriever_8hh.html", "d9/d1a/G4VParticlePropertyRetriever_8hh" ]
];