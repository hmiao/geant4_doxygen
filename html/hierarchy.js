var hierarchy =
[
    [ "__1DSortOut", "d7/d96/class____1DSortOut.html", null ],
    [ "_ListRef< LIST >", "d1/d5e/struct__ListRef.html", null ],
    [ "_MPI_Status", "d0/de4/struct__MPI__Status.html", null ],
    [ "G4SPSRandomGenerator::a_check", "da/d24/structG4SPSRandomGenerator_1_1a__check.html", null ],
    [ "G4ReduciblePolygon::ABVertex", "dd/db5/structG4ReduciblePolygon_1_1ABVertex.html", null ],
    [ "G4Backtrace::actions", "de/d7e/structG4Backtrace_1_1actions.html", null ],
    [ "G4INCL::Random::Adapter", "d5/d95/classG4INCL_1_1Random_1_1Adapter.html", null ],
    [ "G4INCL::AllocationPool< T >", "d9/ddc/classG4INCL_1_1AllocationPool.html", null ],
    [ "std::allocator", null, [
      [ "G4EnhancedVecAllocator< _Tp >", "d3/d45/classG4EnhancedVecAllocator.html", null ]
    ] ],
    [ "G4Profiler::ArgumentParser", "d5/d80/structG4Profiler_1_1ArgumentParser.html", null ],
    [ "G4VisCommandSceneAddArrow2D::Arrow2D", "d8/d61/structG4VisCommandSceneAddArrow2D_1_1Arrow2D.html", null ],
    [ "ATTRIBUTE", "de/d5c/structATTRIBUTE.html", null ],
    [ "attribute_id", "d1/d5d/structattribute__id.html", null ],
    [ "PTL::Back< Types >", "d2/d8d/structPTL_1_1Back.html", null ],
    [ "PTL::Back< Types..., BackT >", "d2/d60/structPTL_1_1Back_3_01Types_8_8_8_00_01BackT_01_4.html", null ],
    [ "std::bad_cast", null, [
      [ "G4BadAnyCast", "d4/d1a/classG4BadAnyCast.html", null ],
      [ "G4BadArgument", "d3/d47/classG4BadArgument.html", null ],
      [ "G4InvalidUICommand", "dc/d29/classG4InvalidUICommand.html", null ]
    ] ],
    [ "std::basic_streambuf", null, [
      [ "G4strstreambuf", "d2/dce/classG4strstreambuf.html", null ]
    ] ],
    [ "HepGeom::BasicVector3D< T >", "d3/d15/classHepGeom_1_1BasicVector3D.html", [
      [ "HepGeom::Normal3D< T >", "dc/d92/classHepGeom_1_1Normal3D.html", null ],
      [ "HepGeom::Point3D< T >", "d4/db6/classHepGeom_1_1Point3D.html", null ],
      [ "HepGeom::Vector3D< T >", "dc/d3c/classHepGeom_1_1Vector3D.html", null ]
    ] ],
    [ "HepGeom::BasicVector3D< double >", "d3/d15/classHepGeom_1_1BasicVector3D.html", [
      [ "HepGeom::Normal3D< double >", "da/d9f/classHepGeom_1_1Normal3D_3_01double_01_4.html", null ],
      [ "HepGeom::Point3D< double >", "d5/d0e/classHepGeom_1_1Point3D_3_01double_01_4.html", null ],
      [ "HepGeom::Vector3D< double >", "dc/d75/classHepGeom_1_1Vector3D_3_01double_01_4.html", null ]
    ] ],
    [ "HepGeom::BasicVector3D< float >", "d3/d15/classHepGeom_1_1BasicVector3D.html", [
      [ "HepGeom::Normal3D< float >", "dc/d3c/classHepGeom_1_1Normal3D_3_01float_01_4.html", null ],
      [ "HepGeom::Point3D< float >", "d3/dcd/classHepGeom_1_1Point3D_3_01float_01_4.html", null ],
      [ "HepGeom::Vector3D< float >", "d2/da3/classHepGeom_1_1Vector3D_3_01float_01_4.html", null ]
    ] ],
    [ "HepGeom::BasicVector3D< G4double >", "d3/d15/classHepGeom_1_1BasicVector3D.html", [
      [ "HepGeom::Point3D< G4double >", "d4/db6/classHepGeom_1_1Point3D.html", null ],
      [ "HepGeom::Vector3D< G4double >", "dc/d3c/classHepGeom_1_1Vector3D.html", null ]
    ] ],
    [ "G4AnalysisMessengerHelper::BinData", "d8/d2c/structG4AnalysisMessengerHelper_1_1BinData.html", null ],
    [ "binding", "d7/d45/structbinding.html", null ],
    [ "block", "d1/d7f/structblock.html", null ],
    [ "G4INCL::Book", "db/d29/classG4INCL_1_1Book.html", null ],
    [ "G4VTwistSurface::Boundary", "d5/d81/classG4VTwistSurface_1_1Boundary.html", null ],
    [ "G4DNASmoluchowskiDiffusion::BoundingBox", "d2/d42/structG4DNASmoluchowskiDiffusion_1_1BoundingBox.html", null ],
    [ "PTL::mpl::impl::Build_index_tuple< 0 >", "dc/d99/structPTL_1_1mpl_1_1impl_1_1Build__index__tuple_3_010_01_4.html", null ],
    [ "PTL::mpl::impl::Build_index_tuple< 1 >", "db/d65/structPTL_1_1mpl_1_1impl_1_1Build__index__tuple_3_011_01_4.html", null ],
    [ "G4SPSRandomGenerator::bweights_t", "d7/d46/structG4SPSRandomGenerator_1_1bweights__t.html", null ],
    [ "G4INCL::BystrickyEvaluator< N >", "d6/d9b/structG4INCL_1_1BystrickyEvaluator.html", null ],
    [ "cacheEl_t", "d9/def/structcacheEl__t.html", null ],
    [ "G4IonDEDXHandler::CacheEntry", "d2/d41/structG4IonDEDXHandler_1_1CacheEntry.html", null ],
    [ "CacheValue", "d4/de4/structCacheValue.html", null ],
    [ "G4INCL::CascadeAction", "d1/da4/classG4INCL_1_1CascadeAction.html", [
      [ "G4INCL::AvatarDumpAction", "db/d6d/classG4INCL_1_1AvatarDumpAction.html", null ]
    ] ],
    [ "code", "da/da8/structcode.html", null ],
    [ "G4GenericMessenger::Command", "d2/df6/structG4GenericMessenger_1_1Command.html", [
      [ "G4GenericMessenger::Method", "de/d85/structG4GenericMessenger_1_1Method.html", null ],
      [ "G4GenericMessenger::Property", "d2/da8/structG4GenericMessenger_1_1Property.html", null ]
    ] ],
    [ "G4DiffractiveExcitation::CommonVariables", "da/d25/structG4DiffractiveExcitation_1_1CommonVariables.html", null ],
    [ "G4FTFAnnihilation::CommonVariables", "d1/dec/structG4FTFAnnihilation_1_1CommonVariables.html", null ],
    [ "G4FTFModel::CommonVariables", "df/d82/structG4FTFModel_1_1CommonVariables.html", null ],
    [ "comparator", "da/dfe/structcomparator.html", null ],
    [ "comparatorEventSet", "d6/dc0/structcomparatorEventSet.html", null ],
    [ "CompareMaterial", "d0/df0/structCompareMaterial.html", null ],
    [ "G4MoleculeHandleManager::CompMoleculePointer", "db/d6e/structG4MoleculeHandleManager_1_1CompMoleculePointer.html", null ],
    [ "compReactionPerTime", "d5/ddf/structcompReactionPerTime.html", null ],
    [ "compTrackPerID", "d9/d23/structcompTrackPerID.html", null ],
    [ "G4INCL::Config", "da/d4f/classG4INCL_1_1Config.html", null ],
    [ "config_s", "d6/d89/structconfig__s.html", null ],
    [ "G4INCL::Nucleus::ConservationBalance", "de/d8a/structG4INCL_1_1Nucleus_1_1ConservationBalance.html", null ],
    [ "G4INCL::ConsideredPartner", "d8/da0/structG4INCL_1_1ConsideredPartner.html", null ],
    [ "CONTENT_SCAFFOLD", "d2/d8b/structCONTENT__SCAFFOLD.html", null ],
    [ "Counter", "d4/d57/structCounter.html", null ],
    [ "crossSectionData_s", "d7/d6c/structcrossSectionData__s.html", null ],
    [ "ct_data_s", "d6/d3b/structct__data__s.html", null ],
    [ "PTL::CTValue< Tp, Value >", "d2/d22/structPTL_1_1CTValue.html", null ],
    [ "G4VTwistSurface::CurrentStatus", "d4/d94/classG4VTwistSurface_1_1CurrentStatus.html", null ],
    [ "D1232", "d1/d58/structD1232.html", null ],
    [ "G4GSMottCorrection::DataPerDelta", "df/d90/structG4GSMottCorrection_1_1DataPerDelta.html", null ],
    [ "G4GSMottCorrection::DataPerEkin", "df/d60/structG4GSMottCorrection_1_1DataPerEkin.html", null ],
    [ "G4GSMottCorrection::DataPerMaterial", "da/d99/structG4GSMottCorrection_1_1DataPerMaterial.html", null ],
    [ "G4GSPWACorrections::DataPerMaterial", "d5/dd0/structG4GSPWACorrections_1_1DataPerMaterial.html", null ],
    [ "G4VisCommandSceneAddDate::Date", "d0/de0/structG4VisCommandSceneAddDate_1_1Date.html", null ],
    [ "CLHEP::DoubConv::DB8", "d8/d67/unionCLHEP_1_1DoubConv_1_1DB8.html", null ],
    [ "DEFAULT_ATTRIBUTE", "d9/d5a/structDEFAULT__ATTRIBUTE.html", null ],
    [ "Delete< T >", "d3/d4c/structDelete.html", null ],
    [ "DeleteCollisionInitialState", "d7/d12/structDeleteCollisionInitialState.html", null ],
    [ "DeleteDynamicParticle", "d0/dca/structDeleteDynamicParticle.html", null ],
    [ "G4StatMFChannel::DeleteFragment", "de/db1/structG4StatMFChannel_1_1DeleteFragment.html", null ],
    [ "G4StatMFMacroCanonical::DeleteFragment", "d2/de9/structG4StatMFMacroCanonical_1_1DeleteFragment.html", null ],
    [ "G4StatMFMicroCanonical::DeleteFragment", "dc/d16/structG4StatMFMicroCanonical_1_1DeleteFragment.html", null ],
    [ "G4StatMFMicroManager::DeleteFragment", "d7/df1/structG4StatMFMicroManager_1_1DeleteFragment.html", null ],
    [ "G4QGSParticipants::DeleteInteractionContent", "d0/d5e/structG4QGSParticipants_1_1DeleteInteractionContent.html", null ],
    [ "DeleteKineticTrack", "d6/d08/structDeleteKineticTrack.html", null ],
    [ "DeleteParton", "dc/d3d/structDeleteParton.html", null ],
    [ "G4QGSParticipants::DeletePartonPair", "d4/d47/structG4QGSParticipants_1_1DeletePartonPair.html", null ],
    [ "DeleteReactionProduct", "d5/d8b/structDeleteReactionProduct.html", null ],
    [ "G4SPBaryonTable::DeleteSPBaryon", "df/dc5/structG4SPBaryonTable_1_1DeleteSPBaryon.html", null ],
    [ "G4QGSParticipants::DeleteSplitableHadron", "d9/d46/structG4QGSParticipants_1_1DeleteSplitableHadron.html", null ],
    [ "DeleteString", "de/d55/structDeleteString.html", null ],
    [ "DeleteVSplitableHadron", "df/d04/structDeleteVSplitableHadron.html", null ],
    [ "G4GMocrenFileSceneHandler::Detector", "d9/d56/classG4GMocrenFileSceneHandler_1_1Detector.html", null ],
    [ "tools::sg::device_interactor", null, [
      [ "G4ToolsSGViewer< tools::Qt::session, tools::Qt::sg_viewer >", "df/d01/classG4ToolsSGViewer.html", [
        [ "G4ToolsSGQtViewer", "d3/d83/classG4ToolsSGQtViewer.html", null ]
      ] ],
      [ "G4ToolsSGViewer< SG_SESSION, SG_VIEWER >", "df/d01/classG4ToolsSGViewer.html", null ]
    ] ],
    [ "CLHEP::do_nothing_deleter", "d3/d01/structCLHEP_1_1do__nothing__deleter.html", null ],
    [ "CLHEP::detail::do_right_shift< n, false >", "de/d5b/structCLHEP_1_1detail_1_1do__right__shift_3_01n_00_01false_01_4.html", null ],
    [ "CLHEP::DoubConv", "d3/d9d/classCLHEP_1_1DoubConv.html", null ],
    [ "DTD", "d6/d63/structDTD.html", null ],
    [ "g4tim::dummy", "d7/d85/structg4tim_1_1dummy.html", null ],
    [ "E_isoAng", "de/d35/structE__isoAng.html", null ],
    [ "E_P_E_isoAng", "d5/d47/structE__P__E__isoAng.html", null ],
    [ "tools::sg::ecbk", null, [
      [ "plots_cbk", "df/d18/classplots__cbk.html", null ]
    ] ],
    [ "GMocrenDetector::Edge", "d3/d8b/structGMocrenDetector_1_1Edge.html", null ],
    [ "ELEMENT_TYPE", "d2/dfe/structELEMENT__TYPE.html", null ],
    [ "G4BetheHeitlerModel::ElementData", "d5/d57/structG4BetheHeitlerModel_1_1ElementData.html", null ],
    [ "G4eBremsstrahlungRelModel::ElementData", "da/dc3/structG4eBremsstrahlungRelModel_1_1ElementData.html", null ],
    [ "G4PairProductionRelModel::ElementData", "dc/d2f/structG4PairProductionRelModel_1_1ElementData.html", null ],
    [ "G4OpenInventorQtExaminerViewer::elementForSorting", "db/de3/structG4OpenInventorQtExaminerViewer_1_1elementForSorting.html", null ],
    [ "G4OpenInventorXtExaminerViewer::elementForSorting", "dc/dc7/structG4OpenInventorXtExaminerViewer_1_1elementForSorting.html", null ],
    [ "encoding", "db/dc5/structencoding.html", null ],
    [ "CLHEP::EngineFactory", "d4/d0b/classCLHEP_1_1EngineFactory.html", null ],
    [ "ENTITY", "d1/d9d/structENTITY.html", null ],
    [ "PTL::EnvSettings", "d4/d0a/classPTL_1_1EnvSettings.html", null ],
    [ "xercesc::ErrorHandler", null, [
      [ "G4GDMLErrorHandler", "dd/da0/classG4GDMLErrorHandler.html", null ]
    ] ],
    [ "HepTool::Evaluator", "d5/df7/classHepTool_1_1Evaluator.html", [
      [ "G4tgrEvaluator", "de/d90/classG4tgrEvaluator.html", null ]
    ] ],
    [ "Event", "d5/da5/classEvent.html", null ],
    [ "G4VisCommandSceneAddEventID::EventID", "d5/d94/structG4VisCommandSceneAddEventID_1_1EventID.html", null ],
    [ "G4INCL::EventInfo", "db/dbd/structG4INCL_1_1EventInfo.html", null ],
    [ "std::exception", null, [
      [ "CLHEP::DoubConvException", "df/d51/classCLHEP_1_1DoubConvException.html", null ],
      [ "G4HadronicException", "dd/d0d/classG4HadronicException.html", null ]
    ] ],
    [ "G4VisCommandSceneAddExtent::Extent", "d5/de9/structG4VisCommandSceneAddExtent_1_1Extent.html", null ],
    [ "Extractor< CONTAINER >", "d2/d19/classExtractor.html", null ],
    [ "G4Backtrace::fake_sigaction", "db/deb/structG4Backtrace_1_1fake__sigaction.html", null ],
    [ "G4Backtrace::fake_siginfo", "d6/d0a/structG4Backtrace_1_1fake__siginfo.html", null ],
    [ "G4INCL::FinalState", "da/d83/classG4INCL_1_1FinalState.html", null ],
    [ "G4PhysicalVolumesSearchScene::Findings", "de/dfb/structG4PhysicalVolumesSearchScene_1_1Findings.html", null ],
    [ "G4OpenGLFontBaseStore::FontInfo", "d3/dc9/structG4OpenGLFontBaseStore_1_1FontInfo.html", null ],
    [ "PTL::ForEachTupleArg< N >", "de/dc2/structPTL_1_1ForEachTupleArg.html", null ],
    [ "PTL::ForwardTupleAsArgs< N >", "d7/df9/structPTL_1_1ForwardTupleAsArgs.html", null ],
    [ "PTL::ForwardTupleAsArgs< 0 >", "dd/d01/structPTL_1_1ForwardTupleAsArgs_3_010_01_4.html", null ],
    [ "G4VisCommandSceneAddFrame::Frame", "de/db2/structG4VisCommandSceneAddFrame_1_1Frame.html", null ],
    [ "PTL::Front< Types >", "d6/d41/structPTL_1_1Front.html", null ],
    [ "PTL::Front< FrontT, Types... >", "dc/dae/structPTL_1_1Front_3_01FrontT_00_01Types_8_8_8_01_4.html", null ],
    [ "G3DetTable", "df/d1d/classG3DetTable.html", null ],
    [ "G3DetTableEntry", "de/d5a/classG3DetTableEntry.html", null ],
    [ "G3Division", "dc/db4/classG3Division.html", null ],
    [ "G3EleTable", "d3/df4/classG3EleTable.html", null ],
    [ "G3MatTable", "de/de1/classG3MatTable.html", null ],
    [ "G3MatTableEntry", "d2/d01/classG3MatTableEntry.html", null ],
    [ "G3MedTable", "df/d5c/classG3MedTable.html", null ],
    [ "G3MedTableEntry", "d0/d4c/classG3MedTableEntry.html", null ],
    [ "G3PartTable", "dc/dd5/classG3PartTable.html", null ],
    [ "G3Pos", "d5/db8/classG3Pos.html", null ],
    [ "G3RotTable", "d7/dd4/classG3RotTable.html", null ],
    [ "G3RotTableEntry", "d9/d17/classG3RotTableEntry.html", null ],
    [ "G3VolTable", "dc/d3c/classG3VolTable.html", null ],
    [ "G3VolTableEntry", "d2/db3/classG3VolTableEntry.html", null ],
    [ "G4Abla", "d2/ddc/classG4Abla.html", null ],
    [ "G4AblaVirtualData", "d9/de7/classG4AblaVirtualData.html", [
      [ "G4AblaDataFile", "dd/d2b/classG4AblaDataFile.html", null ]
    ] ],
    [ "G4Absorber", "d3/da3/classG4Absorber.html", null ],
    [ "G4AccumulableManager", "dc/d11/classG4AccumulableManager.html", null ],
    [ "G4AdjointCrossSurfChecker", "d9/d81/classG4AdjointCrossSurfChecker.html", null ],
    [ "G4AdjointCSManager", "d8/d73/classG4AdjointCSManager.html", null ],
    [ "G4AdjointCSMatrix", "d1/d5a/classG4AdjointCSMatrix.html", null ],
    [ "G4AdjointInterpolator", "dd/dd0/classG4AdjointInterpolator.html", null ],
    [ "G4AdjointPosOnPhysVolGenerator", "d4/d0a/classG4AdjointPosOnPhysVolGenerator.html", null ],
    [ "G4AdjointPrimaryGenerator", "dc/d93/classG4AdjointPrimaryGenerator.html", null ],
    [ "G4AffineTransform", "d8/d1e/classG4AffineTransform.html", null ],
    [ "G4Ald", "de/d37/classG4Ald.html", null ],
    [ "G4AllITFinder", "d5/df9/classG4AllITFinder.html", null ],
    [ "G4AllocatorBase", "df/dda/classG4AllocatorBase.html", [
      [ "G4Allocator< G4KDNode< PointT > >", "d3/dfe/classG4Allocator.html", null ],
      [ "G4Allocator< G4KDNodeCopy< PointCopyT > >", "d3/dfe/classG4Allocator.html", null ],
      [ "G4Allocator< G4Octree >", "d3/dfe/classG4Allocator.html", null ],
      [ "G4Allocator< Type >", "d3/dfe/classG4Allocator.html", null ]
    ] ],
    [ "G4AllocatorList", "d4/d8b/classG4AllocatorList.html", null ],
    [ "G4AllocatorPool", "d1/dc0/classG4AllocatorPool.html", null ],
    [ "G4AllocStats", "de/d84/classG4AllocStats.html", null ],
    [ "G4Analyser", "d1/df3/classG4Analyser.html", null ],
    [ "G4AnalysisManagerState", "df/db3/classG4AnalysisManagerState.html", null ],
    [ "G4AnalysisMessengerHelper", "d4/d0a/classG4AnalysisMessengerHelper.html", null ],
    [ "G4AnalysisVerbose", "d1/dbb/classG4AnalysisVerbose.html", null ],
    [ "G4AnalyticalPolSolver", "d6/d53/classG4AnalyticalPolSolver.html", null ],
    [ "G4AnyMethod", "db/db2/classG4AnyMethod.html", null ],
    [ "G4AnyType", "da/d5d/classG4AnyType.html", null ],
    [ "G4AssemblyTriplet", "d6/d0d/classG4AssemblyTriplet.html", null ],
    [ "G4AssemblyVolume", "d8/d10/classG4AssemblyVolume.html", null ],
    [ "G4ASTARStopping", "d2/d78/classG4ASTARStopping.html", null ],
    [ "G4AtomicBond", "dd/d9e/classG4AtomicBond.html", null ],
    [ "G4AtomicDeexcitation", "dc/da7/classG4AtomicDeexcitation.html", null ],
    [ "G4AtomicFormFactor", "d8/ddb/classG4AtomicFormFactor.html", null ],
    [ "G4AtomicShell", "dc/deb/classG4AtomicShell.html", null ],
    [ "G4AtomicShells", "d0/dbe/classG4AtomicShells.html", null ],
    [ "G4AtomicShells_XDB_EADL", "d9/daf/classG4AtomicShells__XDB__EADL.html", null ],
    [ "G4AtomicTransitionManager", "d3/d9e/classG4AtomicTransitionManager.html", null ],
    [ "G4AttCheck", "d1/d53/classG4AttCheck.html", null ],
    [ "G4AttDef", "d3/d75/classG4AttDef.html", [
      [ "G4AttDefT< T >", "d8/d29/classG4AttDefT.html", null ]
    ] ],
    [ "G4AttHolder", "da/d26/classG4AttHolder.html", [
      [ "SoG4LineSet", "d2/daa/classSoG4LineSet.html", null ],
      [ "SoG4MarkerSet", "dc/dd5/classSoG4MarkerSet.html", null ],
      [ "SoG4Polyhedron", "de/db2/classSoG4Polyhedron.html", null ]
    ] ],
    [ "G4AttValue", "d0/da4/classG4AttValue.html", null ],
    [ "G4AugerData", "d2/d6b/classG4AugerData.html", null ],
    [ "G4AugerTransition", "d1/dd1/classG4AugerTransition.html", null ],
    [ "G4AuxiliaryNavServices", "db/d4b/classG4AuxiliaryNavServices.html", null ],
    [ "G4Backtrace", "df/d94/classG4Backtrace.html", null ],
    [ "G4BaryonConstructor", "dd/dc8/classG4BaryonConstructor.html", null ],
    [ "G4BaryonSplitter", "dd/d3e/classG4BaryonSplitter.html", null ],
    [ "G4BaseAnalysisManager", "d7/d3d/classG4BaseAnalysisManager.html", [
      [ "G4HnManager", "d0/df0/classG4HnManager.html", null ],
      [ "G4NtupleBookingManager", "dd/d27/classG4NtupleBookingManager.html", null ],
      [ "G4RootMainNtupleManager", "dd/d28/classG4RootMainNtupleManager.html", null ],
      [ "G4VNtupleManager", "d7/db9/classG4VNtupleManager.html", [
        [ "G4BaseNtupleManager", "df/df0/classG4BaseNtupleManager.html", [
          [ "G4TNtupleManager< tools::wcsv::ntuple, std::ofstream >", "da/ddd/classG4TNtupleManager.html", [
            [ "G4CsvNtupleManager", "da/d74/classG4CsvNtupleManager.html", null ]
          ] ],
          [ "G4TNtupleManager< tools::hdf5::ntuple, G4Hdf5File >", "da/ddd/classG4TNtupleManager.html", [
            [ "G4Hdf5NtupleManager", "d2/d7e/classG4Hdf5NtupleManager.html", null ]
          ] ],
          [ "G4TNtupleManager< tools::wroot::ntuple, G4RootFile >", "da/ddd/classG4TNtupleManager.html", [
            [ "G4RootNtupleManager", "df/d9c/classG4RootNtupleManager.html", null ]
          ] ],
          [ "G4TNtupleManager< tools::waxml::ntuple, std::ofstream >", "da/ddd/classG4TNtupleManager.html", [
            [ "G4XmlNtupleManager", "d8/d68/classG4XmlNtupleManager.html", null ]
          ] ],
          [ "G4RootPNtupleManager", "d1/df3/classG4RootPNtupleManager.html", null ],
          [ "G4TNtupleManager< NT, FT >", "da/ddd/classG4TNtupleManager.html", null ]
        ] ]
      ] ],
      [ "G4VRNtupleManager", "d3/dd8/classG4VRNtupleManager.html", [
        [ "G4BaseRNtupleManager", "d6/d50/classG4BaseRNtupleManager.html", [
          [ "G4TRNtupleManager< tools::rcsv::ntuple >", "d8/dbf/classG4TRNtupleManager.html", [
            [ "G4CsvRNtupleManager", "db/d98/classG4CsvRNtupleManager.html", null ]
          ] ],
          [ "G4TRNtupleManager< tools::hdf5::ntuple >", "d8/dbf/classG4TRNtupleManager.html", [
            [ "G4Hdf5RNtupleManager", "da/d4a/classG4Hdf5RNtupleManager.html", null ]
          ] ],
          [ "G4TRNtupleManager< tools::rroot::ntuple >", "d8/dbf/classG4TRNtupleManager.html", [
            [ "G4RootRNtupleManager", "d7/de8/classG4RootRNtupleManager.html", null ]
          ] ],
          [ "G4TRNtupleManager< tools::aida::ntuple >", "d8/dbf/classG4TRNtupleManager.html", [
            [ "G4XmlRNtupleManager", "d2/d51/classG4XmlRNtupleManager.html", null ]
          ] ],
          [ "G4TRNtupleManager< NT >", "d8/dbf/classG4TRNtupleManager.html", null ]
        ] ]
      ] ]
    ] ],
    [ "G4BaseFileManager", "de/d56/classG4BaseFileManager.html", [
      [ "G4VFileManager", "d7/dfe/classG4VFileManager.html", [
        [ "G4VTFileManager< std::ofstream >", "d3/d25/classG4VTFileManager.html", [
          [ "G4CsvFileManager", "dc/dd7/classG4CsvFileManager.html", null ],
          [ "G4XmlFileManager", "d0/d1e/classG4XmlFileManager.html", null ]
        ] ],
        [ "G4VTFileManager< G4Hdf5File >", "d3/d25/classG4VTFileManager.html", [
          [ "G4Hdf5FileManager", "d7/d8f/classG4Hdf5FileManager.html", null ]
        ] ],
        [ "G4VTFileManager< G4RootFile >", "d3/d25/classG4VTFileManager.html", [
          [ "G4RootFileManager", "d9/d6d/classG4RootFileManager.html", null ]
        ] ],
        [ "G4GenericFileManager", "da/d3c/classG4GenericFileManager.html", null ],
        [ "G4VTFileManager< FT >", "d3/d25/classG4VTFileManager.html", null ]
      ] ],
      [ "G4VRFileManager", "d4/db4/classG4VRFileManager.html", [
        [ "G4CsvRFileManager", "d5/de7/classG4CsvRFileManager.html", null ],
        [ "G4Hdf5RFileManager", "d0/d52/classG4Hdf5RFileManager.html", null ],
        [ "G4RootRFileManager", "d0/d5d/classG4RootRFileManager.html", null ],
        [ "G4XmlRFileManager", "d1/d9c/classG4XmlRFileManager.html", null ]
      ] ]
    ] ],
    [ "G4BatemanParameters", "dc/d55/classG4BatemanParameters.html", null ],
    [ "G4BCAction", "df/d46/classG4BCAction.html", [
      [ "G4BCDecay", "d4/dc4/classG4BCDecay.html", null ],
      [ "G4BCLateParticle", "df/d78/classG4BCLateParticle.html", null ],
      [ "G4MesonAbsorption", "d4/dca/classG4MesonAbsorption.html", null ],
      [ "G4Scatterer", "d4/d15/classG4Scatterer.html", null ]
    ] ],
    [ "G4BertiniElectroNuclearBuilder", "d4/d8a/classG4BertiniElectroNuclearBuilder.html", [
      [ "G4LENDBertiniGammaElectroNuclearBuilder", "dd/d60/classG4LENDBertiniGammaElectroNuclearBuilder.html", null ]
    ] ],
    [ "G4Bessel", "dd/d77/classG4Bessel.html", null ],
    [ "G4BestUnit", "d7/dfd/classG4BestUnit.html", null ],
    [ "G4BetaDecayCorrections", "d6/d56/classG4BetaDecayCorrections.html", null ],
    [ "G4BiasingHelper", "d0/db0/classG4BiasingHelper.html", null ],
    [ "G4BiasingOperationManager", "da/d18/classG4BiasingOperationManager.html", null ],
    [ "G4BiasingProcessSharedData", "dd/d2d/classG4BiasingProcessSharedData.html", null ],
    [ "G4BlockingList", "d6/dd1/classG4BlockingList.html", null ],
    [ "G4BosonConstructor", "da/ded/classG4BosonConstructor.html", null ],
    [ "G4BoundingEnvelope", "dc/d31/classG4BoundingEnvelope.html", null ],
    [ "G4BrownianAction", "db/db6/classG4BrownianAction.html", null ],
    [ "G4BufferError", "db/d96/classG4BufferError.html", null ],
    [ "G4BulirschStoer", "da/d23/classG4BulirschStoer.html", null ],
    [ "G4Cache< VALTYPE >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4BiasingOperatorStateNotifier * >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4bool >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4double >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4HadFinalState * >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4ParticleHPAngular::toBeCached >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4ParticleHPContAngularPar * >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4ParticleHPContAngularPar::toBeCached * >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4ParticleHPEnAngCorrelation::toBeCached >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4ParticleHPFissionBaseFS::toBeCached >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4ParticleHPFSFissionFS::toBeCached >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4ParticleHPProduct::toBeCached >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4PhysicsFreeVector * >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4SingleParticleSource::part_prop_t >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4SPSEneDistribution::threadLocal_t >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4SPSPosDistribution::thread_data_t >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4SPSRandomGenerator::a_check >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4SPSRandomGenerator::bweights_t >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< G4VParticleHPEnergyAngular::toBeCached >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< std::map< const G4LogicalVolume *, G4VBiasingOperator * > >", "da/dc8/classG4Cache.html", [
      [ "G4MapCache< const G4LogicalVolume *, G4VBiasingOperator * >", "d6/df6/classG4MapCache.html", null ]
    ] ],
    [ "G4Cache< std::map< const G4ProcessManager *, G4BiasingProcessSharedData * > >", "da/dc8/classG4Cache.html", [
      [ "G4MapCache< const G4ProcessManager *, G4BiasingProcessSharedData * >", "d6/df6/classG4MapCache.html", null ]
    ] ],
    [ "G4Cache< std::map< G4VBiasingOperation *, std::size_t > >", "da/dc8/classG4Cache.html", [
      [ "G4MapCache< G4VBiasingOperation *, std::size_t >", "d6/df6/classG4MapCache.html", null ]
    ] ],
    [ "G4Cache< std::map< KEYTYPE, VALTYPE > >", "da/dc8/classG4Cache.html", [
      [ "G4MapCache< KEYTYPE, VALTYPE >", "d6/df6/classG4MapCache.html", null ]
    ] ],
    [ "G4Cache< std::vector< G4int > * >", "da/dc8/classG4Cache.html", null ],
    [ "G4Cache< std::vector< G4VBiasingOperation * > >", "da/dc8/classG4Cache.html", [
      [ "G4VectorCache< G4VBiasingOperation * >", "da/d85/classG4VectorCache.html", null ]
    ] ],
    [ "G4Cache< std::vector< G4VBiasingOperator * > >", "da/dc8/classG4Cache.html", [
      [ "G4VectorCache< G4VBiasingOperator * >", "da/d85/classG4VectorCache.html", null ]
    ] ],
    [ "G4Cache< std::vector< VALTYPE > >", "da/dc8/classG4Cache.html", [
      [ "G4VectorCache< VALTYPE >", "da/d85/classG4VectorCache.html", null ]
    ] ],
    [ "G4Cache< T * >", "da/dc8/classG4Cache.html", [
      [ "G4ThreadLocalSingleton< T >", "d5/dd7/classG4ThreadLocalSingleton.html", null ]
    ] ],
    [ "G4CacheReference< VALTYPE >", "da/d8c/classG4CacheReference.html", null ],
    [ "G4CacheReference< G4double >", "df/de8/classG4CacheReference_3_01G4double_01_4.html", null ],
    [ "G4CacheReference< VALTYPE * >", "de/d07/classG4CacheReference_3_01VALTYPE_01_5_01_4.html", null ],
    [ "G4CacheReference< value_type >", "da/d8c/classG4CacheReference.html", null ],
    [ "G4CameronGilbertPairingCorrections", "db/d1f/classG4CameronGilbertPairingCorrections.html", null ],
    [ "G4CameronGilbertShellCorrections", "d0/d6c/classG4CameronGilbertShellCorrections.html", null ],
    [ "G4CameronShellPlusPairingCorrections", "d4/d31/classG4CameronShellPlusPairingCorrections.html", null ],
    [ "G4CameronTruranHilfPairingCorrections", "dc/df1/classG4CameronTruranHilfPairingCorrections.html", null ],
    [ "G4CameronTruranHilfShellCorrections", "d3/d43/classG4CameronTruranHilfShellCorrections.html", null ],
    [ "G4CascadeChannel", "de/db2/classG4CascadeChannel.html", [
      [ "G4CascadeFunctions< G4CascadeNNChannelData, G4PionNucSampler >", "df/d30/classG4CascadeFunctions.html", [
        [ "G4CascadeNNChannel", "d2/da4/classG4CascadeNNChannel.html", null ]
      ] ],
      [ "G4CascadeFunctions< G4CascadeNPChannelData, G4PionNucSampler >", "df/d30/classG4CascadeFunctions.html", [
        [ "G4CascadeNPChannel", "dd/d38/classG4CascadeNPChannel.html", null ]
      ] ],
      [ "G4CascadeFunctions< G4CascadePPChannelData, G4PionNucSampler >", "df/d30/classG4CascadeFunctions.html", [
        [ "G4CascadePPChannel", "db/d60/classG4CascadePPChannel.html", null ]
      ] ],
      [ "G4CascadeFunctions< DATA, SAMP >", "df/d30/classG4CascadeFunctions.html", null ]
    ] ],
    [ "G4CascadeChannelTables", "da/d35/classG4CascadeChannelTables.html", null ],
    [ "G4CascadeCoalescence", "de/dd8/classG4CascadeCoalescence.html", null ],
    [ "G4CascadeData< NE, N2, N3, N4, N5, N6, N7, N8, N9 >", "d4/dec/structG4CascadeData.html", null ],
    [ "G4CascadeGamNChannelData", "da/d81/structG4CascadeGamNChannelData.html", null ],
    [ "G4CascadeGamPChannelData", "d7/dc8/structG4CascadeGamPChannelData.html", null ],
    [ "G4CascadeHistory", "df/de8/classG4CascadeHistory.html", null ],
    [ "G4CascadeInterpolator< NBINS >", "d5/d3e/classG4CascadeInterpolator.html", null ],
    [ "G4CascadeInterpolator< 30 >", "d5/d3e/classG4CascadeInterpolator.html", null ],
    [ "G4CascadeInterpolator< 5 >", "d5/d3e/classG4CascadeInterpolator.html", null ],
    [ "G4CascadeInterpolator< 72 >", "d5/d3e/classG4CascadeInterpolator.html", null ],
    [ "G4CascadeInterpolator< NKEBINS >", "d5/d3e/classG4CascadeInterpolator.html", null ],
    [ "G4CascadeKminusNChannelData", "d9/d64/structG4CascadeKminusNChannelData.html", null ],
    [ "G4CascadeKminusPChannelData", "df/d7a/structG4CascadeKminusPChannelData.html", null ],
    [ "G4CascadeKplusNChannelData", "d5/d19/structG4CascadeKplusNChannelData.html", null ],
    [ "G4CascadeKplusPChannelData", "d2/d7a/structG4CascadeKplusPChannelData.html", null ],
    [ "G4CascadeKzeroBarNChannelData", "d2/d70/structG4CascadeKzeroBarNChannelData.html", null ],
    [ "G4CascadeKzeroBarPChannelData", "d1/d2f/structG4CascadeKzeroBarPChannelData.html", null ],
    [ "G4CascadeKzeroNChannelData", "d9/d09/structG4CascadeKzeroNChannelData.html", null ],
    [ "G4CascadeKzeroPChannelData", "dc/dd8/structG4CascadeKzeroPChannelData.html", null ],
    [ "G4CascadeLambdaNChannelData", "d9/d67/structG4CascadeLambdaNChannelData.html", null ],
    [ "G4CascadeLambdaPChannelData", "d5/df8/structG4CascadeLambdaPChannelData.html", null ],
    [ "G4CascadeMuMinusPChannelData", "d4/d0d/structG4CascadeMuMinusPChannelData.html", null ],
    [ "G4CascadeNNChannelData", "de/da6/structG4CascadeNNChannelData.html", null ],
    [ "G4CascadeNPChannelData", "d4/d1b/structG4CascadeNPChannelData.html", null ],
    [ "G4CascadeOmegaMinusNChannelData", "dc/ded/structG4CascadeOmegaMinusNChannelData.html", null ],
    [ "G4CascadeOmegaMinusPChannelData", "da/d87/structG4CascadeOmegaMinusPChannelData.html", null ],
    [ "G4CascadeParameters", "d5/d41/classG4CascadeParameters.html", null ],
    [ "G4CascadePiMinusNChannelData", "d0/dd8/structG4CascadePiMinusNChannelData.html", null ],
    [ "G4CascadePiMinusPChannelData", "de/d43/structG4CascadePiMinusPChannelData.html", null ],
    [ "G4CascadePiPlusNChannelData", "d5/d75/structG4CascadePiPlusNChannelData.html", null ],
    [ "G4CascadePiPlusPChannelData", "dd/deb/structG4CascadePiPlusPChannelData.html", null ],
    [ "G4CascadePiZeroNChannelData", "da/d72/structG4CascadePiZeroNChannelData.html", null ],
    [ "G4CascadePiZeroPChannelData", "d5/d8d/structG4CascadePiZeroPChannelData.html", null ],
    [ "G4CascadePPChannelData", "db/d3f/structG4CascadePPChannelData.html", null ],
    [ "G4CascadeSampler< NBINS, NMULT >", "d7/dd9/classG4CascadeSampler.html", null ],
    [ "G4CascadeSampler< 30, 8 >", "d7/dd9/classG4CascadeSampler.html", [
      [ "G4KaonSampler", "d9/d90/structG4KaonSampler.html", null ],
      [ "G4PionNucSampler", "df/dcd/structG4PionNucSampler.html", [
        [ "G4CascadeFunctions< G4CascadeNNChannelData, G4PionNucSampler >", "df/d30/classG4CascadeFunctions.html", null ],
        [ "G4CascadeFunctions< G4CascadeNPChannelData, G4PionNucSampler >", "df/d30/classG4CascadeFunctions.html", null ],
        [ "G4CascadeFunctions< G4CascadePPChannelData, G4PionNucSampler >", "df/d30/classG4CascadeFunctions.html", null ]
      ] ]
    ] ],
    [ "G4CascadeSampler< 31, 6 >", "d7/dd9/classG4CascadeSampler.html", [
      [ "G4HyperonSampler", "d5/d77/structG4HyperonSampler.html", null ],
      [ "G4KaonHypSampler", "d9/d08/structG4KaonHypSampler.html", null ]
    ] ],
    [ "G4CascadeSigmaMinusNChannelData", "d0/d66/structG4CascadeSigmaMinusNChannelData.html", null ],
    [ "G4CascadeSigmaMinusPChannelData", "d4/d89/structG4CascadeSigmaMinusPChannelData.html", null ],
    [ "G4CascadeSigmaPlusNChannelData", "d1/dcf/structG4CascadeSigmaPlusNChannelData.html", null ],
    [ "G4CascadeSigmaPlusPChannelData", "d9/da5/structG4CascadeSigmaPlusPChannelData.html", null ],
    [ "G4CascadeSigmaZeroNChannelData", "d0/d15/structG4CascadeSigmaZeroNChannelData.html", null ],
    [ "G4CascadeSigmaZeroPChannelData", "d8/dd5/structG4CascadeSigmaZeroPChannelData.html", null ],
    [ "G4CascadeXiMinusNChannelData", "d6/d33/structG4CascadeXiMinusNChannelData.html", null ],
    [ "G4CascadeXiMinusPChannelData", "d6/de5/structG4CascadeXiMinusPChannelData.html", null ],
    [ "G4CascadeXiZeroNChannelData", "db/d6c/structG4CascadeXiZeroNChannelData.html", null ],
    [ "G4CascadeXiZeroPChannelData", "d7/dbe/structG4CascadeXiZeroPChannelData.html", null ],
    [ "G4CascadParticle", "dc/d6e/classG4CascadParticle.html", null ],
    [ "G4CellScoreComposer", "d4/d5b/classG4CellScoreComposer.html", null ],
    [ "G4CellScoreValues", "de/d03/classG4CellScoreValues.html", null ],
    [ "G4ChannelingECHARM", "dc/d86/classG4ChannelingECHARM.html", null ],
    [ "G4ChargeState", "d4/d2d/classG4ChargeState.html", null ],
    [ "G4ChatterjeeCrossSection", "de/dc0/classG4ChatterjeeCrossSection.html", null ],
    [ "G4ChebyshevApproximation", "da/d62/classG4ChebyshevApproximation.html", null ],
    [ "G4ChordFinder", "df/d7d/classG4ChordFinder.html", null ],
    [ "G4ChordFinderDelegate< Driver >", "db/d34/classG4ChordFinderDelegate.html", null ],
    [ "G4ChordFinderDelegate< G4FSALIntegrationDriver< T > >", "db/d34/classG4ChordFinderDelegate.html", [
      [ "G4FSALIntegrationDriver< T >", "d6/d6b/classG4FSALIntegrationDriver.html", null ]
    ] ],
    [ "G4ChordFinderDelegate< G4IntegrationDriver< G4BulirschStoer > >", "db/d34/classG4ChordFinderDelegate.html", [
      [ "G4IntegrationDriver< G4BulirschStoer >", "d7/d06/classG4IntegrationDriver_3_01G4BulirschStoer_01_4.html", null ]
    ] ],
    [ "G4ChordFinderDelegate< G4IntegrationDriver< T > >", "db/d34/classG4ChordFinderDelegate.html", [
      [ "G4IntegrationDriver< T >", "d1/dbc/classG4IntegrationDriver.html", null ]
    ] ],
    [ "G4ChordFinderDelegate< G4MagInt_Driver >", "db/d34/classG4ChordFinderDelegate.html", [
      [ "G4MagInt_Driver", "d2/df5/classG4MagInt__Driver.html", null ]
    ] ],
    [ "G4ChordFinderDelegate< G4OldMagIntDriver >", "db/d34/classG4ChordFinderDelegate.html", [
      [ "G4OldMagIntDriver", "de/d00/classG4OldMagIntDriver.html", null ]
    ] ],
    [ "G4ChunkIndexType", "d6/d8a/structG4ChunkIndexType.html", null ],
    [ "G4ChunkType", "d4/dd7/structG4ChunkType.html", null ],
    [ "G4Clebsch", "d0/d6a/classG4Clebsch.html", null ],
    [ "G4ClippablePolygon", "d5/d80/classG4ClippablePolygon.html", null ],
    [ "G4CollisionInitialState", "df/d8a/classG4CollisionInitialState.html", null ],
    [ "G4CollisionManager", "df/d36/classG4CollisionManager.html", null ],
    [ "G4CollisionOutput", "db/d1e/classG4CollisionOutput.html", null ],
    [ "G4Colour", "da/daf/classG4Colour.html", null ],
    [ "G4ConvergenceTester", "d2/dae/classG4ConvergenceTester.html", null ],
    [ "G4ConversionFatalError", "d5/d46/structG4ConversionFatalError.html", [
      [ "G4AttValueFilterT< T, ConversionErrorPolicy >", "d7/d37/classG4AttValueFilterT.html", null ],
      [ "G4DimensionedType< T, ConversionErrorPolicy >", "d8/dae/classG4DimensionedType.html", null ]
    ] ],
    [ "G4CookPairingCorrections", "d0/db2/classG4CookPairingCorrections.html", null ],
    [ "G4CookShellCorrections", "dc/d36/classG4CookShellCorrections.html", null ],
    [ "G4CountedObject< X >", "d8/ded/classG4CountedObject.html", null ],
    [ "G4CountedObject< G4VTouchable >", "d8/ded/classG4CountedObject.html", null ],
    [ "G4coutDestination", "d9/d68/classG4coutDestination.html", [
      [ "G4BuffercoutDestination", "df/d97/classG4BuffercoutDestination.html", null ],
      [ "G4FilecoutDestination", "d2/de9/classG4FilecoutDestination.html", null ],
      [ "G4LockcoutDestination", "db/db5/classG4LockcoutDestination.html", null ],
      [ "G4MasterForwardcoutDestination", "d0/db7/classG4MasterForwardcoutDestination.html", null ],
      [ "G4MulticoutDestination", "db/dc1/classG4MulticoutDestination.html", [
        [ "G4MTcoutDestination", "d0/dac/classG4MTcoutDestination.html", null ]
      ] ],
      [ "G4UIsession", "dd/d2f/classG4UIsession.html", [
        [ "G4UIbatch", "df/d29/classG4UIbatch.html", null ],
        [ "G4VBasicShell", "df/dda/classG4VBasicShell.html", [
          [ "G4UIQt", "d6/d5b/classG4UIQt.html", null ],
          [ "G4UIWin32", "da/d30/classG4UIWin32.html", null ],
          [ "G4UIXm", "d9/d40/classG4UIXm.html", null ],
          [ "G4UIterminal", "d5/de9/classG4UIterminal.html", null ]
        ] ]
      ] ]
    ] ],
    [ "G4coutDestinationVector", null, [
      [ "G4MulticoutDestination", "db/dc1/classG4MulticoutDestination.html", null ]
    ] ],
    [ "G4CreatorFactoryT< T, Identifier, Creator >", "d2/d68/classG4CreatorFactoryT.html", null ],
    [ "G4CrossSectionBuffer", "df/dba/classG4CrossSectionBuffer.html", null ],
    [ "G4CrossSectionDataSetRegistry", "d0/df9/classG4CrossSectionDataSetRegistry.html", null ],
    [ "G4CrossSectionDataStore", "de/da5/classG4CrossSectionDataStore.html", null ],
    [ "G4CrossSectionFactoryRegistry", "dc/d45/classG4CrossSectionFactoryRegistry.html", null ],
    [ "G4CrossSectionSourcePtr", "d5/d87/classG4CrossSectionSourcePtr.html", null ],
    [ "G4CrystalAtomBase", "d1/d75/classG4CrystalAtomBase.html", null ],
    [ "G4CrystalUnitCell", "d6/d53/classG4CrystalUnitCell.html", null ],
    [ "G4DataFormatError", "dd/d65/classG4DataFormatError.html", null ],
    [ "G4DataInterpolation", "dc/dd0/classG4DataInterpolation.html", null ],
    [ "G4DCIOcatalog", "df/d6a/classG4DCIOcatalog.html", null ],
    [ "G4DCofThisEvent", "dd/dcb/classG4DCofThisEvent.html", null ],
    [ "G4DCtable", "d3/d78/classG4DCtable.html", null ],
    [ "G4DecayKineticTracks", "db/d5f/classG4DecayKineticTracks.html", null ],
    [ "G4DecayProducts", "da/d2d/classG4DecayProducts.html", null ],
    [ "G4DecayStrongResonances", "d5/dfc/classG4DecayStrongResonances.html", null ],
    [ "G4DecayTable", "db/d89/classG4DecayTable.html", null ],
    [ "G4DeexPrecoParameters", "d1/d89/classG4DeexPrecoParameters.html", null ],
    [ "G4Delete", "d5/d15/structG4Delete.html", null ],
    [ "G4DensityEffectCalculator", "d7/d18/classG4DensityEffectCalculator.html", null ],
    [ "G4DensityEffectData", "d2/d38/classG4DensityEffectData.html", null ],
    [ "G4DetailedBalancePhaseSpaceIntegral", "d7/d25/classG4DetailedBalancePhaseSpaceIntegral.html", null ],
    [ "G4DiffractiveExcitation", "d9/d3a/classG4DiffractiveExcitation.html", null ],
    [ "G4DiffractiveStringBuilder", "d9/d6d/classG4DiffractiveStringBuilder.html", null ],
    [ "G4DigiManager", "d1/d3f/classG4DigiManager.html", null ],
    [ "G4DNABoundingBox", "d3/d97/classG4DNABoundingBox.html", null ],
    [ "G4DNACPA100WaterExcitationStructure", "da/d87/classG4DNACPA100WaterExcitationStructure.html", null ],
    [ "G4DNACPA100WaterIonisationStructure", "d3/d96/classG4DNACPA100WaterIonisationStructure.html", null ],
    [ "G4DNADamage", "df/d26/classG4DNADamage.html", null ],
    [ "G4DNAEmfietzoglouWaterExcitationStructure", "d0/d50/classG4DNAEmfietzoglouWaterExcitationStructure.html", null ],
    [ "G4DNAEmfietzoglouWaterIonisationStructure", "d7/d58/classG4DNAEmfietzoglouWaterIonisationStructure.html", null ],
    [ "G4DNAGenericIonsManager", "dd/d86/classG4DNAGenericIonsManager.html", null ],
    [ "G4DNAGillespieDirectMethod", "dd/d8f/classG4DNAGillespieDirectMethod.html", null ],
    [ "G4DNAMesh", "d4/d7d/classG4DNAMesh.html", null ],
    [ "G4DNAMolecularReactionData", "d1/dde/classG4DNAMolecularReactionData.html", null ],
    [ "G4DNAPTBAugerModel", "d8/d8f/classG4DNAPTBAugerModel.html", null ],
    [ "G4DNAPTBIonisationStructure", "d1/dee/classG4DNAPTBIonisationStructure.html", null ],
    [ "G4DNARevertProbability", "df/dec/classG4DNARevertProbability.html", null ],
    [ "G4DNASmoluchowskiDiffusion", "de/d66/classG4DNASmoluchowskiDiffusion.html", null ],
    [ "G4DNASolvationModelFactory", "df/d55/classG4DNASolvationModelFactory.html", null ],
    [ "G4DNAWaterExcitationStructure", "d9/de7/classG4DNAWaterExcitationStructure.html", null ],
    [ "G4DNAWaterIonisationStructure", "d1/d1e/classG4DNAWaterIonisationStructure.html", null ],
    [ "G4DopplerProfile", "df/dfb/classG4DopplerProfile.html", null ],
    [ "G4DrawVoxels", "d7/d0d/classG4DrawVoxels.html", null ],
    [ "G4DriverReporter", "d9/d96/classG4DriverReporter.html", null ],
    [ "G4DummyThread", "d2/d0f/classG4DummyThread.html", null ],
    [ "G4DynamicParticle", "d7/d5c/classG4DynamicParticle.html", null ],
    [ "G4Ec2sub", "db/dff/classG4Ec2sub.html", null ],
    [ "G4Ecld", "d2/ddd/classG4Ecld.html", null ],
    [ "G4Facet::G4Edge", "d1/d30/structG4Facet_1_1G4Edge.html", null ],
    [ "G4eDPWAElasticDCS", "d3/d8f/classG4eDPWAElasticDCS.html", null ],
    [ "G4eeCrossSections", "d3/d01/classG4eeCrossSections.html", null ],
    [ "G4Eenuc", "d5/dfa/classG4Eenuc.html", null ],
    [ "G4eIonisationParameters", "df/d4b/classG4eIonisationParameters.html", null ],
    [ "G4ElasticData", "d1/d0e/classG4ElasticData.html", null ],
    [ "G4ElasticHNScattering", "d5/dae/classG4ElasticHNScattering.html", null ],
    [ "G4ElectronIonPair", "d2/d2a/classG4ElectronIonPair.html", null ],
    [ "G4ElectronOccupancy", "d7/d12/classG4ElectronOccupancy.html", null ],
    [ "G4Element", "de/d80/classG4Element.html", null ],
    [ "G4ElementData", "d9/d8f/classG4ElementData.html", null ],
    [ "G4ElementSelector", "de/deb/classG4ElementSelector.html", null ],
    [ "G4EmBiasingManager", "db/d69/classG4EmBiasingManager.html", null ],
    [ "G4EmBuilder", "d7/df3/classG4EmBuilder.html", null ],
    [ "G4EmCalculator", "de/dc4/classG4EmCalculator.html", null ],
    [ "G4EmConfigurator", "d9/df6/classG4EmConfigurator.html", null ],
    [ "G4EmCorrections", "d7/db3/classG4EmCorrections.html", null ],
    [ "G4EmDataHandler", "d3/d44/classG4EmDataHandler.html", null ],
    [ "G4EMDissociationSpectrum", "dc/da1/classG4EMDissociationSpectrum.html", null ],
    [ "G4EmElementSelector", "d6/d2a/classG4EmElementSelector.html", null ],
    [ "G4EmExtraParameters", "dc/d11/classG4EmExtraParameters.html", null ],
    [ "G4EmLowEParameters", "d4/d86/classG4EmLowEParameters.html", null ],
    [ "G4EmModelActivator", "dd/db2/classG4EmModelActivator.html", null ],
    [ "G4EmModelManager", "dd/de5/classG4EmModelManager.html", null ],
    [ "G4EmParameters", "dc/d54/classG4EmParameters.html", null ],
    [ "G4EmParticleList", "d4/d66/classG4EmParticleList.html", null ],
    [ "G4EmSaturation", "d5/d9b/classG4EmSaturation.html", null ],
    [ "G4enable_shared_from_this", null, [
      [ "G4ITReaction", "da/d15/classG4ITReaction.html", null ],
      [ "G4ITReactionPerTrack", "d3/d3c/classG4ITReactionPerTrack.html", null ],
      [ "G4MoleculeShoot", "d3/d86/classG4MoleculeShoot.html", [
        [ "TG4MoleculeShoot< TYPE >", "db/dac/classTG4MoleculeShoot.html", null ]
      ] ]
    ] ],
    [ "G4EnclosingCylinder", "dd/db8/classG4EnclosingCylinder.html", null ],
    [ "G4ENDFTapeRead", "d6/d7b/classG4ENDFTapeRead.html", null ],
    [ "G4ENDFYieldDataContainer", "d3/da3/classG4ENDFYieldDataContainer.html", null ],
    [ "G4EnergyLossForExtrapolator", "da/de2/classG4EnergyLossForExtrapolator.html", null ],
    [ "G4EnergyLossTables", "d1/d32/classG4EnergyLossTables.html", null ],
    [ "G4EnergyLossTablesHelper", "d6/d14/classG4EnergyLossTablesHelper.html", null ],
    [ "G4EnergyRangeManager", "d0/d09/classG4EnergyRangeManager.html", null ],
    [ "G4EnergySplitter", "da/d76/classG4EnergySplitter.html", null ],
    [ "G4EnvSettings", "d2/db2/classG4EnvSettings.html", null ],
    [ "G4EquationOfMotion", "d0/ddc/classG4EquationOfMotion.html", [
      [ "G4EqEMFieldWithEDM", "dc/db8/classG4EqEMFieldWithEDM.html", null ],
      [ "G4EqEMFieldWithSpin", "de/dd6/classG4EqEMFieldWithSpin.html", null ],
      [ "G4EqGravityField", "d1/d33/classG4EqGravityField.html", null ],
      [ "G4EqMagElectricField", "d0/d6f/classG4EqMagElectricField.html", null ],
      [ "G4Mag_EqRhs", "db/d59/classG4Mag__EqRhs.html", [
        [ "G4KM_NucleonEqRhs", "dc/d81/classG4KM__NucleonEqRhs.html", null ],
        [ "G4KM_OpticalEqRhs", "d2/dcb/classG4KM__OpticalEqRhs.html", null ],
        [ "G4Mag_SpinEqRhs", "dd/d32/classG4Mag__SpinEqRhs.html", null ],
        [ "G4Mag_UsualEqRhs", "d7/d85/classG4Mag__UsualEqRhs.html", [
          [ "G4ErrorMag_UsualEqRhs", "da/d48/classG4ErrorMag__UsualEqRhs.html", null ],
          [ "G4TMagFieldEquation< T_Field >", "dc/d51/classG4TMagFieldEquation.html", null ]
        ] ]
      ] ],
      [ "G4MonopoleEq", "d8/d77/classG4MonopoleEq.html", null ],
      [ "G4RepleteEofM", "dd/d4f/classG4RepleteEofM.html", null ]
    ] ],
    [ "G4ErrorFreeTrajParam", "d5/d3e/classG4ErrorFreeTrajParam.html", null ],
    [ "G4ErrorFunction", "db/d62/classG4ErrorFunction.html", null ],
    [ "G4ErrorMatrix", "dc/db6/classG4ErrorMatrix.html", null ],
    [ "G4ErrorMatrix::G4ErrorMatrix_row", "d0/d44/classG4ErrorMatrix_1_1G4ErrorMatrix__row.html", null ],
    [ "G4ErrorMatrix::G4ErrorMatrix_row_const", "dd/d61/classG4ErrorMatrix_1_1G4ErrorMatrix__row__const.html", null ],
    [ "G4ErrorPropagator", "d7/de8/classG4ErrorPropagator.html", null ],
    [ "G4ErrorPropagatorData", "da/d6d/classG4ErrorPropagatorData.html", null ],
    [ "G4ErrorPropagatorManager", "dd/d88/classG4ErrorPropagatorManager.html", null ],
    [ "G4ErrorRunManagerHelper", "d8/db0/classG4ErrorRunManagerHelper.html", null ],
    [ "G4ErrorSurfaceTrajParam", "d4/d80/classG4ErrorSurfaceTrajParam.html", null ],
    [ "G4ErrorSymMatrix", "d1/d85/classG4ErrorSymMatrix.html", null ],
    [ "G4ErrorSymMatrix::G4ErrorSymMatrix_row", "de/d27/classG4ErrorSymMatrix_1_1G4ErrorSymMatrix__row.html", null ],
    [ "G4ErrorSymMatrix::G4ErrorSymMatrix_row_const", "d1/d49/classG4ErrorSymMatrix_1_1G4ErrorSymMatrix__row__const.html", null ],
    [ "G4ErrorTarget", "d2/dd4/classG4ErrorTarget.html", [
      [ "G4ErrorGeomVolumeTarget", "d7/d21/classG4ErrorGeomVolumeTarget.html", null ],
      [ "G4ErrorTanPlaneTarget", "dc/dfc/classG4ErrorTanPlaneTarget.html", [
        [ "G4ErrorSurfaceTarget", "df/dd7/classG4ErrorSurfaceTarget.html", [
          [ "G4ErrorCylSurfaceTarget", "d6/d86/classG4ErrorCylSurfaceTarget.html", null ],
          [ "G4ErrorPlaneSurfaceTarget", "df/de6/classG4ErrorPlaneSurfaceTarget.html", null ]
        ] ]
      ] ],
      [ "G4ErrorTrackLengthTarget", "d6/d41/classG4ErrorTrackLengthTarget.html", null ]
    ] ],
    [ "G4ErrorTrajState", "d5/dbb/classG4ErrorTrajState.html", [
      [ "G4ErrorFreeTrajState", "de/d15/classG4ErrorFreeTrajState.html", null ],
      [ "G4ErrorSurfaceTrajState", "d8/d3c/classG4ErrorSurfaceTrajState.html", null ]
    ] ],
    [ "G4ESTARStopping", "da/daf/classG4ESTARStopping.html", null ],
    [ "G4Event", "d5/de2/classG4Event.html", null ],
    [ "G4EventManager", "da/d44/classG4EventManager.html", null ],
    [ "G4ExcitationHandler", "d4/d72/classG4ExcitationHandler.html", null ],
    [ "G4ExcitedBaryonConstructor", "d7/d1a/classG4ExcitedBaryonConstructor.html", [
      [ "G4ExcitedDeltaConstructor", "d3/d3e/classG4ExcitedDeltaConstructor.html", null ],
      [ "G4ExcitedLambdaConstructor", "dc/d14/classG4ExcitedLambdaConstructor.html", null ],
      [ "G4ExcitedNucleonConstructor", "d5/d3f/classG4ExcitedNucleonConstructor.html", null ],
      [ "G4ExcitedSigmaConstructor", "df/d12/classG4ExcitedSigmaConstructor.html", null ],
      [ "G4ExcitedXiConstructor", "d1/db6/classG4ExcitedXiConstructor.html", null ]
    ] ],
    [ "G4ExcitedMesonConstructor", "d2/db9/classG4ExcitedMesonConstructor.html", null ],
    [ "G4ExcitedString", "d1/d5e/classG4ExcitedString.html", null ],
    [ "G4ExitNormal", "d0/d94/structG4ExitNormal.html", null ],
    [ "G4ExitonConfiguration", "dd/de8/classG4ExitonConfiguration.html", null ],
    [ "G4Facet", "dc/d09/classG4Facet.html", null ],
    [ "G4FakeParticleID", "d1/d51/classG4FakeParticleID.html", null ],
    [ "G4Fancy3DNucleusHelper", "d7/df4/classG4Fancy3DNucleusHelper.html", null ],
    [ "G4FastHit", "d8/d90/classG4FastHit.html", null ],
    [ "G4FastList< OBJECT >", "d4/df2/classG4FastList.html", null ],
    [ "G4FastList< G4FastList< G4Track > >", "d4/df2/classG4FastList.html", null ],
    [ "G4FastList< G4FastList< OBJECT > >", "d4/df2/classG4FastList.html", null ],
    [ "G4FastList< G4Track >", "d4/df2/classG4FastList.html", null ],
    [ "G4FastList_Boundary< OBJECT >", "d9/d0f/classG4FastList__Boundary.html", null ],
    [ "G4FastList_const_iterator< OBJECT >", "d1/ddb/structG4FastList__const__iterator.html", null ],
    [ "G4FastList_iterator< OBJECT >", "df/d20/structG4FastList__iterator.html", null ],
    [ "G4FastListNode< OBJECT >", "db/d48/classG4FastListNode.html", null ],
    [ "G4FastListNode< G4FastList< G4FastList< G4Track > > >", "db/d48/classG4FastListNode.html", null ],
    [ "G4FastListNode< G4FastList< G4FastList< OBJECT > > >", "db/d48/classG4FastListNode.html", null ],
    [ "G4FastListNode< G4FastList< G4Track > >", "db/d48/classG4FastListNode.html", null ],
    [ "G4FastListNode< G4FastList< OBJECT > >", "db/d48/classG4FastListNode.html", null ],
    [ "G4FastListNode< G4Track >", "db/d48/classG4FastListNode.html", null ],
    [ "G4FastSimHitMaker", "df/d27/classG4FastSimHitMaker.html", null ],
    [ "G4FastSimulationHelper", "d4/d41/classG4FastSimulationHelper.html", null ],
    [ "G4FastSimulationManager", "d1/daa/classG4FastSimulationManager.html", null ],
    [ "G4FastTrack", "d6/d25/classG4FastTrack.html", null ],
    [ "G4FastVector< Type, N >", "d4/d53/classG4FastVector.html", null ],
    [ "G4Fb", "d5/d16/classG4Fb.html", null ],
    [ "G4FermiChannels", "d3/d47/classG4FermiChannels.html", null ],
    [ "G4FermiDecayProbability", "d0/d7c/classG4FermiDecayProbability.html", null ],
    [ "G4FermiFragment", "d7/dbf/classG4FermiFragment.html", null ],
    [ "G4FermiFragmentsPoolVI", "df/dee/classG4FermiFragmentsPoolVI.html", null ],
    [ "G4FermiMomentum", "d9/df8/classG4FermiMomentum.html", null ],
    [ "G4FermiPair", "de/d9a/classG4FermiPair.html", null ],
    [ "G4FermiPhaseSpaceDecay", "d3/dbf/classG4FermiPhaseSpaceDecay.html", null ],
    [ "G4Field", "d6/d64/classG4Field.html", [
      [ "G4ElectroMagneticField", "de/d7b/classG4ElectroMagneticField.html", [
        [ "G4ElectricField", "d7/de4/classG4ElectricField.html", [
          [ "G4UniformElectricField", "db/d5b/classG4UniformElectricField.html", null ]
        ] ]
      ] ],
      [ "G4MagneticField", "d8/d9d/classG4MagneticField.html", [
        [ "G4CachedMagneticField", "de/d08/classG4CachedMagneticField.html", null ],
        [ "G4DELPHIMagField", "d0/d07/classG4DELPHIMagField.html", null ],
        [ "G4HarmonicPolMagField", "d8/d26/classG4HarmonicPolMagField.html", null ],
        [ "G4KM_DummyField", "d5/d0d/classG4KM__DummyField.html", null ],
        [ "G4LineCurrentMagField", "dc/dfa/classG4LineCurrentMagField.html", null ],
        [ "G4QuadrupoleMagField", "d9/d2f/classG4QuadrupoleMagField.html", null ],
        [ "G4SextupoleMagField", "dc/d70/classG4SextupoleMagField.html", null ],
        [ "G4TCachedMagneticField< T_Field >", "d3/dcf/classG4TCachedMagneticField.html", null ],
        [ "G4TQuadrupoleMagField", "d3/d9c/classG4TQuadrupoleMagField.html", null ],
        [ "G4TUniformMagneticField", "d3/dcb/classG4TUniformMagneticField.html", null ],
        [ "G4UniformMagField", "d5/d05/classG4UniformMagField.html", null ]
      ] ],
      [ "G4UniformGravityField", "de/da9/classG4UniformGravityField.html", null ]
    ] ],
    [ "G4FieldManager", "dc/d5a/classG4FieldManager.html", null ],
    [ "G4FieldPropagation", "de/dcf/classG4FieldPropagation.html", [
      [ "G4RKFieldIntegrator", "db/dfc/classG4RKFieldIntegrator.html", null ]
    ] ],
    [ "G4FieldTrack", "d5/d40/classG4FieldTrack.html", null ],
    [ "G4FieldTrackUpdator", "dd/da5/classG4FieldTrackUpdator.html", null ],
    [ "G4FileUtilities", "d5/d78/classG4FileUtilities.html", null ],
    [ "G4Fiss", "d5/d52/classG4Fiss.html", null ],
    [ "G4FissionConfiguration", "d1/d2a/classG4FissionConfiguration.html", null ],
    [ "G4fissionEvent", "dd/dfa/classG4fissionEvent.html", null ],
    [ "G4FissionFragmentGenerator", "de/da4/classG4FissionFragmentGenerator.html", null ],
    [ "G4FissionParameters", "d9/df6/classG4FissionParameters.html", null ],
    [ "G4FissionProductYieldDist", "d0/df8/classG4FissionProductYieldDist.html", [
      [ "G4FPYBiasedLightFragmentDist", "d5/dd8/classG4FPYBiasedLightFragmentDist.html", null ],
      [ "G4FPYNormalFragmentDist", "d7/d7f/classG4FPYNormalFragmentDist.html", null ]
    ] ],
    [ "G4FissionStore", "da/d76/classG4FissionStore.html", null ],
    [ "G4FluoData", "d4/dec/classG4FluoData.html", null ],
    [ "G4FluoTransition", "d5/d21/classG4FluoTransition.html", null ],
    [ "G4ForEach< group >", "de/d85/classG4ForEach.html", null ],
    [ "G4ForEach< G4Terminator >", "de/d35/classG4ForEach_3_01G4Terminator_01_4.html", null ],
    [ "G4FPYSamplingOps", "d4/d9c/classG4FPYSamplingOps.html", null ],
    [ "G4Fragment", "df/d5d/classG4Fragment.html", null ],
    [ "G4FragmentingString", "d7/d46/classG4FragmentingString.html", null ],
    [ "G4FRofstream", "da/df5/classG4FRofstream.html", null ],
    [ "G4FTFAnnihilation", "d8/dd5/classG4FTFAnnihilation.html", null ],
    [ "G4FTFParamCollection", "d9/dda/classG4FTFParamCollection.html", [
      [ "G4FTFParamCollBaryonProj", "db/d28/classG4FTFParamCollBaryonProj.html", null ],
      [ "G4FTFParamCollMesonProj", "d1/d09/classG4FTFParamCollMesonProj.html", [
        [ "G4FTFParamCollPionProj", "d3/d0e/classG4FTFParamCollPionProj.html", null ]
      ] ]
    ] ],
    [ "G4FTFParameters", "dc/d81/classG4FTFParameters.html", null ],
    [ "G4FTFSettingDefaultHDP", "dc/dd7/classG4FTFSettingDefaultHDP.html", null ],
    [ "G4GammaTransition", "df/d18/classG4GammaTransition.html", null ],
    [ "G4GDecay3", "d2/d78/classG4GDecay3.html", null ],
    [ "G4GDMLAuxStructType", "d8/d76/structG4GDMLAuxStructType.html", null ],
    [ "G4GDMLEvaluator", "d8/d36/classG4GDMLEvaluator.html", null ],
    [ "G4GDMLMatrix", "d7/dc6/classG4GDMLMatrix.html", null ],
    [ "G4GDMLParser", "d5/dc4/classG4GDMLParser.html", null ],
    [ "G4GDMLRead", "d3/d87/classG4GDMLRead.html", [
      [ "G4GDMLReadDefine", "db/d76/classG4GDMLReadDefine.html", [
        [ "G4GDMLReadMaterials", "db/d87/classG4GDMLReadMaterials.html", [
          [ "G4GDMLReadSolids", "d4/db9/classG4GDMLReadSolids.html", [
            [ "G4GDMLReadSetup", "de/db8/classG4GDMLReadSetup.html", [
              [ "G4GDMLReadParamvol", "d0/d28/classG4GDMLReadParamvol.html", [
                [ "G4GDMLReadStructure", "d6/db8/classG4GDMLReadStructure.html", null ]
              ] ]
            ] ]
          ] ]
        ] ]
      ] ]
    ] ],
    [ "G4GDMLWrite", "d8/dfc/classG4GDMLWrite.html", [
      [ "G4GDMLWriteDefine", "da/d75/classG4GDMLWriteDefine.html", [
        [ "G4GDMLWriteMaterials", "d6/dc2/classG4GDMLWriteMaterials.html", [
          [ "G4GDMLWriteSolids", "d3/d4a/classG4GDMLWriteSolids.html", [
            [ "G4GDMLWriteSetup", "dc/d52/classG4GDMLWriteSetup.html", [
              [ "G4GDMLWriteParamvol", "d8/dba/classG4GDMLWriteParamvol.html", [
                [ "G4GDMLWriteStructure", "d4/ded/classG4GDMLWriteStructure.html", null ]
              ] ]
            ] ]
          ] ]
        ] ]
      ] ]
    ] ],
    [ "G4GeneralParticleSourceData", "d6/d54/classG4GeneralParticleSourceData.html", null ],
    [ "G4GeometryCell", "da/dca/classG4GeometryCell.html", null ],
    [ "G4GeometryCellComp", "d2/d11/classG4GeometryCellComp.html", null ],
    [ "G4GeometryCellStep", "d3/d78/classG4GeometryCellStep.html", null ],
    [ "G4GeometryManager", "dd/db4/classG4GeometryManager.html", null ],
    [ "G4GeometryTolerance", "d5/dd3/classG4GeometryTolerance.html", null ],
    [ "G4GeometryWorkspace", "d6/d0e/classG4GeometryWorkspace.html", null ],
    [ "G4GeomSplitter< T >", "d2/d40/classG4GeomSplitter.html", null ],
    [ "G4GeomTestVolume", "d9/d67/classG4GeomTestVolume.html", null ],
    [ "G4GeomTools", "d4/d35/classG4GeomTools.html", null ],
    [ "G4GFlashSpot", "d3/d08/classG4GFlashSpot.html", null ],
    [ "G4GIDI", "d7/dbb/classG4GIDI.html", null ],
    [ "G4GIDI_map", "d6/def/classG4GIDI__map.html", null ],
    [ "G4GIDI_Product_s", "dc/d76/structG4GIDI__Product__s.html", null ],
    [ "G4GIDI_target", "d6/d19/classG4GIDI__target.html", null ],
    [ "G4GlobalFastSimulationManager", "de/d3b/classG4GlobalFastSimulationManager.html", null ],
    [ "G4GMocrenFileCTtoDensityMap", "d5/da1/classG4GMocrenFileCTtoDensityMap.html", null ],
    [ "G4GMocrenIO", "d4/de1/classG4GMocrenIO.html", null ],
    [ "G4GoudsmitSaundersonTable", "d6/de3/classG4GoudsmitSaundersonTable.html", null ],
    [ "G4GSMottCorrection", "d2/dc5/classG4GSMottCorrection.html", null ],
    [ "G4GSPWACorrections", "dd/d45/classG4GSPWACorrections.html", null ],
    [ "G4HadDecayGenerator", "d2/dbd/classG4HadDecayGenerator.html", [
      [ "G4CascadeFinalStateGenerator", "dd/da3/classG4CascadeFinalStateGenerator.html", null ]
    ] ],
    [ "G4HadElementSelector", "d8/da8/classG4HadElementSelector.html", null ],
    [ "G4HadFinalState", "dd/d4a/classG4HadFinalState.html", null ],
    [ "G4HadParticles", "de/d27/classG4HadParticles.html", null ],
    [ "G4HadProcesses", "d9/d49/classG4HadProcesses.html", null ],
    [ "G4HadProjectile", "da/d11/classG4HadProjectile.html", null ],
    [ "G4HadronBuilder", "d0/d2a/classG4HadronBuilder.html", null ],
    [ "G4HadronicBuilder", "dd/dd0/classG4HadronicBuilder.html", null ],
    [ "G4HadronicDeveloperParameters", "da/d59/classG4HadronicDeveloperParameters.html", null ],
    [ "G4HadronicInteraction", "db/d92/classG4HadronicInteraction.html", [
      [ "G4BinaryLightIonReaction", "dd/d18/classG4BinaryLightIonReaction.html", null ],
      [ "G4CRCoalescence", "dc/d5c/classG4CRCoalescence.html", null ],
      [ "G4ChargeExchange", "d8/dfa/classG4ChargeExchange.html", null ],
      [ "G4EMDissociation", "d0/d33/classG4EMDissociation.html", null ],
      [ "G4ElectroVDNuclearModel", "d8/d6b/classG4ElectroVDNuclearModel.html", null ],
      [ "G4EmCaptureCascade", "dc/d3f/classG4EmCaptureCascade.html", null ],
      [ "G4FissLib", "da/dd1/classG4FissLib.html", null ],
      [ "G4HadronElastic", "dd/dd9/classG4HadronElastic.html", [
        [ "G4AntiNuclElastic", "d0/dcf/classG4AntiNuclElastic.html", null ],
        [ "G4ChipsElasticModel", "d3/d1a/classG4ChipsElasticModel.html", null ],
        [ "G4DiffuseElastic", "dd/d0b/classG4DiffuseElastic.html", null ],
        [ "G4DiffuseElasticV2", "df/d46/classG4DiffuseElasticV2.html", null ],
        [ "G4ElasticHadrNucleusHE", "db/d8f/classG4ElasticHadrNucleusHE.html", null ],
        [ "G4LEHadronProtonElastic", "d5/de5/classG4LEHadronProtonElastic.html", null ],
        [ "G4LEnp", "d8/d2c/classG4LEnp.html", null ],
        [ "G4LEpp", "da/dfc/classG4LEpp.html", null ],
        [ "G4LowEHadronElastic", "dc/d1b/classG4LowEHadronElastic.html", null ],
        [ "G4NeutrinoElectronNcModel", "d7/d73/classG4NeutrinoElectronNcModel.html", null ],
        [ "G4NeutronElectronElModel", "d8/d88/classG4NeutronElectronElModel.html", null ],
        [ "G4NuclNuclDiffuseElastic", "d9/d47/classG4NuclNuclDiffuseElastic.html", null ],
        [ "G4hhElastic", "d3/d61/classG4hhElastic.html", null ]
      ] ],
      [ "G4LENDModel", "dd/d3e/classG4LENDModel.html", [
        [ "G4LENDCapture", "db/d6b/classG4LENDCapture.html", null ],
        [ "G4LENDCombinedModel", "d7/d8e/classG4LENDCombinedModel.html", null ],
        [ "G4LENDElastic", "da/d8a/classG4LENDElastic.html", null ],
        [ "G4LENDFission", "dd/de9/classG4LENDFission.html", null ],
        [ "G4LENDGammaModel", "de/d6b/classG4LENDGammaModel.html", null ],
        [ "G4LENDInelastic", "d0/d2f/classG4LENDInelastic.html", null ],
        [ "G4LENDorBERTModel", "d7/d0d/classG4LENDorBERTModel.html", null ]
      ] ],
      [ "G4LFission", "dd/d0f/classG4LFission.html", null ],
      [ "G4LMsdGenerator", "d6/d6c/classG4LMsdGenerator.html", null ],
      [ "G4LowEGammaNuclearModel", "d2/dda/classG4LowEGammaNuclearModel.html", null ],
      [ "G4LowEIonFragmentation", "d4/d56/classG4LowEIonFragmentation.html", null ],
      [ "G4MuMinusCapturePrecompound", "da/d22/classG4MuMinusCapturePrecompound.html", null ],
      [ "G4MuonMinusBoundDecay", "d7/d56/classG4MuonMinusBoundDecay.html", null ],
      [ "G4MuonVDNuclearModel", "d5/da9/classG4MuonVDNuclearModel.html", null ],
      [ "G4NeutrinoElectronCcModel", "dc/d3c/classG4NeutrinoElectronCcModel.html", null ],
      [ "G4NeutrinoNucleusModel", "d6/de8/classG4NeutrinoNucleusModel.html", [
        [ "G4ANuElNucleusCcModel", "d2/d07/classG4ANuElNucleusCcModel.html", null ],
        [ "G4ANuElNucleusNcModel", "de/d4c/classG4ANuElNucleusNcModel.html", null ],
        [ "G4ANuMuNucleusCcModel", "d2/d9c/classG4ANuMuNucleusCcModel.html", null ],
        [ "G4ANuMuNucleusNcModel", "d7/d10/classG4ANuMuNucleusNcModel.html", null ],
        [ "G4NuElNucleusCcModel", "d0/d7c/classG4NuElNucleusCcModel.html", null ],
        [ "G4NuElNucleusNcModel", "dd/d3a/classG4NuElNucleusNcModel.html", null ],
        [ "G4NuMuNucleusCcModel", "d7/d09/classG4NuMuNucleusCcModel.html", null ],
        [ "G4NuMuNucleusNcModel", "da/da7/classG4NuMuNucleusNcModel.html", null ]
      ] ],
      [ "G4NeutronRadCapture", "d3/d61/classG4NeutronRadCapture.html", null ],
      [ "G4ParticleHPCapture", "db/d3c/classG4ParticleHPCapture.html", null ],
      [ "G4ParticleHPElastic", "d1/d0d/classG4ParticleHPElastic.html", null ],
      [ "G4ParticleHPFission", "d5/dbc/classG4ParticleHPFission.html", null ],
      [ "G4ParticleHPInelastic", "df/d75/classG4ParticleHPInelastic.html", null ],
      [ "G4ParticleHPThermalScattering", "d0/dfe/classG4ParticleHPThermalScattering.html", null ],
      [ "G4QMDReaction", "d4/d51/classG4QMDReaction.html", null ],
      [ "G4QuasiElasticChannel", "d9/db3/classG4QuasiElasticChannel.html", null ],
      [ "G4TheoFSGenerator", "d4/d28/classG4TheoFSGenerator.html", null ],
      [ "G4VHighEnergyGenerator", "d0/d72/classG4VHighEnergyGenerator.html", [
        [ "G4StringModel", "da/df3/classG4StringModel.html", null ],
        [ "G4VPartonStringModel", "d4/d1a/classG4VPartonStringModel.html", [
          [ "G4QGSModel< G4GammaParticipants >", "df/d4a/classG4QGSModel.html", null ],
          [ "G4FTFModel", "dd/dbb/classG4FTFModel.html", null ],
          [ "G4QGSModel< ParticipantType >", "df/d4a/classG4QGSModel.html", null ]
        ] ]
      ] ],
      [ "G4VIntraNuclearTransportModel", "d2/d50/classG4VIntraNuclearTransportModel.html", [
        [ "G4BinaryCascade", "d0/d72/classG4BinaryCascade.html", null ],
        [ "G4CascadeInterface", "dc/d8b/classG4CascadeInterface.html", null ],
        [ "G4GeneratorPrecompoundInterface", "d9/d06/classG4GeneratorPrecompoundInterface.html", null ],
        [ "G4INCLXXInterface", "d8/d9d/classG4INCLXXInterface.html", null ]
      ] ],
      [ "G4VLongitudinalStringDecay", "d5/d97/classG4VLongitudinalStringDecay.html", [
        [ "G4LundStringFragmentation", "d9/d84/classG4LundStringFragmentation.html", null ],
        [ "G4QGSMFragmentation", "d4/dc9/classG4QGSMFragmentation.html", null ]
      ] ],
      [ "G4VPreCompoundModel", "d3/de7/classG4VPreCompoundModel.html", [
        [ "G4AblaInterface", "df/dee/classG4AblaInterface.html", null ],
        [ "G4PreCompoundModel", "d7/dea/classG4PreCompoundModel.html", null ]
      ] ],
      [ "G4VStringFragmentation", "dc/d7a/classG4VStringFragmentation.html", [
        [ "G4ExcitedStringDecay", "d9/d44/classG4ExcitedStringDecay.html", null ]
      ] ],
      [ "G4WilsonAbrasionModel", "d7/d1d/classG4WilsonAbrasionModel.html", null ]
    ] ],
    [ "G4HadronicInteractionRegistry", "d8/d46/classG4HadronicInteractionRegistry.html", null ],
    [ "G4HadronicParameters", "db/dcf/classG4HadronicParameters.html", null ],
    [ "G4HadronicProcessStore", "da/d41/classG4HadronicProcessStore.html", null ],
    [ "G4HadronNucleonXsc", "d8/d11/classG4HadronNucleonXsc.html", null ],
    [ "G4HadronXSDataTable", "d7/d6b/classG4HadronXSDataTable.html", null ],
    [ "G4HadSecondary", "db/d85/classG4HadSecondary.html", null ],
    [ "G4HCIOcatalog", "db/da5/classG4HCIOcatalog.html", null ],
    [ "G4HCofThisEvent", "d7/db4/classG4HCofThisEvent.html", null ],
    [ "G4HCtable", "de/dd7/classG4HCtable.html", null ],
    [ "G4HEPEvtParticle", "da/de2/classG4HEPEvtParticle.html", null ],
    [ "G4HepRepFileXMLWriter", "d1/df2/classG4HepRepFileXMLWriter.html", null ],
    [ "G4HnDimensionInformation", "dc/d23/structG4HnDimensionInformation.html", null ],
    [ "G4HnInformation", "d2/d9d/classG4HnInformation.html", null ],
    [ "G4HuffmanCodeTable", "d6/d5b/structG4HuffmanCodeTable.html", null ],
    [ "G4HyperNucleiProperties", "d0/dac/classG4HyperNucleiProperties.html", null ],
    [ "G4ICRU90StoppingData", "d9/d2c/classG4ICRU90StoppingData.html", null ],
    [ "G4IDataSet", "d4/d4d/classG4IDataSet.html", [
      [ "G4CompositeDataSet", "df/dc0/classG4CompositeDataSet.html", null ],
      [ "G4DataSet", "d9/dba/classG4DataSet.html", null ],
      [ "G4PixeShellDataSet", "d1/df1/classG4PixeShellDataSet.html", null ]
    ] ],
    [ "G4IInterpolator", "de/d14/classG4IInterpolator.html", [
      [ "G4LinInterpolator", "df/d4c/classG4LinInterpolator.html", null ],
      [ "G4LogLogInterpolator", "d9/db2/classG4LogLogInterpolator.html", null ]
    ] ],
    [ "G4INCLXXInterfaceStore", "d8/d50/classG4INCLXXInterfaceStore.html", null ],
    [ "G4INCLXXVInterfaceTally", "de/d72/classG4INCLXXVInterfaceTally.html", null ],
    [ "G4IndexError", "d5/d3e/classG4IndexError.html", null ],
    [ "G4InitXscPAI", "d8/d55/classG4InitXscPAI.html", null ],
    [ "G4Integrator< T, F >", "d5/d82/classG4Integrator.html", null ],
    [ "G4InteractionCase", "d6/de2/classG4InteractionCase.html", null ],
    [ "G4InteractionContent", "d9/d85/classG4InteractionContent.html", null ],
    [ "G4InterpolationIterator", "dd/d83/classG4InterpolationIterator.html", null ],
    [ "G4InterpolationManager", "dc/d8c/classG4InterpolationManager.html", null ],
    [ "G4IntersectingCone", "d1/d98/classG4IntersectingCone.html", null ],
    [ "G4IntGrp< t1, i2, t3 >", "d5/da3/classG4IntGrp.html", null ],
    [ "G4InuclParticle", "d9/da2/classG4InuclParticle.html", [
      [ "G4InuclElementaryParticle", "d2/d5e/classG4InuclElementaryParticle.html", null ],
      [ "G4InuclNuclei", "d6/dbd/classG4InuclNuclei.html", null ]
    ] ],
    [ "G4IonConstructor", "d3/d46/classG4IonConstructor.html", null ],
    [ "G4IonCoulombCrossSection", "dd/db8/classG4IonCoulombCrossSection.html", null ],
    [ "G4IonDEDXHandler", "d9/dbc/classG4IonDEDXHandler.html", null ],
    [ "G4ionEffectiveCharge", "da/d34/classG4ionEffectiveCharge.html", null ],
    [ "G4IonICRU73Data", "d8/d56/classG4IonICRU73Data.html", null ],
    [ "G4IonisParamElm", "da/dd8/classG4IonisParamElm.html", null ],
    [ "G4IonisParamMat", "d6/d69/classG4IonisParamMat.html", null ],
    [ "G4IonTable", "da/d2b/classG4IonTable.html", null ],
    [ "G4IosFlagsSaver", "de/db5/classG4IosFlagsSaver.html", null ],
    [ "G4IRTUtils", "de/ddc/classG4IRTUtils.html", null ],
    [ "G4Isotope", "d8/d66/classG4Isotope.html", null ],
    [ "G4IsotopeProperty", "d2/d12/classG4IsotopeProperty.html", null ],
    [ "G4ITBox", "d7/d9e/classG4ITBox.html", null ],
    [ "G4ITGun", "dc/d90/classG4ITGun.html", [
      [ "G4MoleculeGun", "df/d63/classG4MoleculeGun.html", null ]
    ] ],
    [ "G4ITLeadingTracks", "d7/d8b/classG4ITLeadingTracks.html", null ],
    [ "G4ITModelHandler", "d6/dcb/classG4ITModelHandler.html", null ],
    [ "G4ITModelManager", "d5/df0/classG4ITModelManager.html", null ],
    [ "G4ITModelProcessor", "d4/d21/classG4ITModelProcessor.html", null ],
    [ "G4ITNavigator", null, [
      [ "G4ITMultiNavigator", "db/d5f/classG4ITMultiNavigator.html", null ]
    ] ],
    [ "G4ITNavigator1", "dd/dc9/classG4ITNavigator1.html", null ],
    [ "G4ITNavigator2", "d2/d29/classG4ITNavigator2.html", null ],
    [ "G4ITNavigatorState_Lock1", "d9/d5b/structG4ITNavigatorState__Lock1.html", [
      [ "G4ITNavigator1::G4SaveNavigatorState", "d3/da7/structG4ITNavigator1_1_1G4SaveNavigatorState.html", null ]
    ] ],
    [ "G4ITNavigatorState_Lock2", "d8/d5b/structG4ITNavigatorState__Lock2.html", [
      [ "G4ITNavigator2::G4NavigatorState", "d7/dad/structG4ITNavigator2_1_1G4NavigatorState.html", null ]
    ] ],
    [ "G4ITReactionChange", "d5/dd8/classG4ITReactionChange.html", null ],
    [ "G4ITReactionSet", "dd/dbb/classG4ITReactionSet.html", null ],
    [ "G4ITReactionTable", "de/deb/classG4ITReactionTable.html", [
      [ "G4DNAMolecularReactionTable", "d4/dbd/classG4DNAMolecularReactionTable.html", null ]
    ] ],
    [ "G4ITStepProcessor", "d0/d59/classG4ITStepProcessor.html", null ],
    [ "G4ITStepProcessorState_Lock", "df/d81/classG4ITStepProcessorState__Lock.html", [
      [ "G4ITStepProcessorState", "da/df8/classG4ITStepProcessorState.html", null ]
    ] ],
    [ "G4ITTrackingInteractivity", "d4/d3b/classG4ITTrackingInteractivity.html", null ],
    [ "G4ITTrackingManager", "d0/d1d/classG4ITTrackingManager.html", null ],
    [ "G4ITTransportationManager", "da/dcd/classG4ITTransportationManager.html", null ],
    [ "G4ITType", "da/def/structG4ITType.html", null ],
    [ "G4ITTypeManager", "de/d1c/classG4ITTypeManager.html", null ],
    [ "G4JpegCoder", "dc/d4c/classG4JpegCoder.html", null ],
    [ "G4JpegProperty", "d4/d94/structG4JpegProperty.html", null ],
    [ "G4JTPolynomialSolver", "d0/dad/classG4JTPolynomialSolver.html", null ],
    [ "G4KalbachCrossSection", "d7/da7/classG4KalbachCrossSection.html", null ],
    [ "G4KDMap", "d2/d17/classG4KDMap.html", null ],
    [ "G4KDNode_Base", "df/d17/classG4KDNode__Base.html", [
      [ "G4KDNode< PointT >", "da/d63/classG4KDNode.html", null ],
      [ "G4KDNodeCopy< PointCopyT >", "dc/d99/classG4KDNodeCopy.html", null ]
    ] ],
    [ "G4KDTree", "dc/d21/classG4KDTree.html", null ],
    [ "G4LatticeLogical", "d8/dd0/classG4LatticeLogical.html", null ],
    [ "G4LatticeManager", "d1/d11/classG4LatticeManager.html", null ],
    [ "G4LatticePhysical", "de/d35/classG4LatticePhysical.html", null ],
    [ "G4LatticeReader", "d8/d9b/classG4LatticeReader.html", null ],
    [ "G4LegendrePolynomial", "d3/d0d/classG4LegendrePolynomial.html", null ],
    [ "G4LENDManager", "d8/d3f/classG4LENDManager.html", null ],
    [ "G4LENDUsedTarget", "df/df3/classG4LENDUsedTarget.html", null ],
    [ "G4LeptonConstructor", "d5/d43/classG4LeptonConstructor.html", null ],
    [ "G4LEPTSDiffXS", "de/d6a/classG4LEPTSDiffXS.html", null ],
    [ "G4LEPTSDistribution", "d8/d30/classG4LEPTSDistribution.html", null ],
    [ "G4LEPTSElossDistr", "db/dfe/classG4LEPTSElossDistr.html", null ],
    [ "G4LevelManager", "db/dff/classG4LevelManager.html", null ],
    [ "G4LevelReader", "d6/d60/classG4LevelReader.html", null ],
    [ "G4LindhardSorensenData", "d9/dc6/classG4LindhardSorensenData.html", null ],
    [ "G4LineSection", "d2/d49/classG4LineSection.html", null ],
    [ "G4LocatorChangeRecord", "d9/dfd/classG4LocatorChangeRecord.html", null ],
    [ "G4LogicalSurface", "dd/d73/classG4LogicalSurface.html", [
      [ "G4LogicalBorderSurface", "d8/ddc/classG4LogicalBorderSurface.html", null ],
      [ "G4LogicalSkinSurface", "d1/d86/classG4LogicalSkinSurface.html", null ]
    ] ],
    [ "G4LogicalVolume", "da/dc4/classG4LogicalVolume.html", [
      [ "G4LogicalCrystalVolume", "db/d22/classG4LogicalCrystalVolume.html", null ]
    ] ],
    [ "G4VisCommandSceneAddLogo::G4Logo", "d7/dac/structG4VisCommandSceneAddLogo_1_1G4Logo.html", null ],
    [ "G4LorentzConvertor", "db/d9a/classG4LorentzConvertor.html", null ],
    [ "G4LossTableBuilder", "d3/d1d/classG4LossTableBuilder.html", null ],
    [ "G4LossTableManager", "d9/d78/classG4LossTableManager.html", null ],
    [ "G4LVData", "d5/db4/classG4LVData.html", null ],
    [ "G4MagIntegratorStepper", "d3/df3/classG4MagIntegratorStepper.html", [
      [ "G4TMagErrorStepper< G4TClassicalRK4< T_Equation, N >, T_Equation, N >", "d3/d90/classG4TMagErrorStepper.html", [
        [ "G4TClassicalRK4< T_Equation, N >", "d9/dd3/classG4TClassicalRK4.html", null ]
      ] ],
      [ "G4TMagErrorStepper< G4TExplicitEuler< T_Equation, N >, T_Equation, N >", "d3/d90/classG4TMagErrorStepper.html", [
        [ "G4TExplicitEuler< T_Equation, N >", "de/d07/classG4TExplicitEuler.html", null ]
      ] ],
      [ "G4TMagErrorStepper< G4TSimpleHeum< T_Equation, N >, T_Equation, N >", "d3/d90/classG4TMagErrorStepper.html", [
        [ "G4TSimpleHeum< T_Equation, N >", "dc/d69/classG4TSimpleHeum.html", null ]
      ] ],
      [ "G4TMagErrorStepper< G4TSimpleRunge< T_Equation, N >, T_Equation, N >", "d3/d90/classG4TMagErrorStepper.html", [
        [ "G4TSimpleRunge< T_Equation, N >", "d0/db6/classG4TSimpleRunge.html", null ]
      ] ],
      [ "G4BogackiShampine23", "df/dfc/classG4BogackiShampine23.html", null ],
      [ "G4BogackiShampine45", "dd/dc4/classG4BogackiShampine45.html", null ],
      [ "G4CashKarpRKF45", "d5/d3a/classG4CashKarpRKF45.html", null ],
      [ "G4DoLoMcPriRK34", "d3/daf/classG4DoLoMcPriRK34.html", null ],
      [ "G4DormandPrince745", "d7/d2a/classG4DormandPrince745.html", null ],
      [ "G4DormandPrinceRK56", "d4/dcc/classG4DormandPrinceRK56.html", null ],
      [ "G4DormandPrinceRK78", "d0/d50/classG4DormandPrinceRK78.html", null ],
      [ "G4MagErrorStepper", "dc/d5e/classG4MagErrorStepper.html", [
        [ "G4ClassicalRK4", "d7/d4e/classG4ClassicalRK4.html", null ],
        [ "G4ConstRK4", "d9/d37/classG4ConstRK4.html", null ],
        [ "G4ExplicitEuler", "d0/dae/classG4ExplicitEuler.html", null ],
        [ "G4ImplicitEuler", "dc/d14/classG4ImplicitEuler.html", null ],
        [ "G4SimpleHeum", "df/de2/classG4SimpleHeum.html", null ],
        [ "G4SimpleRunge", "db/d7a/classG4SimpleRunge.html", null ]
      ] ],
      [ "G4MagHelicalStepper", "d5/d53/classG4MagHelicalStepper.html", [
        [ "G4ExactHelixStepper", "d5/d6e/classG4ExactHelixStepper.html", null ],
        [ "G4HelixExplicitEuler", "df/d69/classG4HelixExplicitEuler.html", null ],
        [ "G4HelixHeum", "d6/d22/classG4HelixHeum.html", null ],
        [ "G4HelixImplicitEuler", "dd/d06/classG4HelixImplicitEuler.html", null ],
        [ "G4HelixMixedStepper", "d7/d14/classG4HelixMixedStepper.html", null ],
        [ "G4HelixSimpleRunge", "d6/d4b/classG4HelixSimpleRunge.html", null ]
      ] ],
      [ "G4NystromRK4", "d9/d90/classG4NystromRK4.html", null ],
      [ "G4RK547FEq1", "dd/d72/classG4RK547FEq1.html", null ],
      [ "G4RK547FEq2", "dd/d11/classG4RK547FEq2.html", null ],
      [ "G4RK547FEq3", "dd/d2d/classG4RK547FEq3.html", null ],
      [ "G4RKG3_Stepper", "df/d52/classG4RKG3__Stepper.html", null ],
      [ "G4TCashKarpRKF45< T_Equation, N >", "d3/dbb/classG4TCashKarpRKF45.html", null ],
      [ "G4TDormandPrince45< T_Equation, N >", "df/da6/classG4TDormandPrince45.html", null ],
      [ "G4TMagErrorStepper< T_Stepper, T_Equation, N >", "d3/d90/classG4TMagErrorStepper.html", null ],
      [ "G4TsitourasRK45", "de/d75/classG4TsitourasRK45.html", null ]
    ] ],
    [ "G4ManyFastLists_iterator< OBJECT >", "d7/ddc/structG4ManyFastLists__iterator.html", null ],
    [ "G4Material", "d5/de4/classG4Material.html", [
      [ "G4ExtendedMaterial", "db/d87/classG4ExtendedMaterial.html", null ]
    ] ],
    [ "G4MaterialCutsCouple", "d9/dc9/classG4MaterialCutsCouple.html", null ],
    [ "G4MaterialPropertiesTable", "da/dbc/classG4MaterialPropertiesTable.html", [
      [ "G4UCNMaterialPropertiesTable", "d9/df3/classG4UCNMaterialPropertiesTable.html", null ]
    ] ],
    [ "G4MaterialScanner", "d2/dc8/classG4MaterialScanner.html", null ],
    [ "G4MCCIndexConversionTable", "d3/d5c/classG4MCCIndexConversionTable.html", null ],
    [ "G4MCTEvent", "d1/d3f/classG4MCTEvent.html", null ],
    [ "G4MCTGenEvent", "da/dcd/classG4MCTGenEvent.html", null ],
    [ "G4MCTSimEvent", "db/d82/classG4MCTSimEvent.html", null ],
    [ "G4MCTSimParticle", "d5/d23/classG4MCTSimParticle.html", null ],
    [ "G4MCTSimVertex", "d6/dbd/classG4MCTSimVertex.html", null ],
    [ "G4MemoryError", "de/dbd/classG4MemoryError.html", null ],
    [ "G4Mesh", "d5/dae/classG4Mesh.html", null ],
    [ "G4MesonConstructor", "d8/d9b/classG4MesonConstructor.html", null ],
    [ "G4MesonSplitter", "de/d10/classG4MesonSplitter.html", null ],
    [ "G4Mexp", "d4/ded/classG4Mexp.html", null ],
    [ "G4MicroElecMaterialStructure", "db/d51/classG4MicroElecMaterialStructure.html", null ],
    [ "G4MicroElecSiStructure", "d0/d78/classG4MicroElecSiStructure.html", null ],
    [ "G4ModelColourMap< T >", "dd/d82/classG4ModelColourMap.html", null ],
    [ "G4ModelColourMap< Charge >", "dd/d82/classG4ModelColourMap.html", null ],
    [ "G4ModelColourMap< G4String >", "dd/d82/classG4ModelColourMap.html", null ],
    [ "G4ModelingParameters", "d3/d59/classG4ModelingParameters.html", null ],
    [ "G4ModifiedMidpoint", "d0/d73/classG4ModifiedMidpoint.html", null ],
    [ "G4MolecularConfiguration", "dc/da7/classG4MolecularConfiguration.html", null ],
    [ "G4MolecularConfiguration::G4MolecularConfigurationManager", "d7/d83/classG4MolecularConfiguration_1_1G4MolecularConfigurationManager.html", null ],
    [ "G4MolecularDissociationChannel", "dc/dbc/classG4MolecularDissociationChannel.html", null ],
    [ "G4MolecularDissociationTable", "df/d76/classG4MolecularDissociationTable.html", null ],
    [ "G4MoleculeHandleManager", "d2/d3e/classG4MoleculeHandleManager.html", null ],
    [ "G4MoleculeIterator< MOLECULE >", "d9/d20/classG4MoleculeIterator.html", null ],
    [ "G4MoleculeTable", "d7/d11/classG4MoleculeTable.html", null ],
    [ "G4MPIToolsManager", "d6/ddc/classG4MPIToolsManager.html", null ],
    [ "G4MTBarrier", "dc/de0/classG4MTBarrier.html", null ],
    [ "G4MultiBodyMomentumDist", "db/d85/classG4MultiBodyMomentumDist.html", null ],
    [ "G4MultiUnion::G4MultiUnionSurface", "dd/d65/structG4MultiUnion_1_1G4MultiUnionSurface.html", null ],
    [ "G4MuonicAtomHelper", "d6/d86/classG4MuonicAtomHelper.html", null ],
    [ "G4NavigationHistory", "d4/d3b/classG4NavigationHistory.html", null ],
    [ "G4NavigationHistoryPool", "d1/dd3/classG4NavigationHistoryPool.html", null ],
    [ "G4NavigationLevel", "de/d03/classG4NavigationLevel.html", null ],
    [ "G4NavigationLevelRep", "da/d4f/classG4NavigationLevelRep.html", null ],
    [ "G4NavigationLogger", "db/dd0/classG4NavigationLogger.html", null ],
    [ "G4Navigator", "d9/d53/classG4Navigator.html", [
      [ "G4ErrorPropagationNavigator", "da/d15/classG4ErrorPropagationNavigator.html", null ],
      [ "G4MultiNavigator", "dc/d0d/classG4MultiNavigator.html", null ]
    ] ],
    [ "G4Nevent", "d1/d12/classG4Nevent.html", null ],
    [ "G4NIELCalculator", "dc/dee/classG4NIELCalculator.html", null ],
    [ "G4NistElementBuilder", "d9/df3/classG4NistElementBuilder.html", null ],
    [ "G4NistManager", "d2/d00/classG4NistManager.html", null ],
    [ "G4NistMaterialBuilder", "df/d8e/classG4NistMaterialBuilder.html", null ],
    [ "G4NoModelFound", "da/dcc/classG4NoModelFound.html", null ],
    [ "G4NormalNavigation", "d5/d14/classG4NormalNavigation.html", null ],
    [ "G4NotSupported", "da/dc8/classG4NotSupported.html", null ],
    [ "G4NRESP71M03", "df/d1b/classG4NRESP71M03.html", null ],
    [ "G4Nsplit_Weight", "db/d7d/classG4Nsplit__Weight.html", null ],
    [ "G4NtupleBooking", "da/dfd/structG4NtupleBooking.html", null ],
    [ "G4NuclearAbrasionGeometry", "d9/d5b/classG4NuclearAbrasionGeometry.html", null ],
    [ "G4NuclearLevelData", "d8/dd6/classG4NuclearLevelData.html", null ],
    [ "G4NuclearPolarization", "d6/ddf/classG4NuclearPolarization.html", null ],
    [ "G4NuclearPolarizationStore", "db/dc1/classG4NuclearPolarizationStore.html", null ],
    [ "G4NuclearRadii", "da/d31/classG4NuclearRadii.html", null ],
    [ "G4NucleiModel", "d9/da6/classG4NucleiModel.html", null ],
    [ "G4NucleiProperties", "d3/df1/classG4NucleiProperties.html", null ],
    [ "G4NucleiPropertiesTableAME12", "da/d39/classG4NucleiPropertiesTableAME12.html", null ],
    [ "G4NucleiPropertiesTheoreticalTable", "dd/d6e/classG4NucleiPropertiesTheoreticalTable.html", null ],
    [ "G4Nucleus", "d3/dc0/classG4Nucleus.html", null ],
    [ "G4NucleusLimits", "d8/d35/classG4NucleusLimits.html", null ],
    [ "G4NucLevel", "d7/da2/classG4NucLevel.html", null ],
    [ "G4NuclWatcher", "d7/d6e/classG4NuclWatcher.html", null ],
    [ "G4Number< N >", "d3/d60/structG4Number.html", null ],
    [ "G4Number< 0 >", "d8/dda/structG4Number_3_010_01_4.html", null ],
    [ "G4Octree< Iterator, Extractor, Point >", "d5/d6d/classG4Octree.html", null ],
    [ "G4OpenGL2PSAction", "d2/d90/classG4OpenGL2PSAction.html", [
      [ "SoGL2PSAction", "da/d63/classSoGL2PSAction.html", null ]
    ] ],
    [ "G4OpenGLFontBaseStore", "da/d22/classG4OpenGLFontBaseStore.html", null ],
    [ "G4OpenGLTransform3D", "db/d7a/classG4OpenGLTransform3D.html", null ],
    [ "G4OpenGLViewerPickMap", "d0/d09/classG4OpenGLViewerPickMap.html", null ],
    [ "G4OpenGLXmVWidgetObject", "d0/d4b/classG4OpenGLXmVWidgetObject.html", [
      [ "G4OpenGLXmVWidgetComponent", "d7/db4/classG4OpenGLXmVWidgetComponent.html", [
        [ "G4OpenGLXmFourArrowButtons", "d3/d7b/classG4OpenGLXmFourArrowButtons.html", null ],
        [ "G4OpenGLXmPushButton", "d5/dac/classG4OpenGLXmPushButton.html", null ],
        [ "G4OpenGLXmRadioButton", "d7/d58/classG4OpenGLXmRadioButton.html", null ],
        [ "G4OpenGLXmSeparator", "d3/d71/classG4OpenGLXmSeparator.html", null ],
        [ "G4OpenGLXmSliderBar", "d9/d61/classG4OpenGLXmSliderBar.html", null ],
        [ "G4OpenGLXmTextField", "d2/d9e/classG4OpenGLXmTextField.html", null ]
      ] ],
      [ "G4OpenGLXmVWidgetContainer", "d2/d37/classG4OpenGLXmVWidgetContainer.html", [
        [ "G4OpenGLXmBox", "d8/d0b/classG4OpenGLXmBox.html", [
          [ "G4OpenGLXmFramedBox", "da/d7d/classG4OpenGLXmFramedBox.html", null ]
        ] ]
      ] ],
      [ "G4OpenGLXmVWidgetShell", "d4/d1a/classG4OpenGLXmVWidgetShell.html", [
        [ "G4OpenGLXmTopLevelShell", "d5/d01/classG4OpenGLXmTopLevelShell.html", null ]
      ] ]
    ] ],
    [ "G4Opt", "d1/dce/classG4Opt.html", null ],
    [ "G4OpticalParameters", "d5/d9a/classG4OpticalParameters.html", null ],
    [ "G4OrlicLiXsModel", "df/d35/classG4OrlicLiXsModel.html", null ],
    [ "G4OutBitStream", "d6/d15/classG4OutBitStream.html", null ],
    [ "G4Pace", "da/d14/classG4Pace.html", null ],
    [ "G4PAIModelData", "db/d9e/classG4PAIModelData.html", null ],
    [ "G4PAIPhotData", "d7/d81/classG4PAIPhotData.html", null ],
    [ "G4Pair< t1, t2 >", "d9/dce/classG4Pair.html", null ],
    [ "G4PairingCorrection", "d9/d50/classG4PairingCorrection.html", null ],
    [ "G4PAIxSection", "d4/d59/classG4PAIxSection.html", null ],
    [ "G4PAIySection", "d4/d2f/classG4PAIySection.html", null ],
    [ "G4PartialWidthTable", "d9/da7/classG4PartialWidthTable.html", null ],
    [ "G4ParticleDefinition", "d5/d38/classG4ParticleDefinition.html", [
      [ "G4AdjointElectron", "d3/de0/classG4AdjointElectron.html", null ],
      [ "G4AdjointElectronFI", "d2/d26/classG4AdjointElectronFI.html", null ],
      [ "G4AdjointGamma", "db/d16/classG4AdjointGamma.html", null ],
      [ "G4AdjointIons", "d0/d3c/classG4AdjointIons.html", [
        [ "G4AdjointAlpha", "da/d4d/classG4AdjointAlpha.html", null ],
        [ "G4AdjointDeuteron", "d0/dbd/classG4AdjointDeuteron.html", null ],
        [ "G4AdjointGenericIon", "dc/dd0/classG4AdjointGenericIon.html", null ],
        [ "G4AdjointHe3", "da/d24/classG4AdjointHe3.html", null ],
        [ "G4AdjointTriton", "dd/d58/classG4AdjointTriton.html", null ]
      ] ],
      [ "G4AdjointPositron", "db/d16/classG4AdjointPositron.html", null ],
      [ "G4AdjointProton", "de/dca/classG4AdjointProton.html", null ],
      [ "G4AntiBMesonZero", "db/df9/classG4AntiBMesonZero.html", null ],
      [ "G4AntiBsMesonZero", "d2/d9d/classG4AntiBsMesonZero.html", null ],
      [ "G4AntiDMesonZero", "dd/d55/classG4AntiDMesonZero.html", null ],
      [ "G4AntiKaonZero", "d7/d22/classG4AntiKaonZero.html", null ],
      [ "G4AntiLambda", "df/d5d/classG4AntiLambda.html", null ],
      [ "G4AntiLambdab", "d8/de3/classG4AntiLambdab.html", null ],
      [ "G4AntiLambdacPlus", "d2/db6/classG4AntiLambdacPlus.html", null ],
      [ "G4AntiNeutrinoE", "d6/d27/classG4AntiNeutrinoE.html", null ],
      [ "G4AntiNeutrinoMu", "de/dc6/classG4AntiNeutrinoMu.html", null ],
      [ "G4AntiNeutrinoTau", "de/dbf/classG4AntiNeutrinoTau.html", null ],
      [ "G4AntiNeutron", "d0/dd8/classG4AntiNeutron.html", null ],
      [ "G4AntiOmegaMinus", "d0/d59/classG4AntiOmegaMinus.html", null ],
      [ "G4AntiOmegabMinus", "d9/de4/classG4AntiOmegabMinus.html", null ],
      [ "G4AntiOmegacZero", "de/dc3/classG4AntiOmegacZero.html", null ],
      [ "G4AntiProton", "dd/dc6/classG4AntiProton.html", null ],
      [ "G4AntiSigmaMinus", "d0/deb/classG4AntiSigmaMinus.html", null ],
      [ "G4AntiSigmaPlus", "da/d8d/classG4AntiSigmaPlus.html", null ],
      [ "G4AntiSigmaZero", "d0/d56/classG4AntiSigmaZero.html", null ],
      [ "G4AntiSigmabMinus", "d6/d9e/classG4AntiSigmabMinus.html", null ],
      [ "G4AntiSigmabPlus", "db/deb/classG4AntiSigmabPlus.html", null ],
      [ "G4AntiSigmabZero", "d7/d56/classG4AntiSigmabZero.html", null ],
      [ "G4AntiSigmacPlus", "d5/dfa/classG4AntiSigmacPlus.html", null ],
      [ "G4AntiSigmacPlusPlus", "db/d70/classG4AntiSigmacPlusPlus.html", null ],
      [ "G4AntiSigmacZero", "de/da8/classG4AntiSigmacZero.html", null ],
      [ "G4AntiXiMinus", "d1/dd2/classG4AntiXiMinus.html", null ],
      [ "G4AntiXiZero", "d5/dc8/classG4AntiXiZero.html", null ],
      [ "G4AntiXibMinus", "dd/d0b/classG4AntiXibMinus.html", null ],
      [ "G4AntiXibZero", "df/d0d/classG4AntiXibZero.html", null ],
      [ "G4AntiXicPlus", "da/d54/classG4AntiXicPlus.html", null ],
      [ "G4AntiXicZero", "d7/d92/classG4AntiXicZero.html", null ],
      [ "G4BMesonMinus", "db/d0f/classG4BMesonMinus.html", null ],
      [ "G4BMesonPlus", "dc/d9a/classG4BMesonPlus.html", null ],
      [ "G4BMesonZero", "de/d25/classG4BMesonZero.html", null ],
      [ "G4BcMesonMinus", "de/d00/classG4BcMesonMinus.html", null ],
      [ "G4BcMesonPlus", "d1/d2e/classG4BcMesonPlus.html", null ],
      [ "G4BsMesonZero", "d7/de3/classG4BsMesonZero.html", null ],
      [ "G4ChargedGeantino", "d9/d2f/classG4ChargedGeantino.html", null ],
      [ "G4DMesonMinus", "d7/dfe/classG4DMesonMinus.html", null ],
      [ "G4DMesonPlus", "d7/d18/classG4DMesonPlus.html", null ],
      [ "G4DMesonZero", "da/dde/classG4DMesonZero.html", null ],
      [ "G4DNAIons", "de/d68/classG4DNAIons.html", null ],
      [ "G4DsMesonMinus", "db/d20/classG4DsMesonMinus.html", null ],
      [ "G4DsMesonPlus", "d7/d86/classG4DsMesonPlus.html", null ],
      [ "G4Electron", "d3/d96/classG4Electron.html", null ],
      [ "G4Eta", "d5/d9a/classG4Eta.html", null ],
      [ "G4EtaPrime", "da/d1f/classG4EtaPrime.html", null ],
      [ "G4Etac", "db/df3/classG4Etac.html", null ],
      [ "G4Gamma", "d0/d54/classG4Gamma.html", null ],
      [ "G4Geantino", "d1/df3/classG4Geantino.html", null ],
      [ "G4Ions", "df/d1a/classG4Ions.html", [
        [ "G4Alpha", "dc/d9e/classG4Alpha.html", null ],
        [ "G4AntiAlpha", "df/dd3/classG4AntiAlpha.html", null ],
        [ "G4AntiDeuteron", "d6/d9f/classG4AntiDeuteron.html", null ],
        [ "G4AntiDoubleHyperDoubleNeutron", "d9/d8f/classG4AntiDoubleHyperDoubleNeutron.html", null ],
        [ "G4AntiDoubleHyperH4", "d5/d29/classG4AntiDoubleHyperH4.html", null ],
        [ "G4AntiHe3", "d1/dfc/classG4AntiHe3.html", null ],
        [ "G4AntiHyperAlpha", "d1/d57/classG4AntiHyperAlpha.html", null ],
        [ "G4AntiHyperH4", "d4/d53/classG4AntiHyperH4.html", null ],
        [ "G4AntiHyperHe5", "d4/dd0/classG4AntiHyperHe5.html", null ],
        [ "G4AntiHyperTriton", "d8/d51/classG4AntiHyperTriton.html", null ],
        [ "G4AntiTriton", "d2/d74/classG4AntiTriton.html", null ],
        [ "G4Deuteron", "df/dfb/classG4Deuteron.html", null ],
        [ "G4DoubleHyperDoubleNeutron", "d3/d38/classG4DoubleHyperDoubleNeutron.html", null ],
        [ "G4DoubleHyperH4", "d4/d10/classG4DoubleHyperH4.html", null ],
        [ "G4GenericIon", "d4/d78/classG4GenericIon.html", null ],
        [ "G4He3", "d2/db5/classG4He3.html", null ],
        [ "G4HyperAlpha", "d4/db8/classG4HyperAlpha.html", null ],
        [ "G4HyperH4", "d8/db6/classG4HyperH4.html", null ],
        [ "G4HyperHe5", "d6/d44/classG4HyperHe5.html", null ],
        [ "G4HyperTriton", "d6/d7c/classG4HyperTriton.html", null ],
        [ "G4MuonicAtom", "d0/d75/classG4MuonicAtom.html", [
          [ "G4GenericMuonicAtom", "d6/def/classG4GenericMuonicAtom.html", null ]
        ] ],
        [ "G4Neutron", "d5/d43/classG4Neutron.html", null ],
        [ "G4Proton", "d2/dd8/classG4Proton.html", null ],
        [ "G4Triton", "da/d5b/classG4Triton.html", null ]
      ] ],
      [ "G4JPsi", "d9/de5/classG4JPsi.html", null ],
      [ "G4KaonMinus", "d4/db6/classG4KaonMinus.html", null ],
      [ "G4KaonPlus", "d8/d80/classG4KaonPlus.html", null ],
      [ "G4KaonZero", "d1/dd9/classG4KaonZero.html", null ],
      [ "G4KaonZeroLong", "d6/d08/classG4KaonZeroLong.html", null ],
      [ "G4KaonZeroShort", "d8/dcc/classG4KaonZeroShort.html", null ],
      [ "G4Lambda", "d3/d88/classG4Lambda.html", null ],
      [ "G4Lambdab", "d9/daf/classG4Lambdab.html", null ],
      [ "G4LambdacPlus", "dd/dbe/classG4LambdacPlus.html", null ],
      [ "G4MoleculeDefinition", "dd/d58/classG4MoleculeDefinition.html", [
        [ "G4Adenine", "d9/db5/classG4Adenine.html", null ],
        [ "G4Cytosine", "dd/d56/classG4Cytosine.html", null ],
        [ "G4DamagedAdenine", "d0/dfa/classG4DamagedAdenine.html", null ],
        [ "G4DamagedCytosine", "dc/dbb/classG4DamagedCytosine.html", null ],
        [ "G4DamagedDeoxyribose", "d0/da3/classG4DamagedDeoxyribose.html", null ],
        [ "G4DamagedGuanine", "d5/dd4/classG4DamagedGuanine.html", null ],
        [ "G4DamagedThymine", "da/d5f/classG4DamagedThymine.html", null ],
        [ "G4Deoxyribose", "d2/dc7/classG4Deoxyribose.html", null ],
        [ "G4Electron_aq", "d0/deb/classG4Electron__aq.html", null ],
        [ "G4FakeMolecule", "d6/d4b/classG4FakeMolecule.html", null ],
        [ "G4Guanine", "d3/d37/classG4Guanine.html", null ],
        [ "G4H2", "db/d83/classG4H2.html", null ],
        [ "G4H2O", "dd/d52/classG4H2O.html", null ],
        [ "G4H2O2", "dc/da8/classG4H2O2.html", null ],
        [ "G4H3O", "dd/d80/classG4H3O.html", null ],
        [ "G4HO2", "d9/d8a/classG4HO2.html", null ],
        [ "G4Histone", "d5/db4/classG4Histone.html", null ],
        [ "G4Hydrogen", "da/da0/classG4Hydrogen.html", null ],
        [ "G4ModifiedHistone", "dc/df7/classG4ModifiedHistone.html", null ],
        [ "G4O2", "dc/df3/classG4O2.html", null ],
        [ "G4O3", "d4/d0b/classG4O3.html", null ],
        [ "G4OH", "d2/d33/classG4OH.html", null ],
        [ "G4Oxygen", "d4/d60/classG4Oxygen.html", null ],
        [ "G4Phosphate", "d6/d4a/classG4Phosphate.html", null ],
        [ "G4Thymine", "de/df0/classG4Thymine.html", null ]
      ] ],
      [ "G4MuonMinus", "d6/df9/classG4MuonMinus.html", null ],
      [ "G4MuonPlus", "de/db6/classG4MuonPlus.html", null ],
      [ "G4NeutrinoE", "d5/d7d/classG4NeutrinoE.html", null ],
      [ "G4NeutrinoMu", "de/dc3/classG4NeutrinoMu.html", null ],
      [ "G4NeutrinoTau", "da/d66/classG4NeutrinoTau.html", null ],
      [ "G4OmegaMinus", "d9/d43/classG4OmegaMinus.html", null ],
      [ "G4OmegabMinus", "d2/de6/classG4OmegabMinus.html", null ],
      [ "G4OmegacZero", "d1/d54/classG4OmegacZero.html", null ],
      [ "G4OpticalPhoton", "d9/d8f/classG4OpticalPhoton.html", null ],
      [ "G4PhononLong", "d3/d5c/classG4PhononLong.html", null ],
      [ "G4PhononTransFast", "d5/d50/classG4PhononTransFast.html", null ],
      [ "G4PhononTransSlow", "dd/dbb/classG4PhononTransSlow.html", null ],
      [ "G4PionMinus", "d8/d83/classG4PionMinus.html", null ],
      [ "G4PionPlus", "d0/dc2/classG4PionPlus.html", null ],
      [ "G4PionZero", "dc/d7f/classG4PionZero.html", null ],
      [ "G4Positron", "d3/d12/classG4Positron.html", null ],
      [ "G4SigmaMinus", "d7/dc5/classG4SigmaMinus.html", null ],
      [ "G4SigmaPlus", "de/d23/classG4SigmaPlus.html", null ],
      [ "G4SigmaZero", "db/da0/classG4SigmaZero.html", null ],
      [ "G4SigmabMinus", "d7/dc7/classG4SigmabMinus.html", null ],
      [ "G4SigmabPlus", "d4/d3d/classG4SigmabPlus.html", null ],
      [ "G4SigmabZero", "d1/d8a/classG4SigmabZero.html", null ],
      [ "G4SigmacPlus", "d6/d2d/classG4SigmacPlus.html", null ],
      [ "G4SigmacPlusPlus", "df/d14/classG4SigmacPlusPlus.html", null ],
      [ "G4SigmacZero", "d8/df4/classG4SigmacZero.html", null ],
      [ "G4TauMinus", "dd/d7d/classG4TauMinus.html", null ],
      [ "G4TauPlus", "d2/d7a/classG4TauPlus.html", null ],
      [ "G4UnknownParticle", "d5/d5d/classG4UnknownParticle.html", null ],
      [ "G4Upsilon", "dc/d48/classG4Upsilon.html", null ],
      [ "G4VShortLivedParticle", "d1/d6b/classG4VShortLivedParticle.html", [
        [ "G4DiQuarks", "d8/daf/classG4DiQuarks.html", null ],
        [ "G4Dineutron", "dc/d96/classG4Dineutron.html", null ],
        [ "G4Diproton", "d2/d0b/classG4Diproton.html", null ],
        [ "G4ExcitedBaryons", "db/d42/classG4ExcitedBaryons.html", null ],
        [ "G4ExcitedMesons", "df/de1/classG4ExcitedMesons.html", null ],
        [ "G4Gluons", "d7/d4c/classG4Gluons.html", null ],
        [ "G4Quarks", "dc/dea/classG4Quarks.html", null ],
        [ "G4UnboundPN", "d2/d9f/classG4UnboundPN.html", null ]
      ] ],
      [ "G4XiMinus", "d2/d9c/classG4XiMinus.html", null ],
      [ "G4XiZero", "d1/de6/classG4XiZero.html", null ],
      [ "G4XibMinus", "d5/dbc/classG4XibMinus.html", null ],
      [ "G4XibZero", "d4/d2e/classG4XibZero.html", null ],
      [ "G4XicPlus", "d0/d34/classG4XicPlus.html", null ],
      [ "G4XicZero", "dc/dca/classG4XicZero.html", null ]
    ] ],
    [ "G4ParticleHPAngular", "d2/d58/classG4ParticleHPAngular.html", null ],
    [ "G4ParticleHPAngularP", "d7/d3a/classG4ParticleHPAngularP.html", null ],
    [ "G4ParticleHPChannel", "da/de9/classG4ParticleHPChannel.html", null ],
    [ "G4ParticleHPChannelList", "df/da7/classG4ParticleHPChannelList.html", null ],
    [ "G4ParticleHPContAngularPar", "d3/d26/classG4ParticleHPContAngularPar.html", null ],
    [ "G4ParticleHPData", "d4/d68/classG4ParticleHPData.html", null ],
    [ "G4ParticleHPDataPoint", "d7/d30/classG4ParticleHPDataPoint.html", null ],
    [ "G4ParticleHPDataUsed", "d3/db5/classG4ParticleHPDataUsed.html", null ],
    [ "G4ParticleHPDeExGammas", "d0/d4f/classG4ParticleHPDeExGammas.html", null ],
    [ "G4ParticleHPElementData", "dd/df1/classG4ParticleHPElementData.html", null ],
    [ "G4ParticleHPEnAngCorrelation", "db/d68/classG4ParticleHPEnAngCorrelation.html", null ],
    [ "G4ParticleHPEnergyDistribution", "d2/d96/classG4ParticleHPEnergyDistribution.html", null ],
    [ "G4ParticleHPFastLegendre", "de/d26/classG4ParticleHPFastLegendre.html", null ],
    [ "G4ParticleHPField", "d9/d4c/classG4ParticleHPField.html", null ],
    [ "G4ParticleHPFieldPoint", "d6/dba/classG4ParticleHPFieldPoint.html", null ],
    [ "G4ParticleHPFinalState", "df/d5c/classG4ParticleHPFinalState.html", [
      [ "G4FissionLibrary", "d9/d0b/classG4FissionLibrary.html", null ],
      [ "G4ParticleHPCaptureFS", "da/dc0/classG4ParticleHPCaptureFS.html", null ],
      [ "G4ParticleHPElasticFS", "d8/df6/classG4ParticleHPElasticFS.html", null ],
      [ "G4ParticleHPFSFissionFS", "d3/d79/classG4ParticleHPFSFissionFS.html", null ],
      [ "G4ParticleHPFissionBaseFS", "dd/dc7/classG4ParticleHPFissionBaseFS.html", [
        [ "G4ParticleHPFCFissionFS", "dd/df8/classG4ParticleHPFCFissionFS.html", null ],
        [ "G4ParticleHPFFFissionFS", "de/d22/classG4ParticleHPFFFissionFS.html", null ],
        [ "G4ParticleHPLCFissionFS", "d7/d85/classG4ParticleHPLCFissionFS.html", null ],
        [ "G4ParticleHPSCFissionFS", "d6/d43/classG4ParticleHPSCFissionFS.html", null ],
        [ "G4ParticleHPTCFissionFS", "df/d55/classG4ParticleHPTCFissionFS.html", null ]
      ] ],
      [ "G4ParticleHPFissionFS", "df/df3/classG4ParticleHPFissionFS.html", null ],
      [ "G4ParticleHPInelasticBaseFS", "d2/da9/classG4ParticleHPInelasticBaseFS.html", [
        [ "G4ParticleHP2AInelasticFS", "dc/d77/classG4ParticleHP2AInelasticFS.html", null ],
        [ "G4ParticleHP2N2AInelasticFS", "db/d56/classG4ParticleHP2N2AInelasticFS.html", null ],
        [ "G4ParticleHP2NAInelasticFS", "d7/d64/classG4ParticleHP2NAInelasticFS.html", null ],
        [ "G4ParticleHP2NDInelasticFS", "d9/dbc/classG4ParticleHP2NDInelasticFS.html", null ],
        [ "G4ParticleHP2NInelasticFS", "da/d1b/classG4ParticleHP2NInelasticFS.html", null ],
        [ "G4ParticleHP2NPInelasticFS", "d1/d00/classG4ParticleHP2NPInelasticFS.html", null ],
        [ "G4ParticleHP2PInelasticFS", "d4/dcb/classG4ParticleHP2PInelasticFS.html", null ],
        [ "G4ParticleHP3AInelasticFS", "dc/d01/classG4ParticleHP3AInelasticFS.html", null ],
        [ "G4ParticleHP3NAInelasticFS", "df/d3b/classG4ParticleHP3NAInelasticFS.html", null ],
        [ "G4ParticleHP3NInelasticFS", "d0/d8d/classG4ParticleHP3NInelasticFS.html", null ],
        [ "G4ParticleHP3NPInelasticFS", "d2/d92/classG4ParticleHP3NPInelasticFS.html", null ],
        [ "G4ParticleHP4NInelasticFS", "d2/d64/classG4ParticleHP4NInelasticFS.html", null ],
        [ "G4ParticleHPD2AInelasticFS", "d4/d6d/classG4ParticleHPD2AInelasticFS.html", null ],
        [ "G4ParticleHPDAInelasticFS", "d9/dea/classG4ParticleHPDAInelasticFS.html", null ],
        [ "G4ParticleHPN2AInelasticFS", "db/dd4/classG4ParticleHPN2AInelasticFS.html", null ],
        [ "G4ParticleHPN2PInelasticFS", "db/d09/classG4ParticleHPN2PInelasticFS.html", null ],
        [ "G4ParticleHPN3AInelasticFS", "de/d8e/classG4ParticleHPN3AInelasticFS.html", null ],
        [ "G4ParticleHPNAInelasticFS", "d7/de6/classG4ParticleHPNAInelasticFS.html", null ],
        [ "G4ParticleHPND2AInelasticFS", "d6/dd0/classG4ParticleHPND2AInelasticFS.html", null ],
        [ "G4ParticleHPNDInelasticFS", "d2/d46/classG4ParticleHPNDInelasticFS.html", null ],
        [ "G4ParticleHPNHe3InelasticFS", "d6/d37/classG4ParticleHPNHe3InelasticFS.html", null ],
        [ "G4ParticleHPNPAInelasticFS", "de/dbc/classG4ParticleHPNPAInelasticFS.html", null ],
        [ "G4ParticleHPNPInelasticFS", "db/df2/classG4ParticleHPNPInelasticFS.html", null ],
        [ "G4ParticleHPNT2AInelasticFS", "d0/da1/classG4ParticleHPNT2AInelasticFS.html", null ],
        [ "G4ParticleHPNTInelasticFS", "d7/d3b/classG4ParticleHPNTInelasticFS.html", null ],
        [ "G4ParticleHPNXInelasticFS", "db/d97/classG4ParticleHPNXInelasticFS.html", null ],
        [ "G4ParticleHPPAInelasticFS", "de/d48/classG4ParticleHPPAInelasticFS.html", null ],
        [ "G4ParticleHPPDInelasticFS", "d1/d22/classG4ParticleHPPDInelasticFS.html", null ],
        [ "G4ParticleHPPTInelasticFS", "d3/dff/classG4ParticleHPPTInelasticFS.html", null ],
        [ "G4ParticleHPT2AInelasticFS", "d0/d58/classG4ParticleHPT2AInelasticFS.html", null ]
      ] ],
      [ "G4ParticleHPInelasticCompFS", "de/d7e/classG4ParticleHPInelasticCompFS.html", [
        [ "G4ParticleHPAInelasticFS", "d2/d90/classG4ParticleHPAInelasticFS.html", null ],
        [ "G4ParticleHPDInelasticFS", "de/d2b/classG4ParticleHPDInelasticFS.html", null ],
        [ "G4ParticleHPHe3InelasticFS", "da/d2c/classG4ParticleHPHe3InelasticFS.html", null ],
        [ "G4ParticleHPNInelasticFS", "d2/d3a/classG4ParticleHPNInelasticFS.html", null ],
        [ "G4ParticleHPPInelasticFS", "d7/dc1/classG4ParticleHPPInelasticFS.html", null ],
        [ "G4ParticleHPTInelasticFS", "db/d95/classG4ParticleHPTInelasticFS.html", null ]
      ] ]
    ] ],
    [ "G4ParticleHPFissionERelease", "d3/d6d/classG4ParticleHPFissionERelease.html", null ],
    [ "G4ParticleHPGamma", "d9/dbc/classG4ParticleHPGamma.html", null ],
    [ "G4ParticleHPHash", "d3/dae/classG4ParticleHPHash.html", null ],
    [ "G4ParticleHPInterpolator", "d8/dd6/classG4ParticleHPInterpolator.html", null ],
    [ "G4ParticleHPIsoData", "d4/ded/classG4ParticleHPIsoData.html", null ],
    [ "G4ParticleHPKallbachMannSyst", "db/d86/classG4ParticleHPKallbachMannSyst.html", null ],
    [ "G4ParticleHPLegendreStore", "d7/def/classG4ParticleHPLegendreStore.html", null ],
    [ "G4ParticleHPLegendreTable", "d8/d10/classG4ParticleHPLegendreTable.html", null ],
    [ "G4ParticleHPLevel", "da/dcb/classG4ParticleHPLevel.html", null ],
    [ "G4ParticleHPList", "db/d7c/classG4ParticleHPList.html", null ],
    [ "G4ParticleHPManager", "de/ddf/classG4ParticleHPManager.html", null ],
    [ "G4ParticleHPNames", "db/d66/classG4ParticleHPNames.html", null ],
    [ "G4ParticleHPPartial", "dd/d9f/classG4ParticleHPPartial.html", null ],
    [ "G4ParticleHPParticleYield", "da/d7e/classG4ParticleHPParticleYield.html", null ],
    [ "G4ParticleHPPhotonDist", "da/da5/classG4ParticleHPPhotonDist.html", null ],
    [ "G4ParticleHPPolynomExpansion", "d7/d16/classG4ParticleHPPolynomExpansion.html", null ],
    [ "G4ParticleHPProduct", "d0/dd6/classG4ParticleHPProduct.html", null ],
    [ "G4ParticleHPReactionWhiteBoard", "d8/dbe/classG4ParticleHPReactionWhiteBoard.html", null ],
    [ "G4ParticleHPThermalBoost", "d0/df8/classG4ParticleHPThermalBoost.html", null ],
    [ "G4ParticleHPThermalScatteringNames", "d7/df9/classG4ParticleHPThermalScatteringNames.html", null ],
    [ "G4ParticleHPThreadLocalManager", "d1/db9/classG4ParticleHPThreadLocalManager.html", null ],
    [ "G4ParticleHPVector", "dc/df1/classG4ParticleHPVector.html", null ],
    [ "G4ParticleLargerBeta", "d0/dc0/classG4ParticleLargerBeta.html", null ],
    [ "G4ParticleLargerEkin", "dc/d69/classG4ParticleLargerEkin.html", null ],
    [ "G4ParticlePropertyData", "dc/de3/classG4ParticlePropertyData.html", null ],
    [ "G4ParticlePropertyTable", "d2/d5f/classG4ParticlePropertyTable.html", null ],
    [ "G4ParticlesWorkspace", "d5/d80/classG4ParticlesWorkspace.html", null ],
    [ "G4ParticleTable", "d8/dd3/classG4ParticleTable.html", null ],
    [ "G4ParticleTableIterator< K, V >", "d4/d96/classG4ParticleTableIterator.html", null ],
    [ "G4ParticleTypeConverter", "de/d4f/classG4ParticleTypeConverter.html", null ],
    [ "G4Parton", "db/dde/classG4Parton.html", null ],
    [ "G4PartonPair", "d1/d7a/classG4PartonPair.html", null ],
    [ "G4PathFinder", "dd/dfc/classG4PathFinder.html", null ],
    [ "G4PaulKxsModel", "d8/de9/classG4PaulKxsModel.html", null ],
    [ "G4PDefData", "d3/ddb/classG4PDefData.html", null ],
    [ "G4PDefManager", "d9/d62/classG4PDefManager.html", null ],
    [ "G4PDGCodeChecker", "d6/df6/classG4PDGCodeChecker.html", null ],
    [ "G4PenelopeBremsstrahlungFS", "d5/dbf/classG4PenelopeBremsstrahlungFS.html", null ],
    [ "G4PenelopeCrossSection", "df/de7/classG4PenelopeCrossSection.html", null ],
    [ "G4PenelopeIonisationXSHandler", "d8/d69/classG4PenelopeIonisationXSHandler.html", null ],
    [ "G4PenelopeOscillator", "dd/d93/classG4PenelopeOscillator.html", null ],
    [ "G4PenelopeOscillatorManager", "d4/dee/classG4PenelopeOscillatorManager.html", null ],
    [ "G4PenelopeOscillatorResEnergyComparator", "d9/d0a/structG4PenelopeOscillatorResEnergyComparator.html", null ],
    [ "G4PenelopeSamplingData", "d5/de1/classG4PenelopeSamplingData.html", null ],
    [ "G4PersistencyCenter", "d5/d22/classG4PersistencyCenter.html", null ],
    [ "G4Pevent", "d7/d40/classG4Pevent.html", null ],
    [ "G4PhononTrackMap", "de/d46/classG4PhononTrackMap.html", null ],
    [ "G4PhSideData", "dd/d72/classG4PhSideData.html", null ],
    [ "G4PhysicalVolumeModel::G4PhysicalVolumeNodeID", "d0/de1/classG4PhysicalVolumeModel_1_1G4PhysicalVolumeNodeID.html", null ],
    [ "G4Physics2DVector", "d3/d41/classG4Physics2DVector.html", null ],
    [ "G4PhysicsBuilderInterface", "de/dc8/classG4PhysicsBuilderInterface.html", [
      [ "G4AlphaBuilder", "d9/daa/classG4AlphaBuilder.html", null ],
      [ "G4AntiBarionBuilder", "d2/dfe/classG4AntiBarionBuilder.html", null ],
      [ "G4DeuteronBuilder", "d1/d98/classG4DeuteronBuilder.html", null ],
      [ "G4He3Builder", "d2/dd7/classG4He3Builder.html", null ],
      [ "G4HyperonBuilder", "d7/dbb/classG4HyperonBuilder.html", null ],
      [ "G4KaonBuilder", "dc/d10/classG4KaonBuilder.html", null ],
      [ "G4NeutronBuilder", "d6/d81/classG4NeutronBuilder.html", null ],
      [ "G4PiKBuilder", "d2/d0b/classG4PiKBuilder.html", null ],
      [ "G4PionBuilder", "dc/d86/classG4PionBuilder.html", null ],
      [ "G4ProtonBuilder", "d9/dad/classG4ProtonBuilder.html", null ],
      [ "G4TritonBuilder", "db/da7/classG4TritonBuilder.html", null ],
      [ "G4VAlphaBuilder", "d3/d9c/classG4VAlphaBuilder.html", [
        [ "G4AlphaPHPBuilder", "d8/d23/classG4AlphaPHPBuilder.html", null ],
        [ "G4BinaryAlphaBuilder", "df/da9/classG4BinaryAlphaBuilder.html", null ]
      ] ],
      [ "G4VAntiBarionBuilder", "da/dc6/classG4VAntiBarionBuilder.html", [
        [ "G4FTFPAntiBarionBuilder", "de/d77/classG4FTFPAntiBarionBuilder.html", null ],
        [ "G4QGSPAntiBarionBuilder", "db/d22/classG4QGSPAntiBarionBuilder.html", null ]
      ] ],
      [ "G4VDeuteronBuilder", "d4/d82/classG4VDeuteronBuilder.html", [
        [ "G4BinaryDeuteronBuilder", "dc/dc1/classG4BinaryDeuteronBuilder.html", null ],
        [ "G4DeuteronPHPBuilder", "d0/d98/classG4DeuteronPHPBuilder.html", null ]
      ] ],
      [ "G4VHe3Builder", "db/da2/classG4VHe3Builder.html", [
        [ "G4BinaryHe3Builder", "dd/d0c/classG4BinaryHe3Builder.html", null ],
        [ "G4He3PHPBuilder", "d0/df8/classG4He3PHPBuilder.html", null ]
      ] ],
      [ "G4VHyperonBuilder", "dc/d7b/classG4VHyperonBuilder.html", [
        [ "G4HyperonFTFPBuilder", "d0/d79/classG4HyperonFTFPBuilder.html", null ],
        [ "G4HyperonQGSPBuilder", "d6/db6/classG4HyperonQGSPBuilder.html", null ]
      ] ],
      [ "G4VKaonBuilder", "d3/d7c/classG4VKaonBuilder.html", [
        [ "G4BertiniKaonBuilder", "db/dbe/classG4BertiniKaonBuilder.html", null ],
        [ "G4FTFBinaryKaonBuilder", "d9/d85/classG4FTFBinaryKaonBuilder.html", null ],
        [ "G4FTFPKaonBuilder", "da/d9f/classG4FTFPKaonBuilder.html", null ],
        [ "G4QGSBinaryKaonBuilder", "df/d89/classG4QGSBinaryKaonBuilder.html", null ],
        [ "G4QGSPKaonBuilder", "d6/d23/classG4QGSPKaonBuilder.html", null ]
      ] ],
      [ "G4VNeutronBuilder", "d5/d37/classG4VNeutronBuilder.html", [
        [ "G4BertiniNeutronBuilder", "d5/d00/classG4BertiniNeutronBuilder.html", null ],
        [ "G4BinaryNeutronBuilder", "d2/da6/classG4BinaryNeutronBuilder.html", null ],
        [ "G4FTFBinaryNeutronBuilder", "dd/d3c/classG4FTFBinaryNeutronBuilder.html", null ],
        [ "G4FTFPNeutronBuilder", "d6/ddd/classG4FTFPNeutronBuilder.html", null ],
        [ "G4INCLXXNeutronBuilder", "df/dca/classG4INCLXXNeutronBuilder.html", null ],
        [ "G4NeutronLENDBuilder", "d3/d5e/classG4NeutronLENDBuilder.html", null ],
        [ "G4NeutronPHPBuilder", "d2/d19/classG4NeutronPHPBuilder.html", null ],
        [ "G4PrecoNeutronBuilder", "d8/d18/classG4PrecoNeutronBuilder.html", null ],
        [ "G4QGSBinaryNeutronBuilder", "d2/d5b/classG4QGSBinaryNeutronBuilder.html", null ],
        [ "G4QGSPNeutronBuilder", "da/dbc/classG4QGSPNeutronBuilder.html", null ]
      ] ],
      [ "G4VPiKBuilder", "d9/d2f/classG4VPiKBuilder.html", [
        [ "G4BertiniPiKBuilder", "dd/dc3/classG4BertiniPiKBuilder.html", null ],
        [ "G4BinaryPiKBuilder", "d3/d78/classG4BinaryPiKBuilder.html", null ],
        [ "G4FTFBinaryPiKBuilder", "d8/d2e/classG4FTFBinaryPiKBuilder.html", null ],
        [ "G4FTFPPiKBuilder", "da/d5c/classG4FTFPPiKBuilder.html", null ],
        [ "G4QGSBinaryPiKBuilder", "d2/dd4/classG4QGSBinaryPiKBuilder.html", null ],
        [ "G4QGSPPiKBuilder", "d1/d6c/classG4QGSPPiKBuilder.html", null ]
      ] ],
      [ "G4VPionBuilder", "dc/d26/classG4VPionBuilder.html", [
        [ "G4BertiniPionBuilder", "d7/dbf/classG4BertiniPionBuilder.html", null ],
        [ "G4BinaryPionBuilder", "d1/d2b/classG4BinaryPionBuilder.html", null ],
        [ "G4FTFBinaryPionBuilder", "d1/d77/classG4FTFBinaryPionBuilder.html", null ],
        [ "G4FTFPPionBuilder", "da/d7f/classG4FTFPPionBuilder.html", null ],
        [ "G4INCLXXPionBuilder", "d2/db6/classG4INCLXXPionBuilder.html", null ],
        [ "G4QGSBinaryPionBuilder", "db/dc4/classG4QGSBinaryPionBuilder.html", null ],
        [ "G4QGSPPionBuilder", "d2/d1c/classG4QGSPPionBuilder.html", null ]
      ] ],
      [ "G4VProtonBuilder", "dd/d1f/classG4VProtonBuilder.html", [
        [ "G4BertiniProtonBuilder", "d5/d1f/classG4BertiniProtonBuilder.html", null ],
        [ "G4BinaryProtonBuilder", "d2/d98/classG4BinaryProtonBuilder.html", null ],
        [ "G4FTFBinaryProtonBuilder", "da/d90/classG4FTFBinaryProtonBuilder.html", null ],
        [ "G4FTFPProtonBuilder", "df/dcd/classG4FTFPProtonBuilder.html", null ],
        [ "G4INCLXXProtonBuilder", "da/df4/classG4INCLXXProtonBuilder.html", null ],
        [ "G4PrecoProtonBuilder", "d6/d95/classG4PrecoProtonBuilder.html", null ],
        [ "G4ProtonPHPBuilder", "df/da8/classG4ProtonPHPBuilder.html", null ],
        [ "G4QGSBinaryProtonBuilder", "d2/dbc/classG4QGSBinaryProtonBuilder.html", null ],
        [ "G4QGSPLundStrFragmProtonBuilder", "d2/d73/classG4QGSPLundStrFragmProtonBuilder.html", null ],
        [ "G4QGSPProtonBuilder", "dc/dce/classG4QGSPProtonBuilder.html", null ]
      ] ],
      [ "G4VTritonBuilder", "d9/d59/classG4VTritonBuilder.html", [
        [ "G4BinaryTritonBuilder", "d4/df2/classG4BinaryTritonBuilder.html", null ],
        [ "G4TritonPHPBuilder", "d0/d28/classG4TritonPHPBuilder.html", null ]
      ] ]
    ] ],
    [ "G4PhysicsConstructorRegistry", "d7/d95/classG4PhysicsConstructorRegistry.html", null ],
    [ "G4PhysicsListHelper", "d9/d74/classG4PhysicsListHelper.html", null ],
    [ "G4PhysicsListOrderingParameter", "d6/d74/classG4PhysicsListOrderingParameter.html", null ],
    [ "G4PhysicsListWorkspace", "d5/d18/classG4PhysicsListWorkspace.html", null ],
    [ "G4PhysicsModelCatalog", "dd/dfc/classG4PhysicsModelCatalog.html", null ],
    [ "G4PhysicsTableHelper", "dc/d97/classG4PhysicsTableHelper.html", null ],
    [ "G4PhysicsVector", "d7/d2c/classG4PhysicsVector.html", [
      [ "G4PhysicsFreeVector", "d4/dc2/classG4PhysicsFreeVector.html", null ],
      [ "G4PhysicsLinearVector", "d2/dbc/classG4PhysicsLinearVector.html", null ],
      [ "G4PhysicsLogVector", "d4/de8/classG4PhysicsLogVector.html", null ]
    ] ],
    [ "g4alt::G4PhysListFactory", "dc/d6f/classg4alt_1_1G4PhysListFactory.html", null ],
    [ "G4PhysListFactory", "df/d67/classG4PhysListFactory.html", null ],
    [ "G4PhysListRegistry", "db/dcf/classG4PhysListRegistry.html", null ],
    [ "G4PhysListUtil", "d7/dfe/classG4PhysListUtil.html", null ],
    [ "G4ping", "d8/d4e/classG4ping.html", null ],
    [ "G4PixeCrossSectionHandler", "d1/d57/classG4PixeCrossSectionHandler.html", null ],
    [ "G4PlacedPolyhedron", "da/d6c/classG4PlacedPolyhedron.html", null ],
    [ "G4PlotManager", "d5/d87/classG4PlotManager.html", null ],
    [ "G4PlotParameters", "d9/d94/classG4PlotParameters.html", null ],
    [ "G4Plotter", "d3/db1/classG4Plotter.html", null ],
    [ "G4PlotterManager", "d7/d0d/classG4PlotterManager.html", null ],
    [ "G4PlSideData", "db/dec/classG4PlSideData.html", null ],
    [ "G4PolarizationHelper", "d0/d42/classG4PolarizationHelper.html", null ],
    [ "G4PolarizationManager", "db/d08/classG4PolarizationManager.html", null ],
    [ "G4PolarizationTransition", "d3/dab/classG4PolarizationTransition.html", null ],
    [ "G4PolyconeHistorical", "dc/d8f/classG4PolyconeHistorical.html", null ],
    [ "G4PolyconeSideRZ", "d8/d5a/structG4PolyconeSideRZ.html", null ],
    [ "G4PolyhedraHistorical", "d0/d16/classG4PolyhedraHistorical.html", null ],
    [ "G4PolyhedraSideRZ", "da/de4/structG4PolyhedraSideRZ.html", null ],
    [ "G4PolynomialPDF", "df/db0/classG4PolynomialPDF.html", null ],
    [ "G4PolynomialSolver< T, F >", "d7/db4/classG4PolynomialSolver.html", null ],
    [ "G4PolyPhiFaceEdge", "d3/d1d/structG4PolyPhiFaceEdge.html", null ],
    [ "G4PolyPhiFaceVertex", "d8/d02/structG4PolyPhiFaceVertex.html", null ],
    [ "G4AllocatorPool::G4PoolChunk", "dd/d87/classG4AllocatorPool_1_1G4PoolChunk.html", null ],
    [ "G4AllocatorPool::G4PoolLink", "d2/d34/structG4AllocatorPool_1_1G4PoolLink.html", null ],
    [ "G4Pow", "de/d0f/classG4Pow.html", null ],
    [ "G4PreCompoundEmission", "d7/dc3/classG4PreCompoundEmission.html", null ],
    [ "G4PreCompoundFragmentVector", "de/d2d/classG4PreCompoundFragmentVector.html", null ],
    [ "G4PrimaryParticle", "df/d7e/classG4PrimaryParticle.html", null ],
    [ "G4PrimaryTransformer", "d0/d69/classG4PrimaryTransformer.html", null ],
    [ "G4PrimaryVertex", "de/dbd/classG4PrimaryVertex.html", null ],
    [ "G4ProcessAttribute", "d4/de6/classG4ProcessAttribute.html", null ],
    [ "G4ProcessManager", "d5/d2e/classG4ProcessManager.html", null ],
    [ "G4ProcessState_Lock", "dd/d2e/structG4ProcessState__Lock.html", [
      [ "G4VITProcess::G4ProcessState", "da/d16/structG4VITProcess_1_1G4ProcessState.html", [
        [ "G4DNAScavengerProcess::G4DNAScavengerProcessState", "d8/dc4/structG4DNAScavengerProcess_1_1G4DNAScavengerProcessState.html", null ],
        [ "G4DNASecondOrderReaction::SecondOrderReactionState", "d8/d25/structG4DNASecondOrderReaction_1_1SecondOrderReactionState.html", null ],
        [ "G4ITTransportation::G4ITTransportationState", "de/d34/structG4ITTransportation_1_1G4ITTransportationState.html", [
          [ "G4DNABrownianTransportation::G4ITBrownianState", "da/d2e/structG4DNABrownianTransportation_1_1G4ITBrownianState.html", null ]
        ] ],
        [ "G4VITProcess::G4ProcessStateBase< T >", "da/d1b/classG4VITProcess_1_1G4ProcessStateBase.html", null ]
      ] ]
    ] ],
    [ "G4ProcessStateBase", null, [
      [ "G4DNAElectronHoleRecombination::State", "df/d54/structG4DNAElectronHoleRecombination_1_1State.html", null ]
    ] ],
    [ "G4ProcessTable", "d9/df9/classG4ProcessTable.html", null ],
    [ "G4ProcessVector", "d5/da4/classG4ProcessVector.html", null ],
    [ "G4ProcTblElement", "dc/d6a/classG4ProcTblElement.html", null ],
    [ "G4ProductionCuts", "d4/d1f/classG4ProductionCuts.html", null ],
    [ "G4ProductionCutsTable", "d5/dda/classG4ProductionCutsTable.html", null ],
    [ "G4ProfileOp", "df/da9/structG4ProfileOp.html", null ],
    [ "G4Profiler", "d3/d74/classG4Profiler.html", null ],
    [ "G4ProfilerArgs< Category >", "d7/d79/structG4ProfilerArgs.html", null ],
    [ "G4ProfilerConfig< Category >", "d5/d74/classG4ProfilerConfig.html", null ],
    [ "G4ProfilerFunctors< Category, RetT, CommonT >", "d6/d49/structG4ProfilerFunctors.html", null ],
    [ "G4ProfilerObject< Category >", "d9/d4e/structG4ProfilerObject.html", null ],
    [ "G4ProfilerObject< G4ProfileType::Event >", "df/dd2/structG4ProfilerObject_3_01G4ProfileType_1_1Event_01_4.html", null ],
    [ "G4ProfilerObject< G4ProfileType::Run >", "db/d8a/structG4ProfilerObject_3_01G4ProfileType_1_1Run_01_4.html", null ],
    [ "G4ProfilerObject< G4ProfileType::Step >", "d2/da2/structG4ProfilerObject_3_01G4ProfileType_1_1Step_01_4.html", null ],
    [ "G4ProfilerObject< G4ProfileType::Track >", "dd/de9/structG4ProfilerObject_3_01G4ProfileType_1_1Track_01_4.html", null ],
    [ "G4ProfilerObject< G4ProfileType::User >", "d3/d58/structG4ProfilerObject_3_01G4ProfileType_1_1User_01_4.html", null ],
    [ "G4ProfileType", "d3/d30/structG4ProfileType.html", null ],
    [ "G4PropagatorInField", "dd/d94/classG4PropagatorInField.html", null ],
    [ "G4PSTARStopping", "d4/dd1/classG4PSTARStopping.html", null ],
    [ "G4PVData", "dd/dc3/classG4PVData.html", null ],
    [ "G4QGSDiffractiveExcitation", "da/d8f/classG4QGSDiffractiveExcitation.html", [
      [ "G4SingleDiffractiveExcitation", "da/d6b/classG4SingleDiffractiveExcitation.html", null ]
    ] ],
    [ "G4QMDCollision", "d7/d66/classG4QMDCollision.html", null ],
    [ "G4QMDMeanField", "dd/daf/classG4QMDMeanField.html", null ],
    [ "G4QMDParameters", "df/db3/classG4QMDParameters.html", null ],
    [ "G4QMDParticipant", "d9/dbf/classG4QMDParticipant.html", null ],
    [ "G4QMDSystem", "d4/d2c/classG4QMDSystem.html", [
      [ "G4QMDNucleus", "d7/db5/classG4QMDNucleus.html", [
        [ "G4QMDGroundStateNucleus", "d2/d1c/classG4QMDGroundStateNucleus.html", null ]
      ] ]
    ] ],
    [ "G4QuarkExchange", "db/d5f/classG4QuarkExchange.html", null ],
    [ "G4QuasiElRatios", "d8/ddb/classG4QuasiElRatios.html", null ],
    [ "G4RadioactiveDecayChainsFromParent", "d1/de8/classG4RadioactiveDecayChainsFromParent.html", null ],
    [ "G4RadioactiveDecayRatesToDaughter", "d3/dbc/classG4RadioactiveDecayRatesToDaughter.html", null ],
    [ "G4RadioactivityTable", "dc/d4b/classG4RadioactivityTable.html", null ],
    [ "G4RayShooter", "d3/d0d/classG4RayShooter.html", null ],
    [ "G4ReactionProduct", "dd/d77/classG4ReactionProduct.html", null ],
    [ "G4ReduciblePolygon", "d8/d73/classG4ReduciblePolygon.html", null ],
    [ "G4ReduciblePolygonIterator", "db/dd6/classG4ReduciblePolygonIterator.html", null ],
    [ "G4ReferenceCountedHandle< X >", "d4/d7f/classG4ReferenceCountedHandle.html", null ],
    [ "G4ReferenceCountedHandle< G4VTouchable >", "d4/d7f/classG4ReferenceCountedHandle.html", null ],
    [ "G4ReflectionFactory", "d1/db6/classG4ReflectionFactory.html", null ],
    [ "G4Reggeons", "df/dde/classG4Reggeons.html", null ],
    [ "G4Region", "d1/d35/classG4Region.html", null ],
    [ "G4RegionData", "d7/de2/classG4RegionData.html", null ],
    [ "G4RegionModels", "db/d1d/classG4RegionModels.html", null ],
    [ "G4RegularNavigation", "d7/d97/classG4RegularNavigation.html", null ],
    [ "G4RegularNavigationHelper", "d5/d52/classG4RegularNavigationHelper.html", null ],
    [ "G4ReplicaData", "d0/dd2/classG4ReplicaData.html", null ],
    [ "G4ReplicaNavigation", "d7/d8b/classG4ReplicaNavigation.html", null ],
    [ "G4ResonanceID", "d0/d34/classG4ResonanceID.html", null ],
    [ "G4ResonanceNames", "d5/da2/classG4ResonanceNames.html", null ],
    [ "G4ResonancePartialWidth", "da/da7/classG4ResonancePartialWidth.html", [
      [ "G4BaryonPartialWidth", "df/d9e/classG4BaryonPartialWidth.html", null ]
    ] ],
    [ "G4ResonanceWidth", "d0/db4/classG4ResonanceWidth.html", [
      [ "G4BaryonWidth", "d3/de4/classG4BaryonWidth.html", null ]
    ] ],
    [ "G4RootPNtupleDescription", "d9/df1/structG4RootPNtupleDescription.html", null ],
    [ "G4Run", "d2/d10/classG4Run.html", [
      [ "G4RTRun", "d9/d68/classG4RTRun.html", null ]
    ] ],
    [ "G4RunManager", "d4/daa/classG4RunManager.html", [
      [ "G4MTRunManager", "de/d16/classG4MTRunManager.html", [
        [ "G4TaskRunManager", "db/d9e/classG4TaskRunManager.html", null ]
      ] ],
      [ "G4WorkerRunManager", "d3/db3/classG4WorkerRunManager.html", [
        [ "G4WorkerTaskRunManager", "d7/d3e/classG4WorkerTaskRunManager.html", null ]
      ] ]
    ] ],
    [ "G4RunManagerFactory", "d5/de9/classG4RunManagerFactory.html", null ],
    [ "G4RunManagerKernel", "de/d51/classG4RunManagerKernel.html", [
      [ "G4MTRunManagerKernel", "de/da2/classG4MTRunManagerKernel.html", null ],
      [ "G4TaskRunManagerKernel", "d4/d96/classG4TaskRunManagerKernel.html", null ],
      [ "G4WorkerRunManagerKernel", "db/dd3/classG4WorkerRunManagerKernel.html", null ],
      [ "G4WorkerTaskRunManagerKernel", "de/d14/classG4WorkerTaskRunManagerKernel.html", null ]
    ] ],
    [ "G4SafetyHelper", "d3/db4/classG4SafetyHelper.html", null ],
    [ "G4SampleResonance", "dc/de5/classG4SampleResonance.html", null ],
    [ "G4SamplingPostStepAction", "dd/dc6/classG4SamplingPostStepAction.html", null ],
    [ "G4SandiaTable", "d2/d52/classG4SandiaTable.html", null ],
    [ "G4ITNavigator2::G4SaveNavigatorState", "d2/d60/structG4ITNavigator2_1_1G4SaveNavigatorState.html", null ],
    [ "G4Navigator::G4SaveNavigatorState", "d7/d98/structG4Navigator_1_1G4SaveNavigatorState.html", null ],
    [ "G4SBBremTable", "dc/d83/classG4SBBremTable.html", null ],
    [ "G4ScaleTransform", "d3/d58/classG4ScaleTransform.html", null ],
    [ "G4Scene", "d9/d52/classG4Scene.html", null ],
    [ "G4ScoringManager", "df/d55/classG4ScoringManager.html", null ],
    [ "G4ScreeningMottCrossSection", "d8/d3e/classG4ScreeningMottCrossSection.html", null ],
    [ "G4SDManager", "d9/d6b/classG4SDManager.html", null ],
    [ "G4SDStructure", "d1/d43/classG4SDStructure.html", null ],
    [ "G4SensitiveVolumeList", "d1/d83/classG4SensitiveVolumeList.html", null ],
    [ "G4ShellCorrection", "df/d24/classG4ShellCorrection.html", null ],
    [ "G4ShellData", "d7/dee/classG4ShellData.html", null ],
    [ "G4ShellVacancy", "d4/dbf/classG4ShellVacancy.html", null ],
    [ "G4ShiftedGaussian", "d1/d07/classG4ShiftedGaussian.html", null ],
    [ "G4ShortLivedConstructor", "d7/d9f/classG4ShortLivedConstructor.html", null ],
    [ "G4SimpleIntegration", "d9/d59/classG4SimpleIntegration.html", null ],
    [ "G4SimplexDownhill< T >", "d9/dcd/classG4SimplexDownhill.html", null ],
    [ "G4SimplexDownhill< G4ConvergenceTester >", "d9/dcd/classG4SimplexDownhill.html", null ],
    [ "G4SliceTimer", "db/d62/classG4SliceTimer.html", null ],
    [ "G4SmartTrackStack", "d9/d41/classG4SmartTrackStack.html", null ],
    [ "G4SmartVoxelHeader", "d0/dcf/classG4SmartVoxelHeader.html", null ],
    [ "G4SmartVoxelNode", "d9/d16/classG4SmartVoxelNode.html", null ],
    [ "G4SmartVoxelProxy", "df/d68/classG4SmartVoxelProxy.html", null ],
    [ "G4SmartVoxelStat", "d7/d68/classG4SmartVoxelStat.html", null ],
    [ "G4SoftStringBuilder", "dd/d41/classG4SoftStringBuilder.html", null ],
    [ "G4SolidExtentList", "d2/d4c/classG4SolidExtentList.html", null ],
    [ "G4SolidsWorkspace", "d4/dfa/classG4SolidsWorkspace.html", null ],
    [ "G4Solver< Function >", "d4/d33/classG4Solver.html", null ],
    [ "G4SortHelperPtr< A >", "d7/d7e/classG4SortHelperPtr.html", null ],
    [ "G4SPBaryon", "d9/d52/classG4SPBaryon.html", null ],
    [ "G4SPBaryonTable", "d8/d30/classG4SPBaryonTable.html", null ],
    [ "G4VViewer::G4Spline", "d0/d56/classG4VViewer_1_1G4Spline.html", null ],
    [ "G4SPPartonInfo", "df/d08/classG4SPPartonInfo.html", null ],
    [ "G4SPSAngDistribution", "d7/d84/classG4SPSAngDistribution.html", null ],
    [ "G4SPSEneDistribution", "d1/d55/classG4SPSEneDistribution.html", null ],
    [ "G4SPSPosDistribution", "dd/d43/classG4SPSPosDistribution.html", null ],
    [ "G4SPSRandomGenerator", "d8/d0c/classG4SPSRandomGenerator.html", null ],
    [ "G4StableIsotopes", "d7/d13/classG4StableIsotopes.html", null ],
    [ "G4StackedTrack", "df/d31/classG4StackedTrack.html", null ],
    [ "G4StackManager", "d1/d93/classG4StackManager.html", null ],
    [ "G4StatAnalysis", "da/df3/classG4StatAnalysis.html", null ],
    [ "G4StatDouble", "d9/d18/classG4StatDouble.html", null ],
    [ "G4StateManager", "de/dd6/classG4StateManager.html", null ],
    [ "G4StatMFChannel", "da/d4a/classG4StatMFChannel.html", null ],
    [ "G4StatMFFragment", "d9/d1b/classG4StatMFFragment.html", null ],
    [ "G4StatMFMacroChemicalPotential", "d4/d25/classG4StatMFMacroChemicalPotential.html", null ],
    [ "G4StatMFMacroMultiplicity", "d3/d9d/classG4StatMFMacroMultiplicity.html", null ],
    [ "G4StatMFMacroTemperature", "de/d75/classG4StatMFMacroTemperature.html", null ],
    [ "G4StatMFMicroManager", "dd/da4/classG4StatMFMicroManager.html", null ],
    [ "G4StatMFMicroPartition", "d6/dc9/classG4StatMFMicroPartition.html", null ],
    [ "G4StatMFParameters", "d1/d3f/classG4StatMFParameters.html", null ],
    [ "G4Step", "d9/d7a/classG4Step.html", null ],
    [ "G4SteppingManager", "d0/dd0/classG4SteppingManager.html", null ],
    [ "G4StepPoint", "df/d47/classG4StepPoint.html", null ],
    [ "G4STRead", "dd/d4f/classG4STRead.html", null ],
    [ "G4SurfaceProperty", "d9/ded/classG4SurfaceProperty.html", [
      [ "G4OpticalSurface", "d4/d86/classG4OpticalSurface.html", null ]
    ] ],
    [ "G4SurfBits", "d5/d5c/classG4SurfBits.html", null ],
    [ "G4VTwistSurface::G4SurfCurNormal", "d2/dbf/classG4VTwistSurface_1_1G4SurfCurNormal.html", null ],
    [ "G4VTwistSurface::G4SurfSideQuery", "d5/d5a/classG4VTwistSurface_1_1G4SurfSideQuery.html", null ],
    [ "G4TablesForExtrapolator", "dc/dd9/classG4TablesForExtrapolator.html", null ],
    [ "G4TableTemplate< T >", "d2/de5/classG4TableTemplate.html", null ],
    [ "G4TableTemplate< G4ENDFYieldDataContainer >", "d2/de5/classG4TableTemplate.html", null ],
    [ "G4TaskSingletonData< T >", "d9/da9/classG4TaskSingletonData.html", null ],
    [ "G4TaskSingletonDelegator< T >", "dc/dd1/classG4TaskSingletonDelegator.html", null ],
    [ "G4TaskSingletonEvaluator< T >", "d7/db1/structG4TaskSingletonEvaluator.html", null ],
    [ "G4TemplateRNGHelper< T >", "d4/d1d/classG4TemplateRNGHelper.html", null ],
    [ "G4Terminator", "d7/d11/classG4Terminator.html", null ],
    [ "G4TessellatedGeometryAlgorithms", "d1/d49/classG4TessellatedGeometryAlgorithms.html", null ],
    [ "G4VisCommandSceneAddText2D::G4Text2D", "d6/dbe/structG4VisCommandSceneAddText2D_1_1G4Text2D.html", null ],
    [ "G4OpenGLStoredSceneHandler::G4TextPlus", "d9/daf/structG4OpenGLStoredSceneHandler_1_1G4TextPlus.html", null ],
    [ "G4TFileInformation< FT >", "d7/d0f/structG4TFileInformation.html", null ],
    [ "G4TFileManager< FT >", "d9/d1f/classG4TFileManager.html", [
      [ "G4VTFileManager< FT >", "d3/d25/classG4VTFileManager.html", null ]
    ] ],
    [ "G4TFileManager< G4Hdf5File >", "d9/d1f/classG4TFileManager.html", [
      [ "G4VTFileManager< G4Hdf5File >", "d3/d25/classG4VTFileManager.html", null ]
    ] ],
    [ "G4TFileManager< G4RootFile >", "d9/d1f/classG4TFileManager.html", [
      [ "G4VTFileManager< G4RootFile >", "d3/d25/classG4VTFileManager.html", null ]
    ] ],
    [ "G4TFileManager< std::ofstream >", "d9/d1f/classG4TFileManager.html", [
      [ "G4VTFileManager< std::ofstream >", "d3/d25/classG4VTFileManager.html", null ]
    ] ],
    [ "G4tgbDetectorBuilder", "d9/d6c/classG4tgbDetectorBuilder.html", null ],
    [ "G4tgbDetectorConstruction", "da/d27/classG4tgbDetectorConstruction.html", null ],
    [ "G4tgbElement", "d0/d75/classG4tgbElement.html", null ],
    [ "G4tgbGeometryDumper", "de/d7b/classG4tgbGeometryDumper.html", null ],
    [ "G4tgbIsotope", "da/d5d/classG4tgbIsotope.html", null ],
    [ "G4tgbMaterial", "d8/dc1/classG4tgbMaterial.html", [
      [ "G4tgbMaterialMixture", "d4/d99/classG4tgbMaterialMixture.html", [
        [ "G4tgbMaterialMixtureByNoAtoms", "d7/df5/classG4tgbMaterialMixtureByNoAtoms.html", null ],
        [ "G4tgbMaterialMixtureByVolume", "d2/dfb/classG4tgbMaterialMixtureByVolume.html", null ],
        [ "G4tgbMaterialMixtureByWeight", "dd/dfb/classG4tgbMaterialMixtureByWeight.html", null ]
      ] ],
      [ "G4tgbMaterialSimple", "d4/df0/classG4tgbMaterialSimple.html", null ]
    ] ],
    [ "G4tgbMaterialMgr", "d8/dc5/classG4tgbMaterialMgr.html", null ],
    [ "G4tgbRotationMatrix", "d9/d12/classG4tgbRotationMatrix.html", null ],
    [ "G4tgbRotationMatrixMgr", "da/d3c/classG4tgbRotationMatrixMgr.html", null ],
    [ "G4tgbVolume", "d1/d78/classG4tgbVolume.html", null ],
    [ "G4tgbVolumeMgr", "de/dd0/classG4tgbVolumeMgr.html", null ],
    [ "G4tgrElement", "d0/de3/classG4tgrElement.html", [
      [ "G4tgrElementFromIsotopes", "d3/d42/classG4tgrElementFromIsotopes.html", null ],
      [ "G4tgrElementSimple", "de/d3e/classG4tgrElementSimple.html", null ]
    ] ],
    [ "G4tgrFileIn", "d3/d5f/classG4tgrFileIn.html", null ],
    [ "G4tgrFileReader", "d5/da5/classG4tgrFileReader.html", null ],
    [ "G4tgrIsotope", "d6/da9/classG4tgrIsotope.html", null ],
    [ "G4tgrLineProcessor", "d6/dfd/classG4tgrLineProcessor.html", null ],
    [ "G4tgrMaterial", "d8/db9/classG4tgrMaterial.html", [
      [ "G4tgrMaterialMixture", "d3/df7/classG4tgrMaterialMixture.html", null ],
      [ "G4tgrMaterialSimple", "d9/d5b/classG4tgrMaterialSimple.html", null ]
    ] ],
    [ "G4tgrMaterialFactory", "dc/dd0/classG4tgrMaterialFactory.html", null ],
    [ "G4tgrParameterMgr", "d3/d15/classG4tgrParameterMgr.html", null ],
    [ "G4tgrPlace", "dd/dc9/classG4tgrPlace.html", [
      [ "G4tgrPlaceDivRep", "dc/d81/classG4tgrPlaceDivRep.html", null ],
      [ "G4tgrPlaceParameterisation", "d1/d47/classG4tgrPlaceParameterisation.html", null ],
      [ "G4tgrPlaceSimple", "d8/d3d/classG4tgrPlaceSimple.html", null ]
    ] ],
    [ "G4tgrRotationMatrix", "de/dd5/classG4tgrRotationMatrix.html", null ],
    [ "G4tgrRotationMatrixFactory", "df/d04/classG4tgrRotationMatrixFactory.html", null ],
    [ "G4tgrSolid", "d8/d3d/classG4tgrSolid.html", [
      [ "G4tgrSolidBoolean", "d9/db9/classG4tgrSolidBoolean.html", null ],
      [ "G4tgrSolidMultiUnion", "d4/db6/classG4tgrSolidMultiUnion.html", null ],
      [ "G4tgrSolidScaled", "dd/db3/classG4tgrSolidScaled.html", null ]
    ] ],
    [ "G4tgrUtils", "d1/dc7/classG4tgrUtils.html", null ],
    [ "G4tgrVolume", "d5/df8/classG4tgrVolume.html", [
      [ "G4tgrVolumeAssembly", "d0/d3f/classG4tgrVolumeAssembly.html", null ],
      [ "G4tgrVolumeDivision", "d5/dcd/classG4tgrVolumeDivision.html", null ]
    ] ],
    [ "G4tgrVolumeMgr", "dc/df7/classG4tgrVolumeMgr.html", null ],
    [ "G4TheRayTracer", "d2/dbf/classG4TheRayTracer.html", [
      [ "G4TheMTRayTracer", "d8/dc8/classG4TheMTRayTracer.html", null ]
    ] ],
    [ "G4THnManager< HT >", "d7/def/classG4THnManager.html", null ],
    [ "G4THnManager< tools::histo::h1d >", "d7/def/classG4THnManager.html", [
      [ "G4H1ToolsManager", "df/d39/classG4H1ToolsManager.html", null ]
    ] ],
    [ "G4THnManager< tools::histo::h2d >", "d7/def/classG4THnManager.html", [
      [ "G4H2ToolsManager", "d7/d26/classG4H2ToolsManager.html", null ]
    ] ],
    [ "G4THnManager< tools::histo::h3d >", "d7/def/classG4THnManager.html", [
      [ "G4H3ToolsManager", "d4/de3/classG4H3ToolsManager.html", null ]
    ] ],
    [ "G4THnManager< tools::histo::p1d >", "d7/def/classG4THnManager.html", [
      [ "G4P1ToolsManager", "dc/d19/classG4P1ToolsManager.html", null ]
    ] ],
    [ "G4THnManager< tools::histo::p2d >", "d7/def/classG4THnManager.html", [
      [ "G4P2ToolsManager", "da/dd0/classG4P2ToolsManager.html", null ]
    ] ],
    [ "G4ThreadLocalSingleton< void >", "dc/d54/classG4ThreadLocalSingleton_3_01void_01_4.html", null ],
    [ "G4GDMLWriteSolids::G4ThreeVectorCompare", "d9/de5/classG4GDMLWriteSolids_1_1G4ThreeVectorCompare.html", null ],
    [ "G4Timer", "da/d4f/classG4Timer.html", null ],
    [ "G4TNtupleDescription< NT, FT >", "d3/dae/structG4TNtupleDescription.html", null ],
    [ "G4TNtupleDescription< tools::wroot::ntuple, G4RootFile >", "d3/dae/structG4TNtupleDescription.html", null ],
    [ "G4Tokenizer", "df/ded/classG4Tokenizer.html", null ],
    [ "G4Track", "d1/d33/classG4Track.html", null ],
    [ "G4TrackingInformation", "d9/d0a/classG4TrackingInformation.html", null ],
    [ "G4TrackingManager", "dd/d71/classG4TrackingManager.html", null ],
    [ "G4TrackLogger", "d3/dc8/classG4TrackLogger.html", null ],
    [ "G4TrackStateManager", "de/d2e/classG4TrackStateManager.html", null ],
    [ "G4TrackTerminator", "d3/d35/classG4TrackTerminator.html", null ],
    [ "G4TrajectoryContainer", "d2/dcc/classG4TrajectoryContainer.html", null ],
    [ "G4TransportationLogger", "d9/df0/classG4TransportationLogger.html", null ],
    [ "G4TransportationManager", "d1/dd0/classG4TransportationManager.html", null ],
    [ "G4TrialsCounter", "d0/d96/classG4TrialsCounter.html", null ],
    [ "G4TRNtupleDescription< NT >", "d0/d25/structG4TRNtupleDescription.html", null ],
    [ "G4TwoBodyAngularDist", "d6/d7e/classG4TwoBodyAngularDist.html", null ],
    [ "G4TwoPeaksXS", "d3/d9b/structG4TwoPeaksXS.html", null ],
    [ "G4TWorkspacePool< T >", "d9/dcc/classG4TWorkspacePool.html", null ],
    [ "G4TypeKey", "d0/dd9/classG4TypeKey.html", [
      [ "G4TypeKeyT< T >", "d9/d64/classG4TypeKeyT.html", null ]
    ] ],
    [ "G4UCNMicroRoughnessHelper", "d6/dac/classG4UCNMicroRoughnessHelper.html", null ],
    [ "G4UIaliasList", "dc/dc1/classG4UIaliasList.html", null ],
    [ "G4UIArrayString", "d3/da3/classG4UIArrayString.html", null ],
    [ "G4UIbridge", "d4/ddd/classG4UIbridge.html", null ],
    [ "G4UIcommand", "d2/dd2/classG4UIcommand.html", [
      [ "G4UIcmdWith3Vector", "de/d5f/classG4UIcmdWith3Vector.html", null ],
      [ "G4UIcmdWith3VectorAndUnit", "d0/db2/classG4UIcmdWith3VectorAndUnit.html", null ],
      [ "G4UIcmdWithABool", "d8/d06/classG4UIcmdWithABool.html", null ],
      [ "G4UIcmdWithADouble", "df/d21/classG4UIcmdWithADouble.html", null ],
      [ "G4UIcmdWithADoubleAndUnit", "d7/d89/classG4UIcmdWithADoubleAndUnit.html", null ],
      [ "G4UIcmdWithALongInt", "d9/dca/classG4UIcmdWithALongInt.html", null ],
      [ "G4UIcmdWithAString", "d0/d84/classG4UIcmdWithAString.html", null ],
      [ "G4UIcmdWithAnInteger", "d0/da4/classG4UIcmdWithAnInteger.html", null ],
      [ "G4UIcmdWithNucleusLimits", "dc/dc7/classG4UIcmdWithNucleusLimits.html", null ],
      [ "G4UIcmdWithoutParameter", "dc/d4a/classG4UIcmdWithoutParameter.html", null ],
      [ "G4UIdirectory", "d3/dd7/classG4UIdirectory.html", null ]
    ] ],
    [ "G4UIcommandTree", "d3/d31/classG4UIcommandTree.html", null ],
    [ "G4UIExecutive", "d1/d6d/classG4UIExecutive.html", null ],
    [ "G4UImessenger", "d7/d0e/classG4UImessenger.html", [
      [ "G4VModelCommand< M >", "d9/d8f/classG4VModelCommand.html", [
        [ "G4ModelCmdApplyBool< M >", "d1/db6/classG4ModelCmdApplyBool.html", [
          [ "G4ModelCmdActive< M >", "d2/d91/classG4ModelCmdActive.html", null ],
          [ "G4ModelCmdDraw< M >", "d1/de3/classG4ModelCmdDraw.html", null ],
          [ "G4ModelCmdInvert< M >", "dd/d17/classG4ModelCmdInvert.html", null ],
          [ "G4ModelCmdSetAuxPtsVisible< M >", "de/d62/classG4ModelCmdSetAuxPtsVisible.html", null ],
          [ "G4ModelCmdSetDrawAuxPts< M >", "dc/df0/classG4ModelCmdSetDrawAuxPts.html", null ],
          [ "G4ModelCmdSetDrawLine< M >", "d7/df9/classG4ModelCmdSetDrawLine.html", null ],
          [ "G4ModelCmdSetDrawStepPts< M >", "df/d23/classG4ModelCmdSetDrawStepPts.html", null ],
          [ "G4ModelCmdSetLineVisible< M >", "d3/d98/classG4ModelCmdSetLineVisible.html", null ],
          [ "G4ModelCmdSetStepPtsVisible< M >", "d8/dcb/classG4ModelCmdSetStepPtsVisible.html", null ],
          [ "G4ModelCmdVerbose< M >", "d2/d94/classG4ModelCmdVerbose.html", null ]
        ] ],
        [ "G4ModelCmdApplyColour< M >", "d2/da3/classG4ModelCmdApplyColour.html", [
          [ "G4ModelCmdSetAuxPtsColour< M >", "dc/d9e/classG4ModelCmdSetAuxPtsColour.html", null ],
          [ "G4ModelCmdSetDefaultColour< M >", "d2/d50/classG4ModelCmdSetDefaultColour.html", null ],
          [ "G4ModelCmdSetLineColour< M >", "d1/d8b/classG4ModelCmdSetLineColour.html", null ],
          [ "G4ModelCmdSetStepPtsColour< M >", "d7/d99/classG4ModelCmdSetStepPtsColour.html", null ]
        ] ],
        [ "G4ModelCmdApplyDouble< M >", "d9/d4b/classG4ModelCmdApplyDouble.html", [
          [ "G4ModelCmdSetLineWidth< M >", "d4/d75/classG4ModelCmdSetLineWidth.html", null ]
        ] ],
        [ "G4ModelCmdApplyDoubleAndUnit< M >", "db/da1/classG4ModelCmdApplyDoubleAndUnit.html", [
          [ "G4ModelCmdSetTimeSliceInterval< M >", "d1/da8/classG4ModelCmdSetTimeSliceInterval.html", null ]
        ] ],
        [ "G4ModelCmdApplyInteger< M >", "db/d48/classG4ModelCmdApplyInteger.html", [
          [ "G4ModelCmdAddInt< M >", "d9/d61/classG4ModelCmdAddInt.html", null ]
        ] ],
        [ "G4ModelCmdApplyNull< M >", "d5/d41/classG4ModelCmdApplyNull.html", [
          [ "G4ModelCmdReset< M >", "d0/d6e/classG4ModelCmdReset.html", null ]
        ] ],
        [ "G4ModelCmdApplyString< M >", "d9/d7b/classG4ModelCmdApplyString.html", [
          [ "G4ModelCmdAddInterval< M >", "d1/d99/classG4ModelCmdAddInterval.html", null ],
          [ "G4ModelCmdAddIntervalContext< M >", "db/d5d/classG4ModelCmdAddIntervalContext.html", null ],
          [ "G4ModelCmdAddString< M >", "d0/d4f/classG4ModelCmdAddString.html", null ],
          [ "G4ModelCmdAddValue< M >", "d0/d00/classG4ModelCmdAddValue.html", null ],
          [ "G4ModelCmdAddValueContext< M >", "da/d16/classG4ModelCmdAddValueContext.html", null ],
          [ "G4ModelCmdSetAuxPtsFillStyle< M >", "d8/dbe/classG4ModelCmdSetAuxPtsFillStyle.html", null ],
          [ "G4ModelCmdSetAuxPtsSize< M >", "d5/d69/classG4ModelCmdSetAuxPtsSize.html", null ],
          [ "G4ModelCmdSetAuxPtsSizeType< M >", "df/d84/classG4ModelCmdSetAuxPtsSizeType.html", null ],
          [ "G4ModelCmdSetAuxPtsType< M >", "de/d3d/classG4ModelCmdSetAuxPtsType.html", null ],
          [ "G4ModelCmdSetStepPtsFillStyle< M >", "db/da0/classG4ModelCmdSetStepPtsFillStyle.html", null ],
          [ "G4ModelCmdSetStepPtsSize< M >", "df/ddd/classG4ModelCmdSetStepPtsSize.html", null ],
          [ "G4ModelCmdSetStepPtsSizeType< M >", "df/d26/classG4ModelCmdSetStepPtsSizeType.html", null ],
          [ "G4ModelCmdSetStepPtsType< M >", "d5/d03/classG4ModelCmdSetStepPtsType.html", null ],
          [ "G4ModelCmdSetString< M >", "d2/d8b/classG4ModelCmdSetString.html", null ]
        ] ],
        [ "G4ModelCmdApplyStringColour< M >", "d0/d87/classG4ModelCmdApplyStringColour.html", [
          [ "G4ModelCmdSetStringColour< M >", "d1/dd1/classG4ModelCmdSetStringColour.html", null ]
        ] ]
      ] ],
      [ "G4ASCIITreeMessenger", "d3/d41/classG4ASCIITreeMessenger.html", null ],
      [ "G4AdjointSimMessenger", "d6/d97/classG4AdjointSimMessenger.html", null ],
      [ "G4AnalysisMessenger", "da/d74/classG4AnalysisMessenger.html", null ],
      [ "G4CascadeParamMessenger", "d5/d95/classG4CascadeParamMessenger.html", null ],
      [ "G4DMmessenger", "d9/dbc/classG4DMmessenger.html", null ],
      [ "G4DNAChemistryManager", "dc/d37/classG4DNAChemistryManager.html", null ],
      [ "G4DecayTableMessenger", "d7/d75/classG4DecayTableMessenger.html", null ],
      [ "G4DeexParametersMessenger", "dc/ded/classG4DeexParametersMessenger.html", null ],
      [ "G4EmExtraParametersMessenger", "d1/d53/classG4EmExtraParametersMessenger.html", null ],
      [ "G4EmLowEParametersMessenger", "de/d92/classG4EmLowEParametersMessenger.html", null ],
      [ "G4EmMessenger", "d7/d90/classG4EmMessenger.html", null ],
      [ "G4EmParametersMessenger", "dd/dd1/classG4EmParametersMessenger.html", null ],
      [ "G4ErrorMessenger", "d5/dfd/classG4ErrorMessenger.html", null ],
      [ "G4EvManMessenger", "d0/d84/classG4EvManMessenger.html", null ],
      [ "G4FastSimulationMessenger", "d3/d05/classG4FastSimulationMessenger.html", null ],
      [ "G4FileMessenger", "dc/d6a/classG4FileMessenger.html", null ],
      [ "G4GDMLMessenger", "de/d45/classG4GDMLMessenger.html", null ],
      [ "G4GMocrenMessenger", "dd/d6e/classG4GMocrenMessenger.html", null ],
      [ "G4GeneralParticleSourceMessenger", "d9/d1c/classG4GeneralParticleSourceMessenger.html", null ],
      [ "G4GenericMessenger", "d1/d92/classG4GenericMessenger.html", null ],
      [ "G4GeometryMessenger", "d7/d3e/classG4GeometryMessenger.html", null ],
      [ "G4GlobalMagFieldMessenger", "d9/d64/classG4GlobalMagFieldMessenger.html", null ],
      [ "G4H1Messenger", "dc/d14/classG4H1Messenger.html", null ],
      [ "G4H2Messenger", "df/d35/classG4H2Messenger.html", null ],
      [ "G4H3Messenger", "dd/d87/classG4H3Messenger.html", null ],
      [ "G4HadronicEPTestMessenger", "d1/dc1/classG4HadronicEPTestMessenger.html", null ],
      [ "G4HadronicParametersMessenger", "d2/d85/classG4HadronicParametersMessenger.html", null ],
      [ "G4HepRepMessenger", "d1/d70/classG4HepRepMessenger.html", null ],
      [ "G4HnMessenger", "d3/de8/classG4HnMessenger.html", null ],
      [ "G4INCLXXInterfaceMessenger", "d7/d7e/classG4INCLXXInterfaceMessenger.html", null ],
      [ "G4InteractorMessenger", "d8/d3a/classG4InteractorMessenger.html", null ],
      [ "G4LocalThreadCoutMessenger", "dd/d5c/classG4LocalThreadCoutMessenger.html", null ],
      [ "G4MatScanMessenger", "d4/d36/classG4MatScanMessenger.html", null ],
      [ "G4ModelCmdCreateContextDir< M >", "d7/d19/classG4ModelCmdCreateContextDir.html", null ],
      [ "G4MoleculeGunMessenger", "d0/d6b/classG4MoleculeGunMessenger.html", null ],
      [ "G4MoleculeShootMessenger", "dd/dad/classG4MoleculeShootMessenger.html", null ],
      [ "G4NeutronKillerMessenger", "db/d88/classG4NeutronKillerMessenger.html", null ],
      [ "G4NistMessenger", "d4/de2/classG4NistMessenger.html", null ],
      [ "G4NtupleMessenger", "d0/dba/classG4NtupleMessenger.html", null ],
      [ "G4NuclideTableMessenger", "d9/d30/classG4NuclideTableMessenger.html", null ],
      [ "G4OpenGLViewerMessenger", "d7/d17/classG4OpenGLViewerMessenger.html", null ],
      [ "G4OpenGLXmViewerMessenger", "db/d93/classG4OpenGLXmViewerMessenger.html", null ],
      [ "G4OpenInventorXtExaminerViewerMessenger", "d9/d3b/classG4OpenInventorXtExaminerViewerMessenger.html", null ],
      [ "G4OpticalParametersMessenger", "d5/d2b/classG4OpticalParametersMessenger.html", null ],
      [ "G4P1Messenger", "d2/d77/classG4P1Messenger.html", null ],
      [ "G4P2Messenger", "d2/dff/classG4P2Messenger.html", null ],
      [ "G4ParticleGunMessenger", "d0/db6/classG4ParticleGunMessenger.html", null ],
      [ "G4ParticleHPMessenger", "dc/d2c/classG4ParticleHPMessenger.html", null ],
      [ "G4ParticleMessenger", "db/d05/classG4ParticleMessenger.html", null ],
      [ "G4ParticlePropertyMessenger", "d7/d07/classG4ParticlePropertyMessenger.html", null ],
      [ "G4PersistencyCenterMessenger", "d2/d99/classG4PersistencyCenterMessenger.html", null ],
      [ "G4PhysListFactoryMessenger", "d7/da5/classG4PhysListFactoryMessenger.html", null ],
      [ "G4PlotMessenger", "d7/d2b/classG4PlotMessenger.html", null ],
      [ "G4PlotterManager::Messenger", "db/d51/classG4PlotterManager_1_1Messenger.html", null ],
      [ "G4PolarizationMessenger", "dc/d4f/classG4PolarizationMessenger.html", null ],
      [ "G4ProcessManagerMessenger", "d9/d8c/classG4ProcessManagerMessenger.html", null ],
      [ "G4ProcessTableMessenger", "db/de4/classG4ProcessTableMessenger.html", null ],
      [ "G4ProductionCutsTableMessenger", "d9/de3/classG4ProductionCutsTableMessenger.html", null ],
      [ "G4ProfilerMessenger", "dd/dfb/classG4ProfilerMessenger.html", null ],
      [ "G4RTMessenger", "d9/d5c/classG4RTMessenger.html", null ],
      [ "G4RadioactivationMessenger", "dd/daf/classG4RadioactivationMessenger.html", null ],
      [ "G4RadioactiveDecayMessenger", "d4/d3b/classG4RadioactiveDecayMessenger.html", null ],
      [ "G4ReactionTableMessenger", "d9/d62/classG4ReactionTableMessenger.html", null ],
      [ "G4RunMessenger", "d7/dcf/classG4RunMessenger.html", null ],
      [ "G4SDmessenger", "df/d5c/classG4SDmessenger.html", null ],
      [ "G4SchedulerMessenger", "d1/db4/classG4SchedulerMessenger.html", null ],
      [ "G4ScoreQuantityMessenger", "d1/d8b/classG4ScoreQuantityMessenger.html", null ],
      [ "G4ScoringMessenger", "d9/d02/classG4ScoringMessenger.html", null ],
      [ "G4StackingMessenger", "d0/dcb/classG4StackingMessenger.html", null ],
      [ "G4TScoreNtupleWriterMessenger< T >", "dc/d38/classG4TScoreNtupleWriterMessenger.html", null ],
      [ "G4ToolsAnalysisMessenger", "d3/d51/classG4ToolsAnalysisMessenger.html", null ],
      [ "G4TrackingMessenger", "de/d57/classG4TrackingMessenger.html", null ],
      [ "G4UCNBoundaryProcessMessenger", "de/dc7/classG4UCNBoundaryProcessMessenger.html", null ],
      [ "G4UIcontrolMessenger", "d0/dad/classG4UIcontrolMessenger.html", null ],
      [ "G4UnitsMessenger", "dc/da4/classG4UnitsMessenger.html", null ],
      [ "G4UserPhysicsListMessenger", "dd/d3e/classG4UserPhysicsListMessenger.html", null ],
      [ "G4VITSteppingVerbose", "da/df0/classG4VITSteppingVerbose.html", [
        [ "G4ITSteppingVerbose", "d6/d8a/classG4ITSteppingVerbose.html", null ]
      ] ],
      [ "G4VModelCommand< T >", "d9/d8f/classG4VModelCommand.html", null ],
      [ "G4VVisCommand", "d8/d2c/classG4VVisCommand.html", [
        [ "G4ToolsSGSceneHandler::Messenger", "d7/de0/classG4ToolsSGSceneHandler_1_1Messenger.html", null ],
        [ "G4ToolsSGViewer< SG_SESSION, SG_VIEWER >::Messenger", "dc/d05/classG4ToolsSGViewer_1_1Messenger.html", null ],
        [ "G4VVisCommandGeometry", "d4/d41/classG4VVisCommandGeometry.html", [
          [ "G4VVisCommandGeometrySet", "de/dee/classG4VVisCommandGeometrySet.html", [
            [ "G4VisCommandGeometrySetColour", "d9/dc0/classG4VisCommandGeometrySetColour.html", null ],
            [ "G4VisCommandGeometrySetDaughtersInvisible", "d1/dc0/classG4VisCommandGeometrySetDaughtersInvisible.html", null ],
            [ "G4VisCommandGeometrySetForceAuxEdgeVisible", "d2/d8b/classG4VisCommandGeometrySetForceAuxEdgeVisible.html", null ],
            [ "G4VisCommandGeometrySetForceCloud", "da/d99/classG4VisCommandGeometrySetForceCloud.html", null ],
            [ "G4VisCommandGeometrySetForceLineSegmentsPerCircle", "d6/db5/classG4VisCommandGeometrySetForceLineSegmentsPerCircle.html", null ],
            [ "G4VisCommandGeometrySetForceSolid", "d7/d1d/classG4VisCommandGeometrySetForceSolid.html", null ],
            [ "G4VisCommandGeometrySetForceWireframe", "d9/d10/classG4VisCommandGeometrySetForceWireframe.html", null ],
            [ "G4VisCommandGeometrySetLineStyle", "dc/dff/classG4VisCommandGeometrySetLineStyle.html", null ],
            [ "G4VisCommandGeometrySetLineWidth", "d6/d0c/classG4VisCommandGeometrySetLineWidth.html", null ],
            [ "G4VisCommandGeometrySetVisibility", "de/d88/classG4VisCommandGeometrySetVisibility.html", null ]
          ] ],
          [ "G4VisCommandGeometryList", "d9/d9f/classG4VisCommandGeometryList.html", null ],
          [ "G4VisCommandGeometryRestore", "de/de7/classG4VisCommandGeometryRestore.html", null ]
        ] ],
        [ "G4VVisCommandScene", "dc/d7b/classG4VVisCommandScene.html", [
          [ "G4VisCommandSceneActivateModel", "df/d1d/classG4VisCommandSceneActivateModel.html", null ],
          [ "G4VisCommandSceneAddArrow", "dd/ded/classG4VisCommandSceneAddArrow.html", null ],
          [ "G4VisCommandSceneAddArrow2D", "d9/de7/classG4VisCommandSceneAddArrow2D.html", null ],
          [ "G4VisCommandSceneAddAxes", "d9/d49/classG4VisCommandSceneAddAxes.html", null ],
          [ "G4VisCommandSceneAddDate", "dc/d32/classG4VisCommandSceneAddDate.html", null ],
          [ "G4VisCommandSceneAddDigis", "d9/d75/classG4VisCommandSceneAddDigis.html", null ],
          [ "G4VisCommandSceneAddElectricField", "df/d20/classG4VisCommandSceneAddElectricField.html", null ],
          [ "G4VisCommandSceneAddEventID", "de/d05/classG4VisCommandSceneAddEventID.html", null ],
          [ "G4VisCommandSceneAddExtent", "dc/d9c/classG4VisCommandSceneAddExtent.html", null ],
          [ "G4VisCommandSceneAddFrame", "d6/dc0/classG4VisCommandSceneAddFrame.html", null ],
          [ "G4VisCommandSceneAddGPS", "d0/d77/classG4VisCommandSceneAddGPS.html", null ],
          [ "G4VisCommandSceneAddGhosts", "d0/d55/classG4VisCommandSceneAddGhosts.html", null ],
          [ "G4VisCommandSceneAddHits", "db/da2/classG4VisCommandSceneAddHits.html", null ],
          [ "G4VisCommandSceneAddLine", "db/d7f/classG4VisCommandSceneAddLine.html", null ],
          [ "G4VisCommandSceneAddLine2D", "d0/dea/classG4VisCommandSceneAddLine2D.html", null ],
          [ "G4VisCommandSceneAddLocalAxes", "d7/d60/classG4VisCommandSceneAddLocalAxes.html", null ],
          [ "G4VisCommandSceneAddLogicalVolume", "dd/d63/classG4VisCommandSceneAddLogicalVolume.html", null ],
          [ "G4VisCommandSceneAddLogo", "da/d12/classG4VisCommandSceneAddLogo.html", null ],
          [ "G4VisCommandSceneAddLogo2D", "dc/d32/classG4VisCommandSceneAddLogo2D.html", null ],
          [ "G4VisCommandSceneAddMagneticField", "df/d9d/classG4VisCommandSceneAddMagneticField.html", null ],
          [ "G4VisCommandSceneAddPSHits", "d3/dd7/classG4VisCommandSceneAddPSHits.html", null ],
          [ "G4VisCommandSceneAddPlotter", "d7/d1b/classG4VisCommandSceneAddPlotter.html", null ],
          [ "G4VisCommandSceneAddScale", "d2/d05/classG4VisCommandSceneAddScale.html", null ],
          [ "G4VisCommandSceneAddText", "d5/d9b/classG4VisCommandSceneAddText.html", null ],
          [ "G4VisCommandSceneAddText2D", "d1/d21/classG4VisCommandSceneAddText2D.html", null ],
          [ "G4VisCommandSceneAddTrajectories", "d4/d1e/classG4VisCommandSceneAddTrajectories.html", null ],
          [ "G4VisCommandSceneAddUserAction", "d0/d72/classG4VisCommandSceneAddUserAction.html", null ],
          [ "G4VisCommandSceneAddVolume", "df/da1/classG4VisCommandSceneAddVolume.html", null ],
          [ "G4VisCommandSceneCreate", "da/da1/classG4VisCommandSceneCreate.html", null ],
          [ "G4VisCommandSceneEndOfEventAction", "d7/d4b/classG4VisCommandSceneEndOfEventAction.html", null ],
          [ "G4VisCommandSceneEndOfRunAction", "dd/d92/classG4VisCommandSceneEndOfRunAction.html", null ],
          [ "G4VisCommandSceneList", "d3/d17/classG4VisCommandSceneList.html", null ],
          [ "G4VisCommandSceneNotifyHandlers", "dc/d98/classG4VisCommandSceneNotifyHandlers.html", null ],
          [ "G4VisCommandSceneRemoveModel", "d3/da2/classG4VisCommandSceneRemoveModel.html", null ],
          [ "G4VisCommandSceneSelect", "d5/d0b/classG4VisCommandSceneSelect.html", null ],
          [ "G4VisCommandSceneShowExtents", "d7/d9d/classG4VisCommandSceneShowExtents.html", null ]
        ] ],
        [ "G4VisCommandAbortReviewKeptEvents", "d4/d2e/classG4VisCommandAbortReviewKeptEvents.html", null ],
        [ "G4VisCommandDrawLogicalVolume", "df/d01/classG4VisCommandDrawLogicalVolume.html", null ],
        [ "G4VisCommandDrawOnlyToBeKeptEvents", "da/d1a/classG4VisCommandDrawOnlyToBeKeptEvents.html", null ],
        [ "G4VisCommandDrawTree", "d1/df0/classG4VisCommandDrawTree.html", null ],
        [ "G4VisCommandDrawView", "d0/da2/classG4VisCommandDrawView.html", null ],
        [ "G4VisCommandDrawVolume", "d6/dec/classG4VisCommandDrawVolume.html", null ],
        [ "G4VisCommandEnable", "da/dc1/classG4VisCommandEnable.html", null ],
        [ "G4VisCommandInitialize", "d9/d34/classG4VisCommandInitialize.html", null ],
        [ "G4VisCommandList", "d1/dfe/classG4VisCommandList.html", null ],
        [ "G4VisCommandModelCreate< Factory >", "d5/db4/classG4VisCommandModelCreate.html", null ],
        [ "G4VisCommandOpen", "db/d2c/classG4VisCommandOpen.html", null ],
        [ "G4VisCommandReviewKeptEvents", "da/d3f/classG4VisCommandReviewKeptEvents.html", null ],
        [ "G4VisCommandSceneHandlerAttach", "d9/d7b/classG4VisCommandSceneHandlerAttach.html", null ],
        [ "G4VisCommandSceneHandlerCreate", "d4/db9/classG4VisCommandSceneHandlerCreate.html", null ],
        [ "G4VisCommandSceneHandlerList", "df/d51/classG4VisCommandSceneHandlerList.html", null ],
        [ "G4VisCommandSceneHandlerSelect", "d5/d19/classG4VisCommandSceneHandlerSelect.html", null ],
        [ "G4VisCommandSetArrow3DLineSegmentsPerCircle", "d1/dfe/classG4VisCommandSetArrow3DLineSegmentsPerCircle.html", null ],
        [ "G4VisCommandSetColour", "dd/dd3/classG4VisCommandSetColour.html", null ],
        [ "G4VisCommandSetExtentForField", "db/d93/classG4VisCommandSetExtentForField.html", null ],
        [ "G4VisCommandSetLineWidth", "da/d28/classG4VisCommandSetLineWidth.html", null ],
        [ "G4VisCommandSetTextColour", "d1/d9f/classG4VisCommandSetTextColour.html", null ],
        [ "G4VisCommandSetTextLayout", "dc/df2/classG4VisCommandSetTextLayout.html", null ],
        [ "G4VisCommandSetTextSize", "d1/d16/classG4VisCommandSetTextSize.html", null ],
        [ "G4VisCommandSetTouchable", "d7/df4/classG4VisCommandSetTouchable.html", null ],
        [ "G4VisCommandSetVolumeForField", "d6/d6a/classG4VisCommandSetVolumeForField.html", null ],
        [ "G4VisCommandSpecify", "d7/d74/classG4VisCommandSpecify.html", null ],
        [ "G4VisCommandVerbose", "d4/d43/classG4VisCommandVerbose.html", null ],
        [ "G4VisCommandViewerAddCutawayPlane", "df/de9/classG4VisCommandViewerAddCutawayPlane.html", null ],
        [ "G4VisCommandViewerCentreOn", "d3/d7c/classG4VisCommandViewerCentreOn.html", null ],
        [ "G4VisCommandViewerChangeCutawayPlane", "d0/da8/classG4VisCommandViewerChangeCutawayPlane.html", null ],
        [ "G4VisCommandViewerClear", "d0/de9/classG4VisCommandViewerClear.html", null ],
        [ "G4VisCommandViewerClearCutawayPlanes", "d5/ddb/classG4VisCommandViewerClearCutawayPlanes.html", null ],
        [ "G4VisCommandViewerClearTransients", "d8/d3f/classG4VisCommandViewerClearTransients.html", null ],
        [ "G4VisCommandViewerClearVisAttributesModifiers", "dc/db2/classG4VisCommandViewerClearVisAttributesModifiers.html", null ],
        [ "G4VisCommandViewerClone", "d1/d5a/classG4VisCommandViewerClone.html", null ],
        [ "G4VisCommandViewerColourByDensity", "d3/daa/classG4VisCommandViewerColourByDensity.html", null ],
        [ "G4VisCommandViewerCopyViewFrom", "db/d55/classG4VisCommandViewerCopyViewFrom.html", null ],
        [ "G4VisCommandViewerCreate", "d7/db8/classG4VisCommandViewerCreate.html", null ],
        [ "G4VisCommandViewerDefaultHiddenEdge", "d9/d47/classG4VisCommandViewerDefaultHiddenEdge.html", null ],
        [ "G4VisCommandViewerDefaultStyle", "d8/d7f/classG4VisCommandViewerDefaultStyle.html", null ],
        [ "G4VisCommandViewerDolly", "d5/d5c/classG4VisCommandViewerDolly.html", null ],
        [ "G4VisCommandViewerFlush", "dd/d39/classG4VisCommandViewerFlush.html", null ],
        [ "G4VisCommandViewerInterpolate", "d2/d80/classG4VisCommandViewerInterpolate.html", null ],
        [ "G4VisCommandViewerList", "da/de1/classG4VisCommandViewerList.html", null ],
        [ "G4VisCommandViewerPan", "d2/d9e/classG4VisCommandViewerPan.html", null ],
        [ "G4VisCommandViewerRebuild", "d0/d22/classG4VisCommandViewerRebuild.html", null ],
        [ "G4VisCommandViewerRefresh", "dd/df4/classG4VisCommandViewerRefresh.html", null ],
        [ "G4VisCommandViewerReset", "d1/df6/classG4VisCommandViewerReset.html", null ],
        [ "G4VisCommandViewerSave", "df/dc7/classG4VisCommandViewerSave.html", null ],
        [ "G4VisCommandViewerScale", "d0/da6/classG4VisCommandViewerScale.html", null ],
        [ "G4VisCommandViewerSelect", "d6/d77/classG4VisCommandViewerSelect.html", null ],
        [ "G4VisCommandViewerUpdate", "d7/d26/classG4VisCommandViewerUpdate.html", null ],
        [ "G4VisCommandViewerZoom", "d2/d1a/classG4VisCommandViewerZoom.html", null ],
        [ "G4VisCommandsTouchable", "d8/d36/classG4VisCommandsTouchable.html", null ],
        [ "G4VisCommandsTouchableSet", "d8/d02/classG4VisCommandsTouchableSet.html", null ],
        [ "G4VisCommandsViewerSet", "dc/dc7/classG4VisCommandsViewerSet.html", null ]
      ] ],
      [ "G4VisCommandListManagerList< Manager >", "de/dd2/classG4VisCommandListManagerList.html", null ],
      [ "G4VisCommandListManagerSelect< Manager >", "d8/d5b/classG4VisCommandListManagerSelect.html", null ],
      [ "G4VisCommandManagerMode< Manager >", "d2/dc7/classG4VisCommandManagerMode.html", null ],
      [ "G4VtkMessenger", "d7/d12/classG4VtkMessenger.html", null ],
      [ "G4tgrMessenger", "d1/dda/classG4tgrMessenger.html", null ],
      [ "GFlashShowerModelMessenger", "df/d5a/classGFlashShowerModelMessenger.html", null ]
    ] ],
    [ "G4UIOutputString", "d0/da4/classG4UIOutputString.html", null ],
    [ "G4UIparameter", "d9/d78/classG4UIparameter.html", null ],
    [ "G4UIQt::G4UIQtStyle", "d1/d58/structG4UIQt_1_1G4UIQtStyle.html", null ],
    [ "G4UniformRandPool", "d0/dd4/classG4UniformRandPool.html", null ],
    [ "G4UnitDefinition", "dd/db6/classG4UnitDefinition.html", null ],
    [ "G4UnitsCategory", "d0/dc0/classG4UnitsCategory.html", null ],
    [ "G4UserEventAction", "de/d75/classG4UserEventAction.html", [
      [ "G4MultiEventAction", "d6/dc4/classG4MultiEventAction.html", null ]
    ] ],
    [ "G4UserEventActionVector", null, [
      [ "G4MultiEventAction", "d6/dc4/classG4MultiEventAction.html", null ]
    ] ],
    [ "G4UserLimits", "d9/d11/classG4UserLimits.html", [
      [ "G4UserLimitsForRD", "dc/d41/classG4UserLimitsForRD.html", null ]
    ] ],
    [ "G4UserMeshAction", "dc/de0/classG4UserMeshAction.html", null ],
    [ "G4UserRunAction", "dd/d05/classG4UserRunAction.html", [
      [ "G4AdjointSimManager", "db/d16/classG4AdjointSimManager.html", null ],
      [ "G4MultiRunAction", "d4/dd2/classG4MultiRunAction.html", null ],
      [ "G4RTRunAction", "d5/d3c/classG4RTRunAction.html", null ]
    ] ],
    [ "G4UserRunActionVector", null, [
      [ "G4MultiRunAction", "d4/dd2/classG4MultiRunAction.html", null ]
    ] ],
    [ "G4UserStackingAction", "d9/d8c/classG4UserStackingAction.html", [
      [ "G4AdjointStackingAction", "d1/d92/classG4AdjointStackingAction.html", null ],
      [ "G4StackChecker", "d2/dc5/classG4StackChecker.html", null ]
    ] ],
    [ "G4UserSteppingAction", "dd/d17/classG4UserSteppingAction.html", [
      [ "G4AdjointSteppingAction", "db/d09/classG4AdjointSteppingAction.html", null ],
      [ "G4MSSteppingAction", "dd/de0/classG4MSSteppingAction.html", null ],
      [ "G4MultiSteppingAction", "d7/d52/classG4MultiSteppingAction.html", null ],
      [ "G4RTSteppingAction", "da/dd0/classG4RTSteppingAction.html", null ]
    ] ],
    [ "G4UserSteppingActionVector", null, [
      [ "G4MultiSteppingAction", "d7/d52/classG4MultiSteppingAction.html", null ]
    ] ],
    [ "G4UserTimeStepAction", "de/d58/classG4UserTimeStepAction.html", null ],
    [ "G4UserTrackingAction", "d4/d9f/classG4UserTrackingAction.html", [
      [ "G4AdjointTrackingAction", "d5/dde/classG4AdjointTrackingAction.html", null ],
      [ "G4MultiTrackingAction", "db/d1c/classG4MultiTrackingAction.html", null ],
      [ "G4RTTrackingAction", "d6/d1a/classG4RTTrackingAction.html", null ]
    ] ],
    [ "G4UserTrackingActionVector", null, [
      [ "G4MultiTrackingAction", "db/d1c/classG4MultiTrackingAction.html", null ]
    ] ],
    [ "G4UserWorkerInitialization", "d9/de6/classG4UserWorkerInitialization.html", [
      [ "G4RTWorkerInitialization", "dd/dec/classG4RTWorkerInitialization.html", null ],
      [ "G4UserTaskInitialization", "d3/d9b/classG4UserTaskInitialization.html", null ]
    ] ],
    [ "G4UserWorkerThreadInitialization", "d1/df4/classG4UserWorkerThreadInitialization.html", [
      [ "G4UserTaskThreadInitialization", "db/dd9/classG4UserTaskThreadInitialization.html", null ]
    ] ],
    [ "G4V3DNucleus", "d4/d9e/classG4V3DNucleus.html", [
      [ "G4Fancy3DNucleus", "da/dee/classG4Fancy3DNucleus.html", null ]
    ] ],
    [ "G4VAccumulable", "d0/d47/classG4VAccumulable.html", [
      [ "G4Accumulable< T >", "d1/d5e/classG4Accumulable.html", null ]
    ] ],
    [ "G4VAnalysisManager", "d8/d9b/classG4VAnalysisManager.html", [
      [ "G4ToolsAnalysisManager", "d9/dc0/classG4ToolsAnalysisManager.html", [
        [ "G4CsvAnalysisManager", "db/d34/classG4CsvAnalysisManager.html", null ],
        [ "G4GenericAnalysisManager", "da/df3/classG4GenericAnalysisManager.html", null ],
        [ "G4Hdf5AnalysisManager", "dc/d6a/classG4Hdf5AnalysisManager.html", null ],
        [ "G4RootAnalysisManager", "d9/d51/classG4RootAnalysisManager.html", null ],
        [ "G4XmlAnalysisManager", "de/dc0/classG4XmlAnalysisManager.html", null ]
      ] ]
    ] ],
    [ "G4VAnalysisReader", "de/da9/classG4VAnalysisReader.html", [
      [ "G4ToolsAnalysisReader", "dc/d4d/classG4ToolsAnalysisReader.html", [
        [ "G4CsvAnalysisReader", "da/d1c/classG4CsvAnalysisReader.html", null ],
        [ "G4Hdf5AnalysisReader", "df/d9c/classG4Hdf5AnalysisReader.html", null ],
        [ "G4RootAnalysisReader", "d7/de1/classG4RootAnalysisReader.html", null ],
        [ "G4XmlAnalysisReader", "d1/d4a/classG4XmlAnalysisReader.html", null ]
      ] ]
    ] ],
    [ "G4VAngularDistribution", "d5/d79/classG4VAngularDistribution.html", [
      [ "G4AngularDistribution", "d9/d29/classG4AngularDistribution.html", null ],
      [ "G4AngularDistributionNP", "db/d8b/classG4AngularDistributionNP.html", null ],
      [ "G4AngularDistributionPP", "dc/d34/classG4AngularDistributionPP.html", null ]
    ] ],
    [ "G4VarNtp", "d0/daa/classG4VarNtp.html", null ],
    [ "G4VAtomDeexcitation", "d2/d2c/classG4VAtomDeexcitation.html", [
      [ "G4UAtomicDeexcitation", "d6/dda/classG4UAtomicDeexcitation.html", null ]
    ] ],
    [ "G4VAuxiliaryTrackInformation", "da/dba/classG4VAuxiliaryTrackInformation.html", [
      [ "G4BOptrForceCollisionTrackData", "d9/d69/classG4BOptrForceCollisionTrackData.html", null ],
      [ "G4ChannelingTrackData", "d2/d24/classG4ChannelingTrackData.html", null ],
      [ "G4EntanglementAuxInfo", "d9/d17/classG4EntanglementAuxInfo.html", null ]
    ] ],
    [ "G4VBasePhysConstrFactory", "db/d0a/classG4VBasePhysConstrFactory.html", [
      [ "G4PhysicsConstructorFactory< T >", "d8/d0c/classG4PhysicsConstructorFactory.html", null ]
    ] ],
    [ "G4VBasePhysListStamper", "d4/de2/classG4VBasePhysListStamper.html", [
      [ "G4PhysListStamper< T >", "dc/d8b/classG4PhysListStamper.html", null ]
    ] ],
    [ "G4VBaseXSFactory", "d1/da4/classG4VBaseXSFactory.html", [
      [ "G4CrossSectionFactory< T, mode >", "d9/d8b/classG4CrossSectionFactory.html", null ],
      [ "G4CrossSectionFactory< T, 0 >", "d1/dd2/classG4CrossSectionFactory_3_01T_00_010_01_4.html", null ],
      [ "G4CrossSectionFactory< T, 1 >", "d5/d4c/classG4CrossSectionFactory_3_01T_00_011_01_4.html", null ],
      [ "G4CrossSectionFactory< T, 2 >", "df/d49/classG4CrossSectionFactory_3_01T_00_012_01_4.html", null ]
    ] ],
    [ "G4VBiasingInteractionLaw", "d2/d5e/classG4VBiasingInteractionLaw.html", [
      [ "G4ILawCommonTruncatedExp", "d1/d67/classG4ILawCommonTruncatedExp.html", null ],
      [ "G4ILawForceFreeFlight", "d8/d7a/classG4ILawForceFreeFlight.html", null ],
      [ "G4ILawTruncatedExp", "d6/df7/classG4ILawTruncatedExp.html", null ],
      [ "G4InteractionLawPhysical", "dd/d2b/classG4InteractionLawPhysical.html", null ]
    ] ],
    [ "G4VBiasingOperation", "d7/dd6/classG4VBiasingOperation.html", [
      [ "G4BOptnChangeCrossSection", "d0/d17/classG4BOptnChangeCrossSection.html", null ],
      [ "G4BOptnCloning", "d6/d1d/classG4BOptnCloning.html", null ],
      [ "G4BOptnForceCommonTruncatedExp", "d3/d7e/classG4BOptnForceCommonTruncatedExp.html", null ],
      [ "G4BOptnForceFreeFlight", "d7/d28/classG4BOptnForceFreeFlight.html", null ],
      [ "G4BOptnLeadingParticle", "de/db2/classG4BOptnLeadingParticle.html", null ]
    ] ],
    [ "G4VBiasingOperator", "d1/d27/classG4VBiasingOperator.html", [
      [ "G4BOptrForceCollision", "d1/da6/classG4BOptrForceCollision.html", null ],
      [ "G4ChannelingOptrChangeCrossSection", "db/dd0/classG4ChannelingOptrChangeCrossSection.html", null ],
      [ "G4ChannelingOptrMultiParticleChangeCrossSection", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html", null ]
    ] ],
    [ "G4VCascadeCollider", "d7/d70/classG4VCascadeCollider.html", [
      [ "G4CascadeCheckBalance", "d8/dfe/classG4CascadeCheckBalance.html", null ],
      [ "G4CascadeColliderBase", "d5/d69/classG4CascadeColliderBase.html", [
        [ "G4ElementaryParticleCollider", "d4/ded/classG4ElementaryParticleCollider.html", null ],
        [ "G4IntraNucleiCascader", "d0/d60/classG4IntraNucleiCascader.html", null ],
        [ "G4InuclCollider", "d4/d24/classG4InuclCollider.html", null ],
        [ "G4LightTargetCollider", "da/d7b/classG4LightTargetCollider.html", null ]
      ] ],
      [ "G4CascadeRecoilMaker", "d6/dad/classG4CascadeRecoilMaker.html", null ],
      [ "G4VCascadeDeexcitation", "da/d54/classG4VCascadeDeexcitation.html", [
        [ "G4CascadeDeexciteBase", "dd/d8c/classG4CascadeDeexciteBase.html", [
          [ "G4BigBanger", "dc/d04/classG4BigBanger.html", null ],
          [ "G4CascadeDeexcitation", "db/d19/classG4CascadeDeexcitation.html", null ],
          [ "G4EquilibriumEvaporator", "d2/d58/classG4EquilibriumEvaporator.html", null ],
          [ "G4EvaporationInuclCollider", "db/dc4/classG4EvaporationInuclCollider.html", null ],
          [ "G4Fissioner", "d1/d7a/classG4Fissioner.html", null ],
          [ "G4NonEquilibriumEvaporator", "d8/d2c/classG4NonEquilibriumEvaporator.html", null ],
          [ "G4PreCompoundDeexcitation", "da/d8a/classG4PreCompoundDeexcitation.html", null ]
        ] ]
      ] ]
    ] ],
    [ "G4VChemistryWorld", "d5/d70/classG4VChemistryWorld.html", null ],
    [ "G4VCollision", "d9/d1b/classG4VCollision.html", [
      [ "G4CollisionComposite", "d3/de4/classG4CollisionComposite.html", [
        [ "G4CollisionMesonBaryon", "de/daa/classG4CollisionMesonBaryon.html", null ],
        [ "G4CollisionMesonBaryonToResonance", "d8/d69/classG4CollisionMesonBaryonToResonance.html", null ],
        [ "G4CollisionPN", "d4/dbf/classG4CollisionPN.html", null ],
        [ "G4GeneralNNCollision", "d5/d15/classG4GeneralNNCollision.html", [
          [ "G4CollisionNN", "dd/d1c/classG4CollisionNN.html", null ],
          [ "G4CollisionNNToDeltaDelta", "d8/d87/classG4CollisionNNToDeltaDelta.html", null ],
          [ "G4CollisionNNToDeltaDelta1600", "d6/df1/classG4CollisionNNToDeltaDelta1600.html", null ],
          [ "G4CollisionNNToDeltaDelta1620", "d9/d56/classG4CollisionNNToDeltaDelta1620.html", null ],
          [ "G4CollisionNNToDeltaDelta1700", "dc/dbd/classG4CollisionNNToDeltaDelta1700.html", null ],
          [ "G4CollisionNNToDeltaDelta1900", "d0/d00/classG4CollisionNNToDeltaDelta1900.html", null ],
          [ "G4CollisionNNToDeltaDelta1905", "d5/d98/classG4CollisionNNToDeltaDelta1905.html", null ],
          [ "G4CollisionNNToDeltaDelta1910", "da/d4e/classG4CollisionNNToDeltaDelta1910.html", null ],
          [ "G4CollisionNNToDeltaDelta1920", "d9/d02/classG4CollisionNNToDeltaDelta1920.html", null ],
          [ "G4CollisionNNToDeltaDelta1930", "d2/d1d/classG4CollisionNNToDeltaDelta1930.html", null ],
          [ "G4CollisionNNToDeltaDelta1950", "d0/dac/classG4CollisionNNToDeltaDelta1950.html", null ],
          [ "G4CollisionNNToDeltaDeltastar", "da/d67/classG4CollisionNNToDeltaDeltastar.html", null ],
          [ "G4CollisionNNToDeltaNstar", "df/d78/classG4CollisionNNToDeltaNstar.html", null ],
          [ "G4CollisionNNToNDelta", "d6/de8/classG4CollisionNNToNDelta.html", null ],
          [ "G4CollisionNNToNDelta1600", "df/d16/classG4CollisionNNToNDelta1600.html", null ],
          [ "G4CollisionNNToNDelta1620", "d6/df6/classG4CollisionNNToNDelta1620.html", null ],
          [ "G4CollisionNNToNDelta1700", "db/d89/classG4CollisionNNToNDelta1700.html", null ],
          [ "G4CollisionNNToNDelta1900", "d8/db8/classG4CollisionNNToNDelta1900.html", null ],
          [ "G4CollisionNNToNDelta1905", "da/d0b/classG4CollisionNNToNDelta1905.html", null ],
          [ "G4CollisionNNToNDelta1910", "d3/de3/classG4CollisionNNToNDelta1910.html", null ],
          [ "G4CollisionNNToNDelta1920", "de/df5/classG4CollisionNNToNDelta1920.html", null ],
          [ "G4CollisionNNToNDelta1930", "db/d15/classG4CollisionNNToNDelta1930.html", null ],
          [ "G4CollisionNNToNDelta1950", "df/da5/classG4CollisionNNToNDelta1950.html", null ],
          [ "G4CollisionNNToNDeltastar", "d2/d7e/classG4CollisionNNToNDeltastar.html", null ],
          [ "G4CollisionNNToNNstar", "d5/d7d/classG4CollisionNNToNNstar.html", null ],
          [ "G4CollisionNStarNToNN", "d9/dbc/classG4CollisionNStarNToNN.html", null ]
        ] ]
      ] ],
      [ "G4VAnnihilationCollision", "de/d8b/classG4VAnnihilationCollision.html", [
        [ "G4ConcreteMesonBaryonToResonance", "db/d08/classG4ConcreteMesonBaryonToResonance.html", null ]
      ] ],
      [ "G4VElasticCollision", "d3/d91/classG4VElasticCollision.html", [
        [ "G4CollisionMesonBaryonElastic", "d7/d22/classG4CollisionMesonBaryonElastic.html", null ],
        [ "G4CollisionNNElastic", "d4/de5/classG4CollisionNNElastic.html", null ],
        [ "G4CollisionnpElastic", "d6/d20/classG4CollisionnpElastic.html", null ]
      ] ],
      [ "G4VScatteringCollision", "d2/d66/classG4VScatteringCollision.html", [
        [ "G4ConcreteNNTwoBodyResonance", "d8/da9/classG4ConcreteNNTwoBodyResonance.html", [
          [ "G4ConcreteNNToDeltaDelta", "d6/d7b/classG4ConcreteNNToDeltaDelta.html", null ],
          [ "G4ConcreteNNToDeltaDeltastar", "d6/dc2/classG4ConcreteNNToDeltaDeltastar.html", null ],
          [ "G4ConcreteNNToDeltaNstar", "d8/ddf/classG4ConcreteNNToDeltaNstar.html", null ],
          [ "G4ConcreteNNToNDelta", "d9/d58/classG4ConcreteNNToNDelta.html", null ],
          [ "G4ConcreteNNToNDeltaStar", "d8/d45/classG4ConcreteNNToNDeltaStar.html", null ],
          [ "G4ConcreteNNToNNStar", "d3/db7/classG4ConcreteNNToNNStar.html", null ],
          [ "G4ConcreteNStarNToNN", "d9/d33/classG4ConcreteNStarNToNN.html", null ]
        ] ]
      ] ]
    ] ],
    [ "G4VComponentCrossSection", "d1/d62/classG4VComponentCrossSection.html", [
      [ "G4ChipsComponentXS", "d9/d0a/classG4ChipsComponentXS.html", null ],
      [ "G4ComponentAntiNuclNuclearXS", "d8/d07/classG4ComponentAntiNuclNuclearXS.html", null ],
      [ "G4ComponentBarNucleonNucleusXsc", "da/d1d/classG4ComponentBarNucleonNucleusXsc.html", null ],
      [ "G4ComponentGGHadronNucleusXsc", "d6/d40/classG4ComponentGGHadronNucleusXsc.html", null ],
      [ "G4ComponentGGNuclNuclXsc", "d0/dbc/classG4ComponentGGNuclNuclXsc.html", null ],
      [ "G4ComponentSAIDTotalXS", "de/d9a/classG4ComponentSAIDTotalXS.html", null ]
    ] ],
    [ "G4VCoulombBarrier", "d1/dd8/classG4VCoulombBarrier.html", [
      [ "G4CoulombBarrier", "da/d3b/classG4CoulombBarrier.html", [
        [ "G4AlphaCoulombBarrier", "d1/d98/classG4AlphaCoulombBarrier.html", null ],
        [ "G4DeuteronCoulombBarrier", "d2/db4/classG4DeuteronCoulombBarrier.html", null ],
        [ "G4GEMCoulombBarrier", "d4/dd1/classG4GEMCoulombBarrier.html", null ],
        [ "G4He3CoulombBarrier", "dc/d9f/classG4He3CoulombBarrier.html", null ],
        [ "G4NeutronCoulombBarrier", "d2/d00/classG4NeutronCoulombBarrier.html", null ],
        [ "G4ProtonCoulombBarrier", "dd/d73/classG4ProtonCoulombBarrier.html", null ],
        [ "G4TritonCoulombBarrier", "d9/dff/classG4TritonCoulombBarrier.html", null ]
      ] ]
    ] ],
    [ "G4VCrossSectionDataSet", "da/d21/classG4VCrossSectionDataSet.html", [
      [ "G4BGGNucleonElasticXS", "df/d2b/classG4BGGNucleonElasticXS.html", null ],
      [ "G4BGGNucleonInelasticXS", "d9/dc6/classG4BGGNucleonInelasticXS.html", [
        [ "G4ParticleHPBGGNucleonInelasticXS", "d3/db1/classG4ParticleHPBGGNucleonInelasticXS.html", null ]
      ] ],
      [ "G4BGGPionElasticXS", "db/db4/classG4BGGPionElasticXS.html", null ],
      [ "G4BGGPionInelasticXS", "d1/d91/classG4BGGPionInelasticXS.html", null ],
      [ "G4ChipsAntiBaryonElasticXS", "d7/de3/classG4ChipsAntiBaryonElasticXS.html", null ],
      [ "G4ChipsAntiBaryonInelasticXS", "d5/d77/classG4ChipsAntiBaryonInelasticXS.html", null ],
      [ "G4ChipsHyperonElasticXS", "d7/d8f/classG4ChipsHyperonElasticXS.html", null ],
      [ "G4ChipsHyperonInelasticXS", "dc/db3/classG4ChipsHyperonInelasticXS.html", null ],
      [ "G4ChipsKaonMinusElasticXS", "d4/d7f/classG4ChipsKaonMinusElasticXS.html", null ],
      [ "G4ChipsKaonMinusInelasticXS", "d1/d49/classG4ChipsKaonMinusInelasticXS.html", null ],
      [ "G4ChipsKaonPlusElasticXS", "d6/dee/classG4ChipsKaonPlusElasticXS.html", null ],
      [ "G4ChipsKaonPlusInelasticXS", "da/d5a/classG4ChipsKaonPlusInelasticXS.html", null ],
      [ "G4ChipsKaonZeroElasticXS", "d7/d33/classG4ChipsKaonZeroElasticXS.html", null ],
      [ "G4ChipsKaonZeroInelasticXS", "d6/da5/classG4ChipsKaonZeroInelasticXS.html", null ],
      [ "G4ChipsNeutronElasticXS", "d4/de6/classG4ChipsNeutronElasticXS.html", null ],
      [ "G4ChipsNeutronInelasticXS", "d2/d5f/classG4ChipsNeutronInelasticXS.html", null ],
      [ "G4ChipsPionMinusElasticXS", "d2/d0e/classG4ChipsPionMinusElasticXS.html", null ],
      [ "G4ChipsPionMinusInelasticXS", "db/d4d/classG4ChipsPionMinusInelasticXS.html", null ],
      [ "G4ChipsPionPlusElasticXS", "df/dab/classG4ChipsPionPlusElasticXS.html", null ],
      [ "G4ChipsPionPlusInelasticXS", "d3/dcc/classG4ChipsPionPlusInelasticXS.html", null ],
      [ "G4ChipsProtonElasticXS", "d7/df0/classG4ChipsProtonElasticXS.html", null ],
      [ "G4ChipsProtonInelasticXS", "d9/da8/classG4ChipsProtonInelasticXS.html", null ],
      [ "G4CrossSectionElastic", "d6/ddd/classG4CrossSectionElastic.html", null ],
      [ "G4CrossSectionInelastic", "d1/da8/classG4CrossSectionInelastic.html", null ],
      [ "G4EMDissociationCrossSection", "d6/d25/classG4EMDissociationCrossSection.html", null ],
      [ "G4ElNeutrinoNucleusTotXsc", "d3/dbc/classG4ElNeutrinoNucleusTotXsc.html", null ],
      [ "G4ElNucleusSFcs", "d7/d2e/classG4ElNucleusSFcs.html", null ],
      [ "G4ElectroNuclearCrossSection", "dd/dea/classG4ElectroNuclearCrossSection.html", null ],
      [ "G4GammaNuclearXS", "d9/dc8/classG4GammaNuclearXS.html", null ],
      [ "G4IonsShenCrossSection", "d6/dda/classG4IonsShenCrossSection.html", null ],
      [ "G4KokoulinMuonNuclearXS", "d5/dd6/classG4KokoulinMuonNuclearXS.html", null ],
      [ "G4LENDCrossSection", "d3/d66/classG4LENDCrossSection.html", [
        [ "G4LENDCaptureCrossSection", "dd/d12/classG4LENDCaptureCrossSection.html", null ],
        [ "G4LENDCombinedCrossSection", "d9/d91/classG4LENDCombinedCrossSection.html", null ],
        [ "G4LENDElasticCrossSection", "d3/ddd/classG4LENDElasticCrossSection.html", null ],
        [ "G4LENDFissionCrossSection", "dc/d55/classG4LENDFissionCrossSection.html", null ],
        [ "G4LENDGammaCrossSection", "d5/da1/classG4LENDGammaCrossSection.html", null ],
        [ "G4LENDInelasticCrossSection", "db/d90/classG4LENDInelasticCrossSection.html", null ]
      ] ],
      [ "G4MuNeutrinoNucleusTotXsc", "de/daa/classG4MuNeutrinoNucleusTotXsc.html", null ],
      [ "G4NeutrinoElectronCcXsc", "d9/dc4/classG4NeutrinoElectronCcXsc.html", null ],
      [ "G4NeutrinoElectronNcXsc", "d3/d0c/classG4NeutrinoElectronNcXsc.html", null ],
      [ "G4NeutrinoElectronTotXsc", "dc/d33/classG4NeutrinoElectronTotXsc.html", null ],
      [ "G4NeutronCaptureXS", "dd/d46/classG4NeutronCaptureXS.html", null ],
      [ "G4NeutronElasticXS", "d9/d86/classG4NeutronElasticXS.html", null ],
      [ "G4NeutronElectronElXsc", "df/dbe/classG4NeutronElectronElXsc.html", null ],
      [ "G4NeutronInelasticXS", "d6/d71/classG4NeutronInelasticXS.html", null ],
      [ "G4NucleonNuclearCrossSection", "d2/d5c/classG4NucleonNuclearCrossSection.html", null ],
      [ "G4ParticleHPCaptureData", "d7/d94/classG4ParticleHPCaptureData.html", null ],
      [ "G4ParticleHPElasticData", "d9/de3/classG4ParticleHPElasticData.html", null ],
      [ "G4ParticleHPFissionData", "d1/dfe/classG4ParticleHPFissionData.html", null ],
      [ "G4ParticleHPInelasticData", "d8/d88/classG4ParticleHPInelasticData.html", null ],
      [ "G4ParticleHPJENDLHEData", "d1/dfb/classG4ParticleHPJENDLHEData.html", [
        [ "G4ParticleHPJENDLHEElasticData", "d9/da1/classG4ParticleHPJENDLHEElasticData.html", null ],
        [ "G4ParticleHPJENDLHEInelasticData", "d8/d69/classG4ParticleHPJENDLHEInelasticData.html", null ]
      ] ],
      [ "G4ParticleHPThermalScatteringData", "d0/d20/classG4ParticleHPThermalScatteringData.html", null ],
      [ "G4ParticleInelasticXS", "df/d3c/classG4ParticleInelasticXS.html", null ],
      [ "G4PhotoNuclearCrossSection", "d1/ddd/classG4PhotoNuclearCrossSection.html", null ],
      [ "G4UPiNuclearCrossSection", "df/d90/classG4UPiNuclearCrossSection.html", null ],
      [ "G4VCrossSectionRatio", "d3/d18/classG4VCrossSectionRatio.html", [
        [ "G4DiffElasticRatio", "db/d79/classG4DiffElasticRatio.html", null ]
      ] ],
      [ "G4ZeroXS", "d9/de3/classG4ZeroXS.html", null ]
    ] ],
    [ "G4VCrossSectionHandler", "d6/d94/classG4VCrossSectionHandler.html", [
      [ "G4CrossSectionHandler", "d7/d30/classG4CrossSectionHandler.html", null ],
      [ "G4eCrossSectionHandler", "d6/d9c/classG4eCrossSectionHandler.html", null ],
      [ "G4eIonisationCrossSectionHandler", "d2/d30/classG4eIonisationCrossSectionHandler.html", null ]
    ] ],
    [ "G4VCrossSectionSource", "d8/df3/classG4VCrossSectionSource.html", [
      [ "G4CrossSectionComposite", "dc/d80/classG4CrossSectionComposite.html", null ],
      [ "G4CrossSectionPatch", "d0/ddb/classG4CrossSectionPatch.html", [
        [ "G4XNNElastic", "d9/d7b/classG4XNNElastic.html", null ],
        [ "G4XNNTotal", "d3/d22/classG4XNNTotal.html", null ],
        [ "G4XnpElastic", "d9/dc6/classG4XnpElastic.html", null ],
        [ "G4XnpTotal", "d7/d57/classG4XnpTotal.html", null ]
      ] ],
      [ "G4VXResonance", "d4/dde/classG4VXResonance.html", [
        [ "G4XResonance", "df/d1e/classG4XResonance.html", null ]
      ] ],
      [ "G4XAnnihilationChannel", "d8/d9e/classG4XAnnihilationChannel.html", null ],
      [ "G4XAqmElastic", "d3/dec/classG4XAqmElastic.html", null ],
      [ "G4XAqmTotal", "de/dc4/classG4XAqmTotal.html", null ],
      [ "G4XMesonBaryonElastic", "df/dd3/classG4XMesonBaryonElastic.html", null ],
      [ "G4XNNElasticLowE", "dc/dba/classG4XNNElasticLowE.html", null ],
      [ "G4XNNTotalLowE", "df/d90/classG4XNNTotalLowE.html", null ],
      [ "G4XPDGElastic", "d9/d4b/classG4XPDGElastic.html", null ],
      [ "G4XPDGTotal", "db/d17/classG4XPDGTotal.html", null ],
      [ "G4XnpElasticLowE", "dc/de0/classG4XnpElasticLowE.html", null ],
      [ "G4XnpTotalLowE", "db/da5/classG4XnpTotalLowE.html", null ],
      [ "G4XpimNTotal", "d3/d86/classG4XpimNTotal.html", null ],
      [ "G4XpipNTotal", "d1/d4a/classG4XpipNTotal.html", null ]
    ] ],
    [ "G4VCSGface", "d0/d3d/classG4VCSGface.html", [
      [ "G4PolyPhiFace", "d7/d92/classG4PolyPhiFace.html", null ],
      [ "G4PolyconeSide", "d7/d9a/classG4PolyconeSide.html", null ],
      [ "G4PolyhedraSide", "de/dc4/classG4PolyhedraSide.html", null ]
    ] ],
    [ "G4VCurvedTrajectoryFilter", "d8/de2/classG4VCurvedTrajectoryFilter.html", [
      [ "G4IdentityTrajectoryFilter", "d2/d97/classG4IdentityTrajectoryFilter.html", null ]
    ] ],
    [ "G4VDataSetAlgorithm", "df/d2d/classG4VDataSetAlgorithm.html", [
      [ "G4DNACPA100LogLogInterpolation", "da/d3f/classG4DNACPA100LogLogInterpolation.html", null ],
      [ "G4LinInterpolation", "db/dc7/classG4LinInterpolation.html", null ],
      [ "G4LinLogInterpolation", "d2/d98/classG4LinLogInterpolation.html", null ],
      [ "G4LinLogLogInterpolation", "d8/d05/classG4LinLogLogInterpolation.html", null ],
      [ "G4LogLogInterpolation", "d9/d20/classG4LogLogInterpolation.html", null ],
      [ "G4SemiLogInterpolation", "dc/d4a/classG4SemiLogInterpolation.html", null ]
    ] ],
    [ "G4VDCIOentry", "d1/db8/classG4VDCIOentry.html", [
      [ "G4DCIOentryT< T >", "d7/de7/classG4DCIOentryT.html", null ]
    ] ],
    [ "G4VDecayChannel", "df/d2d/classG4VDecayChannel.html", [
      [ "G4DalitzDecayChannel", "df/d5e/classG4DalitzDecayChannel.html", null ],
      [ "G4GeneralPhaseSpaceDecay", "da/ddf/classG4GeneralPhaseSpaceDecay.html", null ],
      [ "G4KL3DecayChannel", "da/d71/classG4KL3DecayChannel.html", null ],
      [ "G4MuonDecayChannel", "d0/d38/classG4MuonDecayChannel.html", [
        [ "G4MuonDecayChannelWithSpin", "d4/de4/classG4MuonDecayChannelWithSpin.html", null ]
      ] ],
      [ "G4MuonRadiativeDecayChannelWithSpin", "df/de5/classG4MuonRadiativeDecayChannelWithSpin.html", null ],
      [ "G4NeutronBetaDecayChannel", "db/d34/classG4NeutronBetaDecayChannel.html", null ],
      [ "G4NuclearDecay", "de/d02/classG4NuclearDecay.html", [
        [ "G4AlphaDecay", "db/d8c/classG4AlphaDecay.html", null ],
        [ "G4BetaMinusDecay", "dd/d46/classG4BetaMinusDecay.html", null ],
        [ "G4BetaPlusDecay", "dc/dbb/classG4BetaPlusDecay.html", null ],
        [ "G4ECDecay", "db/d02/classG4ECDecay.html", null ],
        [ "G4ITDecay", "d0/db8/classG4ITDecay.html", null ],
        [ "G4NeutronDecay", "d9/d33/classG4NeutronDecay.html", null ],
        [ "G4ProtonDecay", "d0/dbe/classG4ProtonDecay.html", null ],
        [ "G4SFDecay", "d4/d38/classG4SFDecay.html", null ],
        [ "G4TritonDecay", "db/dee/classG4TritonDecay.html", null ]
      ] ],
      [ "G4PhaseSpaceDecayChannel", "d9/d69/classG4PhaseSpaceDecayChannel.html", null ],
      [ "G4PionRadiativeDecayChannel", "d6/dc8/classG4PionRadiativeDecayChannel.html", null ],
      [ "G4TauLeptonicDecayChannel", "da/d98/classG4TauLeptonicDecayChannel.html", null ]
    ] ],
    [ "G4VDigi", "d7/d85/classG4VDigi.html", null ],
    [ "G4VDigiCollection", "d5/d1f/classG4VDigiCollection.html", [
      [ "G4DigiCollection", "d0/d24/classG4DigiCollection.html", [
        [ "G4TDigiCollection< T >", "dd/ddb/classG4TDigiCollection.html", null ]
      ] ]
    ] ],
    [ "G4VDigitizerModule", "db/d5c/classG4VDigitizerModule.html", null ],
    [ "G4VDNAHit", "d0/db4/classG4VDNAHit.html", [
      [ "G4DNAIndirectHit", "d3/d7d/classG4DNAIndirectHit.html", null ]
    ] ],
    [ "G4VDNAModel", "db/dae/classG4VDNAModel.html", [
      [ "G4DNADummyModel", "d6/d53/classG4DNADummyModel.html", null ],
      [ "G4DNAPTBElasticModel", "df/d41/classG4DNAPTBElasticModel.html", null ],
      [ "G4DNAPTBExcitationModel", "da/dd5/classG4DNAPTBExcitationModel.html", null ],
      [ "G4DNAPTBIonisationModel", "dc/d67/classG4DNAPTBIonisationModel.html", null ],
      [ "G4DNAVacuumModel", "da/d50/classG4DNAVacuumModel.html", null ]
    ] ],
    [ "G4VDNAMolecularGeometry", "d9/d2b/classG4VDNAMolecularGeometry.html", null ],
    [ "G4VDNAReactionModel", "d8/de2/classG4VDNAReactionModel.html", [
      [ "G4DNASmoluchowskiReactionModel", "d1/dab/classG4DNASmoluchowskiReactionModel.html", null ],
      [ "G4DiffusionControlledReactionModel", "d9/d3f/classG4DiffusionControlledReactionModel.html", null ]
    ] ],
    [ "G4VecpssrKModel", "da/d71/classG4VecpssrKModel.html", [
      [ "G4ANSTOecpssrKxsModel", "d0/d55/classG4ANSTOecpssrKxsModel.html", null ],
      [ "G4ecpssrBaseKxsModel", "dc/d75/classG4ecpssrBaseKxsModel.html", null ],
      [ "G4ecpssrFormFactorKxsModel", "d1/df9/classG4ecpssrFormFactorKxsModel.html", null ]
    ] ],
    [ "G4VecpssrLiModel", "dc/db9/classG4VecpssrLiModel.html", [
      [ "G4ANSTOecpssrLixsModel", "d4/d0b/classG4ANSTOecpssrLixsModel.html", null ],
      [ "G4ecpssrBaseLixsModel", "d6/d82/classG4ecpssrBaseLixsModel.html", null ],
      [ "G4ecpssrFormFactorLixsModel", "d1/d15/classG4ecpssrFormFactorLixsModel.html", null ]
    ] ],
    [ "G4VecpssrMiModel", "da/d3c/classG4VecpssrMiModel.html", [
      [ "G4ANSTOecpssrMixsModel", "d2/d6e/classG4ANSTOecpssrMixsModel.html", null ],
      [ "G4ecpssrFormFactorMixsModel", "d5/d97/classG4ecpssrFormFactorMixsModel.html", null ]
    ] ],
    [ "G4Vee2hadrons", "d3/d3a/classG4Vee2hadrons.html", [
      [ "G4ee2KChargedModel", "d8/d9a/classG4ee2KChargedModel.html", null ],
      [ "G4ee2KNeutralModel", "d0/d2a/classG4ee2KNeutralModel.html", null ],
      [ "G4eeTo3PiModel", "dc/d45/classG4eeTo3PiModel.html", null ],
      [ "G4eeToPGammaModel", "d6/d3e/classG4eeToPGammaModel.html", null ],
      [ "G4eeToTwoPiModel", "df/dda/classG4eeToTwoPiModel.html", null ]
    ] ],
    [ "G4VelocityTable", "dd/de8/classG4VelocityTable.html", null ],
    [ "G4VEmAdjointModel", "df/d40/classG4VEmAdjointModel.html", [
      [ "G4AdjointBremsstrahlungModel", "dc/d41/classG4AdjointBremsstrahlungModel.html", null ],
      [ "G4AdjointComptonModel", "d0/ddc/classG4AdjointComptonModel.html", null ],
      [ "G4AdjointIonIonisationModel", "d6/d89/classG4AdjointIonIonisationModel.html", null ],
      [ "G4AdjointPhotoElectricModel", "d6/d9d/classG4AdjointPhotoElectricModel.html", null ],
      [ "G4AdjointeIonisationModel", "d5/dbf/classG4AdjointeIonisationModel.html", null ],
      [ "G4AdjointhIonisationModel", "d5/d26/classG4AdjointhIonisationModel.html", null ]
    ] ],
    [ "G4VEmAngularDistribution", "da/d90/classG4VEmAngularDistribution.html", [
      [ "G4AngleDirect", "d2/d85/classG4AngleDirect.html", null ],
      [ "G4DNABornAngle", "dc/da7/classG4DNABornAngle.html", null ],
      [ "G4DNARuddAngle", "da/d50/classG4DNARuddAngle.html", null ],
      [ "G4DeltaAngle", "d4/d42/classG4DeltaAngle.html", null ],
      [ "G4DeltaAngleFreeScat", "d0/d2a/classG4DeltaAngleFreeScat.html", null ],
      [ "G4DipBustGenerator", "da/dbf/classG4DipBustGenerator.html", null ],
      [ "G4Generator2BN", "d1/d1f/classG4Generator2BN.html", null ],
      [ "G4Generator2BS", "d0/d93/classG4Generator2BS.html", null ],
      [ "G4ModifiedMephi", "d5/d37/classG4ModifiedMephi.html", null ],
      [ "G4ModifiedTsai", "de/d29/classG4ModifiedTsai.html", null ],
      [ "G4PenelopeBremsstrahlungAngular", "d6/dca/classG4PenelopeBremsstrahlungAngular.html", null ],
      [ "G4PhotoElectricAngularGeneratorPolarized", "d1/d41/classG4PhotoElectricAngularGeneratorPolarized.html", null ],
      [ "G4PhotoElectricAngularGeneratorSauterGavrila", "d8/dde/classG4PhotoElectricAngularGeneratorSauterGavrila.html", null ],
      [ "G4RayleighAngularGenerator", "d0/d23/classG4RayleighAngularGenerator.html", null ],
      [ "G4SauterGavrilaAngularDistribution", "dc/d0e/classG4SauterGavrilaAngularDistribution.html", null ]
    ] ],
    [ "G4VEMDataSet", "d0/d71/classG4VEMDataSet.html", [
      [ "G4CompositeEMDataSet", "df/d02/classG4CompositeEMDataSet.html", null ],
      [ "G4CrossSectionDataSet", "df/dbf/classG4CrossSectionDataSet.html", null ],
      [ "G4DNACrossSectionDataSet", "d7/de6/classG4DNACrossSectionDataSet.html", null ],
      [ "G4EMDataSet", "d2/de6/classG4EMDataSet.html", null ],
      [ "G4MicroElecCrossSectionDataSet", "d2/d8d/classG4MicroElecCrossSectionDataSet.html", null ],
      [ "G4MicroElecCrossSectionDataSet_new", "d5/dc2/classG4MicroElecCrossSectionDataSet__new.html", null ],
      [ "G4ShellEMDataSet", "d7/df3/classG4ShellEMDataSet.html", null ]
    ] ],
    [ "G4VEmFluctuationModel", "d7/d5c/classG4VEmFluctuationModel.html", [
      [ "G4AtimaFluctuations", "d6/d27/classG4AtimaFluctuations.html", null ],
      [ "G4IonFluctuations", "d7/d34/classG4IonFluctuations.html", null ],
      [ "G4LossFluctuationDummy", "dd/d69/classG4LossFluctuationDummy.html", null ],
      [ "G4PAIModel", "db/d64/classG4PAIModel.html", null ],
      [ "G4PAIPhotModel", "df/d05/classG4PAIPhotModel.html", null ],
      [ "G4UniversalFluctuation", "d6/d90/classG4UniversalFluctuation.html", [
        [ "G4UrbanFluctuation", "d3/d45/classG4UrbanFluctuation.html", null ]
      ] ],
      [ "G4mplIonisationModel", "dc/d47/classG4mplIonisationModel.html", null ],
      [ "G4mplIonisationWithDeltaModel", "d7/dcf/classG4mplIonisationWithDeltaModel.html", null ]
    ] ],
    [ "G4VEmissionProbability", "d9/dc0/classG4VEmissionProbability.html", [
      [ "G4EvaporationProbability", "d9/d84/classG4EvaporationProbability.html", [
        [ "G4AlphaEvaporationProbability", "da/db7/classG4AlphaEvaporationProbability.html", null ],
        [ "G4DeuteronEvaporationProbability", "d3/da1/classG4DeuteronEvaporationProbability.html", null ],
        [ "G4He3EvaporationProbability", "db/d95/classG4He3EvaporationProbability.html", null ],
        [ "G4NeutronEvaporationProbability", "de/def/classG4NeutronEvaporationProbability.html", null ],
        [ "G4ProtonEvaporationProbability", "db/d70/classG4ProtonEvaporationProbability.html", null ],
        [ "G4TritonEvaporationProbability", "d8/d42/classG4TritonEvaporationProbability.html", null ]
      ] ],
      [ "G4FissionProbability", "df/d23/classG4FissionProbability.html", null ],
      [ "G4GEMProbability", "d6/de2/classG4GEMProbability.html", [
        [ "G4AlphaGEMProbability", "d5/da1/classG4AlphaGEMProbability.html", null ],
        [ "G4B10GEMProbability", "d6/d56/classG4B10GEMProbability.html", null ],
        [ "G4B11GEMProbability", "d9/d05/classG4B11GEMProbability.html", null ],
        [ "G4B12GEMProbability", "d6/db3/classG4B12GEMProbability.html", null ],
        [ "G4B13GEMProbability", "db/dd1/classG4B13GEMProbability.html", null ],
        [ "G4B8GEMProbability", "d9/dd1/classG4B8GEMProbability.html", null ],
        [ "G4Be10GEMProbability", "dc/ded/classG4Be10GEMProbability.html", null ],
        [ "G4Be11GEMProbability", "d0/d6d/classG4Be11GEMProbability.html", null ],
        [ "G4Be12GEMProbability", "d2/d16/classG4Be12GEMProbability.html", null ],
        [ "G4Be7GEMProbability", "d8/d5d/classG4Be7GEMProbability.html", null ],
        [ "G4Be9GEMProbability", "dc/d53/classG4Be9GEMProbability.html", null ],
        [ "G4C10GEMProbability", "d7/d64/classG4C10GEMProbability.html", null ],
        [ "G4C11GEMProbability", "d6/d98/classG4C11GEMProbability.html", null ],
        [ "G4C12GEMProbability", "dc/dad/classG4C12GEMProbability.html", null ],
        [ "G4C13GEMProbability", "d1/dc7/classG4C13GEMProbability.html", null ],
        [ "G4C14GEMProbability", "da/d59/classG4C14GEMProbability.html", null ],
        [ "G4C15GEMProbability", "d5/d1f/classG4C15GEMProbability.html", null ],
        [ "G4C16GEMProbability", "d3/d2e/classG4C16GEMProbability.html", null ],
        [ "G4DeuteronGEMProbability", "d5/dc5/classG4DeuteronGEMProbability.html", null ],
        [ "G4F17GEMProbability", "d6/d64/classG4F17GEMProbability.html", null ],
        [ "G4F18GEMProbability", "d3/d23/classG4F18GEMProbability.html", null ],
        [ "G4F19GEMProbability", "d4/d5b/classG4F19GEMProbability.html", null ],
        [ "G4F20GEMProbability", "d4/d0c/classG4F20GEMProbability.html", null ],
        [ "G4F21GEMProbability", "de/d83/classG4F21GEMProbability.html", null ],
        [ "G4He3GEMProbability", "d2/d96/classG4He3GEMProbability.html", null ],
        [ "G4He6GEMProbability", "db/d8b/classG4He6GEMProbability.html", null ],
        [ "G4He8GEMProbability", "d5/de2/classG4He8GEMProbability.html", null ],
        [ "G4Li6GEMProbability", "d4/d5d/classG4Li6GEMProbability.html", null ],
        [ "G4Li7GEMProbability", "da/db1/classG4Li7GEMProbability.html", null ],
        [ "G4Li8GEMProbability", "da/db4/classG4Li8GEMProbability.html", null ],
        [ "G4Li9GEMProbability", "d0/dfe/classG4Li9GEMProbability.html", null ],
        [ "G4Mg22GEMProbability", "d0/d49/classG4Mg22GEMProbability.html", null ],
        [ "G4Mg23GEMProbability", "d6/ddb/classG4Mg23GEMProbability.html", null ],
        [ "G4Mg24GEMProbability", "de/db4/classG4Mg24GEMProbability.html", null ],
        [ "G4Mg25GEMProbability", "de/d74/classG4Mg25GEMProbability.html", null ],
        [ "G4Mg26GEMProbability", "dd/d87/classG4Mg26GEMProbability.html", null ],
        [ "G4Mg27GEMProbability", "dd/dcb/classG4Mg27GEMProbability.html", null ],
        [ "G4Mg28GEMProbability", "d0/dc3/classG4Mg28GEMProbability.html", null ],
        [ "G4N12GEMProbability", "d7/d5d/classG4N12GEMProbability.html", null ],
        [ "G4N13GEMProbability", "de/d2b/classG4N13GEMProbability.html", null ],
        [ "G4N14GEMProbability", "d4/dbe/classG4N14GEMProbability.html", null ],
        [ "G4N15GEMProbability", "d6/d04/classG4N15GEMProbability.html", null ],
        [ "G4N16GEMProbability", "d0/d80/classG4N16GEMProbability.html", null ],
        [ "G4N17GEMProbability", "d4/d90/classG4N17GEMProbability.html", null ],
        [ "G4Na21GEMProbability", "d2/dd0/classG4Na21GEMProbability.html", null ],
        [ "G4Na22GEMProbability", "d8/d5f/classG4Na22GEMProbability.html", null ],
        [ "G4Na23GEMProbability", "dd/d1f/classG4Na23GEMProbability.html", null ],
        [ "G4Na24GEMProbability", "d8/d59/classG4Na24GEMProbability.html", null ],
        [ "G4Na25GEMProbability", "d0/d94/classG4Na25GEMProbability.html", null ],
        [ "G4Ne18GEMProbability", "d9/d09/classG4Ne18GEMProbability.html", null ],
        [ "G4Ne19GEMProbability", "d2/d4a/classG4Ne19GEMProbability.html", null ],
        [ "G4Ne20GEMProbability", "de/dcf/classG4Ne20GEMProbability.html", null ],
        [ "G4Ne21GEMProbability", "d9/d7f/classG4Ne21GEMProbability.html", null ],
        [ "G4Ne22GEMProbability", "d0/d49/classG4Ne22GEMProbability.html", null ],
        [ "G4Ne23GEMProbability", "dd/d43/classG4Ne23GEMProbability.html", null ],
        [ "G4Ne24GEMProbability", "df/d3a/classG4Ne24GEMProbability.html", null ],
        [ "G4NeutronGEMProbability", "dd/d3d/classG4NeutronGEMProbability.html", null ],
        [ "G4O14GEMProbability", "db/dfe/classG4O14GEMProbability.html", null ],
        [ "G4O15GEMProbability", "dc/d61/classG4O15GEMProbability.html", null ],
        [ "G4O16GEMProbability", "d5/dee/classG4O16GEMProbability.html", null ],
        [ "G4O17GEMProbability", "df/d8e/classG4O17GEMProbability.html", null ],
        [ "G4O18GEMProbability", "d3/d21/classG4O18GEMProbability.html", null ],
        [ "G4O19GEMProbability", "d1/d6a/classG4O19GEMProbability.html", null ],
        [ "G4O20GEMProbability", "db/de3/classG4O20GEMProbability.html", null ],
        [ "G4ProtonGEMProbability", "d5/da8/classG4ProtonGEMProbability.html", null ],
        [ "G4TritonGEMProbability", "d9/d81/classG4TritonGEMProbability.html", null ]
      ] ],
      [ "G4GEMProbabilityVI", "de/dec/classG4GEMProbabilityVI.html", null ]
    ] ],
    [ "G4VEmModel", "d3/dc1/classG4VEmModel.html", [
      [ "G4AtimaEnergyLossModel", "d0/dd3/classG4AtimaEnergyLossModel.html", null ],
      [ "G4BetheBlochModel", "d7/df6/classG4BetheBlochModel.html", [
        [ "G4BetheBlochIonGasModel", "d7/d52/classG4BetheBlochIonGasModel.html", null ],
        [ "G4BetheBlochNoDeltaModel", "d5/d12/classG4BetheBlochNoDeltaModel.html", null ]
      ] ],
      [ "G4BetheHeitlerModel", "dc/de8/classG4BetheHeitlerModel.html", [
        [ "G4PolarizedGammaConversionModel", "db/d3f/classG4PolarizedGammaConversionModel.html", null ]
      ] ],
      [ "G4BoldyshevTripletModel", "db/d73/classG4BoldyshevTripletModel.html", null ],
      [ "G4BraggIonModel", "d1/d4d/classG4BraggIonModel.html", [
        [ "G4BraggNoDeltaModel", "d0/d35/classG4BraggNoDeltaModel.html", null ]
      ] ],
      [ "G4BraggModel", "d4/d77/classG4BraggModel.html", [
        [ "G4BraggIonGasModel", "d2/d21/classG4BraggIonGasModel.html", null ]
      ] ],
      [ "G4DNABornExcitationModel1", "d9/d5e/classG4DNABornExcitationModel1.html", null ],
      [ "G4DNABornExcitationModel2", "df/d94/classG4DNABornExcitationModel2.html", null ],
      [ "G4DNABornIonisationModel1", "dc/db0/classG4DNABornIonisationModel1.html", null ],
      [ "G4DNABornIonisationModel2", "d7/d47/classG4DNABornIonisationModel2.html", null ],
      [ "G4DNACPA100ElasticModel", "d9/d81/classG4DNACPA100ElasticModel.html", null ],
      [ "G4DNACPA100ExcitationModel", "db/d91/classG4DNACPA100ExcitationModel.html", null ],
      [ "G4DNACPA100IonisationModel", "dc/da4/classG4DNACPA100IonisationModel.html", null ],
      [ "G4DNAChampionElasticModel", "d7/d9d/classG4DNAChampionElasticModel.html", null ],
      [ "G4DNADingfelderChargeDecreaseModel", "da/d9e/classG4DNADingfelderChargeDecreaseModel.html", null ],
      [ "G4DNADingfelderChargeIncreaseModel", "df/da7/classG4DNADingfelderChargeIncreaseModel.html", null ],
      [ "G4DNADiracRMatrixExcitationModel", "d3/d72/classG4DNADiracRMatrixExcitationModel.html", null ],
      [ "G4DNAELSEPAElasticModel", "d4/d6d/classG4DNAELSEPAElasticModel.html", null ],
      [ "G4DNAEmfietzoglouExcitationModel", "df/d79/classG4DNAEmfietzoglouExcitationModel.html", null ],
      [ "G4DNAEmfietzoglouIonisationModel", "d2/d6f/classG4DNAEmfietzoglouIonisationModel.html", null ],
      [ "G4DNAIonElasticModel", "d1/dbd/classG4DNAIonElasticModel.html", null ],
      [ "G4DNAMeltonAttachmentModel", "de/d3f/classG4DNAMeltonAttachmentModel.html", null ],
      [ "G4DNAMillerGreenExcitationModel", "d4/d3a/classG4DNAMillerGreenExcitationModel.html", null ],
      [ "G4DNAModelInterface", "d1/dc1/classG4DNAModelInterface.html", null ],
      [ "G4DNAQuinnPlasmonExcitationModel", "d6/d49/classG4DNAQuinnPlasmonExcitationModel.html", null ],
      [ "G4DNARelativisticIonisationModel", "db/dee/classG4DNARelativisticIonisationModel.html", null ],
      [ "G4DNARuddIonisationExtendedModel", "d9/de3/classG4DNARuddIonisationExtendedModel.html", null ],
      [ "G4DNARuddIonisationModel", "d5/d93/classG4DNARuddIonisationModel.html", null ],
      [ "G4DNASancheExcitationModel", "d5/d68/classG4DNASancheExcitationModel.html", null ],
      [ "G4DNAScreenedRutherfordElasticModel", "dc/d1b/classG4DNAScreenedRutherfordElasticModel.html", null ],
      [ "G4DNATransformElectronModel", "de/d16/classG4DNATransformElectronModel.html", null ],
      [ "G4DNAUeharaScreenedRutherfordElasticModel", "d5/d21/classG4DNAUeharaScreenedRutherfordElasticModel.html", null ],
      [ "G4EmMultiModel", "dd/dd2/classG4EmMultiModel.html", null ],
      [ "G4ICRU49NuclearStoppingModel", "d7/def/classG4ICRU49NuclearStoppingModel.html", null ],
      [ "G4ICRU73QOModel", "d7/d82/classG4ICRU73QOModel.html", [
        [ "G4ICRU73NoDeltaModel", "d8/d55/classG4ICRU73NoDeltaModel.html", null ]
      ] ],
      [ "G4IonCoulombScatteringModel", "d7/d0d/classG4IonCoulombScatteringModel.html", null ],
      [ "G4IonParametrisedLossModel", "de/d12/classG4IonParametrisedLossModel.html", null ],
      [ "G4JAEAElasticScatteringModel", "d7/dc0/classG4JAEAElasticScatteringModel.html", null ],
      [ "G4JAEAPolarizedElasticScatteringModel", "d1/de4/classG4JAEAPolarizedElasticScatteringModel.html", null ],
      [ "G4KleinNishinaCompton", "db/d54/classG4KleinNishinaCompton.html", [
        [ "G4PolarizedComptonModel", "d8/ddd/classG4PolarizedComptonModel.html", null ]
      ] ],
      [ "G4KleinNishinaModel", "d5/df7/classG4KleinNishinaModel.html", null ],
      [ "G4LindhardSorensenIonModel", "d6/d8c/classG4LindhardSorensenIonModel.html", null ],
      [ "G4LivermoreComptonModel", "de/dcc/classG4LivermoreComptonModel.html", null ],
      [ "G4LivermoreIonisationModel", "dc/d61/classG4LivermoreIonisationModel.html", null ],
      [ "G4LivermoreNuclearGammaConversionModel", "d5/d0e/classG4LivermoreNuclearGammaConversionModel.html", null ],
      [ "G4LivermorePhotoElectricModel", "d2/d10/classG4LivermorePhotoElectricModel.html", null ],
      [ "G4LivermorePolarizedComptonModel", "d4/d4f/classG4LivermorePolarizedComptonModel.html", null ],
      [ "G4LivermorePolarizedGammaConversionModel", "de/df7/classG4LivermorePolarizedGammaConversionModel.html", null ],
      [ "G4LivermorePolarizedRayleighModel", "d3/d6e/classG4LivermorePolarizedRayleighModel.html", null ],
      [ "G4LivermoreRayleighModel", "dd/d27/classG4LivermoreRayleighModel.html", null ],
      [ "G4LowEPComptonModel", "dc/d4b/classG4LowEPComptonModel.html", null ],
      [ "G4LowEPPolarizedComptonModel", "d1/da3/classG4LowEPPolarizedComptonModel.html", null ],
      [ "G4MicroElecElasticModel", "d0/dd3/classG4MicroElecElasticModel.html", null ],
      [ "G4MicroElecElasticModel_new", "dc/dec/classG4MicroElecElasticModel__new.html", null ],
      [ "G4MicroElecInelasticModel", "db/da5/classG4MicroElecInelasticModel.html", null ],
      [ "G4MicroElecInelasticModel_new", "de/dbb/classG4MicroElecInelasticModel__new.html", null ],
      [ "G4MicroElecLOPhononModel", "dd/dee/classG4MicroElecLOPhononModel.html", null ],
      [ "G4MollerBhabhaModel", "d5/dda/classG4MollerBhabhaModel.html", [
        [ "G4PolarizedIonisationModel", "d7/d91/classG4PolarizedIonisationModel.html", null ]
      ] ],
      [ "G4MuBetheBlochModel", "da/d5b/classG4MuBetheBlochModel.html", null ],
      [ "G4MuBremsstrahlungModel", "d4/dc6/classG4MuBremsstrahlungModel.html", [
        [ "G4hBremsstrahlungModel", "d6/dc5/classG4hBremsstrahlungModel.html", null ]
      ] ],
      [ "G4MuPairProductionModel", "da/de8/classG4MuPairProductionModel.html", [
        [ "G4hPairProductionModel", "dc/d1f/classG4hPairProductionModel.html", null ]
      ] ],
      [ "G4PAIModel", "db/d64/classG4PAIModel.html", null ],
      [ "G4PAIPhotModel", "df/d05/classG4PAIPhotModel.html", null ],
      [ "G4PEEffectFluoModel", "da/dad/classG4PEEffectFluoModel.html", [
        [ "G4PolarizedPhotoElectricModel", "d9/d9b/classG4PolarizedPhotoElectricModel.html", null ]
      ] ],
      [ "G4PairProductionRelModel", "da/d8b/classG4PairProductionRelModel.html", [
        [ "G4BetheHeitler5DModel", "dd/dfd/classG4BetheHeitler5DModel.html", [
          [ "G4LivermoreGammaConversion5DModel", "d6/d48/classG4LivermoreGammaConversion5DModel.html", null ]
        ] ],
        [ "G4LivermoreGammaConversionModel", "d3/db3/classG4LivermoreGammaConversionModel.html", null ]
      ] ],
      [ "G4PenelopeAnnihilationModel", "d6/d02/classG4PenelopeAnnihilationModel.html", null ],
      [ "G4PenelopeBremsstrahlungModel", "df/d15/classG4PenelopeBremsstrahlungModel.html", null ],
      [ "G4PenelopeComptonModel", "db/d2c/classG4PenelopeComptonModel.html", null ],
      [ "G4PenelopeGammaConversionModel", "da/dd0/classG4PenelopeGammaConversionModel.html", null ],
      [ "G4PenelopeIonisationModel", "d9/dd7/classG4PenelopeIonisationModel.html", null ],
      [ "G4PenelopePhotoElectricModel", "d9/d44/classG4PenelopePhotoElectricModel.html", null ],
      [ "G4PenelopeRayleighModel", "d2/d2c/classG4PenelopeRayleighModel.html", null ],
      [ "G4PenelopeRayleighModelMI", "d4/dce/classG4PenelopeRayleighModelMI.html", null ],
      [ "G4TDNAOneStepThermalizationModel< MODEL >", "d0/d5e/classG4TDNAOneStepThermalizationModel.html", null ],
      [ "G4VLEPTSModel", "d2/d29/classG4VLEPTSModel.html", [
        [ "G4LEPTSAttachmentModel", "d6/d90/classG4LEPTSAttachmentModel.html", null ],
        [ "G4LEPTSDissociationModel", "d8/dfd/classG4LEPTSDissociationModel.html", null ],
        [ "G4LEPTSElasticModel", "d9/d5a/classG4LEPTSElasticModel.html", null ],
        [ "G4LEPTSExcitationModel", "d3/dad/classG4LEPTSExcitationModel.html", null ],
        [ "G4LEPTSIonisationModel", "d5/dfe/classG4LEPTSIonisationModel.html", null ],
        [ "G4LEPTSPositroniumModel", "d2/d10/classG4LEPTSPositroniumModel.html", null ],
        [ "G4LEPTSRotExcitationModel", "db/d2b/classG4LEPTSRotExcitationModel.html", null ],
        [ "G4LEPTSVibExcitationModel", "d6/d62/classG4LEPTSVibExcitationModel.html", null ]
      ] ],
      [ "G4VMscModel", "d0/d99/classG4VMscModel.html", [
        [ "G4DummyModel", "d6/df0/classG4DummyModel.html", null ],
        [ "G4GoudsmitSaundersonMscModel", "d9/d3b/classG4GoudsmitSaundersonMscModel.html", null ],
        [ "G4UrbanAdjointMscModel", "db/dfc/classG4UrbanAdjointMscModel.html", null ],
        [ "G4UrbanMscModel", "d4/da8/classG4UrbanMscModel.html", null ],
        [ "G4WentzelVIModel", "d8/d2f/classG4WentzelVIModel.html", [
          [ "G4LowEWentzelVIModel", "d4/d8e/classG4LowEWentzelVIModel.html", null ],
          [ "G4WentzelVIRelModel", "df/d39/classG4WentzelVIRelModel.html", null ]
        ] ]
      ] ],
      [ "G4XrayRayleighModel", "dc/dec/classG4XrayRayleighModel.html", null ],
      [ "G4eBremParametrizedModel", "de/d1a/classG4eBremParametrizedModel.html", null ],
      [ "G4eBremsstrahlungRelModel", "d3/db7/classG4eBremsstrahlungRelModel.html", [
        [ "G4LivermoreBremsstrahlungModel", "de/d59/classG4LivermoreBremsstrahlungModel.html", null ],
        [ "G4SeltzerBergerModel", "db/d8c/classG4SeltzerBergerModel.html", [
          [ "G4PolarizedBremsstrahlungModel", "d1/db4/classG4PolarizedBremsstrahlungModel.html", null ]
        ] ]
      ] ],
      [ "G4eCoulombScatteringModel", "db/d99/classG4eCoulombScatteringModel.html", null ],
      [ "G4eDPWACoulombScatteringModel", "de/dd9/classG4eDPWACoulombScatteringModel.html", null ],
      [ "G4eSingleCoulombScatteringModel", "df/d59/classG4eSingleCoulombScatteringModel.html", null ],
      [ "G4eeToHadronsModel", "d8/de6/classG4eeToHadronsModel.html", null ],
      [ "G4eeToHadronsMultiModel", "dd/d0c/classG4eeToHadronsMultiModel.html", null ],
      [ "G4eeToTwoGammaModel", "dd/d42/classG4eeToTwoGammaModel.html", [
        [ "G4PolarizedAnnihilationModel", "da/d31/classG4PolarizedAnnihilationModel.html", null ]
      ] ],
      [ "G4eplusTo2GammaOKVIModel", "de/d51/classG4eplusTo2GammaOKVIModel.html", null ],
      [ "G4eplusTo3GammaOKVIModel", "de/d47/classG4eplusTo3GammaOKVIModel.html", null ],
      [ "G4hCoulombScatteringModel", "d8/d6d/classG4hCoulombScatteringModel.html", null ],
      [ "G4mplIonisationModel", "dc/d47/classG4mplIonisationModel.html", null ],
      [ "G4mplIonisationWithDeltaModel", "d7/dcf/classG4mplIonisationWithDeltaModel.html", null ]
    ] ],
    [ "G4VEnergySpectrum", "d7/d48/classG4VEnergySpectrum.html", [
      [ "G4eIonisationSpectrum", "d1/de1/classG4eIonisationSpectrum.html", null ]
    ] ],
    [ "G4VEntanglementClipBoard", "d3/dc1/classG4VEntanglementClipBoard.html", [
      [ "G4eplusAnnihilationEntanglementClipBoard", "d2/d4f/classG4eplusAnnihilationEntanglementClipBoard.html", null ]
    ] ],
    [ "G4VertexComparator", "de/da6/classG4VertexComparator.html", null ],
    [ "G4VertexInfo", "de/d92/structG4VertexInfo.html", null ],
    [ "G4VEvaporation", "dd/d2c/classG4VEvaporation.html", [
      [ "G4Evaporation", "dc/d86/classG4Evaporation.html", null ],
      [ "G4InuclEvaporation", "d4/ded/classG4InuclEvaporation.html", null ],
      [ "G4WilsonAblationModel", "d7/dfb/classG4WilsonAblationModel.html", null ]
    ] ],
    [ "G4VEvaporationChannel", "d2/de3/classG4VEvaporationChannel.html", [
      [ "G4CompetitiveFission", "df/d94/classG4CompetitiveFission.html", null ],
      [ "G4EvaporationChannel", "dc/d83/classG4EvaporationChannel.html", [
        [ "G4AlphaEvaporationChannel", "dc/d94/classG4AlphaEvaporationChannel.html", null ],
        [ "G4DeuteronEvaporationChannel", "d7/d36/classG4DeuteronEvaporationChannel.html", null ],
        [ "G4He3EvaporationChannel", "d9/d80/classG4He3EvaporationChannel.html", null ],
        [ "G4NeutronEvaporationChannel", "d2/df0/classG4NeutronEvaporationChannel.html", null ],
        [ "G4ProtonEvaporationChannel", "de/d5c/classG4ProtonEvaporationChannel.html", null ],
        [ "G4TritonEvaporationChannel", "d9/d1c/classG4TritonEvaporationChannel.html", null ]
      ] ],
      [ "G4GEMChannel", "d0/d29/classG4GEMChannel.html", [
        [ "G4AlphaGEMChannel", "d4/d54/classG4AlphaGEMChannel.html", null ],
        [ "G4B10GEMChannel", "d8/dab/classG4B10GEMChannel.html", null ],
        [ "G4B11GEMChannel", "dd/d3a/classG4B11GEMChannel.html", null ],
        [ "G4B12GEMChannel", "d0/df4/classG4B12GEMChannel.html", null ],
        [ "G4B13GEMChannel", "d6/d74/classG4B13GEMChannel.html", null ],
        [ "G4B8GEMChannel", "d9/d20/classG4B8GEMChannel.html", null ],
        [ "G4Be10GEMChannel", "d2/d55/classG4Be10GEMChannel.html", null ],
        [ "G4Be11GEMChannel", "d9/d36/classG4Be11GEMChannel.html", null ],
        [ "G4Be12GEMChannel", "d8/df7/classG4Be12GEMChannel.html", null ],
        [ "G4Be7GEMChannel", "d1/ded/classG4Be7GEMChannel.html", null ],
        [ "G4Be9GEMChannel", "d1/de3/classG4Be9GEMChannel.html", null ],
        [ "G4C10GEMChannel", "d1/db3/classG4C10GEMChannel.html", null ],
        [ "G4C11GEMChannel", "d2/ddb/classG4C11GEMChannel.html", null ],
        [ "G4C12GEMChannel", "d7/d74/classG4C12GEMChannel.html", null ],
        [ "G4C13GEMChannel", "d9/dd8/classG4C13GEMChannel.html", null ],
        [ "G4C14GEMChannel", "dc/dab/classG4C14GEMChannel.html", null ],
        [ "G4C15GEMChannel", "d8/d39/classG4C15GEMChannel.html", null ],
        [ "G4C16GEMChannel", "db/de1/classG4C16GEMChannel.html", null ],
        [ "G4DeuteronGEMChannel", "dc/de7/classG4DeuteronGEMChannel.html", null ],
        [ "G4F17GEMChannel", "d4/d81/classG4F17GEMChannel.html", null ],
        [ "G4F18GEMChannel", "de/db7/classG4F18GEMChannel.html", null ],
        [ "G4F19GEMChannel", "d4/db6/classG4F19GEMChannel.html", null ],
        [ "G4F20GEMChannel", "d2/dcd/classG4F20GEMChannel.html", null ],
        [ "G4F21GEMChannel", "d3/dfc/classG4F21GEMChannel.html", null ],
        [ "G4He3GEMChannel", "d1/dcc/classG4He3GEMChannel.html", null ],
        [ "G4He6GEMChannel", "d0/d0d/classG4He6GEMChannel.html", null ],
        [ "G4He8GEMChannel", "d3/d10/classG4He8GEMChannel.html", null ],
        [ "G4Li6GEMChannel", "d0/de2/classG4Li6GEMChannel.html", null ],
        [ "G4Li7GEMChannel", "d9/d0c/classG4Li7GEMChannel.html", null ],
        [ "G4Li8GEMChannel", "d6/d05/classG4Li8GEMChannel.html", null ],
        [ "G4Li9GEMChannel", "d7/dcc/classG4Li9GEMChannel.html", null ],
        [ "G4Mg22GEMChannel", "dc/d75/classG4Mg22GEMChannel.html", null ],
        [ "G4Mg23GEMChannel", "d2/d00/classG4Mg23GEMChannel.html", null ],
        [ "G4Mg24GEMChannel", "d6/dd3/classG4Mg24GEMChannel.html", null ],
        [ "G4Mg25GEMChannel", "da/d31/classG4Mg25GEMChannel.html", null ],
        [ "G4Mg26GEMChannel", "de/deb/classG4Mg26GEMChannel.html", null ],
        [ "G4Mg27GEMChannel", "d5/d04/classG4Mg27GEMChannel.html", null ],
        [ "G4Mg28GEMChannel", "de/d5b/classG4Mg28GEMChannel.html", null ],
        [ "G4N12GEMChannel", "da/d6a/classG4N12GEMChannel.html", null ],
        [ "G4N13GEMChannel", "d8/dcb/classG4N13GEMChannel.html", null ],
        [ "G4N14GEMChannel", "d5/d1f/classG4N14GEMChannel.html", null ],
        [ "G4N15GEMChannel", "df/df6/classG4N15GEMChannel.html", null ],
        [ "G4N16GEMChannel", "df/dad/classG4N16GEMChannel.html", null ],
        [ "G4N17GEMChannel", "d4/d6d/classG4N17GEMChannel.html", null ],
        [ "G4Na21GEMChannel", "d4/d6d/classG4Na21GEMChannel.html", null ],
        [ "G4Na22GEMChannel", "d3/d19/classG4Na22GEMChannel.html", null ],
        [ "G4Na23GEMChannel", "db/da6/classG4Na23GEMChannel.html", null ],
        [ "G4Na24GEMChannel", "d8/d1c/classG4Na24GEMChannel.html", null ],
        [ "G4Na25GEMChannel", "d6/d53/classG4Na25GEMChannel.html", null ],
        [ "G4Ne18GEMChannel", "d1/db6/classG4Ne18GEMChannel.html", null ],
        [ "G4Ne19GEMChannel", "df/d7c/classG4Ne19GEMChannel.html", null ],
        [ "G4Ne20GEMChannel", "d2/dd2/classG4Ne20GEMChannel.html", null ],
        [ "G4Ne21GEMChannel", "d2/de4/classG4Ne21GEMChannel.html", null ],
        [ "G4Ne22GEMChannel", "d7/dce/classG4Ne22GEMChannel.html", null ],
        [ "G4Ne23GEMChannel", "d9/d7e/classG4Ne23GEMChannel.html", null ],
        [ "G4Ne24GEMChannel", "d2/d6f/classG4Ne24GEMChannel.html", null ],
        [ "G4NeutronGEMChannel", "df/dad/classG4NeutronGEMChannel.html", null ],
        [ "G4O14GEMChannel", "d5/dbc/classG4O14GEMChannel.html", null ],
        [ "G4O15GEMChannel", "df/da9/classG4O15GEMChannel.html", null ],
        [ "G4O16GEMChannel", "d9/d4f/classG4O16GEMChannel.html", null ],
        [ "G4O17GEMChannel", "df/dd4/classG4O17GEMChannel.html", null ],
        [ "G4O18GEMChannel", "d0/d37/classG4O18GEMChannel.html", null ],
        [ "G4O19GEMChannel", "d5/d38/classG4O19GEMChannel.html", null ],
        [ "G4O20GEMChannel", "d7/dcd/classG4O20GEMChannel.html", null ],
        [ "G4ProtonGEMChannel", "d4/d29/classG4ProtonGEMChannel.html", null ],
        [ "G4TritonGEMChannel", "dd/d3a/classG4TritonGEMChannel.html", null ]
      ] ],
      [ "G4GEMChannelVI", "d9/d8e/classG4GEMChannelVI.html", null ],
      [ "G4PhotonEvaporation", "d0/dfe/classG4PhotonEvaporation.html", null ],
      [ "G4UnstableFragmentBreakUp", "d9/d25/classG4UnstableFragmentBreakUp.html", null ]
    ] ],
    [ "G4VEvaporationFactory", "da/dfe/classG4VEvaporationFactory.html", [
      [ "G4EvaporationDefaultGEMFactory", "d4/d12/classG4EvaporationDefaultGEMFactory.html", null ],
      [ "G4EvaporationFactory", "d6/de4/classG4EvaporationFactory.html", null ],
      [ "G4EvaporationGEMFactory", "d2/d05/classG4EvaporationGEMFactory.html", null ],
      [ "G4EvaporationGEMFactoryVI", "d2/d1a/classG4EvaporationGEMFactoryVI.html", null ]
    ] ],
    [ "G4VExceptionHandler", "d4/de5/classG4VExceptionHandler.html", [
      [ "G4ExceptionHandler", "da/dda/classG4ExceptionHandler.html", null ]
    ] ],
    [ "G4VExtDecayer", "db/d3d/classG4VExtDecayer.html", null ],
    [ "G4VExternalNavigation", "d4/d78/classG4VExternalNavigation.html", null ],
    [ "G4VFacet", "d4/d28/classG4VFacet.html", [
      [ "G4QuadrangularFacet", "d7/dfa/classG4QuadrangularFacet.html", null ],
      [ "G4TriangularFacet", "d0/db5/classG4TriangularFacet.html", null ]
    ] ],
    [ "G4VFastSimSensitiveDetector", "d2/dd1/classG4VFastSimSensitiveDetector.html", null ],
    [ "G4VFastSimulationModel", "da/dd9/classG4VFastSimulationModel.html", [
      [ "GFlashShowerModel", "d7/db1/classGFlashShowerModel.html", null ]
    ] ],
    [ "G4VFermiBreakUp", "d1/dc2/classG4VFermiBreakUp.html", [
      [ "G4FermiBreakUpVI", "d7/d13/classG4FermiBreakUpVI.html", null ]
    ] ],
    [ "G4VFieldPropagation", "d1/d56/classG4VFieldPropagation.html", [
      [ "G4RKPropagation", "d0/d62/classG4RKPropagation.html", null ]
    ] ],
    [ "G4VFigureFileMaker", "dd/df2/classG4VFigureFileMaker.html", [
      [ "G4RTJpegMaker", "d0/d48/classG4RTJpegMaker.html", null ]
    ] ],
    [ "G4VFilter< T >", "d1/d3c/classG4VFilter.html", [
      [ "G4SmartFilter< T >", "d8/d91/classG4SmartFilter.html", [
        [ "G4AttributeFilterT< T >", "d5/dd3/classG4AttributeFilterT.html", null ]
      ] ]
    ] ],
    [ "G4VFilter< G4AttValue >", "d1/d3c/classG4VFilter.html", [
      [ "G4VAttValueFilter", "db/d99/classG4VAttValueFilter.html", [
        [ "G4AttValueFilterT< T, ConversionErrorPolicy >", "d7/d37/classG4AttValueFilterT.html", null ]
      ] ]
    ] ],
    [ "G4VFilter< G4VTrajectory >", "d1/d3c/classG4VFilter.html", [
      [ "G4SmartFilter< G4VTrajectory >", "d8/d91/classG4SmartFilter.html", [
        [ "G4TrajectoryChargeFilter", "d4/d14/classG4TrajectoryChargeFilter.html", null ],
        [ "G4TrajectoryEncounteredVolumeFilter", "df/dab/classG4TrajectoryEncounteredVolumeFilter.html", null ],
        [ "G4TrajectoryOriginVolumeFilter", "d5/d0a/classG4TrajectoryOriginVolumeFilter.html", null ],
        [ "G4TrajectoryParticleFilter", "d9/def/classG4TrajectoryParticleFilter.html", null ]
      ] ]
    ] ],
    [ "G4VFinder", "d5/d55/classG4VFinder.html", [
      [ "G4OctreeFinder< T, CONTAINER >", "db/d24/classG4OctreeFinder.html", null ]
    ] ],
    [ "G4VFissionBarrier", "d8/da5/classG4VFissionBarrier.html", [
      [ "G4FissionBarrier", "d3/dd0/classG4FissionBarrier.html", null ]
    ] ],
    [ "G4VFlavoredParallelWorld", "d6/d45/classG4VFlavoredParallelWorld.html", null ],
    [ "G4VFSALIntegrationStepper", "dc/d6c/classG4VFSALIntegrationStepper.html", [
      [ "G4FSALBogackiShampine45", "d0/dd7/classG4FSALBogackiShampine45.html", null ],
      [ "G4FSALDormandPrince745", "d3/d5f/classG4FSALDormandPrince745.html", null ]
    ] ],
    [ "G4VGammaTransition", "de/d7a/classG4VGammaTransition.html", null ],
    [ "G4VGaussianQuadrature", "da/d95/classG4VGaussianQuadrature.html", [
      [ "G4GaussChebyshevQ", "dd/df8/classG4GaussChebyshevQ.html", null ],
      [ "G4GaussHermiteQ", "d3/d99/classG4GaussHermiteQ.html", null ],
      [ "G4GaussJacobiQ", "da/d4c/classG4GaussJacobiQ.html", null ],
      [ "G4GaussLaguerreQ", "d4/d66/classG4GaussLaguerreQ.html", null ],
      [ "G4GaussLegendreQ", "d0/d31/classG4GaussLegendreQ.html", null ]
    ] ],
    [ "G4VGCellFinder", "d5/d39/classG4VGCellFinder.html", null ],
    [ "G4VGFlashSensitiveDetector", "d7/d90/classG4VGFlashSensitiveDetector.html", null ],
    [ "G4VGlobalFastSimulationManager", "d6/da8/classG4VGlobalFastSimulationManager.html", null ],
    [ "G4VGraphicsScene", "da/d36/classG4VGraphicsScene.html", [
      [ "G4PseudoScene", "d5/de9/classG4PseudoScene.html", [
        [ "G4BoundingExtentScene", "d0/d6c/classG4BoundingExtentScene.html", null ],
        [ "G4BoundingSphereScene", "d5/dba/classG4BoundingSphereScene.html", null ],
        [ "G4PhysicalVolumeMassScene", "db/dc5/classG4PhysicalVolumeMassScene.html", null ],
        [ "G4PhysicalVolumeSearchScene", "d8/d66/classG4PhysicalVolumeSearchScene.html", null ],
        [ "G4PhysicalVolumesSearchScene", "d5/d4e/classG4PhysicalVolumesSearchScene.html", null ],
        [ "G4TouchablePropertiesScene", "d3/dc2/classG4TouchablePropertiesScene.html", null ]
      ] ],
      [ "G4VSceneHandler", "da/d94/classG4VSceneHandler.html", [
        [ "G4DAWNFILESceneHandler", "da/daa/classG4DAWNFILESceneHandler.html", null ],
        [ "G4GMocrenFileSceneHandler", "df/d82/classG4GMocrenFileSceneHandler.html", null ],
        [ "G4HepRepFileSceneHandler", "d0/d74/classG4HepRepFileSceneHandler.html", null ],
        [ "G4OpenGLSceneHandler", "dd/db8/classG4OpenGLSceneHandler.html", [
          [ "G4OpenGLImmediateSceneHandler", "d1/dc5/classG4OpenGLImmediateSceneHandler.html", null ],
          [ "G4OpenGLStoredSceneHandler", "d8/df6/classG4OpenGLStoredSceneHandler.html", [
            [ "G4OpenGLStoredQtSceneHandler", "d7/d1e/classG4OpenGLStoredQtSceneHandler.html", null ]
          ] ]
        ] ],
        [ "G4OpenInventorSceneHandler", "d1/d75/classG4OpenInventorSceneHandler.html", null ],
        [ "G4Qt3DSceneHandler", "d1/de1/classG4Qt3DSceneHandler.html", null ],
        [ "G4RayTracerSceneHandler", "d0/dcb/classG4RayTracerSceneHandler.html", null ],
        [ "G4ToolsSGSceneHandler", "d1/dcb/classG4ToolsSGSceneHandler.html", null ],
        [ "G4VRML2FileSceneHandler", "de/d44/classG4VRML2FileSceneHandler.html", null ],
        [ "G4VTreeSceneHandler", "dc/de1/classG4VTreeSceneHandler.html", [
          [ "G4ASCIITreeSceneHandler", "db/d85/classG4ASCIITreeSceneHandler.html", null ]
        ] ],
        [ "G4VtkSceneHandler", "d6/ddf/classG4VtkSceneHandler.html", [
          [ "G4VtkQtSceneHandler", "dc/d20/classG4VtkQtSceneHandler.html", null ]
        ] ]
      ] ]
    ] ],
    [ "G4VGraphicsSystem", "df/d51/classG4VGraphicsSystem.html", [
      [ "G4DAWNFILE", "d6/d6c/classG4DAWNFILE.html", null ],
      [ "G4GMocrenFile", "db/de4/classG4GMocrenFile.html", null ],
      [ "G4HepRepFile", "da/d9e/classG4HepRepFile.html", null ],
      [ "G4OpenGLImmediateWin32", "de/d85/classG4OpenGLImmediateWin32.html", null ],
      [ "G4OpenGLImmediateX", "d5/db9/classG4OpenGLImmediateX.html", null ],
      [ "G4OpenGLQt", "d7/da0/classG4OpenGLQt.html", [
        [ "G4OpenGLImmediateQt", "da/da0/classG4OpenGLImmediateQt.html", null ],
        [ "G4OpenGLStoredQt", "d6/d1b/classG4OpenGLStoredQt.html", null ]
      ] ],
      [ "G4OpenGLStoredWin32", "d5/da1/classG4OpenGLStoredWin32.html", null ],
      [ "G4OpenGLStoredX", "dd/d50/classG4OpenGLStoredX.html", null ],
      [ "G4OpenGLXm", "d0/d35/classG4OpenGLXm.html", [
        [ "G4OpenGLImmediateXm", "d0/dd8/classG4OpenGLImmediateXm.html", null ],
        [ "G4OpenGLStoredXm", "d0/d47/classG4OpenGLStoredXm.html", null ]
      ] ],
      [ "G4OpenInventor", "d4/dfc/classG4OpenInventor.html", [
        [ "G4OpenInventorQt", "d0/d6a/classG4OpenInventorQt.html", null ],
        [ "G4OpenInventorWin", "da/d9f/classG4OpenInventorWin.html", [
          [ "G4OpenInventorWin32", "dd/d3a/classG4OpenInventorWin32.html", null ]
        ] ],
        [ "G4OpenInventorXt", "d8/dc0/classG4OpenInventorXt.html", [
          [ "G4OpenInventorX", "d7/dec/classG4OpenInventorX.html", null ]
        ] ],
        [ "G4OpenInventorXtExtended", "da/d72/classG4OpenInventorXtExtended.html", null ]
      ] ],
      [ "G4Qt3D", "d7/d7d/classG4Qt3D.html", null ],
      [ "G4RayTracer", "d9/d7e/classG4RayTracer.html", null ],
      [ "G4RayTracerX", "d2/d18/classG4RayTracerX.html", null ],
      [ "G4ToolsSGQtGLES", "df/df9/classG4ToolsSGQtGLES.html", null ],
      [ "G4ToolsSGWindowsGLES", "df/db7/classG4ToolsSGWindowsGLES.html", null ],
      [ "G4ToolsSGX11GLES", "d8/de2/classG4ToolsSGX11GLES.html", null ],
      [ "G4ToolsSGXtGLES", "d8/dd1/classG4ToolsSGXtGLES.html", null ],
      [ "G4VRML2File", "db/dc4/classG4VRML2File.html", null ],
      [ "G4VTree", "db/d0f/classG4VTree.html", [
        [ "G4ASCIITree", "d5/dee/classG4ASCIITree.html", null ]
      ] ],
      [ "G4Vtk", "d3/dbb/classG4Vtk.html", null ],
      [ "G4VtkQt", "dc/dc9/classG4VtkQt.html", null ]
    ] ],
    [ "G4VH1Manager", "d5/d83/classG4VH1Manager.html", [
      [ "G4H1ToolsManager", "df/d39/classG4H1ToolsManager.html", null ]
    ] ],
    [ "G4VH2Manager", "d9/d5c/classG4VH2Manager.html", [
      [ "G4H2ToolsManager", "d7/d26/classG4H2ToolsManager.html", null ]
    ] ],
    [ "G4VH3Manager", "dd/d0a/classG4VH3Manager.html", [
      [ "G4H3ToolsManager", "d4/de3/classG4H3ToolsManager.html", null ]
    ] ],
    [ "G4VHadDecayAlgorithm", "db/d8c/classG4VHadDecayAlgorithm.html", [
      [ "G4CascadeFinalStateAlgorithm", "d3/d12/classG4CascadeFinalStateAlgorithm.html", null ],
      [ "G4VHadPhaseSpaceAlgorithm", "db/d77/classG4VHadPhaseSpaceAlgorithm.html", [
        [ "G4HadPhaseSpaceGenbod", "d1/d1a/classG4HadPhaseSpaceGenbod.html", null ],
        [ "G4HadPhaseSpaceKopylov", "dc/dc8/classG4HadPhaseSpaceKopylov.html", null ],
        [ "G4HadPhaseSpaceNBodyAsai", "d2/df8/classG4HadPhaseSpaceNBodyAsai.html", null ]
      ] ]
    ] ],
    [ "G4VHadronModelBuilder", "d1/dd5/classG4VHadronModelBuilder.html", [
      [ "G4FTFBuilder", "d6/dc2/classG4FTFBuilder.html", null ],
      [ "G4QGSBuilder", "d9/dc5/classG4QGSBuilder.html", null ]
    ] ],
    [ "G4VHCIOentry", "d6/d2f/classG4VHCIOentry.html", [
      [ "G4HCIOentryT< T >", "d8/dc2/classG4HCIOentryT.html", null ]
    ] ],
    [ "G4VhElectronicStoppingPower", "d9/dae/classG4VhElectronicStoppingPower.html", [
      [ "G4hICRU49He", "d3/d7f/classG4hICRU49He.html", null ],
      [ "G4hICRU49p", "dc/d31/classG4hICRU49p.html", null ],
      [ "G4hZiegler1985p", "d2/d3c/classG4hZiegler1985p.html", null ]
    ] ],
    [ "G4VHit", "d9/da3/classG4VHit.html", null ],
    [ "G4VHitsCollection", "dc/d91/classG4VHitsCollection.html", [
      [ "G4HitsCollection", "d2/dd3/classG4HitsCollection.html", [
        [ "G4VTHitsMap< _Tp, std::map< G4int, _Tp * > >", "d2/d3e/classG4VTHitsMap.html", [
          [ "G4THitsMap< _Tp >", "d7/d42/classG4THitsMap.html", null ]
        ] ],
        [ "G4VTHitsMap< _Tp, std::multimap< G4int, _Tp * > >", "d2/d3e/classG4VTHitsMap.html", [
          [ "G4THitsMultiMap< _Tp >", "d5/db3/classG4THitsMultiMap.html", null ]
        ] ],
        [ "G4VTHitsMap< _Tp, std::unordered_map< G4int, _Tp * > >", "d2/d3e/classG4VTHitsMap.html", [
          [ "G4THitsUnorderedMap< _Tp >", "d3/dcd/classG4THitsUnorderedMap.html", null ]
        ] ],
        [ "G4VTHitsMap< _Tp, std::unordered_multimap< G4int, _Tp * > >", "d2/d3e/classG4VTHitsMap.html", [
          [ "G4THitsUnorderedMultiMap< _Tp >", "d4/d66/classG4THitsUnorderedMultiMap.html", null ]
        ] ],
        [ "G4VTHitsMap< G4double, std::map< G4int, G4double * > >", "d2/d3e/classG4VTHitsMap.html", [
          [ "G4THitsMap< G4double >", "d7/d42/classG4THitsMap.html", null ]
        ] ],
        [ "G4VTHitsMap< G4Colour, std::map< G4int, G4Colour * > >", "d2/d3e/classG4VTHitsMap.html", [
          [ "G4THitsMap< G4Colour >", "d7/d42/classG4THitsMap.html", null ]
        ] ],
        [ "G4VTHitsVector< _Tp, std::deque< _Tp * > >", "db/d45/classG4VTHitsVector.html", [
          [ "G4THitsDeque< _Tp >", "d1/d20/classG4THitsDeque.html", null ]
        ] ],
        [ "G4VTHitsVector< _Tp, std::vector< _Tp * > >", "db/d45/classG4VTHitsVector.html", [
          [ "G4THitsVector< _Tp >", "d6/d51/classG4THitsVector.html", null ]
        ] ],
        [ "G4THitsCollection< T >", "d5/d58/classG4THitsCollection.html", null ],
        [ "G4VTHitsMap< T, Map_t >", "d2/d3e/classG4VTHitsMap.html", null ],
        [ "G4VTHitsVector< T, Vector_t >", "db/d45/classG4VTHitsVector.html", null ]
      ] ]
    ] ],
    [ "G4VhNuclearStoppingPower", "da/de6/classG4VhNuclearStoppingPower.html", [
      [ "G4hICRU49Nuclear", "d5/d19/classG4hICRU49Nuclear.html", null ],
      [ "G4hZiegler1985Nuclear", "d5/dc0/classG4hZiegler1985Nuclear.html", null ]
    ] ],
    [ "G4VhShellCrossSection", "d2/d80/classG4VhShellCrossSection.html", [
      [ "G4LivermoreIonisationCrossSection", "d1/d9b/classG4LivermoreIonisationCrossSection.html", null ],
      [ "G4PenelopeIonisationCrossSection", "d1/d17/classG4PenelopeIonisationCrossSection.html", null ],
      [ "G4empCrossSection", "d3/d98/classG4empCrossSection.html", null ],
      [ "G4teoCrossSection", "d3/dc8/classG4teoCrossSection.html", null ]
    ] ],
    [ "G4ViewParameters", "d7/dc2/classG4ViewParameters.html", null ],
    [ "G4VImportanceAlgorithm", "d3/de4/classG4VImportanceAlgorithm.html", [
      [ "G4ImportanceAlgorithm", "df/ddc/classG4ImportanceAlgorithm.html", null ]
    ] ],
    [ "G4VImportanceSplitExaminer", "dc/d7b/classG4VImportanceSplitExaminer.html", null ],
    [ "G4VIntegrationDriver", "d3/d58/classG4VIntegrationDriver.html", [
      [ "G4BFieldIntegrationDriver", "d6/db0/classG4BFieldIntegrationDriver.html", null ],
      [ "G4IntegrationDriver< G4BulirschStoer >", "d7/d06/classG4IntegrationDriver_3_01G4BulirschStoer_01_4.html", null ],
      [ "G4MagInt_Driver", "d2/df5/classG4MagInt__Driver.html", null ],
      [ "G4OldMagIntDriver", "de/d00/classG4OldMagIntDriver.html", null ],
      [ "G4RKIntegrationDriver< T >", "db/db4/classG4RKIntegrationDriver.html", [
        [ "G4FSALIntegrationDriver< T >", "d6/d6b/classG4FSALIntegrationDriver.html", null ],
        [ "G4IntegrationDriver< T >", "d1/dbc/classG4IntegrationDriver.html", null ],
        [ "G4InterpolationDriver< T >", "d7/d9c/classG4InterpolationDriver.html", null ]
      ] ]
    ] ],
    [ "G4VInteractiveSession", "d2/d71/classG4VInteractiveSession.html", [
      [ "G4UIQt", "d6/d5b/classG4UIQt.html", null ],
      [ "G4UIWin32", "da/d30/classG4UIWin32.html", null ],
      [ "G4UIXm", "d9/d40/classG4UIXm.html", null ]
    ] ],
    [ "G4VInteractorManager", "d4/de4/classG4VInteractorManager.html", [
      [ "G4Qt", "da/d98/classG4Qt.html", null ],
      [ "G4SoQt", "d6/d5c/classG4SoQt.html", null ],
      [ "G4Win32", "d2/d0c/classG4Win32.html", null ],
      [ "G4Xt", "d5/d48/classG4Xt.html", null ]
    ] ],
    [ "G4VIntersectionLocator", "d1/d32/classG4VIntersectionLocator.html", [
      [ "G4BrentLocator", "db/d11/classG4BrentLocator.html", null ],
      [ "G4MultiLevelLocator", "d0/db2/classG4MultiLevelLocator.html", null ],
      [ "G4SimpleLocator", "dc/d68/classG4SimpleLocator.html", null ]
    ] ],
    [ "G4VIonDEDXScalingAlgorithm", "d1/d57/classG4VIonDEDXScalingAlgorithm.html", [
      [ "G4IonDEDXScalingICRU73", "d9/d9e/classG4IonDEDXScalingICRU73.html", null ]
    ] ],
    [ "G4VIonDEDXTable", "d4/d83/classG4VIonDEDXTable.html", [
      [ "G4ExtDEDXTable", "da/d55/classG4ExtDEDXTable.html", null ],
      [ "G4IonStoppingData", "d4/d16/classG4IonStoppingData.html", null ]
    ] ],
    [ "G4VisAttributes", "dc/df1/classG4VisAttributes.html", null ],
    [ "G4VisExtent", "df/d57/classG4VisExtent.html", null ],
    [ "G4VisFilterManager< T >", "d9/d38/classG4VisFilterManager.html", null ],
    [ "G4VisFilterManager< G4VDigi >", "d9/d38/classG4VisFilterManager.html", null ],
    [ "G4VisFilterManager< G4VHit >", "d9/d38/classG4VisFilterManager.html", null ],
    [ "G4VisFilterManager< G4VTrajectory >", "d9/d38/classG4VisFilterManager.html", null ],
    [ "G4Visible", "d8/dda/classG4Visible.html", [
      [ "G4Polyhedron", "df/d2b/classG4Polyhedron.html", [
        [ "G4PolyhedronArbitrary", "d8/da7/classG4PolyhedronArbitrary.html", null ],
        [ "G4PolyhedronBox", "dd/d4d/classG4PolyhedronBox.html", null ],
        [ "G4PolyhedronCone", "da/d1d/classG4PolyhedronCone.html", null ],
        [ "G4PolyhedronCons", "dd/d77/classG4PolyhedronCons.html", null ],
        [ "G4PolyhedronEllipsoid", "d1/d69/classG4PolyhedronEllipsoid.html", null ],
        [ "G4PolyhedronEllipticalCone", "da/dba/classG4PolyhedronEllipticalCone.html", null ],
        [ "G4PolyhedronHype", "d3/dbf/classG4PolyhedronHype.html", null ],
        [ "G4PolyhedronHyperbolicMirror", "db/d76/classG4PolyhedronHyperbolicMirror.html", null ],
        [ "G4PolyhedronPara", "d7/d7d/classG4PolyhedronPara.html", null ],
        [ "G4PolyhedronParaboloid", "d4/daa/classG4PolyhedronParaboloid.html", null ],
        [ "G4PolyhedronPcon", "d2/d9c/classG4PolyhedronPcon.html", null ],
        [ "G4PolyhedronPgon", "df/d0a/classG4PolyhedronPgon.html", null ],
        [ "G4PolyhedronSphere", "d4/d16/classG4PolyhedronSphere.html", null ],
        [ "G4PolyhedronTet", "d5/dce/classG4PolyhedronTet.html", null ],
        [ "G4PolyhedronTorus", "d0/d10/classG4PolyhedronTorus.html", null ],
        [ "G4PolyhedronTrap", "d8/db4/classG4PolyhedronTrap.html", null ],
        [ "G4PolyhedronTrd1", "df/d80/classG4PolyhedronTrd1.html", null ],
        [ "G4PolyhedronTrd2", "d9/dc6/classG4PolyhedronTrd2.html", null ],
        [ "G4PolyhedronTube", "dd/d87/classG4PolyhedronTube.html", null ],
        [ "G4PolyhedronTubs", "da/d19/classG4PolyhedronTubs.html", null ]
      ] ],
      [ "G4Polyline", "d0/dd9/classG4Polyline.html", null ],
      [ "G4VMarker", "da/d97/classG4VMarker.html", [
        [ "G4Circle", "d4/de5/classG4Circle.html", null ],
        [ "G4Polymarker", "d6/d89/classG4Polymarker.html", null ],
        [ "G4Square", "d1/dcc/classG4Square.html", null ],
        [ "G4Text", "da/d06/classG4Text.html", null ]
      ] ]
    ] ],
    [ "G4VisListManager< T >", "d8/d1b/classG4VisListManager.html", null ],
    [ "G4VisModelManager< Model >", "dc/dba/classG4VisModelManager.html", null ],
    [ "G4VisModelManager< G4VTrajectoryModel >", "dc/dba/classG4VisModelManager.html", null ],
    [ "G4VIsotopeTable", "dc/de0/classG4VIsotopeTable.html", [
      [ "G4IsotopeMagneticMomentTable", "df/d39/classG4IsotopeMagneticMomentTable.html", null ],
      [ "G4NuclideTable", "d7/d24/classG4NuclideTable.html", null ]
    ] ],
    [ "G4VIStore", "d4/d48/classG4VIStore.html", [
      [ "G4IStore", "d7/de9/classG4IStore.html", null ]
    ] ],
    [ "G4VisTrajContext", "df/d56/classG4VisTrajContext.html", null ],
    [ "G4VITFinder", "d9/d5b/classG4VITFinder.html", [
      [ "G4ITFinder< T >", "db/d43/classG4ITFinder.html", null ]
    ] ],
    [ "G4VITReactionProcess", "d3/d96/classG4VITReactionProcess.html", [
      [ "G4DNAIRT", "d2/da4/classG4DNAIRT.html", null ],
      [ "G4DNAIRT_geometries", "dc/db9/classG4DNAIRT__geometries.html", null ],
      [ "G4DNAMakeReaction", "d2/dc1/classG4DNAMakeReaction.html", null ],
      [ "G4DNAMolecularReaction", "d2/d3e/classG4DNAMolecularReaction.html", null ]
    ] ],
    [ "G4VITStepModel", "de/d83/classG4VITStepModel.html", [
      [ "G4DNAIndependentReactionTimeModel", "d8/d75/classG4DNAIndependentReactionTimeModel.html", null ],
      [ "G4DNAMolecularIRTModel", "d8/d11/classG4DNAMolecularIRTModel.html", null ],
      [ "G4DNAMolecularStepByStepModel", "df/d69/classG4DNAMolecularStepByStepModel.html", null ]
    ] ],
    [ "G4VITTimeStepComputer", "d5/d79/classG4VITTimeStepComputer.html", [
      [ "G4DNAIRTMoleculeEncounterStepper", "d1/d19/classG4DNAIRTMoleculeEncounterStepper.html", null ],
      [ "G4DNAIndependentReactionTimeStepper", "d1/d6c/classG4DNAIndependentReactionTimeStepper.html", null ],
      [ "G4DNAMoleculeEncounterStepper", "d6/d77/classG4DNAMoleculeEncounterStepper.html", null ]
    ] ],
    [ "G4VITTrackHolder", "d0/d12/classG4VITTrackHolder.html", [
      [ "G4ITTrackHolder", "d6/dfd/classG4ITTrackHolder.html", null ]
    ] ],
    [ "G4VKineticNucleon", "d8/d81/classG4VKineticNucleon.html", [
      [ "G4KineticTrack", "d4/db0/classG4KineticTrack.html", null ],
      [ "G4Nucleon", "d1/dca/classG4Nucleon.html", null ]
    ] ],
    [ "G4VKM_NuclearDensity", "dd/de0/classG4VKM__NuclearDensity.html", null ],
    [ "G4VLeadingParticleBiasing", "d4/d37/classG4VLeadingParticleBiasing.html", [
      [ "G4HadLeadBias", "dc/d1d/classG4HadLeadBias.html", null ]
    ] ],
    [ "G4VLevelDensityParameter", "de/d71/classG4VLevelDensityParameter.html", [
      [ "G4ConstantLevelDensityParameter", "d8/df8/classG4ConstantLevelDensityParameter.html", null ],
      [ "G4EvaporationLevelDensityParameter", "d7/de3/classG4EvaporationLevelDensityParameter.html", null ],
      [ "G4FissionLevelDensityParameter", "da/d4b/classG4FissionLevelDensityParameter.html", null ],
      [ "G4FissionLevelDensityParameterINCLXX", "da/d6a/classG4FissionLevelDensityParameterINCLXX.html", null ]
    ] ],
    [ "G4VLowEnergyModel", "d4/d6c/classG4VLowEnergyModel.html", [
      [ "G4IonChuFluctuationModel", "d3/d92/classG4IonChuFluctuationModel.html", null ],
      [ "G4IonYangFluctuationModel", "da/ddd/classG4IonYangFluctuationModel.html", null ],
      [ "G4QAOLowEnergyLoss", "d3/d5d/classG4QAOLowEnergyLoss.html", null ],
      [ "G4hBetheBlochModel", "d1/deb/classG4hBetheBlochModel.html", null ],
      [ "G4hIonEffChargeSquare", "d4/ddb/classG4hIonEffChargeSquare.html", null ],
      [ "G4hNuclearStoppingModel", "d5/d6e/classG4hNuclearStoppingModel.html", null ],
      [ "G4hParametrisedLossModel", "d9/deb/classG4hParametrisedLossModel.html", null ]
    ] ],
    [ "G4VMaterialExtension", "d7/dd5/classG4VMaterialExtension.html", [
      [ "G4ChannelingMaterialData", "d7/dbe/classG4ChannelingMaterialData.html", null ],
      [ "G4CrystalExtension", "dd/db5/classG4CrystalExtension.html", null ],
      [ "G4MIData", "d9/d04/classG4MIData.html", null ]
    ] ],
    [ "G4VMCTruthIO", "d0/dbc/classG4VMCTruthIO.html", null ],
    [ "G4VModel", "dd/d30/classG4VModel.html", [
      [ "G4ArrowModel", "d7/d6b/classG4ArrowModel.html", null ],
      [ "G4AxesModel", "d4/d76/classG4AxesModel.html", null ],
      [ "G4CallbackModel< F >", "da/d64/classG4CallbackModel.html", null ],
      [ "G4DigiModel", "d8/d3e/classG4DigiModel.html", null ],
      [ "G4GPSModel", "dc/d45/classG4GPSModel.html", null ],
      [ "G4HitsModel", "de/d43/classG4HitsModel.html", null ],
      [ "G4NullModel", "d6/dbe/classG4NullModel.html", null ],
      [ "G4PSHitsModel", "d8/d8d/classG4PSHitsModel.html", null ],
      [ "G4PhysicalVolumeModel", "dc/dec/classG4PhysicalVolumeModel.html", [
        [ "G4LogicalVolumeModel", "d1/d7a/classG4LogicalVolumeModel.html", null ]
      ] ],
      [ "G4PlotterModel", "df/db9/classG4PlotterModel.html", null ],
      [ "G4TextModel", "d4/dbc/classG4TextModel.html", null ],
      [ "G4TrajectoriesModel", "dd/db0/classG4TrajectoriesModel.html", null ],
      [ "G4VFieldModel", "df/d78/classG4VFieldModel.html", [
        [ "G4ElectricFieldModel", "d1/dbe/classG4ElectricFieldModel.html", null ],
        [ "G4MagneticFieldModel", "dd/dc0/classG4MagneticFieldModel.html", null ]
      ] ]
    ] ],
    [ "G4VModelFactory< T >", "d7/dc3/classG4VModelFactory.html", null ],
    [ "G4VModelFactory< G4VFilter< G4VDigi > >", "d7/dc3/classG4VModelFactory.html", [
      [ "G4DigiAttributeFilterFactory", "d2/d30/classG4DigiAttributeFilterFactory.html", null ]
    ] ],
    [ "G4VModelFactory< G4VFilter< G4VHit > >", "d7/dc3/classG4VModelFactory.html", [
      [ "G4HitAttributeFilterFactory", "d4/d04/classG4HitAttributeFilterFactory.html", null ]
    ] ],
    [ "G4VModelFactory< G4VFilter< G4VTrajectory > >", "d7/dc3/classG4VModelFactory.html", [
      [ "G4TrajectoryAttributeFilterFactory", "d4/d04/classG4TrajectoryAttributeFilterFactory.html", null ],
      [ "G4TrajectoryChargeFilterFactory", "d8/d65/classG4TrajectoryChargeFilterFactory.html", null ],
      [ "G4TrajectoryEncounteredVolumeFilterFactory", "d9/d47/classG4TrajectoryEncounteredVolumeFilterFactory.html", null ],
      [ "G4TrajectoryOriginVolumeFilterFactory", "d3/d7d/classG4TrajectoryOriginVolumeFilterFactory.html", null ],
      [ "G4TrajectoryParticleFilterFactory", "d6/d2b/classG4TrajectoryParticleFilterFactory.html", null ]
    ] ],
    [ "G4VModelFactory< G4VTrajectoryModel >", "d7/dc3/classG4VModelFactory.html", [
      [ "G4TrajectoryDrawByAttributeFactory", "d6/dbb/classG4TrajectoryDrawByAttributeFactory.html", null ],
      [ "G4TrajectoryDrawByChargeFactory", "d7/d9e/classG4TrajectoryDrawByChargeFactory.html", null ],
      [ "G4TrajectoryDrawByEncounteredVolumeFactory", "d9/d07/classG4TrajectoryDrawByEncounteredVolumeFactory.html", null ],
      [ "G4TrajectoryDrawByOriginVolumeFactory", "d0/d0c/classG4TrajectoryDrawByOriginVolumeFactory.html", null ],
      [ "G4TrajectoryDrawByParticleIDFactory", "d0/dc9/classG4TrajectoryDrawByParticleIDFactory.html", null ],
      [ "G4TrajectoryGenericDrawerFactory", "d0/de6/classG4TrajectoryGenericDrawerFactory.html", null ]
    ] ],
    [ "G4VMolecularDissociationDisplacer", "de/da8/classG4VMolecularDissociationDisplacer.html", [
      [ "G4DNAWaterDissociationDisplacer", "d8/dcc/classG4DNAWaterDissociationDisplacer.html", null ]
    ] ],
    [ "G4VMoleculeCounter", "db/d48/classG4VMoleculeCounter.html", [
      [ "G4MoleculeCounter", "db/d6f/classG4MoleculeCounter.html", null ]
    ] ],
    [ "G4VMPLData", "d3/d0a/classG4VMPLData.html", null ],
    [ "G4VMultiBodyMomDst", "da/dca/classG4VMultiBodyMomDst.html", [
      [ "G4InuclParamMomDst", "da/daa/classG4InuclParamMomDst.html", [
        [ "G4HadNucl3BodyMomDst", "d0/d02/classG4HadNucl3BodyMomDst.html", null ],
        [ "G4HadNucl4BodyMomDst", "d3/d27/classG4HadNucl4BodyMomDst.html", null ],
        [ "G4NuclNucl3BodyMomDst", "d4/d3b/classG4NuclNucl3BodyMomDst.html", null ],
        [ "G4NuclNucl4BodyMomDst", "d4/d50/classG4NuclNucl4BodyMomDst.html", null ]
      ] ]
    ] ],
    [ "G4VMultiFragmentation", "df/db0/classG4VMultiFragmentation.html", [
      [ "G4StatMF", "dd/d2a/classG4StatMF.html", null ]
    ] ],
    [ "G4VNotifier", "dd/dd0/classG4VNotifier.html", null ],
    [ "G4VNtupleFileManager", "d1/dc0/classG4VNtupleFileManager.html", [
      [ "G4CsvNtupleFileManager", "d0/d12/classG4CsvNtupleFileManager.html", null ],
      [ "G4Hdf5NtupleFileManager", "d1/d82/classG4Hdf5NtupleFileManager.html", null ],
      [ "G4RootNtupleFileManager", "db/d71/classG4RootNtupleFileManager.html", null ],
      [ "G4XmlNtupleFileManager", "db/d95/classG4XmlNtupleFileManager.html", null ]
    ] ],
    [ "G4VNuclearDensity", "db/dd0/classG4VNuclearDensity.html", [
      [ "G4NuclearFermiDensity", "dc/d6f/classG4NuclearFermiDensity.html", null ],
      [ "G4NuclearShellModelDensity", "d2/d70/classG4NuclearShellModelDensity.html", null ]
    ] ],
    [ "G4VNuclearField", "d3/d8f/classG4VNuclearField.html", [
      [ "G4AntiProtonField", "d2/db2/classG4AntiProtonField.html", null ],
      [ "G4KaonMinusField", "df/d44/classG4KaonMinusField.html", null ],
      [ "G4KaonPlusField", "da/d5f/classG4KaonPlusField.html", null ],
      [ "G4KaonZeroField", "d1/d28/classG4KaonZeroField.html", null ],
      [ "G4NeutronField", "d5/d2d/classG4NeutronField.html", null ],
      [ "G4PionMinusField", "d0/d67/classG4PionMinusField.html", null ],
      [ "G4PionPlusField", "d0/de1/classG4PionPlusField.html", null ],
      [ "G4PionZeroField", "df/dc5/classG4PionZeroField.html", null ],
      [ "G4ProtonField", "d8/d11/classG4ProtonField.html", null ],
      [ "G4SigmaMinusField", "da/d45/classG4SigmaMinusField.html", null ],
      [ "G4SigmaPlusField", "d4/d19/classG4SigmaPlusField.html", null ],
      [ "G4SigmaZeroField", "da/d0d/classG4SigmaZeroField.html", null ]
    ] ],
    [ "G4Volant", "d9/d50/classG4Volant.html", null ],
    [ "G4Voxel", "d9/dde/classG4Voxel.html", null ],
    [ "G4VoxelBox", "d5/d9e/structG4VoxelBox.html", null ],
    [ "G4Voxelizer::G4VoxelComparator", "d7/de8/classG4Voxelizer_1_1G4VoxelComparator.html", null ],
    [ "G4VoxelInfo", "d2/ddf/structG4VoxelInfo.html", null ],
    [ "G4Voxelizer", "da/d11/classG4Voxelizer.html", null ],
    [ "G4VoxelLimits", "de/d33/classG4VoxelLimits.html", null ],
    [ "G4VoxelNavigation", "d6/da7/classG4VoxelNavigation.html", [
      [ "G4ParameterisedNavigation", "dd/dff/classG4ParameterisedNavigation.html", null ]
    ] ],
    [ "G4VoxelSafety", "db/de4/classG4VoxelSafety.html", null ],
    [ "G4VP1Manager", "dd/d25/classG4VP1Manager.html", [
      [ "G4P1ToolsManager", "dc/d19/classG4P1ToolsManager.html", null ]
    ] ],
    [ "G4VP2Manager", "d2/d71/classG4VP2Manager.html", [
      [ "G4P2ToolsManager", "da/dd0/classG4P2ToolsManager.html", null ]
    ] ],
    [ "G4VParticipants", "d6/d61/classG4VParticipants.html", [
      [ "G4FTFParticipants", "d3/d32/classG4FTFParticipants.html", null ],
      [ "G4QGSParticipants", "d0/d64/classG4QGSParticipants.html", [
        [ "G4GammaParticipants", "dc/d85/classG4GammaParticipants.html", null ]
      ] ]
    ] ],
    [ "G4VParticleChange", "d6/d6c/classG4VParticleChange.html", [
      [ "G4FastStep", "d4/df5/classG4FastStep.html", null ],
      [ "G4ParticleChange", "d2/d06/classG4ParticleChange.html", [
        [ "G4ParticleChangeForTransport", "d6/d31/classG4ParticleChangeForTransport.html", null ]
      ] ],
      [ "G4ParticleChangeForDecay", "d7/df8/classG4ParticleChangeForDecay.html", [
        [ "G4ParticleChangeForRadDecay", "d6/de3/classG4ParticleChangeForRadDecay.html", null ]
      ] ],
      [ "G4ParticleChangeForGamma", "d7/dcb/classG4ParticleChangeForGamma.html", null ],
      [ "G4ParticleChangeForLoss", "dd/dc0/classG4ParticleChangeForLoss.html", null ],
      [ "G4ParticleChangeForMSC", "d7/d2c/classG4ParticleChangeForMSC.html", null ],
      [ "G4ParticleChangeForNothing", "df/d11/classG4ParticleChangeForNothing.html", null ],
      [ "G4ParticleChangeForOccurenceBiasing", "d9/d47/classG4ParticleChangeForOccurenceBiasing.html", null ]
    ] ],
    [ "G4VParticleHPEDis", "d0/d8f/classG4VParticleHPEDis.html", [
      [ "G4ParticleHPArbitaryTab", "d8/dd9/classG4ParticleHPArbitaryTab.html", null ],
      [ "G4ParticleHPEvapSpectrum", "dc/dfb/classG4ParticleHPEvapSpectrum.html", null ],
      [ "G4ParticleHPFissionSpectrum", "d7/d79/classG4ParticleHPFissionSpectrum.html", null ],
      [ "G4ParticleHPMadlandNixSpectrum", "d2/d84/classG4ParticleHPMadlandNixSpectrum.html", null ],
      [ "G4ParticleHPSimpleEvapSpectrum", "df/d90/classG4ParticleHPSimpleEvapSpectrum.html", null ],
      [ "G4ParticleHPWattSpectrum", "d8/d7c/classG4ParticleHPWattSpectrum.html", null ]
    ] ],
    [ "G4VParticleHPEnergyAngular", "d4/d9a/classG4VParticleHPEnergyAngular.html", [
      [ "G4ParticleHPContEnergyAngular", "d3/d37/classG4ParticleHPContEnergyAngular.html", null ],
      [ "G4ParticleHPDiscreteTwoBody", "d1/de0/classG4ParticleHPDiscreteTwoBody.html", null ],
      [ "G4ParticleHPIsotropic", "d4/d27/classG4ParticleHPIsotropic.html", null ],
      [ "G4ParticleHPLabAngularEnergy", "df/d92/classG4ParticleHPLabAngularEnergy.html", null ],
      [ "G4ParticleHPNBodyPhaseSpace", "d8/d15/classG4ParticleHPNBodyPhaseSpace.html", null ]
    ] ],
    [ "G4VParticlePropertyReporter", "d8/d69/classG4VParticlePropertyReporter.html", [
      [ "G4HtmlPPReporter", "d7/dd4/classG4HtmlPPReporter.html", null ],
      [ "G4SimplePPReporter", "db/dc3/classG4SimplePPReporter.html", null ],
      [ "G4TextPPReporter", "dd/d95/classG4TextPPReporter.html", null ]
    ] ],
    [ "G4VParticlePropertyRetriever", "df/d8a/classG4VParticlePropertyRetriever.html", [
      [ "G4TextPPRetriever", "dc/ddb/classG4TextPPRetriever.html", null ]
    ] ],
    [ "G4VPCData", "d7/d5d/classG4VPCData.html", null ],
    [ "G4VPDigitIO", "d4/d8a/classG4VPDigitIO.html", null ],
    [ "G4VPDigitsCollectionIO", "d2/d81/classG4VPDigitsCollectionIO.html", null ],
    [ "G4VPersistencyManager", "d0/ddc/classG4VPersistencyManager.html", [
      [ "G4PersistencyManager", "d5/dbf/classG4PersistencyManager.html", [
        [ "G4PersistencyManagerT< T >", "df/d89/classG4PersistencyManagerT.html", null ]
      ] ]
    ] ],
    [ "G4VPEventIO", "d6/dde/classG4VPEventIO.html", null ],
    [ "G4VPHitIO", "d1/d52/classG4VPHitIO.html", null ],
    [ "G4VPHitsCollectionIO", "d4/d4a/classG4VPHitsCollectionIO.html", null ],
    [ "G4VPhysChemIO", "d8/d83/classG4VPhysChemIO.html", [
      [ "G4PhysChemIO::FormattedText", "dd/d51/classG4PhysChemIO_1_1FormattedText.html", null ],
      [ "G4PhysChemIO::G4Analysis", "df/de9/classG4PhysChemIO_1_1G4Analysis.html", null ]
    ] ],
    [ "G4VPhysicalVolume", "d1/d28/classG4VPhysicalVolume.html", [
      [ "G4PVPlacement", "dd/d4f/classG4PVPlacement.html", null ],
      [ "G4PVReplica", "de/d93/classG4PVReplica.html", [
        [ "G4PVDivision", "df/d63/classG4PVDivision.html", null ],
        [ "G4PVParameterised", "de/ded/classG4PVParameterised.html", null ],
        [ "G4ReplicatedSlice", "d6/d94/classG4ReplicatedSlice.html", null ]
      ] ],
      [ "G4VExternalPhysicalVolume", "dd/d2d/classG4VExternalPhysicalVolume.html", null ]
    ] ],
    [ "G4VPhysicsConstructor", "d1/d2f/classG4VPhysicsConstructor.html", [
      [ "G4ChargeExchangePhysics", "d6/d57/classG4ChargeExchangePhysics.html", null ],
      [ "G4DecayPhysics", "d9/d5f/classG4DecayPhysics.html", null ],
      [ "G4EmDNAChemistry", "d3/df8/classG4EmDNAChemistry.html", null ],
      [ "G4EmDNAChemistry_option1", "d6/d81/classG4EmDNAChemistry__option1.html", null ],
      [ "G4EmDNAChemistry_option2", "df/dbe/classG4EmDNAChemistry__option2.html", null ],
      [ "G4EmDNAChemistry_option3", "dd/df0/classG4EmDNAChemistry__option3.html", null ],
      [ "G4EmDNAPhysics", "da/df0/classG4EmDNAPhysics.html", [
        [ "G4EmDNAPhysics_stationary", "d6/ddd/classG4EmDNAPhysics__stationary.html", null ]
      ] ],
      [ "G4EmDNAPhysicsActivator", "dc/d47/classG4EmDNAPhysicsActivator.html", null ],
      [ "G4EmDNAPhysics_option1", "d1/d65/classG4EmDNAPhysics__option1.html", null ],
      [ "G4EmDNAPhysics_option2", "de/d92/classG4EmDNAPhysics__option2.html", null ],
      [ "G4EmDNAPhysics_option3", "d6/dd6/classG4EmDNAPhysics__option3.html", null ],
      [ "G4EmDNAPhysics_option4", "d1/d64/classG4EmDNAPhysics__option4.html", null ],
      [ "G4EmDNAPhysics_option5", "d2/d05/classG4EmDNAPhysics__option5.html", null ],
      [ "G4EmDNAPhysics_option6", "d7/def/classG4EmDNAPhysics__option6.html", null ],
      [ "G4EmDNAPhysics_option7", "d8/da4/classG4EmDNAPhysics__option7.html", null ],
      [ "G4EmDNAPhysics_option8", "d5/d98/classG4EmDNAPhysics__option8.html", null ],
      [ "G4EmDNAPhysics_stationary_option2", "d2/de5/classG4EmDNAPhysics__stationary__option2.html", null ],
      [ "G4EmDNAPhysics_stationary_option4", "d4/d9f/classG4EmDNAPhysics__stationary__option4.html", null ],
      [ "G4EmDNAPhysics_stationary_option6", "d8/d1c/classG4EmDNAPhysics__stationary__option6.html", null ],
      [ "G4EmExtraPhysics", "d1/de1/classG4EmExtraPhysics.html", null ],
      [ "G4EmLivermorePhysics", "d3/da8/classG4EmLivermorePhysics.html", [
        [ "G4EmLivermorePolarizedPhysics", "d2/da2/classG4EmLivermorePolarizedPhysics.html", null ]
      ] ],
      [ "G4EmLowEPPhysics", "d6/de3/classG4EmLowEPPhysics.html", null ],
      [ "G4EmPenelopePhysics", "d5/dd0/classG4EmPenelopePhysics.html", null ],
      [ "G4EmStandardPhysics", "d6/da3/classG4EmStandardPhysics.html", null ],
      [ "G4EmStandardPhysicsGS", "db/d0b/classG4EmStandardPhysicsGS.html", null ],
      [ "G4EmStandardPhysicsSS", "d8/d1f/classG4EmStandardPhysicsSS.html", null ],
      [ "G4EmStandardPhysicsWVI", "d4/d49/classG4EmStandardPhysicsWVI.html", null ],
      [ "G4EmStandardPhysics_option1", "de/d40/classG4EmStandardPhysics__option1.html", null ],
      [ "G4EmStandardPhysics_option2", "df/d38/classG4EmStandardPhysics__option2.html", null ],
      [ "G4EmStandardPhysics_option3", "d6/d7c/classG4EmStandardPhysics__option3.html", null ],
      [ "G4EmStandardPhysics_option4", "de/d99/classG4EmStandardPhysics__option4.html", null ],
      [ "G4FastSimulationPhysics", "dd/dc4/classG4FastSimulationPhysics.html", null ],
      [ "G4GenericBiasingPhysics", "d9/d6d/classG4GenericBiasingPhysics.html", null ],
      [ "G4HadronElasticPhysics", "d7/d7f/classG4HadronElasticPhysics.html", [
        [ "G4HadronDElasticPhysics", "db/d7c/classG4HadronDElasticPhysics.html", null ],
        [ "G4HadronElasticPhysicsHP", "d1/db1/classG4HadronElasticPhysicsHP.html", null ],
        [ "G4HadronElasticPhysicsLEND", "df/de5/classG4HadronElasticPhysicsLEND.html", null ],
        [ "G4HadronElasticPhysicsPHP", "d0/df6/classG4HadronElasticPhysicsPHP.html", null ],
        [ "G4HadronElasticPhysicsXS", "d5/db4/classG4HadronElasticPhysicsXS.html", null ],
        [ "G4HadronHElasticPhysics", "d4/d65/classG4HadronHElasticPhysics.html", null ]
      ] ],
      [ "G4HadronPhysicsFTFP_BERT", "da/df3/classG4HadronPhysicsFTFP__BERT.html", [
        [ "G4HadronPhysicsFTFP_BERT_ATL", "db/dd2/classG4HadronPhysicsFTFP__BERT__ATL.html", null ],
        [ "G4HadronPhysicsFTFP_BERT_HP", "d4/dae/classG4HadronPhysicsFTFP__BERT__HP.html", null ],
        [ "G4HadronPhysicsFTFP_BERT_TRV", "db/d24/classG4HadronPhysicsFTFP__BERT__TRV.html", null ],
        [ "G4HadronPhysicsFTFQGSP_BERT", "d3/d2b/classG4HadronPhysicsFTFQGSP__BERT.html", null ],
        [ "G4HadronPhysicsFTF_BIC", "df/df3/classG4HadronPhysicsFTF__BIC.html", null ],
        [ "G4HadronPhysicsINCLXX", "d8/d83/classG4HadronPhysicsINCLXX.html", null ],
        [ "G4HadronPhysicsNuBeam", "df/d24/classG4HadronPhysicsNuBeam.html", null ],
        [ "G4HadronPhysicsShielding", "d9/d3e/classG4HadronPhysicsShielding.html", [
          [ "G4HadronPhysicsShieldingLEND", "df/deb/classG4HadronPhysicsShieldingLEND.html", null ]
        ] ]
      ] ],
      [ "G4HadronPhysicsQGSP_BERT", "d9/d14/classG4HadronPhysicsQGSP__BERT.html", [
        [ "G4HadronPhysicsQGSP_BERT_HP", "d9/d63/classG4HadronPhysicsQGSP__BERT__HP.html", null ],
        [ "G4HadronPhysicsQGSP_FTFP_BERT", "de/dff/classG4HadronPhysicsQGSP__FTFP__BERT.html", null ],
        [ "G4HadronPhysicsQGS_BIC", "d8/dbb/classG4HadronPhysicsQGS__BIC.html", null ]
      ] ],
      [ "G4HadronPhysicsQGSP_BIC", "d3/d9f/classG4HadronPhysicsQGSP__BIC.html", [
        [ "G4HadronPhysicsQGSP_BIC_HP", "d2/d46/classG4HadronPhysicsQGSP__BIC__HP.html", [
          [ "G4HadronPhysicsQGSP_BIC_AllHP", "d2/df9/classG4HadronPhysicsQGSP__BIC__AllHP.html", null ]
        ] ]
      ] ],
      [ "G4ImportanceBiasing", "df/d7d/classG4ImportanceBiasing.html", null ],
      [ "G4IonElasticPhysics", "de/d36/classG4IonElasticPhysics.html", null ],
      [ "G4IonINCLXXPhysics", "d9/dfa/classG4IonINCLXXPhysics.html", null ],
      [ "G4IonPhysics", "da/dcd/classG4IonPhysics.html", [
        [ "G4IonBinaryCascadePhysics", "dc/de5/classG4IonBinaryCascadePhysics.html", null ],
        [ "G4IonPhysicsXS", "d2/d80/classG4IonPhysicsXS.html", null ]
      ] ],
      [ "G4IonPhysicsPHP", "df/d98/classG4IonPhysicsPHP.html", null ],
      [ "G4IonQMDPhysics", "d0/da0/classG4IonQMDPhysics.html", null ],
      [ "G4MuonicAtomDecayPhysics", "de/d66/classG4MuonicAtomDecayPhysics.html", null ],
      [ "G4NeutronCrossSectionXS", "db/dd5/classG4NeutronCrossSectionXS.html", null ],
      [ "G4NeutronTrackingCut", "da/d5b/classG4NeutronTrackingCut.html", null ],
      [ "G4OpticalPhysics", "d1/dbc/classG4OpticalPhysics.html", null ],
      [ "G4ParallelWorldPhysics", "d0/d86/classG4ParallelWorldPhysics.html", null ],
      [ "G4RadioactiveDecayPhysics", "db/d82/classG4RadioactiveDecayPhysics.html", null ],
      [ "G4SpinDecayPhysics", "d1/d3b/classG4SpinDecayPhysics.html", null ],
      [ "G4StepLimiterPhysics", "dd/d40/classG4StepLimiterPhysics.html", null ],
      [ "G4StoppingPhysics", "d1/de4/classG4StoppingPhysics.html", null ],
      [ "G4StoppingPhysicsFritiofWithBinaryCascade", "d7/d60/classG4StoppingPhysicsFritiofWithBinaryCascade.html", null ],
      [ "G4UnknownDecayPhysics", "df/d5d/classG4UnknownDecayPhysics.html", null ],
      [ "G4VHadronPhysics", "d8/d6e/classG4VHadronPhysics.html", [
        [ "G4HadronInelasticQBBC", "d7/de0/classG4HadronInelasticQBBC.html", null ],
        [ "G4ThermalNeutrons", "d2/dba/classG4ThermalNeutrons.html", null ]
      ] ],
      [ "G4WeightWindowBiasing", "da/dad/classG4WeightWindowBiasing.html", null ]
    ] ],
    [ "G4VPolarizedXS", "d5/d2e/classG4VPolarizedXS.html", [
      [ "G4PolarizedAnnihilationXS", "da/d9f/classG4PolarizedAnnihilationXS.html", null ],
      [ "G4PolarizedBremsstrahlungXS", "da/d0f/classG4PolarizedBremsstrahlungXS.html", null ],
      [ "G4PolarizedComptonXS", "d8/d37/classG4PolarizedComptonXS.html", null ],
      [ "G4PolarizedGammaConversionXS", "d2/d68/classG4PolarizedGammaConversionXS.html", null ],
      [ "G4PolarizedIonisationBhabhaXS", "d2/d17/classG4PolarizedIonisationBhabhaXS.html", null ],
      [ "G4PolarizedIonisationMollerXS", "de/d04/classG4PolarizedIonisationMollerXS.html", null ],
      [ "G4PolarizedPhotoElectricXS", "d4/d6f/classG4PolarizedPhotoElectricXS.html", null ]
    ] ],
    [ "G4VPreCompoundEmissionFactory", "df/da0/classG4VPreCompoundEmissionFactory.html", [
      [ "G4HETCEmissionFactory", "de/d58/classG4HETCEmissionFactory.html", null ],
      [ "G4PreCompoundEmissionFactory", "db/d7b/classG4PreCompoundEmissionFactory.html", null ]
    ] ],
    [ "G4VPreCompoundFragment", "d6/df3/classG4VPreCompoundFragment.html", [
      [ "G4HETCFragment", "d2/d36/classG4HETCFragment.html", [
        [ "G4HETCChargedFragment", "df/d63/classG4HETCChargedFragment.html", [
          [ "G4HETCAlpha", "d2/d47/classG4HETCAlpha.html", null ],
          [ "G4HETCDeuteron", "df/d32/classG4HETCDeuteron.html", null ],
          [ "G4HETCHe3", "d0/d8b/classG4HETCHe3.html", null ],
          [ "G4HETCProton", "d2/dca/classG4HETCProton.html", null ],
          [ "G4HETCTriton", "d0/de3/classG4HETCTriton.html", null ]
        ] ],
        [ "G4HETCNeutron", "d3/df6/classG4HETCNeutron.html", null ]
      ] ],
      [ "G4PreCompoundFragment", "d1/de3/classG4PreCompoundFragment.html", [
        [ "G4PreCompoundIon", "df/df2/classG4PreCompoundIon.html", [
          [ "G4PreCompoundAlpha", "d3/d0b/classG4PreCompoundAlpha.html", null ],
          [ "G4PreCompoundDeuteron", "d1/deb/classG4PreCompoundDeuteron.html", null ],
          [ "G4PreCompoundHe3", "d0/d1d/classG4PreCompoundHe3.html", null ],
          [ "G4PreCompoundTriton", "df/d98/classG4PreCompoundTriton.html", null ]
        ] ],
        [ "G4PreCompoundNucleon", "dc/d4f/classG4PreCompoundNucleon.html", [
          [ "G4PreCompoundNeutron", "d4/d64/classG4PreCompoundNeutron.html", null ],
          [ "G4PreCompoundProton", "da/de5/classG4PreCompoundProton.html", null ]
        ] ]
      ] ]
    ] ],
    [ "G4VPreCompoundTransitions", "d6/d4d/classG4VPreCompoundTransitions.html", [
      [ "G4GNASHTransitions", "de/ddf/classG4GNASHTransitions.html", null ],
      [ "G4PreCompoundTransitions", "dd/d30/classG4PreCompoundTransitions.html", null ]
    ] ],
    [ "G4VPrimaryGenerator", "db/d9d/classG4VPrimaryGenerator.html", [
      [ "G4GeneralParticleSource", "d0/d8e/classG4GeneralParticleSource.html", null ],
      [ "G4HEPEvtInterface", "d6/d2a/classG4HEPEvtInterface.html", null ],
      [ "G4ParticleGun", "dc/ded/classG4ParticleGun.html", null ],
      [ "G4SingleParticleSource", "d9/d88/classG4SingleParticleSource.html", null ]
    ] ],
    [ "G4VPrimitiveScorer", "dc/d0d/classG4VPrimitiveScorer.html", [
      [ "G4PSCellCharge", "dc/d36/classG4PSCellCharge.html", [
        [ "G4PSCellCharge3D", "df/d6e/classG4PSCellCharge3D.html", null ]
      ] ],
      [ "G4PSNofCollision", "d8/d59/classG4PSNofCollision.html", [
        [ "G4PSNofCollision3D", "d6/dd8/classG4PSNofCollision3D.html", null ]
      ] ],
      [ "G4PSPopulation", "da/d6e/classG4PSPopulation.html", [
        [ "G4PSPopulation3D", "d1/df7/classG4PSPopulation3D.html", null ]
      ] ],
      [ "G4PSSphereSurfaceCurrent", "dc/de1/classG4PSSphereSurfaceCurrent.html", [
        [ "G4PSSphereSurfaceCurrent3D", "d0/dfb/classG4PSSphereSurfaceCurrent3D.html", null ]
      ] ],
      [ "G4PSSphereSurfaceFlux", "d7/d77/classG4PSSphereSurfaceFlux.html", [
        [ "G4PSSphereSurfaceFlux3D", "d7/da1/classG4PSSphereSurfaceFlux3D.html", null ]
      ] ],
      [ "G4PSStepChecker", "d1/d29/classG4PSStepChecker.html", [
        [ "G4PSStepChecker3D", "d8/d69/classG4PSStepChecker3D.html", null ]
      ] ],
      [ "G4PSTermination", "db/d95/classG4PSTermination.html", [
        [ "G4PSTermination3D", "d0/dee/classG4PSTermination3D.html", null ]
      ] ],
      [ "G4PSTrackLength", "df/d85/classG4PSTrackLength.html", [
        [ "G4PSTrackLength3D", "d6/dfe/classG4PSTrackLength3D.html", null ]
      ] ],
      [ "G4VPrimitivePlotter", "d9/d5e/classG4VPrimitivePlotter.html", [
        [ "G4PSCellFlux", "d4/dad/classG4PSCellFlux.html", [
          [ "G4PSCellFlux3D", "d3/d54/classG4PSCellFlux3D.html", [
            [ "G4PSCellFluxForCylinder3D", "d7/d59/classG4PSCellFluxForCylinder3D.html", null ]
          ] ]
        ] ],
        [ "G4PSCylinderSurfaceCurrent", "d1/d3a/classG4PSCylinderSurfaceCurrent.html", [
          [ "G4PSCylinderSurfaceCurrent3D", "dc/d93/classG4PSCylinderSurfaceCurrent3D.html", null ]
        ] ],
        [ "G4PSCylinderSurfaceFlux", "d3/df6/classG4PSCylinderSurfaceFlux.html", [
          [ "G4PSCylinderSurfaceFlux3D", "d8/d3c/classG4PSCylinderSurfaceFlux3D.html", null ]
        ] ],
        [ "G4PSDoseDeposit", "db/d0d/classG4PSDoseDeposit.html", [
          [ "G4PSDoseDeposit3D", "d2/de8/classG4PSDoseDeposit3D.html", [
            [ "G4PSDoseDepositForCylinder3D", "dc/d39/classG4PSDoseDepositForCylinder3D.html", null ]
          ] ]
        ] ],
        [ "G4PSEnergyDeposit", "de/d64/classG4PSEnergyDeposit.html", [
          [ "G4PSEnergyDeposit3D", "d4/dab/classG4PSEnergyDeposit3D.html", null ]
        ] ],
        [ "G4PSFlatSurfaceCurrent", "da/dc4/classG4PSFlatSurfaceCurrent.html", [
          [ "G4PSFlatSurfaceCurrent3D", "d1/db7/classG4PSFlatSurfaceCurrent3D.html", null ]
        ] ],
        [ "G4PSFlatSurfaceFlux", "de/d78/classG4PSFlatSurfaceFlux.html", [
          [ "G4PSFlatSurfaceFlux3D", "d6/d72/classG4PSFlatSurfaceFlux3D.html", null ]
        ] ],
        [ "G4PSMinKinEAtGeneration", "da/dde/classG4PSMinKinEAtGeneration.html", [
          [ "G4PSMinKinEAtGeneration3D", "dd/df1/classG4PSMinKinEAtGeneration3D.html", null ]
        ] ],
        [ "G4PSNofSecondary", "d3/df6/classG4PSNofSecondary.html", [
          [ "G4PSNofSecondary3D", "d7/d32/classG4PSNofSecondary3D.html", null ]
        ] ],
        [ "G4PSNofStep", "d6/d81/classG4PSNofStep.html", [
          [ "G4PSNofStep3D", "d3/dfb/classG4PSNofStep3D.html", null ]
        ] ],
        [ "G4PSPassageCellCurrent", "d9/d7c/classG4PSPassageCellCurrent.html", [
          [ "G4PSPassageCellCurrent3D", "d4/dc2/classG4PSPassageCellCurrent3D.html", null ]
        ] ],
        [ "G4PSPassageCellFlux", "dc/ddf/classG4PSPassageCellFlux.html", [
          [ "G4PSPassageCellFlux3D", "de/da2/classG4PSPassageCellFlux3D.html", [
            [ "G4PSPassageCellFluxForCylinder3D", "db/dd5/classG4PSPassageCellFluxForCylinder3D.html", null ]
          ] ]
        ] ],
        [ "G4PSPassageTrackLength", "da/d07/classG4PSPassageTrackLength.html", [
          [ "G4PSPassageTrackLength3D", "d0/d89/classG4PSPassageTrackLength3D.html", null ]
        ] ],
        [ "G4PSTrackCounter", "da/d30/classG4PSTrackCounter.html", [
          [ "G4PSTrackCounter3D", "d3/d01/classG4PSTrackCounter3D.html", null ]
        ] ],
        [ "G4PSVolumeFlux", "da/d2d/classG4PSVolumeFlux.html", [
          [ "G4PSVolumeFlux3D", "db/d7b/classG4PSVolumeFlux3D.html", null ]
        ] ]
      ] ]
    ] ],
    [ "G4VProcess", "d2/d84/classG4VProcess.html", [
      [ "G4AdjointProcessEquivalentToDirectProcess", "d8/db8/classG4AdjointProcessEquivalentToDirectProcess.html", null ],
      [ "G4BiasingProcessInterface", "d6/d72/classG4BiasingProcessInterface.html", null ],
      [ "G4Cerenkov", "db/d01/classG4Cerenkov.html", null ],
      [ "G4CoupledTransportation", "de/d27/classG4CoupledTransportation.html", null ],
      [ "G4FastSimulationManagerProcess", "d0/d62/classG4FastSimulationManagerProcess.html", null ],
      [ "G4ImportanceProcess", "da/da9/classG4ImportanceProcess.html", null ],
      [ "G4NoProcess", "d8/db9/classG4NoProcess.html", null ],
      [ "G4ParallelGeometriesLimiterProcess", "de/de9/classG4ParallelGeometriesLimiterProcess.html", null ],
      [ "G4ParallelWorldProcess", "de/dbf/classG4ParallelWorldProcess.html", null ],
      [ "G4ParallelWorldScoringProcess", "d7/d56/classG4ParallelWorldScoringProcess.html", null ],
      [ "G4ScoreSplittingProcess", "dd/d79/classG4ScoreSplittingProcess.html", null ],
      [ "G4SpecialCuts", "de/da1/classG4SpecialCuts.html", [
        [ "G4MaxTimeCuts", "d3/d91/classG4MaxTimeCuts.html", null ],
        [ "G4MinEkineCuts", "da/d46/classG4MinEkineCuts.html", null ]
      ] ],
      [ "G4StepLimiter", "d2/d9f/classG4StepLimiter.html", null ],
      [ "G4Transportation", "d6/ddc/classG4Transportation.html", null ],
      [ "G4UserSpecialCuts", "d3/dd8/classG4UserSpecialCuts.html", null ],
      [ "G4VContinuousDiscreteProcess", "d5/d21/classG4VContinuousDiscreteProcess.html", [
        [ "G4AdjointForcedInteractionForGamma", "d3/d59/classG4AdjointForcedInteractionForGamma.html", null ],
        [ "G4VEnergyLossProcess", "da/d80/classG4VEnergyLossProcess.html", [
          [ "G4MuBremsstrahlung", "d9/d34/classG4MuBremsstrahlung.html", [
            [ "G4hBremsstrahlung", "d7/d4c/classG4hBremsstrahlung.html", null ]
          ] ],
          [ "G4MuIonisation", "de/d7f/classG4MuIonisation.html", null ],
          [ "G4MuPairProduction", "d5/d9f/classG4MuPairProduction.html", [
            [ "G4hPairProduction", "d6/d23/classG4hPairProduction.html", null ]
          ] ],
          [ "G4PolarizedIonisation", "d0/db7/classG4PolarizedIonisation.html", null ],
          [ "G4eBremsstrahlung", "d6/dfe/classG4eBremsstrahlung.html", [
            [ "G4PolarizedBremsstrahlung", "de/dd7/classG4PolarizedBremsstrahlung.html", null ]
          ] ],
          [ "G4eIonisation", "da/d09/classG4eIonisation.html", null ],
          [ "G4ePairProduction", "d8/d5b/classG4ePairProduction.html", null ],
          [ "G4hIonisation", "da/dc3/classG4hIonisation.html", null ],
          [ "G4hhIonisation", "d2/dce/classG4hhIonisation.html", null ],
          [ "G4ionIonisation", "d7/d7a/classG4ionIonisation.html", null ],
          [ "G4mplIonisation", "df/d08/classG4mplIonisation.html", null ]
        ] ],
        [ "G4VMultipleScattering", "d9/d2a/classG4VMultipleScattering.html", [
          [ "G4AdjointhMultipleScattering", "dd/df4/classG4AdjointhMultipleScattering.html", null ],
          [ "G4MuMultipleScattering", "de/d49/classG4MuMultipleScattering.html", null ],
          [ "G4eAdjointMultipleScattering", "d0/dcb/classG4eAdjointMultipleScattering.html", null ],
          [ "G4eMultipleScattering", "d4/d3e/classG4eMultipleScattering.html", null ],
          [ "G4hMultipleScattering", "dd/dbb/classG4hMultipleScattering.html", null ]
        ] ],
        [ "G4hRDEnergyLoss", "da/d59/classG4hRDEnergyLoss.html", [
          [ "G4hImpactIonisation", "d5/de2/classG4hImpactIonisation.html", null ]
        ] ]
      ] ],
      [ "G4VContinuousProcess", "dc/d6b/classG4VContinuousProcess.html", [
        [ "G4AdjointAlongStepWeightCorrection", "db/dca/classG4AdjointAlongStepWeightCorrection.html", null ],
        [ "G4ContinuousGainOfEnergy", "df/d03/classG4ContinuousGainOfEnergy.html", null ],
        [ "G4ErrorEnergyLoss", "d9/d11/classG4ErrorEnergyLoss.html", null ]
      ] ],
      [ "G4VDiscreteProcess", "da/dea/classG4VDiscreteProcess.html", [
        [ "G4AnnihiToMuPair", "d6/dc6/classG4AnnihiToMuPair.html", null ],
        [ "G4Channeling", "d1/d4c/classG4Channeling.html", null ],
        [ "G4ErrorTrackLengthTarget", "d6/d41/classG4ErrorTrackLengthTarget.html", null ],
        [ "G4GammaConversionToMuons", "d4/d14/classG4GammaConversionToMuons.html", null ],
        [ "G4HadronicProcess", "de/dc6/classG4HadronicProcess.html", [
          [ "G4ChargeExchangeProcess", "d9/df3/classG4ChargeExchangeProcess.html", null ],
          [ "G4ElNeutrinoNucleusProcess", "d3/d17/classG4ElNeutrinoNucleusProcess.html", null ],
          [ "G4HadronElasticProcess", "d4/df9/classG4HadronElasticProcess.html", null ],
          [ "G4HadronInelasticProcess", "d8/d13/classG4HadronInelasticProcess.html", [
            [ "G4ElectronNuclearProcess", "db/d3b/classG4ElectronNuclearProcess.html", null ],
            [ "G4PositronNuclearProcess", "d6/d97/classG4PositronNuclearProcess.html", null ]
          ] ],
          [ "G4HadronStoppingProcess", "d1/dfb/classG4HadronStoppingProcess.html", [
            [ "G4HadronicAbsorptionBertini", "d8/d1a/classG4HadronicAbsorptionBertini.html", null ],
            [ "G4HadronicAbsorptionFritiof", "d0/d6c/classG4HadronicAbsorptionFritiof.html", null ],
            [ "G4HadronicAbsorptionFritiofWithBinaryCascade", "dd/da7/classG4HadronicAbsorptionFritiofWithBinaryCascade.html", null ],
            [ "G4MuonMinusCapture", "d3/de8/classG4MuonMinusCapture.html", null ]
          ] ],
          [ "G4MuNeutrinoNucleusProcess", "d1/d29/classG4MuNeutrinoNucleusProcess.html", null ],
          [ "G4MuonNuclearProcess", "da/dd1/classG4MuonNuclearProcess.html", null ],
          [ "G4NeutrinoElectronProcess", "d2/dd8/classG4NeutrinoElectronProcess.html", null ],
          [ "G4NeutronCaptureProcess", "d0/dd5/classG4NeutronCaptureProcess.html", null ],
          [ "G4NeutronFissionProcess", "d3/d78/classG4NeutronFissionProcess.html", null ]
        ] ],
        [ "G4LowECapture", "de/dcf/classG4LowECapture.html", null ],
        [ "G4MicroElecSurface", "d7/dc6/classG4MicroElecSurface.html", null ],
        [ "G4NeutronKiller", "d6/d3e/classG4NeutronKiller.html", null ],
        [ "G4OpAbsorption", "dc/df8/classG4OpAbsorption.html", null ],
        [ "G4OpBoundaryProcess", "dc/d81/classG4OpBoundaryProcess.html", null ],
        [ "G4OpMieHG", "dd/ddb/classG4OpMieHG.html", null ],
        [ "G4OpRayleigh", "d3/dff/classG4OpRayleigh.html", null ],
        [ "G4OpWLS", "d4/dee/classG4OpWLS.html", null ],
        [ "G4OpWLS2", "d9/d3f/classG4OpWLS2.html", null ],
        [ "G4SynchrotronRadiation", "d3/d05/classG4SynchrotronRadiation.html", null ],
        [ "G4SynchrotronRadiationInMat", "d9/d5c/classG4SynchrotronRadiationInMat.html", null ],
        [ "G4TransitionRadiation", "dc/d42/classG4TransitionRadiation.html", [
          [ "G4ForwardXrayTR", "dc/d02/classG4ForwardXrayTR.html", null ]
        ] ],
        [ "G4UCNAbsorption", "de/dc6/classG4UCNAbsorption.html", null ],
        [ "G4UCNBoundaryProcess", "db/de3/classG4UCNBoundaryProcess.html", null ],
        [ "G4UCNLoss", "d0/d76/classG4UCNLoss.html", null ],
        [ "G4UCNMultiScattering", "d8/dcf/classG4UCNMultiScattering.html", null ],
        [ "G4UnknownDecay", "d5/d3b/classG4UnknownDecay.html", null ],
        [ "G4VAdjointReverseReaction", "da/d14/classG4VAdjointReverseReaction.html", [
          [ "G4InversePEEffect", "dc/d4d/classG4InversePEEffect.html", null ],
          [ "G4IonInverseIonisation", "d5/d2f/classG4IonInverseIonisation.html", null ],
          [ "G4eInverseBremsstrahlung", "d3/df8/classG4eInverseBremsstrahlung.html", null ],
          [ "G4eInverseCompton", "d7/d61/classG4eInverseCompton.html", null ],
          [ "G4eInverseIonisation", "d0/d87/classG4eInverseIonisation.html", null ],
          [ "G4hInverseIonisation", "d1/d67/classG4hInverseIonisation.html", null ]
        ] ],
        [ "G4VEmProcess", "d0/dfb/classG4VEmProcess.html", [
          [ "G4ComptonScattering", "d4/d82/classG4ComptonScattering.html", null ],
          [ "G4CoulombScattering", "d5/d0d/classG4CoulombScattering.html", null ],
          [ "G4DNAAttachment", "da/dc8/classG4DNAAttachment.html", null ],
          [ "G4DNAChargeDecrease", "db/ded/classG4DNAChargeDecrease.html", null ],
          [ "G4DNAChargeIncrease", "d0/d5c/classG4DNAChargeIncrease.html", null ],
          [ "G4DNADissociation", "de/d75/classG4DNADissociation.html", null ],
          [ "G4DNAElastic", "d4/d61/classG4DNAElastic.html", null ],
          [ "G4DNAElectronSolvation", "d0/db1/classG4DNAElectronSolvation.html", null ],
          [ "G4DNAExcitation", "d5/dcf/classG4DNAExcitation.html", null ],
          [ "G4DNAIonisation", "d5/d80/classG4DNAIonisation.html", null ],
          [ "G4DNAPlasmonExcitation", "dc/d11/classG4DNAPlasmonExcitation.html", null ],
          [ "G4DNAPositronium", "d0/d8b/classG4DNAPositronium.html", null ],
          [ "G4DNARotExcitation", "d9/d42/classG4DNARotExcitation.html", null ],
          [ "G4DNAVibExcitation", "df/dff/classG4DNAVibExcitation.html", null ],
          [ "G4GammaConversion", "d6/d03/classG4GammaConversion.html", null ],
          [ "G4GammaGeneralProcess", "dd/d59/classG4GammaGeneralProcess.html", null ],
          [ "G4JAEAElasticScattering", "dc/d5d/classG4JAEAElasticScattering.html", null ],
          [ "G4MicroElecElastic", "d7/ddf/classG4MicroElecElastic.html", null ],
          [ "G4MicroElecInelastic", "d9/dd5/classG4MicroElecInelastic.html", null ],
          [ "G4MicroElecLOPhononScattering", "dd/df7/classG4MicroElecLOPhononScattering.html", null ],
          [ "G4NuclearStopping", "d8/d35/classG4NuclearStopping.html", null ],
          [ "G4PhotoElectricEffect", "d4/d13/classG4PhotoElectricEffect.html", null ],
          [ "G4PolarizedCompton", "d1/d92/classG4PolarizedCompton.html", null ],
          [ "G4PolarizedGammaConversion", "df/dcd/classG4PolarizedGammaConversion.html", null ],
          [ "G4PolarizedPhotoElectric", "d1/d1e/classG4PolarizedPhotoElectric.html", null ],
          [ "G4RayleighScattering", "d5/de6/classG4RayleighScattering.html", null ],
          [ "G4eeToHadrons", "db/dd7/classG4eeToHadrons.html", null ],
          [ "G4eplusAnnihilation", "dc/d88/classG4eplusAnnihilation.html", [
            [ "G4PolarizedAnnihilation", "d6/d5b/classG4PolarizedAnnihilation.html", null ]
          ] ]
        ] ],
        [ "G4VErrorLimitProcess", "d0/dd7/classG4VErrorLimitProcess.html", [
          [ "G4ErrorMagFieldLimitProcess", "d1/d20/classG4ErrorMagFieldLimitProcess.html", null ],
          [ "G4ErrorStepLengthLimitProcess", "df/d0b/classG4ErrorStepLengthLimitProcess.html", null ]
        ] ],
        [ "G4VPhononProcess", "d7/d9e/classG4VPhononProcess.html", [
          [ "G4PhononDownconversion", "df/dda/classG4PhononDownconversion.html", null ],
          [ "G4PhononReflection", "db/d78/classG4PhononReflection.html", null ],
          [ "G4PhononScattering", "d0/d18/classG4PhononScattering.html", null ]
        ] ],
        [ "G4VTransitionRadiation", "d7/d70/classG4VTransitionRadiation.html", null ],
        [ "G4VXTRenergyLoss", "da/d62/classG4VXTRenergyLoss.html", [
          [ "G4GammaXTRadiator", "dc/d50/classG4GammaXTRadiator.html", null ],
          [ "G4GaussXTRadiator", "d3/d6f/classG4GaussXTRadiator.html", null ],
          [ "G4RegularXTRadiator", "dc/d69/classG4RegularXTRadiator.html", null ],
          [ "G4StrawTubeXTRadiator", "d2/d4e/classG4StrawTubeXTRadiator.html", null ],
          [ "G4TransparentRegXTRadiator", "d0/d32/classG4TransparentRegXTRadiator.html", null ],
          [ "G4XTRGammaRadModel", "dc/d9e/classG4XTRGammaRadModel.html", null ],
          [ "G4XTRRegularRadModel", "dd/d21/classG4XTRRegularRadModel.html", null ],
          [ "G4XTRTransparentRegRadModel", "db/da3/classG4XTRTransparentRegRadModel.html", null ]
        ] ]
      ] ],
      [ "G4VITProcess", "d9/d54/classG4VITProcess.html", [
        [ "G4DNAScavengerProcess", "de/d25/classG4DNAScavengerProcess.html", null ],
        [ "G4DNASecondOrderReaction", "df/da7/classG4DNASecondOrderReaction.html", null ],
        [ "G4ITTransportation", "df/d6e/classG4ITTransportation.html", [
          [ "G4DNABrownianTransportation", "db/dca/classG4DNABrownianTransportation.html", null ]
        ] ],
        [ "G4VITDiscreteProcess", "de/d71/classG4VITDiscreteProcess.html", null ],
        [ "G4VITRestDiscreteProcess", "d6/db9/classG4VITRestDiscreteProcess.html", [
          [ "G4DNAElectronHoleRecombination", "db/dfe/classG4DNAElectronHoleRecombination.html", null ],
          [ "G4DNAMolecularDissociation", "df/d00/classG4DNAMolecularDissociation.html", null ]
        ] ],
        [ "G4VITRestProcess", "d9/d81/classG4VITRestProcess.html", null ]
      ] ],
      [ "G4VRestContinuousDiscreteProcess", "d6/dfc/classG4VRestContinuousDiscreteProcess.html", null ],
      [ "G4VRestContinuousProcess", "d5/d01/classG4VRestContinuousProcess.html", null ],
      [ "G4VRestDiscreteProcess", "d8/d93/classG4VRestDiscreteProcess.html", [
        [ "G4Decay", "dc/deb/classG4Decay.html", [
          [ "G4DecayWithSpin", "dc/d66/classG4DecayWithSpin.html", null ],
          [ "G4PionDecayMakeSpin", "df/d93/classG4PionDecayMakeSpin.html", null ]
        ] ],
        [ "G4MuonicAtomDecay", "d3/d52/classG4MuonicAtomDecay.html", null ],
        [ "G4RadioactiveDecay", "df/d5e/classG4RadioactiveDecay.html", [
          [ "G4Radioactivation", "d0/dfa/classG4Radioactivation.html", null ]
        ] ],
        [ "G4Scintillation", "d5/d96/classG4Scintillation.html", null ]
      ] ],
      [ "G4VRestProcess", "db/d81/classG4VRestProcess.html", [
        [ "G4MuonMinusAtomicCapture", "de/d74/classG4MuonMinusAtomicCapture.html", null ]
      ] ],
      [ "G4WeightCutOffProcess", "d7/d9d/classG4WeightCutOffProcess.html", null ],
      [ "G4WeightWindowProcess", "d6/d2e/classG4WeightWindowProcess.html", null ],
      [ "G4WrapperProcess", "d7/d66/classG4WrapperProcess.html", null ]
    ] ],
    [ "G4VProcessPlacer", "d5/d11/classG4VProcessPlacer.html", [
      [ "G4ProcessPlacer", "d9/dc5/classG4ProcessPlacer.html", null ]
    ] ],
    [ "G4VPVDivisionFactory", "d2/d33/classG4VPVDivisionFactory.html", [
      [ "G4PVDivisionFactory", "db/d08/classG4PVDivisionFactory.html", null ]
    ] ],
    [ "G4VPVParameterisation", "de/dcc/classG4VPVParameterisation.html", [
      [ "G4GDMLParameterisation", "dc/d7a/classG4GDMLParameterisation.html", null ],
      [ "G4PhantomParameterisation", "d6/da1/classG4PhantomParameterisation.html", [
        [ "G4PartialPhantomParameterisation", "d8/d74/classG4PartialPhantomParameterisation.html", null ]
      ] ],
      [ "G4VDivisionParameterisation", "d5/ddc/classG4VDivisionParameterisation.html", [
        [ "G4VParameterisationBox", "d7/deb/classG4VParameterisationBox.html", [
          [ "G4ParameterisationBoxX", "de/d34/classG4ParameterisationBoxX.html", null ],
          [ "G4ParameterisationBoxY", "df/d37/classG4ParameterisationBoxY.html", null ],
          [ "G4ParameterisationBoxZ", "d9/d1a/classG4ParameterisationBoxZ.html", null ]
        ] ],
        [ "G4VParameterisationCons", "dc/daa/classG4VParameterisationCons.html", [
          [ "G4ParameterisationConsPhi", "d3/dce/classG4ParameterisationConsPhi.html", null ],
          [ "G4ParameterisationConsRho", "d5/d2c/classG4ParameterisationConsRho.html", null ],
          [ "G4ParameterisationConsZ", "d0/d8c/classG4ParameterisationConsZ.html", null ]
        ] ],
        [ "G4VParameterisationPara", "df/d8e/classG4VParameterisationPara.html", [
          [ "G4ParameterisationParaX", "d4/d83/classG4ParameterisationParaX.html", null ],
          [ "G4ParameterisationParaY", "dc/d89/classG4ParameterisationParaY.html", null ],
          [ "G4ParameterisationParaZ", "d5/db4/classG4ParameterisationParaZ.html", null ]
        ] ],
        [ "G4VParameterisationPolycone", "d6/d58/classG4VParameterisationPolycone.html", [
          [ "G4ParameterisationPolyconePhi", "d8/df6/classG4ParameterisationPolyconePhi.html", null ],
          [ "G4ParameterisationPolyconeRho", "d8/de2/classG4ParameterisationPolyconeRho.html", null ],
          [ "G4ParameterisationPolyconeZ", "d5/d61/classG4ParameterisationPolyconeZ.html", null ]
        ] ],
        [ "G4VParameterisationPolyhedra", "df/d57/classG4VParameterisationPolyhedra.html", [
          [ "G4ParameterisationPolyhedraPhi", "d7/dda/classG4ParameterisationPolyhedraPhi.html", null ],
          [ "G4ParameterisationPolyhedraRho", "df/d4d/classG4ParameterisationPolyhedraRho.html", null ],
          [ "G4ParameterisationPolyhedraZ", "de/d28/classG4ParameterisationPolyhedraZ.html", null ]
        ] ],
        [ "G4VParameterisationTrd", "d0/d63/classG4VParameterisationTrd.html", [
          [ "G4ParameterisationTrdX", "d3/d35/classG4ParameterisationTrdX.html", null ],
          [ "G4ParameterisationTrdY", "d3/d4d/classG4ParameterisationTrdY.html", null ],
          [ "G4ParameterisationTrdZ", "d5/d86/classG4ParameterisationTrdZ.html", null ]
        ] ],
        [ "G4VParameterisationTubs", "d0/da1/classG4VParameterisationTubs.html", [
          [ "G4ParameterisationTubsPhi", "d6/dd3/classG4ParameterisationTubsPhi.html", null ],
          [ "G4ParameterisationTubsRho", "db/d15/classG4ParameterisationTubsRho.html", null ],
          [ "G4ParameterisationTubsZ", "d7/d39/classG4ParameterisationTubsZ.html", null ]
        ] ]
      ] ],
      [ "G4VNestedParameterisation", "d3/de0/classG4VNestedParameterisation.html", null ],
      [ "G4tgbPlaceParameterisation", "d2/d93/classG4tgbPlaceParameterisation.html", [
        [ "G4tgbPlaceParamCircle", "da/d0a/classG4tgbPlaceParamCircle.html", null ],
        [ "G4tgbPlaceParamLinear", "da/d8d/classG4tgbPlaceParamLinear.html", null ],
        [ "G4tgbPlaceParamSquare", "dd/dfd/classG4tgbPlaceParamSquare.html", null ]
      ] ]
    ] ],
    [ "G4VRangeToEnergyConverter", "d2/d33/classG4VRangeToEnergyConverter.html", [
      [ "G4RToEConvForElectron", "df/dcc/classG4RToEConvForElectron.html", null ],
      [ "G4RToEConvForGamma", "db/de7/classG4RToEConvForGamma.html", null ],
      [ "G4RToEConvForPositron", "d8/ddd/classG4RToEConvForPositron.html", null ],
      [ "G4RToEConvForProton", "d3/d5e/classG4RToEConvForProton.html", null ]
    ] ],
    [ "G4VReactionType", "d0/dd1/classG4VReactionType.html", [
      [ "G4DNAPartiallyDiffusionControlled", "d0/dd4/classG4DNAPartiallyDiffusionControlled.html", null ],
      [ "G4DNATotallyDiffusionControlled", "de/d70/classG4DNATotallyDiffusionControlled.html", null ]
    ] ],
    [ "G4VReactionTypeManager", "d1/d75/classG4VReactionTypeManager.html", [
      [ "G4DNAReactionTypeManager", "d2/df1/classG4DNAReactionTypeManager.html", null ]
    ] ],
    [ "G4VReadOutGeometry", "d5/d11/classG4VReadOutGeometry.html", null ],
    [ "G4VRTScanner", "d0/d95/classG4VRTScanner.html", [
      [ "G4RTSimpleScanner", "d0/df8/classG4RTSimpleScanner.html", null ],
      [ "G4RTXScanner", "d6/d95/classG4RTXScanner.html", null ]
    ] ],
    [ "G4VSampler", "d3/dc0/classG4VSampler.html", [
      [ "G4GeometrySampler", "dc/db8/classG4GeometrySampler.html", null ]
    ] ],
    [ "G4VSamplerConfigurator", "dc/d9b/classG4VSamplerConfigurator.html", [
      [ "G4ImportanceConfigurator", "df/d07/classG4ImportanceConfigurator.html", null ],
      [ "G4WeightCutOffConfigurator", "d1/dcb/classG4WeightCutOffConfigurator.html", null ],
      [ "G4WeightWindowConfigurator", "df/d2d/classG4WeightWindowConfigurator.html", null ]
    ] ],
    [ "G4VScatterer", "da/dd6/classG4VScatterer.html", [
      [ "G4Scatterer", "d4/d15/classG4Scatterer.html", null ]
    ] ],
    [ "G4VScavengerMaterial", "d4/d3e/classG4VScavengerMaterial.html", [
      [ "G4DNAScavengerMaterial", "dc/db0/classG4DNAScavengerMaterial.html", null ]
    ] ],
    [ "G4VScheduler", "d0/ddb/classG4VScheduler.html", [
      [ "G4Scheduler", "de/dcb/classG4Scheduler.html", null ]
    ] ],
    [ "G4VScoreColorMap", "df/dea/classG4VScoreColorMap.html", [
      [ "G4DefaultLinearColorMap", "d6/d38/classG4DefaultLinearColorMap.html", null ],
      [ "G4ScoreLogColorMap", "d4/dee/classG4ScoreLogColorMap.html", null ]
    ] ],
    [ "G4VScoreHistFiller", "d3/dad/classG4VScoreHistFiller.html", [
      [ "G4TScoreHistFiller< T >", "dd/d5f/classG4TScoreHistFiller.html", null ]
    ] ],
    [ "G4VScoreNtupleWriter", "df/d21/classG4VScoreNtupleWriter.html", [
      [ "G4TScoreNtupleWriter< T >", "d3/d10/classG4TScoreNtupleWriter.html", null ]
    ] ],
    [ "G4VScoreWriter", "d2/d96/classG4VScoreWriter.html", null ],
    [ "G4VScoringMesh", "d0/d53/classG4VScoringMesh.html", [
      [ "G4ScoringBox", "dc/da1/classG4ScoringBox.html", null ],
      [ "G4ScoringCylinder", "d2/d9c/classG4ScoringCylinder.html", null ],
      [ "G4ScoringProbe", "d2/d50/classG4ScoringProbe.html", null ],
      [ "G4ScoringRealWorld", "dc/d70/classG4ScoringRealWorld.html", null ]
    ] ],
    [ "G4VSDFilter", "d5/d2d/classG4VSDFilter.html", [
      [ "G4SDChargedFilter", "d1/d8c/classG4SDChargedFilter.html", null ],
      [ "G4SDKineticEnergyFilter", "da/d22/classG4SDKineticEnergyFilter.html", null ],
      [ "G4SDNeutralFilter", "d5/d59/classG4SDNeutralFilter.html", null ],
      [ "G4SDParticleFilter", "d2/dfd/classG4SDParticleFilter.html", null ],
      [ "G4SDParticleWithEnergyFilter", "d5/d27/classG4SDParticleWithEnergyFilter.html", null ]
    ] ],
    [ "G4VSensitiveDetector", "df/dbc/classG4VSensitiveDetector.html", [
      [ "G4MultiFunctionalDetector", "da/d7e/classG4MultiFunctionalDetector.html", null ],
      [ "G4MultiSensitiveDetector", "df/de6/classG4MultiSensitiveDetector.html", null ]
    ] ],
    [ "G4VSolid", "da/d34/classG4VSolid.html", [
      [ "G4BooleanSolid", "d2/d25/classG4BooleanSolid.html", [
        [ "G4IntersectionSolid", "dd/dcd/classG4IntersectionSolid.html", null ],
        [ "G4SubtractionSolid", "d1/da1/classG4SubtractionSolid.html", null ],
        [ "G4UnionSolid", "de/d18/classG4UnionSolid.html", null ]
      ] ],
      [ "G4CSGSolid", "df/d71/classG4CSGSolid.html", [
        [ "G4Box", "d0/d5a/classG4Box.html", null ],
        [ "G4Cons", "de/d0d/classG4Cons.html", null ],
        [ "G4CutTubs", "d7/d40/classG4CutTubs.html", null ],
        [ "G4Orb", "dd/d9a/classG4Orb.html", null ],
        [ "G4Para", "d3/d6a/classG4Para.html", null ],
        [ "G4Sphere", "da/dd6/classG4Sphere.html", null ],
        [ "G4Torus", "da/d0a/classG4Torus.html", null ],
        [ "G4Trap", "d7/df4/classG4Trap.html", null ],
        [ "G4Trd", "d0/d50/classG4Trd.html", null ],
        [ "G4Tubs", "d0/d28/classG4Tubs.html", null ]
      ] ],
      [ "G4DisplacedSolid", "d5/dfb/classG4DisplacedSolid.html", null ],
      [ "G4Ellipsoid", "d6/d81/classG4Ellipsoid.html", null ],
      [ "G4EllipticalCone", "df/d2e/classG4EllipticalCone.html", null ],
      [ "G4EllipticalTube", "d8/d8b/classG4EllipticalTube.html", null ],
      [ "G4GenericTrap", "d6/dd3/classG4GenericTrap.html", null ],
      [ "G4Hype", "d0/d1a/classG4Hype.html", null ],
      [ "G4MultiUnion", "d9/df1/classG4MultiUnion.html", null ],
      [ "G4Paraboloid", "d2/d69/classG4Paraboloid.html", null ],
      [ "G4ReflectedSolid", "d8/d32/classG4ReflectedSolid.html", null ],
      [ "G4ScaledSolid", "d0/dc1/classG4ScaledSolid.html", null ],
      [ "G4TessellatedSolid", "da/db4/classG4TessellatedSolid.html", [
        [ "G4ExtrudedSolid", "da/d21/classG4ExtrudedSolid.html", null ]
      ] ],
      [ "G4Tet", "de/d99/classG4Tet.html", null ],
      [ "G4TwistedTubs", "db/db5/classG4TwistedTubs.html", null ],
      [ "G4VCSGfaceted", "d7/de5/classG4VCSGfaceted.html", [
        [ "G4GenericPolycone", "dc/d33/classG4GenericPolycone.html", null ],
        [ "G4Polycone", "d9/de5/classG4Polycone.html", null ],
        [ "G4Polyhedra", "d2/d5a/classG4Polyhedra.html", null ]
      ] ],
      [ "G4VTwistedFaceted", "d4/d92/classG4VTwistedFaceted.html", [
        [ "G4TwistedBox", "d3/d7d/classG4TwistedBox.html", null ],
        [ "G4TwistedTrap", "d3/da8/classG4TwistedTrap.html", null ],
        [ "G4TwistedTrd", "d4/d58/classG4TwistedTrd.html", null ]
      ] ]
    ] ],
    [ "G4VSplitableHadron", "de/d91/classG4VSplitableHadron.html", [
      [ "G4DiffractiveSplitableHadron", "d7/d0a/classG4DiffractiveSplitableHadron.html", null ],
      [ "G4QGSMSplitableHadron", "d0/df3/classG4QGSMSplitableHadron.html", null ]
    ] ],
    [ "G4VStateDependent", "d4/d54/classG4VStateDependent.html", [
      [ "G4BiasingOperatorStateNotifier", "df/d50/classG4BiasingOperatorStateNotifier.html", null ],
      [ "G4DNAChemistryManager", "dc/d37/classG4DNAChemistryManager.html", null ],
      [ "G4DNAMolecularMaterial", "d3/db5/classG4DNAMolecularMaterial.html", null ],
      [ "G4Scheduler", "de/dcb/classG4Scheduler.html", null ],
      [ "G4UImanager", "d8/def/classG4UImanager.html", null ],
      [ "G4VisStateDependent", "d4/d4b/classG4VisStateDependent.html", null ],
      [ "HookEventProcState", "df/dd6/classHookEventProcState.html", null ],
      [ "HookEventProcState", "df/dd6/classHookEventProcState.html", null ]
    ] ],
    [ "G4VStatMFEnsemble", "d6/d90/classG4VStatMFEnsemble.html", [
      [ "G4StatMFMacroCanonical", "d9/df6/classG4StatMFMacroCanonical.html", null ],
      [ "G4StatMFMicroCanonical", "d3/d8e/classG4StatMFMicroCanonical.html", null ]
    ] ],
    [ "G4VStatMFMacroCluster", "d2/d2e/classG4VStatMFMacroCluster.html", [
      [ "G4StatMFMacroBiNucleon", "df/db5/classG4StatMFMacroBiNucleon.html", null ],
      [ "G4StatMFMacroMultiNucleon", "dd/d09/classG4StatMFMacroMultiNucleon.html", null ],
      [ "G4StatMFMacroNucleon", "dc/d1d/classG4StatMFMacroNucleon.html", null ],
      [ "G4StatMFMacroTetraNucleon", "d3/dbe/classG4StatMFMacroTetraNucleon.html", null ],
      [ "G4StatMFMacroTriNucleon", "d9/d12/classG4StatMFMacroTriNucleon.html", null ]
    ] ],
    [ "G4VSteppingVerbose", "da/d01/classG4VSteppingVerbose.html", [
      [ "G4SteppingVerbose", "da/d58/classG4SteppingVerbose.html", [
        [ "G4SteppingVerboseWithUnits", "d4/d58/classG4SteppingVerboseWithUnits.html", null ]
      ] ]
    ] ],
    [ "G4VSubCutProducer", "db/da3/classG4VSubCutProducer.html", null ],
    [ "G4VTHnFileManager< HT >", "dd/dfd/classG4VTHnFileManager.html", [
      [ "G4CsvHnFileManager< HT >", "dd/d49/classG4CsvHnFileManager.html", null ],
      [ "G4Hdf5HnFileManager< HT >", "d6/d32/classG4Hdf5HnFileManager.html", null ],
      [ "G4RootHnFileManager< HT >", "d4/d5a/classG4RootHnFileManager.html", null ],
      [ "G4XmlHnFileManager< HT >", "d8/d55/classG4XmlHnFileManager.html", null ]
    ] ],
    [ "G4VTHnRFileManager< HT >", "d0/d7a/classG4VTHnRFileManager.html", [
      [ "G4CsvHnRFileManager< HT >", "dc/dcd/classG4CsvHnRFileManager.html", null ],
      [ "G4Hdf5HnRFileManager< HT >", "dc/d46/classG4Hdf5HnRFileManager.html", null ],
      [ "G4RootHnRFileManager< HT >", "da/d9e/classG4RootHnRFileManager.html", null ],
      [ "G4XmlHnRFileManager< HT >", "dd/d28/classG4XmlHnRFileManager.html", null ]
    ] ],
    [ "G4VTouchable", "d9/d81/classG4VTouchable.html", [
      [ "G4GMocrenTouchable", "d5/da8/classG4GMocrenTouchable.html", null ],
      [ "G4GRSSolid", "d2/d67/classG4GRSSolid.html", null ],
      [ "G4GRSVolume", "db/db5/classG4GRSVolume.html", null ],
      [ "G4PhysicalVolumeModel::G4PhysicalVolumeModelTouchable", "d2/d98/classG4PhysicalVolumeModel_1_1G4PhysicalVolumeModelTouchable.html", null ],
      [ "G4TouchableHistory", "d2/dc3/classG4TouchableHistory.html", null ]
    ] ],
    [ "G4VTrackingManager", "de/dac/classG4VTrackingManager.html", null ],
    [ "G4VTrackState", "da/dce/classG4VTrackState.html", [
      [ "G4TrackStateBase< G4ITNavigator >", "de/d59/classG4TrackStateBase.html", [
        [ "G4TrackState< G4ITNavigator >", "d5/dd5/classG4TrackState.html", [
          [ "G4TrackState< G4ITMultiNavigator >", "d2/db0/classG4TrackState_3_01G4ITMultiNavigator_01_4.html", null ]
        ] ]
      ] ],
      [ "G4TrackStateBase< G4ITPathFinder >", "de/d59/classG4TrackStateBase.html", [
        [ "G4TrackState< G4ITPathFinder >", "db/dd6/classG4TrackState_3_01G4ITPathFinder_01_4.html", null ]
      ] ],
      [ "G4TrackStateBase< T >", "de/d59/classG4TrackStateBase.html", [
        [ "G4TrackState< T >", "d5/dd5/classG4TrackState.html", null ]
      ] ]
    ] ],
    [ "G4VTrackStateDependent", "df/d99/classG4VTrackStateDependent.html", [
      [ "G4TrackStateDependent< G4ITMultiNavigator >", "d4/d3c/classG4TrackStateDependent.html", [
        [ "G4ITMultiNavigator", "db/d5f/classG4ITMultiNavigator.html", null ]
      ] ],
      [ "G4TrackStateDependent< G4ITPathFinder >", "d4/d3c/classG4TrackStateDependent.html", [
        [ "G4ITPathFinder", "dd/d11/classG4ITPathFinder.html", null ]
      ] ],
      [ "G4TrackStateDependent< G4ITSafetyHelper >", "d4/d3c/classG4TrackStateDependent.html", [
        [ "G4ITSafetyHelper", "db/d01/classG4ITSafetyHelper.html", null ]
      ] ],
      [ "G4TrackStateDependent< T >", "d4/d3c/classG4TrackStateDependent.html", null ]
    ] ],
    [ "G4VTrackStateID", "de/db0/classG4VTrackStateID.html", [
      [ "G4TrackStateID< T >", "d4/d68/classG4TrackStateID.html", null ]
    ] ],
    [ "G4VTrackTerminator", "dc/d2b/classG4VTrackTerminator.html", [
      [ "G4ImportanceProcess", "da/da9/classG4ImportanceProcess.html", null ],
      [ "G4WeightWindowProcess", "d6/d2e/classG4WeightWindowProcess.html", null ]
    ] ],
    [ "G4VTrajectory", "d6/d48/classG4VTrajectory.html", [
      [ "G4RayTrajectory", "d6/d2d/classG4RayTrajectory.html", null ],
      [ "G4SmoothTrajectory", "d4/dce/classG4SmoothTrajectory.html", null ],
      [ "G4Trajectory", "da/d9a/classG4Trajectory.html", [
        [ "G4RichTrajectory", "dd/d1b/classG4RichTrajectory.html", null ]
      ] ]
    ] ],
    [ "G4VTrajectoryModel", "d0/d42/classG4VTrajectoryModel.html", [
      [ "G4TrajectoryDrawByAttribute", "d8/d6c/classG4TrajectoryDrawByAttribute.html", null ],
      [ "G4TrajectoryDrawByCharge", "db/dc4/classG4TrajectoryDrawByCharge.html", null ],
      [ "G4TrajectoryDrawByEncounteredVolume", "d5/d7b/classG4TrajectoryDrawByEncounteredVolume.html", null ],
      [ "G4TrajectoryDrawByOriginVolume", "d6/da9/classG4TrajectoryDrawByOriginVolume.html", null ],
      [ "G4TrajectoryDrawByParticleID", "d1/dc7/classG4TrajectoryDrawByParticleID.html", null ],
      [ "G4TrajectoryGenericDrawer", "db/d26/classG4TrajectoryGenericDrawer.html", null ]
    ] ],
    [ "G4VTrajectoryPoint", "dc/de3/classG4VTrajectoryPoint.html", [
      [ "G4RayTrajectoryPoint", "d8/d78/classG4RayTrajectoryPoint.html", null ],
      [ "G4SmoothTrajectoryPoint", "df/d98/classG4SmoothTrajectoryPoint.html", null ],
      [ "G4TrajectoryPoint", "d3/dc2/classG4TrajectoryPoint.html", [
        [ "G4RichTrajectoryPoint", "d6/dae/classG4RichTrajectoryPoint.html", null ]
      ] ]
    ] ],
    [ "G4VTransactionManager", "d3/d6d/classG4VTransactionManager.html", null ],
    [ "G4VTRModel", "d8/dba/classG4VTRModel.html", null ],
    [ "G4VTwistSurface", "d1/d19/classG4VTwistSurface.html", [
      [ "G4TwistBoxSide", "d5/d01/classG4TwistBoxSide.html", null ],
      [ "G4TwistTrapAlphaSide", "dc/da5/classG4TwistTrapAlphaSide.html", null ],
      [ "G4TwistTrapFlatSide", "d2/d48/classG4TwistTrapFlatSide.html", null ],
      [ "G4TwistTrapParallelSide", "dd/db7/classG4TwistTrapParallelSide.html", null ],
      [ "G4TwistTubsFlatSide", "da/def/classG4TwistTubsFlatSide.html", null ],
      [ "G4TwistTubsHypeSide", "da/d4c/classG4TwistTubsHypeSide.html", null ],
      [ "G4TwistTubsSide", "dd/dcc/classG4TwistTubsSide.html", null ]
    ] ],
    [ "G4VTwoBodyAngDst", "d6/d9d/classG4VTwoBodyAngDst.html", [
      [ "G4NumIntTwoBodyAngDst< 15, 19 >", "d1/de5/classG4NumIntTwoBodyAngDst.html", [
        [ "G4GamP2NPipAngDst", "d5/dae/classG4GamP2NPipAngDst.html", null ],
        [ "G4GamP2PPi0AngDst", "d8/d3d/classG4GamP2PPi0AngDst.html", null ]
      ] ],
      [ "G4NumIntTwoBodyAngDst< 11, 19 >", "d1/de5/classG4NumIntTwoBodyAngDst.html", [
        [ "G4NP2NPAngDst", "d7/d39/classG4NP2NPAngDst.html", null ],
        [ "G4PP2PPAngDst", "df/d79/classG4PP2PPAngDst.html", null ],
        [ "G4Pi0P2Pi0PAngDst", "da/daa/classG4Pi0P2Pi0PAngDst.html", null ],
        [ "G4PimP2Pi0NAngDst", "db/d44/classG4PimP2Pi0NAngDst.html", null ],
        [ "G4PimP2PimPAngDst", "d7/d97/classG4PimP2PimPAngDst.html", null ],
        [ "G4PipP2PipPAngDst", "da/d01/classG4PipP2PipPAngDst.html", null ]
      ] ],
      [ "G4ParamExpTwoBodyAngDst< 10 >", "d2/df9/classG4ParamExpTwoBodyAngDst.html", [
        [ "G4GammaNuclAngDst", "d8/d01/classG4GammaNuclAngDst.html", null ],
        [ "G4HadNElastic1AngDst", "d3/d92/classG4HadNElastic1AngDst.html", null ],
        [ "G4HadNElastic2AngDst", "d4/de3/classG4HadNElastic2AngDst.html", null ],
        [ "G4PiNInelasticAngDst", "df/dfb/classG4PiNInelasticAngDst.html", null ]
      ] ],
      [ "G4ParamExpTwoBodyAngDst< 9 >", "d2/df9/classG4ParamExpTwoBodyAngDst.html", [
        [ "G4NuclNuclAngDst", "d9/dac/classG4NuclNuclAngDst.html", null ]
      ] ],
      [ "G4NumIntTwoBodyAngDst< NKEBINS, NANGLES >", "d1/de5/classG4NumIntTwoBodyAngDst.html", null ],
      [ "G4ParamExpTwoBodyAngDst< NKEBINS >", "d2/df9/classG4ParamExpTwoBodyAngDst.html", null ],
      [ "G4VThreeBodyAngDst", "d4/dd2/classG4VThreeBodyAngDst.html", [
        [ "G4InuclParamAngDst", "d7/dd6/classG4InuclParamAngDst.html", [
          [ "G4HadNucl3BodyAngDst", "db/dac/classG4HadNucl3BodyAngDst.html", null ],
          [ "G4NuclNucl3BodyAngDst", "dc/d99/classG4NuclNucl3BodyAngDst.html", null ]
        ] ]
      ] ]
    ] ],
    [ "G4VUIshell", "d8/def/classG4VUIshell.html", [
      [ "G4UIcsh", "d1/d4c/classG4UIcsh.html", null ],
      [ "G4UItcsh", "de/d50/classG4UItcsh.html", null ]
    ] ],
    [ "G4VUpdateSystemModel", "da/d93/classG4VUpdateSystemModel.html", [
      [ "G4DNAUpdateSystemModel", "df/d37/classG4DNAUpdateSystemModel.html", null ]
    ] ],
    [ "G4VUPLData", "db/d07/classG4VUPLData.html", null ],
    [ "G4VUPLSplitter< T >", "de/de6/classG4VUPLSplitter.html", null ],
    [ "G4VUserActionInitialization", "d0/d77/classG4VUserActionInitialization.html", null ],
    [ "G4VUserChemistryList", "df/d41/classG4VUserChemistryList.html", [
      [ "G4EmDNAChemistry", "d3/df8/classG4EmDNAChemistry.html", null ],
      [ "G4EmDNAChemistry_option1", "d6/d81/classG4EmDNAChemistry__option1.html", null ],
      [ "G4EmDNAChemistry_option2", "df/dbe/classG4EmDNAChemistry__option2.html", null ],
      [ "G4EmDNAChemistry_option3", "dd/df0/classG4EmDNAChemistry__option3.html", null ]
    ] ],
    [ "G4VUserDetectorConstruction", "d6/df8/classG4VUserDetectorConstruction.html", null ],
    [ "G4VUserEventInformation", "dc/dc6/classG4VUserEventInformation.html", null ],
    [ "G4VUserParallelWorld", "d6/dfb/classG4VUserParallelWorld.html", null ],
    [ "G4VUserPhysicsList", "d5/d8b/classG4VUserPhysicsList.html", [
      [ "G4ErrorPhysicsList", "d2/ddd/classG4ErrorPhysicsList.html", null ],
      [ "G4VModularPhysicsList", "dc/d2e/classG4VModularPhysicsList.html", [
        [ "FTFP_BERT", "d5/d8f/classFTFP__BERT.html", null ],
        [ "FTFP_BERT_ATL", "d7/d71/classFTFP__BERT__ATL.html", null ],
        [ "FTFP_BERT_HP", "d4/dc2/classFTFP__BERT__HP.html", null ],
        [ "FTFP_BERT_TRV", "d5/dbb/classFTFP__BERT__TRV.html", null ],
        [ "FTFQGSP_BERT", "dd/d75/classFTFQGSP__BERT.html", null ],
        [ "FTF_BIC", "d0/ddd/classFTF__BIC.html", null ],
        [ "LBE", "d4/d5a/classLBE.html", null ],
        [ "NuBeam", "da/da5/classNuBeam.html", null ],
        [ "QBBC", "da/dbe/classQBBC.html", null ],
        [ "QGSP_BERT", "d5/dab/classQGSP__BERT.html", null ],
        [ "QGSP_BERT_HP", "d0/db1/classQGSP__BERT__HP.html", null ],
        [ "QGSP_BIC", "d5/d88/classQGSP__BIC.html", null ],
        [ "QGSP_BIC_AllHP", "d2/d76/classQGSP__BIC__AllHP.html", null ],
        [ "QGSP_BIC_HP", "d1/d75/classQGSP__BIC__HP.html", null ],
        [ "QGSP_FTFP_BERT", "df/d11/classQGSP__FTFP__BERT.html", null ],
        [ "QGS_BIC", "de/dc7/classQGS__BIC.html", null ],
        [ "Shielding", "d3/d77/classShielding.html", [
          [ "ShieldingLEND", "d7/d93/classShieldingLEND.html", null ]
        ] ]
      ] ]
    ] ],
    [ "G4VUserPrimaryGeneratorAction", "d4/dbc/classG4VUserPrimaryGeneratorAction.html", [
      [ "G4AdjointPrimaryGeneratorAction", "d4/d34/classG4AdjointPrimaryGeneratorAction.html", null ],
      [ "G4RTPrimaryGeneratorAction", "de/dbb/classG4RTPrimaryGeneratorAction.html", null ]
    ] ],
    [ "G4VUserPrimaryParticleInformation", "d4/df8/classG4VUserPrimaryParticleInformation.html", null ],
    [ "G4VUserPrimaryVertexInformation", "dc/d2d/classG4VUserPrimaryVertexInformation.html", null ],
    [ "G4VUserRegionInformation", "d7/dad/classG4VUserRegionInformation.html", null ],
    [ "G4VUserTrackInformation", "df/de2/classG4VUserTrackInformation.html", [
      [ "G4IT", "d3/d7e/classG4IT.html", [
        [ "G4Molecule", "d9/d66/classG4Molecule.html", null ]
      ] ],
      [ "G4ScintillationTrackInformation", "d8/de8/classG4ScintillationTrackInformation.html", null ]
    ] ],
    [ "G4VUserVisAction", "d4/dec/classG4VUserVisAction.html", null ],
    [ "G4VViewer", "d3/dbf/classG4VViewer.html", [
      [ "G4ToolsSGViewer< tools::Qt::session, tools::Qt::sg_viewer >", "df/d01/classG4ToolsSGViewer.html", null ],
      [ "G4DAWNFILEViewer", "d4/d14/classG4DAWNFILEViewer.html", null ],
      [ "G4GMocrenFileViewer", "df/dd1/classG4GMocrenFileViewer.html", null ],
      [ "G4HepRepFileViewer", "d4/d16/classG4HepRepFileViewer.html", null ],
      [ "G4OpenGLViewer", "da/d6c/classG4OpenGLViewer.html", [
        [ "G4OpenGLImmediateViewer", "dc/d80/classG4OpenGLImmediateViewer.html", [
          [ "G4OpenGLImmediateQtViewer", "dd/d83/classG4OpenGLImmediateQtViewer.html", null ],
          [ "G4OpenGLImmediateWin32Viewer", "de/d60/classG4OpenGLImmediateWin32Viewer.html", null ],
          [ "G4OpenGLImmediateXViewer", "d2/dff/classG4OpenGLImmediateXViewer.html", null ],
          [ "G4OpenGLImmediateXmViewer", "d3/d2b/classG4OpenGLImmediateXmViewer.html", null ]
        ] ],
        [ "G4OpenGLQtViewer", "d2/d07/classG4OpenGLQtViewer.html", [
          [ "G4OpenGLImmediateQtViewer", "dd/d83/classG4OpenGLImmediateQtViewer.html", null ],
          [ "G4OpenGLStoredQtViewer", "d5/d99/classG4OpenGLStoredQtViewer.html", null ]
        ] ],
        [ "G4OpenGLStoredViewer", "d2/d1d/classG4OpenGLStoredViewer.html", [
          [ "G4OpenGLStoredQtViewer", "d5/d99/classG4OpenGLStoredQtViewer.html", null ],
          [ "G4OpenGLStoredWin32Viewer", "da/d68/classG4OpenGLStoredWin32Viewer.html", null ],
          [ "G4OpenGLStoredXViewer", "dc/dfc/classG4OpenGLStoredXViewer.html", null ],
          [ "G4OpenGLStoredXmViewer", "d1/d28/classG4OpenGLStoredXmViewer.html", null ]
        ] ],
        [ "G4OpenGLWin32Viewer", "dc/d57/classG4OpenGLWin32Viewer.html", [
          [ "G4OpenGLImmediateWin32Viewer", "de/d60/classG4OpenGLImmediateWin32Viewer.html", null ],
          [ "G4OpenGLStoredWin32Viewer", "da/d68/classG4OpenGLStoredWin32Viewer.html", null ]
        ] ],
        [ "G4OpenGLXViewer", "d0/dde/classG4OpenGLXViewer.html", [
          [ "G4OpenGLImmediateXViewer", "d2/dff/classG4OpenGLImmediateXViewer.html", null ],
          [ "G4OpenGLStoredXViewer", "dc/dfc/classG4OpenGLStoredXViewer.html", null ],
          [ "G4OpenGLXmViewer", "da/d69/classG4OpenGLXmViewer.html", [
            [ "G4OpenGLImmediateXmViewer", "d3/d2b/classG4OpenGLImmediateXmViewer.html", null ],
            [ "G4OpenGLStoredXmViewer", "d1/d28/classG4OpenGLStoredXmViewer.html", null ]
          ] ]
        ] ]
      ] ],
      [ "G4OpenInventorViewer", "d6/d41/classG4OpenInventorViewer.html", [
        [ "G4OpenInventorQtViewer", "dd/ddf/classG4OpenInventorQtViewer.html", null ],
        [ "G4OpenInventorWinViewer", "dc/df0/classG4OpenInventorWinViewer.html", null ],
        [ "G4OpenInventorXtExtendedViewer", "db/d35/classG4OpenInventorXtExtendedViewer.html", null ],
        [ "G4OpenInventorXtViewer", "d7/ddb/classG4OpenInventorXtViewer.html", null ]
      ] ],
      [ "G4Qt3DViewer", "db/df7/classG4Qt3DViewer.html", null ],
      [ "G4RayTracerViewer", "d5/dee/classG4RayTracerViewer.html", [
        [ "G4RayTracerXViewer", "d1/dd2/classG4RayTracerXViewer.html", null ]
      ] ],
      [ "G4ToolsSGViewer< SG_SESSION, SG_VIEWER >", "df/d01/classG4ToolsSGViewer.html", null ],
      [ "G4VRML2FileViewer", "d6/d64/classG4VRML2FileViewer.html", null ],
      [ "G4VTreeViewer", "d2/dfc/classG4VTreeViewer.html", [
        [ "G4ASCIITreeViewer", "d9/d1a/classG4ASCIITreeViewer.html", null ]
      ] ],
      [ "G4VtkViewer", "d3/dfd/classG4VtkViewer.html", [
        [ "G4VtkQtViewer", "db/d1b/classG4VtkQtViewer.html", null ]
      ] ]
    ] ],
    [ "G4VVisCommandGeometrySetFunction", "df/d4f/classG4VVisCommandGeometrySetFunction.html", [
      [ "G4VisCommandGeometrySetColourFunction", "df/dfa/classG4VisCommandGeometrySetColourFunction.html", null ],
      [ "G4VisCommandGeometrySetDaughtersInvisibleFunction", "d1/dea/classG4VisCommandGeometrySetDaughtersInvisibleFunction.html", null ],
      [ "G4VisCommandGeometrySetForceAuxEdgeVisibleFunction", "de/d1a/classG4VisCommandGeometrySetForceAuxEdgeVisibleFunction.html", null ],
      [ "G4VisCommandGeometrySetForceCloudFunction", "d8/dbc/classG4VisCommandGeometrySetForceCloudFunction.html", null ],
      [ "G4VisCommandGeometrySetForceLineSegmentsPerCircleFunction", "d1/d96/classG4VisCommandGeometrySetForceLineSegmentsPerCircleFunction.html", null ],
      [ "G4VisCommandGeometrySetForceSolidFunction", "d0/dff/classG4VisCommandGeometrySetForceSolidFunction.html", null ],
      [ "G4VisCommandGeometrySetForceWireframeFunction", "d7/d68/classG4VisCommandGeometrySetForceWireframeFunction.html", null ],
      [ "G4VisCommandGeometrySetLineStyleFunction", "df/d3d/classG4VisCommandGeometrySetLineStyleFunction.html", null ],
      [ "G4VisCommandGeometrySetLineWidthFunction", "d7/d71/classG4VisCommandGeometrySetLineWidthFunction.html", null ],
      [ "G4VisCommandGeometrySetVisibilityFunction", "de/dfd/classG4VisCommandGeometrySetVisibilityFunction.html", null ]
    ] ],
    [ "G4VVisManager", "d8/d25/classG4VVisManager.html", [
      [ "G4VisManager", "d0/d0d/classG4VisManager.html", [
        [ "G4VisExecutive", "df/d51/classG4VisExecutive.html", null ]
      ] ]
    ] ],
    [ "G4VVolumeMaterialScanner", "dd/dbb/classG4VVolumeMaterialScanner.html", [
      [ "G4VNestedParameterisation", "d3/de0/classG4VNestedParameterisation.html", null ]
    ] ],
    [ "G4VWeightWindowAlgorithm", "d8/dcd/classG4VWeightWindowAlgorithm.html", [
      [ "G4WeightWindowAlgorithm", "d1/dde/classG4WeightWindowAlgorithm.html", null ]
    ] ],
    [ "G4VWeightWindowStore", "d2/d84/classG4VWeightWindowStore.html", [
      [ "G4WeightWindowStore", "dd/d33/classG4WeightWindowStore.html", null ]
    ] ],
    [ "G4VWLSTimeGeneratorProfile", "d7/d33/classG4VWLSTimeGeneratorProfile.html", [
      [ "G4WLSTimeGeneratorProfileDelta", "d6/da2/classG4WLSTimeGeneratorProfileDelta.html", null ],
      [ "G4WLSTimeGeneratorProfileExponential", "d4/d7a/classG4WLSTimeGeneratorProfileExponential.html", null ]
    ] ],
    [ "G4VXResonanceTable", "df/d08/classG4VXResonanceTable.html", [
      [ "G4DeltaDeltastarBuilder", "d8/ded/classG4DeltaDeltastarBuilder.html", null ],
      [ "G4DeltaNstarBuilder", "db/d90/classG4DeltaNstarBuilder.html", null ],
      [ "G4NDeltastarBuilder", "d4/da0/classG4NDeltastarBuilder.html", null ],
      [ "G4NNstarBuilder", "de/d29/classG4NNstarBuilder.html", null ],
      [ "G4XDeltaDeltaTable", "d4/dee/classG4XDeltaDeltaTable.html", null ],
      [ "G4XNDeltaTable", "d5/d4b/classG4XNDeltaTable.html", null ]
    ] ],
    [ "G4WarnPLStatus", "d9/da2/classG4WarnPLStatus.html", null ],
    [ "G4WatcherGun", "dd/d4c/classG4WatcherGun.html", null ],
    [ "G4WaterStopping", "d9/dc4/classG4WaterStopping.html", null ],
    [ "G4WendtFissionFragmentGenerator", "db/d28/classG4WendtFissionFragmentGenerator.html", null ],
    [ "G4WentzelOKandVIxSection", "d9/d5f/classG4WentzelOKandVIxSection.html", [
      [ "G4WentzelVIRelXSection", "d9/dbc/classG4WentzelVIRelXSection.html", null ]
    ] ],
    [ "G4WilsonRadius", "de/df5/classG4WilsonRadius.html", null ],
    [ "G4WorkerThread", "dd/d14/classG4WorkerThread.html", null ],
    [ "G4XDeltaDeltastarTable", "d3/d1d/classG4XDeltaDeltastarTable.html", null ],
    [ "G4XDeltaNstarTable", "d1/d64/classG4XDeltaNstarTable.html", null ],
    [ "G4XNDeltastarTable", "d8/da8/classG4XNDeltastarTable.html", null ],
    [ "G4XNNstarTable", "d1/d9a/classG4XNNstarTable.html", null ],
    [ "GFlashEnergySpot", "d0/d7a/classGFlashEnergySpot.html", null ],
    [ "GFlashHitMaker", "d5/d0b/classGFlashHitMaker.html", null ],
    [ "GFlashParticleBounds", "d3/de6/classGFlashParticleBounds.html", null ],
    [ "GIDI_settings", "d2/d8c/classGIDI__settings.html", null ],
    [ "GIDI_settings_flux", "d9/d4a/classGIDI__settings__flux.html", null ],
    [ "GIDI_settings_flux_order", "d4/d1b/classGIDI__settings__flux__order.html", null ],
    [ "GIDI_settings_group", "df/d8e/classGIDI__settings__group.html", null ],
    [ "GIDI_settings_particle", "db/d52/classGIDI__settings__particle.html", null ],
    [ "GIDI_settings_processedFlux", "d7/dea/classGIDI__settings__processedFlux.html", null ],
    [ "GL2PSbackend", "d8/dd0/structGL2PSbackend.html", null ],
    [ "GL2PSbsptree2d_", "da/d9f/structGL2PSbsptree2d__.html", null ],
    [ "GL2PSbsptree_", "dd/d00/structGL2PSbsptree__.html", null ],
    [ "GL2PScompress", "d7/d97/structGL2PScompress.html", null ],
    [ "GL2PScontext", "d3/d48/structGL2PScontext.html", null ],
    [ "GL2PSimage", "d2/d5b/structGL2PSimage.html", null ],
    [ "GL2PSimagemap_", "d2/d52/structGL2PSimagemap__.html", null ],
    [ "GL2PSlist", "d3/d9d/structGL2PSlist.html", null ],
    [ "GL2PSpdfgroup", "d6/d57/structGL2PSpdfgroup.html", null ],
    [ "GL2PSprimitive", "d9/d39/structGL2PSprimitive.html", null ],
    [ "GL2PSstring", "da/d09/structGL2PSstring.html", null ],
    [ "GL2PStriangle", "d4/d7b/structGL2PStriangle.html", null ],
    [ "GL2PSvertex", "d2/d41/structGL2PSvertex.html", null ],
    [ "PTL::tbb::global_control", "df/d97/classPTL_1_1tbb_1_1global__control.html", null ],
    [ "G4INCL::GlobalInfo", "d1/df2/structG4INCL_1_1GlobalInfo.html", null ],
    [ "GMocrenDataPrimitive< T >", "d2/db6/classGMocrenDataPrimitive.html", null ],
    [ "GMocrenDataPrimitive< short >", "d2/db6/classGMocrenDataPrimitive.html", null ],
    [ "GMocrenDetector", "d3/d86/classGMocrenDetector.html", null ],
    [ "GMocrenTrack", "dc/dd1/classGMocrenTrack.html", null ],
    [ "G4GoudsmitSaundersonTable::GSMSCAngularDtr", "df/d6e/structG4GoudsmitSaundersonTable_1_1GSMSCAngularDtr.html", null ],
    [ "GVFlashHomoShowerTuning", "df/da1/classGVFlashHomoShowerTuning.html", [
      [ "GFlashSamplingShowerTuning", "d3/d70/classGFlashSamplingShowerTuning.html", null ]
    ] ],
    [ "GVFlashShowerParameterisation", "d5/d27/classGVFlashShowerParameterisation.html", [
      [ "GFlashHomoShowerParameterisation", "d2/d91/classGFlashHomoShowerParameterisation.html", null ],
      [ "GFlashSamplingShowerParameterisation", "d2/df8/classGFlashSamplingShowerParameterisation.html", null ]
    ] ],
    [ "gz_header_s", "d4/d86/structgz__header__s.html", null ],
    [ "gz_state", "d1/d4f/structgz__state.html", null ],
    [ "gzFile_s", "d2/d88/structgzFile__s.html", null ],
    [ "g4tim::handler< Types >", "d4/d0c/structg4tim_1_1handler.html", null ],
    [ "std::hash< G4Polyhedron >", "d6/d32/structstd_1_1hash_3_01G4Polyhedron_01_4.html", null ],
    [ "std::hash< G4VisAttributes >", "dd/dc2/structstd_1_1hash_3_01G4VisAttributes_01_4.html", null ],
    [ "HASH_TABLE", "d1/d4e/structHASH__TABLE.html", null ],
    [ "HASH_TABLE_ITER", "d7/d3e/structHASH__TABLE__ITER.html", null ],
    [ "G4DimensionedTypeUtils::HasName", "d0/de2/classG4DimensionedTypeUtils_1_1HasName.html", null ],
    [ "CLHEP::Hep2Vector", "d4/da4/classCLHEP_1_1Hep2Vector.html", null ],
    [ "CLHEP::Hep3Vector", "da/dcf/classCLHEP_1_1Hep3Vector.html", [
      [ "G4StokesVector", "dd/d1d/classG4StokesVector.html", null ]
    ] ],
    [ "CLHEP::Hep4RotationInterface", "dd/d4b/classCLHEP_1_1Hep4RotationInterface.html", [
      [ "CLHEP::Hep3RotationInterface", "dd/d61/classCLHEP_1_1Hep3RotationInterface.html", null ]
    ] ],
    [ "CLHEP::HepAxisAngle", "dd/d24/classCLHEP_1_1HepAxisAngle.html", null ],
    [ "CLHEP::HepBoost", "d0/df6/classCLHEP_1_1HepBoost.html", null ],
    [ "CLHEP::HepBoostX", "da/db0/classCLHEP_1_1HepBoostX.html", null ],
    [ "CLHEP::HepBoostY", "da/d3d/classCLHEP_1_1HepBoostY.html", null ],
    [ "CLHEP::HepBoostZ", "d5/dc3/classCLHEP_1_1HepBoostZ.html", null ],
    [ "CLHEP::HepEulerAngles", "d1/dfa/classCLHEP_1_1HepEulerAngles.html", null ],
    [ "CLHEP::HepLorentzRotation", "da/db4/classCLHEP_1_1HepLorentzRotation.html", null ],
    [ "CLHEP::HepLorentzRotation::HepLorentzRotation_row", "de/d68/classCLHEP_1_1HepLorentzRotation_1_1HepLorentzRotation__row.html", null ],
    [ "CLHEP::HepLorentzVector", "dc/df4/classCLHEP_1_1HepLorentzVector.html", null ],
    [ "HepPolyhedron", "d3/d94/classHepPolyhedron.html", [
      [ "G4Polyhedron", "df/d2b/classG4Polyhedron.html", null ],
      [ "HepPolyhedronCons", "d6/d55/classHepPolyhedronCons.html", [
        [ "HepPolyhedronCone", "d6/d4e/classHepPolyhedronCone.html", null ],
        [ "HepPolyhedronTube", "da/d33/classHepPolyhedronTube.html", null ],
        [ "HepPolyhedronTubs", "db/d00/classHepPolyhedronTubs.html", null ]
      ] ],
      [ "HepPolyhedronEllipsoid", "da/d7d/classHepPolyhedronEllipsoid.html", null ],
      [ "HepPolyhedronEllipticalCone", "d7/ded/classHepPolyhedronEllipticalCone.html", null ],
      [ "HepPolyhedronHype", "d5/dfe/classHepPolyhedronHype.html", null ],
      [ "HepPolyhedronHyperbolicMirror", "d0/df5/classHepPolyhedronHyperbolicMirror.html", null ],
      [ "HepPolyhedronParaboloid", "de/d41/classHepPolyhedronParaboloid.html", null ],
      [ "HepPolyhedronPgon", "db/d05/classHepPolyhedronPgon.html", [
        [ "HepPolyhedronPcon", "d8/d4d/classHepPolyhedronPcon.html", null ]
      ] ],
      [ "HepPolyhedronSphere", "db/d0c/classHepPolyhedronSphere.html", null ],
      [ "HepPolyhedronTet", "d7/df3/classHepPolyhedronTet.html", null ],
      [ "HepPolyhedronTorus", "d7/d17/classHepPolyhedronTorus.html", null ],
      [ "HepPolyhedronTrap", "d0/dea/classHepPolyhedronTrap.html", [
        [ "HepPolyhedronPara", "df/de8/classHepPolyhedronPara.html", null ]
      ] ],
      [ "HepPolyhedronTrd2", "d1/d46/classHepPolyhedronTrd2.html", [
        [ "HepPolyhedronBox", "d3/d18/classHepPolyhedronBox.html", null ],
        [ "HepPolyhedronTrd1", "df/d4a/classHepPolyhedronTrd1.html", null ]
      ] ]
    ] ],
    [ "HepPolyhedronProcessor", "dc/d2c/classHepPolyhedronProcessor.html", null ],
    [ "CLHEP::HepRandom", "d8/d19/classCLHEP_1_1HepRandom.html", [
      [ "CLHEP::RandBinomial", "d9/d72/classCLHEP_1_1RandBinomial.html", null ],
      [ "CLHEP::RandBreitWigner", "d4/d99/classCLHEP_1_1RandBreitWigner.html", null ],
      [ "CLHEP::RandChiSquare", "d0/db8/classCLHEP_1_1RandChiSquare.html", null ],
      [ "CLHEP::RandExpZiggurat", "dc/d5a/classCLHEP_1_1RandExpZiggurat.html", null ],
      [ "CLHEP::RandExponential", "d1/db9/classCLHEP_1_1RandExponential.html", null ],
      [ "CLHEP::RandFlat", "d8/d82/classCLHEP_1_1RandFlat.html", [
        [ "CLHEP::RandBit", "d5/d93/classCLHEP_1_1RandBit.html", null ]
      ] ],
      [ "CLHEP::RandGamma", "d6/d25/classCLHEP_1_1RandGamma.html", null ],
      [ "CLHEP::RandGauss", "d0/dce/classCLHEP_1_1RandGauss.html", [
        [ "CLHEP::RandGaussQ", "dc/d3d/classCLHEP_1_1RandGaussQ.html", null ],
        [ "CLHEP::RandGaussZiggurat", "d1/d47/classCLHEP_1_1RandGaussZiggurat.html", null ]
      ] ],
      [ "CLHEP::RandGeneral", "d1/da9/classCLHEP_1_1RandGeneral.html", null ],
      [ "CLHEP::RandLandau", "d4/d50/classCLHEP_1_1RandLandau.html", null ],
      [ "CLHEP::RandPoisson", "d6/dc3/classCLHEP_1_1RandPoisson.html", [
        [ "CLHEP::RandPoissonQ", "d4/d41/classCLHEP_1_1RandPoissonQ.html", null ]
      ] ],
      [ "CLHEP::RandStudentT", "db/da4/classCLHEP_1_1RandStudentT.html", null ]
    ] ],
    [ "CLHEP::HepRandomEngine", "d8/dcf/classCLHEP_1_1HepRandomEngine.html", [
      [ "CLHEP::DualRand", "d1/d39/classCLHEP_1_1DualRand.html", null ],
      [ "CLHEP::HepJamesRandom", "d0/daf/classCLHEP_1_1HepJamesRandom.html", null ],
      [ "CLHEP::MTwistEngine", "d6/d27/classCLHEP_1_1MTwistEngine.html", null ],
      [ "CLHEP::MixMaxRng", "d0/d36/classCLHEP_1_1MixMaxRng.html", null ],
      [ "CLHEP::NonRandomEngine", "d0/d5e/classCLHEP_1_1NonRandomEngine.html", null ],
      [ "CLHEP::RanecuEngine", "de/d85/classCLHEP_1_1RanecuEngine.html", null ],
      [ "CLHEP::Ranlux64Engine", "d4/d73/classCLHEP_1_1Ranlux64Engine.html", null ],
      [ "CLHEP::RanluxEngine", "df/d51/classCLHEP_1_1RanluxEngine.html", null ],
      [ "CLHEP::RanluxppEngine", "da/da9/classCLHEP_1_1RanluxppEngine.html", null ],
      [ "CLHEP::RanshiEngine", "de/dfe/classCLHEP_1_1RanshiEngine.html", null ]
    ] ],
    [ "CLHEP::HepRep3x3", "d9/dc8/structCLHEP_1_1HepRep3x3.html", null ],
    [ "CLHEP::HepRep4x4", "d3/d1d/structCLHEP_1_1HepRep4x4.html", null ],
    [ "CLHEP::HepRep4x4Symmetric", "de/d0a/structCLHEP_1_1HepRep4x4Symmetric.html", null ],
    [ "CLHEP::HepRotation", "dd/d65/classCLHEP_1_1HepRotation.html", [
      [ "G3toG4RotationMatrix", "d5/dea/classG3toG4RotationMatrix.html", null ]
    ] ],
    [ "CLHEP::HepRotation::HepRotation_row", "d9/dcd/classCLHEP_1_1HepRotation_1_1HepRotation__row.html", null ],
    [ "CLHEP::HepRotationX", "d8/df0/classCLHEP_1_1HepRotationX.html", null ],
    [ "CLHEP::HepRotationY", "d0/d79/classCLHEP_1_1HepRotationY.html", null ],
    [ "CLHEP::HepRotationZ", "d1/db8/classCLHEP_1_1HepRotationZ.html", null ],
    [ "CLHEP::HepStat", "d0/db3/classCLHEP_1_1HepStat.html", null ],
    [ "G4CascadeHistory::HistoryEntry", "d4/d19/structG4CascadeHistory_1_1HistoryEntry.html", null ],
    [ "G4INCL::HornerCoefficients< N >", "d9/db8/classG4INCL_1_1HornerCoefficients.html", null ],
    [ "G4INCL::HornerCoefficients< 1 >", "d9/db8/classG4INCL_1_1HornerCoefficients.html", [
      [ "G4INCL::HornerC1", "da/dd2/structG4INCL_1_1HornerC1.html", null ]
    ] ],
    [ "G4INCL::HornerCoefficients< 2 >", "d9/db8/classG4INCL_1_1HornerCoefficients.html", [
      [ "G4INCL::HornerC2", "d4/d48/structG4INCL_1_1HornerC2.html", null ]
    ] ],
    [ "G4INCL::HornerCoefficients< 3 >", "d9/db8/classG4INCL_1_1HornerCoefficients.html", [
      [ "G4INCL::HornerC3", "de/d03/structG4INCL_1_1HornerC3.html", null ]
    ] ],
    [ "G4INCL::HornerCoefficients< 4 >", "d9/db8/classG4INCL_1_1HornerCoefficients.html", [
      [ "G4INCL::HornerC4", "dc/dde/structG4INCL_1_1HornerC4.html", null ]
    ] ],
    [ "G4INCL::HornerCoefficients< 5 >", "d9/db8/classG4INCL_1_1HornerCoefficients.html", [
      [ "G4INCL::HornerC5", "d6/dc9/structG4INCL_1_1HornerC5.html", null ]
    ] ],
    [ "G4INCL::HornerCoefficients< 6 >", "d9/db8/classG4INCL_1_1HornerCoefficients.html", [
      [ "G4INCL::HornerC6", "db/de6/structG4INCL_1_1HornerC6.html", null ]
    ] ],
    [ "G4INCL::HornerCoefficients< 7 >", "d9/db8/classG4INCL_1_1HornerCoefficients.html", [
      [ "G4INCL::HornerC7", "d6/d96/structG4INCL_1_1HornerC7.html", null ]
    ] ],
    [ "G4INCL::HornerCoefficients< 8 >", "d9/db8/classG4INCL_1_1HornerCoefficients.html", [
      [ "G4INCL::HornerC8", "d3/de5/structG4INCL_1_1HornerC8.html", null ]
    ] ],
    [ "G4INCL::HornerEvaluator< M >", "dd/da6/structG4INCL_1_1HornerEvaluator.html", null ],
    [ "G4INCL::HornerEvaluator< 1 >", "d2/de3/structG4INCL_1_1HornerEvaluator_3_011_01_4.html", null ],
    [ "G4KDTree::HyperRect", "d2/dfb/classG4KDTree_1_1HyperRect.html", null ],
    [ "G4INCL::IAvatar", "dd/d90/classG4INCL_1_1IAvatar.html", [
      [ "G4INCL::InteractionAvatar", "d2/dc2/classG4INCL_1_1InteractionAvatar.html", [
        [ "G4INCL::BinaryCollisionAvatar", "d2/d0d/classG4INCL_1_1BinaryCollisionAvatar.html", null ],
        [ "G4INCL::DecayAvatar", "d6/dfb/classG4INCL_1_1DecayAvatar.html", null ]
      ] ],
      [ "G4INCL::ParticleEntryAvatar", "de/d6a/classG4INCL_1_1ParticleEntryAvatar.html", null ],
      [ "G4INCL::SurfaceAvatar", "d4/d6c/classG4INCL_1_1SurfaceAvatar.html", null ]
    ] ],
    [ "G4INCL::IChannel", "d1/da7/classG4INCL_1_1IChannel.html", [
      [ "G4INCL::DeltaDecayChannel", "de/d02/classG4INCL_1_1DeltaDecayChannel.html", null ],
      [ "G4INCL::DeltaProductionChannel", "d2/d01/classG4INCL_1_1DeltaProductionChannel.html", null ],
      [ "G4INCL::ElasticChannel", "d9/d47/classG4INCL_1_1ElasticChannel.html", null ],
      [ "G4INCL::EtaNElasticChannel", "d0/d84/classG4INCL_1_1EtaNElasticChannel.html", null ],
      [ "G4INCL::EtaNToPiNChannel", "d0/d64/classG4INCL_1_1EtaNToPiNChannel.html", null ],
      [ "G4INCL::EtaNToPiPiNChannel", "d1/df8/classG4INCL_1_1EtaNToPiPiNChannel.html", null ],
      [ "G4INCL::NDeltaEtaProductionChannel", "d0/d9d/classG4INCL_1_1NDeltaEtaProductionChannel.html", null ],
      [ "G4INCL::NDeltaOmegaProductionChannel", "d1/d52/classG4INCL_1_1NDeltaOmegaProductionChannel.html", null ],
      [ "G4INCL::NDeltaToDeltaLKChannel", "d0/dd1/classG4INCL_1_1NDeltaToDeltaLKChannel.html", null ],
      [ "G4INCL::NDeltaToDeltaSKChannel", "de/dac/classG4INCL_1_1NDeltaToDeltaSKChannel.html", null ],
      [ "G4INCL::NDeltaToNLKChannel", "d2/d65/classG4INCL_1_1NDeltaToNLKChannel.html", null ],
      [ "G4INCL::NDeltaToNNKKbChannel", "d5/dbb/classG4INCL_1_1NDeltaToNNKKbChannel.html", null ],
      [ "G4INCL::NDeltaToNSKChannel", "db/da1/classG4INCL_1_1NDeltaToNSKChannel.html", null ],
      [ "G4INCL::NKElasticChannel", "d1/d88/classG4INCL_1_1NKElasticChannel.html", null ],
      [ "G4INCL::NKToNK2piChannel", "d6/d3e/classG4INCL_1_1NKToNK2piChannel.html", null ],
      [ "G4INCL::NKToNKChannel", "dc/dcf/classG4INCL_1_1NKToNKChannel.html", null ],
      [ "G4INCL::NKToNKpiChannel", "d6/df5/classG4INCL_1_1NKToNKpiChannel.html", null ],
      [ "G4INCL::NKbElasticChannel", "df/d29/classG4INCL_1_1NKbElasticChannel.html", null ],
      [ "G4INCL::NKbToL2piChannel", "de/db6/classG4INCL_1_1NKbToL2piChannel.html", null ],
      [ "G4INCL::NKbToLpiChannel", "d1/d72/classG4INCL_1_1NKbToLpiChannel.html", null ],
      [ "G4INCL::NKbToNKb2piChannel", "dc/dd1/classG4INCL_1_1NKbToNKb2piChannel.html", null ],
      [ "G4INCL::NKbToNKbChannel", "d1/dcb/classG4INCL_1_1NKbToNKbChannel.html", null ],
      [ "G4INCL::NKbToNKbpiChannel", "d6/de0/classG4INCL_1_1NKbToNKbpiChannel.html", null ],
      [ "G4INCL::NKbToS2piChannel", "d8/d71/classG4INCL_1_1NKbToS2piChannel.html", null ],
      [ "G4INCL::NKbToSpiChannel", "db/dad/classG4INCL_1_1NKbToSpiChannel.html", null ],
      [ "G4INCL::NLToNSChannel", "d6/dc4/classG4INCL_1_1NLToNSChannel.html", null ],
      [ "G4INCL::NNEtaToMultiPionsChannel", "d7/d74/classG4INCL_1_1NNEtaToMultiPionsChannel.html", null ],
      [ "G4INCL::NNOmegaToMultiPionsChannel", "d1/d80/classG4INCL_1_1NNOmegaToMultiPionsChannel.html", null ],
      [ "G4INCL::NNToMissingStrangenessChannel", "d5/deb/classG4INCL_1_1NNToMissingStrangenessChannel.html", null ],
      [ "G4INCL::NNToMultiPionsChannel", "d6/de5/classG4INCL_1_1NNToMultiPionsChannel.html", null ],
      [ "G4INCL::NNToNLK2piChannel", "df/d71/classG4INCL_1_1NNToNLK2piChannel.html", null ],
      [ "G4INCL::NNToNLKChannel", "d2/d74/classG4INCL_1_1NNToNLKChannel.html", null ],
      [ "G4INCL::NNToNLKpiChannel", "d2/dff/classG4INCL_1_1NNToNLKpiChannel.html", null ],
      [ "G4INCL::NNToNNEtaChannel", "df/db0/classG4INCL_1_1NNToNNEtaChannel.html", null ],
      [ "G4INCL::NNToNNKKbChannel", "db/d4d/classG4INCL_1_1NNToNNKKbChannel.html", null ],
      [ "G4INCL::NNToNNOmegaChannel", "d9/d74/classG4INCL_1_1NNToNNOmegaChannel.html", null ],
      [ "G4INCL::NNToNSK2piChannel", "d8/d90/classG4INCL_1_1NNToNSK2piChannel.html", null ],
      [ "G4INCL::NNToNSKChannel", "dc/d29/classG4INCL_1_1NNToNSKChannel.html", null ],
      [ "G4INCL::NNToNSKpiChannel", "d3/d90/classG4INCL_1_1NNToNSKpiChannel.html", null ],
      [ "G4INCL::NSToNLChannel", "d8/df1/classG4INCL_1_1NSToNLChannel.html", null ],
      [ "G4INCL::NSToNSChannel", "d4/dff/classG4INCL_1_1NSToNSChannel.html", null ],
      [ "G4INCL::NYElasticChannel", "d0/d29/classG4INCL_1_1NYElasticChannel.html", null ],
      [ "G4INCL::NeutralKaonDecayChannel", "da/d0f/classG4INCL_1_1NeutralKaonDecayChannel.html", null ],
      [ "G4INCL::NpiToLK2piChannel", "d7/dd0/classG4INCL_1_1NpiToLK2piChannel.html", null ],
      [ "G4INCL::NpiToLKChannel", "d5/d33/classG4INCL_1_1NpiToLKChannel.html", null ],
      [ "G4INCL::NpiToLKpiChannel", "d8/d1c/classG4INCL_1_1NpiToLKpiChannel.html", null ],
      [ "G4INCL::NpiToMissingStrangenessChannel", "d4/d1a/classG4INCL_1_1NpiToMissingStrangenessChannel.html", null ],
      [ "G4INCL::NpiToNKKbChannel", "da/d50/classG4INCL_1_1NpiToNKKbChannel.html", null ],
      [ "G4INCL::NpiToSK2piChannel", "de/d8a/classG4INCL_1_1NpiToSK2piChannel.html", null ],
      [ "G4INCL::NpiToSKChannel", "d1/d51/classG4INCL_1_1NpiToSKChannel.html", null ],
      [ "G4INCL::NpiToSKpiChannel", "d3/dd4/classG4INCL_1_1NpiToSKpiChannel.html", null ],
      [ "G4INCL::OmegaNElasticChannel", "d3/d9b/classG4INCL_1_1OmegaNElasticChannel.html", null ],
      [ "G4INCL::OmegaNToPiNChannel", "d9/dac/classG4INCL_1_1OmegaNToPiNChannel.html", null ],
      [ "G4INCL::OmegaNToPiPiNChannel", "d6/d73/classG4INCL_1_1OmegaNToPiPiNChannel.html", null ],
      [ "G4INCL::ParticleEntryChannel", "dc/da9/classG4INCL_1_1ParticleEntryChannel.html", null ],
      [ "G4INCL::PiNElasticChannel", "dd/d86/classG4INCL_1_1PiNElasticChannel.html", null ],
      [ "G4INCL::PiNToDeltaChannel", "d3/d7a/classG4INCL_1_1PiNToDeltaChannel.html", null ],
      [ "G4INCL::PiNToEtaChannel", "dc/d5f/classG4INCL_1_1PiNToEtaChannel.html", null ],
      [ "G4INCL::PiNToMultiPionsChannel", "d6/d41/classG4INCL_1_1PiNToMultiPionsChannel.html", null ],
      [ "G4INCL::PiNToOmegaChannel", "dd/d02/classG4INCL_1_1PiNToOmegaChannel.html", null ],
      [ "G4INCL::PionResonanceDecayChannel", "dc/d52/classG4INCL_1_1PionResonanceDecayChannel.html", null ],
      [ "G4INCL::RecombinationChannel", "df/d00/classG4INCL_1_1RecombinationChannel.html", null ],
      [ "G4INCL::ReflectionChannel", "d4/df0/classG4INCL_1_1ReflectionChannel.html", null ],
      [ "G4INCL::SigmaZeroDecayChannel", "da/d1d/classG4INCL_1_1SigmaZeroDecayChannel.html", null ],
      [ "G4INCL::StrangeAbsorbtionChannel", "db/df8/classG4INCL_1_1StrangeAbsorbtionChannel.html", null ],
      [ "G4INCL::TransmissionChannel", "d8/d5c/classG4INCL_1_1TransmissionChannel.html", null ]
    ] ],
    [ "G4INCL::IClusteringModel", "dd/d10/classG4INCL_1_1IClusteringModel.html", [
      [ "G4INCL::ClusteringModelIntercomparison", "dd/d6a/classG4INCL_1_1ClusteringModelIntercomparison.html", null ],
      [ "G4INCL::ClusteringModelNone", "de/d28/classG4INCL_1_1ClusteringModelNone.html", null ]
    ] ],
    [ "G4INCL::ICoulomb", "db/d54/classG4INCL_1_1ICoulomb.html", [
      [ "G4INCL::CoulombNonRelativistic", "d9/d99/classG4INCL_1_1CoulombNonRelativistic.html", null ],
      [ "G4INCL::CoulombNone", "d6/d52/classG4INCL_1_1CoulombNone.html", null ]
    ] ],
    [ "G4INCL::ICrossSections", "da/d2d/classG4INCL_1_1ICrossSections.html", [
      [ "G4INCL::CrossSectionsINCL46", "d2/de2/classG4INCL_1_1CrossSectionsINCL46.html", null ],
      [ "G4INCL::CrossSectionsMultiPions", "d5/da6/classG4INCL_1_1CrossSectionsMultiPions.html", [
        [ "G4INCL::CrossSectionsMultiPionsAndResonances", "d7/d43/classG4INCL_1_1CrossSectionsMultiPionsAndResonances.html", [
          [ "G4INCL::CrossSectionsStrangeness", "d0/d36/classG4INCL_1_1CrossSectionsStrangeness.html", null ]
        ] ],
        [ "G4INCL::CrossSectionsTruncatedMultiPions", "dd/dae/classG4INCL_1_1CrossSectionsTruncatedMultiPions.html", null ]
      ] ]
    ] ],
    [ "G4ExpConsts::ieee754", "d9/d0f/unionG4ExpConsts_1_1ieee754.html", null ],
    [ "G4LogConsts::ieee754", "d1/d8c/unionG4LogConsts_1_1ieee754.html", null ],
    [ "IEventScheduler", "d7/dc7/classIEventScheduler.html", [
      [ "G4DNAEventScheduler", "d9/d76/classG4DNAEventScheduler.html", null ]
    ] ],
    [ "IEventSet", "d1/d97/classIEventSet.html", [
      [ "G4DNAEventSet", "d4/d16/classG4DNAEventSet.html", null ]
    ] ],
    [ "G4INCL::IFunction1D", "da/d7a/classG4INCL_1_1IFunction1D.html", [
      [ "G4INCL::InterpolationTable", "d5/d81/classG4INCL_1_1InterpolationTable.html", [
        [ "G4INCL::InvFInterpolationTable", "d4/de7/classG4INCL_1_1InvFInterpolationTable.html", null ]
      ] ],
      [ "G4INCL::NuclearDensityFunctions::Gaussian", "d5/d47/classG4INCL_1_1NuclearDensityFunctions_1_1Gaussian.html", null ],
      [ "G4INCL::NuclearDensityFunctions::GaussianRP", "d6/dd2/classG4INCL_1_1NuclearDensityFunctions_1_1GaussianRP.html", null ],
      [ "G4INCL::NuclearDensityFunctions::HardSphere", "dd/dd0/classG4INCL_1_1NuclearDensityFunctions_1_1HardSphere.html", null ],
      [ "G4INCL::NuclearDensityFunctions::ModifiedHarmonicOscillator", "db/d05/classG4INCL_1_1NuclearDensityFunctions_1_1ModifiedHarmonicOscillator.html", null ],
      [ "G4INCL::NuclearDensityFunctions::ModifiedHarmonicOscillatorRP", "dd/d1a/classG4INCL_1_1NuclearDensityFunctions_1_1ModifiedHarmonicOscillatorRP.html", null ],
      [ "G4INCL::NuclearDensityFunctions::ParisP", "d9/dfe/classG4INCL_1_1NuclearDensityFunctions_1_1ParisP.html", null ],
      [ "G4INCL::NuclearDensityFunctions::ParisR", "d5/d21/classG4INCL_1_1NuclearDensityFunctions_1_1ParisR.html", null ],
      [ "G4INCL::NuclearDensityFunctions::WoodsSaxon", "dd/db9/classG4INCL_1_1NuclearDensityFunctions_1_1WoodsSaxon.html", null ],
      [ "G4INCL::NuclearDensityFunctions::WoodsSaxonRP", "d8/de7/classG4INCL_1_1NuclearDensityFunctions_1_1WoodsSaxonRP.html", null ],
      [ "G4INCL::RootFunctor", "dc/d13/classG4INCL_1_1RootFunctor.html", [
        [ "G4INCL::INCL::RecoilCMFunctor", "df/da7/classG4INCL_1_1INCL_1_1RecoilCMFunctor.html", null ],
        [ "G4INCL::INCL::RecoilFunctor", "db/d1a/classG4INCL_1_1INCL_1_1RecoilFunctor.html", null ],
        [ "G4INCL::InteractionAvatar::ViolationEEnergyFunctor", "dd/dfe/classG4INCL_1_1InteractionAvatar_1_1ViolationEEnergyFunctor.html", null ],
        [ "G4INCL::InteractionAvatar::ViolationEMomentumFunctor", "de/d20/classG4INCL_1_1InteractionAvatar_1_1ViolationEMomentumFunctor.html", null ]
      ] ]
    ] ],
    [ "tools::wroot::imutex", null, [
      [ "mutex", "d8/d99/classmutex.html", null ]
    ] ],
    [ "G4INCL::INCL", "d6/d0a/classG4INCL_1_1INCL.html", null ],
    [ "G4Voxel::Index", "d1/d7a/structG4Voxel_1_1Index.html", null ],
    [ "G4GMocrenFileSceneHandler::Index3D", "dc/d63/classG4GMocrenFileSceneHandler_1_1Index3D.html", null ],
    [ "PTL::mpl::impl::Index_tuple< Indexes >", "d6/db7/structPTL_1_1mpl_1_1impl_1_1Index__tuple.html", null ],
    [ "inflate_state", "d4/d4e/structinflate__state.html", null ],
    [ "INIT_ENCODING", "d1/d2b/structINIT__ENCODING.html", null ],
    [ "G4Octree< Iterator, Extractor, Point >::Node::InnerIterator", "d3/d1a/structG4Octree_1_1Node_1_1InnerIterator.html", null ],
    [ "G4TwistTubsHypeSide::Insidetype", "d9/d90/classG4TwistTubsHypeSide_1_1Insidetype.html", null ],
    [ "INT4< T1, I1, I2, I3, I4 >", "db/d89/classINT4.html", null ],
    [ "PTL::mpl::impl::integer_sequence< Tp, Idx >", "d0/d84/structPTL_1_1mpl_1_1impl_1_1integer__sequence.html", null ],
    [ "CLHEP::DualRand::IntegerCong", "d5/d1b/classCLHEP_1_1DualRand_1_1IntegerCong.html", null ],
    [ "internal_state", "d3/d8d/structinternal__state.html", null ],
    [ "G4INCL::InterpolationNode", "dd/d2e/classG4INCL_1_1InterpolationNode.html", null ],
    [ "G4InterpolationDriver< T >::InterpStepper", "d8/d31/structG4InterpolationDriver_1_1InterpStepper.html", null ],
    [ "G4INCL::Intersection", "db/d5f/structG4INCL_1_1Intersection.html", null ],
    [ "Intersection", "d6/d30/structIntersection.html", null ],
    [ "G4INCL::NuclearPotential::INuclearPotential", "d3/d84/classG4INCL_1_1NuclearPotential_1_1INuclearPotential.html", [
      [ "G4INCL::NuclearPotential::NuclearPotentialConstant", "d6/d96/classG4INCL_1_1NuclearPotential_1_1NuclearPotentialConstant.html", null ],
      [ "G4INCL::NuclearPotential::NuclearPotentialIsospin", "df/daf/classG4INCL_1_1NuclearPotential_1_1NuclearPotentialIsospin.html", [
        [ "G4INCL::NuclearPotential::NuclearPotentialEnergyIsospin", "df/d96/classG4INCL_1_1NuclearPotential_1_1NuclearPotentialEnergyIsospin.html", null ],
        [ "G4INCL::NuclearPotential::NuclearPotentialEnergyIsospinSmooth", "d8/d76/classG4INCL_1_1NuclearPotential_1_1NuclearPotentialEnergyIsospinSmooth.html", null ]
      ] ]
    ] ],
    [ "G4INCL::IPauli", "d3/dcb/classG4INCL_1_1IPauli.html", [
      [ "G4INCL::CDPP", "da/db5/classG4INCL_1_1CDPP.html", null ],
      [ "G4INCL::PauliGlobal", "db/d4c/classG4INCL_1_1PauliGlobal.html", null ],
      [ "G4INCL::PauliStandard", "dc/dc0/classG4INCL_1_1PauliStandard.html", null ],
      [ "G4INCL::PauliStrict", "df/d98/classG4INCL_1_1PauliStrict.html", null ],
      [ "G4INCL::PauliStrictStandard", "d7/dec/classG4INCL_1_1PauliStrictStandard.html", null ]
    ] ],
    [ "G4INCL::IPhaseSpaceGenerator", "d3/d8d/classG4INCL_1_1IPhaseSpaceGenerator.html", [
      [ "G4INCL::PhaseSpaceKopylov", "df/d7f/classG4INCL_1_1PhaseSpaceKopylov.html", null ],
      [ "G4INCL::PhaseSpaceRauboldLynch", "de/dbe/classG4INCL_1_1PhaseSpaceRauboldLynch.html", null ]
    ] ],
    [ "G4INCL::IPropagationModel", "d7/d66/classG4INCL_1_1IPropagationModel.html", [
      [ "G4INCL::StandardPropagationModel", "d4/d3f/classG4INCL_1_1StandardPropagationModel.html", null ]
    ] ],
    [ "G4INCL::IRandomGenerator", "d8/d02/classG4INCL_1_1IRandomGenerator.html", [
      [ "G4INCL::ConstantRandom", "dc/d0b/classG4INCL_1_1ConstantRandom.html", null ],
      [ "G4INCL::Geant4RandomGenerator", "dc/d0e/classG4INCL_1_1Geant4RandomGenerator.html", null ],
      [ "G4INCL::Ranecu", "df/d69/classG4INCL_1_1Ranecu.html", null ],
      [ "G4INCL::Ranecu3", "dd/d81/classG4INCL_1_1Ranecu3.html", null ]
    ] ],
    [ "CompileTimeConstraints::IsA< A, B >", "dd/dab/structCompileTimeConstraints_1_1IsA.html", null ],
    [ "PTL::IsEmpty< List >", "da/d82/classPTL_1_1IsEmpty.html", null ],
    [ "PTL::IsEmpty< Tuple<> >", "df/da2/classPTL_1_1IsEmpty_3_01Tuple_3_4_01_4.html", null ],
    [ "G4INCL::Isotope", "df/d95/structG4INCL_1_1Isotope.html", null ],
    [ "G4INCL::IsotopicDistribution", "d3/d45/classG4INCL_1_1IsotopicDistribution.html", null ],
    [ "Item", "d1/d83/structItem.html", null ],
    [ "PTL::mpl::impl::Itup_cat< Itup1, Itup2 >", "d7/dec/structPTL_1_1mpl_1_1impl_1_1Itup__cat.html", null ],
    [ "PTL::mpl::impl::Itup_cat< Build_index_tuple< NumT/2 >::__type, Build_index_tuple< NumT - NumT/2 >::__type >", "d7/dec/structPTL_1_1mpl_1_1impl_1_1Itup__cat.html", [
      [ "PTL::mpl::impl::Build_index_tuple< NumT >", "d1/d5c/structPTL_1_1mpl_1_1impl_1_1Build__index__tuple.html", null ]
    ] ],
    [ "PTL::mpl::impl::Itup_cat< Index_tuple< Ind1... >, Index_tuple< Ind2... > >", "de/d2d/structPTL_1_1mpl_1_1impl_1_1Itup__cat_3_01Index__tuple_3_01Ind1_8_8_8_01_4_00_01Index__tuple_3_01Ind2_8_8_8_01_4_01_4.html", null ],
    [ "PTL::JoinFunction< JoinT, JoinArg >", "d1/d50/structPTL_1_1JoinFunction.html", null ],
    [ "PTL::JoinFunction< Tp, Tp >", "d1/d50/structPTL_1_1JoinFunction.html", null ],
    [ "PTL::JoinFunction< void, JoinArg >", "d6/dc3/structPTL_1_1JoinFunction_3_01void_00_01JoinArg_01_4.html", null ],
    [ "PTL::JoinFunction< void, void >", "da/d99/structPTL_1_1JoinFunction_3_01void_00_01void_01_4.html", null ],
    [ "KDTR_parent", null, [
      [ "G4KDTreeResult", "d4/de0/classG4KDTreeResult.html", null ]
    ] ],
    [ "DNA::Penetration::Kreipl2009", "dd/d72/structDNA_1_1Penetration_1_1Kreipl2009.html", null ],
    [ "G4TwistedTubs::LastState", "d2/dd2/classG4TwistedTubs_1_1LastState.html", null ],
    [ "G4VTwistedFaceted::LastState", "d3/d70/classG4VTwistedFaceted_1_1LastState.html", null ],
    [ "G4TwistedTubs::LastValue", "dd/d3a/classG4TwistedTubs_1_1LastValue.html", null ],
    [ "G4VTwistedFaceted::LastValue", "dc/d54/classG4VTwistedFaceted_1_1LastValue.html", null ],
    [ "G4TwistedTubs::LastValueWithDoubleVector", "d9/d3e/classG4TwistedTubs_1_1LastValueWithDoubleVector.html", null ],
    [ "G4VTwistedFaceted::LastValueWithDoubleVector", "db/d44/classG4VTwistedFaceted_1_1LastValueWithDoubleVector.html", null ],
    [ "G4TwistedTubs::LastVector", "df/d3c/classG4TwistedTubs_1_1LastVector.html", null ],
    [ "G4VTwistedFaceted::LastVector", "de/dd3/classG4VTwistedFaceted_1_1LastVector.html", null ],
    [ "G4Octree< Iterator, Extractor, Point >::LeafValues", "de/d8e/structG4Octree_1_1LeafValues.html", null ],
    [ "lend_target", "de/d13/structlend__target.html", null ],
    [ "G4ExtrudedSolid::line", "d5/dc6/structG4ExtrudedSolid_1_1line.html", null ],
    [ "G4VisCommandSceneAddLine::Line", "d4/d2d/structG4VisCommandSceneAddLine_1_1Line.html", null ],
    [ "G4VisCommandSceneAddLine2D::Line2D", "dd/d1a/structG4VisCommandSceneAddLine2D_1_1Line2D.html", null ],
    [ "G4VisCommandSceneAddLogo2D::Logo2D", "d3/ddd/structG4VisCommandSceneAddLogo2D_1_1Logo2D.html", null ],
    [ "G4eBremsstrahlungRelModel::LPMFuncs", "d2/db6/structG4eBremsstrahlungRelModel_1_1LPMFuncs.html", null ],
    [ "G4PairProductionRelModel::LPMFuncs", "dc/dd4/structG4PairProductionRelModel_1_1LPMFuncs.html", null ],
    [ "PTL::mpl::impl::Make_integer_sequence< Tp, NumT, ISeq >", "de/d02/structPTL_1_1mpl_1_1impl_1_1Make__integer__sequence.html", null ],
    [ "PTL::mpl::impl::Make_integer_sequence< Tp, NumT, Index_tuple< Idx... > >", "d1/def/structPTL_1_1mpl_1_1impl_1_1Make__integer__sequence_3_01Tp_00_01NumT_00_01Index__tuple_3_01Idx_8_8_8_01_4_01_4.html", null ],
    [ "G4GeneralNNCollision::MakeNNStarToNN< channelType, Np, Nn >", "d9/dc8/structG4GeneralNNCollision_1_1MakeNNStarToNN.html", null ],
    [ "G4GeneralNNCollision::MakeNNToDeltaDelta< dm, d0, dp, dpp, channelType >", "d5/d93/structG4GeneralNNCollision_1_1MakeNNToDeltaDelta.html", null ],
    [ "G4GeneralNNCollision::MakeNNToDeltaNstar< Np, channelType, Nn >", "d6/d6d/structG4GeneralNNCollision_1_1MakeNNToDeltaNstar.html", null ],
    [ "G4GeneralNNCollision::MakeNNToNDelta< dm, d0, dp, dpp, channelType >", "d4/dcc/structG4GeneralNNCollision_1_1MakeNNToNDelta.html", null ],
    [ "G4GeneralNNCollision::MakeNNToNNStar< Np, Nn, channelType >", "d2/d67/structG4GeneralNNCollision_1_1MakeNNToNNStar.html", null ],
    [ "std::map", null, [
      [ "G4ParallelWorldProcessStore", "d0/dcf/classG4ParallelWorldProcessStore.html", null ]
    ] ],
    [ "G4PhysicalVolumesSearchScene::Matcher", "d9/d40/classG4PhysicalVolumesSearchScene_1_1Matcher.html", null ],
    [ "MCGIDI_angular_s", "d1/daa/structMCGIDI__angular__s.html", null ],
    [ "MCGIDI_angularEnergy_s", "d6/de3/structMCGIDI__angularEnergy__s.html", null ],
    [ "MCGIDI_decaySamplingInfo_s", "d2/d63/structMCGIDI__decaySamplingInfo__s.html", null ],
    [ "MCGIDI_distribution_s", "d4/d72/structMCGIDI__distribution__s.html", null ],
    [ "MCGIDI_energy_s", "d7/d7b/structMCGIDI__energy__s.html", null ],
    [ "MCGIDI_energyAngular_s", "df/dc4/structMCGIDI__energyAngular__s.html", null ],
    [ "MCGIDI_energyNBodyPhaseSpace_s", "d1/d53/structMCGIDI__energyNBodyPhaseSpace__s.html", null ],
    [ "MCGIDI_energyWeightedFunctional_s", "d7/d84/structMCGIDI__energyWeightedFunctional__s.html", null ],
    [ "MCGIDI_energyWeightedFunctionals_s", "df/d7c/structMCGIDI__energyWeightedFunctionals__s.html", null ],
    [ "MCGIDI_GammaBranching_s", "de/dc1/structMCGIDI__GammaBranching__s.html", null ],
    [ "MCGIDI_KalbachMann_ras_s", "d5/d0d/structMCGIDI__KalbachMann__ras__s.html", null ],
    [ "MCGIDI_KalbachMann_s", "dc/d95/structMCGIDI__KalbachMann__s.html", null ],
    [ "MCGIDI_map_s", "d3/d47/structMCGIDI__map__s.html", null ],
    [ "MCGIDI_map_smr_s", "d6/d06/structMCGIDI__map__smr__s.html", null ],
    [ "MCGIDI_mapEntry_s", "d6/dd6/structMCGIDI__mapEntry__s.html", null ],
    [ "MCGIDI_outputChannel_s", "d4/d47/structMCGIDI__outputChannel__s.html", null ],
    [ "MCGIDI_particle_s", "d4/d6d/structMCGIDI__particle__s.html", null ],
    [ "MCGIDI_pdfOfX_s", "da/dc4/structMCGIDI__pdfOfX__s.html", null ],
    [ "MCGIDI_pdfsOfXGivenW_s", "da/d0f/structMCGIDI__pdfsOfXGivenW__s.html", null ],
    [ "MCGIDI_pdfsOfXGivenW_sampled_s", "d7/ded/structMCGIDI__pdfsOfXGivenW__sampled__s.html", null ],
    [ "MCGIDI_POP_s", "d7/dc7/structMCGIDI__POP__s.html", null ],
    [ "MCGIDI_POPs_s", "d0/d56/structMCGIDI__POPs__s.html", null ],
    [ "MCGIDI_product_s", "d5/dc0/structMCGIDI__product__s.html", null ],
    [ "MCGIDI_productInfo_s", "d5/d6c/structMCGIDI__productInfo__s.html", null ],
    [ "MCGIDI_productsInfo_s", "dd/da3/structMCGIDI__productsInfo__s.html", null ],
    [ "MCGIDI_quantitiesLookupModes", "d9/d57/classMCGIDI__quantitiesLookupModes.html", null ],
    [ "MCGIDI_reaction_s", "d7/d07/structMCGIDI__reaction__s.html", null ],
    [ "MCGIDI_sampledProductsData_s", "d1/dfd/structMCGIDI__sampledProductsData__s.html", null ],
    [ "MCGIDI_sampledProductsDatas_s", "d7/d73/structMCGIDI__sampledProductsDatas__s.html", null ],
    [ "MCGIDI_samplingMethods", "d2/da3/classMCGIDI__samplingMethods.html", null ],
    [ "MCGIDI_samplingMultiplicityBias_s", "dd/d7f/structMCGIDI__samplingMultiplicityBias__s.html", null ],
    [ "MCGIDI_samplingSettings", "d4/db9/classMCGIDI__samplingSettings.html", null ],
    [ "MCGIDI_target_heated_info_s", "de/d51/structMCGIDI__target__heated__info__s.html", null ],
    [ "MCGIDI_target_heated_s", "d4/d04/structMCGIDI__target__heated__s.html", null ],
    [ "MCGIDI_target_s", "d9/dc6/structMCGIDI__target__s.html", null ],
    [ "DNA::Penetration::Meesungnoen2002", "d9/d41/structDNA_1_1Penetration_1_1Meesungnoen2002.html", null ],
    [ "DNA::Penetration::Meesungnoen2002_amorphous", "d5/d93/structDNA_1_1Penetration_1_1Meesungnoen2002__amorphous.html", null ],
    [ "G4MemStat::MemStat", "df/d26/structG4MemStat_1_1MemStat.html", null ],
    [ "G4Scene::Model", "d2/d88/structG4Scene_1_1Model.html", null ],
    [ "G4ITModelManager::ModelInfo", "d1/df2/structG4ITModelManager_1_1ModelInfo.html", null ],
    [ "G4UrbanMscModel::mscData", "d4/d10/structG4UrbanMscModel_1_1mscData.html", null ],
    [ "MyGamma", "de/d0c/classMyGamma.html", null ],
    [ "NAMED", "da/d14/structNAMED.html", null ],
    [ "PTL::api::native", "de/d76/structPTL_1_1api_1_1native.html", null ],
    [ "G4INCL::NaturalIsotopicDistributions", "d5/d24/classG4INCL_1_1NaturalIsotopicDistributions.html", null ],
    [ "nf_GnG_adaptiveQuadrature_info_s", "d2/dff/structnf__GnG__adaptiveQuadrature__info__s.html", null ],
    [ "nf_Legendre_from_ptwXY_callback_s", "d0/dfb/structnf__Legendre__from__ptwXY__callback__s.html", null ],
    [ "nf_Legendre_GaussianQuadrature_degree", "da/d76/structnf__Legendre__GaussianQuadrature__degree.html", null ],
    [ "nf_Legendre_s", "dd/d76/structnf__Legendre__s.html", null ],
    [ "G4Octree< Iterator, Extractor, Point >::Node", "db/de0/classG4Octree_1_1Node.html", null ],
    [ "CLHEP::noncopyable", "d9/dca/classCLHEP_1_1noncopyable.html", null ],
    [ "normal_encoding", "d1/d89/structnormal__encoding.html", null ],
    [ "NS_ATT", "df/df7/structNS__ATT.html", null ],
    [ "G4INCL::NuclearDensity", "d9/d25/classG4INCL_1_1NuclearDensity.html", null ],
    [ "G4eDPWAElasticDCS::OneSamplingTable", "d0/dcb/structG4eDPWAElasticDCS_1_1OneSamplingTable.html", null ],
    [ "OneSamplingTable", "d8/d24/structOneSamplingTable.html", null ],
    [ "open_internal_entity", "d7/da1/structopen__internal__entity.html", null ],
    [ "G4InuclSpecialFunctions::paraMaker", "d7/dd9/classG4InuclSpecialFunctions_1_1paraMaker.html", null ],
    [ "G4GDMLParameterisation::PARAMETER", "d9/d18/structG4GDMLParameterisation_1_1PARAMETER.html", null ],
    [ "G4SingleParticleSource::part_prop_t", "d5/dd7/structG4SingleParticleSource_1_1part__prop__t.html", null ],
    [ "G4INCL::Particle", "dd/d83/classG4INCL_1_1Particle.html", [
      [ "G4INCL::Cluster", "d0/d7f/classG4INCL_1_1Cluster.html", [
        [ "G4INCL::Nucleus", "d7/db0/classG4INCL_1_1Nucleus.html", null ],
        [ "G4INCL::ProjectileRemnant", "db/dcb/classG4INCL_1_1ProjectileRemnant.html", null ]
      ] ]
    ] ],
    [ "G4INCL::ParticleSampler", "d4/d0c/classG4INCL_1_1ParticleSampler.html", null ],
    [ "G4INCL::ParticleSpecies", "dc/dee/classG4INCL_1_1ParticleSpecies.html", null ],
    [ "G4RayTracerSceneHandler::PathLessThan", "d5/d0c/structG4RayTracerSceneHandler_1_1PathLessThan.html", null ],
    [ "PTL::Singleton< Type, PointerT >::persistent_data", "da/d91/structPTL_1_1Singleton_1_1persistent__data.html", null ],
    [ "G4ProfilerConfig< Category >::PersistentSettings< Idx >", "d6/d26/structG4ProfilerConfig_1_1PersistentSettings.html", null ],
    [ "Pixel", "d2/d90/structPixel.html", null ],
    [ "G4AnyMethod::Placeholder", "d8/d64/classG4AnyMethod_1_1Placeholder.html", [
      [ "G4AnyMethod::FuncRef< S, T >", "dd/ded/structG4AnyMethod_1_1FuncRef.html", null ],
      [ "G4AnyMethod::FuncRef1< S, T, A0 >", "dd/d29/structG4AnyMethod_1_1FuncRef1.html", null ],
      [ "G4AnyMethod::FuncRef2< S, T, A0, A1 >", "d7/dd0/structG4AnyMethod_1_1FuncRef2.html", null ]
    ] ],
    [ "G4AnyType::Placeholder", "db/d46/classG4AnyType_1_1Placeholder.html", [
      [ "G4AnyType::Ref< ValueType >", "dd/d16/classG4AnyType_1_1Ref.html", null ]
    ] ],
    [ "G4ExtrudedSolid::plane", "db/d45/structG4ExtrudedSolid_1_1plane.html", null ],
    [ "HepGeom::Plane3D< T >", "de/dee/classHepGeom_1_1Plane3D.html", null ],
    [ "HepGeom::Plane3D< G4double >", "de/dee/classHepGeom_1_1Plane3D.html", [
      [ "G4ErrorPlaneSurfaceTarget", "df/de6/classG4ErrorPlaneSurfaceTarget.html", null ]
    ] ],
    [ "G4OpenGLStoredSceneHandler::PO", "da/d17/structG4OpenGLStoredSceneHandler_1_1PO.html", null ],
    [ "polynomialCallbackArgs_s", "d0/df8/structpolynomialCallbackArgs__s.html", null ],
    [ "PoP_s", "d7/ddd/structPoP__s.html", null ],
    [ "PoPDatas", "d1/da2/structPoPDatas.html", null ],
    [ "PTL::PopFrontT< List >", "d5/dd8/classPTL_1_1PopFrontT.html", null ],
    [ "PTL::PopFrontT< std::tuple< Head, Tail... > >", "db/d4a/classPTL_1_1PopFrontT_3_01std_1_1tuple_3_01Head_00_01Tail_8_8_8_01_4_01_4.html", null ],
    [ "PoPs_s", "d9/dad/structPoPs__s.html", null ],
    [ "position", "d3/d90/structposition.html", null ],
    [ "prefix", "db/dd2/structprefix.html", null ],
    [ "ProbabilityBranch", "d7/d04/structProbabilityBranch.html", null ],
    [ "ProbabilityTree", "d8/db4/structProbabilityTree.html", null ],
    [ "ProcessGeneralInfo", "dc/d59/structProcessGeneralInfo.html", null ],
    [ "g4tim::ProfilerArgparser", "d6/d60/structg4tim_1_1ProfilerArgparser.html", null ],
    [ "prolog_state", "d0/d65/structprolog__state.html", null ],
    [ "ptwXPoints_s", "d4/d25/structptwXPoints__s.html", null ],
    [ "ptwXY_integrateWithFunctionInfo_s", "d5/d8e/structptwXY__integrateWithFunctionInfo__s.html", null ],
    [ "ptwXY_interpolationOtherInfo", "d7/d47/structptwXY__interpolationOtherInfo.html", null ],
    [ "ptwXYOverflowPoint_s", "d7/df0/structptwXYOverflowPoint__s.html", null ],
    [ "ptwXYPoint_s", "d1/df1/structptwXYPoint__s.html", null ],
    [ "ptwXYPoints_s", "de/dbf/structptwXYPoints__s.html", null ],
    [ "PTL::PushBackT< List, NewElement >", "d2/d12/classPTL_1_1PushBackT.html", null ],
    [ "PTL::PushBackT< Tuple< Elements... >, NewElement >", "d3/dcc/classPTL_1_1PushBackT_3_01Tuple_3_01Elements_8_8_8_01_4_00_01NewElement_01_4.html", null ],
    [ "PTL::PushFrontT< List, Element >", "d9/da5/classPTL_1_1PushFrontT.html", null ],
    [ "PTL::PushFrontT< std::tuple< Types... >, Element >", "d8/de3/classPTL_1_1PushFrontT_3_01std_1_1tuple_3_01Types_8_8_8_01_4_00_01Element_01_4.html", null ],
    [ "PTL::PushFrontT< TransformT< PopFront< List >, MetaFun >::Type, MetaFun< Front< List > >::Type >", "d9/da5/classPTL_1_1PushFrontT.html", [
      [ "PTL::TransformT< List, MetaFun, false >", "d3/d66/classPTL_1_1TransformT_3_01List_00_01MetaFun_00_01false_01_4.html", null ]
    ] ],
    [ "G4ModelingParameters::PVNameCopyNo", "d8/dc6/classG4ModelingParameters_1_1PVNameCopyNo.html", null ],
    [ "G4ModelingParameters::PVPointerCopyNo", "dd/d70/classG4ModelingParameters_1_1PVPointerCopyNo.html", null ],
    [ "QDialog", null, [
      [ "G4OpenGLQtExportDialog", "d3/da2/classG4OpenGLQtExportDialog.html", null ],
      [ "G4OpenGLQtMovieDialog", "d0/deb/classG4OpenGLQtMovieDialog.html", null ]
    ] ],
    [ "QDockWidget", null, [
      [ "G4UIDockWidget", "d5/ddd/classG4UIDockWidget.html", null ]
    ] ],
    [ "Qt3DCore::QEntity", null, [
      [ "G4Qt3DQEntity", "d1/d32/classG4Qt3DQEntity.html", null ]
    ] ],
    [ "QGLWidget", null, [
      [ "G4OpenGLImmediateQtViewer", "dd/d83/classG4OpenGLImmediateQtViewer.html", null ],
      [ "G4OpenGLStoredQtViewer", "d5/d99/classG4OpenGLStoredQtViewer.html", null ]
    ] ],
    [ "QObject", null, [
      [ "G4OpenGLQtViewer", "d2/d07/classG4OpenGLQtViewer.html", null ],
      [ "G4OpenInventorQtExaminerViewer", "d3/d82/classG4OpenInventorQtExaminerViewer.html", null ],
      [ "G4OpenInventorQtViewer", "dd/ddf/classG4OpenInventorQtViewer.html", null ],
      [ "G4ToolsSGQtDestroyCallback", "d6/d95/classG4ToolsSGQtDestroyCallback.html", null ],
      [ "G4UIQt", "d6/d5b/classG4UIQt.html", null ]
    ] ],
    [ "Qt3DExtras::Qt3DWindow", null, [
      [ "G4Qt3DViewer", "db/df7/classG4Qt3DViewer.html", null ]
    ] ],
    [ "QTabWidget", null, [
      [ "G4QTabWidget", "de/d45/classG4QTabWidget.html", null ]
    ] ],
    [ "QVTKOpenGLNativeWidget", null, [
      [ "G4VtkQtViewer", "db/d1b/classG4VtkQtViewer.html", null ]
    ] ],
    [ "G4DNAElectronHoleRecombination::ReactantInfo", "de/d73/structG4DNAElectronHoleRecombination_1_1ReactantInfo.html", null ],
    [ "ReactionProduct4Mom", "d9/d46/structReactionProduct4Mom.html", null ],
    [ "G4Allocator< Type >::rebind< U >", "d7/d5b/structG4Allocator_1_1rebind.html", null ],
    [ "G4EnhancedVecAllocator< _Tp >::rebind< _Tp1 >", "d7/d7c/structG4EnhancedVecAllocator_1_1rebind.html", null ],
    [ "G4CollisionComposite::Register", "d7/df0/structG4CollisionComposite_1_1Register.html", null ],
    [ "G4Scatterer::Register", "df/d8a/structG4Scatterer_1_1Register.html", null ],
    [ "ResNode", "d3/d72/structResNode.html", null ],
    [ "G4CollisionComposite::Resolve", "de/db0/structG4CollisionComposite_1_1Resolve.html", null ],
    [ "DNA::Penetration::Ritchie1994", "d8/d3f/structDNA_1_1Penetration_1_1Ritchie1994.html", null ],
    [ "CLHEP::MixMaxRng::rng_state_st", "db/d6c/structCLHEP_1_1MixMaxRng_1_1rng__state__st.html", null ],
    [ "G4GDMLReadSolids::rzPointType", "dc/da3/structG4GDMLReadSolids_1_1rzPointType.html", null ],
    [ "SAMP", null, [
      [ "G4CascadeFunctions< DATA, SAMP >", "df/d30/classG4CascadeFunctions.html", null ]
    ] ],
    [ "G4SBBremTable::SamplingTablePerZ", "d6/d6d/structG4SBBremTable_1_1SamplingTablePerZ.html", null ],
    [ "SbPainter", "de/de2/classSbPainter.html", [
      [ "SbPainterPS", "df/dc2/classSbPainterPS.html", null ]
    ] ],
    [ "G4VisCommandSceneAddScale::Scale", "df/d5a/structG4VisCommandSceneAddScale_1_1Scale.html", null ],
    [ "G4OpenInventorQtExaminerViewer::sceneElement", "df/df2/structG4OpenInventorQtExaminerViewer_1_1sceneElement.html", null ],
    [ "G4OpenInventorXtExaminerViewer::sceneElement", "d1/d66/structG4OpenInventorXtExaminerViewer_1_1sceneElement.html", null ],
    [ "PTL::ScopeDestructor", "d6/d54/structPTL_1_1ScopeDestructor.html", null ],
    [ "G4eDPWAElasticDCS::SCPCorrection", "d6/d2c/structG4eDPWAElasticDCS_1_1SCPCorrection.html", null ],
    [ "G4GoudsmitSaundersonTable::SCPCorrection", "d3/d3f/structG4GoudsmitSaundersonTable_1_1SCPCorrection.html", null ],
    [ "G4DNAScavengerMaterial::Search", "dd/da2/structG4DNAScavengerMaterial_1_1Search.html", null ],
    [ "G4MoleculeCounter::Search", "d0/ddf/structG4MoleculeCounter_1_1Search.html", null ],
    [ "SelectFromKTV", "df/df5/classSelectFromKTV.html", null ],
    [ "tools::sg::separator", null, [
      [ "G4ToolsSGNode", "d4/daa/classG4ToolsSGNode.html", null ]
    ] ],
    [ "tools::Xt::session", null, [
      [ "session", "dd/dbb/classsession.html", null ]
    ] ],
    [ "G4PolyhedraSide::sG4PolyhedraSideEdge", "d2/def/structG4PolyhedraSide_1_1sG4PolyhedraSideEdge.html", null ],
    [ "G4PolyhedraSide::sG4PolyhedraSideVec", "d4/d79/structG4PolyhedraSide_1_1sG4PolyhedraSideVec.html", null ],
    [ "PTL::Singleton< Type, PointerT >", "de/d0c/classPTL_1_1Singleton.html", null ],
    [ "PTL::SmallerThanT< Tp, Up >", "db/d52/structPTL_1_1SmallerThanT.html", null ],
    [ "SoAction", null, [
      [ "SoAlternateRepAction", "db/db4/classSoAlternateRepAction.html", null ],
      [ "SoCounterAction", "d1/d4e/classSoCounterAction.html", null ]
    ] ],
    [ "SoBaseKit", null, [
      [ "SoDetectorTreeKit", "d3/dca/classSoDetectorTreeKit.html", null ]
    ] ],
    [ "SoGLRenderAction", null, [
      [ "SoGL2PSAction", "da/d63/classSoGL2PSAction.html", null ]
    ] ],
    [ "SoGroup", null, [
      [ "SoStyleCache", "dc/d6f/classSoStyleCache.html", null ]
    ] ],
    [ "SoLineSet", null, [
      [ "SoG4LineSet", "d2/daa/classSoG4LineSet.html", null ]
    ] ],
    [ "G4INCL::RootFinder::Solution", "db/d22/classG4INCL_1_1RootFinder_1_1Solution.html", null ],
    [ "SoNode", null, [
      [ "SoImageWriter", "dc/d67/classSoImageWriter.html", null ]
    ] ],
    [ "SoPointSet", null, [
      [ "HEPVis_SoMarkerSet", "d5/d9d/classHEPVis__SoMarkerSet.html", [
        [ "SoG4MarkerSet", "dc/dd5/classSoG4MarkerSet.html", null ]
      ] ]
    ] ],
    [ "SoQtExaminerViewer", null, [
      [ "G4OpenInventorQtExaminerViewer", "d3/d82/classG4OpenInventorQtExaminerViewer.html", null ]
    ] ],
    [ "G4INCL::ClusteringModelIntercomparison::SortedNucleonConfiguration", "d2/dac/classG4INCL_1_1ClusteringModelIntercomparison_1_1SortedNucleonConfiguration.html", null ],
    [ "__1DSortOut::sortOutNDim", "dd/d57/struct____1DSortOut_1_1sortOutNDim.html", null ],
    [ "sortWatcher< OBJECT >", "d8/db1/structsortWatcher.html", null ],
    [ "SoShape", null, [
      [ "Geant4_SoPolyhedron", "dc/d76/classGeant4__SoPolyhedron.html", [
        [ "SoG4Polyhedron", "de/db2/classSoG4Polyhedron.html", null ]
      ] ],
      [ "SoBox", "d8/db2/classSoBox.html", null ],
      [ "SoCons", "d9/d67/classSoCons.html", null ],
      [ "SoTrap", "dd/d4c/classSoTrap.html", null ],
      [ "SoTrd", "d7/d4f/classSoTrd.html", null ],
      [ "SoTubs", "d6/de5/classSoTubs.html", null ]
    ] ],
    [ "SoWinExaminerViewer", null, [
      [ "Geant4_SoWinExaminerViewer", "d7/d15/classGeant4__SoWinExaminerViewer.html", null ]
    ] ],
    [ "SoXtExaminerViewer", null, [
      [ "G4OpenInventorXtExaminerViewer", "d0/d3c/classG4OpenInventorXtExaminerViewer.html", null ]
    ] ],
    [ "SoXtInternal", "db/d95/classSoXtInternal.html", null ],
    [ "G4SBBremTable::STable", "d3/d9d/structG4SBBremTable_1_1STable.html", null ],
    [ "G4ITSafetyHelper::State", "d7/d9c/classG4ITSafetyHelper_1_1State.html", null ],
    [ "static_tree_desc_s", "d1/d0f/structstatic__tree__desc__s.html", null ],
    [ "CLHEP::StaticRandomStates", "d5/d3b/classCLHEP_1_1StaticRandomStates.html", null ],
    [ "statusMessageReport", "d9/d53/structstatusMessageReport.html", null ],
    [ "statusMessageReporting", "da/d6f/structstatusMessageReporting.html", null ],
    [ "GMocrenTrack::Step", "d2/da0/structGMocrenTrack_1_1Step.html", null ],
    [ "G4INCL::Store", "d5/dd3/classG4INCL_1_1Store.html", null ],
    [ "G4SBBremTable::STPoint", "d1/d69/structG4SBBremTable_1_1STPoint.html", null ],
    [ "std::string", null, [
      [ "G4String", "d5/d72/classG4String.html", null ]
    ] ],
    [ "STRING_POOL", "d1/ddb/structSTRING__POOL.html", null ],
    [ "Struct", "dc/ddd/structStruct.html", null ],
    [ "G4GenericPolycone::surface_element", "dd/d1c/structG4GenericPolycone_1_1surface__element.html", null ],
    [ "G4Polycone::surface_element", "d3/df5/structG4Polycone_1_1surface__element.html", null ],
    [ "G4Polyhedra::surface_element", "dc/d69/structG4Polyhedra_1_1surface__element.html", null ],
    [ "T", null, [
      [ "TG4GenericPhysicsList< T >", "db/dc8/classTG4GenericPhysicsList.html", null ],
      [ "TINCLXXPhysicsListHelper< T, withNeutronHP, withFTFP >", "d2/db7/classTINCLXXPhysicsListHelper.html", null ]
    ] ],
    [ "tag", "d1/d09/structtag.html", null ],
    [ "TAG_NAME", "d0/d4c/structTAG__NAME.html", null ],
    [ "PTL::tbb::task_arena", "d9/d90/classPTL_1_1tbb_1_1task__arena.html", null ],
    [ "PTL::tbb::task_group", "dc/df7/classPTL_1_1tbb_1_1task__group.html", null ],
    [ "PTL::TaskGroup< Tp, Arg, MaxDepth >", "dc/df5/classPTL_1_1TaskGroup.html", null ],
    [ "PTL::TaskManager", "dc/d86/classPTL_1_1TaskManager.html", null ],
    [ "PTL::TaskRunManager", "da/d47/classPTL_1_1TaskRunManager.html", [
      [ "G4TaskRunManager", "db/d9e/classG4TaskRunManager.html", null ]
    ] ],
    [ "G4Traits::TaskSingletonKey< T >", "da/d7d/structG4Traits_1_1TaskSingletonKey.html", null ],
    [ "CLHEP::DualRand::Tausworthe", "de/df7/classCLHEP_1_1DualRand_1_1Tausworthe.html", null ],
    [ "PTL::api::tbb", "d4/d05/structPTL_1_1api_1_1tbb.html", null ],
    [ "CLHEP::Tcomponent", "d2/d3b/classCLHEP_1_1Tcomponent.html", null ],
    [ "DNA::Penetration::Terrisol1990", "d2/dec/structDNA_1_1Penetration_1_1Terrisol1990.html", null ],
    [ "G4SPSPosDistribution::thread_data_t", "d8/d50/structG4SPSPosDistribution_1_1thread__data__t.html", null ],
    [ "PTL::ThreadData", "da/daa/classPTL_1_1ThreadData.html", null ],
    [ "G4SPSEneDistribution::threadLocal_t", "da/d9e/structG4SPSEneDistribution_1_1threadLocal__t.html", null ],
    [ "G4DNAChemistryManager::ThreadLocalData", "dd/db4/structG4DNAChemistryManager_1_1ThreadLocalData.html", null ],
    [ "PTL::ThreadPool", "d3/d19/classPTL_1_1ThreadPool.html", null ],
    [ "G4INCL::ThreeVector", "d1/dc2/classG4INCL_1_1ThreeVector.html", null ],
    [ "G4::MoleculeCounter::TimePrecision", "d4/d04/structG4_1_1MoleculeCounter_1_1TimePrecision.html", null ],
    [ "G4OpenGLStoredSceneHandler::TO", "de/db4/structG4OpenGLStoredSceneHandler_1_1TO.html", null ],
    [ "G4ParticleHPAngular::toBeCached", "dc/d25/structG4ParticleHPAngular_1_1toBeCached.html", null ],
    [ "G4ParticleHPContAngularPar::toBeCached", "d8/d3a/structG4ParticleHPContAngularPar_1_1toBeCached.html", null ],
    [ "G4ParticleHPEnAngCorrelation::toBeCached", "d3/d26/structG4ParticleHPEnAngCorrelation_1_1toBeCached.html", null ],
    [ "G4ParticleHPFissionBaseFS::toBeCached", "d8/d19/structG4ParticleHPFissionBaseFS_1_1toBeCached.html", null ],
    [ "G4ParticleHPFSFissionFS::toBeCached", "d9/d63/structG4ParticleHPFSFissionFS_1_1toBeCached.html", null ],
    [ "G4ParticleHPProduct::toBeCached", "d2/dbd/structG4ParticleHPProduct_1_1toBeCached.html", null ],
    [ "G4VParticleHPEnergyAngular::toBeCached", "d0/d24/structG4VParticleHPEnergyAngular_1_1toBeCached.html", null ],
    [ "tools_gl2ps_gl_funcs_t", "da/d58/structtools__gl2ps__gl__funcs__t.html", null ],
    [ "tools_GL2PSvertex", "d3/dd3/structtools__GL2PSvertex.html", null ],
    [ "G4PhysicalVolumeModel::TouchableProperties", "d2/d33/structG4PhysicalVolumeModel_1_1TouchableProperties.html", null ],
    [ "Tp", null, [
      [ "PTL::TupleElt< Height, Tp, true >", "dd/db7/classPTL_1_1TupleElt_3_01Height_00_01Tp_00_01true_01_4.html", null ]
    ] ],
    [ "HepGeom::Transform3D", "dd/de6/classHepGeom_1_1Transform3D.html", [
      [ "G4OpenInventorTransform3D", "da/d3f/classG4OpenInventorTransform3D.html", null ],
      [ "HepGeom::Reflect3D", "db/d5b/classHepGeom_1_1Reflect3D.html", [
        [ "HepGeom::ReflectX3D", "d2/d93/classHepGeom_1_1ReflectX3D.html", null ],
        [ "HepGeom::ReflectY3D", "d4/da7/classHepGeom_1_1ReflectY3D.html", null ],
        [ "HepGeom::ReflectZ3D", "db/d8c/classHepGeom_1_1ReflectZ3D.html", null ]
      ] ],
      [ "HepGeom::Rotate3D", "d9/da8/classHepGeom_1_1Rotate3D.html", [
        [ "HepGeom::RotateX3D", "d8/dc0/classHepGeom_1_1RotateX3D.html", null ],
        [ "HepGeom::RotateY3D", "d6/d96/classHepGeom_1_1RotateY3D.html", null ],
        [ "HepGeom::RotateZ3D", "d8/d65/classHepGeom_1_1RotateZ3D.html", null ]
      ] ],
      [ "HepGeom::Scale3D", "da/d27/classHepGeom_1_1Scale3D.html", [
        [ "HepGeom::ScaleX3D", "d4/d9b/classHepGeom_1_1ScaleX3D.html", null ],
        [ "HepGeom::ScaleY3D", "d9/d76/classHepGeom_1_1ScaleY3D.html", null ],
        [ "HepGeom::ScaleZ3D", "d0/de1/classHepGeom_1_1ScaleZ3D.html", null ]
      ] ],
      [ "HepGeom::Translate3D", "d1/d22/classHepGeom_1_1Translate3D.html", [
        [ "HepGeom::TranslateX3D", "dd/dab/classHepGeom_1_1TranslateX3D.html", null ],
        [ "HepGeom::TranslateY3D", "df/d09/classHepGeom_1_1TranslateY3D.html", null ],
        [ "HepGeom::TranslateZ3D", "d3/d67/classHepGeom_1_1TranslateZ3D.html", null ]
      ] ]
    ] ],
    [ "HepGeom::Transform3D::Transform3D_row", "dd/deb/classHepGeom_1_1Transform3D_1_1Transform3D__row.html", null ],
    [ "PTL::transform_tuple< Head, Tail >", "db/dad/structPTL_1_1transform__tuple.html", null ],
    [ "PTL::transform_tuple< Head >", "db/d06/structPTL_1_1transform__tuple_3_01Head_01_4.html", null ],
    [ "PTL::TransformT< List, MetaFun, Empty >", "dd/d3e/classPTL_1_1TransformT.html", null ],
    [ "PTL::TransformT< List, MetaFun, true >", "d6/d69/classPTL_1_1TransformT_3_01List_00_01MetaFun_00_01true_01_4.html", null ],
    [ "PTL::TransformT< Tuple< Elements... >, MetaFun, false >", "d3/d2c/classPTL_1_1TransformT_3_01Tuple_3_01Elements_8_8_8_01_4_00_01MetaFun_00_01false_01_4.html", null ],
    [ "TrapSidePlane", "d9/d68/structTrapSidePlane.html", null ],
    [ "tree_desc_s", "dc/d2c/structtree__desc__s.html", null ],
    [ "PTL::Tuple< Types >", "dd/d4f/classPTL_1_1Tuple.html", null ],
    [ "PTL::Tuple< Tail... >", "dd/d4f/classPTL_1_1Tuple.html", [
      [ "PTL::Tuple< Head, Tail... >", "d2/d6a/classPTL_1_1Tuple_3_01Head_00_01Tail_8_8_8_01_4.html", null ]
    ] ],
    [ "PTL::Tuple<>", "da/d63/classPTL_1_1Tuple_3_4.html", null ],
    [ "PTL::tuple_subset< Args >", "d9/dd5/structPTL_1_1tuple__subset.html", null ],
    [ "PTL::TupleElt< Height, Tp, bool >", "db/d98/classPTL_1_1TupleElt.html", null ],
    [ "PTL::TupleElt< Height, Tp, false >", "d9/ddd/classPTL_1_1TupleElt_3_01Height_00_01Tp_00_01false_01_4.html", null ],
    [ "PTL::TupleElt< sizeof...(Tail), Head >", "db/d98/classPTL_1_1TupleElt.html", [
      [ "PTL::Tuple< Head, Tail... >", "d2/d6a/classPTL_1_1Tuple_3_01Head_00_01Tail_8_8_8_01_4.html", null ]
    ] ],
    [ "type_wrapper< T >", "d5/d91/structtype__wrapper.html", null ],
    [ "Ui_Dialog", "d7/d7b/classUi__Dialog.html", [
      [ "Ui::Dialog", "df/d9e/classUi_1_1Dialog.html", null ]
    ] ],
    [ "std::unique_lock", null, [
      [ "G4TemplateAutoLock< _Mutex_t >", "d7/d27/classG4TemplateAutoLock.html", null ],
      [ "PTL::TemplateAutoLock< MutexT >", "de/d47/classPTL_1_1TemplateAutoLock.html", null ]
    ] ],
    [ "unitConversions_s", "d8/d1a/structunitConversions__s.html", null ],
    [ "unitsDB_s", "dc/d01/structunitsDB__s.html", null ],
    [ "unknown_encoding", "de/dac/structunknown__encoding.html", null ],
    [ "UReadBinaryString", "da/d40/classUReadBinaryString.html", null ],
    [ "g4tim::user_bundle< Idx, Tp >", "de/de8/structg4tim_1_1user__bundle.html", null ],
    [ "G4VisManager::UserVisAction", "dc/d47/structG4VisManager_1_1UserVisAction.html", null ],
    [ "G4DNAIndependentReactionTimeStepper::Utils", "d9/daa/classG4DNAIndependentReactionTimeStepper_1_1Utils.html", null ],
    [ "G4DNAIRTMoleculeEncounterStepper::Utils", "d7/d0b/classG4DNAIRTMoleculeEncounterStepper_1_1Utils.html", null ],
    [ "G4DNAMoleculeEncounterStepper::Utils", "df/d2e/classG4DNAMoleculeEncounterStepper_1_1Utils.html", null ],
    [ "G4AnalysisMessengerHelper::ValueData", "d4/dcf/structG4AnalysisMessengerHelper_1_1ValueData.html", null ],
    [ "PTL::Valuelist< T, Values >", "d8/dea/structPTL_1_1Valuelist.html", null ],
    [ "VCall< A >", "d3/d79/classVCall.html", [
      [ "Call< g, f, A >", "d6/d3d/classCall.html", null ]
    ] ],
    [ "std::vector", null, [
      [ "G4FastSimulationVector< G4VFastSimulationModel >", "d6/dd4/classG4FastSimulationVector.html", null ],
      [ "G4FastSimulationVector< HepGeom::Transform3D >", "d6/dd4/classG4FastSimulationVector.html", null ],
      [ "G4FastSimulationVector< G4FastSimulationManager >", "d6/dd4/classG4FastSimulationVector.html", null ],
      [ "G4FastSimulationVector< G4FastSimulationManagerProcess >", "d6/dd4/classG4FastSimulationVector.html", null ],
      [ "G4INCL::UnorderedVector< Particle * >", "d2/dc8/classG4INCL_1_1UnorderedVector.html", [
        [ "G4INCL::ParticleList", "de/dd1/classG4INCL_1_1ParticleList.html", null ]
      ] ],
      [ "G4INCL::UnorderedVector< IAvatar * >", "d2/dc8/classG4INCL_1_1UnorderedVector.html", null ],
      [ "G4AssemblyStore", "d1/d9c/classG4AssemblyStore.html", null ],
      [ "G4CollectionNameVector", "df/d6f/classG4CollectionNameVector.html", null ],
      [ "G4DataVector", "de/d4e/classG4DataVector.html", null ],
      [ "G4FastSimulationVector< T >", "d6/dd4/classG4FastSimulationVector.html", null ],
      [ "G4FieldManagerStore", "d0/d40/classG4FieldManagerStore.html", null ],
      [ "G4GraphicsSystemList", "da/df2/classG4GraphicsSystemList.html", null ],
      [ "G4INCL::Random::SeedVector", "db/d5d/classG4INCL_1_1Random_1_1SeedVector.html", null ],
      [ "G4INCL::UnorderedVector< T >", "d2/dc8/classG4INCL_1_1UnorderedVector.html", null ],
      [ "G4KineticTrackVector", "dd/d47/classG4KineticTrackVector.html", null ],
      [ "G4LocatorChangeLogger", "d9/dc9/classG4LocatorChangeLogger.html", null ],
      [ "G4LogicalVolumeStore", "d9/df2/classG4LogicalVolumeStore.html", null ],
      [ "G4LowEXsection", "d3/d1d/classG4LowEXsection.html", null ],
      [ "G4OrderedTable", "d9/d56/classG4OrderedTable.html", null ],
      [ "G4PhysicalVolumeStore", "d9/dbb/classG4PhysicalVolumeStore.html", null ],
      [ "G4PhysicsTable", "dd/d15/classG4PhysicsTable.html", null ],
      [ "G4PiData", "df/d09/classG4PiData.html", null ],
      [ "G4Point3DList", "dc/d81/classG4Point3DList.html", [
        [ "G4Polyline", "d0/dd9/classG4Polyline.html", null ],
        [ "G4Polymarker", "d6/d89/classG4Polymarker.html", null ]
      ] ],
      [ "G4RegionStore", "db/d69/classG4RegionStore.html", null ],
      [ "G4SceneHandlerList", "dd/d5d/classG4SceneHandlerList.html", null ],
      [ "G4SceneList", "d2/d33/classG4SceneList.html", null ],
      [ "G4SolidStore", "de/dbf/classG4SolidStore.html", null ],
      [ "G4TrackStack", "df/dd3/classG4TrackStack.html", null ],
      [ "G4ViewerList", "d3/db9/classG4ViewerList.html", null ]
    ] ],
    [ "G4OpenInventorQtExaminerViewer::viewPtData", "d1/d4c/structG4OpenInventorQtExaminerViewer_1_1viewPtData.html", null ],
    [ "G4OpenInventorXtExaminerViewer::viewPtData", "d3/d49/structG4OpenInventorXtExaminerViewer_1_1viewPtData.html", null ],
    [ "G4ModelingParameters::VisAttributesModifier", "d4/d3c/classG4ModelingParameters_1_1VisAttributesModifier.html", null ],
    [ "PTL::VTask", "d6/d05/classPTL_1_1VTask.html", [
      [ "PTL::TaskFuture< void >", "d0/d8c/classPTL_1_1TaskFuture.html", [
        [ "PTL::Task< void, void >", "d3/d55/classPTL_1_1Task_3_01void_00_01void_01_4.html", null ]
      ] ],
      [ "PTL::TaskFuture< RetT >", "d0/d8c/classPTL_1_1TaskFuture.html", [
        [ "PTL::PackagedTask< RetT, Args >", "d6/d83/classPTL_1_1PackagedTask.html", null ],
        [ "PTL::Task< RetT, Args >", "d0/df1/classPTL_1_1Task.html", null ],
        [ "PTL::Task< RetT, void >", "df/d6d/classPTL_1_1Task_3_01RetT_00_01void_01_4.html", null ]
      ] ]
    ] ],
    [ "vtkCommand", null, [
      [ "vtkGeant4Callback", "dd/ddb/classvtkGeant4Callback.html", null ],
      [ "vtkInfoCallback", "d5/d06/classvtkInfoCallback.html", null ]
    ] ],
    [ "vtkPolyDataAlgorithm", null, [
      [ "vtkTensorGlyphColor", "dc/d02/classvtkTensorGlyphColor.html", null ]
    ] ],
    [ "PTL::VUserTaskQueue", "d4/daf/classPTL_1_1VUserTaskQueue.html", [
      [ "PTL::UserTaskQueue", "d1/d80/classPTL_1_1UserTaskQueue.html", null ]
    ] ],
    [ "G4FastList< OBJECT >::Watcher", "dd/d37/classG4FastList_1_1Watcher.html", [
      [ "G4FastList< OBJECT >::TWatcher< WATCHER_TYPE >", "de/d3e/classG4FastList_1_1TWatcher.html", null ],
      [ "G4ManyFastLists< OBJECT >", "d6/d52/classG4ManyFastLists.html", null ]
    ] ],
    [ "G4FastList< OBJECT >::Watcher", null, [
      [ "G4ManyFastLists< G4Track >", "d6/d52/classG4ManyFastLists.html", null ]
    ] ],
    [ "G4TrackList::Watcher", null, [
      [ "PriorityList", "d6/d0e/classPriorityList.html", null ]
    ] ],
    [ "WattSpectrumConstants", "d0/d61/structWattSpectrumConstants.html", null ],
    [ "xDataTOM_attribute_s", "d4/d95/structxDataTOM__attribute__s.html", null ],
    [ "xDataTOM_attributionList_s", "d5/de0/structxDataTOM__attributionList__s.html", null ],
    [ "xDataTOM_axes_s", "da/d7f/structxDataTOM__axes__s.html", null ],
    [ "xDataTOM_axis_s", "dc/d94/structxDataTOM__axis__s.html", null ],
    [ "xDataTOM_element_s", "d0/dae/structxDataTOM__element__s.html", null ],
    [ "xDataTOM_elementList_s", "dc/d16/structxDataTOM__elementList__s.html", null ],
    [ "xDataTOM_elementListItem_s", "d4/d18/structxDataTOM__elementListItem__s.html", null ],
    [ "xDataTOM_interpolation_s", "d4/de7/structxDataTOM__interpolation__s.html", null ],
    [ "xDataTOM_KalbachMann_s", "d5/d98/structxDataTOM__KalbachMann__s.html", null ],
    [ "xDataTOM_KalbachMannCoefficients_s", "da/d9b/structxDataTOM__KalbachMannCoefficients__s.html", null ],
    [ "xDataTOM_LegendreSeries_s", "d7/dc2/structxDataTOM__LegendreSeries__s.html", null ],
    [ "xDataTOM_polynomial_s", "de/d77/structxDataTOM__polynomial__s.html", null ],
    [ "xDataTOM_regionsW_XYs_LegendreSeries_s", "d6/def/structxDataTOM__regionsW__XYs__LegendreSeries__s.html", null ],
    [ "xDataTOM_regionsXYs_s", "d2/de6/structxDataTOM__regionsXYs__s.html", null ],
    [ "xDataTOM_subAxes_s", "d1/da0/structxDataTOM__subAxes__s.html", null ],
    [ "xDataTOM_TOM_s", "dc/d31/structxDataTOM__TOM__s.html", null ],
    [ "xDataTOM_V_W_XYs_LegendreSeries_s", "d7/d6b/structxDataTOM__V__W__XYs__LegendreSeries__s.html", null ],
    [ "xDataTOM_V_W_XYs_s", "dd/d0b/structxDataTOM__V__W__XYs__s.html", null ],
    [ "xDataTOM_W_XYs_LegendreSeries_s", "df/dd1/structxDataTOM__W__XYs__LegendreSeries__s.html", null ],
    [ "xDataTOM_W_XYs_s", "da/d17/structxDataTOM__W__XYs__s.html", null ],
    [ "xDataTOM_xDataInfo_s", "d8/d7b/structxDataTOM__xDataInfo__s.html", null ],
    [ "xDataTOM_XYs_s", "da/d85/structxDataTOM__XYs__s.html", null ],
    [ "xDataXML_attribute_s", "de/ded/structxDataXML__attribute__s.html", null ],
    [ "xDataXML_attributionList_s", "da/df6/structxDataXML__attributionList__s.html", null ],
    [ "xDataXML_docInfo_s", "d9/d76/structxDataXML__docInfo__s.html", null ],
    [ "xDataXML_document_s", "da/dea/structxDataXML__document__s.html", null ],
    [ "xDataXML_element_s", "da/d13/structxDataXML__element__s.html", null ],
    [ "xDataXML_elementList_s", "dc/d02/structxDataXML__elementList__s.html", null ],
    [ "xDataXML_elementListItem_s", "dc/d73/structxDataXML__elementListItem__s.html", null ],
    [ "xDataXML_item_s", "d6/dcd/structxDataXML__item__s.html", null ],
    [ "xDataXML_rootElement_s", "d8/d9b/structxDataXML__rootElement__s.html", null ],
    [ "xDataXML_smr_s", "d8/d8f/structxDataXML__smr__s.html", null ],
    [ "xDataXML_text_s", "dd/d03/structxDataXML__text__s.html", null ],
    [ "xDataXMLType_s", "dc/d7a/structxDataXMLType__s.html", null ],
    [ "XML_cp", "d4/d81/structXML__cp.html", null ],
    [ "XML_Encoding", "dd/d0b/structXML__Encoding.html", null ],
    [ "XML_Expat_Version", "dc/d7c/structXML__Expat__Version.html", null ],
    [ "XML_Feature", "d0/dfa/structXML__Feature.html", null ],
    [ "XML_Memory_Handling_Suite", "dd/db1/structXML__Memory__Handling__Suite.html", null ],
    [ "XML_ParserStruct", "db/ddf/structXML__ParserStruct.html", null ],
    [ "XML_ParsingStatus", "d0/dd0/structXML__ParsingStatus.html", null ],
    [ "G4UItokenNum::yystype", "d3/df5/structG4UItokenNum_1_1yystype.html", null ],
    [ "z_stream_s", "d7/d8a/structz__stream__s.html", null ],
    [ "ZAMass", "d2/dc0/structZAMass.html", null ],
    [ "ZLabels", "db/d64/structZLabels.html", null ],
    [ "G4GDMLReadSolids::zplaneType", "da/d13/structG4GDMLReadSolids_1_1zplaneType.html", null ],
    [ "G4ExtrudedSolid::ZSection", "df/d9e/structG4ExtrudedSolid_1_1ZSection.html", null ],
    [ "ZSymbol", "d8/dc8/structZSymbol.html", null ]
];