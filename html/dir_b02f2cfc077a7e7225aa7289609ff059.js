var dir_b02f2cfc077a7e7225aa7289609ff059 =
[
    [ "G4BaryonSplitter.hh", "d3/dde/G4BaryonSplitter_8hh.html", "d3/dde/G4BaryonSplitter_8hh" ],
    [ "G4DiffractiveStringBuilder.hh", "dc/d1e/G4DiffractiveStringBuilder_8hh.html", "dc/d1e/G4DiffractiveStringBuilder_8hh" ],
    [ "G4GammaParticipants.hh", "d8/d8e/G4GammaParticipants_8hh.html", "d8/d8e/G4GammaParticipants_8hh" ],
    [ "G4MesonSplitter.hh", "d8/dfe/G4MesonSplitter_8hh.html", "d8/dfe/G4MesonSplitter_8hh" ],
    [ "G4PartonPair.hh", "d3/d29/G4PartonPair_8hh.html", "d3/d29/G4PartonPair_8hh" ],
    [ "G4QGSDiffractiveExcitation.hh", "d8/d7a/G4QGSDiffractiveExcitation_8hh.html", "d8/d7a/G4QGSDiffractiveExcitation_8hh" ],
    [ "G4QGSModel.hh", "d5/d25/G4QGSModel_8hh.html", "d5/d25/G4QGSModel_8hh" ],
    [ "G4QGSMSplitableHadron.hh", "d5/d37/G4QGSMSplitableHadron_8hh.html", "d5/d37/G4QGSMSplitableHadron_8hh" ],
    [ "G4QGSParticipants.hh", "db/dc1/G4QGSParticipants_8hh.html", "db/dc1/G4QGSParticipants_8hh" ],
    [ "G4QuarkExchange.hh", "d5/d81/G4QuarkExchange_8hh.html", "d5/d81/G4QuarkExchange_8hh" ],
    [ "G4Reggeons.hh", "dc/d08/G4Reggeons_8hh.html", "dc/d08/G4Reggeons_8hh" ],
    [ "G4SingleDiffractiveExcitation.hh", "df/dfb/G4SingleDiffractiveExcitation_8hh.html", "df/dfb/G4SingleDiffractiveExcitation_8hh" ],
    [ "G4SoftStringBuilder.hh", "de/dad/G4SoftStringBuilder_8hh.html", "de/dad/G4SoftStringBuilder_8hh" ],
    [ "G4SPBaryon.hh", "dd/d3c/G4SPBaryon_8hh.html", "dd/d3c/G4SPBaryon_8hh" ],
    [ "G4SPBaryonTable.hh", "d6/ddb/G4SPBaryonTable_8hh.html", "d6/ddb/G4SPBaryonTable_8hh" ],
    [ "G4SPPartonInfo.hh", "d8/d8f/G4SPPartonInfo_8hh.html", "d8/d8f/G4SPPartonInfo_8hh" ]
];