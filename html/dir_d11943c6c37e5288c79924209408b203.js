var dir_d11943c6c37e5288c79924209408b203 =
[
    [ "G4BFieldIntegrationDriver.hh", "d3/d29/G4BFieldIntegrationDriver_8hh.html", "d3/d29/G4BFieldIntegrationDriver_8hh" ],
    [ "G4BogackiShampine23.hh", "d4/dd5/G4BogackiShampine23_8hh.html", "d4/dd5/G4BogackiShampine23_8hh" ],
    [ "G4BogackiShampine45.hh", "d4/d82/G4BogackiShampine45_8hh.html", "d4/d82/G4BogackiShampine45_8hh" ],
    [ "G4BulirschStoer.hh", "dc/da1/G4BulirschStoer_8hh.html", "dc/da1/G4BulirschStoer_8hh" ],
    [ "G4BulirschStoerDriver.hh", "df/d6e/G4BulirschStoerDriver_8hh.html", "df/d6e/G4BulirschStoerDriver_8hh" ],
    [ "G4CachedMagneticField.hh", "de/d47/G4CachedMagneticField_8hh.html", "de/d47/G4CachedMagneticField_8hh" ],
    [ "G4CashKarpRKF45.hh", "d4/d11/G4CashKarpRKF45_8hh.html", "d4/d11/G4CashKarpRKF45_8hh" ],
    [ "G4ChargeState.hh", "d7/dc0/G4ChargeState_8hh.html", "d7/dc0/G4ChargeState_8hh" ],
    [ "G4ChordFinder.hh", "d4/dd7/G4ChordFinder_8hh.html", "d4/dd7/G4ChordFinder_8hh" ],
    [ "G4ChordFinderDelegate.hh", "d1/d4b/G4ChordFinderDelegate_8hh.html", "d1/d4b/G4ChordFinderDelegate_8hh" ],
    [ "G4ClassicalRK4.hh", "d8/df0/G4ClassicalRK4_8hh.html", "d8/df0/G4ClassicalRK4_8hh" ],
    [ "G4ConstRK4.hh", "d5/d8a/G4ConstRK4_8hh.html", "d5/d8a/G4ConstRK4_8hh" ],
    [ "G4DELPHIMagField.hh", "d9/d55/G4DELPHIMagField_8hh.html", "d9/d55/G4DELPHIMagField_8hh" ],
    [ "G4DoLoMcPriRK34.hh", "d0/d92/G4DoLoMcPriRK34_8hh.html", "d0/d92/G4DoLoMcPriRK34_8hh" ],
    [ "G4DormandPrince745.hh", "de/dd7/G4DormandPrince745_8hh.html", "de/dd7/G4DormandPrince745_8hh" ],
    [ "G4DormandPrinceRK56.hh", "d6/d73/G4DormandPrinceRK56_8hh.html", "d6/d73/G4DormandPrinceRK56_8hh" ],
    [ "G4DormandPrinceRK78.hh", "d4/d53/G4DormandPrinceRK78_8hh.html", "d4/d53/G4DormandPrinceRK78_8hh" ],
    [ "G4DriverReporter.hh", "d2/d2a/G4DriverReporter_8hh.html", "d2/d2a/G4DriverReporter_8hh" ],
    [ "G4ElectricField.hh", "d6/d7f/G4ElectricField_8hh.html", "d6/d7f/G4ElectricField_8hh" ],
    [ "G4ElectroMagneticField.hh", "dd/d37/G4ElectroMagneticField_8hh.html", "dd/d37/G4ElectroMagneticField_8hh" ],
    [ "G4EqEMFieldWithEDM.hh", "d2/d99/G4EqEMFieldWithEDM_8hh.html", "d2/d99/G4EqEMFieldWithEDM_8hh" ],
    [ "G4EqEMFieldWithSpin.hh", "dc/d80/G4EqEMFieldWithSpin_8hh.html", "dc/d80/G4EqEMFieldWithSpin_8hh" ],
    [ "G4EqGravityField.hh", "db/d38/G4EqGravityField_8hh.html", "db/d38/G4EqGravityField_8hh" ],
    [ "G4EqMagElectricField.hh", "d8/d7c/G4EqMagElectricField_8hh.html", "d8/d7c/G4EqMagElectricField_8hh" ],
    [ "G4EquationOfMotion.hh", "d1/df6/G4EquationOfMotion_8hh.html", "d1/df6/G4EquationOfMotion_8hh" ],
    [ "G4ErrorMag_UsualEqRhs.hh", "d2/d4a/G4ErrorMag__UsualEqRhs_8hh.html", "d2/d4a/G4ErrorMag__UsualEqRhs_8hh" ],
    [ "G4ExactHelixStepper.hh", "d3/d32/G4ExactHelixStepper_8hh.html", "d3/d32/G4ExactHelixStepper_8hh" ],
    [ "G4ExplicitEuler.hh", "d3/de5/G4ExplicitEuler_8hh.html", "d3/de5/G4ExplicitEuler_8hh" ],
    [ "G4Field.hh", "d3/d77/G4Field_8hh.html", "d3/d77/G4Field_8hh" ],
    [ "G4FieldManager.hh", "d3/d88/G4FieldManager_8hh.html", "d3/d88/G4FieldManager_8hh" ],
    [ "G4FieldManagerStore.hh", "d6/d36/G4FieldManagerStore_8hh.html", "d6/d36/G4FieldManagerStore_8hh" ],
    [ "G4FieldTrack.hh", "d6/da8/G4FieldTrack_8hh.html", "d6/da8/G4FieldTrack_8hh" ],
    [ "G4FieldUtils.hh", "db/d3b/G4FieldUtils_8hh.html", "db/d3b/G4FieldUtils_8hh" ],
    [ "G4FSALBogackiShampine45.hh", "db/d88/G4FSALBogackiShampine45_8hh.html", "db/d88/G4FSALBogackiShampine45_8hh" ],
    [ "G4FSALDormandPrince745.hh", "d8/d63/G4FSALDormandPrince745_8hh.html", "d8/d63/G4FSALDormandPrince745_8hh" ],
    [ "G4FSALIntegrationDriver.hh", "db/d25/G4FSALIntegrationDriver_8hh.html", "db/d25/G4FSALIntegrationDriver_8hh" ],
    [ "G4HarmonicPolMagField.hh", "d4/dd3/G4HarmonicPolMagField_8hh.html", "d4/dd3/G4HarmonicPolMagField_8hh" ],
    [ "G4HelixExplicitEuler.hh", "da/d3a/G4HelixExplicitEuler_8hh.html", "da/d3a/G4HelixExplicitEuler_8hh" ],
    [ "G4HelixHeum.hh", "d5/d34/G4HelixHeum_8hh.html", "d5/d34/G4HelixHeum_8hh" ],
    [ "G4HelixImplicitEuler.hh", "d7/da8/G4HelixImplicitEuler_8hh.html", "d7/da8/G4HelixImplicitEuler_8hh" ],
    [ "G4HelixMixedStepper.hh", "d5/d2f/G4HelixMixedStepper_8hh.html", "d5/d2f/G4HelixMixedStepper_8hh" ],
    [ "G4HelixSimpleRunge.hh", "de/dbf/G4HelixSimpleRunge_8hh.html", "de/dbf/G4HelixSimpleRunge_8hh" ],
    [ "G4ImplicitEuler.hh", "de/d26/G4ImplicitEuler_8hh.html", "de/d26/G4ImplicitEuler_8hh" ],
    [ "G4IntegrationDriver.hh", "d2/d8b/G4IntegrationDriver_8hh.html", "d2/d8b/G4IntegrationDriver_8hh" ],
    [ "G4InterpolationDriver.hh", "d6/dc5/G4InterpolationDriver_8hh.html", "d6/dc5/G4InterpolationDriver_8hh" ],
    [ "G4LineCurrentMagField.hh", "da/d62/G4LineCurrentMagField_8hh.html", "da/d62/G4LineCurrentMagField_8hh" ],
    [ "G4LineSection.hh", "db/dd8/G4LineSection_8hh.html", "db/dd8/G4LineSection_8hh" ],
    [ "G4Mag_EqRhs.hh", "d1/d7d/G4Mag__EqRhs_8hh.html", "d1/d7d/G4Mag__EqRhs_8hh" ],
    [ "G4Mag_SpinEqRhs.hh", "de/d8d/G4Mag__SpinEqRhs_8hh.html", "de/d8d/G4Mag__SpinEqRhs_8hh" ],
    [ "G4Mag_UsualEqRhs.hh", "d9/dc8/G4Mag__UsualEqRhs_8hh.html", "d9/dc8/G4Mag__UsualEqRhs_8hh" ],
    [ "G4MagErrorStepper.hh", "d6/d84/G4MagErrorStepper_8hh.html", "d6/d84/G4MagErrorStepper_8hh" ],
    [ "G4MagHelicalStepper.hh", "d6/dab/G4MagHelicalStepper_8hh.html", "d6/dab/G4MagHelicalStepper_8hh" ],
    [ "G4MagIntegratorDriver.hh", "d0/d90/G4MagIntegratorDriver_8hh.html", "d0/d90/G4MagIntegratorDriver_8hh" ],
    [ "G4MagIntegratorStepper.hh", "de/d95/G4MagIntegratorStepper_8hh.html", "de/d95/G4MagIntegratorStepper_8hh" ],
    [ "G4MagneticField.hh", "d7/dd4/G4MagneticField_8hh.html", "d7/dd4/G4MagneticField_8hh" ],
    [ "G4ModifiedMidpoint.hh", "d0/df4/G4ModifiedMidpoint_8hh.html", "d0/df4/G4ModifiedMidpoint_8hh" ],
    [ "G4MonopoleEq.hh", "df/de4/G4MonopoleEq_8hh.html", "df/de4/G4MonopoleEq_8hh" ],
    [ "G4NystromRK4.hh", "de/d44/G4NystromRK4_8hh.html", "de/d44/G4NystromRK4_8hh" ],
    [ "G4OldMagIntDriver.hh", "df/d8e/G4OldMagIntDriver_8hh.html", "df/d8e/G4OldMagIntDriver_8hh" ],
    [ "G4QuadrupoleMagField.hh", "db/d51/G4QuadrupoleMagField_8hh.html", "db/d51/G4QuadrupoleMagField_8hh" ],
    [ "G4RepleteEofM.hh", "df/dff/G4RepleteEofM_8hh.html", "df/dff/G4RepleteEofM_8hh" ],
    [ "G4RK547FEq1.hh", "dd/d1e/G4RK547FEq1_8hh.html", "dd/d1e/G4RK547FEq1_8hh" ],
    [ "G4RK547FEq2.hh", "d7/d78/G4RK547FEq2_8hh.html", "d7/d78/G4RK547FEq2_8hh" ],
    [ "G4RK547FEq3.hh", "dc/d2e/G4RK547FEq3_8hh.html", "dc/d2e/G4RK547FEq3_8hh" ],
    [ "G4RKG3_Stepper.hh", "d0/d6d/G4RKG3__Stepper_8hh.html", "d0/d6d/G4RKG3__Stepper_8hh" ],
    [ "G4RKIntegrationDriver.hh", "d6/d4c/G4RKIntegrationDriver_8hh.html", "d6/d4c/G4RKIntegrationDriver_8hh" ],
    [ "G4SextupoleMagField.hh", "df/dc4/G4SextupoleMagField_8hh.html", "df/dc4/G4SextupoleMagField_8hh" ],
    [ "G4SimpleHeum.hh", "d7/dbc/G4SimpleHeum_8hh.html", "d7/dbc/G4SimpleHeum_8hh" ],
    [ "G4SimpleRunge.hh", "d1/dc6/G4SimpleRunge_8hh.html", "d1/dc6/G4SimpleRunge_8hh" ],
    [ "G4TCachedMagneticField.hh", "d8/d13/G4TCachedMagneticField_8hh.html", "d8/d13/G4TCachedMagneticField_8hh" ],
    [ "G4TCashKarpRKF45.hh", "d6/d30/G4TCashKarpRKF45_8hh.html", "d6/d30/G4TCashKarpRKF45_8hh" ],
    [ "G4TClassicalRK4.hh", "df/db2/G4TClassicalRK4_8hh.html", "df/db2/G4TClassicalRK4_8hh" ],
    [ "G4TDormandPrince45.hh", "d1/d35/G4TDormandPrince45_8hh.html", "d1/d35/G4TDormandPrince45_8hh" ],
    [ "G4TExplicitEuler.hh", "d1/db6/G4TExplicitEuler_8hh.html", "d1/db6/G4TExplicitEuler_8hh" ],
    [ "G4TMagErrorStepper.hh", "da/d34/G4TMagErrorStepper_8hh.html", "da/d34/G4TMagErrorStepper_8hh" ],
    [ "G4TMagFieldEquation.hh", "d2/d79/G4TMagFieldEquation_8hh.html", "d2/d79/G4TMagFieldEquation_8hh" ],
    [ "G4TQuadrupoleMagField.hh", "df/da3/G4TQuadrupoleMagField_8hh.html", "df/da3/G4TQuadrupoleMagField_8hh" ],
    [ "G4TrialsCounter.hh", "d2/d87/G4TrialsCounter_8hh.html", "d2/d87/G4TrialsCounter_8hh" ],
    [ "G4TSimpleHeum.hh", "d4/d4c/G4TSimpleHeum_8hh.html", "d4/d4c/G4TSimpleHeum_8hh" ],
    [ "G4TSimpleRunge.hh", "d8/d44/G4TSimpleRunge_8hh.html", "d8/d44/G4TSimpleRunge_8hh" ],
    [ "G4TsitourasRK45.hh", "df/de0/G4TsitourasRK45_8hh.html", "df/de0/G4TsitourasRK45_8hh" ],
    [ "G4TUniformMagneticField.hh", "d3/d22/G4TUniformMagneticField_8hh.html", "d3/d22/G4TUniformMagneticField_8hh" ],
    [ "G4UniformElectricField.hh", "dc/d72/G4UniformElectricField_8hh.html", "dc/d72/G4UniformElectricField_8hh" ],
    [ "G4UniformGravityField.hh", "d3/db2/G4UniformGravityField_8hh.html", "d3/db2/G4UniformGravityField_8hh" ],
    [ "G4UniformMagField.hh", "d9/d7b/G4UniformMagField_8hh.html", "d9/d7b/G4UniformMagField_8hh" ],
    [ "G4VFSALIntegrationStepper.hh", "d6/d22/G4VFSALIntegrationStepper_8hh.html", "d6/d22/G4VFSALIntegrationStepper_8hh" ],
    [ "G4VIntegrationDriver.hh", "d1/dc3/G4VIntegrationDriver_8hh.html", "d1/dc3/G4VIntegrationDriver_8hh" ]
];