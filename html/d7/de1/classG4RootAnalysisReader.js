var classG4RootAnalysisReader =
[
    [ "~G4RootAnalysisReader", "d7/de1/classG4RootAnalysisReader.html#ac2714366e5b6d662d9ae1accd383ee74", null ],
    [ "G4RootAnalysisReader", "d7/de1/classG4RootAnalysisReader.html#a217bc2cb7289084f09c47425ee551c29", null ],
    [ "CloseFilesImpl", "d7/de1/classG4RootAnalysisReader.html#a51e894fae15f73ccfc58558ea54e47cd", null ],
    [ "GetNtuple", "d7/de1/classG4RootAnalysisReader.html#a2272da51f04863db776942fab2dc4b7c", null ],
    [ "GetNtuple", "d7/de1/classG4RootAnalysisReader.html#abbb85d623bd3215dfc51c42b90525840", null ],
    [ "GetNtuple", "d7/de1/classG4RootAnalysisReader.html#ad60d14c490c55aedd44fb0dcbb38980d", null ],
    [ "Instance", "d7/de1/classG4RootAnalysisReader.html#a480f6de51e217e0b948bb3c5b9e3fefd", null ],
    [ "Reset", "d7/de1/classG4RootAnalysisReader.html#a8a918834292fa3fec33b972e029eb780", null ],
    [ "G4ThreadLocalSingleton< G4RootAnalysisReader >", "d7/de1/classG4RootAnalysisReader.html#ad8405700df16f19cdbf0f3f9bad784fb", null ],
    [ "fFileManager", "d7/de1/classG4RootAnalysisReader.html#a5e503614a5a478749b26c1cee8112557", null ],
    [ "fgMasterInstance", "d7/de1/classG4RootAnalysisReader.html#ab4909c8e09290e126206ff92997ab995", null ],
    [ "fkClass", "d7/de1/classG4RootAnalysisReader.html#ad6fcab357bfceed4d3b19b81a6b6e4bc", null ],
    [ "fNtupleManager", "d7/de1/classG4RootAnalysisReader.html#aa10a2416b59606cb8359198704f088fe", null ]
];