var classG4FermiFragment =
[
    [ "G4FermiFragment", "d7/dbf/classG4FermiFragment.html#a844ceee3991c4b91386aaad017de299d", null ],
    [ "~G4FermiFragment", "d7/dbf/classG4FermiFragment.html#a049b323a0caaa4a8a4e744e381e122fd", null ],
    [ "G4FermiFragment", "d7/dbf/classG4FermiFragment.html#a530e6779f245dd17a8e332a77d135b66", null ],
    [ "GetA", "d7/dbf/classG4FermiFragment.html#a9c28af4c6ffe90479d37d17912293634", null ],
    [ "GetCoulombBarrier", "d7/dbf/classG4FermiFragment.html#a7aad440e924195e36d33f220b6ee1444", null ],
    [ "GetExcitationEnergy", "d7/dbf/classG4FermiFragment.html#a5768b4c1108b4cf84dbf87df1e802ef6", null ],
    [ "GetFragmentMass", "d7/dbf/classG4FermiFragment.html#ac0cac83f3101f09d7c96948ef772998a", null ],
    [ "GetSpin", "d7/dbf/classG4FermiFragment.html#ab0005b08638dd9db727500fb2b4d57a6", null ],
    [ "GetTotalEnergy", "d7/dbf/classG4FermiFragment.html#afa57c7f444ba55b755d186f4b5b9ed2b", null ],
    [ "GetZ", "d7/dbf/classG4FermiFragment.html#a73aff43ed30c94a14b943ed999f5421e", null ],
    [ "operator!=", "d7/dbf/classG4FermiFragment.html#ac1969b1b9c88555a784fd047aaec4f4f", null ],
    [ "operator=", "d7/dbf/classG4FermiFragment.html#ad4bec131e2f028d317815669d995bd48", null ],
    [ "operator==", "d7/dbf/classG4FermiFragment.html#a39ace8bf0ea154f631aef362cc38cd7e", null ],
    [ "A", "d7/dbf/classG4FermiFragment.html#a1a2d28cd9a5265de0bd234af862256bf", null ],
    [ "cBarrier", "d7/dbf/classG4FermiFragment.html#a6a2aa9a77a2392ab632a5f6203faa5b1", null ],
    [ "excitEnergy", "d7/dbf/classG4FermiFragment.html#ad38c88756246bff8b5dc33364dcbb511", null ],
    [ "fragmentMass", "d7/dbf/classG4FermiFragment.html#a906254055f2dd18ce3a3e8e8a8a3b1d7", null ],
    [ "spin", "d7/dbf/classG4FermiFragment.html#a8f0efcfc5433521e2ab91c7d98575b77", null ],
    [ "Z", "d7/dbf/classG4FermiFragment.html#a9db041a37dd745e783da01ba4a83c465", null ]
];