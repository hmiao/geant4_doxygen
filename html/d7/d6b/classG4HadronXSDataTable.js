var classG4HadronXSDataTable =
[
    [ "G4HadronXSDataTable", "d7/d6b/classG4HadronXSDataTable.html#a73295be27b0f33cc1e95f5d998a2c254", null ],
    [ "~G4HadronXSDataTable", "d7/d6b/classG4HadronXSDataTable.html#a14d2a7bbfef31161c300d48e008cb498", null ],
    [ "G4HadronXSDataTable", "d7/d6b/classG4HadronXSDataTable.html#aee81e85ad69f750f790781326189a440", null ],
    [ "Dump", "d7/d6b/classG4HadronXSDataTable.html#a14951a0ff7e9e755e4f877298e8ce89c", null ],
    [ "GetCrossSection", "d7/d6b/classG4HadronXSDataTable.html#a459f6fa57a4d9ca25c9f324041197583", null ],
    [ "HasData", "d7/d6b/classG4HadronXSDataTable.html#ac64a618fe58f693893ab4786a1d4dea8", null ],
    [ "Initialise", "d7/d6b/classG4HadronXSDataTable.html#a09a53285b992bdecab0079f02598db64", null ],
    [ "operator=", "d7/d6b/classG4HadronXSDataTable.html#ad87494b4fdf98b16abc8d5a443c75d49", null ],
    [ "SelectRandomAtom", "d7/d6b/classG4HadronXSDataTable.html#ad1d915138ddda9aaad0669af4e0939ba", null ],
    [ "elmSelectors", "d7/d6b/classG4HadronXSDataTable.html#a7c2cae7d3cb7b0bf584f1f10dc145e3e", null ],
    [ "nMaterials", "d7/d6b/classG4HadronXSDataTable.html#a414627102803dd8ca38ef9a7a55c0193", null ],
    [ "xsData", "d7/d6b/classG4HadronXSDataTable.html#a4cd7331f5ea943d4281a444774639e5f", null ]
];