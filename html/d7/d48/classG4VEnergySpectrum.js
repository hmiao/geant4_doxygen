var classG4VEnergySpectrum =
[
    [ "G4VEnergySpectrum", "d7/d48/classG4VEnergySpectrum.html#a17efb9fc107959cd43093473188c8587", null ],
    [ "~G4VEnergySpectrum", "d7/d48/classG4VEnergySpectrum.html#a06ec0b2f634f06e1788c052098049fb8", null ],
    [ "G4VEnergySpectrum", "d7/d48/classG4VEnergySpectrum.html#aaad1a495ea166b7c01af191906c40d70", null ],
    [ "AverageEnergy", "d7/d48/classG4VEnergySpectrum.html#a452117194dd8f8284f3d812426a27f9e", null ],
    [ "Excitation", "d7/d48/classG4VEnergySpectrum.html#ab1e3d225a54ebc56ef63519b254a3e47", null ],
    [ "MaxEnergyOfSecondaries", "d7/d48/classG4VEnergySpectrum.html#a393cbadc443e33b9c360257a57634c35", null ],
    [ "operator=", "d7/d48/classG4VEnergySpectrum.html#a8a3d2dcf8c1b41f210cd95c49073c831", null ],
    [ "PrintData", "d7/d48/classG4VEnergySpectrum.html#a18df39cc8e0702727762760e4ac61daa", null ],
    [ "Probability", "d7/d48/classG4VEnergySpectrum.html#aeb139369d8fcd6c87050e16fd830e8cc", null ],
    [ "SampleEnergy", "d7/d48/classG4VEnergySpectrum.html#a92416716c6a689f5498b0598feebdf5e", null ]
];