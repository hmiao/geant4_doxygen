var classG4CutTubs =
[
    [ "G4CutTubs", "d7/d40/classG4CutTubs.html#aebe0405d9afd81a1ffffcf3604dd37a5", null ],
    [ "~G4CutTubs", "d7/d40/classG4CutTubs.html#a8dda9fe8a983b5549e96bc973a597ad7", null ],
    [ "G4CutTubs", "d7/d40/classG4CutTubs.html#ac26978abee406ec875cfbd4b4c103733", null ],
    [ "G4CutTubs", "d7/d40/classG4CutTubs.html#af6e04c7e7d98be07e29a5661c54b01a2", null ],
    [ "ApproxSurfaceNormal", "d7/d40/classG4CutTubs.html#a5d198aacfe9bcb1399b4389754821f14", null ],
    [ "BoundingLimits", "d7/d40/classG4CutTubs.html#a6af68660cc894c22158d2b480f42892f", null ],
    [ "CalculateExtent", "d7/d40/classG4CutTubs.html#a7d1e1f702985e0fa3b506b532b2f621c", null ],
    [ "CheckDPhiAngle", "d7/d40/classG4CutTubs.html#a2b65f200c7a8b8141cbb5d1b4cd0c756", null ],
    [ "CheckPhiAngles", "d7/d40/classG4CutTubs.html#a6626ac594fba567792d44a59c1826859", null ],
    [ "CheckSPhiAngle", "d7/d40/classG4CutTubs.html#aae28999d2b36cf66acf2ed97e37e5603", null ],
    [ "Clone", "d7/d40/classG4CutTubs.html#a6f676844701a4fa522cf988217cb5e68", null ],
    [ "CreatePolyhedron", "d7/d40/classG4CutTubs.html#a4f2095cc19e699b75a9ea2e1b0ae25d6", null ],
    [ "DescribeYourselfTo", "d7/d40/classG4CutTubs.html#ae741c6b7b4484b507e782adc98448c0b", null ],
    [ "DistanceToIn", "d7/d40/classG4CutTubs.html#aeb1f26168e994e21ba3b05cadd8de0d6", null ],
    [ "DistanceToIn", "d7/d40/classG4CutTubs.html#ab92a3136eba0d3466d15438df58fa4c4", null ],
    [ "DistanceToOut", "d7/d40/classG4CutTubs.html#a8d85c4e9013e2f2de9fed96949727fa1", null ],
    [ "DistanceToOut", "d7/d40/classG4CutTubs.html#a6f0a1c1cb8749cb7dbb5cdcfc61edbcb", null ],
    [ "GetCosEndPhi", "d7/d40/classG4CutTubs.html#ad60a0483240251f36fd72b23de46b938", null ],
    [ "GetCosStartPhi", "d7/d40/classG4CutTubs.html#ac776e0378d6c6a4ab0cfd29303f1627d", null ],
    [ "GetCubicVolume", "d7/d40/classG4CutTubs.html#a06cda9c2c72f8a01764971492fa1d452", null ],
    [ "GetCutZ", "d7/d40/classG4CutTubs.html#a4ceb227e88fa6500d68f7dcb0cd97647", null ],
    [ "GetDeltaPhiAngle", "d7/d40/classG4CutTubs.html#a534daecc344d5a86b6cdc801524e41e9", null ],
    [ "GetEntityType", "d7/d40/classG4CutTubs.html#a6414f7bf99d3e27b144e3eda2bab66d4", null ],
    [ "GetHighNorm", "d7/d40/classG4CutTubs.html#a05f7269b6f122a670d763155d82dc55e", null ],
    [ "GetInnerRadius", "d7/d40/classG4CutTubs.html#a1fe750029a99228944de00779322714c", null ],
    [ "GetLowNorm", "d7/d40/classG4CutTubs.html#a2ade9f71f990cf57700de58899a599d6", null ],
    [ "GetOuterRadius", "d7/d40/classG4CutTubs.html#a5440849997f6365ecedb8e2829177bff", null ],
    [ "GetPointOnSurface", "d7/d40/classG4CutTubs.html#a7f641366007f58e24e0ba4fc544cdbd6", null ],
    [ "GetSinEndPhi", "d7/d40/classG4CutTubs.html#a84a82e31a2d7b88336e9e0938102177b", null ],
    [ "GetSinStartPhi", "d7/d40/classG4CutTubs.html#a63ea20f24e0f6dafd6049a3f38682e01", null ],
    [ "GetStartPhiAngle", "d7/d40/classG4CutTubs.html#a0b185b37057f00d190b3deb515e0ac81", null ],
    [ "GetSurfaceArea", "d7/d40/classG4CutTubs.html#a78b6e5596dcc95fbd2df11961b8264e9", null ],
    [ "GetZHalfLength", "d7/d40/classG4CutTubs.html#a177248629dd08fbc10b484a908eb59c7", null ],
    [ "Initialize", "d7/d40/classG4CutTubs.html#a104380ed3d0e1a4580f1496916ab235e", null ],
    [ "InitializeTrigonometry", "d7/d40/classG4CutTubs.html#a8187f0aa3713bf4e271dcd0de36941dd", null ],
    [ "Inside", "d7/d40/classG4CutTubs.html#a235037141cec803af89357b91ccaccc9", null ],
    [ "IsCrossingCutPlanes", "d7/d40/classG4CutTubs.html#a5e33996e4d404bce1be2331b7dced47e", null ],
    [ "operator=", "d7/d40/classG4CutTubs.html#a6755befd0a721e7dcd5635fcfc232d1a", null ],
    [ "SetDeltaPhiAngle", "d7/d40/classG4CutTubs.html#a3941c7f7d520b49b81cd0d91789f4296", null ],
    [ "SetInnerRadius", "d7/d40/classG4CutTubs.html#a2459801969e4b274aebbc0ec3ba243d7", null ],
    [ "SetOuterRadius", "d7/d40/classG4CutTubs.html#af03c39b92073b29a00e8e98e8b61724b", null ],
    [ "SetStartPhiAngle", "d7/d40/classG4CutTubs.html#a35ad078593c8f2c57118df2441251dec", null ],
    [ "SetZHalfLength", "d7/d40/classG4CutTubs.html#a669a468a6e86c06a05f70d81dafb6b09", null ],
    [ "StreamInfo", "d7/d40/classG4CutTubs.html#aaacd9d755fb43a9c95b41937b144b9b4", null ],
    [ "SurfaceNormal", "d7/d40/classG4CutTubs.html#ab128e981b20645a05d892354493919a1", null ],
    [ "cosCPhi", "d7/d40/classG4CutTubs.html#a62c503433b4b6ecb1e2dbecea697d100", null ],
    [ "cosEPhi", "d7/d40/classG4CutTubs.html#a742984db72a94c68a0be4571c1a8226d", null ],
    [ "cosHDPhi", "d7/d40/classG4CutTubs.html#abb76070b149f20cd61cafc69b0c6fb30", null ],
    [ "cosHDPhiIT", "d7/d40/classG4CutTubs.html#adf27d5bdf45cd6f4e6c06d97f6992e90", null ],
    [ "cosHDPhiOT", "d7/d40/classG4CutTubs.html#ab1bf868c6052a7099ceac4de3ebb81b3", null ],
    [ "cosSPhi", "d7/d40/classG4CutTubs.html#a167b5bc5388d4fe5e1b72fc09b29ed1d", null ],
    [ "fDPhi", "d7/d40/classG4CutTubs.html#a1b03dd2b5b8cd30a51d78e3267f9d8cd", null ],
    [ "fDz", "d7/d40/classG4CutTubs.html#a32464a50a7e34c2832821929b88a0a1e", null ],
    [ "fHighNorm", "d7/d40/classG4CutTubs.html#a8db036cca07d04dcce4b6355e154ac94", null ],
    [ "fLowNorm", "d7/d40/classG4CutTubs.html#a985dd3c79152926a513a0351fe9e602e", null ],
    [ "fPhiFullCutTube", "d7/d40/classG4CutTubs.html#a8a19be5b82252b26d6dabb29c7f16401", null ],
    [ "fRMax", "d7/d40/classG4CutTubs.html#a3fa0eeeedc3da5e85fd105e5b5348879", null ],
    [ "fRMin", "d7/d40/classG4CutTubs.html#af8136e037b11324c3decac1bcede7134", null ],
    [ "fSPhi", "d7/d40/classG4CutTubs.html#a252a7f5eca217fe9a8d54bdbda714346", null ],
    [ "fZMax", "d7/d40/classG4CutTubs.html#a7644802aecbee23d25f6dd21be08b54f", null ],
    [ "fZMin", "d7/d40/classG4CutTubs.html#ad4dc0e6e962a8724f25709513e73a591", null ],
    [ "halfAngTolerance", "d7/d40/classG4CutTubs.html#a44c1d3a7aab5b724ab169530f6406469", null ],
    [ "halfCarTolerance", "d7/d40/classG4CutTubs.html#aa5ee81a28f596210284dbaf695bf8d77", null ],
    [ "halfRadTolerance", "d7/d40/classG4CutTubs.html#a088367f6de46a35a0685037946351d78", null ],
    [ "kAngTolerance", "d7/d40/classG4CutTubs.html#aee970d2f4a85793656f11d3506c23b9e", null ],
    [ "kRadTolerance", "d7/d40/classG4CutTubs.html#ac1e456c449a73601f1fdd81e8be2bf1f", null ],
    [ "sinCPhi", "d7/d40/classG4CutTubs.html#abbd5f3775c270772811b3420084ea002", null ],
    [ "sinEPhi", "d7/d40/classG4CutTubs.html#a6b1f13680453e2dd9ae2957b101382a3", null ],
    [ "sinSPhi", "d7/d40/classG4CutTubs.html#a840da88b819a9540fde1a7035a329640", null ]
];