var classG4StoppingPhysicsFritiofWithBinaryCascade =
[
    [ "G4StoppingPhysicsFritiofWithBinaryCascade", "d7/d60/classG4StoppingPhysicsFritiofWithBinaryCascade.html#a7891a456dc8a64d0b18ca868ad5e012c", null ],
    [ "G4StoppingPhysicsFritiofWithBinaryCascade", "d7/d60/classG4StoppingPhysicsFritiofWithBinaryCascade.html#a8c824019e5cced84dc2105d807e2c62a", null ],
    [ "~G4StoppingPhysicsFritiofWithBinaryCascade", "d7/d60/classG4StoppingPhysicsFritiofWithBinaryCascade.html#adf4aec71a78dd0985e101b1a035aa017", null ],
    [ "ConstructParticle", "d7/d60/classG4StoppingPhysicsFritiofWithBinaryCascade.html#a5b11c28641ffda10b7297fb2b9682ab9", null ],
    [ "ConstructProcess", "d7/d60/classG4StoppingPhysicsFritiofWithBinaryCascade.html#a0b4eb2891502312aad5acae6c335c35e", null ],
    [ "SetMuonMinusCapture", "d7/d60/classG4StoppingPhysicsFritiofWithBinaryCascade.html#a73a023a9e1d35b71f56afc87b0998016", null ],
    [ "useMuonMinusCapture", "d7/d60/classG4StoppingPhysicsFritiofWithBinaryCascade.html#a56baba5d2157c9b0e9baba316b364342", null ],
    [ "verbose", "d7/d60/classG4StoppingPhysicsFritiofWithBinaryCascade.html#ac0cec2a04a885b72296a65b9aaf73b39", null ]
];