var classG4ANuMuNucleusNcModel =
[
    [ "G4ANuMuNucleusNcModel", "d7/d10/classG4ANuMuNucleusNcModel.html#ae72354bec94dd623cee6c2482514bf88", null ],
    [ "~G4ANuMuNucleusNcModel", "d7/d10/classG4ANuMuNucleusNcModel.html#ab1b5460aad5c3de04e5e1946bf0a25ff", null ],
    [ "ApplyYourself", "d7/d10/classG4ANuMuNucleusNcModel.html#a5c517785743a3bc084f302f0d41024d2", null ],
    [ "GetMinNuMuEnergy", "d7/d10/classG4ANuMuNucleusNcModel.html#a346d353c2252a4c2ad1527fa85a41d68", null ],
    [ "InitialiseModel", "d7/d10/classG4ANuMuNucleusNcModel.html#a49e3e5454828b08a9627879bcc9fdd2a", null ],
    [ "IsApplicable", "d7/d10/classG4ANuMuNucleusNcModel.html#af6bab070af0f52e3af4d4a356d76323e", null ],
    [ "ModelDescription", "d7/d10/classG4ANuMuNucleusNcModel.html#a24e6d1f034f46a4fe13bb63be725a527", null ],
    [ "SampleLVkr", "d7/d10/classG4ANuMuNucleusNcModel.html#adfbecb995ec2822ee9e638727bc386c9", null ],
    [ "ThresholdEnergy", "d7/d10/classG4ANuMuNucleusNcModel.html#a0c6d4c9966a5b59e6920330fbc0b000a", null ],
    [ "fData", "d7/d10/classG4ANuMuNucleusNcModel.html#aada41668de042a3b4a8f296d1529733b", null ],
    [ "fMaster", "d7/d10/classG4ANuMuNucleusNcModel.html#a152b9c5b41a4bc4e1e4e1ff63f43ff2f", null ],
    [ "fMnumu", "d7/d10/classG4ANuMuNucleusNcModel.html#a60e6e50cd92fc0062a57c677b3bff41d", null ],
    [ "theANuMu", "d7/d10/classG4ANuMuNucleusNcModel.html#a2354e43aaaf1d1ed9cf14c520a880581", null ]
];