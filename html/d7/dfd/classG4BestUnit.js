var classG4BestUnit =
[
    [ "G4BestUnit", "d7/dfd/classG4BestUnit.html#af5c80707b55cb80b1ac47fa688d496a2", null ],
    [ "G4BestUnit", "d7/dfd/classG4BestUnit.html#a9ead073b992b388436b9315e548c7319", null ],
    [ "~G4BestUnit", "d7/dfd/classG4BestUnit.html#aa9c8dcd65467025d7bced5a8a57301cc", null ],
    [ "GetCategory", "d7/dfd/classG4BestUnit.html#af253eaad3960790ed64343dca7c581ec", null ],
    [ "GetIndexOfCategory", "d7/dfd/classG4BestUnit.html#af57e2216927fee2e32ea957aee7462db", null ],
    [ "GetValue", "d7/dfd/classG4BestUnit.html#a0edd2af232eaef0d5d7180ca3e5fa42c", null ],
    [ "operator G4String", "d7/dfd/classG4BestUnit.html#af6091059ea14238f95584d349a3b424e", null ],
    [ "operator<<", "d7/dfd/classG4BestUnit.html#a329b4be550f50fa63f67f9cd327c3eb9", null ],
    [ "Category", "d7/dfd/classG4BestUnit.html#a2c18eee0c3f8968b47a65b555b79408d", null ],
    [ "IndexOfCategory", "d7/dfd/classG4BestUnit.html#a653576d54ca271900fa1953ea36f41fc", null ],
    [ "nbOfVals", "d7/dfd/classG4BestUnit.html#aee721534cecc020819833690f85eb0b5", null ],
    [ "Value", "d7/dfd/classG4BestUnit.html#aaf947867f24a7e7375e35e46565b60a1", null ]
];