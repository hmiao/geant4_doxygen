var classG4PolarizedIonisationModel =
[
    [ "G4PolarizedIonisationModel", "d7/d91/classG4PolarizedIonisationModel.html#a56d30ef9fc2cc09678cd40d98c67d39d", null ],
    [ "~G4PolarizedIonisationModel", "d7/d91/classG4PolarizedIonisationModel.html#a0de09e41d830e5153f4838695aab3863", null ],
    [ "G4PolarizedIonisationModel", "d7/d91/classG4PolarizedIonisationModel.html#a29725c6539a7d425daa678b1dc56e1a6", null ],
    [ "ComputeCrossSectionPerElectron", "d7/d91/classG4PolarizedIonisationModel.html#a909cc3c5fc122de56074ebe8adf08b19", null ],
    [ "GetBeamPolarization", "d7/d91/classG4PolarizedIonisationModel.html#a9843fbc950d2b6879b4cffa22f7f6d5b", null ],
    [ "GetFinalElectronPolarization", "d7/d91/classG4PolarizedIonisationModel.html#af368f4c20c5aec6349846b3308fa6704", null ],
    [ "GetFinalPositronPolarization", "d7/d91/classG4PolarizedIonisationModel.html#a475c988e1ae9fdb2d125624da452326c", null ],
    [ "GetTargetPolarization", "d7/d91/classG4PolarizedIonisationModel.html#a48d46db5d3f473f7e77a34313ff583b4", null ],
    [ "operator=", "d7/d91/classG4PolarizedIonisationModel.html#a84e089f8230256a641830cce9ab0eea8", null ],
    [ "SampleSecondaries", "d7/d91/classG4PolarizedIonisationModel.html#a6532ab600a5e678343b914751629d2e2", null ],
    [ "SetBeamPolarization", "d7/d91/classG4PolarizedIonisationModel.html#afad6b8194d3cb6051e773968db98fe07", null ],
    [ "SetTargetPolarization", "d7/d91/classG4PolarizedIonisationModel.html#a84b7c959cebefef9ba31f8a6fce22eb1", null ],
    [ "fBeamPolarization", "d7/d91/classG4PolarizedIonisationModel.html#a6c65483192abdf6ba0836600e23c5ab1", null ],
    [ "fCrossSectionCalculator", "d7/d91/classG4PolarizedIonisationModel.html#aefda1d084148530a47dbdf0299431ada", null ],
    [ "fElectronPolarization", "d7/d91/classG4PolarizedIonisationModel.html#a96458d9263897ad49d8bf139264a4e73", null ],
    [ "fPositronPolarization", "d7/d91/classG4PolarizedIonisationModel.html#abb9fc787254fc4909b775f577d34884a", null ],
    [ "fTargetPolarization", "d7/d91/classG4PolarizedIonisationModel.html#a12048c0496238910a123a63cac8ea24f", null ]
];