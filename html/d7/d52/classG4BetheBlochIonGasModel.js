var classG4BetheBlochIonGasModel =
[
    [ "G4BetheBlochIonGasModel", "d7/d52/classG4BetheBlochIonGasModel.html#a8e6434bfdebc00436bde33c8c653c9d2", null ],
    [ "~G4BetheBlochIonGasModel", "d7/d52/classG4BetheBlochIonGasModel.html#a038573991324db6886559ee0a713471b", null ],
    [ "G4BetheBlochIonGasModel", "d7/d52/classG4BetheBlochIonGasModel.html#aa0e01b320bc1b3c0877e6fda664bca8b", null ],
    [ "ChargeSquareRatio", "d7/d52/classG4BetheBlochIonGasModel.html#a1df8d98d0865f96c0cf3e5cf934f3490", null ],
    [ "GetParticleCharge", "d7/d52/classG4BetheBlochIonGasModel.html#a01f1ea25a6c404b1bb2d370a313b7979", null ],
    [ "operator=", "d7/d52/classG4BetheBlochIonGasModel.html#a71e41e4cc342f94cecce3bd7097c2aa3", null ],
    [ "currentCharge", "d7/d52/classG4BetheBlochIonGasModel.html#aef97f482bc167887fd286df095346eb0", null ]
];