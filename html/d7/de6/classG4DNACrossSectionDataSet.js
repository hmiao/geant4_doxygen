var classG4DNACrossSectionDataSet =
[
    [ "G4DNACrossSectionDataSet", "d7/de6/classG4DNACrossSectionDataSet.html#a570eb2e9c79f708ec537b91a8854e85e", null ],
    [ "~G4DNACrossSectionDataSet", "d7/de6/classG4DNACrossSectionDataSet.html#ae42848ec37b6e22663f73f698505749d", null ],
    [ "G4DNACrossSectionDataSet", "d7/de6/classG4DNACrossSectionDataSet.html#a77f4c1cb1030c6f509621eadfa2f723c", null ],
    [ "G4DNACrossSectionDataSet", "d7/de6/classG4DNACrossSectionDataSet.html#aa86601dff9968e59abb6aa06af6a3cea", null ],
    [ "AddComponent", "d7/de6/classG4DNACrossSectionDataSet.html#af1a1712612ecc10dc28ddb6e9f959257", null ],
    [ "CleanUpComponents", "d7/de6/classG4DNACrossSectionDataSet.html#a7e4aa1b62317c5ee8af0bed5fbed0080", null ],
    [ "FindValue", "d7/de6/classG4DNACrossSectionDataSet.html#a28cadb3c14f96bfe0aa137a21bc7b0ad", null ],
    [ "FullFileName", "d7/de6/classG4DNACrossSectionDataSet.html#ad1a9675b2101d8282a3b1a290a357a04", null ],
    [ "GetAlgorithm", "d7/de6/classG4DNACrossSectionDataSet.html#a79b97ac67c755b64d7455f7d85fc314c", null ],
    [ "GetComponent", "d7/de6/classG4DNACrossSectionDataSet.html#a7bf6fee3bec70088659c3f0f4e4e531d", null ],
    [ "GetData", "d7/de6/classG4DNACrossSectionDataSet.html#ae613db7db23989dfd723a29edadfd5c6", null ],
    [ "GetEnergies", "d7/de6/classG4DNACrossSectionDataSet.html#a6bacfcbb5bc10dc48cd868df410f2b0a", null ],
    [ "GetLogData", "d7/de6/classG4DNACrossSectionDataSet.html#afcbb4bbf6925039baaa6afb48e06fd5f", null ],
    [ "GetLogEnergies", "d7/de6/classG4DNACrossSectionDataSet.html#a09d4fb75414c9ab3c65dd93c4dd54e9f", null ],
    [ "GetUnitData", "d7/de6/classG4DNACrossSectionDataSet.html#a2f57389605fccac82ff219860a4552d5", null ],
    [ "GetUnitEnergies", "d7/de6/classG4DNACrossSectionDataSet.html#a5edd4ac94de0b833a0fc8b6852be0614", null ],
    [ "LoadData", "d7/de6/classG4DNACrossSectionDataSet.html#aa269f89bf5af1f75c2915ba283a306a1", null ],
    [ "LoadNonLogData", "d7/de6/classG4DNACrossSectionDataSet.html#a89c1ad26fcff90badf25502dc91bf382", null ],
    [ "NumberOfComponents", "d7/de6/classG4DNACrossSectionDataSet.html#a5a54c8a1d966e3405d906151e17e2d4b", null ],
    [ "operator=", "d7/de6/classG4DNACrossSectionDataSet.html#abee584a957702176ebb08726aa8d18ed", null ],
    [ "PrintData", "d7/de6/classG4DNACrossSectionDataSet.html#aec682c02bc9d559ff1bdd3d052b109b4", null ],
    [ "RandomSelect", "d7/de6/classG4DNACrossSectionDataSet.html#ac73090f926f442f894b35881154a9c1b", null ],
    [ "SaveData", "d7/de6/classG4DNACrossSectionDataSet.html#add3aefb70bcd1bef86ec35453d94abfa", null ],
    [ "SetEnergiesData", "d7/de6/classG4DNACrossSectionDataSet.html#a9e44c91d7bdf50cce8532ef0484bfa83", null ],
    [ "SetLogEnergiesData", "d7/de6/classG4DNACrossSectionDataSet.html#abdd0fdcaadf650624af635a79efbbbe6", null ],
    [ "algorithm", "d7/de6/classG4DNACrossSectionDataSet.html#a40e58c6bb121fed2bfdca1cfd302fe3b", null ],
    [ "components", "d7/de6/classG4DNACrossSectionDataSet.html#ae489bbaacc77d3107a6f520b4693a83f", null ],
    [ "unitData", "d7/de6/classG4DNACrossSectionDataSet.html#ab2ba6d9abd2d7229586fb490083454ef", null ],
    [ "unitEnergies", "d7/de6/classG4DNACrossSectionDataSet.html#ab15f5bf5ade0109f80f2b67f48219df9", null ],
    [ "z", "d7/de6/classG4DNACrossSectionDataSet.html#a9b271f365b39b5c4ad1c578ae92419ce", null ]
];