var classG4ParticleHPDataPoint =
[
    [ "G4ParticleHPDataPoint", "d7/d30/classG4ParticleHPDataPoint.html#a5a5d99a3efa31b7d77f35ab2941d902f", null ],
    [ "G4ParticleHPDataPoint", "d7/d30/classG4ParticleHPDataPoint.html#ad70d319c8bee83d3ecfb7465d4de7e9d", null ],
    [ "GetEnergy", "d7/d30/classG4ParticleHPDataPoint.html#a7df7168b9f0706ada525e75d416cba25", null ],
    [ "GetX", "d7/d30/classG4ParticleHPDataPoint.html#ab6e847d8497e9ae93bc33c36ed00f5e0", null ],
    [ "GetXsection", "d7/d30/classG4ParticleHPDataPoint.html#a515fdf3dab2125af7e074e1768cfd30f", null ],
    [ "GetY", "d7/d30/classG4ParticleHPDataPoint.html#a9c61f0852c92582e89a63f93d6c86ea5", null ],
    [ "operator=", "d7/d30/classG4ParticleHPDataPoint.html#a896b90090e4bf2df1c481b8994f3e8d5", null ],
    [ "SetData", "d7/d30/classG4ParticleHPDataPoint.html#aba9392893ce9d2da647a5f0e97d4a442", null ],
    [ "SetEnergy", "d7/d30/classG4ParticleHPDataPoint.html#abc4399f2f9d0a1422fb687da250f2d1c", null ],
    [ "SetX", "d7/d30/classG4ParticleHPDataPoint.html#a6e98c1ce52606f2d90e76e3b0a086be0", null ],
    [ "SetXsection", "d7/d30/classG4ParticleHPDataPoint.html#abffef808b393c82f58afc01b01335bab", null ],
    [ "SetY", "d7/d30/classG4ParticleHPDataPoint.html#a9bb3ee21d0ee875de40c9717c4f27ce7", null ],
    [ "energy", "d7/d30/classG4ParticleHPDataPoint.html#a5224012bfbca717664c4acbe3e337cf6", null ],
    [ "xSec", "d7/d30/classG4ParticleHPDataPoint.html#a497326d1412ff71341a7afceb1979de6", null ]
];