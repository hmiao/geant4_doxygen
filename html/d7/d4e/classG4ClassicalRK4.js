var classG4ClassicalRK4 =
[
    [ "G4ClassicalRK4", "d7/d4e/classG4ClassicalRK4.html#a43407a1a5fd69cd45ccf73bc94a58d74", null ],
    [ "~G4ClassicalRK4", "d7/d4e/classG4ClassicalRK4.html#a2d5b1db13def6d842aa98706cd13c4c7", null ],
    [ "G4ClassicalRK4", "d7/d4e/classG4ClassicalRK4.html#a5f15b5b7cd632ac3c925b6ce57392887", null ],
    [ "DumbStepper", "d7/d4e/classG4ClassicalRK4.html#a1f4e6604897ebc1d9581acacfea90789", null ],
    [ "IntegratorOrder", "d7/d4e/classG4ClassicalRK4.html#a7638ce261390afd2c950427fd759ca17", null ],
    [ "operator=", "d7/d4e/classG4ClassicalRK4.html#a8a917ad80b15aae95e1d8cd7051bc731", null ],
    [ "StepWithEst", "d7/d4e/classG4ClassicalRK4.html#a0771e50a6af67a45286fca99d9748f0d", null ],
    [ "dydxm", "d7/d4e/classG4ClassicalRK4.html#ab63c580ef42fb5910b5a894fc617ade6", null ],
    [ "dydxt", "d7/d4e/classG4ClassicalRK4.html#adc39431c03fc927f2954ca54186cbe6f", null ],
    [ "yt", "d7/d4e/classG4ClassicalRK4.html#ac48670b77ecd15a6c8dbe3516eddd850", null ]
];