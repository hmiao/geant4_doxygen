var classG4GDMLMatrix =
[
    [ "G4GDMLMatrix", "d7/dc6/classG4GDMLMatrix.html#a5aa4eda8594a8e28df88ae6fecbb5898", null ],
    [ "G4GDMLMatrix", "d7/dc6/classG4GDMLMatrix.html#a7a2ea687a6d5206b718a2142f36c712c", null ],
    [ "G4GDMLMatrix", "d7/dc6/classG4GDMLMatrix.html#a9bf8c61ce96ddbc599d4470af56a7937", null ],
    [ "~G4GDMLMatrix", "d7/dc6/classG4GDMLMatrix.html#a30e95817b189235ff33e7cd1052d3d95", null ],
    [ "Get", "d7/dc6/classG4GDMLMatrix.html#ac5ea9014933ecffb6b7750757e58cab1", null ],
    [ "GetCols", "d7/dc6/classG4GDMLMatrix.html#a22c9e301f0053dea8cabc8c8e10b2ce9", null ],
    [ "GetRows", "d7/dc6/classG4GDMLMatrix.html#ab690d1f4ca70044fc7d525c6f204115f", null ],
    [ "operator=", "d7/dc6/classG4GDMLMatrix.html#a5b6088de0efd5739adf9ade18ee19f1f", null ],
    [ "Set", "d7/dc6/classG4GDMLMatrix.html#a1f05b815c1122f60f1b55a26b32c15c0", null ],
    [ "cols", "d7/dc6/classG4GDMLMatrix.html#aa4b3d0aed0d4291f693a1ae63dc9caeb", null ],
    [ "m", "d7/dc6/classG4GDMLMatrix.html#ad1b9503f4a3f2a3384c07be91bea894a", null ],
    [ "rows", "d7/dc6/classG4GDMLMatrix.html#a447fc9bbbafdf60da9543512f659d253", null ]
];