var classG4VEmFluctuationModel =
[
    [ "G4VEmFluctuationModel", "d7/d5c/classG4VEmFluctuationModel.html#abd25d71be509584ac58807b53f78b470", null ],
    [ "~G4VEmFluctuationModel", "d7/d5c/classG4VEmFluctuationModel.html#a8a9f3f19ebe919e07722d28f90c71626", null ],
    [ "G4VEmFluctuationModel", "d7/d5c/classG4VEmFluctuationModel.html#a93e8c723aef458e0cd6643e7e3885e17", null ],
    [ "Dispersion", "d7/d5c/classG4VEmFluctuationModel.html#a0962c7d4817ddf333b44617f58740758", null ],
    [ "GetName", "d7/d5c/classG4VEmFluctuationModel.html#a869d8f331b17a3743a049c7747028606", null ],
    [ "InitialiseMe", "d7/d5c/classG4VEmFluctuationModel.html#ad0534cb0f8ee16118b54e658b569e209", null ],
    [ "operator=", "d7/d5c/classG4VEmFluctuationModel.html#a9b644a988d74abb9cb40dae56943afb0", null ],
    [ "SampleFluctuations", "d7/d5c/classG4VEmFluctuationModel.html#aa701b180f38fcf0e024f03afbb0d1ca7", null ],
    [ "SetParticleAndCharge", "d7/d5c/classG4VEmFluctuationModel.html#a4f89afcb1f32d42b63c0a58ea232b078", null ],
    [ "fManager", "d7/d5c/classG4VEmFluctuationModel.html#a1001120a7dada3df30bd8041d1c4d1b4", null ],
    [ "name", "d7/d5c/classG4VEmFluctuationModel.html#a0601738882cc9e7a14062a9e7ac22e5d", null ]
];