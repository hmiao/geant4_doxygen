var classG4VMaterialExtension =
[
    [ "G4VMaterialExtension", "d7/dd5/classG4VMaterialExtension.html#a324678b9a01dc470f3653323c220380d", null ],
    [ "~G4VMaterialExtension", "d7/dd5/classG4VMaterialExtension.html#a527ed67130d5776a2368d04d46859b1c", null ],
    [ "GetHash", "d7/dd5/classG4VMaterialExtension.html#afe5ac224ff9121793b77d6eab2479fc7", null ],
    [ "GetName", "d7/dd5/classG4VMaterialExtension.html#a1b43c43aeff3800196f230ac6f501883", null ],
    [ "Print", "d7/dd5/classG4VMaterialExtension.html#a2f147ce423b4e93d8ea0f1214dd9f7ab", null ],
    [ "fHash", "d7/dd5/classG4VMaterialExtension.html#a0d99f50872b1601edea2c29106542cbd", null ],
    [ "fName", "d7/dd5/classG4VMaterialExtension.html#af306e83c1e5d032a3880adb9a595bd1b", null ]
];