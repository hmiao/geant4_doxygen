var classG4VGFlashSensitiveDetector =
[
    [ "G4VGFlashSensitiveDetector", "d7/d90/classG4VGFlashSensitiveDetector.html#a1104b99777dbae21b1812576a3a9e7c4", null ],
    [ "G4VGFlashSensitiveDetector", "d7/d90/classG4VGFlashSensitiveDetector.html#a7ad7b8d80746f34f9332a91e633a2658", null ],
    [ "~G4VGFlashSensitiveDetector", "d7/d90/classG4VGFlashSensitiveDetector.html#ad873bb8db233595efb577d6b32663f8d", null ],
    [ "Hit", "d7/d90/classG4VGFlashSensitiveDetector.html#af28f09edb7818b9dc89dbea584694d84", null ],
    [ "operator!=", "d7/d90/classG4VGFlashSensitiveDetector.html#afde584224d97998aba4a7eff5d38339e", null ],
    [ "operator==", "d7/d90/classG4VGFlashSensitiveDetector.html#aae5b9e5e71c1774a37141ecb3ed0a3df", null ],
    [ "ProcessHits", "d7/d90/classG4VGFlashSensitiveDetector.html#a2338925baf1d4e50a794b1a583002f65", null ]
];