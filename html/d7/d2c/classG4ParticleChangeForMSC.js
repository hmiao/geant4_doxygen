var classG4ParticleChangeForMSC =
[
    [ "G4ParticleChangeForMSC", "d7/d2c/classG4ParticleChangeForMSC.html#a6549ed0950c381de3e8caa4c35269144", null ],
    [ "~G4ParticleChangeForMSC", "d7/d2c/classG4ParticleChangeForMSC.html#a6834be05119cea01959c75b6d85a5014", null ],
    [ "G4ParticleChangeForMSC", "d7/d2c/classG4ParticleChangeForMSC.html#a157c15b3ee4e4cd001e3ef2c69b88aed", null ],
    [ "CheckIt", "d7/d2c/classG4ParticleChangeForMSC.html#acaa18893249fdd17b759aec49b467335", null ],
    [ "DumpInfo", "d7/d2c/classG4ParticleChangeForMSC.html#aca0c8e065204f040d8745cc7ac34d7ca", null ],
    [ "GetMomentumDirection", "d7/d2c/classG4ParticleChangeForMSC.html#a814774e6420ef435c4a83984f98a2da9", null ],
    [ "GetPosition", "d7/d2c/classG4ParticleChangeForMSC.html#a9a77770e24f8a4d914d6258d2dcc22c5", null ],
    [ "GetProposedMomentumDirection", "d7/d2c/classG4ParticleChangeForMSC.html#a4eb3ffa0caad204764d2d57ec9f61aae", null ],
    [ "GetProposedPosition", "d7/d2c/classG4ParticleChangeForMSC.html#a05bac585c93260f045c517f63c2d2ce1", null ],
    [ "Initialize", "d7/d2c/classG4ParticleChangeForMSC.html#a0f2503475acc00d066716e7590365e81", null ],
    [ "operator=", "d7/d2c/classG4ParticleChangeForMSC.html#aaa1415b381c5d73edd48550f973bef29", null ],
    [ "ProposeMomentumDirection", "d7/d2c/classG4ParticleChangeForMSC.html#ab9fc294e979f3289b1ff49d41b4ad47a", null ],
    [ "ProposeMomentumDirection", "d7/d2c/classG4ParticleChangeForMSC.html#af99d1554344001dac69c73615b09ae66", null ],
    [ "ProposePosition", "d7/d2c/classG4ParticleChangeForMSC.html#afb652564edfb84fb1666f99e5b0d3985", null ],
    [ "SetProposedMomentumDirection", "d7/d2c/classG4ParticleChangeForMSC.html#ad98b2d983dafa9b4cc23abc68c2b12bd", null ],
    [ "SetProposedPosition", "d7/d2c/classG4ParticleChangeForMSC.html#a94d3d1898538d0b9f2ef1e57fa33e96d", null ],
    [ "UpdateStepForAlongStep", "d7/d2c/classG4ParticleChangeForMSC.html#a65706cfe7006917ed6a0e7caae24eea0", null ],
    [ "UpdateStepForPostStep", "d7/d2c/classG4ParticleChangeForMSC.html#a520bac7db9be66e92326430837910f80", null ],
    [ "theMomentumDirection", "d7/d2c/classG4ParticleChangeForMSC.html#aded04baa13e5e95cf0f6c7fd39a3620b", null ],
    [ "thePosition", "d7/d2c/classG4ParticleChangeForMSC.html#a3f76113ffd7052c2ca2f38d472866ae7", null ]
];