var classG4XnpTotal =
[
    [ "G4XnpTotal", "d7/d57/classG4XnpTotal.html#a847761a2ed0d96449ff8f8667c921f1b", null ],
    [ "~G4XnpTotal", "d7/d57/classG4XnpTotal.html#a2df4b19e4216c2c8e5d83273e46baf43", null ],
    [ "G4XnpTotal", "d7/d57/classG4XnpTotal.html#a9a1408b1e739903aab6c09c84a1db002", null ],
    [ "GetComponents", "d7/d57/classG4XnpTotal.html#a4c0e1f6afb523ee67cee9fb2034c3b8e", null ],
    [ "Name", "d7/d57/classG4XnpTotal.html#aedc8734e7d01d13eee1101088a833ede", null ],
    [ "operator!=", "d7/d57/classG4XnpTotal.html#ada01748d44383fa1f4c84ede1313efba", null ],
    [ "operator=", "d7/d57/classG4XnpTotal.html#aef692c4332c9711665e780b289309dac", null ],
    [ "operator==", "d7/d57/classG4XnpTotal.html#a4e69f4fa29001c79087891277f91565c", null ],
    [ "components", "d7/d57/classG4XnpTotal.html#a2d35c438641b4c887dd384c396e2ec80", null ]
];