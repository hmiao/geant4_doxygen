var classG4SmartVoxelStat =
[
    [ "G4SmartVoxelStat", "d7/d68/classG4SmartVoxelStat.html#a1d0feb4e033e1133c792fc9249cc9092", null ],
    [ "CountHeadsAndNodes", "d7/d68/classG4SmartVoxelStat.html#a35b2e2d4370e4d737d5de3eafe82c42d", null ],
    [ "GetMemoryUse", "d7/d68/classG4SmartVoxelStat.html#a9d898b96f0355619e89aa5a4b7a4efa5", null ],
    [ "GetNumberHeads", "d7/d68/classG4SmartVoxelStat.html#ac77a53f327d95ba375fa2c14c98e9c8c", null ],
    [ "GetNumberNodes", "d7/d68/classG4SmartVoxelStat.html#afc70507df564f6303da48af1db47eec7", null ],
    [ "GetNumberPointers", "d7/d68/classG4SmartVoxelStat.html#abef42fe429846c40f5493c9111b1e61d", null ],
    [ "GetSysTime", "d7/d68/classG4SmartVoxelStat.html#ad0af34ea90bc95cbd074382552fdc2f6", null ],
    [ "GetTotalTime", "d7/d68/classG4SmartVoxelStat.html#ab19eda3d2fe40df186860792c4c42372", null ],
    [ "GetUserTime", "d7/d68/classG4SmartVoxelStat.html#a2fa7b593e6905f5559fdd7751ad01695", null ],
    [ "GetVolume", "d7/d68/classG4SmartVoxelStat.html#af15675dc0778f3e0542ab9cf351c41aa", null ],
    [ "GetVoxel", "d7/d68/classG4SmartVoxelStat.html#a1216228787d7f5e09703674172ff4037", null ],
    [ "heads", "d7/d68/classG4SmartVoxelStat.html#aaefc6677feb39d6a1f07b62627108afd", null ],
    [ "nodes", "d7/d68/classG4SmartVoxelStat.html#a9d9ea131867b3ea2e02c5869aafe4643", null ],
    [ "pointers", "d7/d68/classG4SmartVoxelStat.html#a50c97eba7bdf3883f42ae6e34c679ff8", null ],
    [ "sysTime", "d7/d68/classG4SmartVoxelStat.html#abe2061fce62359a01ce36ff022761177", null ],
    [ "userTime", "d7/d68/classG4SmartVoxelStat.html#ad1b9a8e782eff8d5e3a55f07c13e69ed", null ],
    [ "volume", "d7/d68/classG4SmartVoxelStat.html#ab8fd7b1a8e7b31a33162ae0c220b9e89", null ],
    [ "voxel", "d7/d68/classG4SmartVoxelStat.html#aa72d257e3bc58229825ddf939bcb9bff", null ]
];