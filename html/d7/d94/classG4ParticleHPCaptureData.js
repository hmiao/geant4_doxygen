var classG4ParticleHPCaptureData =
[
    [ "G4ParticleHPCaptureData", "d7/d94/classG4ParticleHPCaptureData.html#a88e93879df16ac3eafc8f8fda34b60da", null ],
    [ "~G4ParticleHPCaptureData", "d7/d94/classG4ParticleHPCaptureData.html#ae1fbd27834961a9bf2df6e5b1b4ec789", null ],
    [ "BuildPhysicsTable", "d7/d94/classG4ParticleHPCaptureData.html#abab76c773c7954ab78f2c987167a75b0", null ],
    [ "CrossSectionDescription", "d7/d94/classG4ParticleHPCaptureData.html#a09511c02555a3e1712238b53319979d9", null ],
    [ "DumpPhysicsTable", "d7/d94/classG4ParticleHPCaptureData.html#aa71a175d2a244abed7dd9b37ab031969", null ],
    [ "GetCrossSection", "d7/d94/classG4ParticleHPCaptureData.html#a64fe4fa8dab4c1142c1948163d8ae026", null ],
    [ "GetIsoCrossSection", "d7/d94/classG4ParticleHPCaptureData.html#a0bd9a05b31f4c762bc58a471b134c80e", null ],
    [ "GetVerboseLevel", "d7/d94/classG4ParticleHPCaptureData.html#afee644a0ab0e7dd3b0b45219a684b2df", null ],
    [ "IsIsoApplicable", "d7/d94/classG4ParticleHPCaptureData.html#a4b5f2ad6712e678e49db2dfe2cf4ca00", null ],
    [ "SetVerboseLevel", "d7/d94/classG4ParticleHPCaptureData.html#a7510d64352f764b946b6363de26e8aa3", null ],
    [ "element_cache", "d7/d94/classG4ParticleHPCaptureData.html#a5b2c1721e8bc6e70d41dbfc1a9b1bdd3", null ],
    [ "instanceOfWorker", "d7/d94/classG4ParticleHPCaptureData.html#a9c8f704e1a7696caa0c4ef8c42b857f8", null ],
    [ "ke_cache", "d7/d94/classG4ParticleHPCaptureData.html#a78500c14b31bb1defcea66f486321408", null ],
    [ "material_cache", "d7/d94/classG4ParticleHPCaptureData.html#a2bbb9f602700113df90366af60ee87a1", null ],
    [ "theCrossSections", "d7/d94/classG4ParticleHPCaptureData.html#aff4d7653497f99c8e351b3da24b89b5f", null ],
    [ "xs_cache", "d7/d94/classG4ParticleHPCaptureData.html#a428e629850488522dad0a88e9ac987bd", null ]
];