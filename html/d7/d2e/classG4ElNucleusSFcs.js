var classG4ElNucleusSFcs =
[
    [ "G4ElNucleusSFcs", "d7/d2e/classG4ElNucleusSFcs.html#a8f065e5df41010cf095be6da40272135", null ],
    [ "~G4ElNucleusSFcs", "d7/d2e/classG4ElNucleusSFcs.html#a84867f399c8fe39af507a3810a14191b", null ],
    [ "CrossSectionDescription", "d7/d2e/classG4ElNucleusSFcs.html#a50bb1862f1a82e2d486064fafd833fec", null ],
    [ "Default_Name", "d7/d2e/classG4ElNucleusSFcs.html#aedfed7d6f7b460ccef8aef06e4e30d40", null ],
    [ "GetIsoCrossSection", "d7/d2e/classG4ElNucleusSFcs.html#a1f29fab301759590e9f936916abf3864", null ],
    [ "GetRatio", "d7/d2e/classG4ElNucleusSFcs.html#aca98537cba08eeea1d2e6de36869ec8d", null ],
    [ "IsElementApplicable", "d7/d2e/classG4ElNucleusSFcs.html#a64074a76043b1f7ee119c44d034b8f34", null ],
    [ "ThresholdEnergy", "d7/d2e/classG4ElNucleusSFcs.html#a6a5e53237f33ed8cd3327fc4952fe691", null ],
    [ "fCHIPScs", "d7/d2e/classG4ElNucleusSFcs.html#ade0db84e1fd7c9ca77ce5be9f81fd6f3", null ],
    [ "fRR", "d7/d2e/classG4ElNucleusSFcs.html#abebce89f0a87215d5b6a8daea0810f0b", null ],
    [ "fZZ", "d7/d2e/classG4ElNucleusSFcs.html#ae84dd7845b68501b764089869c13f710", null ]
];