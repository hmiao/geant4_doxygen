var classG4VPhononProcess =
[
    [ "G4VPhononProcess", "d7/d9e/classG4VPhononProcess.html#a3787b6aecd76a471b80409492ceaa8db", null ],
    [ "~G4VPhononProcess", "d7/d9e/classG4VPhononProcess.html#acf056129fbbb5783c47d8e978d7ef8f4", null ],
    [ "G4VPhononProcess", "d7/d9e/classG4VPhononProcess.html#a30cefc09f6e499ce8a8c505aea5165f2", null ],
    [ "ChoosePolarization", "d7/d9e/classG4VPhononProcess.html#a9a027a6fdc1b3a454d419d8af3dfa612", null ],
    [ "CreateSecondary", "d7/d9e/classG4VPhononProcess.html#a9fa1887575b357d80888a380c6f5d46e", null ],
    [ "EndTracking", "d7/d9e/classG4VPhononProcess.html#a7ec69e5d8f0f9b3eb6a291792a57539d", null ],
    [ "GetPolarization", "d7/d9e/classG4VPhononProcess.html#a6378be328df1bee40b361c2070894709", null ],
    [ "GetPolarization", "d7/d9e/classG4VPhononProcess.html#a1ae0c0d802c20254c435437055570c60", null ],
    [ "IsApplicable", "d7/d9e/classG4VPhononProcess.html#ac20949a6a44c7ce36dcf52b0183eb88f", null ],
    [ "operator=", "d7/d9e/classG4VPhononProcess.html#ad269c15737b8bfef06ff0594f3363c37", null ],
    [ "StartTracking", "d7/d9e/classG4VPhononProcess.html#a06574d7041907595c1941b1a3ecfbf5c", null ],
    [ "currentTrack", "d7/d9e/classG4VPhononProcess.html#a7100e6d93e050813817d16ce41c074dd", null ],
    [ "theLattice", "d7/d9e/classG4VPhononProcess.html#aa14389002d872527fc45e236fb95b851", null ],
    [ "trackKmap", "d7/d9e/classG4VPhononProcess.html#afa0f087465e49afba96dd2f9545163ad", null ]
];