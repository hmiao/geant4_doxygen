var classG4hBremsstrahlung =
[
    [ "G4hBremsstrahlung", "d7/d4c/classG4hBremsstrahlung.html#a429e29308aa4e41af174af1198504dca", null ],
    [ "~G4hBremsstrahlung", "d7/d4c/classG4hBremsstrahlung.html#a7563bcefa02c1a36b8e029e1565c0333", null ],
    [ "G4hBremsstrahlung", "d7/d4c/classG4hBremsstrahlung.html#aac2efd940350a24a6a391981cc59649c", null ],
    [ "InitialiseEnergyLossProcess", "d7/d4c/classG4hBremsstrahlung.html#abc39fed04505d81ae5cc9799fee4c24e", null ],
    [ "IsApplicable", "d7/d4c/classG4hBremsstrahlung.html#a7176f1b4eac9c79d7a9c521fff683f86", null ],
    [ "operator=", "d7/d4c/classG4hBremsstrahlung.html#a08089678e4696d01dec6ba7b2a0c45aa", null ],
    [ "ProcessDescription", "d7/d4c/classG4hBremsstrahlung.html#adde5aedfddeb5be5b93cd477e2325dc8", null ]
];