var structG4TFileInformation =
[
    [ "G4TFileInformation", "d7/d0f/structG4TFileInformation.html#aac5d76a7e15188a90b45fbcb35bd384c", null ],
    [ "G4TFileInformation", "d7/d0f/structG4TFileInformation.html#a8ae53214f6aae6ae6ab0fcb702e080aa", null ],
    [ "~G4TFileInformation", "d7/d0f/structG4TFileInformation.html#aab8ae94bcf458a348268ff8d31f46b1d", null ],
    [ "fFile", "d7/d0f/structG4TFileInformation.html#a6bd699c158c7cfee58b3011823f718fe", null ],
    [ "fFileName", "d7/d0f/structG4TFileInformation.html#a789b84d5fffc23505f4cd433c97af7ca", null ],
    [ "fIsDeleted", "d7/d0f/structG4TFileInformation.html#add65245532b4d93354b49ab72662f8ac", null ],
    [ "fIsEmpty", "d7/d0f/structG4TFileInformation.html#a52ec23dfbf69e84929582b67390f7d82", null ],
    [ "fIsOpen", "d7/d0f/structG4TFileInformation.html#a87ca9a26732fbde7089f43820e1f8075", null ]
];