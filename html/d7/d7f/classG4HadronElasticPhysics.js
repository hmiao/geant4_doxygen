var classG4HadronElasticPhysics =
[
    [ "G4HadronElasticPhysics", "d7/d7f/classG4HadronElasticPhysics.html#a93c5d8f7fe0aa458c16202884c9c8338", null ],
    [ "~G4HadronElasticPhysics", "d7/d7f/classG4HadronElasticPhysics.html#adb8a38d6dcb4e300fd8e47e63d325656", null ],
    [ "G4HadronElasticPhysics", "d7/d7f/classG4HadronElasticPhysics.html#a2755942ee317a3b59288a0cfb2598175", null ],
    [ "AddXSection", "d7/d7f/classG4HadronElasticPhysics.html#aee4da704b9ef14e3d290825a436c3cd4", null ],
    [ "ConstructParticle", "d7/d7f/classG4HadronElasticPhysics.html#a3f031f167234450c4275257470213e2e", null ],
    [ "ConstructProcess", "d7/d7f/classG4HadronElasticPhysics.html#a845014d328ea273fe944a1444933578f", null ],
    [ "GetElasticModel", "d7/d7f/classG4HadronElasticPhysics.html#a154a56c1d872ffcddb8c68a8dbe0dc61", null ],
    [ "GetElasticProcess", "d7/d7f/classG4HadronElasticPhysics.html#ae5dd9cb2701e8dc10077dfea50ceba53", null ],
    [ "GetNeutronModel", "d7/d7f/classG4HadronElasticPhysics.html#a2478c0cad0d08d3625415b3ae8764152", null ],
    [ "GetNeutronProcess", "d7/d7f/classG4HadronElasticPhysics.html#a2c38e3cfd4d28e1856b863fd3648bac3", null ],
    [ "operator=", "d7/d7f/classG4HadronElasticPhysics.html#aaba08884da54b2a2cd175d169bc0fdff", null ]
];