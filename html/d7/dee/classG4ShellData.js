var classG4ShellData =
[
    [ "G4ShellData", "d7/dee/classG4ShellData.html#ac2425cee57a2f352686dba6478124505", null ],
    [ "~G4ShellData", "d7/dee/classG4ShellData.html#a6d66a20032b293e5d9d1cc8cd32cdadc", null ],
    [ "G4ShellData", "d7/dee/classG4ShellData.html#a7164c258e9b24e81ba3aee4bc78d4d33", null ],
    [ "BindingEnergy", "d7/dee/classG4ShellData.html#a975fc24b655f5b2d20c9a8b776936af7", null ],
    [ "LoadData", "d7/dee/classG4ShellData.html#ac78d8e8d0315d9767d2cc8585a119e63", null ],
    [ "NumberOfShells", "d7/dee/classG4ShellData.html#aa5b88afb69cba46cf15f8c927d0773c5", null ],
    [ "operator=", "d7/dee/classG4ShellData.html#a8c69e4fcab32f820236cf8288d06766e", null ],
    [ "PrintData", "d7/dee/classG4ShellData.html#a234c2af548b3135fd06c8952515b722c", null ],
    [ "SelectRandomShell", "d7/dee/classG4ShellData.html#ae3c347a2bc9f818ea290f6fdc92039fa", null ],
    [ "SetOccupancyData", "d7/dee/classG4ShellData.html#a8282ab6691bae288a2f6e67057ec194c", null ],
    [ "ShellId", "d7/dee/classG4ShellData.html#a22b63f4f2a857a0d704ab8aceb19a4a3", null ],
    [ "ShellIdVector", "d7/dee/classG4ShellData.html#ace5ab1ce667b57503ba6c9f3d1796500", null ],
    [ "ShellOccupancyProbability", "d7/dee/classG4ShellData.html#ac5b3152f6e33800812e26486f4882b4a", null ],
    [ "ShellVector", "d7/dee/classG4ShellData.html#a14ce904f60abbefa9945bb6a9e27a246", null ],
    [ "bindingMap", "d7/dee/classG4ShellData.html#a12331ef45e71a8084376f0226f345e33", null ],
    [ "idMap", "d7/dee/classG4ShellData.html#a3c003cd86820e0ed3bbcdea18a473ac7", null ],
    [ "nShells", "d7/dee/classG4ShellData.html#a63abafa175f9c7397667123b750febc0", null ],
    [ "occupancyData", "d7/dee/classG4ShellData.html#ac834c58db855aa43d79aefb396bf5d56", null ],
    [ "occupancyPdfMap", "d7/dee/classG4ShellData.html#ae02b8cc9c5e4991d91030a2b872c88da", null ],
    [ "zMax", "d7/dee/classG4ShellData.html#ae89a4fe84693e9e899fc614b1e789f0e", null ],
    [ "zMin", "d7/dee/classG4ShellData.html#ade4a77c6ad8be54ab5c3806fae131d8d", null ]
];