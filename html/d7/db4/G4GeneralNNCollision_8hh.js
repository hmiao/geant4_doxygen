var G4GeneralNNCollision_8hh =
[
    [ "G4GeneralNNCollision", "d5/d15/classG4GeneralNNCollision.html", "d5/d15/classG4GeneralNNCollision" ],
    [ "G4GeneralNNCollision::MakeNNToNDelta< dm, d0, dp, dpp, channelType >", "d4/dcc/structG4GeneralNNCollision_1_1MakeNNToNDelta.html", "d4/dcc/structG4GeneralNNCollision_1_1MakeNNToNDelta" ],
    [ "G4GeneralNNCollision::MakeNNToNNStar< Np, Nn, channelType >", "d2/d67/structG4GeneralNNCollision_1_1MakeNNToNNStar.html", "d2/d67/structG4GeneralNNCollision_1_1MakeNNToNNStar" ],
    [ "G4GeneralNNCollision::MakeNNStarToNN< channelType, Np, Nn >", "d9/dc8/structG4GeneralNNCollision_1_1MakeNNStarToNN.html", "d9/dc8/structG4GeneralNNCollision_1_1MakeNNStarToNN" ],
    [ "G4GeneralNNCollision::MakeNNToDeltaNstar< Np, channelType, Nn >", "d6/d6d/structG4GeneralNNCollision_1_1MakeNNToDeltaNstar.html", "d6/d6d/structG4GeneralNNCollision_1_1MakeNNToDeltaNstar" ],
    [ "G4GeneralNNCollision::MakeNNToDeltaDelta< dm, d0, dp, dpp, channelType >", "d5/d93/structG4GeneralNNCollision_1_1MakeNNToDeltaDelta.html", "d5/d93/structG4GeneralNNCollision_1_1MakeNNToDeltaDelta" ]
];