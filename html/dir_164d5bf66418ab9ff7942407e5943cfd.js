var dir_164d5bf66418ab9ff7942407e5943cfd =
[
    [ "G4AtomicBond.hh", "d0/d6a/G4AtomicBond_8hh.html", "d0/d6a/G4AtomicBond_8hh" ],
    [ "G4AtomicFormFactor.hh", "d1/d63/G4AtomicFormFactor_8hh.html", "d1/d63/G4AtomicFormFactor_8hh" ],
    [ "G4AtomicShells.hh", "d6/d50/G4AtomicShells_8hh.html", "d6/d50/G4AtomicShells_8hh" ],
    [ "G4AtomicShells_XDB_EADL.hh", "d1/deb/G4AtomicShells__XDB__EADL_8hh.html", "d1/deb/G4AtomicShells__XDB__EADL_8hh" ],
    [ "G4CrystalAtomBase.hh", "d0/d31/G4CrystalAtomBase_8hh.html", "d0/d31/G4CrystalAtomBase_8hh" ],
    [ "G4CrystalBravaisLattices.h", "df/d63/G4CrystalBravaisLattices_8h.html", "df/d63/G4CrystalBravaisLattices_8h" ],
    [ "G4CrystalExtension.hh", "d6/d19/G4CrystalExtension_8hh.html", "d6/d19/G4CrystalExtension_8hh" ],
    [ "G4CrystalLatticeSystems.h", "d4/d9e/G4CrystalLatticeSystems_8h.html", "d4/d9e/G4CrystalLatticeSystems_8h" ],
    [ "G4CrystalUnitCell.hh", "db/d63/G4CrystalUnitCell_8hh.html", "db/d63/G4CrystalUnitCell_8hh" ],
    [ "G4DensityEffectCalculator.hh", "de/d4f/G4DensityEffectCalculator_8hh.html", "de/d4f/G4DensityEffectCalculator_8hh" ],
    [ "G4DensityEffectData.hh", "d0/dac/G4DensityEffectData_8hh.html", "d0/dac/G4DensityEffectData_8hh" ],
    [ "G4Element.hh", "dc/d43/G4Element_8hh.html", "dc/d43/G4Element_8hh" ],
    [ "G4ElementData.hh", "d4/d71/G4ElementData_8hh.html", "d4/d71/G4ElementData_8hh" ],
    [ "G4ElementTable.hh", "d8/ddd/G4ElementTable_8hh.html", "d8/ddd/G4ElementTable_8hh" ],
    [ "G4ElementVector.hh", "d1/d10/G4ElementVector_8hh.html", "d1/d10/G4ElementVector_8hh" ],
    [ "G4ExtDEDXTable.hh", "da/dd6/G4ExtDEDXTable_8hh.html", "da/dd6/G4ExtDEDXTable_8hh" ],
    [ "G4ExtendedMaterial.hh", "d8/dfd/G4ExtendedMaterial_8hh.html", "d8/dfd/G4ExtendedMaterial_8hh" ],
    [ "G4ICRU90StoppingData.hh", "d7/d75/G4ICRU90StoppingData_8hh.html", "d7/d75/G4ICRU90StoppingData_8hh" ],
    [ "G4IonisParamElm.hh", "d3/d76/G4IonisParamElm_8hh.html", "d3/d76/G4IonisParamElm_8hh" ],
    [ "G4IonisParamMat.hh", "d6/d29/G4IonisParamMat_8hh.html", "d6/d29/G4IonisParamMat_8hh" ],
    [ "G4IonStoppingData.hh", "de/dcb/G4IonStoppingData_8hh.html", "de/dcb/G4IonStoppingData_8hh" ],
    [ "G4Isotope.hh", "d3/d2d/G4Isotope_8hh.html", "d3/d2d/G4Isotope_8hh" ],
    [ "G4IsotopeVector.hh", "db/da9/G4IsotopeVector_8hh.html", "db/da9/G4IsotopeVector_8hh" ],
    [ "G4LatticeLogical.hh", "da/d24/G4LatticeLogical_8hh.html", "da/d24/G4LatticeLogical_8hh" ],
    [ "G4LatticePhysical.hh", "d6/d3a/G4LatticePhysical_8hh.html", "d6/d3a/G4LatticePhysical_8hh" ],
    [ "G4Material.hh", "d4/dc8/G4Material_8hh.html", "d4/dc8/G4Material_8hh" ],
    [ "G4MaterialPropertiesIndex.hh", "da/d68/G4MaterialPropertiesIndex_8hh.html", "da/d68/G4MaterialPropertiesIndex_8hh" ],
    [ "G4MaterialPropertiesTable.hh", "df/d70/G4MaterialPropertiesTable_8hh.html", "df/d70/G4MaterialPropertiesTable_8hh" ],
    [ "G4MaterialPropertyVector.hh", "dd/dd6/G4MaterialPropertyVector_8hh.html", "dd/dd6/G4MaterialPropertyVector_8hh" ],
    [ "G4MaterialTable.hh", "d1/db0/G4MaterialTable_8hh.html", "d1/db0/G4MaterialTable_8hh" ],
    [ "G4MicroElecMaterialStructure.hh", "d5/dc5/G4MicroElecMaterialStructure_8hh.html", "d5/dc5/G4MicroElecMaterialStructure_8hh" ],
    [ "G4MicroElecSiStructure.hh", "de/d7b/G4MicroElecSiStructure_8hh.html", "de/d7b/G4MicroElecSiStructure_8hh" ],
    [ "G4NistElementBuilder.hh", "dd/d4a/G4NistElementBuilder_8hh.html", "dd/d4a/G4NistElementBuilder_8hh" ],
    [ "G4NistManager.hh", "d9/de1/G4NistManager_8hh.html", "d9/de1/G4NistManager_8hh" ],
    [ "G4NistMaterialBuilder.hh", "d7/dae/G4NistMaterialBuilder_8hh.html", "d7/dae/G4NistMaterialBuilder_8hh" ],
    [ "G4NistMessenger.hh", "dd/d6a/G4NistMessenger_8hh.html", "dd/d6a/G4NistMessenger_8hh" ],
    [ "G4OpticalMaterialProperties.hh", "d8/dda/G4OpticalMaterialProperties_8hh.html", "d8/dda/G4OpticalMaterialProperties_8hh" ],
    [ "G4OpticalSurface.hh", "d9/d8d/G4OpticalSurface_8hh.html", "d9/d8d/G4OpticalSurface_8hh" ],
    [ "G4SandiaTable.hh", "d3/d36/G4SandiaTable_8hh.html", "d3/d36/G4SandiaTable_8hh" ],
    [ "G4StaticSandiaData.hh", "d0/dd6/G4StaticSandiaData_8hh.html", "d0/dd6/G4StaticSandiaData_8hh" ],
    [ "G4SurfaceProperty.hh", "dd/db6/G4SurfaceProperty_8hh.html", "dd/db6/G4SurfaceProperty_8hh" ],
    [ "G4UCNMaterialPropertiesTable.hh", "dc/d69/G4UCNMaterialPropertiesTable_8hh.html", "dc/d69/G4UCNMaterialPropertiesTable_8hh" ],
    [ "G4UCNMicroRoughnessHelper.hh", "de/d29/G4UCNMicroRoughnessHelper_8hh.html", "de/d29/G4UCNMicroRoughnessHelper_8hh" ],
    [ "G4VIonDEDXTable.hh", "d1/dfb/G4VIonDEDXTable_8hh.html", "d1/dfb/G4VIonDEDXTable_8hh" ],
    [ "G4VMaterialExtension.hh", "da/dfc/G4VMaterialExtension_8hh.html", "da/dfc/G4VMaterialExtension_8hh" ]
];