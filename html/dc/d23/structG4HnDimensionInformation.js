var structG4HnDimensionInformation =
[
    [ "G4HnDimensionInformation", "dc/d23/structG4HnDimensionInformation.html#ac9f75a84e3dcfbaddf946ae282147a79", null ],
    [ "G4HnDimensionInformation", "dc/d23/structG4HnDimensionInformation.html#a8fe0cab1b0f5a21107bd2bc51116ba03", null ],
    [ "G4HnDimensionInformation", "dc/d23/structG4HnDimensionInformation.html#a66daf7408ecf1c7535cccb1f03cedb13", null ],
    [ "operator=", "dc/d23/structG4HnDimensionInformation.html#ac1ba13469ae10bbd9e49b5705e6efb96", null ],
    [ "fBinScheme", "dc/d23/structG4HnDimensionInformation.html#ad9edbcfe2a293c1068992693d959c939", null ],
    [ "fFcn", "dc/d23/structG4HnDimensionInformation.html#ad85ade37958ce77dee5f0e6e42e92aa6", null ],
    [ "fFcnName", "dc/d23/structG4HnDimensionInformation.html#ad2c33022b5b6c7ebb80aa7be054041d8", null ],
    [ "fUnit", "dc/d23/structG4HnDimensionInformation.html#aec2e4f72ece20810aab917075f2511f6", null ],
    [ "fUnitName", "dc/d23/structG4HnDimensionInformation.html#a238579f2aa0aa2b2abdb46641424cc0f", null ]
];