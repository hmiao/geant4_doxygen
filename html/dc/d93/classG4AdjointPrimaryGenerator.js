var classG4AdjointPrimaryGenerator =
[
    [ "G4AdjointPrimaryGenerator", "dc/d93/classG4AdjointPrimaryGenerator.html#aa1a4fd36fe13ccc32c5658a07d7420df", null ],
    [ "~G4AdjointPrimaryGenerator", "dc/d93/classG4AdjointPrimaryGenerator.html#aca0acf8326095eb2f43dca869f5c6fb8", null ],
    [ "G4AdjointPrimaryGenerator", "dc/d93/classG4AdjointPrimaryGenerator.html#a69a1e27f4455ca27795442a8dfefa241", null ],
    [ "ComputeAccumulatedDepthVectorAlongBackRay", "dc/d93/classG4AdjointPrimaryGenerator.html#a9ac32cca1ec0af5fa8d7badf45e982c6", null ],
    [ "GenerateAdjointPrimaryVertex", "dc/d93/classG4AdjointPrimaryGenerator.html#a7b685604fbcf7f8265ee3e91146ed628", null ],
    [ "GenerateFwdPrimaryVertex", "dc/d93/classG4AdjointPrimaryGenerator.html#a8a91cf18718187801f85d9fde599bda0", null ],
    [ "operator=", "dc/d93/classG4AdjointPrimaryGenerator.html#ab29c02b2b77b588a7d8dda9c783d6860", null ],
    [ "SampleDistanceAlongBackRayAndComputeWeightCorrection", "dc/d93/classG4AdjointPrimaryGenerator.html#a83bf495b7a66929d7aebccf82f00a19a", null ],
    [ "SetAdjointPrimarySourceOnAnExtSurfaceOfAVolume", "dc/d93/classG4AdjointPrimaryGenerator.html#a617893ba51ae2de04927ef3e93d3bf9a", null ],
    [ "SetSphericalAdjointPrimarySource", "dc/d93/classG4AdjointPrimaryGenerator.html#a50ec03c4d53ddd8021debfa4800be75c", null ],
    [ "center_spherical_source", "dc/d93/classG4AdjointPrimaryGenerator.html#a0015fc8f5ef9caa5e9afa4f327f1b1a7", null ],
    [ "fLinearNavigator", "dc/d93/classG4AdjointPrimaryGenerator.html#af59114f2d0be530790d70d6fe8afe3a2", null ],
    [ "radius_spherical_source", "dc/d93/classG4AdjointPrimaryGenerator.html#a51f1f16aa20f0289ed8c33f9d1524df7", null ],
    [ "theAccumulatedDepthVector", "dc/d93/classG4AdjointPrimaryGenerator.html#a7f7763ee59c938bc7aa105cd8a05d1a5", null ],
    [ "theG4AdjointPosOnPhysVolGenerator", "dc/d93/classG4AdjointPrimaryGenerator.html#a238304f0cdf710693997ecc33b7574cf", null ],
    [ "theSingleParticleSource", "dc/d93/classG4AdjointPrimaryGenerator.html#a590371ceb99c0884ab59a830c29835cd", null ],
    [ "type_of_adjoint_source", "dc/d93/classG4AdjointPrimaryGenerator.html#a9c505e10e8d7627d0ce0f5a70aabdbad", null ]
];