var classG4BinaryDeuteronBuilder =
[
    [ "G4BinaryDeuteronBuilder", "dc/dc1/classG4BinaryDeuteronBuilder.html#aedf31b2753115c484c6dff9cf722945e", null ],
    [ "~G4BinaryDeuteronBuilder", "dc/dc1/classG4BinaryDeuteronBuilder.html#abe118c4a8151817d34bca6a3852eabb4", null ],
    [ "Build", "dc/dc1/classG4BinaryDeuteronBuilder.html#accea21e6617f515f60a8af99e0b43b10", null ],
    [ "Build", "dc/dc1/classG4BinaryDeuteronBuilder.html#a9c6625814c319208f9a6cd03fceccbd8", null ],
    [ "Build", "dc/dc1/classG4BinaryDeuteronBuilder.html#aa4cfc9ba629a7034c3204765d2bad80b", null ],
    [ "Build", "dc/dc1/classG4BinaryDeuteronBuilder.html#abe046cd9f98fa1ebc62b4cbe2ca432a9", null ],
    [ "SetMaxEnergy", "dc/dc1/classG4BinaryDeuteronBuilder.html#a00ee6d109103dc97969ad592e879996e", null ],
    [ "SetMinEnergy", "dc/dc1/classG4BinaryDeuteronBuilder.html#a43078ac39fb4e92827d17e5e48b805e4", null ],
    [ "theMax", "dc/dc1/classG4BinaryDeuteronBuilder.html#ad28dbd6ff1ba264cb930aee46b05c734", null ],
    [ "theMin", "dc/dc1/classG4BinaryDeuteronBuilder.html#a51fe46bb9c93503620b9f31c3a6dade7", null ],
    [ "theModel", "dc/dc1/classG4BinaryDeuteronBuilder.html#aef5fa1515cb08b21afb48a937d678f5d", null ]
];