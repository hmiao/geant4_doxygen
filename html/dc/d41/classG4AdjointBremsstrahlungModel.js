var classG4AdjointBremsstrahlungModel =
[
    [ "G4AdjointBremsstrahlungModel", "dc/d41/classG4AdjointBremsstrahlungModel.html#a9b6836b4931ec2fe53078d845d057478", null ],
    [ "G4AdjointBremsstrahlungModel", "dc/d41/classG4AdjointBremsstrahlungModel.html#aad30a16d9b0dfaafb247bce0989f8e5d", null ],
    [ "~G4AdjointBremsstrahlungModel", "dc/d41/classG4AdjointBremsstrahlungModel.html#a0078745606dba48a29d9ed66a8d7b300", null ],
    [ "G4AdjointBremsstrahlungModel", "dc/d41/classG4AdjointBremsstrahlungModel.html#a6c5d40fc56d3a1439610308d7f659ef3", null ],
    [ "AdjointCrossSection", "dc/d41/classG4AdjointBremsstrahlungModel.html#a2bd334792f9ffcf65ac370b61a069363", null ],
    [ "DiffCrossSectionPerVolumePrimToSecond", "dc/d41/classG4AdjointBremsstrahlungModel.html#a06cd2cbc8404f33f784e522e103e5a7c", null ],
    [ "Initialize", "dc/d41/classG4AdjointBremsstrahlungModel.html#aca0bd18d612a30fa346b126290b9d7d6", null ],
    [ "operator=", "dc/d41/classG4AdjointBremsstrahlungModel.html#a289edd54fdc1fce0ce2294b8e584f6bc", null ],
    [ "RapidSampleSecondaries", "dc/d41/classG4AdjointBremsstrahlungModel.html#a2c197302b7ee5b033116c07e2961f9ec", null ],
    [ "SampleSecondaries", "dc/d41/classG4AdjointBremsstrahlungModel.html#a02415c2b90d8d94f7adfdecc2dd7d437", null ],
    [ "fCSManager", "dc/d41/classG4AdjointBremsstrahlungModel.html#aaed57fca84ec4f302b2ed66c267484be", null ],
    [ "fElectron", "dc/d41/classG4AdjointBremsstrahlungModel.html#ac1694ac434b1dfc0d7e70f225f14c290", null ],
    [ "fEmModelManagerForFwdModels", "dc/d41/classG4AdjointBremsstrahlungModel.html#aa99166258b8c7a51d0189ae269f16237", null ],
    [ "fGamma", "dc/d41/classG4AdjointBremsstrahlungModel.html#a6d7934db2e8caf045cfd69207de698c5", null ],
    [ "fIsDirectModelInitialised", "dc/d41/classG4AdjointBremsstrahlungModel.html#ae5ca8c03cad39dafe70384c331e72b3f", null ],
    [ "fLastCZ", "dc/d41/classG4AdjointBremsstrahlungModel.html#a33bcb5054e73ce33a82bb4505f263920", null ]
];