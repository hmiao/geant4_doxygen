var classG4DNAPlasmonExcitation =
[
    [ "G4DNAPlasmonExcitation", "dc/d11/classG4DNAPlasmonExcitation.html#a6adf5514f40b81694c75157bd089421f", null ],
    [ "~G4DNAPlasmonExcitation", "dc/d11/classG4DNAPlasmonExcitation.html#a90307ecce6bd76209ef9d8960fea8e10", null ],
    [ "InitialiseProcess", "dc/d11/classG4DNAPlasmonExcitation.html#a47ec1ecc11e05cfac8f3122b7ed3938d", null ],
    [ "IsApplicable", "dc/d11/classG4DNAPlasmonExcitation.html#a4c40f77a29560710a45cd7bd0a07583e", null ],
    [ "PrintInfo", "dc/d11/classG4DNAPlasmonExcitation.html#a5f69f2cc6a1490e7ee68d8705704d6a1", null ],
    [ "isInitialised", "dc/d11/classG4DNAPlasmonExcitation.html#a2b7062faef62bb9f704011b044901ddb", null ]
];