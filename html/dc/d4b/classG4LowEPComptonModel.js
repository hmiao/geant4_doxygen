var classG4LowEPComptonModel =
[
    [ "G4LowEPComptonModel", "dc/d4b/classG4LowEPComptonModel.html#a5f955758433490ea66dbd471e89268b8", null ],
    [ "~G4LowEPComptonModel", "dc/d4b/classG4LowEPComptonModel.html#a10de61700115d478ce85ac64888f7654", null ],
    [ "G4LowEPComptonModel", "dc/d4b/classG4LowEPComptonModel.html#a0b87e92d6d954fc4df1999e24eaaa505", null ],
    [ "ComputeCrossSectionPerAtom", "dc/d4b/classG4LowEPComptonModel.html#acaae3854d5b49777c2a09ef17d0278fb", null ],
    [ "ComputeScatteringFunction", "dc/d4b/classG4LowEPComptonModel.html#ae5a0b6e7e684a4cfc39e0f96ce4b6ab2", null ],
    [ "Initialise", "dc/d4b/classG4LowEPComptonModel.html#a349938f1bc96c5111af060730fbeb38d", null ],
    [ "InitialiseForElement", "dc/d4b/classG4LowEPComptonModel.html#af3b9dd3e0746b3c7c9394b5c03258735", null ],
    [ "InitialiseLocal", "dc/d4b/classG4LowEPComptonModel.html#a44f02cdda54130331ae35d4c29ef7267", null ],
    [ "operator=", "dc/d4b/classG4LowEPComptonModel.html#a26fc81cde845fa8e1b496ed316340099", null ],
    [ "ReadData", "dc/d4b/classG4LowEPComptonModel.html#a2417a452d070200abc22fe7991a3972f", null ],
    [ "SampleSecondaries", "dc/d4b/classG4LowEPComptonModel.html#a581c55f60ce2b8e49606ae8241b84ecc", null ],
    [ "data", "dc/d4b/classG4LowEPComptonModel.html#ab1fbe9033b30376b4b4660259696c101", null ],
    [ "fAtomDeexcitation", "dc/d4b/classG4LowEPComptonModel.html#ab0e41aecdbf7e236d98ca7bd88e180ff", null ],
    [ "fParticleChange", "dc/d4b/classG4LowEPComptonModel.html#a4ef3b3ff6017dae89d138219ad63f5b2", null ],
    [ "isInitialised", "dc/d4b/classG4LowEPComptonModel.html#ab177aaa594f16fa20e53437eedcd9026", null ],
    [ "maxZ", "dc/d4b/classG4LowEPComptonModel.html#a82e454e36e7180f34761820b7fab3d6d", null ],
    [ "profileData", "dc/d4b/classG4LowEPComptonModel.html#a1e1d6ec94a76331e3f3d7dca21c675af", null ],
    [ "ScatFuncFitParam", "dc/d4b/classG4LowEPComptonModel.html#a2559f4a438ea550b30551a8ceb29001f", null ],
    [ "shellData", "dc/d4b/classG4LowEPComptonModel.html#aa91f15a0f99a33ff208358790fb01453", null ],
    [ "verboseLevel", "dc/d4b/classG4LowEPComptonModel.html#a546b2d5ae9c1247eaf20d429526f0ae6", null ]
];