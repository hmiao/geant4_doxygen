var classG4CrossSectionComposite =
[
    [ "G4CrossSectionComposite", "dc/d80/classG4CrossSectionComposite.html#a0e7d992ad67874007c4b121c14302855", null ],
    [ "~G4CrossSectionComposite", "dc/d80/classG4CrossSectionComposite.html#a9a539872bb4975209235cbf78a8972ca", null ],
    [ "G4CrossSectionComposite", "dc/d80/classG4CrossSectionComposite.html#a594340ece288a1507bacbc7d2e9ec8e3", null ],
    [ "G4CrossSectionComposite", "dc/d80/classG4CrossSectionComposite.html#a0e7d992ad67874007c4b121c14302855", null ],
    [ "~G4CrossSectionComposite", "dc/d80/classG4CrossSectionComposite.html#ae34dd83da776d72f3d11fd8cc53f8e8d", null ],
    [ "Add", "dc/d80/classG4CrossSectionComposite.html#a32fe43a117c3961e625747c6b707241b", null ],
    [ "CrossSection", "dc/d80/classG4CrossSectionComposite.html#a6a75eb6441a2edc5c11630807bead036", null ],
    [ "CrossSection", "dc/d80/classG4CrossSectionComposite.html#a7b416f5f9cf096bed96411c06572ecaf", null ],
    [ "GetComponents", "dc/d80/classG4CrossSectionComposite.html#a44a593e95a4cfc2e697e39a60f2135a9", null ],
    [ "IsValid", "dc/d80/classG4CrossSectionComposite.html#ad2ee5588f6ae5a8be18e5110ed0b3116", null ],
    [ "operator!=", "dc/d80/classG4CrossSectionComposite.html#a9e1ab1b65066b23bc26a85f87d10f3fb", null ],
    [ "operator!=", "dc/d80/classG4CrossSectionComposite.html#a9e1ab1b65066b23bc26a85f87d10f3fb", null ],
    [ "operator=", "dc/d80/classG4CrossSectionComposite.html#abfeaa13174aab6dd7aa6f56e439bf241", null ],
    [ "operator==", "dc/d80/classG4CrossSectionComposite.html#a9664b4350fcfa5a5dd9b65feb92ca31b", null ],
    [ "operator==", "dc/d80/classG4CrossSectionComposite.html#a9664b4350fcfa5a5dd9b65feb92ca31b", null ],
    [ "Remove", "dc/d80/classG4CrossSectionComposite.html#aaa2da853b552736a1e92895d28456a89", null ],
    [ "_components", "dc/d80/classG4CrossSectionComposite.html#a66762248d0b11caf8435a5eb25f70548", null ]
];