var classG4RegularXTRadiator =
[
    [ "G4RegularXTRadiator", "dc/d69/classG4RegularXTRadiator.html#a0826c64daf1540117d686831a2197930", null ],
    [ "~G4RegularXTRadiator", "dc/d69/classG4RegularXTRadiator.html#a6dacddc62ae7d48153abbf753cb1882a", null ],
    [ "DumpInfo", "dc/d69/classG4RegularXTRadiator.html#ac3845281d0ffc0a43a5a20368b56a3c4", null ],
    [ "GetStackFactor", "dc/d69/classG4RegularXTRadiator.html#a4ab7e96bfccff916646f10b002a2254a", null ],
    [ "ProcessDescription", "dc/d69/classG4RegularXTRadiator.html#a86ee7fe00708245594cc483d0d115ee1", null ],
    [ "SpectralXTRdEdx", "dc/d69/classG4RegularXTRadiator.html#a7d9e5214bc40f87e516605a440bda204", null ]
];