var classG4ecpssrBaseKxsModel =
[
    [ "TriDimensionMap", "dc/d75/classG4ecpssrBaseKxsModel.html#a039ea1a55c6455974683f17e72145553", null ],
    [ "VecMap", "dc/d75/classG4ecpssrBaseKxsModel.html#a760e6c5977552e9a744b25ebc32572fb", null ],
    [ "G4ecpssrBaseKxsModel", "dc/d75/classG4ecpssrBaseKxsModel.html#ae861d75f61e59f2c695862204e9b23be", null ],
    [ "~G4ecpssrBaseKxsModel", "dc/d75/classG4ecpssrBaseKxsModel.html#a2a98a3498b703b4c4a5c36474ef4dab5", null ],
    [ "G4ecpssrBaseKxsModel", "dc/d75/classG4ecpssrBaseKxsModel.html#ac195043ccf6c28cf4ff29f2d90918aa8", null ],
    [ "CalculateCrossSection", "dc/d75/classG4ecpssrBaseKxsModel.html#a019fa34a582f059fdcd3fb832f3832ca", null ],
    [ "ExpIntFunction", "dc/d75/classG4ecpssrBaseKxsModel.html#ae2e772fbd3d663dd19af9770cd1e2ff4", null ],
    [ "FunctionFK", "dc/d75/classG4ecpssrBaseKxsModel.html#a677a41b6af72d4f900cde60a07a2f80d", null ],
    [ "LinLogInterpolate", "dc/d75/classG4ecpssrBaseKxsModel.html#aab9f06f149deab738fc84af6da099702", null ],
    [ "LogLogInterpolate", "dc/d75/classG4ecpssrBaseKxsModel.html#aac29c5700b3e0380d351fab763d18c02", null ],
    [ "operator=", "dc/d75/classG4ecpssrBaseKxsModel.html#ad6c146fa3b2ea097255415a58beef5bf", null ],
    [ "QuadInterpolator", "dc/d75/classG4ecpssrBaseKxsModel.html#adc38ce3b16d542eb385f697ce967188d", null ],
    [ "aVecMap", "dc/d75/classG4ecpssrBaseKxsModel.html#a9edf846ca7f0aec8c41db1a23157d8f8", null ],
    [ "dummyVec", "dc/d75/classG4ecpssrBaseKxsModel.html#a0adc554bda27e881f3e14acc43815ab5", null ],
    [ "FKData", "dc/d75/classG4ecpssrBaseKxsModel.html#a85fdaebf5c3da069500987ff77c3fc6c", null ],
    [ "tableC1", "dc/d75/classG4ecpssrBaseKxsModel.html#a7a464510d14286f53986c8dc0e76c6f2", null ],
    [ "tableC2", "dc/d75/classG4ecpssrBaseKxsModel.html#a5dc32d1c8fab3c8b983f1f5a4dabd9c6", null ],
    [ "tableC3", "dc/d75/classG4ecpssrBaseKxsModel.html#aadbcde1a140a97f0716c2059768713ab", null ],
    [ "verboseLevel", "dc/d75/classG4ecpssrBaseKxsModel.html#af0b2b986c5383ffb7d6a17ebdb6fc24c", null ]
];