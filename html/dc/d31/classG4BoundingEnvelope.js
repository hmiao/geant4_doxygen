var classG4BoundingEnvelope =
[
    [ "G4BoundingEnvelope", "dc/d31/classG4BoundingEnvelope.html#ae119ecc84b2485f4923fac0ca2be3318", null ],
    [ "G4BoundingEnvelope", "dc/d31/classG4BoundingEnvelope.html#ab04c9ec7c6043fb3039cdd9b7838c5ca", null ],
    [ "G4BoundingEnvelope", "dc/d31/classG4BoundingEnvelope.html#a351f312293d054be86ddbbda918f1dbf", null ],
    [ "~G4BoundingEnvelope", "dc/d31/classG4BoundingEnvelope.html#a6cf8351de20bffbf0d3fa11d0f29b53c", null ],
    [ "BoundingBoxVsVoxelLimits", "dc/d31/classG4BoundingEnvelope.html#a235fa24aac2e9ce4d984e27ff6fb6e57", null ],
    [ "CalculateExtent", "dc/d31/classG4BoundingEnvelope.html#a2a5a1b7ce3899bedf6b53fc559488b2f", null ],
    [ "CheckBoundingBox", "dc/d31/classG4BoundingEnvelope.html#ae7d21d2f029c5ebce0f7521a934e2306", null ],
    [ "CheckBoundingPolygons", "dc/d31/classG4BoundingEnvelope.html#a0b8376cf9a6f7340d361bdab59ed71b2", null ],
    [ "ClipEdgesByVoxel", "dc/d31/classG4BoundingEnvelope.html#ad0e885af0cdec287b3bb2288c3dd1a30", null ],
    [ "ClipVoxelByPlanes", "dc/d31/classG4BoundingEnvelope.html#a2d11023224e9698df15f44cc8bac100d", null ],
    [ "CreateListOfEdges", "dc/d31/classG4BoundingEnvelope.html#a77dff9684b48f07f83f3ab3227793685", null ],
    [ "CreateListOfPlanes", "dc/d31/classG4BoundingEnvelope.html#ae9fb2c773052e3bfb64275efa2132023", null ],
    [ "FindScaleFactor", "dc/d31/classG4BoundingEnvelope.html#aa8770200c4dc51f921ed6244fef63ff7", null ],
    [ "GetPrismAABB", "dc/d31/classG4BoundingEnvelope.html#aa9fed3977d3b6bc0c351bd6ef4984d87", null ],
    [ "TransformVertices", "dc/d31/classG4BoundingEnvelope.html#a34c2e6ffac8c716b5522cd6edc4d97f0", null ],
    [ "fMax", "dc/d31/classG4BoundingEnvelope.html#a204cdb0671535fa95716a275465c09b0", null ],
    [ "fMin", "dc/d31/classG4BoundingEnvelope.html#adad30259a6eeb5af8f7e04ad25cfc8c3", null ],
    [ "fPolygons", "dc/d31/classG4BoundingEnvelope.html#ada62e2259784722e0d886ae1a92ae5ce", null ]
];