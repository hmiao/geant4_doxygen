var classG4hICRU49p =
[
    [ "G4hICRU49p", "dc/d31/classG4hICRU49p.html#add1eb9d70caca268be4a4a05b6731e44", null ],
    [ "~G4hICRU49p", "dc/d31/classG4hICRU49p.html#af372a5d137c9edf5a8dd38009d82ae51", null ],
    [ "ElectronicStoppingPower", "dc/d31/classG4hICRU49p.html#a47e58ff05515edca7b9eb85074345e56", null ],
    [ "HasMaterial", "dc/d31/classG4hICRU49p.html#a13e26df34985570d5c12fb0fc807b7b7", null ],
    [ "SetMoleculaNumber", "dc/d31/classG4hICRU49p.html#aad708bcbefb394051914b862b234e945", null ],
    [ "StoppingPower", "dc/d31/classG4hICRU49p.html#a2685dddaebc77405144cb7df91edcaeb", null ],
    [ "iMolecula", "dc/d31/classG4hICRU49p.html#a487101fdd7f110e87199a32dbbf9294c", null ],
    [ "protonMassAMU", "dc/d31/classG4hICRU49p.html#a24f0afa10193553e0a3888cac60bebcb", null ]
];