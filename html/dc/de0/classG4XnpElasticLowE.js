var classG4XnpElasticLowE =
[
    [ "G4XnpElasticLowE", "dc/de0/classG4XnpElasticLowE.html#ac9c51c5b5f2e81bf081d815df4a0fc60", null ],
    [ "~G4XnpElasticLowE", "dc/de0/classG4XnpElasticLowE.html#a7f2e8fd4d0b302aa03dc402ce21b4bd6", null ],
    [ "G4XnpElasticLowE", "dc/de0/classG4XnpElasticLowE.html#a5f97888481d6eacab63a39b8c54ab5a3", null ],
    [ "CrossSection", "dc/de0/classG4XnpElasticLowE.html#a196d69f7f7b873e858dc027c645830a8", null ],
    [ "GetComponents", "dc/de0/classG4XnpElasticLowE.html#aea96ae56a2bedddbef508237db5b53ed", null ],
    [ "HighLimit", "dc/de0/classG4XnpElasticLowE.html#a3927c62f3a024051d8eebf0dbc119391", null ],
    [ "IsValid", "dc/de0/classG4XnpElasticLowE.html#af4617a82f90906a5aa6614a60b140628", null ],
    [ "Name", "dc/de0/classG4XnpElasticLowE.html#ae537fd419323bad24e0db199962d99f8", null ],
    [ "operator!=", "dc/de0/classG4XnpElasticLowE.html#aedd3b351ecdeb9d477c67e27457d9bd1", null ],
    [ "operator=", "dc/de0/classG4XnpElasticLowE.html#a49196fe44bb016b9b2571accc69961a4", null ],
    [ "operator==", "dc/de0/classG4XnpElasticLowE.html#ae5efc00a69b3ac3515338fc70a467445", null ],
    [ "Print", "dc/de0/classG4XnpElasticLowE.html#a9daafc8b01bc03f667d7544fc9e4cdd8", null ],
    [ "_eMax", "dc/de0/classG4XnpElasticLowE.html#a612dd9b469fe488c3704e7c9432975c4", null ],
    [ "_eMin", "dc/de0/classG4XnpElasticLowE.html#af1fe7cf031a173e664be434edda83190", null ],
    [ "_eMinTable", "dc/de0/classG4XnpElasticLowE.html#a2689982192eeeaa0053be9b67ded53a2", null ],
    [ "_eStepLog", "dc/de0/classG4XnpElasticLowE.html#a2ac7617fcb2a812d03a035c22a559e9f", null ],
    [ "_highLimit", "dc/de0/classG4XnpElasticLowE.html#a247c0ac1099777b34a090e8f2d06a92a", null ],
    [ "_lowLimit", "dc/de0/classG4XnpElasticLowE.html#aec31ed584b77bc87f05e090c89f06642", null ],
    [ "_sigma", "dc/de0/classG4XnpElasticLowE.html#aaf9b91090dfbab5dd77c66ecc0e4d34f", null ],
    [ "_sigmaTable", "dc/de0/classG4XnpElasticLowE.html#a83bd62f34847b7f4c44baa4bce6a59de", null ],
    [ "_tableSize", "dc/de0/classG4XnpElasticLowE.html#a325eacccef54e2652d2d31498870f58c", null ]
];