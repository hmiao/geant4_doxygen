var classG4eeTo3PiModel =
[
    [ "G4eeTo3PiModel", "dc/d45/classG4eeTo3PiModel.html#a41cd5e571813dd22283f1d6f69959852", null ],
    [ "~G4eeTo3PiModel", "dc/d45/classG4eeTo3PiModel.html#a71f2ceb9a21355a948e6b02bf17dc8c5", null ],
    [ "G4eeTo3PiModel", "dc/d45/classG4eeTo3PiModel.html#a24e68a18fbbf7e27c07b0a390dd5232c", null ],
    [ "ComputeCrossSection", "dc/d45/classG4eeTo3PiModel.html#a88f3864185e2d4039a43289b029525a7", null ],
    [ "operator=", "dc/d45/classG4eeTo3PiModel.html#a9f48e23750173227f122059c9a7bca33", null ],
    [ "PeakEnergy", "dc/d45/classG4eeTo3PiModel.html#a1f0a6c2d138f692c1a126b25369fd84c", null ],
    [ "SampleSecondaries", "dc/d45/classG4eeTo3PiModel.html#a43f05c272f5c79d55eed776da84f75d5", null ],
    [ "gmax", "dc/d45/classG4eeTo3PiModel.html#acc57e6c9b3b9dfd4796f7db1578a824a", null ],
    [ "massOm", "dc/d45/classG4eeTo3PiModel.html#ac0a2e846e238435227c1b509f4448e3a", null ],
    [ "massPhi", "dc/d45/classG4eeTo3PiModel.html#afd743b3f1adebf2961e5d20a5c2da7c5", null ],
    [ "massPi", "dc/d45/classG4eeTo3PiModel.html#a551fdd1bcb241497d703ae75eb3710f5", null ],
    [ "massPi0", "dc/d45/classG4eeTo3PiModel.html#ad98d8417bcdc0e62620ff1857e2185a6", null ]
];