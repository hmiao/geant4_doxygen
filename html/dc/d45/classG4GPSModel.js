var classG4GPSModel =
[
    [ "G4GPSModel", "dc/d45/classG4GPSModel.html#a06a1e9722bfaf5d20ee3815aca28afc5", null ],
    [ "~G4GPSModel", "dc/d45/classG4GPSModel.html#aa209c7d26e9cf03cb77b864c7ba1a57d", null ],
    [ "G4GPSModel", "dc/d45/classG4GPSModel.html#a2e7d871e5bc2cc45695db4973a579dfc", null ],
    [ "DescribeYourselfTo", "dc/d45/classG4GPSModel.html#a96e5036f410975a32ebd67e5a504501a", null ],
    [ "GetCurrentDescription", "dc/d45/classG4GPSModel.html#a02fcf6b4c1bc0a0de85f3f311962ebc9", null ],
    [ "GetCurrentTag", "dc/d45/classG4GPSModel.html#af33186c0e957fce5e28d8b2a1e487533", null ],
    [ "operator=", "dc/d45/classG4GPSModel.html#a26daa2a085096e52d7f4a3a6ca88798e", null ],
    [ "fColour", "dc/d45/classG4GPSModel.html#af26271ffb84bd9cd6c6763f2bfde4583", null ]
];