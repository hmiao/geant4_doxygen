var classG4EmDNAPhysicsActivator =
[
    [ "G4EmDNAPhysicsActivator", "dc/d47/classG4EmDNAPhysicsActivator.html#a80efe85dd7a77bd34de2a75f72647fb9", null ],
    [ "~G4EmDNAPhysicsActivator", "dc/d47/classG4EmDNAPhysicsActivator.html#a54d0c8e820f8d9e58677954b6efc4a17", null ],
    [ "AddElectronModels0", "dc/d47/classG4EmDNAPhysicsActivator.html#acac2f08a28151a64044a1427730e0633", null ],
    [ "AddElectronModels2", "dc/d47/classG4EmDNAPhysicsActivator.html#a23d087e61f29d79f3eb7dd6cf578e365", null ],
    [ "AddElectronModels4", "dc/d47/classG4EmDNAPhysicsActivator.html#a7c5b80e425dab3fe03a93123eceb861f", null ],
    [ "AddElectronModels4a", "dc/d47/classG4EmDNAPhysicsActivator.html#a69c0d12cb644cdb1abed29ce4e5d431c", null ],
    [ "AddElectronModels6", "dc/d47/classG4EmDNAPhysicsActivator.html#af07ee76710a092636ffeb5d853254c2a", null ],
    [ "AddElectronModels6a", "dc/d47/classG4EmDNAPhysicsActivator.html#ac9437f37148fa0791d62dfc1867be9d2", null ],
    [ "AddElectronModels7", "dc/d47/classG4EmDNAPhysicsActivator.html#aa55686e38fab21af4805393560d586ce", null ],
    [ "AddGenericIonModels0", "dc/d47/classG4EmDNAPhysicsActivator.html#aa923471c4b2576eb116d9fabdfd336aa", null ],
    [ "AddHeliumModels0", "dc/d47/classG4EmDNAPhysicsActivator.html#a279126e359aea0ccfd9c4ff702d1acaf", null ],
    [ "AddProtonModels0", "dc/d47/classG4EmDNAPhysicsActivator.html#a65035d769cc1abaa9b4b2fe87c3e0249", null ],
    [ "ConstructParticle", "dc/d47/classG4EmDNAPhysicsActivator.html#a48e1f3ae7584e44383ea0220de57fec8", null ],
    [ "ConstructProcess", "dc/d47/classG4EmDNAPhysicsActivator.html#a104d7ddd55b91d87aebf6d29fcd6caaa", null ],
    [ "DeactivateNuclearStopping", "dc/d47/classG4EmDNAPhysicsActivator.html#aa8115783ab28679867099c2986390dd4", null ],
    [ "HasMsc", "dc/d47/classG4EmDNAPhysicsActivator.html#a26eaf639000b50948ffbe07de2705aed", null ],
    [ "IsVerbose", "dc/d47/classG4EmDNAPhysicsActivator.html#a273c005a505c4db60ff822e6086bec80", null ],
    [ "theParameters", "dc/d47/classG4EmDNAPhysicsActivator.html#a56353abf716ac9f776aa29f65a6fd19a", null ],
    [ "verbose", "dc/d47/classG4EmDNAPhysicsActivator.html#a3f273e950731fa20480435c829328bd4", null ]
];