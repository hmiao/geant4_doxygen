var classG4BigBanger =
[
    [ "G4BigBanger", "dc/d04/classG4BigBanger.html#ad6f0c3b51aa454285887dec96a23bec6", null ],
    [ "~G4BigBanger", "dc/d04/classG4BigBanger.html#a87f4caef05f9dedc3d3dbd1375b1fd9a", null ],
    [ "G4BigBanger", "dc/d04/classG4BigBanger.html#ad600104cbddaabaad1772e69a128c0d1", null ],
    [ "deExcite", "dc/d04/classG4BigBanger.html#a93e3fdb1ffba78f6328049b5aab9b60d", null ],
    [ "generateBangInSCM", "dc/d04/classG4BigBanger.html#aca055b2f281ec162f16a3ad9847444a4", null ],
    [ "generateMomentumModules", "dc/d04/classG4BigBanger.html#a82d558f21426589964b216d15d1034ad", null ],
    [ "generateX", "dc/d04/classG4BigBanger.html#ade71450d725aa71811513f049506cc67", null ],
    [ "maxProbability", "dc/d04/classG4BigBanger.html#a003cb8b81ab1d05c7b3f5cedf5a764bf", null ],
    [ "operator=", "dc/d04/classG4BigBanger.html#acb47c2eccb44721575c726998a312b6c", null ],
    [ "xProbability", "dc/d04/classG4BigBanger.html#a250dccd4d6fd9b9cb1e3391ff60e560c", null ],
    [ "momModules", "dc/d04/classG4BigBanger.html#adcd5f4ac088cf3b22b431569aaf37aef", null ],
    [ "particles", "dc/d04/classG4BigBanger.html#a13bb5794c3cd0808b362ea85f1301297", null ],
    [ "scm_momentums", "dc/d04/classG4BigBanger.html#ae5004de8b9eb257d4a6127a85290611b", null ]
];