var classG4OpAbsorption =
[
    [ "G4OpAbsorption", "dc/df8/classG4OpAbsorption.html#af1cd2ee5ccfbdb600e79a7df2fd9146e", null ],
    [ "~G4OpAbsorption", "dc/df8/classG4OpAbsorption.html#af4a72fba89cca1eea186abe3a2c7b9ae", null ],
    [ "G4OpAbsorption", "dc/df8/classG4OpAbsorption.html#a80dd409d2ff0430548155fb329e3ce12", null ],
    [ "GetMeanFreePath", "dc/df8/classG4OpAbsorption.html#a4a8012231258cc583546c075b646bee3", null ],
    [ "Initialise", "dc/df8/classG4OpAbsorption.html#af8967ef0c473400d86647dd66108fdae", null ],
    [ "IsApplicable", "dc/df8/classG4OpAbsorption.html#a13e0f1a486537d2e7082c61687f8c876", null ],
    [ "operator=", "dc/df8/classG4OpAbsorption.html#aae6248c949f46d07b1e94e4490338fe8", null ],
    [ "PostStepDoIt", "dc/df8/classG4OpAbsorption.html#adbb6fec66f56ff7ffa7e5cb3a9e74c83", null ],
    [ "PreparePhysicsTable", "dc/df8/classG4OpAbsorption.html#afd95cc124f8d1a7191b07ae32266b122", null ],
    [ "SetVerboseLevel", "dc/df8/classG4OpAbsorption.html#aec410bbf5f397f1ef68080f597a1361a", null ],
    [ "idx_absorption", "dc/df8/classG4OpAbsorption.html#a5f97b5ae2ed4597ed44b4470c1204f09", null ]
];