var classG4LivermoreIonisationModel =
[
    [ "G4LivermoreIonisationModel", "dc/d61/classG4LivermoreIonisationModel.html#a9b2d7eeac8e462df2f51af106c3c6941", null ],
    [ "~G4LivermoreIonisationModel", "dc/d61/classG4LivermoreIonisationModel.html#a3509bfb62e3c8df5614002a1187ff081", null ],
    [ "G4LivermoreIonisationModel", "dc/d61/classG4LivermoreIonisationModel.html#a713222d514bfcb4b6c1493bc4095960c", null ],
    [ "ComputeCrossSectionPerAtom", "dc/d61/classG4LivermoreIonisationModel.html#aa7fe9dd0660b32f3fae6cdc9e0fcd85a", null ],
    [ "ComputeDEDXPerVolume", "dc/d61/classG4LivermoreIonisationModel.html#a4f76179153a64e616c15eb05b194fe7b", null ],
    [ "GetVerboseLevel", "dc/d61/classG4LivermoreIonisationModel.html#a14780baae8504e93406a892e8b7d8472", null ],
    [ "Initialise", "dc/d61/classG4LivermoreIonisationModel.html#a6fa84636ea3c0037feac1c3428df0080", null ],
    [ "operator=", "dc/d61/classG4LivermoreIonisationModel.html#afcc49dce814bf139b7a29e32d1c76181", null ],
    [ "SampleSecondaries", "dc/d61/classG4LivermoreIonisationModel.html#a1924f51b78a340b024c86284dab60460", null ],
    [ "SetVerboseLevel", "dc/d61/classG4LivermoreIonisationModel.html#addc5c3b977f37f2792525a9776a1d00e", null ],
    [ "crossSectionHandler", "dc/d61/classG4LivermoreIonisationModel.html#a34635f0a10ef97a8f6e5fb96cd0bce05", null ],
    [ "energySpectrum", "dc/d61/classG4LivermoreIonisationModel.html#af821dc4cc72fa68a8a4e72e7295eed66", null ],
    [ "fIntrinsicHighEnergyLimit", "dc/d61/classG4LivermoreIonisationModel.html#ae56da20daaea1631dd60a907ce6619d6", null ],
    [ "fIntrinsicLowEnergyLimit", "dc/d61/classG4LivermoreIonisationModel.html#a51df17c72d18401993a359cb5d734dc8", null ],
    [ "fParticleChange", "dc/d61/classG4LivermoreIonisationModel.html#a3752872e4224662b42610211f90c2af9", null ],
    [ "isInitialised", "dc/d61/classG4LivermoreIonisationModel.html#a5fe490891c9b0a484ca9218a4cbca4b9", null ],
    [ "transitionManager", "dc/d61/classG4LivermoreIonisationModel.html#a0eea84d710166066df39283af17d505f", null ],
    [ "verboseLevel", "dc/d61/classG4LivermoreIonisationModel.html#a645f9bd73187495be0e31884d4b48bca", null ]
];