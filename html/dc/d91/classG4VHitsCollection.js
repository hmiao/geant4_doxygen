var classG4VHitsCollection =
[
    [ "G4VHitsCollection", "dc/d91/classG4VHitsCollection.html#af85965c921d156f990132429d5315e8d", null ],
    [ "G4VHitsCollection", "dc/d91/classG4VHitsCollection.html#a726fc38075c19a23c25a212eccfa406f", null ],
    [ "~G4VHitsCollection", "dc/d91/classG4VHitsCollection.html#aca01f574226a2349661596eef9335e38", null ],
    [ "DrawAllHits", "dc/d91/classG4VHitsCollection.html#a5af70446815190cabc6afac5c50a5a34", null ],
    [ "GetColID", "dc/d91/classG4VHitsCollection.html#a0c99e051cdde9d22f4ac44e81e88efb6", null ],
    [ "GetHit", "dc/d91/classG4VHitsCollection.html#a61a4f5a9066ba95d72abaee0ed5062fd", null ],
    [ "GetName", "dc/d91/classG4VHitsCollection.html#aeb70986c54224a859ee97d97dd2192a1", null ],
    [ "GetSDname", "dc/d91/classG4VHitsCollection.html#a02fab4d9e837518b0745472d18ee6f8d", null ],
    [ "GetSize", "dc/d91/classG4VHitsCollection.html#ad7443e73dfb0310affc82efc932cdb8f", null ],
    [ "operator==", "dc/d91/classG4VHitsCollection.html#ad99e48e5fea7d000e82805bafa171c10", null ],
    [ "PrintAllHits", "dc/d91/classG4VHitsCollection.html#a9e9d2c23e66afc3958994fe2626875ac", null ],
    [ "SetColID", "dc/d91/classG4VHitsCollection.html#ad6522624193ccb0133919ab1064689be", null ],
    [ "colID", "dc/d91/classG4VHitsCollection.html#a8320ac6bb5a2e3dd2abee9e6409a5648", null ],
    [ "collectionName", "dc/d91/classG4VHitsCollection.html#aff281fa39bcf05fe3edb8e31217b5620", null ],
    [ "SDname", "dc/d91/classG4VHitsCollection.html#a0aa9a51974833cb8a0076c7639fad2e5", null ]
];