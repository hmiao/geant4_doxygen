var classG4ChannelingOptrMultiParticleChangeCrossSection =
[
    [ "G4ChannelingOptrMultiParticleChangeCrossSection", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#aa7b0c948e76db857cc46700cb1f72c3e", null ],
    [ "~G4ChannelingOptrMultiParticleChangeCrossSection", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#a771b48bf1a243495e9eb844de2c409b3", null ],
    [ "AddChargedParticles", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#a98f86594d2efcabea218d1f7073e5243", null ],
    [ "AddParticle", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#adb734a5bd4afae423994baaa05c426c2", null ],
    [ "OperationApplied", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#a25a0a35bc02747957669f357bf363b3d", null ],
    [ "OperationApplied", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#aecdf929da1cb2d3d6685db734bc93721", null ],
    [ "OperationApplied", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#a3ac6796c69eff4d52c64afc10162ec49", null ],
    [ "ProposeFinalStateBiasingOperation", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#adbb0053768669d4e65c7b6f8e304f533", null ],
    [ "ProposeNonPhysicsBiasingOperation", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#a32b81a90fc4e6debe3e21b34ee8ba047", null ],
    [ "ProposeOccurenceBiasingOperation", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#afd7b4c3948248f97ec00832408c5d08f", null ],
    [ "StartTracking", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#afcc0dbc93051bf4e13e1b2bd4bd3ebc6", null ],
    [ "fBOptrForParticle", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#a3b2466768dc7e521d052c9d9b8a0c513", null ],
    [ "fCurrentOperator", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#a885a3bdfba83e38f6addb6c5f7a4230e", null ],
    [ "fnInteractions", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#a48e4fe872a9325b35829fea2368664f7", null ],
    [ "fParticlesToBias", "dc/d97/classG4ChannelingOptrMultiParticleChangeCrossSection.html#a9c7ca3d9b7087a9dfd67d6b3f46c752e", null ]
];