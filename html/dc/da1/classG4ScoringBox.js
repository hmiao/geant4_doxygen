var classG4ScoringBox =
[
    [ "G4ScoringBox", "dc/da1/classG4ScoringBox.html#a6f7e3f5bd4e56dfd53470a230b0ae8bb", null ],
    [ "~G4ScoringBox", "dc/da1/classG4ScoringBox.html#a92aa5d9ed44947b51a2ecb1f5df30c2d", null ],
    [ "Draw", "dc/da1/classG4ScoringBox.html#a372650a9284cc4295a783d3cf18c2d9b", null ],
    [ "DrawColumn", "dc/da1/classG4ScoringBox.html#abb34edfe864d0b79b472be896e9572aa", null ],
    [ "GetIndex", "dc/da1/classG4ScoringBox.html#aecfc3d3c33c9debac436480d9c4fff42", null ],
    [ "GetReplicaPosition", "dc/da1/classG4ScoringBox.html#a411cc7037d51854806b1409cefe7f208", null ],
    [ "GetXYZ", "dc/da1/classG4ScoringBox.html#a17487b4135a75518ac4d6deb52c8a697", null ],
    [ "List", "dc/da1/classG4ScoringBox.html#a5407b14f15940528c257603375e6ea20", null ],
    [ "SetSegmentDirection", "dc/da1/classG4ScoringBox.html#aa85bcf9bf3b6ed22d1479cb872dd7bb3", null ],
    [ "SetupGeometry", "dc/da1/classG4ScoringBox.html#a6c6a5680bd12533d5ef50ce6f68de35a", null ],
    [ "fSegmentDirection", "dc/da1/classG4ScoringBox.html#a0dafe55207824f787ef858cd988e3a3d", null ]
];