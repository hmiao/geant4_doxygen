var classG4DNABornAngle =
[
    [ "G4DNABornAngle", "dc/da7/classG4DNABornAngle.html#aa6cbe771f6e4b9b2ac01b36013f2f426", null ],
    [ "~G4DNABornAngle", "dc/da7/classG4DNABornAngle.html#ab26c119a24e679a2a4d48daa6783ae44", null ],
    [ "G4DNABornAngle", "dc/da7/classG4DNABornAngle.html#a9f75fe169237c97efa271a9f15204ddb", null ],
    [ "operator=", "dc/da7/classG4DNABornAngle.html#a30ef605a58efaf19b8d83846387f3128", null ],
    [ "PrintGeneratorInformation", "dc/da7/classG4DNABornAngle.html#aed3e72e803e2ff0570671564265ac155", null ],
    [ "SampleDirection", "dc/da7/classG4DNABornAngle.html#a4f80f0ff03cb5c15e9182ed4e038d938", null ],
    [ "SampleDirectionForShell", "dc/da7/classG4DNABornAngle.html#abd47f52d18f2c6263e09c51418014e1f", null ],
    [ "fElectron", "dc/da7/classG4DNABornAngle.html#ad914d163de4d8df5627505fcdec7cd82", null ]
];