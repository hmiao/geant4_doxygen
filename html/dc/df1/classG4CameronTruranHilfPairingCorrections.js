var classG4CameronTruranHilfPairingCorrections =
[
    [ "G4CameronTruranHilfPairingCorrections", "dc/df1/classG4CameronTruranHilfPairingCorrections.html#a8342479d03897af701dbf3aac902c1de", null ],
    [ "G4CameronTruranHilfPairingCorrections", "dc/df1/classG4CameronTruranHilfPairingCorrections.html#aed35af55d0c8617510e14f21aa9194f9", null ],
    [ "GetPairingCorrection", "dc/df1/classG4CameronTruranHilfPairingCorrections.html#ab0a2a45037b073df71e92de2810ad1c4", null ],
    [ "operator=", "dc/df1/classG4CameronTruranHilfPairingCorrections.html#a0dae38c485194bdb473e06de2b3c0d73", null ],
    [ "PairingNTable", "dc/df1/classG4CameronTruranHilfPairingCorrections.html#a40ee2e6924d85d85931ecefa73439d3e", null ],
    [ "PairingZTable", "dc/df1/classG4CameronTruranHilfPairingCorrections.html#a6645fd12d162dec153b896a625f5eeda", null ]
];