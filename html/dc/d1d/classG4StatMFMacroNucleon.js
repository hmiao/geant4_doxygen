var classG4StatMFMacroNucleon =
[
    [ "G4StatMFMacroNucleon", "dc/d1d/classG4StatMFMacroNucleon.html#a5686143ad1ef6b36f5a46847152703c4", null ],
    [ "~G4StatMFMacroNucleon", "dc/d1d/classG4StatMFMacroNucleon.html#abb399f8780092039a5b06e28eaf2d2c8", null ],
    [ "G4StatMFMacroNucleon", "dc/d1d/classG4StatMFMacroNucleon.html#a762580ba38c11ee644514e18b729eb01", null ],
    [ "CalcEnergy", "dc/d1d/classG4StatMFMacroNucleon.html#a76a985bc5433554c8354fe1f6c1296b7", null ],
    [ "CalcEntropy", "dc/d1d/classG4StatMFMacroNucleon.html#a1af9e350a2e494bc0b6d0832d9c2c2d0", null ],
    [ "CalcMeanMultiplicity", "dc/d1d/classG4StatMFMacroNucleon.html#a03477dec776d30eb82962a1a9b21199b", null ],
    [ "CalcZARatio", "dc/d1d/classG4StatMFMacroNucleon.html#aa267c0e6c9f12e3e1420681f0ba51325", null ],
    [ "operator!=", "dc/d1d/classG4StatMFMacroNucleon.html#ac9d6235f91f87b0d6cb4734165cd00f4", null ],
    [ "operator=", "dc/d1d/classG4StatMFMacroNucleon.html#aaaee40a557e26f1d34b6a1748bcebe75", null ],
    [ "operator==", "dc/d1d/classG4StatMFMacroNucleon.html#adca746cbeddc1258db346ad4cd1cf578", null ],
    [ "_NeutronMeanMultiplicity", "dc/d1d/classG4StatMFMacroNucleon.html#adafc618c69318281c633615a329f0fdd", null ],
    [ "_ProtonMeanMultiplicity", "dc/d1d/classG4StatMFMacroNucleon.html#a6828c92a0e9c5d145e635b3dd7c406ac", null ]
];