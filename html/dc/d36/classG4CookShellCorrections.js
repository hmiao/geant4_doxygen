var classG4CookShellCorrections =
[
    [ "G4CookShellCorrections", "dc/d36/classG4CookShellCorrections.html#aab57d3cc877fa2dcd54b1bf57fc4e3f0", null ],
    [ "G4CookShellCorrections", "dc/d36/classG4CookShellCorrections.html#a235fb86f1b6f072859bcde3732cf86a1", null ],
    [ "GetShellCorrection", "dc/d36/classG4CookShellCorrections.html#af6f1b39f6d2fa0dcb0d626a5dd383736", null ],
    [ "operator=", "dc/d36/classG4CookShellCorrections.html#ab38f1ac518a38b978996a6ee8dd6ca28", null ],
    [ "ShellNTable", "dc/d36/classG4CookShellCorrections.html#af420f5e7377a8e990501779f9dece81c", null ],
    [ "ShellZTable", "dc/d36/classG4CookShellCorrections.html#ab49de46ee17ddc4a950ecc144d9bc599", null ]
];