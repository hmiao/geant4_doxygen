var classG4SextupoleMagField =
[
    [ "G4SextupoleMagField", "dc/d70/classG4SextupoleMagField.html#a04e30a367bfae819d9abb3d12b7cadaa", null ],
    [ "G4SextupoleMagField", "dc/d70/classG4SextupoleMagField.html#a6baed3ffbe2ee7c138bc9cf897d6e0c1", null ],
    [ "~G4SextupoleMagField", "dc/d70/classG4SextupoleMagField.html#a80a09822fe051795b485bc9015bf53b6", null ],
    [ "Clone", "dc/d70/classG4SextupoleMagField.html#a1669c1954d0eb42dd4d27b874ffd715b", null ],
    [ "GetFieldValue", "dc/d70/classG4SextupoleMagField.html#a67c0876aacc194232122712a170eedf7", null ],
    [ "fGradient", "dc/d70/classG4SextupoleMagField.html#abf74e4a2b0ae2b2afde425e26d251cf4", null ],
    [ "fOrigin", "dc/d70/classG4SextupoleMagField.html#a131ddec0f985af62acef768c93c07950", null ],
    [ "fpMatrix", "dc/d70/classG4SextupoleMagField.html#a0e56aac19ec2b4d3ff134e890d245989", null ]
];