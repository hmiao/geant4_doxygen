var classG4AngularDistributionPP =
[
    [ "G4AngularDistributionPP", "dc/d34/classG4AngularDistributionPP.html#a1956872bf884c327db3a5baee4e575cf", null ],
    [ "~G4AngularDistributionPP", "dc/d34/classG4AngularDistributionPP.html#adbd6ed68afeecbcbb9c8096a1f708353", null ],
    [ "CosTheta", "dc/d34/classG4AngularDistributionPP.html#afae2cce58d9a40cbbe12fc1a50b9cf27", null ],
    [ "Phi", "dc/d34/classG4AngularDistributionPP.html#a71f6fe5a33bde72a2fd6242d6a6ebc8d", null ],
    [ "dsigmax", "dc/d34/classG4AngularDistributionPP.html#ab225dbe65298a7ec85a7fb433305746e", null ],
    [ "elab", "dc/d34/classG4AngularDistributionPP.html#a7b474c7fcf283885e032cf02b0a05808", null ],
    [ "pcm", "dc/d34/classG4AngularDistributionPP.html#a5887b0c0e4f873d62e2e091c3006f69e", null ],
    [ "sig", "dc/d34/classG4AngularDistributionPP.html#aad087d3ca873006c833754ab52a432c0", null ],
    [ "sigtot", "dc/d34/classG4AngularDistributionPP.html#a827aa42f967536e92edfdc5b94fd12fc", null ]
];