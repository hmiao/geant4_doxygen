var structG4RootPNtupleDescription =
[
    [ "G4RootPNtupleDescription", "d9/df1/structG4RootPNtupleDescription.html#a79fdc7627df6dcb62040dd265fb377e2", null ],
    [ "~G4RootPNtupleDescription", "d9/df1/structG4RootPNtupleDescription.html#ab58c3ecec71839fdb7849f131cf088ee", null ],
    [ "fBasePNtuple", "d9/df1/structG4RootPNtupleDescription.html#a7529276743c1cdd480599354942c7ce1", null ],
    [ "fDescription", "d9/df1/structG4RootPNtupleDescription.html#aafc81ec2e4d4f12015c5c8fc3f8ec169", null ],
    [ "fMainBranches", "d9/df1/structG4RootPNtupleDescription.html#a92c47a3662a4369c91bed7a7db016653", null ],
    [ "fNtuple", "d9/df1/structG4RootPNtupleDescription.html#a922af6d9bec964426c3919e32336825e", null ]
];