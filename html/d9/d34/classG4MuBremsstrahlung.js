var classG4MuBremsstrahlung =
[
    [ "G4MuBremsstrahlung", "d9/d34/classG4MuBremsstrahlung.html#a16597ca9f728bbbae25c31c640e9c8d7", null ],
    [ "~G4MuBremsstrahlung", "d9/d34/classG4MuBremsstrahlung.html#a73ca41ba315d4938974d1494b8ec1222", null ],
    [ "G4MuBremsstrahlung", "d9/d34/classG4MuBremsstrahlung.html#af0b3d789091996785015960b7796977f", null ],
    [ "InitialiseEnergyLossProcess", "d9/d34/classG4MuBremsstrahlung.html#ac1719bb36c7fb67875e4b199c2372644", null ],
    [ "IsApplicable", "d9/d34/classG4MuBremsstrahlung.html#a89ba1dc24aa7a1b274daf2993aeb97cb", null ],
    [ "MinPrimaryEnergy", "d9/d34/classG4MuBremsstrahlung.html#afb55db7bb6d71983512742df7d604c77", null ],
    [ "operator=", "d9/d34/classG4MuBremsstrahlung.html#a297fd88c5619adebb6ab54caa4f150bd", null ],
    [ "ProcessDescription", "d9/d34/classG4MuBremsstrahlung.html#adfa24da10ddd77e318fb8fa1793459d8", null ],
    [ "SetLowestKineticEnergy", "d9/d34/classG4MuBremsstrahlung.html#af0d837cd59069b6d079734f40f67e667", null ],
    [ "isInitialised", "d9/d34/classG4MuBremsstrahlung.html#ac7c3f7a640018e4e494558492f7b4a00", null ],
    [ "lowestKinEnergy", "d9/d34/classG4MuBremsstrahlung.html#a96e02145cdeda09b920ecb72608e74e6", null ]
];