var classG4PairingCorrection =
[
    [ "G4PairingCorrection", "d9/d50/classG4PairingCorrection.html#a0f7f1f8d7068bee42a2b03ce693a7c65", null ],
    [ "~G4PairingCorrection", "d9/d50/classG4PairingCorrection.html#a1b3c4612f9b89253cddcf6c5360e69bc", null ],
    [ "GetFissionPairingCorrection", "d9/d50/classG4PairingCorrection.html#ab4e488fd01e77f16bb1db81982d190f4", null ],
    [ "GetPairingCorrection", "d9/d50/classG4PairingCorrection.html#af5ef0cb34a4cdc46f514a2743e298bd6", null ],
    [ "theCameronGilbertPairingCorrections", "d9/d50/classG4PairingCorrection.html#a4165cdc2dc8d00cd68f2d803b9fc68af", null ],
    [ "theCookPairingCorrections", "d9/d50/classG4PairingCorrection.html#a7edc8cc4332424f70a4fd8c88d239010", null ]
];