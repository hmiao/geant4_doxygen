var classG4Voxel =
[
    [ "Index", "d1/d7a/structG4Voxel_1_1Index.html", "d1/d7a/structG4Voxel_1_1Index" ],
    [ "MapList", "d9/dde/classG4Voxel.html#a5cfa79a24f8399b8c295785ef21034b9", null ],
    [ "MolType", "d9/dde/classG4Voxel.html#a0bf1f1407308d47d9c367476229cc8f3", null ],
    [ "G4Voxel", "d9/dde/classG4Voxel.html#a68d452af8492b7fdc447dd79e68b002d", null ],
    [ "~G4Voxel", "d9/dde/classG4Voxel.html#ac5f24e3015f14692166855bf5e2418c0", null ],
    [ "GetIndex", "d9/dde/classG4Voxel.html#a4b16f315e3b548bdbe6a49756de97eca", null ],
    [ "GetMapList", "d9/dde/classG4Voxel.html#a233ae592156dc6bf22b375c0849a313b", null ],
    [ "GetVolume", "d9/dde/classG4Voxel.html#a0268514793d3fef0a05b98b5e1083b4d", null ],
    [ "SetMapList", "d9/dde/classG4Voxel.html#a53ed64fadb1a052defb2956bd29dc96e", null ],
    [ "fBox", "d9/dde/classG4Voxel.html#a034e43a6b972771687a25a460e3fe68d", null ],
    [ "fIndex", "d9/dde/classG4Voxel.html#a7866cb9935cfb14ded9294a9b44103a3", null ],
    [ "fMapList", "d9/dde/classG4Voxel.html#a303282bbf686079d150825df65708bb6", null ]
];