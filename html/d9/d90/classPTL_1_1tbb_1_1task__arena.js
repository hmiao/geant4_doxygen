var classPTL_1_1tbb_1_1task__arena =
[
    [ "parameter", "d9/d90/classPTL_1_1tbb_1_1task__arena.html#a19254a3a84f634d4fc8bd02cc53052ac", [
      [ "not_initialized", "d9/d90/classPTL_1_1tbb_1_1task__arena.html#a19254a3a84f634d4fc8bd02cc53052acaab31ac14791b3625a6e49c10dab9ed5e", null ],
      [ "automatic", "d9/d90/classPTL_1_1tbb_1_1task__arena.html#a19254a3a84f634d4fc8bd02cc53052aca8d18e6d4c9197c5447f7e6a35f4471b5", null ]
    ] ],
    [ "task_arena", "d9/d90/classPTL_1_1tbb_1_1task__arena.html#ad2b7fd4ec0772306911d2d913b29d30e", null ],
    [ "~task_arena", "d9/d90/classPTL_1_1tbb_1_1task__arena.html#ac226600cd98fdc9690d2e7e91257ead9", null ],
    [ "execute", "d9/d90/classPTL_1_1tbb_1_1task__arena.html#a1580a1d939de63955d85e2fda4f472cc", null ],
    [ "initialize", "d9/d90/classPTL_1_1tbb_1_1task__arena.html#a782b4f777b449cc1d32816de3bf8e759", null ]
];