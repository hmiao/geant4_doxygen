var classG4WaterStopping =
[
    [ "G4WaterStopping", "d9/dc4/classG4WaterStopping.html#a5c48874d71f3de22fb7602c40170a025", null ],
    [ "~G4WaterStopping", "d9/dc4/classG4WaterStopping.html#af345e600312aca2a5924686e6dc3bff5", null ],
    [ "G4WaterStopping", "d9/dc4/classG4WaterStopping.html#a78b31033313d360070071454f0234578", null ],
    [ "AddData", "d9/dc4/classG4WaterStopping.html#a38b0464e16364c6a662746263996d920", null ],
    [ "GetElectronicDEDX", "d9/dc4/classG4WaterStopping.html#abd7586b1489bb52ca2ebc125146b69f0", null ],
    [ "Initialise", "d9/dc4/classG4WaterStopping.html#a4acd87c1dba4c337b13caeef93740baf", null ],
    [ "operator=", "d9/dc4/classG4WaterStopping.html#a12ead60906d32740cd7035dfe741bbbe", null ],
    [ "dedx", "d9/dc4/classG4WaterStopping.html#af794421e1ea14b68a4c70c690382a168", null ],
    [ "emin", "d9/dc4/classG4WaterStopping.html#a1ed71f31ff001d40b4553198a5c61418", null ],
    [ "spline", "d9/dc4/classG4WaterStopping.html#ab3a0fe6161b8c3ced78aac8a5893836f", null ]
];