var classG4PolarizedPhotoElectricModel =
[
    [ "G4PolarizedPhotoElectricModel", "d9/d9b/classG4PolarizedPhotoElectricModel.html#ad568ac1e85ef4ab2cdc577c4caaea5ea", null ],
    [ "~G4PolarizedPhotoElectricModel", "d9/d9b/classG4PolarizedPhotoElectricModel.html#ad228f8a9d9d63f147ab516c879d7e908", null ],
    [ "G4PolarizedPhotoElectricModel", "d9/d9b/classG4PolarizedPhotoElectricModel.html#a73cac93d6fc33aef4aa80fafa46b3f2a", null ],
    [ "Initialise", "d9/d9b/classG4PolarizedPhotoElectricModel.html#aa75728aa642830a26af5e9a755d7cec2", null ],
    [ "operator=", "d9/d9b/classG4PolarizedPhotoElectricModel.html#ade47f063e9bef74f8c8e31baba2b748f", null ],
    [ "SampleSecondaries", "d9/d9b/classG4PolarizedPhotoElectricModel.html#a96ddab7efac9cdff6bd3e4f05cb6081d", null ],
    [ "fCrossSectionCalculator", "d9/d9b/classG4PolarizedPhotoElectricModel.html#a7969886e22c5514567779626d9f9073e", null ],
    [ "fVerboseLevel", "d9/d9b/classG4PolarizedPhotoElectricModel.html#af74227dde7a0107ccdb97b5edc3ddf5f", null ]
];