var classG4ParticleHPElasticData =
[
    [ "G4ParticleHPElasticData", "d9/de3/classG4ParticleHPElasticData.html#a0e545a5dc447ec621453b250134532da", null ],
    [ "~G4ParticleHPElasticData", "d9/de3/classG4ParticleHPElasticData.html#a1aa8c8022f9ab46b1d77200faeb85e80", null ],
    [ "BuildPhysicsTable", "d9/de3/classG4ParticleHPElasticData.html#a445b0bc4d1b2ab34f1e8fb69489ea539", null ],
    [ "CrossSectionDescription", "d9/de3/classG4ParticleHPElasticData.html#ae83a9197844a96f2e8e60d3b8c2d3515", null ],
    [ "DumpPhysicsTable", "d9/de3/classG4ParticleHPElasticData.html#ada21c444bf6f55a7d77ffa077f11e4d1", null ],
    [ "GetCrossSection", "d9/de3/classG4ParticleHPElasticData.html#aeea5336955c561d9b5f8e0ff0f9a9358", null ],
    [ "GetIsoCrossSection", "d9/de3/classG4ParticleHPElasticData.html#a45231c84ad3c5864b44f3fa8c9642c96", null ],
    [ "GetVerboseLevel", "d9/de3/classG4ParticleHPElasticData.html#a8c30ca6d7adcec2f8c7541c8d24883d5", null ],
    [ "IsIsoApplicable", "d9/de3/classG4ParticleHPElasticData.html#a106366bda55deb72e8544fbed151863e", null ],
    [ "SetVerboseLevel", "d9/de3/classG4ParticleHPElasticData.html#a3bb1adee0025c69516235818aafc4a04", null ],
    [ "element_cache", "d9/de3/classG4ParticleHPElasticData.html#a3777d31296ff0c6553b59b4f7b745026", null ],
    [ "instanceOfWorker", "d9/de3/classG4ParticleHPElasticData.html#afb1a5cc269750127ec4dd15ac8d78b01", null ],
    [ "ke_cache", "d9/de3/classG4ParticleHPElasticData.html#a51439020340dee7cf177a7ec7e04f05f", null ],
    [ "material_cache", "d9/de3/classG4ParticleHPElasticData.html#a6e70c32b90bc6db0fbf6c2a787bfaa83", null ],
    [ "theCrossSections", "d9/de3/classG4ParticleHPElasticData.html#afd376d5d552d6a38659b477aa072abbf", null ],
    [ "xs_cache", "d9/de3/classG4ParticleHPElasticData.html#a2e1a3ea354d6887ae96e547143d6a42e", null ]
];