var classG4ZeroXS =
[
    [ "G4ZeroXS", "d9/de3/classG4ZeroXS.html#a115c7e6b9ab6afd43a732cb781ad9e80", null ],
    [ "~G4ZeroXS", "d9/de3/classG4ZeroXS.html#af8448ef721c98456f321bb4cf7cf671a", null ],
    [ "G4ZeroXS", "d9/de3/classG4ZeroXS.html#a1821f27c5921eb7999481fbc510ff533", null ],
    [ "CrossSectionDescription", "d9/de3/classG4ZeroXS.html#a4ab5011daa78f4bf46d2ee05a776ecc7", null ],
    [ "GetElementCrossSection", "d9/de3/classG4ZeroXS.html#a40321b098f4db247bce06bdf87d9b7d6", null ],
    [ "IsElementApplicable", "d9/de3/classG4ZeroXS.html#a4e87a1dfec82ce9d4c7f94830c5d5ac9", null ],
    [ "operator=", "d9/de3/classG4ZeroXS.html#ae49e9ed40fbb43681bd8f9fec4f7408c", null ]
];