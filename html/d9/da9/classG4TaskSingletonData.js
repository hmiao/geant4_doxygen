var classG4TaskSingletonData =
[
    [ "compare_type", "d9/da9/classG4TaskSingletonData.html#af19f5eeb119b79af2e38b7a45f4ae634", null ],
    [ "key_type", "d9/da9/classG4TaskSingletonData.html#a8e6374f3851e808bcf36dea99b86a75b", null ],
    [ "this_type", "d9/da9/classG4TaskSingletonData.html#a9cb900b76ce8c790a5348896f1ff411a", null ],
    [ "GetInstance", "d9/da9/classG4TaskSingletonData.html#a50b5982210379ae44713e0add7b52bfd", null ],
    [ "G4TaskSingletonDelegator< T >", "d9/da9/classG4TaskSingletonData.html#ac7c6982d72cf0d60b40fbd0b6809932f", null ],
    [ "G4TaskSingletonEvaluator< T >", "d9/da9/classG4TaskSingletonData.html#afa46d15c39339eda059788cfa3523235", null ],
    [ "m_data", "d9/da9/classG4TaskSingletonData.html#a4a0bdd07a76d1ded831c94ffd306d67a", null ]
];