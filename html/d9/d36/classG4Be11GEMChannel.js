var classG4Be11GEMChannel =
[
    [ "G4Be11GEMChannel", "d9/d36/classG4Be11GEMChannel.html#a8af32075e077d27aa0c9f86ed9393f1c", null ],
    [ "~G4Be11GEMChannel", "d9/d36/classG4Be11GEMChannel.html#a4aa0bcca7c618a3b8184f3ce1fed46a0", null ],
    [ "G4Be11GEMChannel", "d9/d36/classG4Be11GEMChannel.html#a1224bad73970bf773e81f918ae9fe23f", null ],
    [ "operator!=", "d9/d36/classG4Be11GEMChannel.html#a3f5abd6b50640b29c23b4a7672b99aec", null ],
    [ "operator=", "d9/d36/classG4Be11GEMChannel.html#aa124715819f1e3e83757b30d6cc3b390", null ],
    [ "operator==", "d9/d36/classG4Be11GEMChannel.html#a8a4a1083c6b5e480c7979132845157e2", null ],
    [ "theEvaporationProbability", "d9/d36/classG4Be11GEMChannel.html#ac928553fcd8c872cb12c823860f629a8", null ]
];