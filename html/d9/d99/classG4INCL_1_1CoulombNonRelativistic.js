var classG4INCL_1_1CoulombNonRelativistic =
[
    [ "CoulombNonRelativistic", "d9/d99/classG4INCL_1_1CoulombNonRelativistic.html#aaeb092c0decacafc893a392745f2231a", null ],
    [ "~CoulombNonRelativistic", "d9/d99/classG4INCL_1_1CoulombNonRelativistic.html#a4ae3eb4ef202b96d9829ec6c8a28e5c0", null ],
    [ "bringToSurface", "d9/d99/classG4INCL_1_1CoulombNonRelativistic.html#a3b5f8f26f5062c42244f618b549ed334", null ],
    [ "bringToSurface", "d9/d99/classG4INCL_1_1CoulombNonRelativistic.html#af570e01106a87302741031c6a884c8b2", null ],
    [ "coulombDeviation", "d9/d99/classG4INCL_1_1CoulombNonRelativistic.html#ad4267e92358ae3855d72d0fb2d24355c", null ],
    [ "distortOut", "d9/d99/classG4INCL_1_1CoulombNonRelativistic.html#a818c29251f97d22cc22afbdfaf5197ca", null ],
    [ "getCoulombRadius", "d9/d99/classG4INCL_1_1CoulombNonRelativistic.html#a307e699b6065667b0d6366ff0827381d", null ],
    [ "maxImpactParameter", "d9/d99/classG4INCL_1_1CoulombNonRelativistic.html#ac46dac49cb274775e99b24a0e6a64c22", null ],
    [ "minimumDistance", "d9/d99/classG4INCL_1_1CoulombNonRelativistic.html#a7f715fd1eea0ad2c1937a8bd2b7e7968", null ],
    [ "minimumDistance", "d9/d99/classG4INCL_1_1CoulombNonRelativistic.html#ac0ec57fa7e10e9fbc256b1aa19d97baf", null ],
    [ "theCoulombNoneSlave", "d9/d99/classG4INCL_1_1CoulombNonRelativistic.html#a265e4615bc026f6004571c618fe43777", null ]
];