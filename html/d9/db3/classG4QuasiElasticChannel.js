var classG4QuasiElasticChannel =
[
    [ "G4QuasiElasticChannel", "d9/db3/classG4QuasiElasticChannel.html#a0c400930a083b0dbd653b0b2f4962531", null ],
    [ "~G4QuasiElasticChannel", "d9/db3/classG4QuasiElasticChannel.html#a31d21e39d28d560e92173e1fe5b62df4", null ],
    [ "G4QuasiElasticChannel", "d9/db3/classG4QuasiElasticChannel.html#a4f5acc8003a5d558281a3c5afa68b308", null ],
    [ "GetFraction", "d9/db3/classG4QuasiElasticChannel.html#aa29363d20f1059c3084093f1009c6d6d", null ],
    [ "operator!=", "d9/db3/classG4QuasiElasticChannel.html#ac5ef7daf7b2bc0e5e55777d6965de2a9", null ],
    [ "operator=", "d9/db3/classG4QuasiElasticChannel.html#aa8d26cfa2ee662be6ee174644b45e6f8", null ],
    [ "operator==", "d9/db3/classG4QuasiElasticChannel.html#aa5e35e5141c159d2fe13be0bb34b2517", null ],
    [ "Scatter", "d9/db3/classG4QuasiElasticChannel.html#a77c7ab32488569f2d1303132af2023a6", null ],
    [ "secID", "d9/db3/classG4QuasiElasticChannel.html#ae559bbfcbeb5120096f969d87e00bb20", null ],
    [ "the3DNucleus", "d9/db3/classG4QuasiElasticChannel.html#ad3267912845a5d52d57393c852d29a41", null ],
    [ "theQuasiElastic", "d9/db3/classG4QuasiElasticChannel.html#ad8f70dcca5b6430ee002425abb986ee1", null ]
];