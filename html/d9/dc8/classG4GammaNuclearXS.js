var classG4GammaNuclearXS =
[
    [ "G4GammaNuclearXS", "d9/dc8/classG4GammaNuclearXS.html#a9e204b782a84a48f08a65066a54d9b2f", null ],
    [ "~G4GammaNuclearXS", "d9/dc8/classG4GammaNuclearXS.html#a227624f7e7e31f6e692b023d66bbe86d", null ],
    [ "G4GammaNuclearXS", "d9/dc8/classG4GammaNuclearXS.html#a293ec0d7a7a3aac49e82ded87de59a8e", null ],
    [ "BuildPhysicsTable", "d9/dc8/classG4GammaNuclearXS.html#a3227225b3bebc8be09770210dca59453", null ],
    [ "CrossSectionDescription", "d9/dc8/classG4GammaNuclearXS.html#aaf827f66f35e6d82f9c274fc4fe50324", null ],
    [ "Default_Name", "d9/dc8/classG4GammaNuclearXS.html#a012abf4a5fc23524c4b7a9b84a179869", null ],
    [ "ElementCrossSection", "d9/dc8/classG4GammaNuclearXS.html#a4acf4a4c1484556f9afbb9196769582f", null ],
    [ "FindDirectoryPath", "d9/dc8/classG4GammaNuclearXS.html#a276547f4adb508b31581d463aeae80f2", null ],
    [ "GetElementCrossSection", "d9/dc8/classG4GammaNuclearXS.html#aa0e10655644fe87f43ad9e28c29b7c65", null ],
    [ "GetIsoCrossSection", "d9/dc8/classG4GammaNuclearXS.html#a00054a8b985d480121888d8d3ad2b7fd", null ],
    [ "GetPhysicsVector", "d9/dc8/classG4GammaNuclearXS.html#aaa862c1dac6f078e1261410998532f26", null ],
    [ "Initialise", "d9/dc8/classG4GammaNuclearXS.html#a2bbae957c58bcd6d23c644dd74b60696", null ],
    [ "InitialiseOnFly", "d9/dc8/classG4GammaNuclearXS.html#a2d5cd033c070c1105e1defb533899700", null ],
    [ "IsElementApplicable", "d9/dc8/classG4GammaNuclearXS.html#aef338767bda93ec1fdb3a88a2d5db6f7", null ],
    [ "IsIsoApplicable", "d9/dc8/classG4GammaNuclearXS.html#a7760b495821be43efacfe04ab18bbde6", null ],
    [ "IsoCrossSection", "d9/dc8/classG4GammaNuclearXS.html#ab0bf2833241d9e19e4b63cd38c018eb3", null ],
    [ "operator=", "d9/dc8/classG4GammaNuclearXS.html#a5868d5e2054a2bccfdeab8f4534ffeb5", null ],
    [ "RetrieveVector", "d9/dc8/classG4GammaNuclearXS.html#a22481b004df7c00e9030a36e15d33046", null ],
    [ "SelectIsotope", "d9/dc8/classG4GammaNuclearXS.html#ae7acad7b5ea1fd8311cf1a6c2dbd825e", null ],
    [ "coeff", "d9/dc8/classG4GammaNuclearXS.html#a88770b4cd1021554d3d33918f74eefd7", null ],
    [ "data", "d9/dc8/classG4GammaNuclearXS.html#a9ad31646f174e921155557d88653f06c", null ],
    [ "freeVectorException", "d9/dc8/classG4GammaNuclearXS.html#adf23b3acd1f66dfc580ec4e9187c7f14", null ],
    [ "gamma", "d9/dc8/classG4GammaNuclearXS.html#a1b2b8a7ce2a64d04e6c40aa2d352a7c5", null ],
    [ "gDataDirectory", "d9/dc8/classG4GammaNuclearXS.html#afeb76e53037dfcb9c884ac0fbda64f5b", null ],
    [ "ggXsection", "d9/dc8/classG4GammaNuclearXS.html#a557df1a674aaaf4c2d6306603856dc8d", null ],
    [ "isMaster", "d9/dc8/classG4GammaNuclearXS.html#a3e35da6b0d5029ad87cca501a08bec1f", null ],
    [ "MAXZGAMMAXS", "d9/dc8/classG4GammaNuclearXS.html#a08376584687c56259f80445e7a771d26", null ],
    [ "rTransitionBound", "d9/dc8/classG4GammaNuclearXS.html#a91cf239173954e046763f0479ac44dcf", null ],
    [ "temp", "d9/dc8/classG4GammaNuclearXS.html#a9d7681934d99f134e542caa960f04ddd", null ],
    [ "xs150", "d9/dc8/classG4GammaNuclearXS.html#a42c3ae6f41a73946a6e5d01ff32908ef", null ]
];