var G4EmTableType_8hh =
[
    [ "G4TwoPeaksXS", "d3/d9b/structG4TwoPeaksXS.html", "d3/d9b/structG4TwoPeaksXS" ],
    [ "G4CrossSectionType", "d9/d1f/G4EmTableType_8hh.html#aebc1c791eda70608be739b8282b4d7e9", [
      [ "fEmNoIntegral", "d9/d1f/G4EmTableType_8hh.html#aebc1c791eda70608be739b8282b4d7e9aade1a80a280e4ca4805b3cf59971db98", null ],
      [ "fEmIncreasing", "d9/d1f/G4EmTableType_8hh.html#aebc1c791eda70608be739b8282b4d7e9aefbdcd44b39cf75ab708358a5301c2e4", null ],
      [ "fEmDecreasing", "d9/d1f/G4EmTableType_8hh.html#aebc1c791eda70608be739b8282b4d7e9aaba5d84b60efefea045610fc5874f0db", null ],
      [ "fEmOnePeak", "d9/d1f/G4EmTableType_8hh.html#aebc1c791eda70608be739b8282b4d7e9a9c45857414603da4ac501d7cada6e8b2", null ],
      [ "fEmTwoPeaks", "d9/d1f/G4EmTableType_8hh.html#aebc1c791eda70608be739b8282b4d7e9ab1cac4dbab879faee29cf31099ff56e8", null ]
    ] ],
    [ "G4EmTableType", "d9/d1f/G4EmTableType_8hh.html#ac0ff9c3adbb3e6e0c4bb413055a604ba", [
      [ "fTotal", "d9/d1f/G4EmTableType_8hh.html#ac0ff9c3adbb3e6e0c4bb413055a604baa37e59df0f8a678ec8a66e6efa0d0f8ae", null ],
      [ "fRestricted", "d9/d1f/G4EmTableType_8hh.html#ac0ff9c3adbb3e6e0c4bb413055a604baaa9457c4138f4d8e888d60647d61c42cd", null ],
      [ "fIsIonisation", "d9/d1f/G4EmTableType_8hh.html#ac0ff9c3adbb3e6e0c4bb413055a604baaad28ac4c77c92a0ea70fd82c958fe606", null ],
      [ "fIsCrossSectionPrim", "d9/d1f/G4EmTableType_8hh.html#ac0ff9c3adbb3e6e0c4bb413055a604baaaaad98a56063007b01f42c437856f8ad", null ]
    ] ]
];