var classG4FTFParamCollection =
[
    [ "~G4FTFParamCollection", "d9/dda/classG4FTFParamCollection.html#acfd920e98c96c78804d8a1f5c21185f6", null ],
    [ "G4FTFParamCollection", "d9/dda/classG4FTFParamCollection.html#aba4d7ba915870698444d76e2185b35a5", null ],
    [ "GetAveragePt2", "d9/dda/classG4FTFParamCollection.html#ab8857491c2e2d709d62a86a9b08357b4", null ],
    [ "GetDeltaProbAtQuarkExchange", "d9/dda/classG4FTFParamCollection.html#a8e892e12a4beb64628f2dde9f4e66432", null ],
    [ "GetDofNuclearDestruct", "d9/dda/classG4FTFParamCollection.html#a8f31e3c92ed1429a4b8f5f271a687b07", null ],
    [ "GetExciEnergyPerWoundedNucleon", "d9/dda/classG4FTFParamCollection.html#a25d33538efc29a4f10b1117025f9dc98", null ],
    [ "GetMaxPt2ofNuclearDestruct", "d9/dda/classG4FTFParamCollection.html#ae7e09a0d492281dc9a834225250e2bf5", null ],
    [ "GetNuclearProjDestructP1", "d9/dda/classG4FTFParamCollection.html#afcd8c3a391ed135a3ac63da226bd40d9", null ],
    [ "GetNuclearProjDestructP2", "d9/dda/classG4FTFParamCollection.html#a2a958936c5cbbd14ef30cd943dbf03ec", null ],
    [ "GetNuclearProjDestructP3", "d9/dda/classG4FTFParamCollection.html#a3cef38ea8bff96a5e0f6883749c699c4", null ],
    [ "GetNuclearTgtDestructP1", "d9/dda/classG4FTFParamCollection.html#ae9e7730054c80b7fef47b39a5498b65f", null ],
    [ "GetNuclearTgtDestructP2", "d9/dda/classG4FTFParamCollection.html#a81465ea975b956582a9bd87478239c51", null ],
    [ "GetNuclearTgtDestructP3", "d9/dda/classG4FTFParamCollection.html#a129cee268f15f2ec87f36b70d438604c", null ],
    [ "GetProbLogDistr", "d9/dda/classG4FTFParamCollection.html#a5f27016b678494f51d803d9d30e61519", null ],
    [ "GetProbLogDistrPrD", "d9/dda/classG4FTFParamCollection.html#a2bd749381d3960ba7968a89252fce13f", null ],
    [ "GetProbOfSameQuarkExchange", "d9/dda/classG4FTFParamCollection.html#ad230878f4befb828e48a7d10efda3252", null ],
    [ "GetProc0A1", "d9/dda/classG4FTFParamCollection.html#afbe0206e7a4bdad554689cb1788c6904", null ],
    [ "GetProc0A2", "d9/dda/classG4FTFParamCollection.html#a2539a20e8ee2575e69b31702bc9cd611", null ],
    [ "GetProc0A3", "d9/dda/classG4FTFParamCollection.html#a91cf0941fbfd64dc5e5ba4f81f853ba6", null ],
    [ "GetProc0Atop", "d9/dda/classG4FTFParamCollection.html#aa2a4f7a27833162d5a21f07c7ae452be", null ],
    [ "GetProc0B1", "d9/dda/classG4FTFParamCollection.html#ac8e2f402fcb8c7374108ae6e6169d2b4", null ],
    [ "GetProc0B2", "d9/dda/classG4FTFParamCollection.html#a58c22c16bd76b458c222c1cc3cafd220", null ],
    [ "GetProc0Ymin", "d9/dda/classG4FTFParamCollection.html#ade014e1613db65ee5b49ca486d574b0f", null ],
    [ "GetProc1A1", "d9/dda/classG4FTFParamCollection.html#a575bc1b82d6426adb6df7987825a4fe4", null ],
    [ "GetProc1A2", "d9/dda/classG4FTFParamCollection.html#aa954f166990882bfa0596533ce209a48", null ],
    [ "GetProc1A3", "d9/dda/classG4FTFParamCollection.html#a75344927c76e5dd986eaf5ab47aa5749", null ],
    [ "GetProc1Atop", "d9/dda/classG4FTFParamCollection.html#a2881df7d1ef68bc0bc891c29c61ae59d", null ],
    [ "GetProc1B1", "d9/dda/classG4FTFParamCollection.html#a75bfc2613f1bd496c2fa70a6da4188c2", null ],
    [ "GetProc1B2", "d9/dda/classG4FTFParamCollection.html#a68a3a5035204d09d5fbd0215a0bfea78", null ],
    [ "GetProc1Ymin", "d9/dda/classG4FTFParamCollection.html#a7c978519e7db2ac99ce819d04224a74b", null ],
    [ "GetProc2A1", "d9/dda/classG4FTFParamCollection.html#aa6b11d9d1f29527eed9b449aa31d9186", null ],
    [ "GetProc2A2", "d9/dda/classG4FTFParamCollection.html#a4d5dddb3b8509c858e61ad0342035e92", null ],
    [ "GetProc2A3", "d9/dda/classG4FTFParamCollection.html#a2a900e90f3358a09289a51a8ca89fce7", null ],
    [ "GetProc2Atop", "d9/dda/classG4FTFParamCollection.html#aa79956e10b9affa34ec17c721ec27de2", null ],
    [ "GetProc2B1", "d9/dda/classG4FTFParamCollection.html#a7d62fe84f20d29f6dd9ae267edeeb891", null ],
    [ "GetProc2B2", "d9/dda/classG4FTFParamCollection.html#aa48b904471d4822ce45720411ae8f461", null ],
    [ "GetProc2Ymin", "d9/dda/classG4FTFParamCollection.html#a455e866e745f7eec6d2094cdebd07106", null ],
    [ "GetProc3A1", "d9/dda/classG4FTFParamCollection.html#ab744060ba3466b8a4d9944ab6d67d6e3", null ],
    [ "GetProc3A2", "d9/dda/classG4FTFParamCollection.html#adfac2a598e41c08ef50f0fe878d9018b", null ],
    [ "GetProc3A3", "d9/dda/classG4FTFParamCollection.html#ae0a7f27928c7f14857b63e8ff6517759", null ],
    [ "GetProc3Atop", "d9/dda/classG4FTFParamCollection.html#a721ebb035d62150636a6c6902c116c18", null ],
    [ "GetProc3B1", "d9/dda/classG4FTFParamCollection.html#a15da14ba0936b185d9a59d595138d140", null ],
    [ "GetProc3B2", "d9/dda/classG4FTFParamCollection.html#a0718ae723237f0913d0bbe0a99b0968f", null ],
    [ "GetProc3Ymin", "d9/dda/classG4FTFParamCollection.html#a4c4e358b87a3e059bf1bff4c369d8c23", null ],
    [ "GetProc4A1", "d9/dda/classG4FTFParamCollection.html#ae8087b3f1c71821040208896cf523ecc", null ],
    [ "GetProc4A2", "d9/dda/classG4FTFParamCollection.html#a3e0c210127d956616fc84c6a4d34a35a", null ],
    [ "GetProc4A3", "d9/dda/classG4FTFParamCollection.html#a0972d2817554b1ff37e5652ebfd42168", null ],
    [ "GetProc4Atop", "d9/dda/classG4FTFParamCollection.html#ab3384f76be1c4eb604caf88eb1285330", null ],
    [ "GetProc4B1", "d9/dda/classG4FTFParamCollection.html#a4ec878a8eec4bb8ab9a2d2beab633cf9", null ],
    [ "GetProc4B2", "d9/dda/classG4FTFParamCollection.html#ad8e10719fc6d290bbb54667efc206cc2", null ],
    [ "GetProc4Ymin", "d9/dda/classG4FTFParamCollection.html#abc18c7fe0f75c9f51dbc195c542c2578", null ],
    [ "GetProjMinDiffMass", "d9/dda/classG4FTFParamCollection.html#aec27065026ea4c8ad50edf0080ea9f78", null ],
    [ "GetProjMinNonDiffMass", "d9/dda/classG4FTFParamCollection.html#a47a7e2c942705b07ae22881705eef6f3", null ],
    [ "GetPt2NuclearDestructP1", "d9/dda/classG4FTFParamCollection.html#a3ff0b8ff1f1ce745971c460c7706d2f8", null ],
    [ "GetPt2NuclearDestructP2", "d9/dda/classG4FTFParamCollection.html#a83856922fbf7b3691ace61c69ab2e1c6", null ],
    [ "GetPt2NuclearDestructP3", "d9/dda/classG4FTFParamCollection.html#a2e573239cae01313ae99670144bee4cd", null ],
    [ "GetPt2NuclearDestructP4", "d9/dda/classG4FTFParamCollection.html#a2cb46cbef420295b038cdd6dd54ba508", null ],
    [ "GetR2ofNuclearDestruct", "d9/dda/classG4FTFParamCollection.html#a866d423a74f32afeabb256287b82842a", null ],
    [ "GetTgtMinDiffMass", "d9/dda/classG4FTFParamCollection.html#ae9a53a36eb80f70f252a03557fc6b3aa", null ],
    [ "GetTgtMinNonDiffMass", "d9/dda/classG4FTFParamCollection.html#aca4ba19e8420c094fd0484df6c024307", null ],
    [ "IsNuclearProjDestructP1_NBRNDEP", "d9/dda/classG4FTFParamCollection.html#a182ac1b29fed0e2a29ce614ee42dbabf", null ],
    [ "IsNuclearTgtDestructP1_ADEP", "d9/dda/classG4FTFParamCollection.html#ae125cc8d5ec6021729ccd475ca91ddec", null ],
    [ "IsProjDiffDissociation", "d9/dda/classG4FTFParamCollection.html#a9616e9ba9b7f0fd9963ba957c010ade5", null ],
    [ "IsTgtDiffDissociation", "d9/dda/classG4FTFParamCollection.html#ad041409e23eb1d4d9af5f78ae5e7ccda", null ],
    [ "fAveragePt2", "d9/dda/classG4FTFParamCollection.html#aa166c60401bef08a3d68fc7b8ba5feab", null ],
    [ "fDeltaProbAtQuarkExchange", "d9/dda/classG4FTFParamCollection.html#a7133a5e9fec5b2d8ed6a3d5a466fed84", null ],
    [ "fDofNuclearDestruct", "d9/dda/classG4FTFParamCollection.html#aa20290a96f487f552f2c5ef6ccbd0559", null ],
    [ "fExciEnergyPerWoundedNucleon", "d9/dda/classG4FTFParamCollection.html#a91203a43995cc7b7de2f5e39743162d7", null ],
    [ "fMaxPt2ofNuclearDestruct", "d9/dda/classG4FTFParamCollection.html#a63899106f4513cd86aa5017f4d8eea34", null ],
    [ "fNuclearProjDestructP1", "d9/dda/classG4FTFParamCollection.html#abeedf361eace1cb0f16c0d4807cdf25b", null ],
    [ "fNuclearProjDestructP1_NBRNDEP", "d9/dda/classG4FTFParamCollection.html#a7883e8e21f1355193a1029d92a774453", null ],
    [ "fNuclearProjDestructP2", "d9/dda/classG4FTFParamCollection.html#a5b4f0c983de68958e6d85cfe9915701f", null ],
    [ "fNuclearProjDestructP3", "d9/dda/classG4FTFParamCollection.html#a480aad1a5ee2056d3dcbda4812f98ebf", null ],
    [ "fNuclearTgtDestructP1", "d9/dda/classG4FTFParamCollection.html#ab6d0c32ccee0357ddbeffc5248d00243", null ],
    [ "fNuclearTgtDestructP1_ADEP", "d9/dda/classG4FTFParamCollection.html#a579620e081b93e7c9bd8b44e083b8d99", null ],
    [ "fNuclearTgtDestructP2", "d9/dda/classG4FTFParamCollection.html#a3dc07a3c2b45012bd4bb6acfcbc75e11", null ],
    [ "fNuclearTgtDestructP3", "d9/dda/classG4FTFParamCollection.html#ae3c200cb0aa348be5853e2eb977b1549", null ],
    [ "fProbLogDistr", "d9/dda/classG4FTFParamCollection.html#a5aeea94fbefc179e15c8b97ab77fb9a1", null ],
    [ "fProbLogDistrPrD", "d9/dda/classG4FTFParamCollection.html#ae8fe78c5137fffca40f4b4ec0abf10d1", null ],
    [ "fProbOfSameQuarkExchange", "d9/dda/classG4FTFParamCollection.html#a5ffa8ef6c60aa82c63b231499bf0ee89", null ],
    [ "fProc0A1", "d9/dda/classG4FTFParamCollection.html#a4436418e0bb6cd1f4d2fa145d585e844", null ],
    [ "fProc0A2", "d9/dda/classG4FTFParamCollection.html#a78f74772df330ceab7f0dbd44a8d6c76", null ],
    [ "fProc0A3", "d9/dda/classG4FTFParamCollection.html#ac97b7f3414014efad46d2f133e122ea4", null ],
    [ "fProc0Atop", "d9/dda/classG4FTFParamCollection.html#a0b3fac176d3806200fe72c5323833698", null ],
    [ "fProc0B1", "d9/dda/classG4FTFParamCollection.html#a51c18973b4e3d49d1385becafa47e967", null ],
    [ "fProc0B2", "d9/dda/classG4FTFParamCollection.html#ad3aeb694096b45efd00cc24a6ca1cb67", null ],
    [ "fProc0Ymin", "d9/dda/classG4FTFParamCollection.html#a20877cdac71a7fb4f9bdb2c5db45b48f", null ],
    [ "fProc1A1", "d9/dda/classG4FTFParamCollection.html#af6eb1aae42242541a75fbaa99c0f1a41", null ],
    [ "fProc1A2", "d9/dda/classG4FTFParamCollection.html#a983a6718911cf2f1fe5417f97b7fdded", null ],
    [ "fProc1A3", "d9/dda/classG4FTFParamCollection.html#af777a6443ff795837d850a87b1bf1312", null ],
    [ "fProc1Atop", "d9/dda/classG4FTFParamCollection.html#a9151c70d2a3b35699a5d0c6d37b59f32", null ],
    [ "fProc1B1", "d9/dda/classG4FTFParamCollection.html#a0f7f61b323ace344cb054de8a6a713d8", null ],
    [ "fProc1B2", "d9/dda/classG4FTFParamCollection.html#a21205fb0e6abaa6cecdc88919d4f0c94", null ],
    [ "fProc1Ymin", "d9/dda/classG4FTFParamCollection.html#acffa4cffb5907a9017532bd132dd4a53", null ],
    [ "fProc2A1", "d9/dda/classG4FTFParamCollection.html#aa95b1271bc4ec75accb91d5a06c4868a", null ],
    [ "fProc2A2", "d9/dda/classG4FTFParamCollection.html#a3234b37d2e100ae1d2948e96076342a3", null ],
    [ "fProc2A3", "d9/dda/classG4FTFParamCollection.html#aff1f112b80e02ff79a4bbd281fa03b12", null ],
    [ "fProc2Atop", "d9/dda/classG4FTFParamCollection.html#a1485b4f886c16db4b8617e3b0d3cf510", null ],
    [ "fProc2B1", "d9/dda/classG4FTFParamCollection.html#a9c38daa9d32dda4336cb4501bf5f953f", null ],
    [ "fProc2B2", "d9/dda/classG4FTFParamCollection.html#a62b498200fd2d0c87524e1d38eb601d6", null ],
    [ "fProc2Ymin", "d9/dda/classG4FTFParamCollection.html#a4e0bfc344e026eba2678c4d679e5fca2", null ],
    [ "fProc3A1", "d9/dda/classG4FTFParamCollection.html#a67fbb850f2e0ec3c0cb89f02304999c6", null ],
    [ "fProc3A2", "d9/dda/classG4FTFParamCollection.html#a2da922b01c7cae0446ffcbd8155a8520", null ],
    [ "fProc3A3", "d9/dda/classG4FTFParamCollection.html#a95a98cf0662e2fcdf0b633a488d7090e", null ],
    [ "fProc3Atop", "d9/dda/classG4FTFParamCollection.html#a614426676bebb27f7c6bbb5fd4c7cbda", null ],
    [ "fProc3B1", "d9/dda/classG4FTFParamCollection.html#a1992416b1b5d53f9788fd3fe1d931e30", null ],
    [ "fProc3B2", "d9/dda/classG4FTFParamCollection.html#a6706e42c3f4d8eef9b19e0b446f2cbbd", null ],
    [ "fProc3Ymin", "d9/dda/classG4FTFParamCollection.html#aa844278f0860ef5c225b55ce668df2ab", null ],
    [ "fProc4A1", "d9/dda/classG4FTFParamCollection.html#a33ecd28fca17fda608e97a72442b6a55", null ],
    [ "fProc4A2", "d9/dda/classG4FTFParamCollection.html#a8185737e571085947c8e336f6c4af659", null ],
    [ "fProc4A3", "d9/dda/classG4FTFParamCollection.html#a624dd2380e018cdb77a5a7eec62eccb6", null ],
    [ "fProc4Atop", "d9/dda/classG4FTFParamCollection.html#a258299fe83ceeba09c8386cb0a0e7a9e", null ],
    [ "fProc4B1", "d9/dda/classG4FTFParamCollection.html#a33167dc959ea1cb767df11dac06b66b5", null ],
    [ "fProc4B2", "d9/dda/classG4FTFParamCollection.html#aa124afa4464654653dd47705959d3f09", null ],
    [ "fProc4Ymin", "d9/dda/classG4FTFParamCollection.html#adf5940b1a13528ce75ac17dbcc2f53f7", null ],
    [ "fProjDiffDissociation", "d9/dda/classG4FTFParamCollection.html#a0e8f0417aca94d40fec0993003e60deb", null ],
    [ "fProjMinDiffMass", "d9/dda/classG4FTFParamCollection.html#a6498f35502c20575baac40f7f311b406", null ],
    [ "fProjMinNonDiffMass", "d9/dda/classG4FTFParamCollection.html#af12ad77ff592ab6e3e543949c9a13b72", null ],
    [ "fPt2NuclearDestructP1", "d9/dda/classG4FTFParamCollection.html#a307f3710adaedec4491680e89ba5b177", null ],
    [ "fPt2NuclearDestructP2", "d9/dda/classG4FTFParamCollection.html#a7079bd27859bb6fd41530b3971acb323", null ],
    [ "fPt2NuclearDestructP3", "d9/dda/classG4FTFParamCollection.html#aad0539f157b877a32e0f082c45b2db0a", null ],
    [ "fPt2NuclearDestructP4", "d9/dda/classG4FTFParamCollection.html#a4a601d860f39263910f61e426f58d35e", null ],
    [ "fR2ofNuclearDestruct", "d9/dda/classG4FTFParamCollection.html#a35a155f32ff21c8c793454c272071efa", null ],
    [ "fTgtDiffDissociation", "d9/dda/classG4FTFParamCollection.html#aa5ca96c79c8a9224a46c2d79e589dba4", null ],
    [ "fTgtMinDiffMass", "d9/dda/classG4FTFParamCollection.html#ab1713261a5ad8bcac3678e7a15c5df68", null ],
    [ "fTgtMinNonDiffMass", "d9/dda/classG4FTFParamCollection.html#ab087ade866ef4d3bcd27ebb03121ca73", null ]
];