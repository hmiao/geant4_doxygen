var classG4Step =
[
    [ "ProfilerConfig", "d9/d7a/classG4Step.html#a847fbde17fb01d0dc7e11894a5ba3d67", null ],
    [ "G4Step", "d9/d7a/classG4Step.html#a60cb68928daea41f0da5082bf3eeae78", null ],
    [ "~G4Step", "d9/d7a/classG4Step.html#a28b930b3bcfa574bc32f7726ddfeed68", null ],
    [ "G4Step", "d9/d7a/classG4Step.html#abd874ebf75e9bd8159fe1bb5eb2d81af", null ],
    [ "AddNonIonizingEnergyDeposit", "d9/d7a/classG4Step.html#a5fd626411a4ee48f097f6c87f598c7c3", null ],
    [ "AddTotalEnergyDeposit", "d9/d7a/classG4Step.html#a82ecc3cfb0e94aebcce40e82b4f71467", null ],
    [ "ClearFirstStepFlag", "d9/d7a/classG4Step.html#ad697bbe2690fefd391314766fbc2eb9b", null ],
    [ "ClearLastStepFlag", "d9/d7a/classG4Step.html#ad529c39932e4b25d755b3be71bfe8404", null ],
    [ "CopyPostToPreStepPoint", "d9/d7a/classG4Step.html#aa967077b66907eaa6e4db28b835b23d7", null ],
    [ "CreatePolyline", "d9/d7a/classG4Step.html#a252a3f34a29028b846779d4343feadc2", null ],
    [ "DeleteSecondaryVector", "d9/d7a/classG4Step.html#a0a025fc44008d81aabe171bf1c18556a", null ],
    [ "GetControlFlag", "d9/d7a/classG4Step.html#a1fd059db3f86c58fa57bd9642ab39569", null ],
    [ "GetDeltaEnergy", "d9/d7a/classG4Step.html#a2f55ebb8f41e5aedd43312b147a1027e", null ],
    [ "GetDeltaMomentum", "d9/d7a/classG4Step.html#a483b1ae25d7a3d54d29af141dbcb6969", null ],
    [ "GetDeltaPosition", "d9/d7a/classG4Step.html#a954d634341eb03d09132dce2df83726c", null ],
    [ "GetDeltaTime", "d9/d7a/classG4Step.html#a0fd1c45fe277955b949b1f3e6ac8ab2f", null ],
    [ "GetfSecondary", "d9/d7a/classG4Step.html#a6842a07b604f875b98dcb2a6b9914ed5", null ],
    [ "GetNonIonizingEnergyDeposit", "d9/d7a/classG4Step.html#abad3c8682126eb208f147ef3432e0562", null ],
    [ "GetNumberOfSecondariesInCurrentStep", "d9/d7a/classG4Step.html#a95a8fedbb0b3753803919e5ba7cf869b", null ],
    [ "GetPointerToVectorOfAuxiliaryPoints", "d9/d7a/classG4Step.html#a26841a77092e1852c77d34324c31c7b5", null ],
    [ "GetPostStepPoint", "d9/d7a/classG4Step.html#aebf22f9c5aac753fe86478e738d0886f", null ],
    [ "GetPreStepPoint", "d9/d7a/classG4Step.html#ab177d54c68c7038208b5c2c03c58a435", null ],
    [ "GetSecondary", "d9/d7a/classG4Step.html#a935ab44d623553627ed3567487210341", null ],
    [ "GetSecondaryInCurrentStep", "d9/d7a/classG4Step.html#a899bcfee44fc2f1c33842f7e13b05a0b", null ],
    [ "GetStepLength", "d9/d7a/classG4Step.html#ac3077666a7883639a5dbbba06a8b1b85", null ],
    [ "GetTotalEnergyDeposit", "d9/d7a/classG4Step.html#ac68863f70024bde7165d2757233a542e", null ],
    [ "GetTrack", "d9/d7a/classG4Step.html#a28b34abd72d663b531ea69372a85792c", null ],
    [ "InitializeStep", "d9/d7a/classG4Step.html#a14f1f1f3d489d39d1524f34674b8fb10", null ],
    [ "IsFirstStepInVolume", "d9/d7a/classG4Step.html#a834ebb3a46aa8d5d43aadffbc45cceeb", null ],
    [ "IsLastStepInVolume", "d9/d7a/classG4Step.html#a7ece06ec136958b67d89e764aa1b13d4", null ],
    [ "NewSecondaryVector", "d9/d7a/classG4Step.html#ace52801abde07044b0ea1194b17bb3cc", null ],
    [ "operator=", "d9/d7a/classG4Step.html#a6638e74b6cd84f24e2f701ed2dede1c4", null ],
    [ "ResetNonIonizingEnergyDeposit", "d9/d7a/classG4Step.html#aebf9682704f35637184b843f199c2064", null ],
    [ "ResetTotalEnergyDeposit", "d9/d7a/classG4Step.html#a46d401f8bf2f58b90436b85e1c9884bd", null ],
    [ "SetControlFlag", "d9/d7a/classG4Step.html#ad064aeb3455bbbcfc8bb732f0a24a264", null ],
    [ "SetFirstStepFlag", "d9/d7a/classG4Step.html#ad53f753787cd066b82be2b703ef49e7c", null ],
    [ "SetLastStepFlag", "d9/d7a/classG4Step.html#a5a703fd3c272d11c0abe25b1bdd6f41a", null ],
    [ "SetNonIonizingEnergyDeposit", "d9/d7a/classG4Step.html#aa96f14b774f53024134940c459cb705f", null ],
    [ "SetPointerToVectorOfAuxiliaryPoints", "d9/d7a/classG4Step.html#a14217a03a659281aa53590242e1a7441", null ],
    [ "SetPostStepPoint", "d9/d7a/classG4Step.html#a63a85b11e001b30611f8075144d3726c", null ],
    [ "SetPreStepPoint", "d9/d7a/classG4Step.html#ad29a08a29ecd7285fd0ec3266afc57e5", null ],
    [ "SetSecondary", "d9/d7a/classG4Step.html#ace7815aef249d174b26c186333b5388a", null ],
    [ "SetStepLength", "d9/d7a/classG4Step.html#a5a9e931167900640af83194f85c04d43", null ],
    [ "SetTotalEnergyDeposit", "d9/d7a/classG4Step.html#ad8fe9389a2649a1de456df55963e1f02", null ],
    [ "SetTrack", "d9/d7a/classG4Step.html#afc1de34603d19d96d6f5628c3f4fdd6e", null ],
    [ "UpdateTrack", "d9/d7a/classG4Step.html#a4276df6a2711290873268b6d5f4af2e5", null ],
    [ "fFirstStepInVolume", "d9/d7a/classG4Step.html#a8d1508413f31a937a245550afffa63e1", null ],
    [ "fLastStepInVolume", "d9/d7a/classG4Step.html#a8b587d37728abb5b86abfe1f7c19e3dd", null ],
    [ "fNonIonizingEnergyDeposit", "d9/d7a/classG4Step.html#abd88821e954089e62ee582351b24ce5f", null ],
    [ "fpPostStepPoint", "d9/d7a/classG4Step.html#ad72db05970111ccea0d3bae2e56a430c", null ],
    [ "fpPreStepPoint", "d9/d7a/classG4Step.html#a0f16deadf5f22703df16c168ad227f35", null ],
    [ "fpSteppingControlFlag", "d9/d7a/classG4Step.html#acb28fe23a891bcd433cabd4ab25a4546", null ],
    [ "fpTrack", "d9/d7a/classG4Step.html#a3ac4a444d5d5af43539fc83f4f1519b5", null ],
    [ "fpVectorOfAuxiliaryPointsPointer", "d9/d7a/classG4Step.html#a175c0922d86c2ca2b0145b6d6708ddbc", null ],
    [ "fSecondary", "d9/d7a/classG4Step.html#a87ca49abbe2805c3b1473b62f1f73a5e", null ],
    [ "fStepLength", "d9/d7a/classG4Step.html#a7ed33ef8eb5ffadaccc186dc81d22efe", null ],
    [ "fTotalEnergyDeposit", "d9/d7a/classG4Step.html#ad84662d987676236deeb90e9c6ef67d4", null ],
    [ "nSecondaryByLastStep", "d9/d7a/classG4Step.html#a37288aced76a8caebda0f50395055e82", null ],
    [ "secondaryInCurrentStep", "d9/d7a/classG4Step.html#a007ec5d313664f3464311c0b11191ee3", null ]
];