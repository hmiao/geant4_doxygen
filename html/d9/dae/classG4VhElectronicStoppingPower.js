var classG4VhElectronicStoppingPower =
[
    [ "G4VhElectronicStoppingPower", "d9/dae/classG4VhElectronicStoppingPower.html#a152c9cbfc594509970a37cd6e714ac1a", null ],
    [ "~G4VhElectronicStoppingPower", "d9/dae/classG4VhElectronicStoppingPower.html#abef62748eef10fd95e61fa06c17358de", null ],
    [ "G4VhElectronicStoppingPower", "d9/dae/classG4VhElectronicStoppingPower.html#a6d99dbf659589d2f0d7c199941b9a93d", null ],
    [ "ElectronicStoppingPower", "d9/dae/classG4VhElectronicStoppingPower.html#af3e447b33426898750047c8dee493456", null ],
    [ "GetHeMassAMU", "d9/dae/classG4VhElectronicStoppingPower.html#a8d196d812849356aecb35bc38ce6474c", null ],
    [ "HasMaterial", "d9/dae/classG4VhElectronicStoppingPower.html#ade921ee102082783e2041e672535104c", null ],
    [ "HeEffChargeSquare", "d9/dae/classG4VhElectronicStoppingPower.html#a18a3e7eedfd34fec11dd3aadbbfc8be5", null ],
    [ "operator=", "d9/dae/classG4VhElectronicStoppingPower.html#a75e2ebf74ee65ace82b73110100f85fc", null ],
    [ "StoppingPower", "d9/dae/classG4VhElectronicStoppingPower.html#a0f1caa90f7a8f7c2e876e4ff4b75cf2d", null ],
    [ "theHeMassAMU", "d9/dae/classG4VhElectronicStoppingPower.html#ab69860121f92ecece6c14f80dedadaf4", null ]
];