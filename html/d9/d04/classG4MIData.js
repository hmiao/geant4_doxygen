var classG4MIData =
[
    [ "G4MIData", "d9/d04/classG4MIData.html#ab5b47e65adc99235bb00da2a8a6ce804", null ],
    [ "~G4MIData", "d9/d04/classG4MIData.html#af44cc6f47dee6af115ca6ce5dbfaeb31", null ],
    [ "GetFilenameCS", "d9/d04/classG4MIData.html#a8d5535aba33eec558cd94193891ac9ca", null ],
    [ "GetFilenameFF", "d9/d04/classG4MIData.html#a23bc654dfc24b9fbbf3a3363ffc2183b", null ],
    [ "GetMolWeight", "d9/d04/classG4MIData.html#a360c2e0930c7b83c28632b6b5b6526d1", null ],
    [ "Print", "d9/d04/classG4MIData.html#a6493949c9b7f61ee21c7dad5dd83b2e8", null ],
    [ "SetFilenameCS", "d9/d04/classG4MIData.html#abdf30f1f65e1f6c74d411b7b8b668987", null ],
    [ "SetFilenameFF", "d9/d04/classG4MIData.html#a926c09fc94a5c053cc72417a216764a4", null ],
    [ "SetMolWeight", "d9/d04/classG4MIData.html#a79afb7d1f6096c45a15768144c768a3f", null ],
    [ "fFilenameCS", "d9/d04/classG4MIData.html#a825a485c811add72bc214d8a15853207", null ],
    [ "fFilenameFF", "d9/d04/classG4MIData.html#aad3bf8d2d0b10e911adef7b001f2d4d1", null ],
    [ "fMolWeight", "d9/d04/classG4MIData.html#a2a781e66de495435be286f69503e3382", null ]
];