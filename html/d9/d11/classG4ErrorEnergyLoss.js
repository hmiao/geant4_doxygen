var classG4ErrorEnergyLoss =
[
    [ "G4ErrorEnergyLoss", "d9/d11/classG4ErrorEnergyLoss.html#aa0f340c001354d353b4c189e33757175", null ],
    [ "~G4ErrorEnergyLoss", "d9/d11/classG4ErrorEnergyLoss.html#aff97189d1d96d81d3f7c8ac3b7d463a7", null ],
    [ "G4ErrorEnergyLoss", "d9/d11/classG4ErrorEnergyLoss.html#a09bef1f9d775f6385aff7346d0e69b64", null ],
    [ "AlongStepDoIt", "d9/d11/classG4ErrorEnergyLoss.html#a6cd08338c876907863c78a7f830a3537", null ],
    [ "GetContinuousStepLimit", "d9/d11/classG4ErrorEnergyLoss.html#a50c6b03ec237ac098af4855402483115", null ],
    [ "GetStepLimit", "d9/d11/classG4ErrorEnergyLoss.html#ad1f2bbf12ba7aa90c31fb5a4cc12cb6e", null ],
    [ "InstantiateEforExtrapolator", "d9/d11/classG4ErrorEnergyLoss.html#ac7393dc46895a8a33b2999db86a38df1", null ],
    [ "IsApplicable", "d9/d11/classG4ErrorEnergyLoss.html#aafe0d77250ba20c5046f8fb93480f952", null ],
    [ "operator=", "d9/d11/classG4ErrorEnergyLoss.html#a3deb090ad1792e4aae5db4bc8df55964", null ],
    [ "SetStepLimit", "d9/d11/classG4ErrorEnergyLoss.html#ae8cb8e33a14aa51fd2211730b6658e79", null ],
    [ "theELossForExtrapolator", "d9/d11/classG4ErrorEnergyLoss.html#ac1c49418440acb3925f167524b287bdc", null ],
    [ "theFractionLimit", "d9/d11/classG4ErrorEnergyLoss.html#a4e8cced73cb6a1c72fc2c473830e7a94", null ],
    [ "theStepLimit", "d9/d11/classG4ErrorEnergyLoss.html#ac408235d63f32ed54e8c00e926e9d1d0", null ]
];