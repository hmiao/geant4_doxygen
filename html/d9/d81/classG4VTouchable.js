var classG4VTouchable =
[
    [ "G4VTouchable", "d9/d81/classG4VTouchable.html#a0f75517bc0e3e8388a3074e9757c17c2", null ],
    [ "~G4VTouchable", "d9/d81/classG4VTouchable.html#aedd59f51b24e66cb6632533c13777d8a", null ],
    [ "GetCopyNumber", "d9/d81/classG4VTouchable.html#a78e2116fa24e1e3921cd1a27897b14a2", null ],
    [ "GetHistory", "d9/d81/classG4VTouchable.html#a7ed192711ab706542cc17438eddce469", null ],
    [ "GetHistoryDepth", "d9/d81/classG4VTouchable.html#ae12d33cd03911973504cf1afca415d59", null ],
    [ "GetReplicaNumber", "d9/d81/classG4VTouchable.html#aeb9d8a026b76faf71c28bc3cf9612401", null ],
    [ "GetRotation", "d9/d81/classG4VTouchable.html#a4848b3c00b8d232ba7a6821098cd1bd1", null ],
    [ "GetSolid", "d9/d81/classG4VTouchable.html#a31e11179865b47e0743a0db0c954d87c", null ],
    [ "GetTranslation", "d9/d81/classG4VTouchable.html#a1bf796f7680f2386c233c6017ded0aee", null ],
    [ "GetVolume", "d9/d81/classG4VTouchable.html#aa384105c4da9ce3356136eac2fa6d821", null ],
    [ "MoveUpHistory", "d9/d81/classG4VTouchable.html#a10a4857dde198833902a4a041faca533", null ],
    [ "UpdateYourself", "d9/d81/classG4VTouchable.html#aba7f69fcf720a56189e6d0d2661e3680", null ]
];