var classG4ICRU90StoppingData =
[
    [ "G4ICRU90StoppingData", "d9/d2c/classG4ICRU90StoppingData.html#a5156144e9927652fb8c039d69ca27770", null ],
    [ "~G4ICRU90StoppingData", "d9/d2c/classG4ICRU90StoppingData.html#a4d27ac7370c2817d06dc211ef4c6c31b", null ],
    [ "G4ICRU90StoppingData", "d9/d2c/classG4ICRU90StoppingData.html#a13badd4555c460ba8ebc9a787315a8e0", null ],
    [ "AddData", "d9/d2c/classG4ICRU90StoppingData.html#a8a8b9a630b041b09ddcb791db2c2a45a", null ],
    [ "FillData", "d9/d2c/classG4ICRU90StoppingData.html#ab3e4261535a6e17dbf945288e9ac8f96", null ],
    [ "GetDEDX", "d9/d2c/classG4ICRU90StoppingData.html#a51cd1fdac1a75836f47dfe681c5608f4", null ],
    [ "GetElectronicDEDXforAlpha", "d9/d2c/classG4ICRU90StoppingData.html#adc175f365107128131671054a7c3c074", null ],
    [ "GetElectronicDEDXforAlpha", "d9/d2c/classG4ICRU90StoppingData.html#a17e75a062bed7508d4b14db100d00cc9", null ],
    [ "GetElectronicDEDXforProton", "d9/d2c/classG4ICRU90StoppingData.html#a559f0b3801d0f1ae4bd158c48c54ccf3", null ],
    [ "GetElectronicDEDXforProton", "d9/d2c/classG4ICRU90StoppingData.html#a7debfadb73782ea1d7c4e6ca2fb23ea1", null ],
    [ "GetIndex", "d9/d2c/classG4ICRU90StoppingData.html#a772eb57d795640a1c04553c5a127fbdd", null ],
    [ "GetIndex", "d9/d2c/classG4ICRU90StoppingData.html#a9bacb86d579f6d6e1afff924e8ad5e03", null ],
    [ "Initialise", "d9/d2c/classG4ICRU90StoppingData.html#abefae004487b88e5b2bc5eeb9be2e04b", null ],
    [ "IsApplicable", "d9/d2c/classG4ICRU90StoppingData.html#a0e2202f60d4026031a31c0045001731a", null ],
    [ "operator=", "d9/d2c/classG4ICRU90StoppingData.html#a7426a5666778c4fb41d6d2958629eed2", null ],
    [ "isInitialized", "d9/d2c/classG4ICRU90StoppingData.html#a9c1a688103735e3625b3d4159f28edb9", null ],
    [ "materials", "d9/d2c/classG4ICRU90StoppingData.html#aa53181b5e6302f26c5644e391b694dad", null ],
    [ "nvectors", "d9/d2c/classG4ICRU90StoppingData.html#a9d04556e2e5d13358582fa487cfecb66", null ],
    [ "sdata_alpha", "d9/d2c/classG4ICRU90StoppingData.html#adfa96f8e73c223427061edac878d2e91", null ],
    [ "sdata_proton", "d9/d2c/classG4ICRU90StoppingData.html#a7ad13abc878e23d45879ed2d6092da94", null ]
];