var classG4QuadrupoleMagField =
[
    [ "G4QuadrupoleMagField", "d9/d2f/classG4QuadrupoleMagField.html#a41595eec794a3e71efc85f3877dad8b8", null ],
    [ "G4QuadrupoleMagField", "d9/d2f/classG4QuadrupoleMagField.html#a36b352cc2f324b114861ddb1b7b818d7", null ],
    [ "~G4QuadrupoleMagField", "d9/d2f/classG4QuadrupoleMagField.html#a07ed7cf52df2ce88c92a5db0ad0b266a", null ],
    [ "Clone", "d9/d2f/classG4QuadrupoleMagField.html#ab1c450e26731fe56a34db7a3c3b6ce14", null ],
    [ "GetFieldValue", "d9/d2f/classG4QuadrupoleMagField.html#a081da63cf801c7badab5ed8c257d1d5c", null ],
    [ "fGradient", "d9/d2f/classG4QuadrupoleMagField.html#a8c5524b471ccb552a63e9a732052f760", null ],
    [ "fOrigin", "d9/d2f/classG4QuadrupoleMagField.html#a72336edbc718958388e4a5e9b4709115", null ],
    [ "fpMatrix", "d9/d2f/classG4QuadrupoleMagField.html#a75b6d62078fee88fef585a4d65e78e81", null ]
];