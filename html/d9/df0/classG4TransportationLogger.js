var classG4TransportationLogger =
[
    [ "G4TransportationLogger", "d9/df0/classG4TransportationLogger.html#a1731270ba271b0eb10efd6a48776203b", null ],
    [ "G4TransportationLogger", "d9/df0/classG4TransportationLogger.html#a7ffbf6d4114bf5da76a6a534465430cf", null ],
    [ "~G4TransportationLogger", "d9/df0/classG4TransportationLogger.html#a4e8e278a220457ad99f2f07825cdf922", null ],
    [ "GetThresholdImportantEnergy", "d9/df0/classG4TransportationLogger.html#a50accc1cc5532a9276a59430f1e019f1", null ],
    [ "GetThresholdTrials", "d9/df0/classG4TransportationLogger.html#a87152c89e7198bc2fe76f22c02e64b61", null ],
    [ "GetThresholdWarningEnergy", "d9/df0/classG4TransportationLogger.html#a95009481fc7601a345232f18369b3fed", null ],
    [ "GetVerboseLevel", "d9/df0/classG4TransportationLogger.html#a8fedf2f2f918ea5c0f3ab401228bb643", null ],
    [ "ReportLooperThresholds", "d9/df0/classG4TransportationLogger.html#af28e4a8289cc70698060d94ef6ebe3ba", null ],
    [ "ReportLoopingTrack", "d9/df0/classG4TransportationLogger.html#ad120ab686357cc3195e89a1d22872678", null ],
    [ "SetThresholdImportantEnergy", "d9/df0/classG4TransportationLogger.html#a3cab1eee8449106396695ea94e76696d", null ],
    [ "SetThresholds", "d9/df0/classG4TransportationLogger.html#abbee5cc76750f9d903dc32ff24c813a3", null ],
    [ "SetThresholdTrials", "d9/df0/classG4TransportationLogger.html#a67e449387d3fbeb5cdbee21c85a9ed04", null ],
    [ "SetThresholdWarningEnergy", "d9/df0/classG4TransportationLogger.html#a1d4745bc45c5939b16f0da7fcfaffbbe", null ],
    [ "SetVerboseLevel", "d9/df0/classG4TransportationLogger.html#ae652ff90553098d57223fcd7da1cc4c1", null ],
    [ "fClassName", "d9/df0/classG4TransportationLogger.html#abe4c8c33b0439beced291d544d40a078", null ],
    [ "fThldImportantEnergy", "d9/df0/classG4TransportationLogger.html#ac1fb94e3e7950c86bac0d1a534a484e2", null ],
    [ "fThldTrials", "d9/df0/classG4TransportationLogger.html#a3be422b76b2d2423b66b51725884ee66", null ],
    [ "fThldWarningEnergy", "d9/df0/classG4TransportationLogger.html#ac5e07ce76cfe0125d9be054d43da9b2b", null ],
    [ "fVerbose", "d9/df0/classG4TransportationLogger.html#ac4771154870becccf75a923c6ed963be", null ]
];