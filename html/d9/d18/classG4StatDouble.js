var classG4StatDouble =
[
    [ "G4StatDouble", "d9/d18/classG4StatDouble.html#a36d6774c0069b4f84a90d9e90cc38fc0", null ],
    [ "G4StatDouble", "d9/d18/classG4StatDouble.html#af1bb3ad2ac19460764e41ed81f28bddf", null ],
    [ "~G4StatDouble", "d9/d18/classG4StatDouble.html#a1f7e672383f9f19146fb0d2ea04fb26b", null ],
    [ "G4StatDouble", "d9/d18/classG4StatDouble.html#a2bc75c50e526f3774a2572c1c0840ccf", null ],
    [ "add", "d9/d18/classG4StatDouble.html#a7c12f29731b3df6217292d3f582197f7", null ],
    [ "fill", "d9/d18/classG4StatDouble.html#ada63640fbfb002972bd5a6dfabe34d27", null ],
    [ "mean", "d9/d18/classG4StatDouble.html#aa44363ed527510f87573c6151d9014cf", null ],
    [ "mean", "d9/d18/classG4StatDouble.html#a73fb46bd7e517bcd0bbbf760480a1698", null ],
    [ "n", "d9/d18/classG4StatDouble.html#a9b8e95bbe6f385f25934bd870b3e17ef", null ],
    [ "operator+=", "d9/d18/classG4StatDouble.html#ace74655d711cb5dcf3c662da62a03f90", null ],
    [ "operator+=", "d9/d18/classG4StatDouble.html#a8d2c212fd2b3945f5a1cc4b726ca3273", null ],
    [ "operator=", "d9/d18/classG4StatDouble.html#a7b4ca5ab88b138079303513c80edfb0a", null ],
    [ "operator=", "d9/d18/classG4StatDouble.html#a7e1a544bab862657e8775cb2c509b8ee", null ],
    [ "reset", "d9/d18/classG4StatDouble.html#a061e3773026bc76d174d744371b1e15c", null ],
    [ "rms", "d9/d18/classG4StatDouble.html#a6aadb448b2efc501baf9a88c26f1bdf7", null ],
    [ "rms", "d9/d18/classG4StatDouble.html#aff64fb0150b049adc571d28a14d6ace3", null ],
    [ "rms", "d9/d18/classG4StatDouble.html#aadaff32abfffa1a7f6865cdfe193f647", null ],
    [ "scale", "d9/d18/classG4StatDouble.html#af8878e39a6720cc1d1736e6382301765", null ],
    [ "sum_w", "d9/d18/classG4StatDouble.html#a302250e5b78711041853a2ae1aa075cd", null ],
    [ "sum_w2", "d9/d18/classG4StatDouble.html#a8b90316c7f58f63549c1c091d240d8d7", null ],
    [ "sum_wx", "d9/d18/classG4StatDouble.html#af47d2f1d92dc5ddf77fdeb95ddce401d", null ],
    [ "sum_wx2", "d9/d18/classG4StatDouble.html#adcec1bca8fbe042ac44f33f2e99ef73f", null ],
    [ "m_n", "d9/d18/classG4StatDouble.html#ac63d797323b06ca2e7033658a40fa903", null ],
    [ "m_scale", "d9/d18/classG4StatDouble.html#a7aaa1c68c914c80e7e6835185f9c14ea", null ],
    [ "m_sum_w", "d9/d18/classG4StatDouble.html#a5e140c6974c6e2caa8f902929cecd5a8", null ],
    [ "m_sum_w2", "d9/d18/classG4StatDouble.html#aa209278452b453468e50330494580608", null ],
    [ "m_sum_wx", "d9/d18/classG4StatDouble.html#a073e02f06790f9dd9bda67b0bb19d0cd", null ],
    [ "m_sum_wx2", "d9/d18/classG4StatDouble.html#abf3aa8f3254a5050ce1b79705cfe61c9", null ]
];