var classG4He3EvaporationChannel =
[
    [ "G4He3EvaporationChannel", "d9/d80/classG4He3EvaporationChannel.html#a035e7f24daf9a896079cb4e44c2003e5", null ],
    [ "~G4He3EvaporationChannel", "d9/d80/classG4He3EvaporationChannel.html#ab521fe9a08847881f846a60457d2462e", null ],
    [ "G4He3EvaporationChannel", "d9/d80/classG4He3EvaporationChannel.html#a094ebe169e4ce358a6686f31be90e864", null ],
    [ "operator!=", "d9/d80/classG4He3EvaporationChannel.html#a921b111837786a4bef60e6b744719591", null ],
    [ "operator=", "d9/d80/classG4He3EvaporationChannel.html#aafb3da0b017bd2edf78fbbb0de693f75", null ],
    [ "operator==", "d9/d80/classG4He3EvaporationChannel.html#adc98a02a9d1ad59f1c126f13402aae7e", null ],
    [ "pr", "d9/d80/classG4He3EvaporationChannel.html#ae14046ff80d5189e40e924a6b2751f9f", null ]
];