var classG4LocatorChangeRecord =
[
    [ "EChangeLocation", "d9/dfd/classG4LocatorChangeRecord.html#a294318a567cb307d8c8550f900c7cfa1", [
      [ "kInvalidCL", "d9/dfd/classG4LocatorChangeRecord.html#a294318a567cb307d8c8550f900c7cfa1a464a42370d0bda0562bebaa0e770dc81", null ],
      [ "kUnknownCL", "d9/dfd/classG4LocatorChangeRecord.html#a294318a567cb307d8c8550f900c7cfa1a547ede5bcb066bdc567fd8c1ce054070", null ],
      [ "kInitialisingCL", "d9/dfd/classG4LocatorChangeRecord.html#a294318a567cb307d8c8550f900c7cfa1a76e779e033b876932e55c22142360889", null ],
      [ "kIntersectsAF", "d9/dfd/classG4LocatorChangeRecord.html#a294318a567cb307d8c8550f900c7cfa1a431dbb39f25bbc07c685d9531222dd6d", null ],
      [ "kIntersectsFB", "d9/dfd/classG4LocatorChangeRecord.html#a294318a567cb307d8c8550f900c7cfa1a3599bddfa275547dd9225fc9bf64c845", null ],
      [ "kNoIntersectAForFB", "d9/dfd/classG4LocatorChangeRecord.html#a294318a567cb307d8c8550f900c7cfa1a5c3bee5d4de159c4989fbb62b4e8d031", null ],
      [ "kRecalculatedB", "d9/dfd/classG4LocatorChangeRecord.html#a294318a567cb307d8c8550f900c7cfa1a668d99c3ce4a46d8c0ef14dbafd9e705", null ],
      [ "kInsertingMidPoint", "d9/dfd/classG4LocatorChangeRecord.html#a294318a567cb307d8c8550f900c7cfa1a08772979b738fec13655f89cb12e05fa", null ],
      [ "kRecalculatedBagn", "d9/dfd/classG4LocatorChangeRecord.html#a294318a567cb307d8c8550f900c7cfa1a8267301a98f588fc2eeb1142ab07df9f", null ],
      [ "kLevelPop", "d9/dfd/classG4LocatorChangeRecord.html#a294318a567cb307d8c8550f900c7cfa1a44105ecf1fde05feaeb363d169c40738", null ]
    ] ],
    [ "G4LocatorChangeRecord", "d9/dfd/classG4LocatorChangeRecord.html#a0412e9ef6feab289b697d056aec76807", null ],
    [ "G4LocatorChangeRecord", "d9/dfd/classG4LocatorChangeRecord.html#a87e2dd7dabe6ba7258eb03c74b39852d", null ],
    [ "G4LocatorChangeRecord", "d9/dfd/classG4LocatorChangeRecord.html#a55327d05362f6c259ba79f915f9edaca", null ],
    [ "GetCount", "d9/dfd/classG4LocatorChangeRecord.html#a6cd5ea06fc5d44a7441b2dbbe7a80883", null ],
    [ "GetIteration", "d9/dfd/classG4LocatorChangeRecord.html#ac253462a898474c4b62e9ef41ffe671c", null ],
    [ "GetLength", "d9/dfd/classG4LocatorChangeRecord.html#a0ece51962e46c32e7d2f5bf99c82633c", null ],
    [ "GetLocation", "d9/dfd/classG4LocatorChangeRecord.html#a4ea6823ca04953e5f64b27652ccad7fa", null ],
    [ "GetNameChangeLocation", "d9/dfd/classG4LocatorChangeRecord.html#a0ead733efebf302150bda3ff3fb4bb19", null ],
    [ "ReportEndChanges", "d9/dfd/classG4LocatorChangeRecord.html#a499d0a66c01f1830555d1755d1ef2817", null ],
    [ "ReportVector", "d9/dfd/classG4LocatorChangeRecord.html#a5c43250b8acc4ae54b55dc5e57add343", null ],
    [ "StreamInfo", "d9/dfd/classG4LocatorChangeRecord.html#a50023261d6992fc5119d60d0104c5e3f", null ],
    [ "operator<<", "d9/dfd/classG4LocatorChangeRecord.html#a084d68fa838cb696e3151e8154d6d135", null ],
    [ "operator<<", "d9/dfd/classG4LocatorChangeRecord.html#ae6bad3f237f67e9962f0be87f3db42ed", null ],
    [ "fCodeLocation", "d9/dfd/classG4LocatorChangeRecord.html#a6845bb7ef041d8ddf40c404a9fcdec91", null ],
    [ "fEventCount", "d9/dfd/classG4LocatorChangeRecord.html#ada1c3c3af405e028cdab1e2a53fc9f51", null ],
    [ "fFieldTrack", "d9/dfd/classG4LocatorChangeRecord.html#a9a4db63be9f4abef66c2f2d5e53264e5", null ],
    [ "fIteration", "d9/dfd/classG4LocatorChangeRecord.html#a188358981e64164136c5f06828f4d2ec", null ],
    [ "fNameChangeLocation", "d9/dfd/classG4LocatorChangeRecord.html#a08b33453d083d49133828519d18ca250", null ]
];