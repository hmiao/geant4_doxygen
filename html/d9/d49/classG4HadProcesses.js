var classG4HadProcesses =
[
    [ "AddCaptureCrossSection", "d9/d49/classG4HadProcesses.html#a9dfdf6803c810d3134d496cc393a56ec", null ],
    [ "AddElasticCrossSection", "d9/d49/classG4HadProcesses.html#a196e971f600dcdc3de4024b3a076c709", null ],
    [ "AddElasticCrossSection", "d9/d49/classG4HadProcesses.html#aa0d898b6eeb3e5880a9ad7f9df7a089f", null ],
    [ "AddFissionCrossSection", "d9/d49/classG4HadProcesses.html#a6e164566fad772ffafa7841925c30781", null ],
    [ "AddInelasticCrossSection", "d9/d49/classG4HadProcesses.html#a872f2658aa894ed2d000ccda3dc96e8b", null ],
    [ "AddInelasticCrossSection", "d9/d49/classG4HadProcesses.html#a4fbf7954afd9d36b23e2cd572ba4ef1e", null ],
    [ "ElasticXS", "d9/d49/classG4HadProcesses.html#a43a69388fe82d2c623160f3053837d7b", null ],
    [ "FindCaptureProcess", "d9/d49/classG4HadProcesses.html#a811690f64356f0085f0310f2bc5858ef", null ],
    [ "FindElasticProcess", "d9/d49/classG4HadProcesses.html#af15e699a83875c9e7e71b864240b9137", null ],
    [ "FindElasticProcess", "d9/d49/classG4HadProcesses.html#a4252cf61c90f57c275ff5f1dad186e31", null ],
    [ "FindFissionProcess", "d9/d49/classG4HadProcesses.html#aa2d4323177d7347d082b542ca716ff84", null ],
    [ "FindInelasticProcess", "d9/d49/classG4HadProcesses.html#a4933b6585a5855ec346e0f2536d06f5f", null ],
    [ "FindInelasticProcess", "d9/d49/classG4HadProcesses.html#a846d2f963f3e565f68431df3ed44c7dd", null ],
    [ "FindParticle", "d9/d49/classG4HadProcesses.html#a5f127afd7a4530a06dbaef97ff27f71b", null ],
    [ "InelasticXS", "d9/d49/classG4HadProcesses.html#ad208f44d07bad95b3d7a920ae752b896", null ]
];