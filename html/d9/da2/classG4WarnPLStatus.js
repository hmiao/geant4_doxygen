var classG4WarnPLStatus =
[
    [ "G4WarnPLStatus", "d9/da2/classG4WarnPLStatus.html#ab3a199fa88dd2d79ba9b6eec675c6b7a", null ],
    [ "~G4WarnPLStatus", "d9/da2/classG4WarnPLStatus.html#a7802f41d8d947a816f3246debf051a31", null ],
    [ "Experimental", "d9/da2/classG4WarnPLStatus.html#ae8d8a1a383140d7b42db0f0bfdab74d7", null ],
    [ "OnlyFromFactory", "d9/da2/classG4WarnPLStatus.html#a4140cd124619670da71f4046c39a747c", null ],
    [ "Replaced", "d9/da2/classG4WarnPLStatus.html#afab811d6365af3ac6d72b43c73990398", null ],
    [ "Unsupported", "d9/da2/classG4WarnPLStatus.html#a24c6cea1185f433b2d002ee07b1f29c6", null ]
];