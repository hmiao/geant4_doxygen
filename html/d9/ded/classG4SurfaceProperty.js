var classG4SurfaceProperty =
[
    [ "G4SurfaceProperty", "d9/ded/classG4SurfaceProperty.html#a074cd34ea49c08142e1dbc5860753cef", null ],
    [ "G4SurfaceProperty", "d9/ded/classG4SurfaceProperty.html#a41e205345511f81b2a18784f7605bf86", null ],
    [ "~G4SurfaceProperty", "d9/ded/classG4SurfaceProperty.html#aee4f245f30cda27c3b501cbb3df7af51", null ],
    [ "CleanSurfacePropertyTable", "d9/ded/classG4SurfaceProperty.html#af900ce2ba1134a111fcb25d5ac70e1a2", null ],
    [ "DumpTableInfo", "d9/ded/classG4SurfaceProperty.html#a785c56bb27ac39cbe6b8ba4d8badf940", null ],
    [ "GetName", "d9/ded/classG4SurfaceProperty.html#a19c5d34914d6ffa1f6f54cc2abc83f8c", null ],
    [ "GetNumberOfSurfaceProperties", "d9/ded/classG4SurfaceProperty.html#a6ad0b51ab19eb2a7dee58221cbbd78ab", null ],
    [ "GetSurfacePropertyTable", "d9/ded/classG4SurfaceProperty.html#a656fbb10b60f25247ffad84792a123fd", null ],
    [ "GetType", "d9/ded/classG4SurfaceProperty.html#ac856c2191ecdf42222fda34681babd46", null ],
    [ "SetName", "d9/ded/classG4SurfaceProperty.html#ac926b532034f3d850f75b8e8b3817bcf", null ],
    [ "SetType", "d9/ded/classG4SurfaceProperty.html#a51034bd791625188c5e7864701876ba3", null ],
    [ "theName", "d9/ded/classG4SurfaceProperty.html#aad2147bb549001444fb79e9077766998", null ],
    [ "theSurfacePropertyTable", "d9/ded/classG4SurfaceProperty.html#a2257b3cf9ab8961aeef90f9b98479629", null ],
    [ "theType", "d9/ded/classG4SurfaceProperty.html#a00d57d0a9f02f597beca6c86a13e6964", null ]
];