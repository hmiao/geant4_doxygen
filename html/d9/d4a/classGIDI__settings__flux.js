var classGIDI__settings__flux =
[
    [ "GIDI_settings_flux", "d9/d4a/classGIDI__settings__flux.html#a7f28dd5aec94aa182243c634e2345e73", null ],
    [ "GIDI_settings_flux", "d9/d4a/classGIDI__settings__flux.html#aed55d734302430668df110cbf4404890", null ],
    [ "GIDI_settings_flux", "d9/d4a/classGIDI__settings__flux.html#aef6e010be7f1bf4c705ae1bf035d7f22", null ],
    [ "~GIDI_settings_flux", "d9/d4a/classGIDI__settings__flux.html#a4dafebe1630b294bad9ff224fa1905eb", null ],
    [ "addFluxOrder", "d9/d4a/classGIDI__settings__flux.html#ab31834aa0ab52c37ad051324dc838506", null ],
    [ "getLabel", "d9/d4a/classGIDI__settings__flux.html#af8ac13b39bde8f404639acd1dc26b885", null ],
    [ "getMaxOrder", "d9/d4a/classGIDI__settings__flux.html#a73f161558422e5eb4fcd2e56ab2dc003", null ],
    [ "getTemperature", "d9/d4a/classGIDI__settings__flux.html#a368e7391ebde65c1cda8e7ede551349d", null ],
    [ "isLabel", "d9/d4a/classGIDI__settings__flux.html#a27c11f373051d3ef3041a676852f3ff2", null ],
    [ "isLabel", "d9/d4a/classGIDI__settings__flux.html#afd0a422e37564e1b36158ccad087c58f", null ],
    [ "operator=", "d9/d4a/classGIDI__settings__flux.html#afd095e7a8a32c9fa0a16336f422a1621", null ],
    [ "operator[]", "d9/d4a/classGIDI__settings__flux.html#a225bfdad0b86e0e91e59ad07a8bea89e", null ],
    [ "print", "d9/d4a/classGIDI__settings__flux.html#a2a629ec3536b73cb48f08a6a3606f734", null ],
    [ "size", "d9/d4a/classGIDI__settings__flux.html#a4f870241029c8759e4caef98196e8263", null ],
    [ "mFluxOrders", "d9/d4a/classGIDI__settings__flux.html#a40d56185d0d1f7fef220065be0a094ad", null ],
    [ "mLabel", "d9/d4a/classGIDI__settings__flux.html#a75f845ce9d3205213f979d3c4d1a30ab", null ],
    [ "mTemperature", "d9/d4a/classGIDI__settings__flux.html#a77e9d8d4351d87d1069ba0701a21c52e", null ]
];