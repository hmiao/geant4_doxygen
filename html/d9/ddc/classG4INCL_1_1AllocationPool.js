var classG4INCL_1_1AllocationPool =
[
    [ "AllocationPool", "d9/ddc/classG4INCL_1_1AllocationPool.html#a442b2dfa91b9fdd9a1a655486ad74c5b", null ],
    [ "~AllocationPool", "d9/ddc/classG4INCL_1_1AllocationPool.html#a249238e38d4e87980d1ca4d4b76c27c0", null ],
    [ "clear", "d9/ddc/classG4INCL_1_1AllocationPool.html#ac11f3e182466cfdb110fa303cd7061c5", null ],
    [ "getInstance", "d9/ddc/classG4INCL_1_1AllocationPool.html#abc18fb8671e79d88fd6a5e782d3ec4fa", null ],
    [ "getObject", "d9/ddc/classG4INCL_1_1AllocationPool.html#ae63468fc7f5de6fea51e1172c4d8d919", null ],
    [ "recycleObject", "d9/ddc/classG4INCL_1_1AllocationPool.html#ad30b4ec0b0d3a13ee7b808363880ce24", null ],
    [ "theInstance", "d9/ddc/classG4INCL_1_1AllocationPool.html#a6d26a785b17133a9fd8d45c5af55399b", null ],
    [ "theStack", "d9/ddc/classG4INCL_1_1AllocationPool.html#a007b83bcfb9d004b9c31d502a1f47316", null ]
];