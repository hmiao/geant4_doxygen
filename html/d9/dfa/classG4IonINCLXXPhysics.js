var classG4IonINCLXXPhysics =
[
    [ "G4IonINCLXXPhysics", "d9/dfa/classG4IonINCLXXPhysics.html#afd1cdf4a03ad569516439bed2e948a6e", null ],
    [ "G4IonINCLXXPhysics", "d9/dfa/classG4IonINCLXXPhysics.html#a53ede2158305482c3bbd880b74fb7337", null ],
    [ "~G4IonINCLXXPhysics", "d9/dfa/classG4IonINCLXXPhysics.html#a46940eba8f82427393dd9d9b4f4e59c3", null ],
    [ "AddProcess", "d9/dfa/classG4IonINCLXXPhysics.html#ad972b6e32e4ba697bb39704a79bd1c1d", null ],
    [ "ConstructParticle", "d9/dfa/classG4IonINCLXXPhysics.html#a56c3857932eeacc7d331ff2596e0f1e8", null ],
    [ "ConstructProcess", "d9/dfa/classG4IonINCLXXPhysics.html#a31f6bd95166d8dd313d5dd5b82ba697c", null ],
    [ "deltaE", "d9/dfa/classG4IonINCLXXPhysics.html#a0c3c7c0690cfa903034cec0af2c1e804", null ],
    [ "emaxINCLXX", "d9/dfa/classG4IonINCLXXPhysics.html#a3140cb37e94dacade5b379466d08dad7", null ],
    [ "verbose", "d9/dfa/classG4IonINCLXXPhysics.html#a0ef3da4b1fd79f82b658f20ad3983d1c", null ]
];