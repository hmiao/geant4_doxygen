var structGL2PSprimitive =
[
    [ "boundary", "d9/d39/structGL2PSprimitive.html#a1888e49651c47c98848b80287eeb6ab1", null ],
    [ "culled", "d9/d39/structGL2PSprimitive.html#a4df4eaa4723bf58404efa81e600a8fc9", null ],
    [ "data", "d9/d39/structGL2PSprimitive.html#a11110391d3e2e91ec4a12ec6a9538159", null ],
    [ "factor", "d9/d39/structGL2PSprimitive.html#a668f776cc038ba6697fb3a59ac9808d8", null ],
    [ "image", "d9/d39/structGL2PSprimitive.html#aeb241b576955f15767567f8f12d82088", null ],
    [ "linecap", "d9/d39/structGL2PSprimitive.html#a246742aa30ae0c41d896feb940b90be3", null ],
    [ "linejoin", "d9/d39/structGL2PSprimitive.html#a988d1903658949870a3134c26583f54a", null ],
    [ "numverts", "d9/d39/structGL2PSprimitive.html#a0c08e03e33ce4a72b2a0ffa1af80621c", null ],
    [ "ofactor", "d9/d39/structGL2PSprimitive.html#ab9a7114df8dd1bc6074b2ef419e6dc2e", null ],
    [ "offset", "d9/d39/structGL2PSprimitive.html#a17d640d1108f03b6e57c9a4705266944", null ],
    [ "ounits", "d9/d39/structGL2PSprimitive.html#a746634b3c75f5a75e150a96beb0d90b1", null ],
    [ "pattern", "d9/d39/structGL2PSprimitive.html#a60f7dcbf793edb8c10f617e77a817c33", null ],
    [ "sortid", "d9/d39/structGL2PSprimitive.html#a45aed64777ca12b19967814b41cfc0d2", null ],
    [ "text", "d9/d39/structGL2PSprimitive.html#af54b8ee11bcb5cc40daed4f311597053", null ],
    [ "type", "d9/d39/structGL2PSprimitive.html#a760f35c02d70a961d9a1e2c39f083136", null ],
    [ "verts", "d9/d39/structGL2PSprimitive.html#a8e2aec50f1c86078f89a930588f30f0c", null ],
    [ "width", "d9/d39/structGL2PSprimitive.html#aebf18baa0efe97f7e57bef844d998d76", null ]
];