var classG4PartialWidthTable =
[
    [ "G4PartialWidthTable", "d9/da7/classG4PartialWidthTable.html#a60bb7e30560ed413f6d3853926fb475b", null ],
    [ "~G4PartialWidthTable", "d9/da7/classG4PartialWidthTable.html#a901c15d647e1aa1e2b3ace051f9f883f", null ],
    [ "G4PartialWidthTable", "d9/da7/classG4PartialWidthTable.html#a4bc8fe9f8cd156e94d34995cd1dd9984", null ],
    [ "AddWidths", "d9/da7/classG4PartialWidthTable.html#a697d7f3c31f59cf7862794e97e52675e", null ],
    [ "Dump", "d9/da7/classG4PartialWidthTable.html#a6c82428faf33645a5f65cc648999ac6b", null ],
    [ "NumberOfChannels", "d9/da7/classG4PartialWidthTable.html#ad39c65069880d7cf12edc96e7f276351", null ],
    [ "operator!=", "d9/da7/classG4PartialWidthTable.html#a16ccff0c1215442586479496988df6ef", null ],
    [ "operator=", "d9/da7/classG4PartialWidthTable.html#a4a71d58d87f7a3650c9028e7124ec178", null ],
    [ "operator==", "d9/da7/classG4PartialWidthTable.html#af602581d1d6e7412f00e06a4d39c0fa7", null ],
    [ "Width", "d9/da7/classG4PartialWidthTable.html#a07a77b66e987fc9e3105908c153198e3", null ],
    [ "daughter1", "d9/da7/classG4PartialWidthTable.html#a73f9af95252ad43af34e50cd71f6124f", null ],
    [ "daughter2", "d9/da7/classG4PartialWidthTable.html#a445a934bd5a6cedca5ad7cc139525cbd", null ],
    [ "energy", "d9/da7/classG4PartialWidthTable.html#a5a0f255e12d15c10ce1a4c9f4f18860c", null ],
    [ "nEnergies", "d9/da7/classG4PartialWidthTable.html#a173b0f09ee7dbe5673d300b824bb7443", null ],
    [ "widths", "d9/da7/classG4PartialWidthTable.html#a994f7d2727d0a7fc32cb121f22e2425d", null ]
];