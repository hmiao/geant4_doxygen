var classG4OpWLS2 =
[
    [ "G4OpWLS2", "d9/d3f/classG4OpWLS2.html#ad80b574faeddd01f1932bdbdfc4b0328", null ],
    [ "~G4OpWLS2", "d9/d3f/classG4OpWLS2.html#a3c0632d16914758d0e56e27404e04c1f", null ],
    [ "G4OpWLS2", "d9/d3f/classG4OpWLS2.html#ade214ffd7544c9d1d47dfe9c622364ca", null ],
    [ "BuildPhysicsTable", "d9/d3f/classG4OpWLS2.html#a6769a63006e3de6266e3c42b321c25f4", null ],
    [ "DumpPhysicsTable", "d9/d3f/classG4OpWLS2.html#a71797d6d6fc87fa8dd72107add79827b", null ],
    [ "GetIntegralTable", "d9/d3f/classG4OpWLS2.html#a63fabe9c7f875b42d4aa6816abffe12d", null ],
    [ "GetMeanFreePath", "d9/d3f/classG4OpWLS2.html#ad42b16a79224cd5492c6e23f56a1f570", null ],
    [ "Initialise", "d9/d3f/classG4OpWLS2.html#a18c056eb0ac9b51a0709d29403decaf3", null ],
    [ "IsApplicable", "d9/d3f/classG4OpWLS2.html#a38a9b8c790e2eb569e73da5e32d4abad", null ],
    [ "operator=", "d9/d3f/classG4OpWLS2.html#af661180b4991bfc11560876aca869c14", null ],
    [ "PostStepDoIt", "d9/d3f/classG4OpWLS2.html#ab2ba8d678d31766b8cc61b00c4ceef38", null ],
    [ "PreparePhysicsTable", "d9/d3f/classG4OpWLS2.html#aa48975d47ec06f3a294e4154686951b9", null ],
    [ "SetVerboseLevel", "d9/d3f/classG4OpWLS2.html#a089de4597d392e84c2f204c69739423d", null ],
    [ "UseTimeProfile", "d9/d3f/classG4OpWLS2.html#a3533b7eddfc7a3553c39a1affe1cf3eb", null ],
    [ "idx_wls2", "d9/d3f/classG4OpWLS2.html#aabe04f7d45ea518741fab09a9b89db84", null ],
    [ "theIntegralTable", "d9/d3f/classG4OpWLS2.html#ac3e875c8b07d0b7c8bb30b4c0ec5b7ec", null ],
    [ "WLSTimeGeneratorProfile", "d9/d3f/classG4OpWLS2.html#ac906b53f150dc38fb81ad3680672e4b6", null ]
];