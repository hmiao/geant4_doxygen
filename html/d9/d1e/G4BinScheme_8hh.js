var G4BinScheme_8hh =
[
    [ "G4BinScheme", "d9/d1e/G4BinScheme_8hh.html#aa396ffb9e5957167b5e5210ab1a82c87", [
      [ "kLinear", "d9/d1e/G4BinScheme_8hh.html#aa396ffb9e5957167b5e5210ab1a82c87aac97f0008bcf7c7fe4f2ff94160e1385", null ],
      [ "kLog", "d9/d1e/G4BinScheme_8hh.html#aa396ffb9e5957167b5e5210ab1a82c87af7971cffe1eeab35748c8d08e50703ec", null ],
      [ "kUser", "d9/d1e/G4BinScheme_8hh.html#aa396ffb9e5957167b5e5210ab1a82c87a3ea6f9f6140b78f0c5a050d926097f56", null ]
    ] ],
    [ "ComputeEdges", "d9/d1e/G4BinScheme_8hh.html#a98df8ef13fc8cc677eb2303b86ab7fbf", null ],
    [ "ComputeEdges", "d9/d1e/G4BinScheme_8hh.html#a9dfd8939fcf97b7ee3343b8665e8d178", null ],
    [ "GetBinScheme", "d9/d1e/G4BinScheme_8hh.html#a0b926bbdd3d26c2a12786e50f9501aea", null ]
];