var classG4tgrMaterialSimple =
[
    [ "G4tgrMaterialSimple", "d9/d5b/classG4tgrMaterialSimple.html#adddaa2816930060705aee82bc8e26cd3", null ],
    [ "~G4tgrMaterialSimple", "d9/d5b/classG4tgrMaterialSimple.html#adab45123e84ca519f2eddc23e4ac23b6", null ],
    [ "G4tgrMaterialSimple", "d9/d5b/classG4tgrMaterialSimple.html#abd02702fcbdbf20ffd9bc6fe52a6103b", null ],
    [ "GetA", "d9/d5b/classG4tgrMaterialSimple.html#a98d56dd3f8beaea5868597a981973ae3", null ],
    [ "GetComponent", "d9/d5b/classG4tgrMaterialSimple.html#a578a576fa0b049bb626ebda7f3d3dd09", null ],
    [ "GetFraction", "d9/d5b/classG4tgrMaterialSimple.html#a4673f075e014c0696280d418aa06d3e7", null ],
    [ "GetZ", "d9/d5b/classG4tgrMaterialSimple.html#a028e7a244f57e68ad3f3c65a9aa8f865", null ],
    [ "operator<<", "d9/d5b/classG4tgrMaterialSimple.html#a049493714033b9022eaf4712e607685e", null ],
    [ "name", "d9/d5b/classG4tgrMaterialSimple.html#a2b904a0db89ba4fbd091b45a17668be6", null ],
    [ "theA", "d9/d5b/classG4tgrMaterialSimple.html#a5dbe11dcd3393d66fdba334ddce0b186", null ],
    [ "theZ", "d9/d5b/classG4tgrMaterialSimple.html#af810102f1e19b4d63f9b4d2d90a1ccae", null ]
];