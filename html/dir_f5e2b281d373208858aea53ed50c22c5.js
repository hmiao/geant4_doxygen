var dir_f5e2b281d373208858aea53ed50c22c5 =
[
    [ "G4BiasingHelper.hh", "de/d3b/G4BiasingHelper_8hh.html", "de/d3b/G4BiasingHelper_8hh" ],
    [ "G4BiasingProcessInterface.hh", "d8/d81/G4BiasingProcessInterface_8hh.html", "d8/d81/G4BiasingProcessInterface_8hh" ],
    [ "G4BiasingProcessSharedData.hh", "df/da0/G4BiasingProcessSharedData_8hh.html", "df/da0/G4BiasingProcessSharedData_8hh" ],
    [ "G4BOptnChangeCrossSection.hh", "db/d63/G4BOptnChangeCrossSection_8hh.html", "db/d63/G4BOptnChangeCrossSection_8hh" ],
    [ "G4BOptnCloning.hh", "d4/dae/G4BOptnCloning_8hh.html", "d4/dae/G4BOptnCloning_8hh" ],
    [ "G4BOptnForceCommonTruncatedExp.hh", "d3/dee/G4BOptnForceCommonTruncatedExp_8hh.html", "d3/dee/G4BOptnForceCommonTruncatedExp_8hh" ],
    [ "G4BOptnForceFreeFlight.hh", "df/d22/G4BOptnForceFreeFlight_8hh.html", "df/d22/G4BOptnForceFreeFlight_8hh" ],
    [ "G4BOptnLeadingParticle.hh", "d8/de2/G4BOptnLeadingParticle_8hh.html", "d8/de2/G4BOptnLeadingParticle_8hh" ],
    [ "G4BOptrForceCollision.hh", "d1/d63/G4BOptrForceCollision_8hh.html", "d1/d63/G4BOptrForceCollision_8hh" ],
    [ "G4BOptrForceCollisionTrackData.hh", "d1/d10/G4BOptrForceCollisionTrackData_8hh.html", "d1/d10/G4BOptrForceCollisionTrackData_8hh" ],
    [ "G4ILawCommonTruncatedExp.hh", "d7/d6b/G4ILawCommonTruncatedExp_8hh.html", "d7/d6b/G4ILawCommonTruncatedExp_8hh" ],
    [ "G4ILawForceFreeFlight.hh", "d1/df5/G4ILawForceFreeFlight_8hh.html", "d1/df5/G4ILawForceFreeFlight_8hh" ],
    [ "G4ILawTruncatedExp.hh", "df/db4/G4ILawTruncatedExp_8hh.html", "df/db4/G4ILawTruncatedExp_8hh" ],
    [ "G4InteractionLawPhysical.hh", "db/dcc/G4InteractionLawPhysical_8hh.html", "db/dcc/G4InteractionLawPhysical_8hh" ],
    [ "G4ParallelGeometriesLimiterProcess.hh", "dc/d24/G4ParallelGeometriesLimiterProcess_8hh.html", "dc/d24/G4ParallelGeometriesLimiterProcess_8hh" ],
    [ "G4ParticleChangeForNothing.hh", "d5/d00/G4ParticleChangeForNothing_8hh.html", "d5/d00/G4ParticleChangeForNothing_8hh" ],
    [ "G4ParticleChangeForOccurenceBiasing.hh", "d7/d96/G4ParticleChangeForOccurenceBiasing_8hh.html", "d7/d96/G4ParticleChangeForOccurenceBiasing_8hh" ]
];